$(document).ready(function(){

	$(".modal-trigger").off("click").on("click",function(){
		$("body").css({overflow:'hidden'});
		var tm = $(this).attr("modal-target");

		$("div[modal-id~='"+tm+"']").addClass("showed");

		$("div[modal-id~='"+tm+"'] .close-me").on("click",function(){
			$("body").css({'overflow-y':'initial'});
			$("div[modal-id~='"+tm+"']").removeClass("showed");
			$('#myFeedback').hide();
		});
	});

	var uiModal = $("#trashModal");
	uiModal.find(".close-me.red-color").off("click.hidetrash").on("click.hidetrash", function(){
		$("body").css({'overflow-y':'initial'});
		$(this).closest('.modal-container').removeClass("showed");
	});

	//datepicker
	$(".dp:not(.time)").datetimepicker({pickTime: false});
	$(".dp.time").datetimepicker({pickDate: false});

	// navi
	$(".side-nav-container").mCustomScrollbar({
		scrollInertia:150
	});

	$(".nav-ticker").each(function(){
		$(this).off("click").on("click",function(e){
			e.preventDefault();
			var snh, snc;

			snh = $(this).next(".subnav-panel").find("ul").height();
			snc = $(this).next(".subnav-panel");
			
			snc.toggleClass("show");


			if(snc.hasClass("show")){
				snc.css({height:snh});
				setTimeout(function(){
					snc.removeAttr("style");
				},500)
			}else{

				snc.css({height:snh});
				setTimeout(function(){
					snc.removeAttr("style");
				},100)
			}

		});
	});

	// accordion
	$(".panel-group .panel-heading").each(function(){
		var ps = $(this).next(".panel-collapse");
		var ph = ps.find(".panel-body").outerHeight();

		if(ps.hasClass("in")){
			$(this).find("h4").addClass("active")
		}

		$(this).find("a").off("click").on("click",function(e){
			e.preventDefault();
			ps.css({height:ph});

			if(ps.hasClass("in")){
				$(this).find("h4").removeClass("active");
				$(this).find(".fa").removeClass("fa-caret-down").addClass("fa-caret-right");
				ps.removeClass("in");
			}else{
				$(this).find("h4").addClass("active");
				$(this).find(".fa").removeClass("fa-caret-right").addClass("fa-caret-down");
				ps.addClass("in");
			}

			setTimeout(function(){
				ps.removeAttr("style")
			},500);
		});
	});
	
	// notification
	$("header .announcement").off("click").on("click",function(){
		if($(".notif").hasClass("hide")){
			$(".notif").removeClass("hide");
			$(".notif").addClass("showing");
			setTimeout(function(){
				$(".notif").removeClass("showing");
				$(".notif").addClass("show");
			},200);
		}else{
			$(".notif").removeClass("show");
			$(".notif").addClass("hidding");
			setTimeout(function(){
				$(".notif").removeClass("hidding");
				$(".notif").addClass("hide");
			},200);
		}
	}).on("blur",function(){
		$(".notif").removeClass("show");
		$(".notif").addClass("hidding");
		setTimeout(function(){
			$(".notif").removeClass("hidding");
			$(".notif").addClass("hide");
		},200);
	});

	//setting
	$(".settings").off("click").on("click",function(){
		var setL = $(this).offset().left - 23.75;
		
		$(".settings-box").css({left:setL});
		if($(".settings-box").hasClass("showed")){
			$(".settings-box").stop().fadeOut(300,function(){
				$(this).removeClass("showed")
			});
		}else{
			$(".settings-box").stop().fadeIn(300,function(){
				$(this).addClass("showed")
			});
		}
	});

	// deals interact show/hide
	$(".show-hide-interact").off("click").on("click",function(){
		if($(".show-interact").hasClass("showed")){
			$(".show-interact").stop().fadeOut(500,function(){
				$(".edit-interact").stop().fadeIn(500);
				$(".show-interact").removeClass("showed");
			});
		}else{
			$(".edit-interact").stop().fadeOut(500,function(){
				$(".show-interact").stop().fadeIn(500);
				$(".show-interact").addClass("showed");
			});
		}
	});

	// deals
	$(".email.template").css({"display":"block"});
	$(".template-type").find(".option").off("click").on("click",function(){

		var val = $(this).attr("data-value");
		$(".template").removeAttr("style");
		$("."+val+".template").css({"display":"block"});
	});

	//tooltip
	$("body").append("<div class='show-dialogue'></div>");
	if($("body .tool-tip").length > 0){
		$(".tool-tip").each(function(){
			var tthtml = $(this).attr("tt-html");

			$(this).mouseenter(function(){
				var ttx = $(this).offset().left;
				var tty = $(this).offset().top - ($(".show-dialogue").height() + 30) ;
				$(".show-dialogue").css({top:tty,left:ttx}).html(tthtml).stop().fadeIn(300);
			}).mouseleave(function(){
				$(".show-dialogue").stop().html(tthtml).fadeOut(300);
			});
		});
	}	

	 $('.search-button1').off("click").on("click", function(){
        $('.container-show').toggleClass("show");        
        $('.search-button-display1').toggleClass("show");
     })
	 $('.search-button2').off("click").on("click", function() {
	 	$('.container-show1').toggleClass("show");
	 })

	 $('.search-button').off("click").on("click", function(){
        $('.search-button-display').toggleClass("show");
    })
	 

	 // receiving delivery detail
	 	// default value
	 $('.ship-name').hide();
	 $('.air-name').hide();
	 $('.item-input').hide();
	 $('.ship-next').hide();
	 $('.air-next').hide();
	 $('.item-next').hide();
	 $('.modepayment').hide();
	 $('.select-payment').hide();
	 $('.forward-name').hide();
	 $('.forwarder').hide();


	 $('.select div.option').click(function(){
	 	var deldetails = $(this).text();	 	
	 	switch (deldetails) {
	 		case 'Ship':
	 			$('.forward-name').hide();
	 			$('.forwarder').hide();
	 			$('.ship-name').show();
	 			$('.ship-next').show();
				$('.air-next').hide();
	 			$('.air-name').hide();
	 			$('.item-input').show();	 			
	 			$('.item-next').show();	 			
	 			break;
	 		case 'Air':
	 			$('.forward-name').hide();
	 			$('.forwarder').hide();
	 			$('.ship-name').hide();
	 			$('.ship-next').hide();
	 			$('.air-name').show();
	 			$('.air-next').show();
	 			$('.item-input').show();	 			
	 			$('.item-next').show();	 			
	 			break;
	 		case 'Forwarder':
	 			$('.forward-name').show();
	 			$('.forwarder').show();
	 			$('.ship-name').hide();
				$('.ship-next').hide();
				$('.air-name').hide();
				$('.air-next').hide();
				$('.item-input').hide();				
				$('.item-next').hide();
				break;
	 		case 'Empty':
	 			$('.forward-name').hide();
	 			$('.forwarder').hide();
	 			$('.ship-name').hide();
				$('.ship-next').hide();
				$('.air-name').hide();
				$('.air-next').hide();
				$('.item-input').hide();				
				$('.item-next').hide();
				break;
	 	}
	 })


	 //receiving radiobutton product transfer delivery 
	 $('input[name=delivery]').click(function() {
	 	if ($('.prodel').is(':checked')) {	 		
	 		$('.modepayment').show();
	 		$('.select-payment').show();

	 		$('.origin').hide(); 
	 		$('.select-origin').hide();	 		
	 		$('.mode-del').hide();
	 		$('.select-mode').hide();

	 		$('.air-next').hide();
	 		$('.air-name').hide();
 			$('.ship-name').hide();
 			$('.ship-next').hide()
			$('.item-input').hide();				
			$('.item-next').hide();

	 	} else {
	 		$('select-mode').val('empty');	 		
	 		$('.modepayment').hide();
	 		$('.select-payment').hide();

	 		$('.origin').show(); 
	 		$('.select-origin').show();	 		
	 		$('.mode-del').show();
	 		$('.select-mode').show();
	 	}})

	 /*$(".close-me").click(function() {
	 	
	 	$(".display-input").hide();
	 	$(".btn-modal-display").hide();
	 	$(".display-text").show();
	
	 });*/

	 $(".display-input").hide();
	 $(".btn-modal-display").hide();
	 $(".edit-display").off("click").on("click", function() {
	 	$(".display-input").show();
	 	$(".display-text").hide();
	 	$(".btn-modal-display").show();
	 });


	 $('.wh-select .select.stocks div.option').click(function() {
		var selectedValue = $(this).text();			
		switch(selectedValue) {
			case 'Product': 
				window.location.href="product-inventory-1.php"				
				break;
			case 'Storage':
				window.location.href="product-inventory-2.php"
				break;
		}
	});

	 $('.wh-select .select.consignee div.option').click(function() {
		var selectedValue = $(this).text();			
		switch(selectedValue) {
			case 'Product': 
				window.location.href="consignee-inventory.php"				
				break;
			case 'Storage Location':
				window.location.href="consignee-storage-location.php"
				break;
		}
	});

	 

	 // loading accordion	 
 	// if ($(".loading-type .panel-collapse").hasClass("in") == true) {	 		
 	// 	$(".loading-type .panel-collapse .panel-body").addClass("padding-bottom-90");
 	// } else {
 	// 	$(".loading-type .panel-collapse .panel-body").removeClass(".padding-bottom-90");
 	// }

 	$(".batch-check").each(function() {
		
		var selectedLocation = true;
		
		$(this).off("click").on("click", function() {
					
			if (selectedLocation == true) {
				$(this).removeClass('bggray-middark');
				$(this).addClass('bggray-7cace5');
				$(this).siblings(".qty-text-parent").find(".qty-text").removeAttr("disabled");
				$(this).find(".check").removeClass('gray-color');
				$(this).find(".check").addClass('black-color');

				selectedLocation = false;
			} else {
				$(this).removeClass('bggray-7cace5');
				$(this).addClass('bggray-middark');
				$(this).siblings(".qty-text-parent").find(".qty-text").attr("disabled",true);
				$(this).find(".check").removeClass('black-color');
				$(this).find(".check").addClass('gray-color');
				selectedLocation = true;
			};

		});
	});
	
	 

 	$(".content-bullet").hide();
	 // product inventory list
	 $(".bullet").off("click").on("click", function() {
	 	$(".content-list").fadeOut();
	 	$(".content-bullet").fadeIn();
	 	$(this).css({
	 		'color':'#000',
	 		'fontSize':'20px'
	 	});
	 	$(".list").css({
	 		'color':'#808080',
	 		'fontSize':'18px'
	 	});
	 });
	 $(".list").off("click").on("click",function() {
	 	$(".content-list").fadeIn();
	 	$(".content-bullet").fadeOut();
	 	$(this).css({
	 		'color':'#000',
	 		'fontSize':'20px'
	 	});
	 	$(".bullet").css({
	 		'color':'#808080',
	 		'fontSize':'18px'
	 	});
	 });
	 
	 // transfer product dropdown
	 $('.drop-button div.option').click(function () {
	 	var myValue = $(this).text();
	 	switch(myValue) {
	 		case 'Stock Goods':
	 			window.location.href="transfer-products-stock.php"
	 			break;
	 		case 'Consignee' :
	 			window.location.href="transfer-products.php"
	 			break;
	 	}
	 });

	 // withdraw product dropdown
	 $('.drop-button-withdraw div.option').click(function () {
	 	var myValue = $(this).text();
	 	switch(myValue) {
	 		case 'Stock Goods':
	 			window.location.href="withdraw-product-stock.php"
	 			break;
	 		case 'Consignee' :
	 			window.location.href="withdraw-product.php"
	 			break;	 		
	 	}
	 });

	 // receiving dropdown 
	 $('.drop-receiving div.option').click(function() {
	 	var myValue = $(this).text();
	 	switch(myValue) {
	 		case 'Stock Goods':
	 			window.location.href="create-receiving-record-stock.php";	 			
	 			break;
	 		case 'Consignee - Truck':
	 			window.location.href="create-receiving-record-truck.php";
	 			break;
	 		case 'Consignee - Vessel':
	 			window.location.href="create-receiving-record-vessel.php";
	 			break;
	 	}
	 });


	 // withdraw product radio button 
	$(".vessel-tbl-display").hide();
	$(".mod-text-edit").text("Scale Ticket No.:");
	$(".truck-tbl").hide();
	$(".vessel-radio label").off("click").on("click", function(){
		$(".truck-tbl").hide();
		$(".vessel-tbl").show();
		$(".mod-text").text("SRS No.");
	});
	$(".truck-radio label").off("click").on("click", function(){
		$(".truck-tbl").show();
		$(".vessel-tbl").hide();
		$(".mod-text").text("Scale Ticket No.:");
	});


	// withdraw modal
	$(".void-button").off("click").on("click", function() {
		$(".button-content button").hide();
		$(".status-result").text("Void");
		$(".date-trans").text("N/A");
	})

	$(".complete-button").off("click").on("click", function(){
		$(".button-content button").hide();		
		$(".method-loading").text("Manual Labor");
		$(".area-loading").text("Area 54");
		$(".date-trans").text("September 11, 2015");
		$(".method-loading").text("Manual Labor");
		$(".time-start-loading").text("09/11/15 | 09:00 AM");
		$(".time-end-loading").text("09/11/15 | 09:00 AM");
		$(".duration-loading").text("1 Day, 0 Hours");
		$(".gross-weight").text("1500 Kilograms");
		$(".net-weight").text("500 Kilograms");
		$(".final-weight").text("1000 Kilograms");
	})


	// accordion for product-inventory 
	$(".panel-title").off("click").on("click", function() {		
		$(".panel-title i").removeClass("fa-caret-right").addClass("fa-caret-down");
	});
	$(".panel-title .caret-click").off("click").on("click", function() {
		$(this).removeClass(".fa-caret-right").addClass(".fa-caret-down");
	});

	// hover button on product inventory	
	$(".with-button button").hide();
	$(".with-button").hover(function() {
		$(".with-button button").fadeIn();
	});

	$(".with-button").mouseleave(function() {
		$(".with-button button").fadeOut();
	})

	// button receving navigation 
	// stock
	$(".assign-storage-stock").off("click").on("click", function() {
		window.location.href="view-receiving-record2-stock.php"
	});
	$(".complete-storage-stock").off("click").on("click", function() {
		window.location.href="view-receiving-record4-stock.php"
	});
	$(".verify-product-stock").off("click").on("click", function() {
		window.location.href="view-receiving-record5-stock.php"
	});
	$(".complete-quantity-stock").off("click").on("click", function() {
		window.location.href="view-receiving-record3-stock.php"
	})

	// vessel
	$(".assign-storage-vessel").off("click").on("click", function() {
		window.location.href="view-receiving-record2-vessel.php"
	})
	$(".complete-storage-vessel").off("click").on("click", function() {
		window.location.href="view-receiving-record4-vessel.php"
	});
	$(".verify-product-vessel").off("click").on("click", function() {
		window.location.href="view-receiving-record5-vessel.php";
	});
	$(".complete-quantity-vessel").off("click").on("click", function() {
		window.location.href="view-receiving-record3-vessel.php";
	});

	// truck
	$(".assign-storage-truck").off("click").on("click", function() {
		window.location.href="view-receiving-record2-truck.php"
	})
	$(".complete-storage-truck").off("click").on("click", function() {
		window.location.href="view-receiving-record4-truck.php"
	});
	$(".verify-product-truck").off("click").on("click", function() {
		window.location.href="view-receiving-record5-truck.php";
	});
	$(".complete-quantity-truck").off("click").on("click", function() {
		window.location.href="view-receiving-record3-truck.php";
	});

	// receiving truck interaction 
	receiving_panel_product1();
	receiving_panel_product2();

	tool_tip();

	// edit detail link inside accordion header
	$(".other-doc-js").off("click").on("click", function() {
		window.location.href="edit-receiving-truck1.php#upload-document";
	});

	$(".other-receiving-js").off("click").on("click", function() {
		window.location.href="edit-receiving-truck1.php#receiving-information";
	});

	$(".vessel-js").off("click").on("click", function() {
		window.location.href="edit-receiving-vessel1.php#upload-document";
	});

	$(".other-stock-js").off("click").on("click", function() {
		window.location.href="edit-receiving-stock1.php";
	});
	$(".withdrawal-truck").off("click").on("click", function() {
		window.location.href="edit-product-truck.php";
	});
	$(".withdrawal-truck1").off("click").on("click", function() {
		window.location.href="edit-product-truck1.php";
	});
	$(".withdrawal-stock").off("click").on("click", function() {
		window.location.href="edit-product-stock1.php";	
	});
	$(".withdrawal-stock-profile").off("click").on("click", function() {
		window.location.href="edit-product-stock.php";
	});
	$(".withdrawal-vessel-profile").off("click").on("click", function() {
		window.location.href="edit-product-vessel.php";
	});
	$(".withdrawal-information").off("click").on("click", function() {
		window.location.href="edit-product-vessel1.php";
	});


	$(".final-btn").off("click").on("click", function() {
		$(".assign-storage-save").hide();
		$(".assign-storage-popup").hide();
		$(".verify-quantity-save").hide();
		$(".verify-quantity-check").hide();
		$(".assign-storage-save1-other").hide();
		$(".assign-storage-popup1-other").hide();
		$(".verify-quantity-save-other").hide();
		$(".verify-quantity-check-other").hide();
		$(".receiving-link").hide();
		$(".change-status").text("Complete");
		$(".finalize-btn").hide();
		$(".void-btn").hide();
		$(".vessel-js").hide();
		$(".verify-stock").hide();
		$(".verify-stock-save").hide();
		$(".other-stock-js").hide();

		$(".pending-stock").text("Complete");
	});

	$(".stock-expected1").hide();
	$(".stock-loss1").hide();
	$(".stock-details1").hide();
	$(".stock-remarks1").hide();
	$(".stock-product1").hide();

	$(".verify-stock-save").hide();

	$(".verify-stock").off("click").on("click", function() {

		if ($(".stock-expected").is(":visible")) {
			
			$(".stock-expected").hide();
			$(".stock-loss").hide();
			$(".stock-details").hide();
			$(".stock-remarks").hide();
			$(".stock-product").hide();	

			$(".stock-expected1").fadeIn();
			$(".stock-loss1").fadeIn();
			$(".stock-details1").fadeIn();
			$(".stock-remarks1").fadeIn();
			$(".stock-product1").fadeIn();

			$(this).attr("disabled","disabled");

			$(".verify-stock-save").show();



		} else {

			$(".stock-expected1").hide();
			$(".stock-loss1").hide();
			$(".stock-details1").hide();
			$(".stock-remarks1").hide();
			$(".stock-product1").hide();

			$(".stock-expected").fadeIn();
			$(".stock-loss").fadeIn();
			$(".stock-details").fadeIn();
			$(".stock-remarks").fadeIn();
			$(".stock-product").fadeIn();
		}
	});
	$(".verify-stock-save").off("click").on("click", function() {

			$(".stock-expected1").hide();
			$(".stock-loss1").hide();
			$(".stock-details1").hide();
			$(".stock-remarks1").hide();
			$(".stock-product1").hide();

			$(".stock-expected").fadeIn();
			$(".stock-loss").fadeIn();
			$(".stock-details").fadeIn();
			$(".stock-remarks").fadeIn();
			$(".stock-product").fadeIn();	

			$(".verify-stock").removeAttr("disabled").text("Edit Product Quantity");

			$(".pending-stock").text("On Going 1 of 2 Pending");
			$(this).hide();
			$(".verify-stock-save").hide();
	});
	
	// composite product trigger switch with modal 
	$(".composite-activated label").off("click").on("click", function() {
		if ($(".composite-activated .toggleSwitch input").is(":checked")){
			$(".toggleSwitch input").attr("checked", false);
			$(".composite-product-modal").css({"display":"block"}).addClass("showed");

		} else {
			$(".composite-add-product").slideDown(500);
			$(".remove-composite").show();			
		}
	});

	$(".composite-product-modal .close-me").click(function() {		 	
		$(".composite-product-modal").removeClass("showed");
	});

	$(".composite-product-modal .yes-btn").off("click").on("click", function() {		
		$(".add-composite-switch .switch").attr("checked", false);		
		$(".composite-add-product").slideUp(500);		
		$(".remove-composite").hide();	
		$('div[modal-id="compo-product"]').removeClass('showed');
		$('div[modal-id="compo-product"]').removeAttr("<style></style>");
		//$(".composite-product-modal .close-me").click();		
	});
	$(".empty-composite").hide();
	$(".empty-composite").hide();
	$(".add-product-result").hide();
	$(".add-product-content").hide();


	// modal container 
	$(".with-modal .frm-custom-dropdown-option .option:last-child").click(function() {
		$(".add-cat-mdl").css({"display":"block"}).addClass("showed");	
	});
	$(".add-cat-mdl .close-me").click(function() {
		$(".add-cat-mdl").removeClass("showed");
	});


	$(".thumb_list_view").click(function(){			
		$(this).stop().toggleClass("marked");
	});
  
  	$(".popup_person_list").hide();
  	var popup_person = true;
  	$(".member-container").off("click").on("click", function() {
  		if (popup_person == true) {
  			$(".popup_person_list").slideDown();  		
  			popup_person = false;
  		} else {
  			$(".popup_person_list").slideUp();  			
  			popup_person =  true;
  		}  		
  	});

  	$(".table-content .content-show").each(function() {
  		var getHeight = $(this).outerHeight();	
  		$(this).closest(".table-content").find(".content-hide").css({ 'height' : getHeight });  		
  	});

  	$(".hover-transfer-tbl .first-tbl").each(function() {
  		var getHeight = $(this).outerHeight();
  		// $(this).closest(".hover-transfer-tbl").find(".hover-tbl").css({'height' : getHeight});
  		$(this).siblings(".hover-tbl").css({'height':getHeight});
  	});

  	$(".tbl-like .first-tbl").each(function() {
  		var getHeight = $(this).outerHeight();
  		// $(this).closest(".hover-transfer-tbl").find(".hover-tbl").css({'height' : getHeight});
  		$(this).siblings(".hover-tbl").css({'height':getHeight});
  	});

  	$(".transfer-prod-location .show-ongoing-content").each(function(){
  		var storeHeight = $(this).outerHeight();
  		$(this).closest(".transfer-prod-location").find(".ongoing-edit-hide").css({'height' : storeHeight});
  	});


  	$(".modal-to-transfer").off("click").on("click", function() {
	    window.location.href="view-transfer-ongoing.php"	    
	    localStorage.setItem("modal-tracker", true);	      
	});
  	

	// modal container hide result 
	$(".modal-person-hide").hide();  
	$(".modal-person-trigger").off("click").on("click", function() {
		$(".modal-person-hide").slideDown();
		$(".no-contact-hide").hide();
	});

	// add consignee hover image 
	$(".left-information .hover-image").hide();
	$(".left-information .image-here").hover(function() {
		$(".hover-image").fadeIn(300);
		$(".hover-image p").css({'color':'#fff'});
	}, function() {
		$(".hover-image").fadeOut(300);
	});	

	$(".show-new-window").off("click").on("click", function() {
		window.open("stock-viewing.php", "windowName", "height=800,width=1200");
	});

	$(".show-view-record").off("click").on("click", function() {
		window.open("consign-viewing.php", "windowName", "height=800,width=1200");
	});

	$(".show-consignee-record").off("click").on("click", function() {
		window.open("consignee-viewing.php", "windowName", "height=800,width=1200");
	});
	$(".show-consignee-record-truck").off("click").on("click", function() {
		window.open("consignee-viewing-truck.php", "windowName", "height=800,width=1200");
	});

	$(".withdrawal-show-record").off("click").on("click", function() {
		window.open("stock-withdrawal-show.php", "windowName", "height=800,width=1200");
	})


	// // left navigation control 
	// $(".config-click").off("click").on("click", function() {
	// 	// $(this).siblings(".subnav-panel").addClass("show").;
	// });
	$(".truck-delivery, .vessel-delivery").hide();
	$(".mode-of-deliver.select div.option").click(function() {
		var selectedDelivery = $(this).text();
		switch (selectedDelivery) {
			case 'Vessel' :
				$(".vessel-delivery").slideDown();
				$(".truck-delivery").slideUp();
				break;
			case 'Truck' :
				$(".vessel-delivery").slideUp();
				$(".truck-delivery").slideDown();
				break;
			default : 
				break;
		}
	});

	// consignee view ongoing under storage assignment 



	$(".storage-assign-panel").each(function() {
		var getHeight = $(this).outerHeight();				
		$(this).find(".btn-hover-storage").css({'height': getHeight});
	});

	$(".storage-assign-panel").each(function() {
		$(this).hover(function() {
			$(this).find(".btn-hover-storage").css({'opacity' :'1'});
		}, function() {
			$(this).find(".btn-hover-storage").css({'opacity' :'0'});
		});
	});

	$(".storage-check-btn").hide();
	$(".quantity-check-btn").hide();
	$(".btn-cancel-hide").hide();
	$('.adding-batch').hide();
	

	

	
	var checkValue = true;
	// $(".click-check-btn").addClass("modal-trigger").attr("modal-target" , "product-details");
	
	$(".click-check-btn").off("click").on("click", function() {				
		if ($(".click-check-btn").text() == "Enter Product Details" ) {
			$(".storage-check-hide").fadeOut(100);
			$(".quantity-check-hide").fadeOut(100);

			$(".storage-check-btn").fadeIn(100);
			$(".quantity-check-btn").fadeIn(100);

			$(".btn-cancel-hide").fadeIn(100);
			$(this).text("Complete Product Details");

			checkValue = false;


			
// 			$(".bread-show").text("Receiving No. 123456789 (Enter Product Details)");

			$(".edit-enter-product").hide();
			$(".edit-sku-item").hide();
			$(".edit-enter-sku").hide();
			$(".receiving-info-edit").hide();

			$(".receiving-truck-edit").hide();

			$(".btn-hover-storage").css({'display':'block'});

			// $(".hide-cancel").hide();

		} else {
// 			$(".complete-product-modal").addClass("showed");					
// 			$(".edit-enter-product").show();
		}
	});


	$(".complete-product-modal .close-me").off("click").on("click", function() {
		$(".complete-product-modal").removeClass("showed").css({'display':'none'});
	});

	$(".complete-product-modal .submit-btn").off("click").on("click", function() {
		$(".display-product-details").addClass("showed").css({'display':'block'});
		$(".complete-product-modal").removeClass("showed").css({'display':'none'});
	});
	

	$(".btn-cancel-hide").off("click").on("click", function(){
		
		checkvalue = true;

		$(this).fadeOut(100);

		$(".storage-check-btn").fadeOut(100);
		$(".quantity-check-btn").fadeOut(100);			

		$(".storage-check-hide").fadeIn(100);
		$(".quantity-check-hide").fadeIn(100);

		$(".click-check-btn").text("Enter Product Details");
// 		$(".bread-show").text("Receiving No. 1234567890");

		$(".edit-enter-product").show();
		$(".edit-enter-sku").show();

		$(".receiving-truck-edit").show();

		$(".receiving-info-edit").show();
	});


	// smooth scrolling 
	$(function() {
	  $('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 500);
	        return false;
	      }
	    }
	  });
	});

	$('.complete-receiving-record').attr('disabled',true);
	var controller = false;
	$('.marker1').on('click',function(){
		var text1 = $(this).text();
		if(text1 == 'Save Details'){
			$('.complete-receiving-record').attr('disabled',false);

			$('.marker1').html('Enter Product Details');
			$(".bread-show").html("Receiving No. 1234567890");
			$(".marker1-cancel").hide();
			$(".editing-cancel").hide();
			$(".to-editing-page").show();
			$(".marker1").show();
			$(".editing-button").show();

			$('.hide-distribution').hide();
			$('.display-distribution').show();

			$('.adding-batch').hide();
			$('.quantity-check-hide').hide();
			$('.quantity-check-btn').hide();
			$('.quantity-check-btn-with-value').show();

			$('.exclamation-mark').hide();
			$('.check-mark').show();

			$('.adding-batch').hide();


			$(this).hide();
			
			controller = true;
		} else if (text1 == 'Enter Product Details'){
			$('.complete-receiving-record').attr('disabled',true);
			$('.marker1').html('Save Details');
			$(".to-editing-page").hide();
			$(".editing-button").hide();
			$(".marker1-cancel").show();
			$('.display-distribution').hide();
			$('.hide-distribution').show();

			$('.storage-check-hide').hide();
			$('.storage-check-btn').show();
			

			$('.quantity-check-btn-with-value').hide();
			$('.quantity-check-hide').hide();
			$('.quantity-check-btn').show();
			$('.adding-batch').show();
			
			
		};
	});


	$('.marker1-cancel').on('click',function(){
		$(".marker1-cancel").hide();
		$(".editing-cancel").hide();
		$(".to-editing-page").show();

		$(".marker1").show();
		$(".editing-button").show();
		$('.marker1').html('Enter Product Details');

		$('.hide-distribution').hide();
		$('.display-distribution').show();

		$('.storage-check-btn').hide();
		$('.storage-check-hide').show();

		$('.adding-batch').hide();
		
		$('.quantity-check-btn').hide();
		$('.quantity-check-hide').show();

	});

	$('.editing-button').on('click',function(){
		var getText = $(this).text();
		if (getText == 'Edit'){
			$('.marker1').hide();
			$('.marker1-cancel').hide();

			$('.to-editing-page').hide();
			$('.editing-cancel').show();


			$('.display-label').hide();
			$('.hide-label').css({'display':'inline-block'});


			$('.display-distribution').hide();
			$('.hide-distribution').show();

			$('.hide-link').show();
			$('.btn-hover-storage').show();

			$('.adding-batch').show();
			

			$(this).text('Save Changes');
		} else if (getText == 'Save Changes'){
			if(controller == true){
				$('.marker1').hide();
			}else{
				$('.marker1').show();
			}
			$(this).text('Edit');
			$('.to-editing-page').show();
			$('.editing-cancel').hide();
			

			$('.hide-label').hide();
			$('.display-label').show();

			$('.display-distribution').show();
			$('.hide-distribution').hide();

			$('.hide-link').hide();	

			$('.btn-hover-storage').hide();

			$('.adding-batch').hide();

		}
	});

	$('.editing-cancel').on('click',function(){
		if(controller == true){
			$('.marker1').hide();
		}else{
			$('.marker1').show();
		}
		$(this).hide();
		$('.editing-button').text('Edit');
		$('.to-editing-page').show();
		$('.hide-label').hide();
		$('.display-label').show();
		$('.display-distribution').show();
		$('.hide-distribution').hide();
		$('.hide-link').hide();
		$('.adding-batch').hide();
		$('.btn-hover-storage').hide();
		$('.quantity-check-hide').hide();
		$('.quantity-check-btn').hide();
		$('.quantity-check-btn-with-value').show();
	});


	event_consignee();

	$(".hide-receiving").hide();
	$(".hide-method").hide();
	$(".hide-distribution").hide();
	$(".hide-link").hide();
	$(".hide-cancel").hide();
	$(".add-batch-hide").hide();
	$(".btn-hover-storage").hide();	
	$(".hide-qty").hide();

	$(".edit-enter-sku").off("click").on("click", function() {
		
		$(".hide-method").show(100);
		$(".hide-receiving").show(100);

		$(".display-method").hide(100);
		$(".display-receiving").hide(100);

		$(".hide-distribution").show(100);
		$(".display-distribution").hide(100);

		$(".hide-link").show(100);

		$(".hide-cancel").show(100);

		$(this).text("Save Changes");
		$(".bread-show").text("");
		$(".bread-show").text ("Receiving No. 1234567890 SKU (edit SKU# 1234567890) ")

		$(".add-batch-hide").show(100);
		$(".btn-hover-storage").show(100);

		$(".hide-qty").show(100);
		$(".show-qty").hide(100);

	
		$(".truck-btn-disabled").attr("disabled",true);

		$(".receiving-first-disable").attr("disabled", true);
		$(".receiving-second-disable").attr("disabled", true);
		$(".receiving-stock-disable").attr("disabled", true);
	
	});

	$(".hide-cancel").off("click").on("click", function() {
		
		$(".hide-method").hide(100);
		$(".hide-receiving").hide(100);

		$(".display-method").show(100);
		$(".display-receiving").show(100);

		$(".hide-distribution").hide(100);
		$(".display-distribution").show(100);

		$(".hide-link").hide(100);

		$(".hide-cancel").hide(100);

		$(".add-batch-hide").hide(100);
		$(".btn-hover-storage").hide(100);

		$(".edit-enter-sku").text("Edit");

		$(".show-qty").show(100);
		$(".hide-qty").hide(100);

		$(".truck-btn-disabled").attr("disabled",false);
		$(".receiving-first-disable").attr("disabled", false);
		$(".receiving-second-disable").attr("disabled", false);
		$(".receiving-stock-disable").attr("disabled", false);
	});


	$(".receiving-info-edit").off("click").on("click", function() {
		window.location.href=BASEURL+"receiving/consignee_edit_vessel";
	});
	$(".receiving-truck-edit").off("click").on("click", function() {
		window.location.href=BASEURL+"receiving/consignee_edit_truck";
	});

	// withdrawal stock goods 
	$(".withdraw-batch").hide();
	$(".btn-hover-storage").hide();
	$(".withdraw-edit").hide();

	$(".withdraw-edit-btn").off("click").on("click", function() {		
		$(".hide-cancel").show(100);	
		$(".withdraw-batch").show(100);			
		$(".btn-hover-storage").show(100);
		$(".withdraw-edit").show(100);
		$(".withdraw-display").hide(100);
		$(".click-check-btn").attr("disabled", true);
		$(".withdraw-edit-link").attr("disabled", true);
		$(this).text("Save Changes");

	});
	$(".hide-cancel").click(function() {		
		$(this).hide(100);
		$(".withdraw-batch").hide(100);
		$(".withdraw-edit-btn").text("Edit");	
		$(".btn-hover-storage").hide();	

		$(".click-check-btn").attr("disabled", false);
		$(".withdraw-edit-link").attr("disabled", false);
	});


	$(".withdraw-edit-link").off("click").on("click", function() {
		window.location.href="stock-withdrawal-edit.php";
	});


	// button change under transfer > stock-ongoing

	$(".transfer-hide-cancel").hide();
	$(".transfer-add-batch").hide();
	$(".ongoing-edit-hide").hide();
	$(".transfer-hide-piece").hide();

	// $(".transfer-ongoing-btn").off("click").on("click", function() {
	// 	$(this).text("Save Changes");
	// 	$(".transfer-hide-cancel").show(100);

	// 	$(".transfer-add-batch").show();
	// 	$(".ongoing-edit-hide").show()

	// 	$(".transfer-display-piece").hide(100)
	// 	$(".transfer-hide-piece").show(100);

	// 	$(".transfer-edit-information").attr("disabled", true);
	// 	$(".complete-transfer-btn").attr("disabled", true);
	// });

	$(".transfer-ongoing-btn").off("click").on("click", function() {

		$(this).text("Save Changes");
		$(".transfer-hide-cancel").show(100);

		$(".transfer-add-batch").show();
		$(".ongoing-edit-hide").show()

		$(".transfer-display-piece").hide(100)
		$(".transfer-hide-piece").show(100);


		$(".transfer-edit-information").attr("disabled", true);
		$(".complete-transfer-btn").attr("disabled", true);

		$('.qty-transfer').removeClass('width-20percent');
		$('.qty-transfer').addClass('width-15percent');

		$('.qty-to-transfer').removeClass('width-125px');
		$('.qty-to-transfer').addClass('width-100px');
	});

	$(".transfer-hide-cancel").off("click").on("click", function() {
		$(this).hide(100);
		$(".transfer-ongoing-btn").text("Edit");

		$(".transfer-add-batch").hide(100);
		$(".ongoing-edit-hide").hide(100)


		$(".transfer-display-piece").show(100)
		$(".transfer-hide-piece").hide(100);

		$(".transfer-edit-information").attr("disabled", false);
		$(".complete-transfer-btn").attr("disabled", false);
	});

	$(".transfer-edit-information").off("click").on("click", function() {
		window.location.href="consign_ongoing_edit";
	})


	$(".show-record-transfer").off("click").on("click", function() {
		window.open("stock-transfer-view.php", "windowName", "height=800,width=1200");
	});

});



function receiving_panel_product1() {

	// **********************************************************
	// TRUCK RECEIVING PAGE 
	// assign storage location 
	$(".storage-assign-popup").hide();
	$(".assign-storage-save").hide();
	$(".storage-assign-popup1").hide();
	$(".assign-storage-save1").hide();

	// assign storage location button 
	$(".assign-storage-popup").off("click").on("click", function() {
		$(".storage-assign-default").hide();
		$(".storage-assign-popup").fadeIn();
		$(".assign-storage-save").fadeIn();
		$(".assign-storage-popup").attr("disabled","disabled");
	});
	// assign storage save button 
	$(".assign-storage-save").off("click").on("click", function() {
		$(".change-status").text("On-going 1 of 2 Pending");
		$(".storage-assign-default").fadeIn();
		$(".storage-assign-popup").hide();
		$(".storage-assign-default tbody tr").hide();
		$(".storage-assign-default tbody").html(
			'<tr>'+
				'<td class="text-center">10/20/2015</td>'+
				'<td class="text-center">123456</td>' +
				'<td class="text-center">Warehouse 1</td>' +
				'<td class="text-center">Pallette 1 > Container 1</td>' +
				'<td class="text-center">101 KG</td>' +
			'</tr>'
		);
		$(".assign-storage-popup").text("Edit Assign Location");
		$(".assign-storage-save").hide();
		$(".assign-storage-popup").removeAttr("disabled");
		$(".stock-finalize-btn").removeAttr("disabled");
	});


	// verify product quantity button
	$(".verify-quantity-check").off("click").on("click", function() {
		$(".qc-del1").fadeIn();
		$(".qc-prod1").fadeIn();
		$(".qc-loss1").fadeIn();
		$(".qc-rem1").fadeIn();

		$(".qc-del").hide();
		$(".qc-prod").hide();
		$(".qc-loss").hide();
		$(".qc-rem").hide();		
		$(".verify-quantity-save").fadeIn();
		$(this).attr("disabled","disabled");
	});

	$(".qc-del1").hide();
	$(".qc-prod1").hide();
	$(".qc-loss1").hide();
	$(".qc-rem1").hide();

	$(".verify-quantity-save").hide();

	// verify product save button 
	$(".verify-quantity-save").off("click").on("click", function() {
		// $(".change-status").text("Complete");	
		$(".qc-del1").hide();
		$(".qc-prod1").hide();
		$(".qc-loss1").hide();
		$(".qc-rem1").hide();

		$(".qc-del").fadeIn().text("1010 KG");
		$(".qc-prod").fadeIn().text("Others");
		$(".qc-loss").fadeIn().text("10 KG (1% Loss)");
		$(".qc-rem").fadeIn().text("Group Spillage");
		$(".verify-quantity-check").removeAttr("disabled");
		$(".verify-quantity-check").text("Edit Product Quantity");
		$(this).hide();

		$(".finalize-btn").removeAttr("disabled","disabled");
	});

	// kevin code for team management
	$('.add-user-role.select div.option').click(function() {
		var selectedValue = $(this).text();
		switch (selectedValue) {
			case 'Admin' :						
				$(".add-content1 input, .add-content2 input, .add-content3 input").prop("disabled", false);
				$(".add-content1 label, .add-content2 label, .add-content3 label").css({"opacity":"1"});				
				break;
			case 'Super Visor' :
				$(" .add-content3 input").prop("disabled", true).prop("checked", false);
				$(" .add-content3 label").css({"opacity":"0.5"});
				break;
			case 'Operator' :
				$(".add-content2 input").prop("disabled", true).prop("checked", false);
				$(".add-content3 input").prop("disabled", true).prop("checked", false);
				$(".add-content2 label").css({"opacity" : "0.5"});
				$(".add-content3 label").css({"opacity" : "0.5"});
				break;
			default : 
				break;
		}
	});

	$('.edit-user-role.select div.option').click(function() {
		var selectedValue = $(this).text();
		switch (selectedValue) {
			case 'Admin' :						
				$(".edit-content1 input, .edit-content2 input, .edit-content3 input").prop("disabled", false);
				$(".edit-content1 label, .edit-content2 label, .edit-content3 label").css({"opacity":"1"});				
				break;
			case 'Super Visor' :
				$(" .edit-content3 input").prop("disabled", true).prop("checked", false);
				$(" .edit-content3 label").css({"opacity":"0.5"});
				break;
			case 'Operator' :
				$(".edit-content2 input").prop("disabled", true).prop("checked", false);
				$(".edit-content3 input").prop("disabled", true).prop("checked", false);
				$(".edit-content2 label").css({"opacity" : "0.5"});
				$(".edit-content3 label").css({"opacity" : "0.5"});
				break;
			default : 
				break;
		}
	});

	$(".dropdown-search").slideUp(0);
	$(".dropdown-btn").off("click").on("click", function() {
		if($(".dropdown-search").is(":visible")) {
			$(".dropdown-search").slideUp();
		} else {
			$(".dropdown-search").slideDown();
		}
	});





	


	// new product management grid table view 
	$(".table").hide();
	$(".icon-table").css({"color":"#333"});
	$(".icon-grid").css({"color":"gray"});
	$(".icon-table").off("click").on("click", function() {
		$(".grid.grid-container").slideDown();
		$(".table.tbl-container").slideUp();
		$(".icon-grid").css({"color":"gray"});
		$(this).css({"color":"#333"});
	});
	$(".icon-grid").off("click").on("click", function() {
		$(".grid.grid-container").slideUp();
		$(".table.tbl-container").slideDown();
		$(".icon-table").css({"color":"gray"});
		$(this).css({"color":"#333"});
	});

	/*Transfer Add Batch Modal*/
	$(".area").each(function() {
		
		var selectedArea = true;
		
		$(this).off("click").on("click", function() {
					
			if (selectedArea == true) {
				$(".area").removeClass("bggray-light");
				$(this).addClass("bggray-light");
				selectedArea = false;
			} else {

				$(this).removeClass("bggray-light");
				selectedArea = true;

			}
		})
	})

	$(".location-check").each(function() {
		
		var selectedLocation = true;
		
		$(this).off("click").on("click", function() {
			if (selectedLocation == true) {
				$(this).find(".location-address-check").removeClass("display-block");
				$(this).find(".location-address-check").addClass("display-block");
				selectedLocation = false;
			} else {

				$(this).find(".location-address-check").removeClass("display-block");
				selectedLocation = true;

			}

		})
	})


	/*Withdrawal page*/
	$(".second-content").hide();
	$(".pictures").css({"color":"#333"});
	$(".listing").css({"color":"gray"});
	$(".pictures").off("click").on("click", function() {
		$(".first-content").slideDown();
		$(".second-content").slideUp();
		$(".listing").css({"color":"gray"});
		$(this).css({"color":"#333"});
	});
	$(".listing").off("click").on("click", function() {
		$(".first-content").slideUp();
		$(".second-content").slideDown();
		$(".pictures").css({"color":"gray"});
		$(this).css({"color":"#333"});
	});


	$("#tab-product").hide();
	$("#tab-prod").hide();
	$("#tab-batch").hide();
	$("#tab-consignee").hide();
	$(".batch-list").addClass("border-blue border-top");

	if($("#tab-batch").is(":checked")){
		$(".consign-assignment").removeClass("padding-all-20");
		$(".product-act").removeClass("padding-all-20");
		$("#tab-con").css({"display":"none"}).removeClass("showed");
		$(".consign-assignment").addClass("padding-all-15");
		$(".product-act").addClass("padding-all-15");
	}

	$("#tab-batch").off("click").on("click", function() {
		if($('#tab-batch').is(':checked')) { 
			$("#tab-bat").css({"display":"block"}).addClass("showed");
			$("#tab-prod").css({"display":"none"}).removeClass("showed");
			$("#tab-con").css({"display":"none"}).removeClass("showed");

			$(".batch-list").addClass("border-blue border-top padding-all-20");
			$(".consign-assignment").removeClass("padding-all-20");
			$(".product-act").removeClass("padding-all-20");
			$(".consign-assignment").addClass("padding-all-15");
			$(".product-act").addClass("padding-all-15");

			$(".product-act").removeClass("border-blue border-top");
			$(".consign-assignment").removeClass("border-blue border-top");
				
		}
	});

	$("#tab-product").off("click").on("click", function() {
		if($('#tab-product').is(':checked')) { 
			$("#tab-prod").css({"display":"block"}).addClass("showed");
			$("#tab-bat").css({"display":"none"}).removeClass("showed");
			$("#tab-con").css({"display":"none"}).removeClass("showed");

			$(".product-act").addClass("border-blue border-top padding-all-20");
			$(".consign-assignment").removeClass("padding-all-20");
			$(".batch-list").removeClass("padding-all-20");
			$(".consign-assignment").addClass("padding-all-15");
			$(".batch-list").addClass("padding-all-15");


			$(".batch-list").removeClass("border-blue border-top");
			$(".consign-assignment").removeClass("border-blue border-top");

		}
	});

	$("#tab-consignee").off("click").on("click", function() {
		if($('#tab-consignee').is(':checked')) { 
			$("#tab-con").css({"display":"block"}).addClass("showed");
			$("#tab-bat").css({"display":"none"}).removeClass("showed");
			$("#tab-prod").css({"display":"none"}).removeClass("showed");

			$(".consign-assignment").addClass("border-blue border-top padding-all-20");
			$(".product-act").removeClass("padding-all-20");
			$(".batch-list").removeClass("padding-all-20");
			$(".product-act").addClass("padding-all-15");
			$(".batch-list").addClass("padding-all-15");

			$(".batch-list").removeClass("border-blue border-top");
			$(".product-act").removeClass("border-blue border-top");
			
		}
	});


	


	if ($('#increaseBy').is(':checked')){
		$('#text-decrease').attr('disabled',true);
	}

	$("#increaseBy").off("click").on("click", function() {
		if($('#increaseBy').is(':checked')) {
		
			$('#text-decrease').attr('disabled',true);
			$('#text-increase').attr('disabled',false);
			
			$('#text-decrease').val("");
			$('#display-adjust-batch-state').html("");
		}
	});
	$("#decreaseBy").off("click").on("click", function() {
		if($('#decreaseBy').is(':checked')) {
			
			$('#text-increase').attr('disabled',true);
			$('#text-decrease').attr('disabled',false);
			$('#text-increase').val("");

			$('#display-adjust-batch-state').html("");
			
		}
	});

	if ($('#bulk, #bag').not(':checked')) {
		$('.txt-bulk , .txt-bag').attr('disabled',true);
	}
	$("#bulk").off("click").on("click", function() {
		if($(this).is(':checked')) {
			$('.txt-bulk').attr('disabled',false);
		}else{
			$('.txt-bulk').attr('disabled',true);
		}
	});
	$("#bag").off("click").on("click", function() {
		if($(this).is(':checked')) {
			$('.txt-bag').attr('disabled',false);
		}else{
			$('.txt-bag').attr('disabled',true);
		}
	});

};

function receiving_panel_product2() {

	// **********************************************************
	// TRUCK RECEIVING PAGE 
	// assign storage location 
	$(".storage-assign-popup1-other").hide();
	$(".assign-storage-save1-other").hide();
	$(".storage-assign-popup1-other").hide();
	$(".assign-storage-save1-other").hide();

	// assign storage location button 
	$(".assign-storage-popup1-other").off("click").on("click", function() {
		$(".storage-assign-default1-other").hide();
		$(".storage-assign-popup1-other").fadeIn();
		$(".assign-storage-save1-other").fadeIn();
		$(".assign-storage-popup1-other").attr("disabled","disabled");
	});
	// assign storage save button 
	$(".assign-storage-save1-other").off("click").on("click", function() {
		$(".storage-assign-default1-other").fadeIn();
		$(".storage-assign-popup1-other").hide();
		$(".storage-assign-default1-other tbody tr").hide();
		$(".storage-assign-default1-other tbody").html(
			'<tr>'+
				'<td class="text-center">10/20/2015</td>'+
				'<td class="text-center">123456</td>' +
				'<td class="text-center">Warehouse 1</td>' +
				'<td class="text-center">Pallette 1 > Container 1</td>' +
				'<td class="text-center">101 KG</td>' +
			'</tr>'
		);
		$(".assign-storage-popup1-other").text("Edit Assign Location");
		$(".assign-storage-save1-other").hide();
		$(".assign-storage-popup1-other").removeAttr("disabled");
	});


	// verify product quantity button
	$(".verify-quantity-check-other").off("click").on("click", function() {
		$(".qc-del1-other").fadeIn();
		$(".qc-prod1-other").fadeIn();
		$(".qc-loss1-other").fadeIn();
		$(".qc-rem1-other").fadeIn();

		$(".qc-del-other").hide();
		$(".qc-prod-other").hide();
		$(".qc-loss-other").hide();
		$(".qc-rem-other").hide();		
		$(".verify-quantity-save-other").fadeIn();
		$(this).attr("disabled","disabled");
	});

	$(".qc-del1-other").hide();
	$(".qc-prod1-other").hide();
	$(".qc-loss1-other").hide();
	$(".qc-rem1-other").hide();

	$(".verify-quantity-save-other").hide();

	// verify product save button 
	$(".verify-quantity-save-other").off("click").on("click", function() {
		$(".qc-del1-other").hide();
		$(".qc-prod1-other").hide();
		$(".qc-loss1-other").hide();
		$(".qc-rem1-other").hide();

		$(".qc-del-other").fadeIn().text("1010 KG");
		$(".qc-prod-other").fadeIn().text("Others");
		$(".qc-loss-other").fadeIn().text("10 KG (1% Loss)");
		$(".qc-rem-other").fadeIn().text("Group Spillage");
		$(".verify-quantity-check-other").removeAttr("disabled");
		$(".verify-quantity-check-other").text("Edit Product Quantity");
		$(this).hide();



	});
}

function tool_tip () {
	//tooltip
	$("body").append("<div class='show-dialogue'></div>");
	if($("body .tool-tip").length > 0){
		$(".tool-tip").each(function(){
			var tthtml = $(this).attr("tt-html");

			$(this).mouseenter(function(){
				var ttx = $(this).offset().left;
				var tty = $(this).offset().top - ($(".show-dialogue").height() + 30) ;
				$(".show-dialogue").css({top:tty,left:ttx}).html(tthtml).stop().fadeIn(300);
			}).mouseleave(function(){
				$(".show-dialogue").stop().html(tthtml).fadeOut(300);
			});
		});
	}
}

function event_consignee () {


	var getMode;
	$(".mode-of-deliver.select div.option").click(function() {
		var selectedDelivery = $(this).text();
		switch (selectedDelivery) {
			case 'Vessel' :
				getMode = "vessel";
				break;
			case 'Truck' :
				getMode = "truck";
				break;
			default : 
				break;
		}
	});

	$(".mode-deliver-transfer").off("click").on("click", function() {
		if (getMode == "truck") {
			window.location.href="consignee_ongoing_truck"	
		} else {
			window.location.href="consignee_ongoing_vessel_1"	
		}
	})
}