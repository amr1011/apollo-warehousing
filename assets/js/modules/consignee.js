var CConsignee = (function(){

	var arrConsigneeAll = {},
		failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg";

	function _add(simagePath)
	{
		

		var oAddData = {},
			oDataContactPerson = [],
			oDataGetContactPerson = {},
			oTempDataContactPerson = cr8v_platform.localStorage.get_local_storage({name : "add_contactPerson_information"}),
			oGetConsigneeParentId = cr8v_platform.localStorage.get_local_storage({name:"setAddCurrentSelectedImage"});

			for(var x in oTempDataContactPerson)
			{
				oDataGetContactPerson = {
					"name":oTempDataContactPerson[x]["contactPersonName"],
					"position":oTempDataContactPerson[x]["contactPersonPosition"],
					"contact_number":oTempDataContactPerson[x]["contactPersonNumber"],
					"alternate_contact_number":oTempDataContactPerson[x]["contactPersonAlterNumber"],
					"email_address":oTempDataContactPerson[x]["contactPersonEmail"],
					"image":oTempDataContactPerson[x]["contactPersonImage"]
				};
				oDataContactPerson.push(oDataGetContactPerson);

			}
			

			oData = {
				name : $("#addConsigneeName").val(),
				alias : $("#addConsigneeAlias").val(),
				gb_billing_mode : $("#AddConsigneeBillingMode").val(),
				address : $("#addConsigneeBusinessAdd").val(),
				notes : $("#addConsigneeNotes").val(),
				image : oGetConsigneeParentId.value,
				contact_person : oDataContactPerson
			}

			// console.log(JSON.stringify(oData));


		var oOptions = {
			url : BASEURL+"consignee/add_consignee",
			type : "POST",
			data : {
				"consignee_information": JSON.stringify(oData)
				// contact_person : {}
			},
			success: function(oResponse)
			{
				console.log(oResponse);

				var options = {title : "Successfully Added", message : "New Consignee added!", speed : "slow", withShadow : true};
				$("body").feedback(options);
				// delete the temp information of contact person
				cr8v_platform.localStorage.delete_local_storage({name:"setCurrentImage"});
				cr8v_platform.localStorage.delete_local_storage({name:"setAddCurrentSelectedImage"});
				cr8v_platform.localStorage.delete_local_storage({name:"add_contactPerson_information"});

				window.location.href = BASEURL+"consignee/consignee_list";
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);


	}

	function _update()
	{
		var oAddData = {},
			oDataContactPerson = [],
			oDataGetContactPerson = {},
			oTempDataContactPerson = cr8v_platform.localStorage.get_local_storage({name : "edit_contactPerson_information"}),
			oGetConsigneeId = cr8v_platform.localStorage.get_local_storage({name:"selectedConsignEdit"}),
			oGetConsigneeParentId = "",
			oGetSelectedIdUpdated = CConsignee.arrConsigneeAll;
			// console.log(oTempDataContactPerson);
// 			console.log(oGetConsigneeId);
// 			console.log(CConsignee.arrConsigneeAll);


			for(var i in oGetSelectedIdUpdated){
				if(oGetSelectedIdUpdated[i]["id"] == oGetConsigneeId.value)
				{
					console.log(oGetSelectedIdUpdated[i]["image"]);
					if(oGetSelectedIdUpdated[i]["image"] != ""){
						oGetConsigneeParentId = oGetSelectedIdUpdated[i]["image"];
					}
					else{
						var newImage = cr8v_platform.localStorage.get_local_storage({name:"setEditCurrentSelectedImage"});
						oGetConsigneeParentId = newImage.value;
					}



				}
			}


			for(var x in oTempDataContactPerson)
			{
				oDataGetContactPerson = {
					"name":oTempDataContactPerson[x]["contactPersonName"],
					"position":oTempDataContactPerson[x]["contactPersonPosition"],
					"contact_number":oTempDataContactPerson[x]["contactPersonNumber"],
					"alternate_contact_number":oTempDataContactPerson[x]["contactPersonAlterNumber"],
					"email_address":oTempDataContactPerson[x]["contactPersonEmail"],
					"consignee_id":oGetConsigneeId.value,
					"image":oTempDataContactPerson[x]["contactPersonImage"]
				};
				oDataContactPerson.push(oDataGetContactPerson);

			}
			

			oData = {
				name : $("#editConsigneeName").val(),
				alias : $("#editConsigneeAlias").val(),
				gb_billing_mode : $("#editConsigneeBillingMode").val(),
				address : $("#editConsigneeAddress").val(),
				notes : $("#editConsigneeNotes").val(),
				image : oGetConsigneeParentId,
				contact_person : oDataContactPerson
			}
			// console.log(oGetConsigneeId);
			// console.log(JSON.stringify(oData));


		var oOptions = {
			url : BASEURL+"consignee/update_consignee_information",
			type : "POST",
			data : {
				"id":oGetConsigneeId.value,
				"data": JSON.stringify(oData)
			},
			success: function(oResponse)
			{
				console.log(oResponse);

				var options = {title : "Successfully Updated", message : "Consignee Updated!", speed : "slow", withShadow : true};
				$("body").feedback(options);
				// delete the temp information of contact person
				cr8v_platform.localStorage.delete_local_storage({name:"setEditCurrentImage"});
				// 	cr8v_platform.localStorage.delete_local_storage({name:"selectedConsignEdit"});
				cr8v_platform.localStorage.delete_local_storage({name:"edit_consignee_image"});
				cr8v_platform.localStorage.delete_local_storage({name:"edit_consignee_contact_person_image"});
				cr8v_platform.localStorage.delete_local_storage({name:"edit_contactPerson_information"});
				cr8v_platform.localStorage.delete_local_storage({name:"setEditCurrentSelectedImage"});

				window.location.href = BASEURL+"consignee/consignee_list";
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _delete()
	{
		console.log(CConsignee.arrConsigneeSelected);

		var oOptions = {
			url : BASEURL+"consignee/delete",
			type : "POST",
			data : {
				"data":JSON.stringify(CConsignee.arrConsigneeSelected)
			},
			success: function(oResponse)
			{
				console.log(oResponse);
				window.location.href = BASEURL+"consignee";
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _get()
	{
			
	}

	function getSideUpdate(oData)
	{
		
		// ========================For The SideView of Update Consigee ==================================//

			
			var oGetSelectedPersonAllUpdate = oData,
				iSelectedPerson = cr8v_platform.localStorage.get_local_storage({name:"selectedConsignEdit"}),
				uiIsActive = "",
				sDefaultImg = "",
				iCount = 0,
				uiSideViewConsigneeAllUpdate = $("#sideViewConsigneeAllEdit");

				console.log(JSON.stringify(iSelectedPerson));
			
			for(var x in oGetSelectedPersonAllUpdate)
			{

				if(iSelectedPerson.value != oGetSelectedPersonAllUpdate[x]["id"])
				{
					uiIsActive = '<div class="getTriggerActive item-list select_view item-list-no-hover" style="cursor: inherit;">';
				}
				else{
					 uiIsActive = '<div class="getTriggerActive item-list select_view active" style="cursor: inherit;">';
				}

				(oGetSelectedPersonAllUpdate[x]["image"] != "") ? sDefaultImg = oGetSelectedPersonAllUpdate[x]["image"] : sDefaultImg = DEFAULT_PROD_IMG;

				var sHtml = uiIsActive+
								'<div class="display-inline-mid image">'+
									'<img src="'+sDefaultImg+'">'+
								'</div>'+
								'<div class="display-inline-mid text">'+
									'<p>'+oGetSelectedPersonAllUpdate[x]["name"]+'</p>'+
								'</div>'+
							'</div>';
						
						uiSideViewConsigneeAllUpdate.append(sHtml);

				iCount++;
				// console.log(iCount);
						
			}
			//============================End of The SideView of Update Consigee ==================================//
	}



	function bindEvents (sAction, ui)
	{
		if(sCurrentPath.indexOf("consignee/add_consignee_view") > -1) {

			var uiAddConsigneeSubmit = $("#AddConsigneeSubmit"),
				uiAddConsigneeContactPersonSubmit = $("#addConsigneeContactPersonSubmit"),
				oTempSaveConsigneeContactPerson = [];

				uiAdContactPersonModal = $('[modal-id="add-contact-person"]').detach();

				$(".main-section").append(uiAdContactPersonModal);

			var cosigneeUpload = $("#upload_consignee_img").cr8vUpload({
				url: BASEURL+"consignee/upload",
				accept: "jpg,png",
				autoUpload: true,
				onSelected : function(bValid)
				{
					// if(bValid)
					// {
					// 	cr8v_platform.localStorage.set_local_storage({
					// 		name : "add_consignee_image",
					// 		data : {value:"true"}
					// 	});
					// }else{
					// 	cr8v_platform.localStorage,set_local_storage({
					// 		name : "add_consignee_image",
					// 		data : {value:"false"}
					// 	});
					// }
				},
				success: function(data, textStatus, jqXHR)
				{
					// _add(data.data.location);
					var	imagePath = data.data.location;
					cr8v_platform.localStorage.set_local_storage({
						name:"setAddCurrentSelectedImage",
						data: {value:imagePath}
					});
					var ogetit = cr8v_platform.localStorage.get_local_storage({name:"setAddCurrentSelectedImage"});
					console.log(ogetit);

				},
				error: function(jqXHR, textStatus, errorThrown)
				{

				}
			});


			//This is for validation 
			$("#AddConsigneeForm").cr8vformvalidation({
				'preventDefault' : true,
			    'data_validation' : 'datavalid',
			    'input_label' : 'labelinput',
			    'onValidationError': function(arrMessages) {
			        // alert(JSON.stringify(arrMessages)); //shows the errors
			       
			        var options = {title : "Add Consignee Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger",};
					$("body").feedback(options);
		        },
			    'onValidationSuccess': function() {
			     

					var checkImage = cr8v_platform.localStorage.get_local_storage({name:"setAddCurrentSelectedImage"});

						if(checkImage === null)
						{
							cr8v_platform.localStorage.set_local_storage({
								name:"setAddCurrentSelectedImage",
								data: {value:""}
							});
						}

						_add();

		        }
			});

			uiAddConsigneeSubmit.off("click.addConsignee").on("click.addConsignee", function(){
						// $("#AddConsigneeForm").submit()
						// alert("fsafsd");
						$("#AddConsigneeForm").trigger("submit");

			});

			if(sAction === 'AddContactPerson')
			{	
				var imagePath = "";
				cr8v_platform.localStorage.set_local_storage({
					name:"setCurrentImage",
					data: {value:imagePath}
				});
				var uiAddNewContactImage = $("#add_new_contact_person_img").cr8vUpload({
					url: BASEURL+"consignee/upload_contact_person_image",
					accept: "jpg,png",
					autoUpload: true,
					onSelected : function(bValid)
					{
						if(bValid)
						{
							cr8v_platform.localStorage.set_local_storage({
								name : "add_contactPerson_image",
								data : {value:"true"}
							});
						}else{
							cr8v_platform.localStorage.set_local_storage({
								name : "add_contactPerson_image",
								data : {value:"false"}
							});
						}
						// var bg =$(this).css('background-image');
						// 	bg = bg.replace('url(','').replace(')','');
						// 	console.log(bg);

					},
					success: function(data, textStatus, jqXHR)
					{
						imagePath = data.data.location;
						cr8v_platform.localStorage.set_local_storage({
							name:"setCurrentImage",
							data: {value:imagePath}
						});

					},
					error: function(jqXHR, textStatus, errorThrown)
					{

					}
				});

				
				// validate consignee contact person
				$("#AddConsigneeContactPersonForm").cr8vformvalidation({
					'preventDefault' : true,
				    'data_validation' : 'datavalid',
				    'input_label' : 'labelinput',
				    'onValidationError': function(arrMessages) {
				        // alert(JSON.stringify(arrMessages)); //shows the errors
				        var options = {title : "Add Contact Person Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger",};
						$("body").feedback(options);
			        },
				    'onValidationSuccess': function() {
				        // alert("Success"); //function if you have something to do upon submission

				        var oHasImage = cr8v_platform.localStorage.get_local_storage({name : "add_contactPerson_image"});
						if(oHasImage.value == "true"){
							uiAddNewContactImage.startUpload();
						}else{}

							var oTempData = cr8v_platform.localStorage.get_local_storage({name : "add_contactPerson_information"}),
								oCurrentImage = cr8v_platform.localStorage.get_local_storage({name : "setCurrentImage"})
								icount = 1;

								console.log(oCurrentImage.value);


							if(oTempData === null)
        					{
        						var oDataNew = {
        							contactPersonId : 1,
									contactPersonName : $("#addConsigneeContactPersonName").val(),
									contactPersonPosition : $("#addConsigneeContactPersonPosition").val(),
									contactPersonNumber : $("#addConsigneeContactNumber").val(),
									contactPersonAlterNumber: $("#addConsigneeContactAlterNumber").val(),
									contactPersonEmail : $("#addConsigneeContactPersonEmail").val(),
									contactPersonImage : (oCurrentImage.value !== null) ? oCurrentImage.value : ""

								};
								oTempSaveConsigneeContactPerson.push(oDataNew);

								cr8v_platform.localStorage.delete_local_storage({name:"setCurrentImage"})

        					}else{

        						for(x in oTempData)
        						{
        							var oDataNew = {
        								contactPersonId : icount,
        								contactPersonName : oTempData[x]["contactPersonName"],
										contactPersonPosition :oTempData[x]["contactPersonPosition"],
										contactPersonNumber : oTempData[x]["contactPersonNumber"],
										contactPersonAlterNumber: oTempData[x]["contactPersonAlterNumber"],
										contactPersonEmail : oTempData[x]["contactPersonEmail"],
										contactPersonImage : oTempData[x]["contactPersonImage"],
        							}
        							oTempSaveConsigneeContactPerson.push(oDataNew);
        							icount++;

        						}

        							
        							var oDataNew2 = {
        								contactPersonId : icount,
										contactPersonName : $("#addConsigneeContactPersonName").val(),
										contactPersonPosition : $("#addConsigneeContactPersonPosition").val(),
										contactPersonNumber : $("#addConsigneeContactNumber").val(),
										contactPersonAlterNumber: $("#addConsigneeContactAlterNumber").val(),
										contactPersonEmail : $("#addConsigneeContactPersonEmail").val(),
										contactPersonImage : (oCurrentImage.value !== null) ? oCurrentImage.value : ""

									};
									oTempSaveConsigneeContactPerson.push(oDataNew2);

									cr8v_platform.localStorage.delete_local_storage({name:"setCurrentImage"})

        					}	

							cr8v_platform.localStorage.set_local_storage({
								name : "add_contactPerson_information",
								data : oTempSaveConsigneeContactPerson
							});

							var oTempDataCheck = cr8v_platform.localStorage.get_local_storage({name : "add_contactPerson_information"});

							if(oTempDataCheck === null)
							{
								
							}
							else
							{
								var uiDisplayContactPersons = $("#displayContactPersons"),
									uinoContactPersonHide = $("#noContactPersonHide");

								uiDisplayContactPersons.removeClass("modal-person-hide");
								uiDisplayContactPersons.css({"display": "block"});
								uinoContactPersonHide.css({"display": "none"});

								CConsignee.renderElement("displayConsigneeContactPerson", oTempDataCheck);

								$(".modal-close.close-me").trigger("click");

								var options = {
								  form : "box",
								  autoShow : true,
								  type : "success",
								  title  : "Add Contact Person",
								  message : "Successfully Added",
								  speed : "slow",
								  withShadow : true
								}
								
								$("body").feedback(options);


							}

			        }
				});

				uiAddConsigneeContactPersonSubmit.off("click.AddConsigneeContactPersonSubmit").on("click.AddConsigneeContactPersonSubmit", function(){
					$("#AddConsigneeContactPersonForm").submit();
				});

			}

			else if(sAction === "editConsigneeContactInfo")
			{
				var uiBtn = ui.find('[modal-target="edit-contact-person"]');

				uiBtn.off('click.editConsignContactInfo').on('click.editConsignContactInfo', function(){

					$("body").css({overflow:'hidden'});
					var tm = $(this).attr("modal-target");

					$("div[modal-id~='"+tm+"']").addClass("showed");

					$("div[modal-id~='"+tm+"'] .close-me").on("click",function(){
						$("body").css({'overflow-y':'initial'});
						$("div[modal-id~='"+tm+"']").removeClass("showed");
					});

					var uiParent = $(this).closest(".text-left.contact-person"),
						sConsigneePersonId = uiParent.data("contactPersonId"),
						oTempDataCheck = cr8v_platform.localStorage.get_local_storage({name : "add_contactPerson_information"});

						for(var x in oTempDataCheck)
						{
							if(sConsigneePersonId == oTempDataCheck[x]["contactPersonId"])
							{
								// console.log(oTempDataCheck[x]["contactPersonId"]);
								var oCurrentPersonInfo = {
									"currentPersonId":oTempDataCheck[x]["contactPersonId"],
									"currentPersonName":oTempDataCheck[x]["contactPersonName"],
									"currentPersonPosition":oTempDataCheck[x]["contactPersonPosition"],
									"currentPersonNumber":oTempDataCheck[x]["contactPersonNumber"],
									"currentPersonAlterNumber":oTempDataCheck[x]["contactPersonAlterNumber"],
									"currentPersonEmail":oTempDataCheck[x]["contactPersonEmail"]
								};
								CConsignee.renderElement("displayEditConsigneeContactPerson", oCurrentPersonInfo);
								console.log("asdsa");

							}
						}


				});
			}
			else if(sAction === 'editConsigneeContactPersonSelected')
			{
				var uiParentContainer = ui.closest(".editContactPersonModalFooterUi"),
					sSelectedContactPersonId = uiParentContainer.data("contactPersonId"),
					uiDeleteSelectedContactPersonBtn = $(".deleteSelectedContactPersonBtn"),
					oTempDataContactPerson = cr8v_platform.localStorage.get_local_storage({name : "add_contactPerson_information"}),
					uitriggerId = $("#triggerId"+sSelectedContactPersonId)
					uiBtnSaveUpdateContactPerson = $(".saveUpdateSelectedContactPerson"),
					uiCancelUpdatingseleted = $(".cancelUpdatingseleted"),
					uiconfirmDelete = $("#confirmDelete");
				
					// remove selected Contact Person
					uiDeleteSelectedContactPersonBtn.off("click.contactSelected").on("click.contactSelected", function(){


						$("body").css({overflow:'hidden'});
						

						$("#trashModal").addClass("showed");

						$("#trashModal .close-me").on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$("#trashModal").removeClass("showed");
						});

						uiconfirmDelete.off('click.cofirmDelete').on('click.cofirmDelete', function(){
							var oGetData = oTempDataContactPerson


							$.each(oGetData, function(i, el){
							    if (this.contactPersonId == sSelectedContactPersonId){
							        oGetData.splice(i, 1);
							    }
							});

							cr8v_platform.localStorage.set_local_storage({
								name : "add_contactPerson_information",
								data : oGetData
							});

							// uitriggerId.remove();

							CConsignee.renderElement("displayConsigneeContactPerson", oGetData);
							$(".modal-close.close-me").trigger("click");

							var options = {
							  form : "box",
							  autoShow : true,
							  type : "success",
							  title  : "Delete Contact Person",
							  message : "Successfully Deleted",
							  speed : "slow",
							  withShadow : true
							}
							
							$("body").feedback(options);

						})




						
					});

					// update Selected Contact Person
					uiBtnSaveUpdateContactPerson.off('click.updateConsigneeContactPerson').on('click.updateConsigneeContactPerson', function(){
						var uiEditConsigneeContactPersonName = $("#editConsigneeContactPersonName"),
							uiEditConsigneeContactPersonPosition = $("#editConsigneeContactPersonPosition"),
							uiEditConsigneeContactPersonNumber = $("#editConsigneeContactPersonNumber"),
							uiEditConsigneeContactPersonAlterNumber = $("#editConsigneeContactPersonAlterNumber"),
							uiEditConsigneeContactPersonEmail = $("#editConsigneeContactPersonEmail"),
							uiEditConsignessContactPersonModalFooter = $("#editConsignessContactPersonModalFooter"),
							oTempDataContactPersonSelected = cr8v_platform.localStorage.get_local_storage({name : "add_contactPerson_information"}),
							oTempSelectedImg = cr8v_platform.localStorage.get_local_storage({name : "setSelectedContactPersonImage"});


							// console.log(oTempSelectedImg);
							for(var x in oTempDataContactPersonSelected)
							{
								if(sSelectedContactPersonId == oTempDataContactPersonSelected[x]["contactPersonId"])
								{
									
									oTempDataContactPersonSelected[x]["contactPersonName"] = $("#editConsigneeContactPersonName").val();
									oTempDataContactPersonSelected[x]["contactPersonPosition"] = $("#editConsigneeContactPersonPosition").val();
									oTempDataContactPersonSelected[x]["contactPersonNumber"] = $("#editConsigneeContactPersonNumber").val();
									oTempDataContactPersonSelected[x]["contactPersonAlterNumber"] = $("#editConsigneeContactPersonAlterNumber").val();
									oTempDataContactPersonSelected[x]["contactPersonEmail"] = $("#editConsigneeContactPersonEmail").val(),
									uidisplayContactPersonsContainer = $("#displayContactPersons");

									if(oTempSelectedImg.value != ""){
										oTempDataContactPersonSelected[x]["contactPersonImage"] = oTempSelectedImg.value;
									}


								}
							}
							cr8v_platform.localStorage.set_local_storage({
								name : "add_contactPerson_information",
								data : oTempDataContactPersonSelected
							});

							$(".modal-close.close-me").trigger("click");

							CConsignee.renderElement("displayConsigneeContactPerson", oTempDataContactPersonSelected);

							var options = {
							  form : "box",
							  autoShow : true,
							  type : "success",
							  title  : "Updated Contact Person",
							  message : "Successfully Updated",
							  speed : "slow",
							  withShadow : true
							}
							
							$("body").feedback(options);
					});

					uiCancelUpdatingseleted.off("click.cancelUpdating").on("click.cancelUpdating", function(){
						$(".modal-close.close-me").trigger("click");
					});


			}
			
		}else if(sCurrentPath.indexOf("consignee/consignee_list") > -1){
			
			if(sAction === 'ViewConsigneeInfo')
			{

				var uiParent = ui.closest(".item-list.select_view"),
					iConsigneeId = uiParent.data("consigneeId"),
					uiGetTriggerActive = $(".getTriggerActive")
					uiTriggerConsignee = $("#triggerConsignId"+iConsigneeId),
					uiSideViewConsigneeAll = $("#sideViewConsigneeAll"),
					oDataTrigger = CConsignee.arrConsigneeAll,
					btngetIdss = $("#generateConsignIdBtn");

				
					uiTriggerConsignee.off("click.triggerConsign").on("click.triggerConsign", function(){
						// uiGetTriggerActive.removeClass("active");
						

						$('#sideViewConsigneeAll > .getTriggerActive').removeClass("active");
						$(this).addClass("active");

						cr8v_platform.localStorage.set_local_storage({
							name : "selectedConsignClick",
							data : {value:iConsigneeId}
						});


						btngetIdss.off("click.generateId").on("click.generateId", function(){

							cr8v_platform.localStorage.set_local_storage({
								name : "selectedConsignEdit",
								data : {value:iConsigneeId}
							});

							window.location.href = BASEURL+"consignee/edit_consignee";
						});



						CConsignee.renderElement('viewSelectedConsignee', oDataTrigger);
						
											
					});
	
			}

		}
		else if(sCurrentPath.indexOf("consignee/edit_consignee") > -1)
		{

			var uiaddEditConsigneeContactPersonSubmit = $("#addEditConsigneeContactPersonSubmit"),
				uiUpdateConsigneeSubmit = $("#UpdateConsigneeSubmit"),
				uiUpdateConsigneeDeleteBtn = $("#updateConsigneeDeleteContainer"),
				oTempSaveConsigneeContactPerson = [],
				uiConfirmDeleteUpdate = $("#confirmDelete");

				
				uiAdContactPersonModal = $('[modal-id="add-contact-person"]').detach();

				$(".main-section").append(uiAdContactPersonModal);
				

				//This is for validation 
				$("#updateConsigneeForm").cr8vformvalidation({
					'preventDefault' : true,
				    'data_validation' : 'datavalid',
				    'input_label' : 'labelinput',
				    'onValidationError': function(arrMessages) {
				        // alert(JSON.stringify(arrMessages)); //shows the errors
				        var options = {title : "Edit Consignee Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger",};
						$("body").feedback(options);
			        },
				    'onValidationSuccess': function() {


						var checkImage = cr8v_platform.localStorage.get_local_storage({name:"setEditCurrentSelectedImage"});

						if(checkImage === null)
						{
							cr8v_platform.localStorage.set_local_storage({
								name:"setEditCurrentSelectedImage",
								data: {value:""}
							});
						}


						_update();



			        }
				});

				uiUpdateConsigneeSubmit.off("click.updateConsignee").on("click.updateConsignee", function(){
		
					$("#updateConsigneeForm").trigger("submit");

				});

				uiUpdateConsigneeDeleteBtn.off("click.updateConsigneeDelete").on("click.updateConsigneeDelete", function(){
						
						$("body").css({overflow:'hidden'});
						
						$("#trashModal").addClass("showed");

						$("#trashModal .close-me").on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$("#trashModal").removeClass("showed");
						});

						uiConfirmDeleteUpdate.off("click.confirmIt").on("click.confirmIt", function(){
							$(".modal-close.close-me").trigger("click");

							var options = {
							  form : "box",
							  autoShow : true,
							  type : "success",
							  title  : "Delete Contact Person",
							  message : "Successfully Deleted",
							  speed : "slow",
							  withShadow : true
							}
							
							$("body").feedback(options);
							_delete();
						});
				})




			if(sAction === 'EditAddContactPerson')
			{

				var imagePath = "";
				cr8v_platform.localStorage.set_local_storage({
					name:"setEditCurrentImage",
					data: {value:imagePath}
				});
				

				var cosigneeUpload = $("#upload_consignee_img").cr8vUpload({
					url: BASEURL+"consignee/upload",
					accept: "jpg,png",
					autoUpload: true,
					onSelected : function(bValid)
					{
						// if(bValid)
						// {
						// 	cr8v_platform.localStorage.set_local_storage({
						// 		name : "add_consignee_image",
						// 		data : {value:"true"}
						// 	});
						// }else{
						// 	cr8v_platform.localStorage,set_local_storage({
						// 		name : "add_consignee_image",
						// 		data : {value:"false"}
						// 	});
						// }
					},
					success: function(data, textStatus, jqXHR)
					{
						// _add(data.data.location);
						var	imagePath = data.data.location;
						cr8v_platform.localStorage.set_local_storage({
							name:"setEditCurrentImage",
							data: {value:imagePath}
						});
						var ogetit = cr8v_platform.localStorage.get_local_storage({name:"setEditCurrentImage"});
						console.log(ogetit);

					},
					error: function(jqXHR, textStatus, errorThrown)
					{

					}
				});
				


				//This is for validation 
				$("#editConsigneeContactPersonForm").cr8vformvalidation({
					'preventDefault' : true,
				    'data_validation' : 'datavalid',
				    'input_label' : 'labelinput',
				    'onValidationError': function(arrMessages) {
				        // alert(JSON.stringify(arrMessages)); //shows the errors
				        var options = {title : "Add Consignee Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger",};
						$("body").feedback(options);
			        },
				    'onValidationSuccess': function() {
				        // alert("Success"); //function if you have something to do upon submission
				        // $("body").feedback({message : "is valid."});
				        // console.log("ok");
				        var oHasImage = cr8v_platform.localStorage.get_local_storage({name : "edit_consignee_contact_person_image"});
						// if(oHasImage.value == "true"){
						// 	cosigneeUploadEdit.startUpload();
						// }else{
							
							var oTempData = cr8v_platform.localStorage.get_local_storage({name : "edit_contactPerson_information"}),
								oCurrentImage = cr8v_platform.localStorage.get_local_storage({name : "setEditCurrentImage"}),
								icount = 1;


								if(oTempData === null)
	        					{
	        						var oDataNew = {
	        							contactPersonId : 1,
										contactPersonName : $("#addConsigneeContactPersonName").val(),
										contactPersonPosition : $("#addConsigneeContactPersonPosition").val(),
										contactPersonNumber : $("#addConsigneeContactNumber").val(),
										contactPersonAlterNumber: $("#addConsigneeContactAlterNumber").val(),
										contactPersonEmail : $("#addConsigneeContactPersonEmail").val(),
										contactPersonImage : (oCurrentImage.value !== null) ? oCurrentImage.value : ""

									};
									oTempSaveConsigneeContactPerson.push(oDataNew);

									cr8v_platform.localStorage.delete_local_storage({name:"setEditCurrentImage"})

	        					}else{

	        						for(x in oTempData)
	        						{
	        							var oDataNew = {
	        								contactPersonId : icount,
	        								contactPersonName : oTempData[x]["contactPersonName"],
											contactPersonPosition :oTempData[x]["contactPersonPosition"],
											contactPersonNumber : oTempData[x]["contactPersonNumber"],
											contactPersonAlterNumber: oTempData[x]["contactPersonAlterNumber"],
											contactPersonEmail : oTempData[x]["contactPersonEmail"],
											contactPersonImage : oTempData[x]["contactPersonImage"],
	        							}
	        							oTempSaveConsigneeContactPerson.push(oDataNew);
	        							icount++;

	        						}

	        							
	        							var oDataNew2 = {
	        								contactPersonId : icount,
											contactPersonName : $("#addConsigneeContactPersonName").val(),
											contactPersonPosition : $("#addConsigneeContactPersonPosition").val(),
											contactPersonNumber : $("#addConsigneeContactNumber").val(),
											contactPersonAlterNumber: $("#addConsigneeContactAlterNumber").val(),
											contactPersonEmail : $("#addConsigneeContactPersonEmail").val(),
											contactPersonImage : (oCurrentImage.value !== null) ? oCurrentImage.value : ""

										};
										oTempSaveConsigneeContactPerson.push(oDataNew2);

										cr8v_platform.localStorage.delete_local_storage({name:"setEditCurrentImage"});
										// console.log(oTempSaveConsigneeContactPerson);

	        					}	
	        					// console.log("dsafa");


								cr8v_platform.localStorage.set_local_storage({
									name : "edit_contactPerson_information",
									data : oTempSaveConsigneeContactPerson
								});

							var oTempDataCheck = cr8v_platform.localStorage.get_local_storage({name : "edit_contactPerson_information"});

							if(oTempDataCheck === null)
							{
								
							}
							else
							{
								

								CConsignee.renderElement("displayEditConsigneeContactPerson", oTempDataCheck);

								$(".modal-close.close-me").trigger("click");

								var options = {
								  form : "box",
								  autoShow : true,
								  type : "success",
								  title  : "Add Contact Person",
								  message : "Successfully Added",
								  speed : "slow",
								  withShadow : true
								}
								
								$("body").feedback(options);



							}



						//}
			        }
				});

				uiaddEditConsigneeContactPersonSubmit.off("click.editConsignee").on("click.editConsignee", function(){

					// console.log("clicked");
					$("#editConsigneeContactPersonForm").trigger("submit");

				});
			}
			else if(sAction === 'editConsigneeContactPersonInfo')
			{


				var uiBtn = ui.find('[modal-target="edit-contact-person"]');

				uiBtn.off('click.editConsignContactPersonInfo').on('click.editConsignContactPersonInfo', function(){

					$("body").css({overflow:'hidden'});
					var tm = $(this).attr("modal-target");

					$("div[modal-id~='"+tm+"']").addClass("showed");

					$("div[modal-id~='"+tm+"'] .close-me").on("click",function(){
						$("body").css({'overflow-y':'initial'});
						$("div[modal-id~='"+tm+"']").removeClass("showed");
					});

					var uiParent = $(this).closest(".text-left.contact-person"),
						sConsigneePersonId = uiParent.data("contactPersonId"),
						oTempDataCheck = cr8v_platform.localStorage.get_local_storage({name : "edit_contactPerson_information"});

						for(var x in oTempDataCheck)
						{

							if(sConsigneePersonId == oTempDataCheck[x]["contactPersonId"])
							{
								// console.log(oTempDataCheck[x]["contactPersonId"]);
								// 	console.log(JSON.stringify(oTempDataCheck));
								var oCurrentPersonInfo = {
									"currentPersonId":oTempDataCheck[x]["contactPersonId"],
									"currentPersonName":oTempDataCheck[x]["contactPersonName"],
									"currentPersonPosition":oTempDataCheck[x]["contactPersonPosition"],
									"currentPersonNumber":oTempDataCheck[x]["contactPersonNumber"],
									"currentPersonAlterNumber":oTempDataCheck[x]["contactPersonAlterNumber"],
									"currentPersonEmail":oTempDataCheck[x]["contactPersonEmail"],
									"currentPersonImage":oTempDataCheck[x]["contactPersonImage"]
								};
								CConsignee.renderElement("displayUpdateConsigneeContactPerson", oCurrentPersonInfo);
								// console.log(JSON.stringify(oCurrentPersonInfo));

							}
						}


				});
			}
			else if(sAction === 'updateConsigneeContactPersonSelected')
			{
				uiEditContactPersonModal = $('[modal-id="edit-contact-person"]').detach();

				$(".main-section").append(uiEditContactPersonModal);


				var uiParentContainer = ui.closest(".editContactPersonModalFooterUi"),
					sSelectedContactPersonId = uiParentContainer.data("contactPersonId"),
					uiDeleteSelectedContactPersonBtn = $(".deleteSelectedContactPersonBtn"),
					oTempDataContactPerson = cr8v_platform.localStorage.get_local_storage({name : "edit_contactPerson_information"}),
					uitriggerId = $("#triggerId"+sSelectedContactPersonId),
					uiBtnSaveUpdateContactPerson = $(".saveUpdateSelectedContactPerson"),
					uiCancelUpdatingseleted = $(".cancelUpdatingseleted"),
					uiconfirmDelete = $("#confirmDelete");
				
					// remove selected Contact Person
					uiDeleteSelectedContactPersonBtn.off("click.contactSelected").on("click.contactSelected", function(){


						$("body").css({overflow:'hidden'});
						

						$("#trashModal").addClass("showed");

						$("#trashModal .close-me").on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$("#trashModal").removeClass("showed");
						});

						uiconfirmDelete.off('click.cofirmDelete').on('click.cofirmDelete', function(){
							var oGetData = oTempDataContactPerson


							$.each(oGetData, function(i, el){
							    if (this.contactPersonId == sSelectedContactPersonId){
							        oGetData.splice(i, 1);
							    }
							});

							cr8v_platform.localStorage.set_local_storage({
								name : "edit_contactPerson_information",
								data : oGetData
							});
							// console.log(uitriggerId);
							// uitriggerId.remove();
							CConsignee.renderElement("displayEditConsigneeContactPerson", oGetData);
							$(".modal-close.close-me").trigger("click");

							var options = {
							  form : "box",
							  autoShow : true,
							  type : "success",
							  title  : "Delete Contact Person",
							  message : "Successfully Deleted",
							  speed : "slow",
							  withShadow : true
							}
							
							$("body").feedback(options);

						})
						
					});

					
					//This is for validation 
					$("#SaveeditConsigneeContactPersonForm").cr8vformvalidation({
						'preventDefault' : true,
					    'data_validation' : 'datavalid',
					    'input_label' : 'labelinput',
					    'onValidationError': function(arrMessages) {
					        // alert(JSON.stringify(arrMessages)); //shows the errors
					        var options = {title : "Edit Contact Person Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger",};
							$("body").feedback(options);
				        },
					    'onValidationSuccess': function() {

					    	var uiEditConsigneeContactPersonName = $("#editConsigneeContactPersonName"),
								uiEditConsigneeContactPersonPosition = $("#editConsigneeContactPersonPosition"),
								uiEditConsigneeContactPersonNumber = $("#editConsigneeContactPersonNumber"),
								uiEditConsigneeContactPersonAlterNumber = $("#editConsigneeContactPersonAlterNumber"),
								uiEditConsigneeContactPersonEmail = $("#editConsigneeContactPersonEmail"),
								uiEditConsignessContactPersonModalFooter = $("#editConsignessContactPersonModalFooter"),
								oTempDataContactPersonSelected = cr8v_platform.localStorage.get_local_storage({name : "edit_contactPerson_information"}),
								oTempSelectedImg = cr8v_platform.localStorage.get_local_storage({name : "setUpdateSelectedContactPersonImage"});


							for(var x in oTempDataContactPersonSelected)
							{
								if(sSelectedContactPersonId == oTempDataContactPersonSelected[x]["contactPersonId"])
								{
									
									oTempDataContactPersonSelected[x]["contactPersonName"] = $("#editConsigneeContactPersonName").val();
									oTempDataContactPersonSelected[x]["contactPersonPosition"] = $("#editConsigneeContactPersonPosition").val();
									oTempDataContactPersonSelected[x]["contactPersonNumber"] = $("#editConsigneeContactPersonNumber").val();
									oTempDataContactPersonSelected[x]["contactPersonAlterNumber"] = $("#editConsigneeContactPersonAlterNumber").val();
									oTempDataContactPersonSelected[x]["contactPersonEmail"] = $("#editConsigneeContactPersonEmail").val(),
									uidisplayContactPersonsContainer = $("#displayContactPersons");

									if(oTempSelectedImg.value != ""){
										oTempDataContactPersonSelected[x]["contactPersonImage"] = oTempSelectedImg.value;
									}
								}
							}

							cr8v_platform.localStorage.set_local_storage({
								name : "edit_contactPerson_information",
								data : oTempDataContactPersonSelected
							});

							$(".modal-close.close-me").trigger("click");

							CConsignee.renderElement("displayEditConsigneeContactPerson", oTempDataContactPersonSelected);

							var options = {
							  form : "box",
							  autoShow : true,
							  type : "success",
							  title  : "Updated Contact Person",
							  message : "Successfully Updated",
							  speed : "slow",
							  withShadow : true
							}
							
							$("body").feedback(options);
							        
						       
						}

						
					});


					// update Selected Contact Person
					uiBtnSaveUpdateContactPerson.off('click.updateConsigneeContactPerson').on('click.updateConsigneeContactPerson', function(){

						$("#SaveeditConsigneeContactPersonForm").trigger("submit");
					});
						
							
					uiCancelUpdatingseleted.off("click.cancelUpdating").on("click.cancelUpdating", function(){
						$(".modal-close.close-me").trigger("click");
					});

			}



		}


		
	}

	function renderElement (sType, oData)
	{

		if(sCurrentPath.indexOf("consignee/add_consignee_view") > -1)
		{
			if(sType === 'displayConsigneeContactPerson' && Object.keys(oData).length > 0)
			{

				var uidisplayContactPersonsContainer = $("#displayContactPersons"),
					icount = 0,
					uiOddEven = "",
					sImageDefault = "";
					uidisplayContactPersonsContainer.html("");

					for(x in oData)
					{
						(oData[x]["contactPersonImage"] != "") ? sImageDefault = oData[x]["contactPersonImage"] :  sImageDefault = DEFAULT_PROD_IMG;

							if(icount % 2 != 0){
								uiOddEven = '<div id="triggerId'+oData[x]["contactPersonId"]+'" class="width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person margin-left-20">';
								
							}else{
								uiOddEven = '<div id="triggerId'+oData[x]["contactPersonId"]+'" class="width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person">';
							}

							var sHtml = uiOddEven+
										'<div class="display-inline-mid width-70px margin-left-25">'+
											'<img src="'+sImageDefault+'" alt="user icon" class="width-100percent">'+
										'</div>'+
										'<div class=" parent-contact-name">'+
											'<div class="contact-name width-95per">'+
												'<p class="font-14 font-500">'+oData[x]["contactPersonName"]+'</p>'+
												'<p class="font-14 font-400 gray-color italic">'+oData[x]["contactPersonPosition"]+'</p>'+
											'</div>'+
										'</div>'+							
										'<div class="clear"></div>'+
										'<div class="f-left padding-left-30 margin-top-15">'+
											'<p class="font-14 font-500">Mobile 1:</p>'+
											'<p class="font-14 font-500 margin-top-5">Mobile 2:</p>'+
											'<p class="font-14 font-500 margin-top-5">Email:</p>'+											
										'</div>'+
										'<div class="f-right text-right padding-right-30 margin-top-15">'+
											'<p class="font-14 font-400">'+oData[x]["contactPersonNumber"]+'</p>'+
											'<p class="font-14 font-400 margin-top-5">'+oData[x]["contactPersonAlterNumber"]+'</p>'+
											'<p class="font-14 font-400 margin-top-5">'+oData[x]["contactPersonEmail"]+'</p>'+
										'</div>'+				
										'<div class="clear"></div>'+
										'<i class=" fa fa-pencil gray-color font-20 f-right margin-top-10 margin-right-30 default-cursor modal-trigger" modal-target="edit-contact-person"></i>'+
										'<div class="clear"></div>'+
									'</div>';

							

							var uiAddData = $(sHtml);
							uiAddData.data({
								"contactPersonId" : oData[x]["contactPersonId"],
								"contactPersonEmail" : oData[x]["contactPersonEmail"]
							});
							uidisplayContactPersonsContainer.append(uiAddData);
							bindEvents("editConsigneeContactInfo", uiAddData);

							icount++;

					}
					


			}
			else if(sType === 'displayEditConsigneeContactPerson' && Object.keys(oData).length > 0)
			{
				var uiEditConsigneeContactPersonName = $("#editConsigneeContactPersonName"),
					uiEditConsigneeContactPersonPosition = $("#editConsigneeContactPersonPosition"),
					uiEditConsigneeContactPersonNumber = $("#editConsigneeContactPersonNumber"),
					uiEditConsigneeContactPersonAlterNumber = $("#editConsigneeContactPersonAlterNumber"),
					uiEditConsigneeContactPersonEmail = $("#editConsigneeContactPersonEmail"),
					uiEditConsignessContactPersonModalFooter = $("#editConsignessContactPersonModalFooter");

					uiEditConsignessContactPersonModalFooter.html("");


					
					uiEditConsigneeContactPersonName.val(oData["currentPersonName"]);
					uiEditConsigneeContactPersonPosition.val(oData["currentPersonPosition"]);
					uiEditConsigneeContactPersonNumber.val(oData["currentPersonNumber"]);
					uiEditConsigneeContactPersonAlterNumber.val(oData["currentPersonAlterNumber"]);
					uiEditConsigneeContactPersonEmail.val(oData["currentPersonEmail"]);



					var sHtml = '<div class="editContactPersonModalFooterUi">'+
									'<i  class="fa fa-trash-o f-left gray-color font-20 margin-top-5 default-cursor deleteSelectedContactPersonBtn" ></i>'+
									'<div class="f-right text-right width-300px">'+
										'<button type="button" class="cancelUpdatingseleted font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>'+
										'<button type="button" class="saveUpdateSelectedContactPerson font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Save Changes</button>'+
									'</div>'+
									'<div class="clear"></div>'+
								'</div>';

							var uiGetData = $(sHtml);
							uiGetData.data({
								"contactPersonId": oData["currentPersonId"]
							});
							uiEditConsignessContactPersonModalFooter.append(uiGetData);
							bindEvents("editConsigneeContactPersonSelected", uiGetData);
							


					var imagePath = "";
						cr8v_platform.localStorage.set_local_storage({
							name:"setUpdateSelectedContactPersonImage",
							data: {value:imagePath}
						});
						cr8v_platform.localStorage.set_local_storage({
							name:"setSelectedContactPersonImage",
							data: {value:imagePath}
						});


					var cosigneeUpload = $("#edit_new_contact_person_img").cr8vUpload({
						url: BASEURL+"consignee/upload",
						accept: "jpg,png",
						autoUpload: true,
						onSelected : function(bValid)
						{
							if(bValid)
							{
								cr8v_platform.localStorage.set_local_storage({
									name : "edit_consignee_contact_image",
									data : {value:"true"}
								});
							}else{
								cr8v_platform.localStorage,set_local_storage({
									name : "edit_consignee_contact_image",
									data : {value:"false"}
								});
							}
						},
						success: function(data, textStatus, jqXHR)
						{

							imagePath = data.data.location;
								cr8v_platform.localStorage.set_local_storage({
									name:"setUpdateSelectedContactPersonImage",
									data: {value:imagePath}
								});
								cr8v_platform.localStorage.set_local_storage({
									name:"setSelectedContactPersonImage",
									data: {value:imagePath}
								});
						},
						error: function(jqXHR, textStatus, errorThrown)
						{

						}
					});

			}
			
			// ========================For The SideView of Add Consigee ==================================//

			var oGetSelectedPersonAdd = CConsignee.arrConsigneeAll,
				sDefaultImg = "",
				iCount = 0,
				uiSideViewConsigneeAllAdd = $("#sideViewConsigneeAllAdd");

				uiSideViewConsigneeAllAdd.html("");

				var FirstsHtml = '<div class="item-list active new-consignee-parent normal-cursor">'+
								'<div class="new-consignee" >'+
									'<i class="fa fa-plus display-inline-mid gray-color"></i>'+
									'<p class="display-inline-mid">New Consignee</p>'+
								'</div>'+
							'</div>';
				uiSideViewConsigneeAllAdd.append(FirstsHtml);
			
			for(var x in oGetSelectedPersonAdd)
			{

				(oGetSelectedPersonAdd[x]["image"] != "") ? sDefaultImg = oGetSelectedPersonAdd[x]["image"] : sDefaultImg = DEFAULT_PROD_IMG;

				var sHtml = '<div class="getTriggerActive item-list select_view item-list-no-hover text-no-hover" style="cursor: inherit;">'+
								'<div class="display-inline-mid image">'+
									'<img src="'+sDefaultImg+'">'+
								'</div>'+
								'<div class="display-inline-mid text">'+
									'<p>'+oGetSelectedPersonAdd[x]["name"]+'</p>'+
								'</div>'+
							'</div>';
						
						uiSideViewConsigneeAllAdd.append(sHtml);

						
			}

			//============================End of The SideView of Add Consigee ==================================//
		}
		else if(sCurrentPath.indexOf("consignee/consignee_list") > -1)
		{
			if(sType === 'getAllConsignee')
			{



				// For Consignee View Page
				var uiSideViewConsigneeAll = $("#sideViewConsigneeAll"),
					uiSetConsigneeImage = $("#setConsigneeImage"),
					uiSetConsigneeName = $("#setConsigneeName"),
					uiSetConsigneeAlias = $("#setConsigneeAlias"),
					uiSetConsigneeBillingMode = $("#setConsigneeBillingMode"),
					uisetConsigneeAddress = $("#setConsigneeAddress"),
					uiSetConsigneeNotes = $("#setConsigneeNotes"),
					uiDispalyConsigneeContactPersonBox = $("#dispalyConsigneeContactPersonBox"),
					iCount = 0,
					countPerson = 0,
					uiIsActive = "",
					sDefaultImg = "",
					btngetId = $("#generateConsignIdBtn");

					uiSideViewConsigneeAll.html("");
					uiDispalyConsigneeContactPersonBox.html("");


					btngetId.off("click.generateId").on("click.generateId", function(){

						cr8v_platform.localStorage.set_local_storage({
							name : "selectedConsignEdit",
							data : {value:oData[0]["id"]}
						});

						window.location.href = BASEURL+"consignee/edit_consignee";
					});				


				for(var x in oData)
				{

					(iCount == 0) ? uiIsActive = '<div id="triggerConsignId'+oData[x]["id"]+'"  class="getTriggerActive item-list select_view active">' : uiIsActive = '<div id="triggerConsignId'+oData[x]["id"]+'" class="getTriggerActive item-list select_view">';
					(oData[x]["image"] != "") ? sDefaultImg = oData[x]["image"] : sDefaultImg = DEFAULT_PROD_IMG;

					var sHtml = uiIsActive+
									'<div class="display-inline-mid image">'+
										'<img src="'+sDefaultImg+'">'+
									'</div>'+
									'<div class="display-inline-mid text">'+
										'<p>'+oData[x]["name"]+'</p>'+
									'</div>'+
								'</div>';
							var uiSetData = $(sHtml);
								uiSetData.data({
								"consigneeId":oData[x]["id"]
							});
							uiSideViewConsigneeAll.append(uiSetData);
							CConsignee.bindEvents('ViewConsigneeInfo', uiSetData);

							iCount++;


				}



				(oData[0]["image"] != "") ? sDefaultImg = oData[0]["image"] : sDefaultImg = DEFAULT_PROD_IMG;

				// uiSetConsigneeImage.text(sDefaultImg);
				uiSetConsigneeName.text(oData[0]["name"]);
				uiSetConsigneeAlias.text(oData[0]["alias"]);
				uiSetConsigneeBillingMode.text(oData[0]["gb_billing_mode"]);
				uisetConsigneeAddress.text(oData[0]["address"]);
				uiSetConsigneeNotes.text(oData[0]["notes"]);

				uiSetConsigneeImage.attr('src', sDefaultImg);
				var sImageDefault = "";


				// Display Contact Person
				for(var i in oData[0]["contact_person"])
				{
						(oData[0]["contact_person"][i]["image"] != "") ? sImageDefault = oData[0]["contact_person"][i]["image"] :  sImageDefault = DEFAULT_PROD_IMG;
					
						// console.log(oData[x]["contact_person"][i]["position"]);
						if(countPerson % 2 != 0){
							uiOddEven = '<div class=" width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person margin-left-20">';
							
						}else{
							uiOddEven = '<div class=" width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person ">';
						}

						var sContactPersonHtml = uiOddEven+
										'<div class="display-inline-mid width-70px margin-left-25">'+
											'<img src="'+sImageDefault+'" alt="user icon" class="width-100percent" />'+
										'</div>'+
										'<div class=" parent-contact-name">'+
											'<div class="contact-name width-95per">'+
												'<p class="font-14 font-500">'+oData[0]["contact_person"][i]["name"]+'</p>'+
												'<p class="font-14 font-400 gray-color italic">'+oData[0]["contact_person"][i]["position"]+'</p>'+
											'</div>'+
										'</div>'+							
										'<div class="clear"></div>'+
										'<div class="f-left padding-left-30 margin-top-15">'+
											'<p class="font-14 font-500">Mobile 1:</p>'+
											'<p class="font-14 font-500 margin-top-5">Mobile 2:</p>'+
											'<p class="font-14 font-500 margin-top-5">Email:</p>	'+										
										'</div>'+
										'<div class="f-right text-right padding-right-30 margin-top-15">'+
											'<p class="font-14 font-400">'+oData[0]["contact_person"][i]["contact_number"]+'</p>'+
											'<p class="font-14 font-400 margin-top-5">'+oData[0]["contact_person"][i]["alternate_contact_number"]+'</p>'+
											'<p class="font-14 font-400 margin-top-5">'+oData[0]["contact_person"][i]["email_address"]+'</p>'+
										'</div>'+
										'<div class="clear"></div>'+
									'</div>';

									var uiSetConsignContact = $(sContactPersonHtml);
									uiDispalyConsigneeContactPersonBox.append(uiSetConsignContact);
								countPerson++;

					

				}

			}
			else if(sType === 'viewSelectedConsignee')
			{


				var oConsignIdClick = cr8v_platform.localStorage.get_local_storage({name:"selectedConsignClick"}),
					uiSetConsigneeImage = $("#setConsigneeImage"),
					uiSetConsigneeName = $("#setConsigneeName"),
					uiSetConsigneeAlias = $("#setConsigneeAlias"),
					uiSetConsigneeBillingMode = $("#setConsigneeBillingMode"),
					uisetConsigneeAddress = $("#setConsigneeAddress"),
					uiSetConsigneeNotes = $("#setConsigneeNotes"),
					uiDispalyConsigneeContactPersonBox = $("#dispalyConsigneeContactPersonBox"),
					iCount = 0,
					countPerson = 0,
					uiOddEven = "",
					sDefaultImg = "",
					sImageDefault = "";

					
					uiDispalyConsigneeContactPersonBox.html("");

				for(var x in oData)
				{
					
					if(oData[x]["id"] == oConsignIdClick.value)
					{
						(oData[x]["image"] != "") ? sDefaultImg = oData[x]["image"] : sDefaultImg = DEFAULT_PROD_IMG;

						// uiSetConsigneeImage.text(sDefaultImg);
						uiSetConsigneeName.text(oData[x]["name"]);
						uiSetConsigneeAlias.text(oData[x]["alias"]);
						uiSetConsigneeBillingMode.text(oData[x]["gb_billing_mode"]);
						uisetConsigneeAddress.text(oData[x]["address"]);
						uiSetConsigneeNotes.text(oData[x]["notes"]);

						uiSetConsigneeImage.attr('src', sDefaultImg);

						// Display Contact Person
						for(var i in oData[x]["contact_person"])
						{	
							(oData[x]["contact_person"][i]["image"] != "") ? sImageDefault = oData[x]["contact_person"][i]["image"] :  sImageDefault = DEFAULT_PROD_IMG;
							
								// console.log(oData[x]["contact_person"][i]["position"]);
								if(countPerson % 2 != 0){
									uiOddEven = '<div class=" width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person margin-left-20">';
									
								}else{
									uiOddEven = '<div class=" width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person ">';
								}

								var sContactPersonHtml = uiOddEven+
												'<div class="display-inline-mid width-70px margin-left-25">'+
													'<img src="'+sImageDefault+'" alt="user icon" class="width-100percent" />'+
												'</div>'+
												'<div class=" parent-contact-name">'+
													'<div class="contact-name width-95per">'+
														'<p class="font-14 font-500">'+oData[x]["contact_person"][i]["name"]+'</p>'+
														'<p class="font-14 font-400 gray-color italic">'+oData[x]["contact_person"][i]["position"]+'</p>'+
													'</div>'+
												'</div>'+							
												'<div class="clear"></div>'+
												'<div class="f-left padding-left-30 margin-top-15">'+
													'<p class="font-14 font-500">Mobile 1:</p>'+
													'<p class="font-14 font-500 margin-top-5">Mobile 2:</p>'+
													'<p class="font-14 font-500 margin-top-5">Email:</p>	'+										
												'</div>'+
												'<div class="f-right text-right padding-right-30 margin-top-15">'+
													'<p class="font-14 font-400">'+oData[x]["contact_person"][i]["contact_number"]+'</p>'+
													'<p class="font-14 font-400 margin-top-5">'+oData[x]["contact_person"][i]["alternate_contact_number"]+'</p>'+
													'<p class="font-14 font-400 margin-top-5">'+oData[x]["contact_person"][i]["email_address"]+'</p>'+
												'</div>'+
												'<div class="clear"></div>'+
											'</div>';

											var uiSetConsignContact = $(sContactPersonHtml);
											uiDispalyConsigneeContactPersonBox.append(uiSetConsignContact);
										countPerson++;
						}
					}
				}
			}
			else if(sType === 'displayNoConsignee')
			{
				var uiConsigneeInformationBox = $(".consignee-information-box"),
					btnGenerateConsignIdBtn = $("#generateConsignIdBtn");

					uiConsigneeInformationBox.hide();
					btnGenerateConsignIdBtn.hide();
			}

		}
		else if(sCurrentPath.indexOf("consignee/edit_consignee") > -1)
		{



			if(sType === 'displaySelectedEdit')
			{
				// console.log('sadsaas');

				var uidisplayContactPersonsContainer = $("#displayContactPersons"),
					uiOddEven = "",
					icount = 0,
					sImageDefault = "",
					oGetCurrentEditContactPerson = {},
					oGetallContacts = [];

				uidisplayContactPersonsContainer.html("");

				for(var x in oData)
				{
					$("#editConsigneeName").val(oData[x]["name"]);
					$("#editConsigneeAlias").val(oData[x]["alias"]);
					$("#editConsigneeBillingMode").val(oData[x]["gb_billing_mode"]);
					$("#editConsigneeAddress").val(oData[x]["address"]);
					$("#editConsigneeNotes").val(oData[x]["notes"]);


					$(".inputGpBilling > .frm-custom-dropdown > .frm-custom-dropdown-txt > .dd-txt").val(oData[x]["gb_billing_mode"]);

					for(var i in oData[x]["contact_person"])
					{

						(oData[x]["contact_person"][i]["image"] != "") ? sImageDefault = oData[x]["contact_person"][i]["image"]:  sImageDefault = DEFAULT_PROD_IMG;

						if(icount % 2 != 0){
							uiOddEven = '<div id="triggerId'+oData[x]["contact_person"][i]["id"]+'" class="width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person margin-left-20">';
							
						}else{
							uiOddEven = '<div id="triggerId'+oData[x]["contact_person"][i]["id"]+'" class="width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person ">';
						}

						var sHtml = uiOddEven+
								'<div class="display-inline-mid width-70px margin-left-25">'+
									'<img src="'+sImageDefault+'" alt="user icon" class="width-100percent">'+
								'</div>'+
								'<div class=" parent-contact-name">'+
									'<div class="contact-name width-95per">'+
										'<p class="font-14 font-500">'+oData[x]["contact_person"][i]["name"]+'</p>'+
										'<p class="font-14 font-400 gray-color italic">'+oData[x]["contact_person"][i]["position"]+'</p>'+
									'</div>'+
								'</div>'+							
								'<div class="clear"></div>'+
								'<div class="f-left padding-left-30 margin-top-15">'+
									'<p class="font-14 font-500">Mobile 1:</p>'+
									'<p class="font-14 font-500 margin-top-5">Mobile 2:</p>'+
									'<p class="font-14 font-500 margin-top-5">Email:</p>'+											
								'</div>'+
								'<div class="f-right text-right padding-right-30 margin-top-15">'+
									'<p class="font-14 font-400">'+oData[x]["contact_person"][i]["contact_number"]+'</p>'+
									'<p class="font-14 font-400 margin-top-5">'+oData[x]["contact_person"][i]["alternate_contact_number"]+'</p>'+
									'<p class="font-14 font-400 margin-top-5">'+oData[x]["contact_person"][i]["email_address"]+'</p>'+
								'</div>'+				
								'<div class="clear"></div>'+
								'<i class=" fa fa-pencil gray-color font-20 f-right margin-top-10 margin-right-30 default-cursor modal-trigger" modal-target="edit-contact-person"></i>'+
								'<div class="clear"></div>'+
							'</div>';
						var uiGetData = $(sHtml);
							uiGetData.data({
								"contactPersonId" : oData[x]["contact_person"][i]["id"]
							});

							uidisplayContactPersonsContainer.append(uiGetData);
							bindEvents("editConsigneeContactPersonInfo", uiGetData);

						icount++;

						oGetCurrentEditContactPerson = {
							"contactPersonId": oData[x]["contact_person"][i]["id"],
					        "contactPersonName": oData[x]["contact_person"][i]["name"],
					        "contactPersonPosition": oData[x]["contact_person"][i]["position"],
					        "contactPersonNumber": oData[x]["contact_person"][i]["contact_number"],
					        "contactPersonAlterNumber": oData[x]["contact_person"][i]["alternate_contact_number"],
					        "contactPersonEmail": oData[x]["contact_person"][i]["email_address"],
					        "consignee_id": oData[x]["contact_person"][i]["consignee_id"],
					        "contactPersonImage": oData[x]["contact_person"][i]["image"]
						}

						oGetallContacts.push(oGetCurrentEditContactPerson);
						
						
					}

				}

				cr8v_platform.localStorage.set_local_storage({
					name : "edit_contactPerson_information",
					data : oGetallContacts
				});

				var cosigneeUpload = $("#upload_edit_consignee_img").cr8vUpload({
					backgroundPhotoUrl: oData[0]["image"],
					url: BASEURL+"consignee/upload",
					accept: "jpg,png",
					autoUpload: true,
					onSelected : function(bValid)
					{
						if(bValid)
						{
							// cr8v_platform.localStorage.set_local_storage({
							// 	name : "edit_consignee_image",
							// 	data : {value:"true"}
							// });
						}else{
							// cr8v_platform.localStorage,set_local_storage({
							// 	name : "edit_consignee_image",
							// 	data : {value:"false"}
							// });
						}
					},
					success: function(data, textStatus, jqXHR)
					{
						var	imagePath = data.data.location;
						cr8v_platform.localStorage.set_local_storage({
							name:"setEditCurrentSelectedImage",
							data: {value:imagePath}
						});
						var ogetit = cr8v_platform.localStorage.get_local_storage({name:"setEditCurrentSelectedImage"});
						console.log(ogetit);

					},
					error: function(jqXHR, textStatus, errorThrown)
					{

					}
				});

				
			}
			else if(sType === 'displayEditConsigneeContactPerson')
			{

				var uidisplayContactPersonsContainer = $("#displayContactPersons"),
					icount = 0,
					uiOddEven = "",
					sImageDefault = "";
					uidisplayContactPersonsContainer.html("");

					for(x in oData)
					{
							(oData[x]["contactPersonImage"] != "") ? sImageDefault = oData[x]["contactPersonImage"] :  sImageDefault = DEFAULT_PROD_IMG;

							if(icount % 2 != 0){
								uiOddEven = '<div id="triggerId'+oData[x]["contactPersonId"]+'" class="width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person margin-left-20">';
								
							}else{
								uiOddEven = '<div id="triggerId'+oData[x]["contactPersonId"]+'" class="width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person">';
							}

							var sHtml = uiOddEven+
										'<div class="display-inline-mid width-70px margin-left-25">'+
											'<img src="'+sImageDefault+'" alt="user icon" class="width-100percent">'+
										'</div>'+
										'<div class=" parent-contact-name">'+
											'<div class="contact-name width-95per">'+
												'<p class="font-14 font-500">'+oData[x]["contactPersonName"]+'</p>'+
												'<p class="font-14 font-400 gray-color italic">'+oData[x]["contactPersonPosition"]+'</p>'+
											'</div>'+
										'</div>'+							
										'<div class="clear"></div>'+
										'<div class="f-left padding-left-30 margin-top-15">'+
											'<p class="font-14 font-500">Mobile 1:</p>'+
											'<p class="font-14 font-500 margin-top-5">Mobile 2:</p>'+
											'<p class="font-14 font-500 margin-top-5">Email:</p>'+											
										'</div>'+
										'<div class="f-right text-right padding-right-30 margin-top-15">'+
											'<p class="font-14 font-400">'+oData[x]["contactPersonNumber"]+'</p>'+
											'<p class="font-14 font-400 margin-top-5">'+oData[x]["contactPersonAlterNumber"]+'</p>'+
											'<p class="font-14 font-400 margin-top-5">'+oData[x]["contactPersonEmail"]+'</p>'+
										'</div>'+				
										'<div class="clear"></div>'+
										'<i class=" fa fa-pencil gray-color font-20 f-right margin-top-10 margin-right-30 default-cursor modal-trigger" modal-target="edit-contact-person"></i>'+
										'<div class="clear"></div>'+
									'</div>';

							

							var uiAddData = $(sHtml);
							uiAddData.data({
								"contactPersonId" : oData[x]["contactPersonId"],
								"contactPersonEmail" : oData[x]["contactPersonEmail"]
							});
							uidisplayContactPersonsContainer.append(uiAddData);
							bindEvents("editConsigneeContactPersonInfo", uiAddData);

							icount++;


					}
			}
			else if(sType === 'displayUpdateConsigneeContactPerson')
			{
				var uiEditConsigneeContactPersonName = $("#editConsigneeContactPersonName"),
					uiEditConsigneeContactPersonPosition = $("#editConsigneeContactPersonPosition"),
					uiEditConsigneeContactPersonNumber = $("#editConsigneeContactPersonNumber"),
					uiEditConsigneeContactPersonAlterNumber = $("#editConsigneeContactPersonAlterNumber"),
					uiEditConsigneeContactPersonEmail = $("#editConsigneeContactPersonEmail"),
					uiEditConsignessContactPersonModalFooter = $("#editConsignessContactPersonModalFooter");

					uiEditConsignessContactPersonModalFooter.html("");

					
					uiEditConsigneeContactPersonName.val(oData["currentPersonName"]);
					uiEditConsigneeContactPersonPosition.val(oData["currentPersonPosition"]);
					uiEditConsigneeContactPersonNumber.val(oData["currentPersonNumber"]);
					uiEditConsigneeContactPersonAlterNumber.val(oData["currentPersonAlterNumber"]);
					uiEditConsigneeContactPersonEmail.val(oData["currentPersonEmail"]);



					var sHtml = '<div class="editContactPersonModalFooterUi">'+
									'<i  class="fa fa-trash-o f-left gray-color font-20 margin-top-5 default-cursor deleteSelectedContactPersonBtn" ></i>'+
									'<div class="f-right text-right width-300px">'+
										'<button type="button" class="cancelUpdatingseleted font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>'+
										'<button type="button" class="saveUpdateSelectedContactPerson font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Save Changes</button>'+
									'</div>'+
									'<div class="clear"></div>'+
								'</div>';

							var uiGetData = $(sHtml);
							uiGetData.data({
								"contactPersonId": oData["currentPersonId"]
							});

							// console.log(oData["currentPersonId"]);
							uiEditConsignessContactPersonModalFooter.append(uiGetData);
							bindEvents("updateConsigneeContactPersonSelected", uiGetData);
							


					var imagePath = "";
						cr8v_platform.localStorage.set_local_storage({
							name:"setUpdateSelectedContactPersonImage",
							data: {value:imagePath}
						});


					var cosigneeUpload = $("#update_new_contact_person_img").cr8vUpload({
						url: BASEURL+"consignee/upload",
						backgroundPhotoUrl: oData["currentPersonImage"],
						accept: "jpg,png",
						autoUpload: true,
						onSelected : function(bValid)
						{
							if(bValid)
							{
								cr8v_platform.localStorage.set_local_storage({
									name : "update_consignee_contact_image",
									data : {value:"true"}
								});
							}else{
								cr8v_platform.localStorage,set_local_storage({
									name : "update_consignee_contact_image",
									data : {value:"false"}
								});
							}
						},
						success: function(data, textStatus, jqXHR)
						{

							var imagePath = data.data.location;
								cr8v_platform.localStorage.set_local_storage({
									name:"setUpdateSelectedContactPersonImage",
									data: {value:imagePath}
								});
						},
						error: function(jqXHR, textStatus, errorThrown)
						{

						}
					});
			}


		}

	}

	return {
		getSideUpdate : getSideUpdate,
		_add : _add,
		_update : _update,
		_delete : _delete,
		bindEvents : bindEvents,
		renderElement : renderElement
	}

})();

$(document).ready(function(){

	//For Consignee View Page
	if(sCurrentPath.indexOf("consignee/consignee_list") > -1)
	{
		var oAjaxConfig = {
			type: "GET",
			url: BASEURL+"consignee/get_all_consignee",
			data: {params : 'adsds'},
			async: false,
			success: function(oResponse)
			{
				if(oResponse.data.length == 0)
				{
					// console.log("walang laman");
					CConsignee.renderElement("displayNoConsignee");

				}else{
					// console.log(JSON.stringify(oResponse.data));
					CConsignee.arrConsigneeAll = oResponse.data;
					CConsignee.renderElement("getAllConsignee", CConsignee.arrConsigneeAll);

					CConsignee.getSideUpdate(oResponse.data);
					
				}
				
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);

		
	}

	// For Edit Consignee
	if(sCurrentPath.indexOf("consignee/edit_consignee") > -1 && sCurrentPath.indexOf("consignee/edit_consignee_record") == -1)
	{
		var oGetConsigneeId = cr8v_platform.localStorage.get_local_storage({name:"selectedConsignEdit"});

			console.log(oGetConsigneeId);
		var oAjaxConfig = {
				type: "POST",
				url: BASEURL+"consignee/get_selected_consignee",
				data: {"consignee_id" : oGetConsigneeId.value},
				async: false,
				success: function(oResponse)
				{

					// console.log(JSON.stringify(oResponse.data));
					CConsignee.arrConsigneeAll = oResponse.data
					CConsignee.arrConsigneeSelected = oResponse.data;
					CConsignee.renderElement("displaySelectedEdit", CConsignee.arrConsigneeSelected);

					CConsignee.bindEvents();
					
				}
			}

			cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);

			var uieditContactlinkModal = $("#editContactlinkModal");

			uieditContactlinkModal.off("click.editContactlinkModal").on("click.editContactlinkModal", function(){
				$("#addConsigneeContactPersonName").val("");
				$("#addConsigneeContactPersonPosition").val("");
				$("#addConsigneeContactNumber").val("");
				$("#addConsigneeContactAlterNumber").val("");
				$("#addConsigneeContactPersonEmail").val("");

				CConsignee.bindEvents("EditAddContactPerson");
			});


			

	}

	if(sCurrentPath.indexOf("consignee/add_consignee_view") > -1) {
	

		CConsignee.bindEvents();
		CConsignee.renderElement();
	}


	// delete the temp information of contact person
	cr8v_platform.localStorage.delete_local_storage({name:"setCurrentImage"});
	cr8v_platform.localStorage.delete_local_storage({name:"add_contactPerson_information"});

	

	var uiAddContactlinkModal = $("#addContactlinkModal");

	$("select").transformDD();
	CDropDownRetract.retractDropdown($("select"));

	uiAddContactlinkModal.off("click.AddContactlinkModal").on("click.AddContactlinkModal", function(){
		$("#addConsigneeContactPersonName").val("");
		$("#addConsigneeContactPersonPosition").val("");
		$("#addConsigneeContactNumber").val("");
		$("#addConsigneeContactAlterNumber").val("");
		$("#addConsigneeContactPersonEmail").val("");

		CConsignee.bindEvents("AddContactPerson");
	});
});
