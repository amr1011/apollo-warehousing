var CTransferCompanyGoods = (function() {
	var oTransferList = {},
	currentDateTime = {},
	oCurrentTransfer,
	oStorages = {},
	oLocations = {},
	oProductBatches = {},
	oBatchList = {},
	failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg",
	successIcon = BASEURL + "assets/js/plugins/feedback/images/success.svg",
	oProducts = {},
	oUploadedDocuments = {},
	oProductLocations = {},
	oWorkerTypes = {},
	oCURRENTPRODUCTBATCHES = {},
	oCURRENTPRODUCTTRANSFERS = {},
	oTRANSFERPRODUCTS = {},
	oCURRENTWORKERLIST = {},
	oCURRENTEQUIPMENTLIST = {},
	oUNITOFMEASURELIST = {},
	oMODEOFTRANSFERLIST = {},
	oWORKERTYPELIST = {},
	oTRANSFERLIST = {},
	uiGenerateViewModal = $('.modal-generate-view');

	var uiProductDetailsDisplay = $("#company_create_product_details"),
	uiProductListDiv = $("#product_list_div");

	function getAllProducts() {
		var oOptions = {
			type : "GET",
			data : {test : ""},
			returnType : "json",
			url : BASEURL + "transfers/get_product_list",
			success : function(oResult) {
				_setAllProducts(oResult.data);
			}
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllProducts(callBack) {
		var oOptions = {
			type : 'GET',
			data : {test : true},
			returnType : 'json',
			url : BASEURL + 'transfers/get_product_list',
			success : function(oResult) {
				if(typeof(callBack) == 'function') {
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getAllTransfers() {
		var oOptions = {
			type : "GET",
			data : {test : ""},
			returnType : "json",
			url : BASEURL + "transfers/get_all_company_transfers",
			success : function(oResult) {
				_setAllTransfers(oResult.data);
			}
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getTransfersList(callBack) {
		var oOptions = {
			type : "GET",
			data : {test : ""},
			returnType : "json",
			url : BASEURL + 'transfers/get_all_company_transfers',
			success : function(oResult) {
				oTRANSFERLIST = oResult.data;
				
				if(typeof(callBack) == 'function') {
					if(typeof callBack == 'function'){
						callBack(oResult.data);
					}
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _setAllProducts(oResult) 
	{
		oProducts = oResult;

		cr8v_platform.localStorage.delete_local_storage({name : "productList"});
		cr8v_platform.localStorage.set_local_storage({name : "productList", data : oProducts});
		return oProducts;

	}

	function _setAllTransfers(oResult) 
	{
		oTransferList = oResult;

		cr8v_platform.localStorage.delete_local_storage({name : "transferCompanyGoodList"});
		cr8v_platform.localStorage.set_local_storage({name : "transferCompanyGoodList", data : oTransferList});
		return oTransferList;

	}

	function bindEvents() 
	{
		var sCurrentPath = window.location.href;

		if(sCurrentPath.indexOf('transfers/') > - 1) {
			
			if(sCurrentPath.indexOf("company_transfer") > -1) {
				// SHOWS THE GENERATING VIEW MODAL.
				uiGenerateViewModal.addClass('showed');

				// GET ALL TRANSFERS
				_getTransfersList(function(oResult) {
					
					// GET ALL WORKER TYPES.
					_getWorkerTypeList(function(oResult){
						oWORKERTYPELIST = oResult;
						oTransferList = oResult;
						_UI_companyTransfer();
						
						var btnSubmitCompleteTransfer = $('.btn-submit-complete-tranfer'),
						btnCloseCompleteTransfer = $(".transfer-close"),
						btnViewRecord = $(".transfer_view_record"),
						modalCompleteTransfer = $(".modal-complete-transfer"),
						btnShowTransferRecord = $('.show-transfer-record'),
						uiHover = $('.hover-tbl');

						uiHover.css({
							'height' : '100%'
						});

						btnSubmitCompleteTransfer.off('click').on('click', function() {
							var oParams = {
								page : "company_transfer",
								id : modalCompleteTransfer.data("id")
							};

							_completeTransfer(oParams);
						});

						btnViewRecord.off("click").on("click", function() {
							var iCurrentTransferID = $(this).attr("id");
							// alert(iCurrentTransferID);
							_setCurrentTransfer(iCurrentTransferID);
						});

						btnCloseCompleteTransfer.off("click").on("click", function() {
							modalCompleteTransfer.removeClass("showed");
						});

						btnShowTransferRecord.off('click').on('click', function() {
							_showTransferRecord($(this).data('id'));
						});

					});

					if(Object.keys(oTransferList).length == 0){
						uiGenerateViewModal.removeClass('showed');
					}

				});
				
			}

			if(sCurrentPath.indexOf("company_complete") > -1) {
				// SHOWS THE GENERATING VIEW MODAL.
				uiGenerateViewModal.addClass('showed');

				var iTransferLogId = cr8v_platform.localStorage.get_local_storage({name : 'currentTransferCompanyGoods'});

				_getTransfersList(function(oResult){
					oTransferList = oResult;

					for(var a in oTransferList) {
						if(oTransferList[a].id == iTransferLogId.transfer_log_id) {
							oCurrentTransfer = oTransferList[a];
						}
					}

					// GETS ALL MODE OF TRANSFER LIST.
					_getAllModeOfTransferList(function(oResult) {
						oMODEOFTRANSFERLIST = oResult;

						// GETS ALL THE UNIT OF MEASURE.
						_getUnitOfMeasureList(function(oResult) {
							oUNITOFMEASURELIST = oResult;

							// GETS ALL THE LOCATION.
							_getLocations(function(oResult) {
								oLocations = oResult;

								// GETS ALL THE WORKER FOR THE CURRENT TRANSFER.
								_getAllWorkers(oCurrentTransfer.id, function(oResult) {
									oCURRENTWORKERLIST = oResult;

									// GETS ALL THE EQUIPMENT FOR THE CURRENT TRANSFER.
									_getAllEquipments(oCurrentTransfer.id, function(oResult) {
										oCURRENTEQUIPMENTLIST = oResult;

										// FINALLY GETS THE TRANSFER RECORD DETAILS AND THEN PROCEEDS ON RENDERING IT.
										_getTransferRecord(oCurrentTransfer.id, function(oResult) {
											_UI_company_complete(oResult);
											_getAllTransferNote(oCurrentTransfer.id);
										});
										
									});

								});

							});	

						});

					});

				});	
					
			}

			if(sCurrentPath.indexOf("company_ongoing") > -1 && sCurrentPath.indexOf('company_ongoing_edit') == -1) {
				// SHOWS THE GENERATING VIEW MODAL.
				uiGenerateViewModal.addClass('showed');

				// GET ALL TRANSFERS.
				_getTransfersList(function(oResult) {
					var oCurrentTransferDetails = cr8v_platform.localStorage.get_local_storage({name : 'currentTransferCompanyGoods'}),
					oTRANSFERLIST = oResult,
					iTransferLogId = oCurrentTransferDetails.transfer_log_id;

					// SET THE CURRENT TRANSFER DETAILS.
					for(var a in oTRANSFERLIST) {
						if(iTransferLogId == oTRANSFERLIST[a].id) {
							oCurrentTransfer = oTRANSFERLIST[a];
						}
					}

					// console.log(oCurrentTransfer);

					// GETS ALL UNIT OF MEASURE
					_getUnitOfMeasureList(function(oResult) {
						oUNITOFMEASURELIST = oResult

						// GET MODE OF TRANSFER LIST.
						_getAllModeOfTransferList(function(oResult) {
							oMODEOFTRANSFERLIST = oResult;

							// GET ALL PRODUCTS FROM DB.
							_getAllProducts(function(oResult) {
								oProducts = oResult;

								// GETS ALL STORAGES.
								getAllStorage(function(oResult){	
									oStorages = oResult;

									// var oCurrentTransfer = _getCurrentTransfer(),
									var uiTransferNumber = $('.ongoing-transfer-number'),
									btnSubmitAddNote = $('#submit_add_note'),
									btnEditProduct = $('#btn_ongoing_edit_product'),
									btnEditTransfer = $(".transfer-edit-information"),
									btnSubmitCompleteTransfer = $('.btn-submit-complete-tranfer');

									btnSubmitCompleteTransfer.off('click').on('click', function() {
										_completeTransfer(oParams);
									});

									btnSubmitAddNote.off('click').on('click', function() {
										_addNote();
									});

									btnEditTransfer.off('click').on('click', function() {
										var oData = {
											name : "edit_transfer",
											data : oCurrentTransfer
										};

										cr8v_platform.localStorage.delete_local_storage({name : "edit_transfer"});
										cr8v_platform.localStorage.set_local_storage(oData);
										setTimeout(function() {
											window.location.href = BASEURL + 'transfers/company_ongoing_edit';
										}, 300);
									});


									_getOngoingDetails(oCurrentTransfer.id, function(oResult) {
										oTRANSFERPRODUCTS = oResult;

										uiTransferNumber.html('Transfer No. ' + oCurrentTransfer.to_number);
										uiTransferNumber.data('id', oCurrentTransfer.id);
										_UI_company_ongoing();
										_getAllTransferNote(oCurrentTransfer.id);
										_getTransferDocuments({id : oCurrentTransfer.id});
									});

								});

							});
							
						});	

					});
					
				});		

			}

			if(sCurrentPath.indexOf("company_create") > -1) {
				getAllProducts();
				getAllTransfers();
				getAllStorage(function(oResult){	
					oStorages = oResult;
					_displayBatchStorages(oStorages);
				});
				_getAllModeOfTransfer();
				_getAllLocation({page : 'company_create'});
				_getAllBatches(function(oResult) {
					oBatchList = oResult;
					// console.log(oBatchList);
				});

				uiGenerateViewModal.addClass('showed');
				setTimeout(function() {
					uiGenerateViewModal.removeClass('showed');
				}, 800);

				var productDropDown = $("#company_create_product_dropdown"),
				btnAddProduct = $("#company_create_add_product"),
				btnAddTransferCompany = $("#submit_add_transfer_company"),
				btnAddNote = $("#submit_add_note"),
				btnCloseAddBatchModal = $(".close-add-batch-modal"),
				modalAddBatch = $("#add_batch_modal"),
				btnSubmitAddBatch = $("#company_create_submit_add_batch"),
				btnSubmitBrowse = $(".browse");
				_getTransferNumber();
				_UI_companyCreateProductDropdown();
				_UI_companyCreateModeOfTransfer({name : "get"});

				btnAddProduct.off("click").on("click", function() {
					var clonedProductListDiv = uiProductListDiv.clone(),
					oParams = {
						iSelectedProduct : productDropDown,
						sClonedTemplate : clonedProductListDiv
					};

					_addProduct(oParams);
					_getUnitOfMeasures();
				});

				btnAddTransferCompany.off("click").on("click", function() {
					_add();
				});

				btnAddNote.off("click").on("click", function() {
					_getCurrentDateTime();
				});

				btnCloseAddBatchModal.off("click").on("click", function() {
					modalAddBatch.removeClass("showed");
				});

			}

			if(sCurrentPath.indexOf("company_ongoing_edit") > -1) {
				// SHOWS THE GENERATING VIEW MODAL.
				uiGenerateViewModal.addClass('showed');

				// GETS ALL THE PRODUCT
				_getAllProducts(function(oResult) {
					// console.log(oResult);
					oProducts = oResult;

					// GETS ALL THE LOCATION.
					_getLocations(function(oResult) {
						oLocations = oResult;

						// GETS ALL MODE OF TRANSFER.
						_getAllModeOfTransferList(function(oResult) {
							oMODEOFTRANSFERLIST = oResult;

							// GETS ALL STORAGE.
							getAllStorage(function(oResult){
								oStorages = oResult;
								_displayBatchStorages(oStorages);

								var oEditTransfer = _getEditTransfer(),
								btnAddProduct = $('.btn-add-product'),
								btnSubmitEdit = $('.btn-submit-edit'),
								inputProductDropDown = $('#edit_product_list_dropdown');

								btnAddProduct.off("click").on("click", function() {
									var oData = {
										product_id : inputProductDropDown.val(),
										div_class : 'edit-product-list',
										template_class : 'edit-product-original-template',
										template_object : $('.edit-product-original-template')
									};

									_addProductToList(oData);
								});

								btnSubmitEdit.off('click').on('click', function() {
									// alert('submit daw dre');
									_edit();
								});

								_UI_OngoingEdit({data : oEditTransfer});
							});

						});

					});

				});	

			}
		}	

	}

	function _buildTemplate(oParams) 
	{
		var oElementData = oParams.data,
		sTemplate = "";

		switch(oParams.name) {

			case "company_transfer" : 

				if(oElementData.status == 0) {

					sTemplate += '<div data-id="' + oElementData.id + '" class="transfer-hoverable tbl-like hover-transfer-tbl">'
					+	'<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">'
					+		'<div class="width-15percent display-inline-mid text-center">'
					+			'<p class="transfer_number font-400 font-14 f-none width-100percent">1234567890</p>'						
					+		'</div>'
					+		'<div class="width-15percent display-inline-mid text-center">'
					+			'<p class="transfer_date font-400 font-14 f-none width-100percent">22-Oct-2015</p>'
					+		'</div>'
					+		'<div class="width-40percent display-inline-mid font-400 text-center">'
					+			'<ul class="transfer_products text-left text-indent-20 padding-left-50 font-14">'
					+			'</ul>'
					+		'</div>'
					+		'<div class="width-15percent font-400 display-inline-mid text-center">'
					+			'<div class="transfer_image owner-img width-30per display-inline-mid ">'
					+			'</div>'
					+			'<p class="transfer_owner width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>'								
					+		'</div>'
					+		'<div class="width-15percent display-inline-mid text-center">'
					+			'<p class="transfer_status font-400 font-14 f-none text-center width-100percent">Ongoing</p>'							
					+		'</div>'
					+	'</div>'
					+	'<div class="hover-tbl text-center ">' 
					+		'<a href="'+ BASEURL + 'transfers/company_ongoing">'
					+			'<button id="' + oElementData.id + '" class="transfer_view_record btn general-btn margin-right-10">View Record</button>'
					+		'</a>'
					+		'<button class="transfer_complete_transfer btn general-btn modal-trigger" modal-target="view-complete-transfer">Complete Transfer</button>'	
					+	'</div>';
					+'</div>'
				} else if(oElementData.status == 1) {

					sTemplate += '<div data-id="' + oElementData.id + '" class="tbl-like hover-transfer-tbl">'
					+	'<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">'
					+		'<div class="width-15percent display-inline-mid text-center">'
					+			'<p class="transfer_number font-400 font-14 f-none width-100percent">1234567890</p>'						
					+		'</div>'
					+		'<div class="width-15percent display-inline-mid text-center">'
					+			'<p class="transfer_date font-400 font-14 f-none width-100percent">22-Oct-2015</p>'
					+		'</div>'
					+		'<div class="width-40percent display-inline-mid font-400 text-center">'
					+			'<ul class="transfer_products text-left text-indent-20 padding-left-50 font-14">'								
					+			'</ul>'
					+		'</div>'
					+		'<div class="width-15percent font-400 display-inline-mid text-center">'
					+			'<div class="transfer_image owner-img width-30per display-inline-mid ">'
					+			'</div>'
					+			'<p class="transfer_owner width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>'								
					+		'</div>'
					+		'<div class="width-15percent display-inline-mid text-center">'
					+			'<p class="transfer_status font-400 font-14 f-none text-center width-100percent">Complete</p>'							
					+		'</div>'	
					+	'</div>'
					+	'<div class="hover-tbl text-center ">' 
					+		'<a href="'+ BASEURL + 'transfers/company_complete">'
					+			'<button id="' + oElementData.id + '" class="transfer_view_record btn general-btn margin-right-10">View Record</button>'
					+		'</a>'	
					+	'</div>'
					+ '</div>';
				}

				return sTemplate;
			break;

			case 'company_transfer_add_product':
				// sTemplate += '<div id="company_create_product_display" data-id="' + oElementData + '" class="bggray-white height-190px">'
				sTemplate += '<div id="company_create_product_div">'
				+'<div id="add_product_image" class="height-100percent display-inline-mid width-230px">'
				+		'<img src="../assets/images/holcim.jpg" alt="" class="height-100percent width-100percent">'
				+	'</div>'
				+	'<div class="display-inline-top width-75percent padding-left-10">'
				+		'<div class="padding-all-15 bg-light-gray">'
				+			'<p class="add_product_sku_name font-16 font-bold f-left">SKU# 1234567890 - Holcim Cement</p>'
				+			'<div id="add_product_close_display" class="f-right width-20px margin-left-10">'
				+				'<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">'
				+			'</div>'
				+			'<div class="clear"></div>'
				+			'</div>'
				+			'<div class="padding-all-15 text-left">'
				+				'<p class="f-left no-margin-all width-20percent font-bold">Vendor</p>'
				+				'<p id="add_product_vendor" class="f-left no-margin-all width-20percent">Innova Farming</p>'
				+				'<p class="f-left no-margin-all width-20percent font-bold">Transfer Method</p>'	
				+				'<div class="f-left">'										
				+					'<input type="radio" class="display-inline-mid width-50px default-cursor" name="bag-or-bulk" id="bulk">'
				+					'<label for="bulk" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>'
				+				'</div>'
				+				'<div class="f-left margin-left-10">'																
				+					'<input type="radio" class="display-inline-mid width-50px default-cursor" name="bag-or-bulk" id="piece" >'
				+					'<label for ="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Piece</label>'
				+				'</div>'								
				+				'<div class="clear"></div>'
				+			'</div>'
				+			'<div class="padding-all-15 bg-light-gray text-left font-0">'
				+				'<p class="display-inline-mid width-20percent font-bold">Aging Days</p>'
				+				'<p id="add_product_aging_days" class="display-inline-mid width-20percent">10 Days</p>'
				+				'<p class="display-inline-mid width-25per font-14 font-bold">Qty to Transfer</p>'
				+				'<input id="qty_to_transfer_KG" type="text" class="display-inline-mid width-70px height-26 t-small" name="bag-or-bulk" disabled>'
				+				'<div class="select display-inline-mid small width-70px height-26">'
				+					'<div class="frm-custom-dropdown">'
				+						'<div class="frm-custom-dropdown-txt">'
				+							'<input class="dd-txt" type="text">'
				+					'</div>'
				+					'<div class="frm-custom-icon"></div>'
				+						'<div class="frm-custom-dropdown-option" style="display: none;">'
				// +								'<div class="option" data-value="1">#000000001 Cobra</div>'
				// +								'<div class="option" data-value="2">#000000002 Samurai</div>'
				+						'</div>'
				+					'</div>'
				+					'<select class="unit_of_measure_dropdown frm-custom-dropdown-origin" style="display: none;">'
				// +						'<option value="1">#000000001 Cobra</option>'
				// +						'<option value="2">#000000002 Samurai</option>'
				+					'</select>'
				+				'</div>'
				// +				'<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">KG</p>'
				+				'<input id="qty_to_transfer_PIECE" type="text" class="display-inline-mid width-70px height-26 t-small" name="bag-or-bulk" disabled>'
				+				'<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">Piece</p>'				
				+				'<div class="clear"></div>'
				+			'</div>'				
				+			'<div class="padding-all-15 text-left">'
				+				'<p class="f-left width-20percent font-bold">Description</p>'
				+				'<p id="add_product_description" class="f-left">Ang Cement na matibay</p>'
				+				'<div class="clear"></div>'
				+			'</div>'	
				+		'</div>';
				+'</div>'
				// +	'</div>';
				return sTemplate;
			break;

			case 'company_complete_worker_list':
				sTemplate += '<div class="padding-left-20 padding-top-15 padding-right-20 padding-bottom-15 font-0 bggray-middark">'
				+	'<p class="worker_name no-margin-all font-14 font-bold f-left  ">Drew Tanaka</p>'
				+	'<p class="worker_position no-margin-all font-14 font-bold f-right ">Bagger</p>'
				+	'<div class="clear"></div>'
				+ '</div>'
				return sTemplate;
			break;

			case 'company_complete_equipment_list':
				sTemplate += '<div class="padding-left-20 padding-top-15 padding-right-20 padding-bottom-15 font-0 bggray-middark">'
				+	'<p class="no-margin-all font-14 font-bold f-left  ">Crane</p>'	
				+	'<div class="clear"></div>'
				+ '</div>';

				return sTemplate;
			break;

			case 'company_create_add_note':
				sTemplate += '<div class="note_div border-full padding-all-10 margin-left-18 margin-top-10">'
				+	'<div class="border-bottom-small border-gray padding-bottom-10">'
				+		'<p id="company_create_note_subject" class="f-left font-14 font-400">Subject: <span class="note_subject">' + oElementData.note_subject + '</span></p>'
				+		'<p class="f-right font-14 font-400">Date/Time: <span class="note_date" data-record="' + oElementData.note_date_record + '">' + oElementData.note_date + '</span></p>'
				+		'<div class="clear"></div>'
				+	'</div>'
				+	'<p class="font-14 font-400 no-padding-left padding-all-10"><span class="note_text">' + oElementData.note_text + '</span></p>'
				+	'<a href="" class="f-right padding-all-10  font-400">Show More</a>'
				+	'<div class="clear"></div>'
				+ '</div>';

				_render({name : "company_create_note", data : sTemplate});
			break;

			case 'company_complete_view_note_list':
				sTemplate += '<div class="border-full padding-all-10 margin-left-25 margin-right-10 margin-top-10">'
				+	'<div class="border-bottom-small border-gray padding-bottom-10">'
				+		'<p class="f-left font-14 font-400">Subject: ' + oElementData.subject + '</p>'
				+		'<p class="f-right font-14 font-400">Date/Time: ' + oElementData.date_created + '</p>'
				+		'<div class="clear"></div>'
				+	'</div>'
				+	'<p class="font-14 font-400 no-padding-left padding-all-10">' + oElementData.note + '</p>'
				+	'<a href="" class="f-right padding-all-10 font-400">Show More</a>'
				+	'<div class="clear"></div>'
				+ '</div>';

				return sTemplate;
			break;

			case 'company_create_add_batch_sub_location':

				if(oElementData == "white_bg") {
					sTemplate += '<div class="font-0 text-center location-check position-rel default-cursor">'
					+	'<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">'
					+		'<i class="fa fa-check font-22 "></i>'
					+	'</div>'
					+	'<div class="width-75percent display-inline-mid padding-all-20">'
					+		'<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>'
					+		'<i class="display-inline-mid font-14 padding-right-10">&gt;</i>'
					+		'<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>'
					+		'<i class="display-inline-mid font-14 padding-right-10">&gt;</i>'
					+		'<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>'
					+		'<i class="display-inline-mid font-14 padding-right-10">&gt;</i>'
					+		'<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>'
					+		'<i class="display-inline-mid font-14 padding-right-10">&gt;</i>'
					+		'<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>'
					+		'<div class="clear"></div>'
					+	'</div>'
					+	'<div class="width-25percent display-inline-mid">'
					+		'<p class="font-14 font-400 no-margin-all ">1000 KG MO</p>'
					+	'</div>'
					+ '</div>';
				} else {
					sTemplate += '<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">'
					+		'<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">'
					+			'<i class="fa fa-check font-22"></i>'
					+		'</div>'
					+		'<div class="width-75percent display-inline-mid padding-all-20">'
					+			'<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>'
					+			'<i class="display-inline-mid font-14 padding-right-10">&gt;</i>'
					+			'<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>'
					+			'<i class="display-inline-mid font-14 padding-right-10">&gt;</i>'
					+			'<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>'
					+			'<i class="display-inline-mid font-14 padding-right-10">&gt;</i>'
					+			'<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>'
					+			'<i class="display-inline-mid font-14 padding-right-10">&gt;</i>'
					+			'<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">PLATO </p>'
					+			'<div class="clear"></div>'
					+		'</div>'
					+		'<div class="width-25percent display-inline-mid">'
					+			'<p class="font-14 font-400 no-margin-all ">1000 KG</p>'
					+		'</div>'
					+	'</div>';
				}

				_render({name : "company_create_add_batch_sub_location", data : sTemplate});
			break;

			case 'company_create_batch_list':
				sTemplate += '<div class="product-location">'
				+	'<p class="f-left font-14 font-bold padding-right-20">Batch Name:</p>'
				+	'<p class="batch-name-display f-left font-14 font-400">Sample Batch Name</p>'
				+	'<p class="batch-quantity-display f-right font-14 font-bold padding-bottom-20">Quantity 50 KG</p>'
				+	'<div class="clear"></div>'
				+	'<table class="tbl-4c3h">'
				+		'<thead>'
				+			'<tr>'
				+				'<th class="width-50percent black-color">Origin</th>'
				+				'<th class="width-50percent">Destination</th>'
				+			'</tr>'
				+		'</thead>'
				+	'</table>'
				+	'<div class="font-0 tbl-dark-color margin-bottom-20 position-rel">'
				+		'<div class="width-100percent ">'
				+			'<div class="batch-origin padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25">'
				+				'<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>'
				+				'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>'
				+				'<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>'
				+				'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>'
				+				'<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>'
				+				'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>'
				+				'<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>'
				+				'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>'
				+				'<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>'
				+				'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>'
				+				'<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>'
				+			'</div>'
				+			'<div class="batch-destination padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25">'
				+				'<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>'
				+				'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>'
				+				'<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>'
				+				'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>'
				+				'<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>'
				+				'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>'
				+				'<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>'
				+				'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>'
				+				'<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>'
				+				'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>'
				+				'<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>'
				+			'</div>'
				+		'</div>'
				+		'<div class="edit-hide">'
				+			'<button class="btn-edit-batch btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit Batch</button>'
				+			'<button class="btn-remove-batch btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10" >Remove Batch</button>'
				+		'</div>'
				+	'</div>';

				_render({name: "company_create_batch_list", data : sTemplate, batch_details : oElementData});
			break;

			case 'add_worker' :
				var sWorkerTypes = _UI_completeTransferWorkerTypes();
				sTemplate = '<div class="add-worker-div border-bottom-small border-black padding-bottom-10 padding-top-10 italic margin-left-10">'
				+	'<input type="text" placeholder="Name" class="worker-name display-inline-mid width-45per">'
				+ 	'<select class="worker-designation display-inline-mid width-45per no-margin-left">'
				+ sWorkerTypes
				+	'</select>'
				+	'<div class="btn-remove-worker display-inline-mid width-15px margin-left-10">'
				+		'<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">'
				+	'</div>'
				+ '</div>';

				return sTemplate;
			break;

			case 'add_equipment' :
				sTemplate = '<div class="equipment-div border-bottom-small border-black padding-bottom-10 margin-left-10 padding-top-10 italic">'
				+	'<input type="text" placeholder="Equipment name" class="equipment-name display-inline-mid width-90per">'
				+	'<div class="btn-remove-equipment display-inline-mid width-15px margin-left-10">'
				+		'<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">'
				+	'</div>'
				+'</div>';
				
				return sTemplate;
			break;

			case 'transfer_document':
				if(oElementData == 'dark') {
					sTemplate += '<div class="document-dark-template table-content position-rel tbl-dark-color">'
					+	'<div class="content-show padding-all-10">'
					+		'<div class="width-85per display-inline-mid padding-left-10">'
					+			'<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>'
					+			'<p class="document-name display-inline-mid">Document Name</p>'
					+		'</div>'
					+		'<p class="document-date-time display-inline-mid ">22-Oct-2015</p>'
					+	'</div>'
					+	'<div class="content-hide">'
					+		'<a href="#" class="display-inline-mid">'
					+			'<button class="btn-view-document btn general-btn padding-left-30 padding-right-30">View</button>'
					+		'</a>'
					+		'<a href="#" class="display-inline-mid">'
					+			'<button class="btn-download-document btn general-btn">Download</button>'
					+		'</a>'
					+		'<a href="#" class="display-inline-mid">'
					+			'<button class="btn-print-document btn general-btn padding-left-30 padding-right-30">Print</button>'
					+		'</a>'
					+	'</div>'
					+ '</div>';
				} else {
					sTemplate += '<div class="document-template table-content position-rel">'
					+	'<div class="content-show padding-all-10">'
					+		'<div class="width-85per display-inline-mid padding-left-10">'
					+			'<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>'
					+			'<p class="document-name display-inline-mid">Document Name</p>'
					+		'</div>'
					+		'<p class="document-date-time display-inline-mid ">22-Oct-2015</p>'
					+	'</div>'
					+	'<div class="content-hide">'
					+		'<a href="#" class="display-inline-mid">'
					+			'<button class="btn-view-document btn general-btn padding-left-30 padding-right-30">View</button>'
					+		'</a>'
					+		'<a href="#" class="display-inline-mid">'
					+			'<button class="btn-download-document btn general-btn">Download</button>'
					+		'</a>'
					+		'<a href="#" class="display-inline-mid">'
					+			'<button class="btn-print-document btn general-btn padding-left-30 padding-right-30">Print</button>'
					+		'</a>'
					+	'</div>'
					+ '</div>';
				}

				return sTemplate;
			break;

			case 'add_note':
				sTemplate =	 '<div class="note_div border-full padding-all-10 margin-left-18 margin-top-10">'
				+	'<div class="border-bottom-small border-gray padding-bottom-10">'
				+		'<p id="company_create_note_subject" class="f-left font-14 font-400">Subject: <span class="note-subject"></span></p>'
				+		'<p class="note-date f-right font-14 font-400"></p>'
				+		'<div class="clear"></div>'
				+	'</div>'
				+	'<p class="font-14 font-400 no-padding-left padding-all-10"><span class="note-text"></span></p>'
				+	'<a href="" class="f-right padding-all-10  font-400">Show More</a>'
				+	'<div class="clear"></div>'
				+ '</div>';

				return sTemplate;
			break;

			case 'sub_location':
				sTemplate = '<div class="location-div bg-light-gray font-0 text-center position-rel default-cursor">'
				+	'<div class="location-breadcrumb width-65percent display-inline-mid padding-all-20">'
				+	'</div>'
				+	'<div class="width-20percent display-inline-mid">'
				+		'<p class="batch-quantity font-14 font-400 no-margin-all ">1000 KG</p>'
				+	'</div>'
				+ '</div>';

				return sTemplate;
			break;

			case 'sub_location_origin':
				if(oElementData == 'last') {
					sTemplate = '<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>';
				} else {
					sTemplate = '<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>'
					+ '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>';	
				}

				return sTemplate;
			break;

		}

	}

	function _render(oParams) 
	{
		var oElementData = oParams.data,
		oElementTemplate = oParams.template;


		var oElement = {

			"company_transfer" : function(oData) {

				var uiDiv = $("#company_transfer_list"),
				uiCurrentDiv = null;
				uiDiv.append(oElementTemplate);
				uiCurrentDiv = uiDiv.find("[data-id='" + oElementData.id + "']");
				uiCurrentDiv.find('.transfer_number').html(oElementData.to_number);
				uiCurrentDiv.find('.transfer_date').html(oElementData.transfer_date);
				uiCurrentDiv.find('.transfer_owner').html("Abdul O'neil");

				var arrProducts = oElementData.product_list,
				sTemplate = "",
				uiProductList = uiCurrentDiv.find('.transfer_products');

				for(var a in arrProducts) {
					sTemplate += '<li>#' + arrProducts[a].details.sku + ' - ' + arrProducts[a].details.name + '</li>';
				}

				uiProductList.html(sTemplate);
				var btnCompleteTransfer = $('.transfer_complete_transfer'),
				btnAddWorker = $("#btn_add_worker"),
				uiAddWorkerList = $("#add_worker_list"),
				btnAddEquipment = $("#btn_add_equipment"),
				uiAddEquipmentList = $("#add_equipment_list");

				btnCompleteTransfer.off("click").on("click", function() {
					var uiParentDiv = $(this).closest(".transfer-hoverable"),
					modalCompleteTransfer = $('.modal-complete-transfer'),
					btnShowTransferRecord = $('.show-transfer-record');

					modalCompleteTransfer.addClass('showed');
					modalCompleteTransfer.attr("data-id", uiParentDiv.data("id"));
					btnShowTransferRecord.data("id", uiParentDiv.data("id"));
					btnCompleteTransfer.data("id", uiParentDiv.data("id"));
				});

				btnAddWorker.off('click').on('click', function() {
					var sTemplate = _buildTemplate({name : "add_worker"});

					uiAddWorkerList.append(sTemplate);

					var btnRemoveWorker = $(".btn-remove-worker");

					btnRemoveWorker.off("click").on("click", function() {
						var btnParent = $(this).closest('.add-worker-div');
						btnParent.remove();
					});
				});

				btnAddEquipment.off('click').on('click', function() {
					var sTemplate = _buildTemplate({name : "add_equipment"});

					uiAddEquipmentList.append(sTemplate);

					var btnRemoveEquipment = $(".btn-remove-equipment");

					btnRemoveEquipment.off("click").on("click", function() {
						var btnParent = $(this).closest('.equipment-div');
						btnParent.remove();
					});
				});

				setTimeout(function(){
					// HIDES THE GENERATING VIEW MODAL.
					uiGenerateViewModal.removeClass('showed');
				}, 800);

			},
			// COMPLETED TRANSFER VIEW.
			"company_complete" : function(oData) {

				//DATA DECLARATIONS.
				var oTransferRecord = oParams.transfer_record,
				oProductList = oTransferRecord.product_list,
				oNoteList = oTransferRecord.note_list,
				oDocumentList = oTransferRecord.document_list,
				sModeOfTransfer = '';

				//console.log(oProductList);

				if(Object.keys(oProductList).length == 0) {
					uiGenerateViewModal.removeClass('showed');
				}

				// console.log(oProductList);

				// UI DECLARATIONS.
				var uiTranferNumber = $("#company_complete_transfer_number"),
				uiModeOfTransportation = $("#company_complete_mode_of_transformation"),
				uiReason = $("#company_complete_reason"),
				uiDateTimeIssued = $("#company_complete_date_and_time"),
				uiRequestedBy = $("#company_complete_requested_by"),
				uiStatus = $("#company_complete_status"),
				uiStartDate = $("#company_complete_start_date"),
				uiTimeDuration = $("#company_complete_time_duration"),
				uiEndDate = $("#company_complete_end_date"),
				uiWorkerList = $("#company_complete_worker_list"),
				uiEquipmentList = $("#company_complete_equipment_list"),
				sWorkerList = "",
				sEquipmentList = "",
				uiProductTemplate = $(".complete-product-div-template"),
				uiProductDiv = $("#complete_product_div"),
				uiProductBatchTemplate = $(".product-batch-template"),
				uiProductLocationDarkTemplate = $(".product-location-dark-template"),
				uiProductLocationTemplate = $(".product-location-template"),
				uiDocumentDiv = $("#complete_document_div"),
				uiNoteDiv = $("#complete_note_div");

				//LOOP FOR WORKER LIST
				for(var a in oCURRENTWORKERLIST) {
					if(a % 2 == 0) {
						sWorkerList += '<div class="padding-left-20 padding-top-15 padding-right-20 padding-bottom-15 font-0 bggray-middark">'
						+	'<p class="no-margin-all font-14 font-bold f-left  ">' + oCURRENTWORKERLIST[a].name + '</p>'
						+	'<p class="no-margin-all font-14 font-bold f-right ">' + oCURRENTWORKERLIST[a].designation + '</p>'
						+	'<div class="clear"></div>'
						+ '</div>';	
					} else {
						sWorkerList += '<div class="padding-left-20 padding-top-15 padding-right-20 padding-bottom-15 font-0">'
						+	'<p class="no-margin-all font-14 font-bold f-left  ">' + oCURRENTWORKERLIST[a].name + '</p>'
						+	'<p class="no-margin-all font-14 font-bold f-right ">' + oCURRENTWORKERLIST[a].designation + '</p>'
						+	'<div class="clear"></div>'
						+ '</div>';	
					}
				}

				//LOOP FOR EQUIPMENT LIST
				for(var b in oCURRENTEQUIPMENTLIST) {
					if(b % 2 == 0) {
						sEquipmentList += '<div class="padding-left-20 padding-top-15 padding-right-20 padding-bottom-15 font-0 bggray-middark">'
						+	'<p class="no-margin-all font-14 font-bold f-left  ">' + oCURRENTEQUIPMENTLIST[b].name + '</p>'
						+	'<div class="clear"></div>'
						+ '</div>';
					} else {
						sEquipmentList += '<div class="padding-left-20 padding-top-15 padding-right-20 padding-bottom-15 font-0 ">'
						+	'<p class="no-margin-all font-14 font-bold f-left  ">' + oCURRENTEQUIPMENTLIST[b].name + '</p>'
						+	'<div class="clear"></div>'
						+ '</div>';
					}
				}

				//LOOP FOR PRODUCT LIST
				for(var x in oProductList) {

					var oProduct = oProductList[x],
					oBatches = oProduct.batches,
					currentProductDiv = null, 	
					sUnitOfMeasureName = '',
					sTransferMethod = oProductList[x].map_transfer_method == 1 ? 'By Bulk' : 'By Piece',
					uiProductTemplateClone = uiProductTemplate.clone(),
					sTotalQtyToTransfer = _calculateTotalQty({data : oBatches});
					oCurrentProductDetails = {
						id : oProduct.map_product_id,
						sku : oProduct.product_sku,
						name : oProduct.name,
						image : oProduct.image
					};


					uiProductTemplateClone.show();
					uiProductTemplateClone.removeClass('complete-product-div-template');
					uiProductTemplateClone.addClass('complete-product-div');
					uiProductTemplateClone.attr("data-id", oProduct.id);
					uiProductTemplateClone.data(oCurrentProductDetails);


					var uiProductNameAndSku = uiProductTemplateClone.find('.product-sku-name'),
					uiProductVendor = uiProductTemplateClone.find('.product-vendor'),
					uiProductQtyToTransfer = uiProductTemplateClone.find('.product-qty-to-transfer'),
					uiTotalQtyToTransfer = uiProductTemplateClone.find('.total-qty-to-transfer'),
					uiProductTransferMethod = uiProductTemplateClone.find('.product-transfer-method'),
					uiProductAgingdays = uiProductTemplateClone.find('.product-aging-days'),
					uiProductImage = uiProductTemplateClone.find('.product-image'),
					uiProductDescription = uiProductTemplateClone.find('.product-description');


					uiProductNameAndSku.html('SKU #' + oProduct.product_sku + ' - ' + oProduct.product_name);
					uiProductDescription.html(oProduct.product_description);
					uiProductImage.html('<img src="' + oProduct.product_image + '" alt="" class="height-100percent" style="width:190px;">');
					uiTotalQtyToTransfer.html(sTotalQtyToTransfer.toFixed(2) + ' ' + sUnitOfMeasureName);
					uiProductTransferMethod.html(sTransferMethod);


					if(Object.keys(oBatches).length == 0) {
						uiGenerateViewModal.removeClass('showed');
					} else {
						for(var a in oUNITOFMEASURELIST) {
							if(oUNITOFMEASURELIST[a].id == oBatches[0].unit_of_measure) {
								sUnitOfMeasureName = oUNITOFMEASURELIST[a].name;
							}
						}
					}


					// LOOP FOR PRODUCT BATCHES
					for(var e in oBatches) {
						var uiBatchCloned = uiProductBatchTemplate.clone(),
						uiBatchName = uiBatchCloned.find('.batch-name'),
						uiBatchQty = uiBatchCloned.find('.batch-qty'),
						uiBatchList = uiProductTemplateClone.find('.product-location-div'),
						uiOrigin = uiBatchCloned.find('.batch-origin'),
						uiDestination = uiBatchCloned.find('.batch-destination'),
						sUnitOfMeasure = '',
						oOrigin = _buildLocationBreadCrumb({id : oBatches[e].origin_container}),
						oDestination = _buildLocationBreadCrumb({id : oBatches[e].destination_container}),
						iOriginLength = parseInt(Object.keys(oOrigin).length) - parseInt(1),
						iDestinationLength = parseInt(Object.keys(oDestination).length) - parseInt(1),
						sDestination = '',
						sOrigin = '',
						oUnitOfMeasure = cr8v_platform.localStorage.get_local_storage({name : 'unit_of_measures'});


						// LOOP FOR GETTING THE UNIT OF MEASURE NAME.
						for(var a in oUnitOfMeasure) {
							if(oUnitOfMeasure[a].id == oBatches[e].unit_of_measure) {
								sUnitOfMeasure = oUnitOfMeasure[a].name;
							}
						}


						// LOOP FOR BUILDING THE ORIGIN BREADCRUMB.
						for(var b in oOrigin) {
							if(b != iOriginLength) {
								sOrigin += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oOrigin[b].name + '</p>'
								+'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>';
							} else {
								sOrigin += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oOrigin[b].name + '</p>';
							}
						}


						// LOOP FOR BUILDING DESTINATION BREADCRUMB.
						for(var c in oDestination) {
							if(c != iDestinationLength) {
								sDestination += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oDestination[c].name + '</p>'
								+'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>';
							} else {
								sDestination += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oDestination[c].name + '</p>';

								// HIDES THE GENERATING VIEW MODAL.
								uiGenerateViewModal.removeClass('showed');
							}
						}


						uiOrigin.html(sOrigin);
						uiDestination.html(sDestination);		
						uiBatchCloned.removeClass('product-batch-template');
						uiBatchCloned.addClass('product-batch-cloned');
						uiBatchCloned.show();
						uiBatchName.html(oBatches[e].batch_name);
						uiBatchQty.html('Quantity ' + oBatches[e].quantity + ' ' + sUnitOfMeasureName);
						uiBatchList.append(uiBatchCloned);

					}

					uiProductDiv.append(uiProductTemplateClone);

				}

				_getTransferDocuments({id : oElementData.id});

				for(var a in oMODEOFTRANSFERLIST) {
					if(oMODEOFTRANSFERLIST[a].id == oTransferRecord.mode_of_transfer_id) {
						sModeOfTransfer = oMODEOFTRANSFERLIST[a].name;
					}
				}

				uiTranferNumber.html('Transfer No. ' + oTransferRecord.to_number + ' (Complete)');
				uiReason.html(oTransferRecord.reason_for_transfer);
				uiDateTimeIssued.html(oTransferRecord.date_issued);
				uiStartDate.html(oTransferRecord.date_start);
				uiEndDate.html(oTransferRecord.date_complete);
				uiTimeDuration.html(oTransferRecord.date_interval);
				uiWorkerList.append(sWorkerList);
				uiEquipmentList.append(sEquipmentList);
				uiModeOfTransportation.html(sModeOfTransfer);

			},
			"company_create_product_dropdown" : function(oData) {

				var uiSelect = $("#company_create_product_dropdown");
				var uiSiblings = uiSelect.siblings();
				uiSiblings.remove();
				uiSelect.removeClass("frm-custom-dropdown-origin");
				uiSelect.html("");
				uiSelect.html(oElementData);
				uiSelect.transformDD();
				CDropDownRetract.retractDropdown(uiSelect);
				_documentEvents(true);

			},
			// CREATES THE VIEW FOR EACH PRODUCT ADDED.
			"company_create_product_display" : function(oData) {

				var uiProductDisplay = $("#company_create_product_details"),
				oProductDetails = oElementData.product_details;
				oClonedTemplate = oElementData.cloned_template,
				sRandomName = _generateRandomName();

				oClonedTemplate.find(".add_product_sku_name").html('SKU# ' + oProductDetails.sku + ' - ' + oProductDetails.name);
				oClonedTemplate.find("#add_product_image").html('<img src="' + oProductDetails.image + '" alt="" class="height-100percent width-100percent"></img>')
				oClonedTemplate.find("#add_product_vendor").html("Test Vendor");
				oClonedTemplate.find("#add_product_aging_days").html("100 Days");
				oClonedTemplate.find("#add_product_description").html(oProductDetails.description);
				oClonedTemplate.find(".btn_add_company_transfer_batch").data(oProductDetails);
				oClonedTemplate.find(".remove_product").attr("data-id", oProductDetails.id);
				oClonedTemplate.find(".qty-to-transfer-KG").attr("data-id", oProductDetails.id);
				oClonedTemplate.find(".qty-to-transfer-Piece").attr("data-id", oProductDetails.id);
				oClonedTemplate.find(".radio-bulk").attr("data-id", oProductDetails.id).attr("name", "radio-" + sRandomName);
				oClonedTemplate.find(".radio-piece").attr("data-id", oProductDetails.id).attr("name", "radio-" + sRandomName);
				oClonedTemplate.find('.add-product-image').html('<img src="' + oProductDetails.image + '" alt="" class="height-100percent width-100percent">');
				oClonedTemplate.attr("data-id", oProductDetails.id);

				_getProductBatches(oProductDetails.id, function(oResult) {
					oCURRENTPRODUCTBATCHES = oResult;
					oClonedTemplate.data('product_batches', oCURRENTPRODUCTBATCHES);
				});

				oClonedTemplate.show();
				uiProductDisplay.append(oClonedTemplate);
				
				//DECLARE THE NEWLY ADDED DOMS.
				var btnBulk = $(".radio-bulk"),
				btnPiece = $(".radio-piece"),
				uiProductDiv = $("#company_create_product_div"),
				inputTransferPiece = $(".qty-to-transfer-Piece"),
				inputTransferKg = $(".qty-to-transfer-KG"),
				btnRemoveProduct = $(".remove_product"),
				btnAddBatch = $(".btn_add_company_transfer_batch"),
				btnConfirmAddBatch = $("#company_create_submit_add_batch");

				inputTransferKg.attr("disabled", true);
				inputTransferPiece.attr("disabled", true);
				btnAddBatch.addClass("disabled");

				//============================================================//
				//==============EVENTS FOR NEWLY ADDED DOMS===================//
				//============================================================//
				btnRemoveProduct.off("click").on("click", function() {
					iParentId = $(this).data("id");
					var oParentDiv = uiProductDisplay.find(".product-list-div[data-id ='" + iParentId + "']");
					// var oParentDiv = uiProductDisplay.find(".product-list-div[data-id]='" + iParentId + "'").attr("data-id", iParentId);
					oParentDiv.remove();
					_UI_companyCreateProductDropdown();

					// _checkSelectedProducts({
					//   element : $("#company_create_product_details"),
					//   dropdown : $("#company_create_product_dropdown"),
					//   option : $("#company_create_product_dropdown option")
					// });

					_checkSelectedProducts({
					  element : $("#company_create_product_details"),
					  dropdown : $("#company_create_product_dropdown")
					});
				});

				btnBulk.off("click").on("click", function() {
					var uiParent = $(this).closest(".display-inline-top"),
					uiQtyToTransferContainer = uiParent.find(".padding-all-15:nth-child(3)"),
					uiParentDiv = $(this).closest('.product-list-div'),
					siblingAddBatch = uiParentDiv.find('.btn_add_company_transfer_batch');
					
					if($(this).prop("checked")) {
						siblingAddBatch.removeClass("disabled");
					}

					uiUnitOfMeasure = $(".unit_of_measure_dropdown"),
					uiUnitRemoveDropdown = $(".unit-remove-dropdown").find(".frm-custom-dropdown");
					uiUnitRemoveDropdown.remove();
					uiUnitOfMeasure.show().css({
		            	"padding" : "3px 4px",
		            	"width" :  "70px"
	            	});
	            	uiParent.find('.unit_of_measure_dropdown').removeAttr('disabled');
				});

				btnPiece.off("click").on("click", function() {
					var uiParent = $(this).closest(".display-inline-top"),
					uiQtyToTransferContainer = uiParent.find(".padding-all-15:nth-child(3)"),
					uiParentDiv = $(this).closest('.product-list-div'),
					siblingAddBatch = uiParentDiv.find('.btn_add_company_transfer_batch');
					
					if($(this).prop("checked")) {
						siblingAddBatch.removeClass("disabled");
					}

					uiUnitOfMeasure = $(".unit_of_measure_dropdown"),
					uiUnitRemoveDropdown = $(".unit-remove-dropdown").find(".frm-custom-dropdown");
					uiUnitRemoveDropdown.remove();
					uiUnitOfMeasure.show().css({
		            	"padding" : "3px 4px",
		            	"width" :  "70px"
	            	});
	            	uiParent.find('.unit_of_measure_dropdown').attr('disabled', true);
				});

				uiProductDisplay.off('mouseover').on('mouseover', function() {
					uiUnitOfMeasure = $(".unit_of_measure_dropdown"),
					uiUnitRemoveDropdown = $(".unit-remove-dropdown").find(".frm-custom-dropdown");
					uiUnitRemoveDropdown.remove();
					uiUnitOfMeasure.show().css({
		            	"padding" : "3px 4px",
		            	"width" :  "70px"
	            	});
				});

				btnAddBatch.off("click").on("click", function() {
					var oCurrentProduct = $(this).data(),
					modalAddBatch = $("#add_batch_modal"),
					uiParent = $(this).closest("#product_list_div"),
					uiRadioBulk = uiParent.find('.radio-bulk'),
					uiRadioPiece = uiParent.find('.radio-piece'),
					uiInputBulk = uiParent.find('.qty-to-transfer-KG'),
					uiInputPiece = uiParent.find('.qty-to-transfer-Piece'),
					uiAttr = $(this).hasClass('disabled');
					oCURRENTPRODUCTBATCHES = uiParent.data('product_batches');

					if(uiRadioBulk.prop("checked")) {
						oCurrentProduct["balance_to_transfer"] = uiInputBulk.val();
						oCurrentProduct["unit_of_measure"] = uiParent.find('.unit_of_measure_dropdown option:selected').text();
						oCurrentProduct["unit_of_measure_id"] = uiParent.find('.unit_of_measure_dropdown option:selected').val();
					} else {
						oCurrentProduct["balance_to_transfer"] = uiInputPiece.val();
						oCurrentProduct["unit_of_measure"] = "Pcs";
						oCurrentProduct["unit_of_measure_id"] = 0;
					}

					btnConfirmAddBatch.data(oCurrentProduct);
					modalAddBatch.addClass("showed");
					modalAddBatch.data(oCurrentProduct);
					_render({name: "add_batch_modal", button : $(this)});
				});

				// inputTransferPiece.off("change keyup").on("change keyup", function() {
				// 	var uiParent = $(this).closest(".product-list-div"),
				// 	siblingInput = uiParent.find('.qty-to-transfer-KG'),
				//	siblingAddBatch = uiParent.find('.btn_add_company_transfer_batch');

				// 	$(this).val() > 0 || siblingInput.val() > 0 ? siblingAddBatch.removeClass("disabled") : siblingAddBatch.addClass("disabled");
				// });

				// inputTransferKg.off("change keyup").on("change keyup", function() {
				// 	var uiParent = $(this).closest(".product-list-div"),
				// 	siblingInput = uiParent.find('.qty-to-transfer-Piece'),
				// 	siblingAddBatch = uiParent.find('.btn_add_company_transfer_batch');
					
				// 	$(this).val() > 0 || siblingInput.val() > 0 ? siblingAddBatch.removeClass("disabled") : siblingAddBatch.addClass("disabled");
				// });
				//============================================================//
				//==========END OF EVENTS FOR NEWLY ADDED DOMS================//
				//============================================================//

				// _checkSelectedProducts({
				//   element : $("#company_create_product_details"),
				//   dropdown : $("#company_create_product_dropdown"),
				//   option : $("#company_create_product_dropdown option")
				// });

				_checkSelectedProducts({
				  element : $("#company_create_product_details"),
				  dropdown : $("#company_create_product_dropdown")
				});

			},
			"company_create_mode_of_transfer" : function(oData) {

				var uiSelect = $("#company_create_mode_of_transfer");
				var uiSiblings = uiSelect.siblings();
				uiSiblings.remove();
				uiSelect.removeClass("frm-custom-dropdown-origin");
				uiSelect.html("");
				uiSelect.html(oElementData);
				uiSelect.transformDD();
				CDropDownRetract.retractDropdown(uiSelect);

			},
			"company_create_transfer_number" : function(oData) {

				var uiTranferNumber = $(".company_create_transfer_number");

				uiTranferNumber.html(oElementData);

			},
			// SHOWS THE ADD BATCH MODAL.
			"add_batch_modal" : function(oData) {

				var sAmountToTransfer = $("#add_product_amount_to_transfer"),
				sNewAmountToTransfer = "",
				sUnitOfMeasure = '',
				modalAddBatch = $("#add_batch_modal"),
				oCurrentProduct = modalAddBatch.data();

				var uiProductNameAndSku = $('#add_product_sku_name'),
				uiProductTransferBalance = $("#add_product_transfer_balance"),
				uiProductListDiv = $('#company_create_product_details'),
				uiContainer = $("#company_create_product_details"),
				uiParentDiv = uiProductListDiv.find('.product-list-div[data-id="' + oCurrentProduct.id + '"]');

				var inputBatch = $("#add_batch_name"),
				inputUnitOfMeasure = uiParentDiv.find('.unit_of_measure_dropdown'),
				inputRadioPiece = uiParentDiv.find('.radio-piece'),
				inputRadioBulk = uiParentDiv.find('.radio-bulk'),
				btnSubmitAddBatch = $("#company_create_submit_add_batch"),
				btnCurrentAddBatchButton = oParams.button;

				sUnitOfMeasure = inputRadioPiece.prop('checked') == true ? 'Bags' : uiParentDiv.find('.unit_of_measure_dropdown options:selected').text();

				// SHOWS THE BATCHES FOR THE CHOSEN PRODUCT.
				_UI_batchDropDown({page : "create_batch"});

				// SHOWS THE TRANSFER BALANCE IN THE MODAL.
				uiProductNameAndSku.html('SKU# ' + oCurrentProduct.sku + ' - ' + oCurrentProduct.name);
				if(oCurrentProduct.balance == undefined) {
					uiProductTransferBalance.html(oCurrentProduct.balance_to_transfer + " " + oCurrentProduct.unit_of_measure);
				} else {
					uiProductTransferBalance.html(oCurrentProduct.balance + " " + oCurrentProduct.unit_of_measure);
				}


				// SUBMIT BATCH EVENT
				btnSubmitAddBatch.off("click").on("click", function() {

					var uiParent = uiContainer.find('.product-list-div[data-id=' + oCurrentProduct.id + ']'),
					btnAddBatch = uiParent.find('.btn_add_company_transfer_batch'),
					uiSubLocationList = $("#company_create_add_batch_sub_location"),
					iCurrentBalance = oCurrentProduct.balance_to_transfer,
					iTotalQtyToTransfer = uiParentDiv.data('total_quantity_to_transfer'),
					uiRemainingBalance = uiParent.find('.product-remaining-balance'),
					uiLocationDiv = $('.batch-check'),
					oDestination = _getBreadCrumbsHierarchy(),
					oOrigin = uiSubLocationList.data('breadcrumb'),
					oFixedDestination =	_buildDestinationHierarchy({data : oDestination}),
					oCurrentBatch = $("#add_batch_name option:selected").data();

					//if()

					if(Object.keys(oCurrentBatch).length > 0 && oDestination != undefined) {

						var iDestinationCount = Object.keys(oFixedDestination).length - parseInt(1),
						iOriginCount = Object.keys(oOrigin).length - parseInt(1);

						
						iTotalQtyToTransfer = iTotalQtyToTransfer != undefined ? parseInt(iTotalQtyToTransfer) + parseInt($(".batch-quantity").data('quantity')) : parseInt($(".batch-quantity").data('quantity'));

						oBatch = {
							id : oCurrentProduct.id,
							quantity : $(".batch-quantity").data('quantity'),
							old_storage_location : $('.batch-storage-name').data('id'),
							new_storage_location : oDestination[0].id,
							origin : uiSubLocationList.data('breadcrumb'),
							new_location : oDestination[ iDestinationCount ].id,
							destination : oFixedDestination,
							batch_name : $("#add_batch_name option:selected").text(),
							old_batch_id : $('#add_batch_name option:selected').val(),
							storage_assignment_id : oCurrentBatch.batch_details.storage_assignment_id,
							old_location_id : oOrigin[ iOriginCount ].id,
						};

						uiParentDiv.data('total_quantity_to_transfer', iTotalQtyToTransfer);
						uiParentDiv.find('.total-qty-to-transfer').html(iTotalQtyToTransfer + ' ' + sUnitOfMeasure);
						btnAddBatch.data(oCurrentProduct);
						_UI_companyCreateSubmitAddBatch(oBatch);
						modalAddBatch.removeClass("showed");
						sAmountToTransfer.val("");
						uiRemainingBalance.html(iCurrentBalance);

					} else {
						
						oFeedback = {
							title : "Error!",
							message : "Please fill in all required data.",
							speed : "mid",
							icon : failIcon
						};

						$('body').feedback(oFeedback);

					}
					
				});

				// SELECT BATCH EVENT.
				inputBatch.off('change').on('change', function() {

					var iSelectedBatch = $(this).val(),
					uiStorageName = $('.batch-storage-name'),
					iStorageId = null,
					oCurrentBatchDetails = $('#add_batch_name option:selected').data('batch_details');

					for(var a in oBatchList) {
						if(oBatchList[a].id == iSelectedBatch) {
							iStorageId = oBatchList[a].storage_id;
						}
					}

					for(var b in oStorages) {
						if(oStorages[b].id == iStorageId) {
							uiStorageName.html(oStorages[b].name);
							uiStorageName.data('id', oStorages[b].id)
						}
					}

					_buildLocationHierarchy({id : iSelectedBatch, name : "add_batch_modal"});
				});


				inputBatch.trigger("change");


				// REMOVES THE 'SELECT A BATCH' OPTION FROM THE DROPDOWN.
				// $('.frm-custom-dropdown').off('click').on('click', function() {
				// 	var uiOptions = $('#add_batch_name option'),
				// 	uiNewOptions = "";
					
				// 	uiOptions.each(function() {
				// 		console.log($(this));
				// 		var uiOption = $(this);
						
				// 		if(uiOption.text() == "Select a batch") {
				// 			uiOption.remove();
				// 		} else {
				// 			uiNewOptions += '<option value="' + uiOption.val() + '">' + uiOption.text() + '</option>';
				// 		}
				// 	});

				// 	var uiSelect = $("#add_batch_name");
				// 	var uiSiblings = uiSelect.siblings();
				// 	uiSiblings.remove();
				// 	uiSelect.removeClass("frm-custom-dropdown-origin");
				// 	uiSelect.html("");
				// 	uiSelect.html(uiNewOptions);
				// 	uiSelect.transformDD();
				// });


			},
			"company_create_note" : function(oData) {

				var uiNoteList = $("#company_create_note_list"),
				modalAddNotes = $("#modal_company_create_add_note"),
				uiBody = $("body"),
				uiNoteText = $("#add_note_text"),
				uiNoteSubject = $("#add_note_subject");

				modalAddNotes.removeClass("showed");
				uiNoteList.append(oElementData);
				uiBody.removeAttr("style");
				uiNoteText.val("");
				uiNoteSubject.val("");


			},
			"company_complete_view_note_list" : function(oData) {

				var uiNoteList = $(".view_note_list");

				var sCurrentPath = window.location.href;
				if(sCurrentPath.indexOf("company_ongoing") > -1) { 
					var oNote = $(oElementData);
					oNote.removeClass('margin-left-25');
					oNote.removeClass('margin-right-10');
					oNote.addClass('margin-left-18');

					uiNoteList.append(oNote);
				} else {
					uiNoteList.append(oElementData);
				}	


			},
			"company_create_add_batch_sub_location" : function(oData) {

				var uiSubLocationList = $("#company_create_add_batch_sub_location");

				uiSubLocationList.append(oElementData);

				$(".location-check").each(function() {
					var selectedLocation = true;
					
					$(this).off("click").on("click", function() {
						if (selectedLocation == true) {
							$(this).find(".location-address-check").removeClass("display-block");
							$(this).find(".location-address-check").addClass("display-block");
							selectedLocation = false;
						} else {
							$(this).find(".location-address-check").removeClass("display-block");
							selectedLocation = true;
						}
					});
				});


			}, 
			//CREATES THE LIST OF PRODUCT LOCATION FOR EACH PRODUCT IN CREATE TRANSFER RECORD.
			"company_create_batch_list" : function(oData) { 
				
				var batch_details = oParams.batch_details,
				parent_id = batch_details.id,
				uiParent = $('#company_create_product_details').find('#product_list_div[data-id=' + parent_id + ']'),
				uiBatchList = uiParent.find('#ui_company_create_product_location'),
				inputUnitOfMeasure = uiParent.find('.unit_of_measure_dropdown option:selected'),
				inputRadioPiece = uiParent.find('.radio-piece'),
				inputRadioBulk = uiParent.find('.radio-bulk'),
				sUnitOfMeasure = '';

				sUnitOfMeasure = inputRadioBulk.prop('checked') == true ? inputUnitOfMeasure.text() : 'Bags';
				
				uiTemplate = $(oParams.data);
				uiTemplate.find('.batch-name-display').html(batch_details.batch_name);
				uiTemplate.find('.batch-quantity-display').html('Quantity ' + batch_details.quantity + ' ' + sUnitOfMeasure);
				
				var sOriginTemplate = '',
				sDestinationTemplate = '',
				oOrigin = batch_details.origin,
				iOriginLength = Object.keys(oOrigin).length,
				oDestination = batch_details.destination,
				iDestinationLength = Object.keys(oDestination).length;

				for(var a in oOrigin) {
					var iChecker = parseInt(a) + parseInt(1);
					if(iOriginLength == iChecker) {
						sOriginTemplate += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oOrigin[a].name + '</p>';
					} else {
						sOriginTemplate += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oOrigin[a].name + '</p>'
						+ '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>';	
					}
				}

				for(var b in oDestination) {
					var iChecker = parseInt(b) + parseInt(1);
					if(iDestinationLength == iChecker) {
						sDestinationTemplate += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oDestination[b].name + '</p>';
					} else {
						sDestinationTemplate += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oDestination[b].name + '</p>'
						+ '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>';	
					}
				}
				
				uiTemplate.find('.batch-origin').html(sOriginTemplate);
				uiTemplate.find('.batch-destination').html(sDestinationTemplate);		
				uiTemplate.attr("data-parent", parent_id);
				uiBatchList.append(uiTemplate);

				var lastTemplate = $(".product-location", uiBatchList).last(),
				btnEditBatch = uiParent.find('.btn-edit-batch'),
				btnRemoveBatch = uiParent.find('.btn-remove-batch');
				lastTemplate.data(oParams.batch_details);

				btnRemoveBatch.off('click').on('click', function() {
					var uiParentBatch = $(this).closest('.product-location');
					uiParentBatch.remove();
				});

				btnEditBatch.off('click').on('click', function() {
					alert('edit batch');
					//show modal
					//get batch details
					//place details on modal.
				});


			},
			"unit_of_measure_dropdown" : function(oData) {

				var uiSelect = $(".unit_of_measure_dropdown");
				var uiSiblings = uiSelect.siblings();

				uiSiblings.remove();
				uiSelect.removeClass("frm-custom-dropdown-origin");
				uiSelect.html("");
				uiSelect.html(oElementData);
				uiSelect.transformDD();
				CDropDownRetract.retractDropdown(uiSelect);


			},
			// COMPANY ONGOING PAGE.
			"company_ongoing" : function(oData) {

				var uiModeOfTransfer = $('.ongoing-mode-of-transfer'),
				uiReasonForTransfer = $('.ongoing-reason-for-transfer'),
				uiDateIssued = $('.ongoing-date-issued'),
				uiRequestedBy = $('ongoing-requested-by'),
				uiStatus = $('.ongoing-status'),
				uiDuration = $('.ongoing-transfer-duration'),
				uiDateStart = $('.ongoing-transfer-start'),
				uiDateComplete = $('.ongoing-transfer-complete'),
				uiWorkerList = $('.ongoing-worker-list'),
				uiEquipmentUsed = $('.ongoing-equipment-used'),
				uiProductList = $("#ongoing_product_list"),
				uiProductTemplate = $('.ongoing-product-clone-template'),
				uiBreadCrumb = $('.ongoing-transfer-number'),
				modalAddBatch = $('#add_batch_modal'),
				btnCloseModal = $('.close-add-batch-modal'),
				oCurrentProductDetails = {},
				sModeOfTransfer = '';
				//oProducts = cr8v_platform.localStorage.get_local_storage({name : "productList"});


				for(var a in oMODEOFTRANSFERLIST) {
					//if(oMODEOFTRANSFERLIST[a].id == oElementData.mode_of_transfer_id) {
					if(oMODEOFTRANSFERLIST[a].id == oCurrentTransfer.mode_of_transfer_id) {	
						sModeOfTransfer = oMODEOFTRANSFERLIST[a].name;
					}
				}

				uiReasonForTransfer.html(oElementData.reason_for_transfer);
				uiDateIssued.html(oElementData.date_issued);
				uiModeOfTransfer.html(sModeOfTransfer);

				//console.log(oTRANSFERPRODUCTS);

				_getLocations(function(oResult) {
					oLocations = oResult;

					for(var a in oTRANSFERPRODUCTS) {

						// GETS THE DETAIL FOR THE CURRENT PRODUCT.
						for(var b in oProducts) {
							if(oTRANSFERPRODUCTS[a].product_id == oProducts[b].id) {
								oCurrentProductDetails = oProducts[b];
							}
						}

						var uiProductTemplateClone = uiProductTemplate.clone(),
						oProductBatches = oTRANSFERPRODUCTS[a].batches,
						uiProductSkuAndName = uiProductTemplateClone.find('.product-sku-name'),
						btnAddBatch = uiProductTemplateClone.find('.transfer-add-batch'),
						uiProductVendor = uiProductTemplateClone.find('.product-vendor'),
						uiProductTransferMethod = uiProductTemplateClone.find('.product-transfer-method'),
						uiProductDescription = uiProductTemplateClone.find('.product-description'),
						uiProductQtyToTransfer = uiProductTemplateClone.find('.product-qty-to-transfer'),
						uiTotalQtyToTransfer = uiProductTemplateClone.find('.total-qty-to-transfer'),
						uiProductImage = uiProductTemplateClone.find('.product-image'),
						inputRadioBulk = uiProductTemplateClone.find('.radio-bulk'),
						inputRadioPiece = uiProductTemplateClone.find('.radio-piece'),
						iTotalQtyToTransfer = 0,
						sTransferMethod = oTRANSFERPRODUCTS[a].transfer_method == 1 ? 'By Bulk' : 'By Piece',
						sUnitOfMEasureList = '';

						for(var c in oUNITOFMEASURELIST) {
							if(oUNITOFMEASURELIST[c].name != 'bag') {
								sUnitOfMEasureList += '<option value="' + oUNITOFMEASURELIST[c].id + '">' + oUNITOFMEASURELIST[c].name + '</option>'
							}
						}

						var uiSelect = uiProductTemplateClone.find('.unit_of_measure_dropdown');
						uiSiblings = uiProductTemplateClone.find(".unit-remove-dropdown").find(".frm-custom-dropdown");
						uiSiblings.remove();
						uiSelect.append(sUnitOfMEasureList);
						uiSelect.show().css({
							"padding" : "3px 4px",
							"width" :  "50px"
						});

						if(oTRANSFERPRODUCTS[a].transfer_method == 1) {
							inputRadioBulk.prop('checked', true);
							inputRadioBulk.attr('name', 'bag-or-bulk-' + oTRANSFERPRODUCTS[a].product_id);
							inputRadioPiece.attr('name', 'bag-or-bulk-' + oTRANSFERPRODUCTS[a].product_id);
						} else {
							inputRadioPiece.prop('checked', true);
							inputRadioPiece.attr('name', 'bag-or-bulk-' + oTRANSFERPRODUCTS[a].product_id);
							inputRadioBulk.attr('name', 'bag-or-bulk-' + oTRANSFERPRODUCTS[a].product_id);
						}


						uiProductTemplateClone.removeClass('ongoing-product-clone-template');
						uiProductTemplateClone.addClass('ongoing-product-template');
						uiProductTemplateClone.addClass('product-list-div');
						uiProductTemplateClone.data(oCurrentProductDetails);
						uiProductTemplateClone.data('product_map_details', oTRANSFERPRODUCTS[a]);
						uiProductTemplateClone.attr('data-id', oCurrentProductDetails.id);
						uiProductSkuAndName.html('SKU #' + oCurrentProductDetails.sku + ' - ' + oCurrentProductDetails.name),
						uiProductImage.html('<img src="' + oCurrentProductDetails.image + '" alt="" class="height-100percent" style="width:190px;">');
						uiProductDescription.html(oCurrentProductDetails.description);
						btnAddBatch.data('product_id', oTRANSFERPRODUCTS[a].product_id);
						uiProductTransferMethod.html(sTransferMethod);


						var uiBatchList = uiProductTemplateClone.find('.product-batch-list'),
						uiBatchTemplate = uiBatchList.find('.ongoing-batch-template'),
						uiBatchCloned = {};

						// LOOP THROUGH THE PRODUCT'S TRANSFERS.
						for(var c in oProductBatches) {
							var oBuiltOrigin = _buildLocationBreadCrumb({id : oProductBatches[c].destination_container}),
							oBuiltDestination = _buildLocationBreadCrumb({id : oProductBatches[c].origin_container}),
							sTotalQtyToTransfer = parseFloat(oProductBatches[c].quantity);

							uiBatchCloned = uiBatchTemplate.clone();
							uiBatchCloned.find('.ongoing-batch-name').html(oProductBatches[c].batch_name);
							uiBatchCloned.find('.ongoing-quantity').html(sTotalQtyToTransfer.toFixed(2) + ' ' + oProductBatches[c].unit_of_measure_name);
							
							var sOriginTemplate = "",
							sDestinationTemplate = "",
							iDestinationLength = parseInt(Object.keys(oBuiltDestination).length) - parseInt(1),
							iOriginLength = parseInt(Object.keys(oBuiltOrigin).length) - parseInt(1),
							btnRemoveBatch = uiBatchCloned.find('.btn-remove-batch');


							//BUILDS THE HTML TEMPLATE OF THE BATCH'S ORIGIN.
							for(var d in oBuiltOrigin) {
								sOriginTemplate += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oBuiltOrigin[d].name + '</p>'
								if(iOriginLength != d) {
									sOriginTemplate += '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>';	
								}	
							}


							//BUILDS THE HTML TEMPLATE OF THE BATCH'S DESTINATION.
							for(var e in oBuiltDestination) {
								sDestinationTemplate += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oBuiltDestination[e].name + '</p>'
								if(iDestinationLength != e) {
									sDestinationTemplate += '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>';	
								} else {
									// HIDES THE GENERATING VIEW MODAL.
									uiGenerateViewModal.removeClass('showed');
								}
							}


							// DISPLAY IS REVERSED.....				
							uiBatchCloned.find('.batch-origin').html(sDestinationTemplate);
							uiBatchCloned.find('.batch-destination').html(sOriginTemplate);
							uiBatchCloned.data('destination',oBuiltOrigin);
							uiBatchCloned.data('origin', oBuiltDestination);
							uiBatchCloned.addClass('ongoing-batch-cloned');
							uiBatchCloned.removeClass('ongoing-batch-template');
							uiBatchCloned.show();
							uiBatchCloned.data('transfer_details', oProductBatches[c]);

							iTotalQtyToTransfer = parseFloat(iTotalQtyToTransfer) + parseFloat(oProductBatches[c].quantity);
							uiBatchList.append(uiBatchCloned);

							btnRemoveBatch.off('click').on('click', function() {
								var btnParent = $(this).closest('.ongoing-batch-cloned');

								btnParent.remove();
							});
						}


						uiProductTemplateClone.show();
						uiTotalQtyToTransfer.html(iTotalQtyToTransfer.toFixed(2) + ' ' + oProductBatches[c].unit_of_measure_name);
						uiProductList.append(uiProductTemplateClone);


						var btnEditProduct = $('.transfer-ongoing-btn'),
						btnCancelEditProduct = $('.transfer-hide-cancel');

						btnEditProduct.off('click').on('click', function() {
							if($(this).text() == 'Edit') {
								// alert('edit');
							} else {
								var iProductId = $(this).closest('.product-list-div').data('id');
								_saveBatchChanges({product_id : iProductId});
							}
							var uiParent = $(this).closest('.ongoing-product-template');
							$(this).text("Save Changes");

							uiParent.find(".transfer-hide-cancel").show(100);

							uiParent.find(".transfer-add-batch").show();
							uiParent.find(".ongoing-edit-hide").show()

							uiParent.find(".transfer-display-piece").hide(100)
							uiParent.find(".transfer-hide-piece").show(100);


							uiParent.find(".transfer-edit-information").attr("disabled", true);
							uiParent.find(".complete-transfer-btn").attr("disabled", true);

							uiParent.find('.qty-transfer').removeClass('width-20percent');
							uiParent.find('.qty-transfer').addClass('width-15percent');

							uiParent.find('.qty-to-transfer').removeClass('width-125px');
							uiParent.find('.qty-to-transfer').addClass('width-100px');

							//GET PRODUCT CURRENT DETAILS HERE.
							_getOngoingProductDetails();
						});

						btnCancelEditProduct.off("click").on("click", function() {
							var uiParent = $(this).closest('.ongoing-product-template');
							$(this).hide(100);
							
							uiParent.find(".transfer-ongoing-btn").text("Edit");

							uiParent.find(".transfer-add-batch").hide(100);
							uiParent.find(".ongoing-edit-hide").hide(100)


							uiParent.find(".transfer-display-piece").show(100)
							uiParent.find(".transfer-hide-piece").hide(100);

							uiParent.find(".transfer-edit-information").attr("disabled", false);
							uiParent.find(".complete-transfer-btn").attr("disabled", false);
						});

						btnAddBatch.off('click').on('click', function() {
							var thisButton = $(this),
							iProductId = thisButton.data('product_id');

							modalAddBatch.addClass('showed');
							modalAddBatch.data('product_id', iProductId);
							_getProductBatches(iProductId, function(oResult) {
								var oProductBatches = oResult,
								oData = {
									product_id : iProductId,
									product_batches : oProductBatches
								};

								_buildAddBatchModal(oData);
								_displayBatchStorages(oStorages);
							});
						});

						$('body').off('mouseover').on('mouseover', function() {
							uiUnitOfMeasure = $(".unit_of_measure_dropdown"),
							uiUnitRemoveDropdown = $(".unit-remove-dropdown").find(".frm-custom-dropdown");
							uiUnitRemoveDropdown.remove();
							uiUnitOfMeasure.show().css({
					        	"padding" : "3px 4px",
					        	"width" :  "70px"
					    	});
						});

					}

					var btnCompleteTransfer = $('.transfer_complete_transfer'),
					btnAddWorker = $("#btn_add_worker"),
					uiAddWorkerList = $("#add_worker_list"),
					btnAddEquipment = $("#btn_add_equipment"),
					uiAddEquipmentList = $("#add_equipment_list");

					// EVENT FOR TO START THE COMPLETE TRANSFER FUNCTION.
					btnCompleteTransfer.off("click").on("click", function() {
						var uiParentDiv = uiBreadCrumb,
						modalCompleteTransfer = $('.modal-complete-transfer'),
						btnShowTransferRecord = $('.show-transfer-record');

						modalCompleteTransfer.addClass('showed');
						modalCompleteTransfer.attr("data-id", uiParentDiv.data("id"));
						btnShowTransferRecord.data("id", uiParentDiv.data("id"));
					});

					// EVENT FOR ADDING A NEW WORKER.
					btnAddWorker.off('click').on('click', function() {
						var sTemplate = _buildTemplate({name : "add_worker"});

						uiAddWorkerList.append(sTemplate);

						var btnRemoveWorker = $(".btn-remove-worker");

						btnRemoveWorker.off("click").on("click", function() {
							var btnParent = $(this).closest('.add-worker-div');
							btnParent.remove();
						});
					});

					// EVENT FOR ADDING A NEW EQUIPMENT.
					btnAddEquipment.off('click').on('click', function() {
						var sTemplate = _buildTemplate({name : "add_equipment"});

						uiAddEquipmentList.append(sTemplate);

						var btnRemoveEquipment = $(".btn-remove-equipment");

						btnRemoveEquipment.off("click").on("click", function() {
							var btnParent = $(this).closest('.equipment-div');
							btnParent.remove();
						});
					});

					btnCloseModal.off('click').on('click', function() {
						modalAddBatch.removeClass('showed');
					});
				});	

			},
			"ongoing_productList" : function(oData) {

			},
			"transfer_document" : function(oData) {

				var uiDocumentDiv = $("#complete_document_div"),
				uiDocument = oParams.data;

				uiDocumentDiv.append(uiDocument);


			},
			"add_note" : function(oData) {

				var uiNoteList = $(".note-list"),
				sCurrentPath = window.location.href;

				uiNoteList.append(oElementData);

				if(sCurrentPath.indexOf("company_ongoing") > -1) {
					//var oCurrentTransfer = _getCurrentTransfer(),
					var oNoteDetails = {
						transfer_log_id : oCurrentTransfer.id,
						subject : oElementData.find('.note-subject').html(),
						note : oElementData.find('.note-text').html(),
						date_created : oElementData.find('.note-date').data('record'),
						is_deleted : 0,
						user_id : 1
					};

					var oOptions = {
						type : "POST",
						data : {note_details : oNoteDetails},
						// returnType : "json",
						url : BASEURL + 'transfers/add_note',
						success : function(oResult) {
							console.log(oResult);
						}
					}
					//console.log(oNoteDetails);
					cr8v_platform.CconnectionDetector.ajax(oOptions);	
				}	


			},
			// CREATES THE EDIT VIEW, BINDS NEW EVENTS.
			"ongoing_edit" : function(oData) {

				var uiBreadCrumb = $('.edit-breadcrumb'),
				uiTransferNumber = $('.edit-transfer-number'),
				inputRequestedBy = $('#edit_requested_by'),
				inputReason = $('#edit_reason'),
				inputModeOfTransfer = $('#edit_mode_of_transfer'),
				inputProductList = $('#edit_product_list_dropdown'),
				oProductList = oParams.product_list,
				oModeOfTransfer = oParams.mode_of_transfer,
				oTransferDetails = oParams.transfer_details,
				oCurrentTransferDetails = {},
				oTransferProducts = oTransferDetails.product_list,
				uiProductListDiv = $('.edit-product-list'),
				uiSmallProductListDiv = $('.edit-small-product-list'),
				sProductTemplate = $('.edit-product-original-template'),
				sSmallProductTemplate = $('.edit-small-product-template'),
				arrProducts = cr8v_platform.localStorage.get_local_storage({name : "productList"});

				_getTransferDetails(oTransferDetails.id, function(oResult) {
					oCurrentTransferDetails = oResult[0];
					oCurrentTransfer = oCurrentTransferDetails;

					_getTransferBatchList(oTransferDetails.id, function(oResult) {
						oTransferProducts = oResult;

						var iTransferProductLength = parseInt(Object.keys(oTransferProducts).length) - parseInt(1);

						for(var a in oTransferProducts) {
							var oClonedTemplate = sSmallProductTemplate.clone();

							// for(var b in arrProducts) {
							// 	if(arrProducts[b].id == oTransferProducts[a].product_id) {
							// 		var oCurrentProduct = arrProducts[b];
							// 	}
							// }

							for(var b in oProducts) {
								if(oProducts[b].id == oTransferProducts[a].product_id) {
									var oCurrentProduct = oProducts[b];
								}
							}

							var btnRemoveProduct = oClonedTemplate.find(".remove-product");

							oClonedTemplate.removeClass('edit-small-product-template');
							oClonedTemplate.addClass('edit-small-product-cloned');
							oClonedTemplate.addClass('product-list-div');
							oClonedTemplate.find('.small-product-sku-and-name').html('SKU# ' + oCurrentProduct.sku + ' - ' + oCurrentProduct.name);
							oClonedTemplate.find('.small-product-image-display').html('<img src="' + oCurrentProduct.image + '" alt="" class="height-100percent width-100percent">');
							oClonedTemplate.attr('data-id', oCurrentProduct.id);
							oClonedTemplate.data('transfer_product_map_id', oTransferProducts[a].map_id);
							oClonedTemplate.show();
							uiSmallProductListDiv.append(oClonedTemplate);


							btnRemoveProduct.off('click').on('click', function() {
								var oRemovedProducts = uiSmallProductListDiv.data('removed_product_list') == undefined ? {} : uiSmallProductListDiv.data('removed_product_list'),
								iRemovedProductCount = oRemovedProducts == {} ? 0 : Object.keys(oRemovedProducts).length,
								uiProductParent = $(this).closest('.edit-small-product-cloned'),
								iCounter = parseInt(iRemovedProductCount) + parseInt(1);
								
								// oRemovedProducts[ iCounter ] = {
								//	 transfer_product_map_id : uiProductParent.data('transfer_product_map_id')
								// };

								oRemovedProducts[iCounter] = {
									transfer_product_map_id : uiProductParent.data('transfer_product_map_id')
								};

								
								uiSmallProductListDiv.data('removed_product_list', oRemovedProducts);
								uiProductParent.remove();


								_checkSelectedProducts({
									element : $(".edit-product-master-list"),
									dropdown : $("#edit_product_list_dropdown")
								});
							});

							if(a == iTransferProductLength) {
								// HIDES THE GENERATING VIEW MODAL.
								uiGenerateViewModal.removeClass('showed');
							}
						}

						uiBreadCrumb.html('Edit Transfer No. ' + oCurrentTransferDetails.to_number);
						uiTransferNumber.html('Transfer No. ' + oCurrentTransferDetails.to_number);
						inputReason.val(oCurrentTransferDetails.reason_for_transfer);

						var uiTransferSiblings = inputModeOfTransfer.siblings();
						uiTransferSiblings.remove();
						inputModeOfTransfer.removeClass("frm-custom-dropdown-origin");
						inputModeOfTransfer.html("");
						inputModeOfTransfer.html(oParams.mode_of_transfer_dropdown);
						inputModeOfTransfer.transformDD();
						CDropDownRetract.retractDropdown(inputModeOfTransfer);

						var uiProductSiblings = inputProductList.siblings();
						uiProductSiblings.remove();
						inputProductList.removeClass("frm-custom-dropdown-origin");
						inputProductList.html("");
						inputProductList.html(oParams.product_dropdown);
						inputProductList.transformDD();
						CDropDownRetract.retractDropdown(inputProductList);

						_checkSelectedProducts({
							element : $(".edit-product-master-list"),
							dropdown : $("#edit_product_list_dropdown")
						});

					});

				});

			}
		};

		if (typeof (oElement[oParams.name]) == "function") 
		{
			oElement[oParams.name](oParams.name);
		}
		
	}

	function _getTransferRecord(iTransferLogId, callBack) 
	{
		/**
		* _getTransferRecord
		* @description Gets the transfer's details, it's products and their transfer batches
		* @dependencies 
		* @param N/A
		* @response N/A
		* @criticality N/A
		* @software_architect N/A
		* @developer Dru Moncatar
		* @method_id N/A
		*/

		var oOptions = {
			type : "GET",
			data : {transfer_log_id : iTransferLogId},
			returnType : "json",
			url : BASEURL + "transfers/get_transfer_record",
			success : function(oResult) {
				if(typeof callBack == 'function') {
					callBack(oResult.data);	
				}
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	function _saveBatchChanges(oParams) {
		var iProductId = oParams.product_id,
		oNewBatches = {},
		uiProductDiv = $('#ongoing_product_list').find('.product-list-div[data-id="' + iProductId + '"]'),
		uiBatchDiv = uiProductDiv.find('.product-batch-list'),
		uiNewBatches = uiBatchDiv.find('.new-batch'),
		iCounter = 0,
		oProductMapDetails = uiProductDiv.data('product_map_details'),
		iProductMapId = oProductMapDetails.id;

		if(uiNewBatches.length > 0) {
			uiNewBatches.each(function() {
				var oNewBatchDetails = $(this).data();

				oNewBatches[iCounter] = oNewBatchDetails
				++iCounter;
			});
		}

		for(var a in oNewBatches) {
			var oCurrentBatchDetails = oNewBatches[a].transfer_details,
			oData = {
				product_map_id : iProductMapId,
				batch_id  : oCurrentBatchDetails.batch_id,
				storage_location : oCurrentBatchDetails.destination_storage_id,
				quantity : oCurrentBatchDetails.quantity,
				unit_of_measure : oCurrentBatchDetails.unit_of_measure,
				destination_container : oCurrentBatchDetails.destination_location_id,
				origin_container : oCurrentBatchDetails.origin_location_id
			},
			oOptions = {
				type : "POST",
				data : {batch_details : oData},
				returnType : 'json',
				url : BASEURL + 'transfers/company_add_new_batch',
				success : function(oResult) {
					var oFeedback = {};

					if(oResult.status) {
						oFeedback['icon'] = successIcon;
						setTimeout(function() {
							window.location.href = BASEURL + 'transfers/company_ongoing';
						}, 1000);
					} else {
						oFeedback['icon'] = failIcon;
					}
					oFeedback['message'] = oResult.message;
					$('body').feedback(oFeedback);
				}
			}

			cr8v_platform.CconnectionDetector.ajax(oOptions);
			//console.log(oOptions);
		}
	}

	function _add() 
	{
		/**
		* _add
		* @description This function is used in adding new transfer record
		* @dependencies 
		* @param N/A
		* @response N/A
		* @criticality N/A
		* @software_architect N/A
		* @developer Dru Moncatar
		* @method_id N/A
		*/

		var uiNoteList = $("#company_create_note_list"),
		iCounter = 0,
		oNote = {},
		sNote = [],
		sNoteList = "",
		btnBulk = $("#bulk"),
		btnPiece = $("#piece"),
		inputPiece = $("#qty_to_transfer_PIECE"),
		inputKg = $("#qty_to_transfer_KG"),
		iUnitOfMeasure = $(".unit_of_measure_dropdown"),
		oDocuments = $(".uploaded_document"),
		oDocumentDetails = {},
		iDocumentCounter = 0,
		iProductCounter = 0,
		oProductList = {},
		oLocationList = {},
		iLocationCounter = 0,
		modalCreatedRecord = $('.modal-created-record'),
		sCreatedToNumber = modalCreatedRecord.find('.created_to_number'),
		uiCreatedModalMessage = modalCreatedRecord.find('.modal-created-record-message'),
		btnCloseModalCreatedRecord = $('.close-modal-created-record'),
		btnShowRecord = $('.btn-show-record'),
		iTransferLogId = null;

		//GET ALL ADDED NOTES.
		uiNoteList.find(".note_div").each(function() {
			var sSubject = $(this).find(".note_subject").html();
			var sText = $(this).find(".note_text").html();
			var sDate = $(this).find(".note_date").data("record");

			oNote = {
				subject : sSubject,
				note : sText,
				date_created : sDate
			};

			sNote[iCounter] = oNote;
			++iCounter;
		});

		//GET ALL PRODUCT ADDED FOR EACH RECORDS.
		uiProductDetailsDisplay.find(".product-list-div").each(function() {
			if($(this).data("id") !== undefined) {
				var thisProduct = $(this);
				oLocationList = {},
				uiParent = thisProduct.closest('.product-list-div');
				inputUnitOfMeasure  = uiParent.find('.unit_of_measure_dropdown option:selected'),
				uiPiece = uiParent.find('.radio-piece'),
				uiBulk = uiParent.find('.radio-bulk'),
				inputTransferMethod = null;

				if(uiBulk.prop("checked")) {
					inputTransferMethod = 1;
				} else {
					inputTransferMethod = 2;
				}

				//GET ALL PRODUCT LOCATION FOR EACH PRODUCT.
				thisProduct.find(".product-location").each(function() {
					var thisLocation = $(this);
					var oLocationData = thisLocation.data();

					if(thisLocation.data("parent") == thisProduct.data("id")) {
						oLocationList[ iLocationCounter ] = {
							quantity : oLocationData.quantity,
							batch_name : oLocationData.batch_name,
							storage_id : oLocationData.new_storage_location,
							location_id : oLocationData.new_location,
							unit_of_measure_id : uiPiece.prop('checked') ? 8 : inputUnitOfMeasure.val(),
							status : oLocationData.status,
							destination_container : oLocationData.destination_container,
							parent_product : thisLocation.data("parent"),
							old_batch_id : oLocationData.old_batch_id,
							storage_assignment_id : oLocationData.storage_assignment_id,
							old_location_id : oLocationData.old_location_id
						};	

						++iLocationCounter;
						oProductList[ thisProduct.data("id") ] = {
							id : thisProduct.data("id"), 
							origins : oLocationList, 
							transfer_method : inputTransferMethod, 
							unit_of_measure : inputUnitOfMeasure.val()
						};
					}	
				});

				++iProductCounter;
			}
		});

		//GET ALL ADDED DOCUMENTS.
		oDocuments.each(function() {
			oDocumentDetails[iDocumentCounter] = $(this).data()
			++iDocumentCounter;
		});

		var oPostData = {
			to_number : $(".company_create_transfer_number").html(),
			user_id : 1,
			status : 0,
			requested_by : $("#company_create_requested_by").val(),
			quantity : 100,
			reason_for_transfer : $("#company_create_reason").val(),
			mode_of_transfer_id : $("#company_create_mode_of_transfer").val(),
			notes : sNote.length > 0 ? sNote : 'null',
			upload_documents : JSON.stringify(oDocumentDetails) != "{}" ? oDocumentDetails : 'null',
			products : JSON.stringify(oProductList) != '{}' ? oProductList : 'null'
		};

		var oOptions = {
			type : "POST",
			data : oPostData,
			url : BASEURL + "transfers/add_company_transfer",
			returnType : "json",
			success : function(oResult) {
				if(oResult.status) {
					// var oFeedback = {
					// 	title : "Success!",
					// 	message : oResult.message,
					// 	icon : successIcon
					// };

					// $("body").feedback(oFeedback);
					// setTimeout(function() {
					// 	window.location.href = BASEURL + "transfers/company_create";
					// }, 2000);
					// alert(oResult.data);
					iTransferLogId = oResult.data;
					modalCreatedRecord.addClass('showed');
					sCreatedToNumber.html($(".company_create_transfer_number").html());
				} else {
					var oFeedback = {
						title : "Failed!",
						message : oResult.message,
						icon : failIcon
					};

					$("body").feedback(oFeedback);
				}
			}
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
		// console.log(JSON.stringify(oOptions));	

		btnCloseModalCreatedRecord.off('click').on('click', function() {
			modalCreatedRecord.removeClass('showed');
			setTimeout(function() {
				window.location.href = BASEURL + "transfers/company_create";
			}, 800);
		});

		btnShowRecord.off('click').on('click', function() {
			// var oData = {
			// 	name : 'currentTransferCompanyGoods',
			// 	data : {id : iTransferLogId}
			// }
			_getTransfersList(function(oResult) {
				uiCreatedModalMessage.html('Redirecting...');
				oTransferList = oResult;
				_setCurrentTransfer(iTransferLogId);

				setTimeout(function() {
					window.location.href = BASEURL + "transfers/company_ongoing";
				}, 800);
			});
			
		});
	}

	function _edit() {
		/**
		* _edit
		* @description This function is used in updating a chosen transfer record
		* @dependencies 
		* @param N/A
		* @response N/A
		* @criticality N/A
		* @software_architect N/A
		* @developer Dru Moncatar
		* @method_id N/A
		*/
		// var oCurrentTransfer = _getCurrentTransfer();
		var inputReason = $('#edit_reason'),
		inputModeOfTransfer = $("#edit_mode_of_transfer option:selected"),
		inputRequestedBy = $('#edit_reason'),
		uiSmallProductListDiv = $('.edit-small-product-list'),
		uiProductListDiv = $('.edit-product-list'),
		oOldProductList = uiSmallProductListDiv.data('old_product_list'),
		oRemovedProducts = uiSmallProductListDiv.data('removed_product_list') == undefined ? 'undefined' : uiSmallProductListDiv.data('removed_product_list'),
		oNewProductList = {},
		oEditDetails = {};

		// LOOPS THROUGH ALL PRODUCT OF THE TRANSFER.
		var iNewProductCount = 0;
		uiProductListDiv.find('.product-list-div').each(function() {
			var thisProduct = $(this),
			oProductBatches = {},
			iBatchesCount = 0;


			// LOOPS THROUGH ALL BATCHES OF THE PRODUCT.
			thisProduct.find('.product-batch-cloned').each(function() {
				var thisBatch = $(this),
				oBatchDetails = thisBatch.data('batch_details'),
				oOrigin = thisBatch.data('origin'),
				oDestination = thisBatch.data('destination'),
				iDestinationLength = parseInt(Object.keys(oDestination).length) - parseInt(1),
				iOriginLength = parseInt(Object.keys(oOrigin).length) - parseInt(1);

				oProductBatches[ iBatchesCount ] = {
					origin : oOrigin,
					destination : oDestination,
					batch_name : oBatchDetails.batch_name,
					batch_id : oBatchDetails.batch_id,
					new_location : oDestination[ iDestinationLength ].id,
					new_storage : oDestination[ 0 ].id,
					old_location : oOrigin[ iOriginLength ].id,
					old_storage : oOrigin[ 0 ].id,
					quantity : thisBatch.data('converted_qty'),
					storage_assignment_id : oBatchDetails.storage_assignment_id,
					unit_of_measure : thisBatch.data('unit_of_measure_id')
				};
				
				++iBatchesCount;
			});

			oNewProductList[iNewProductCount] = {
				product_id : thisProduct.data('id'),
				batches : oProductBatches,
				transfer_method : thisProduct.data('transfer_method')
			};

			++iNewProductCount;
		});

		console.log(oCurrentTransfer);
		
		oEditDetails = {
			removed_products : oRemovedProducts,
			new_products : Object.keys(oNewProductList).length > 0 ? oNewProductList : 'undefined',
			reason_for_transfer : inputReason.val(),
			requested_by : inputRequestedBy.val(),
			mode_of_transfer : inputModeOfTransfer.val(),
			transfer_log_id : oCurrentTransfer.id
		};


		var oOptions = {
			type : "POST",
			data : {edit_details : oEditDetails},
			returnType : 'json',
			url : BASEURL + 'transfers/company_transfer_edit',
			success : function(oResult) {
				var oFeedback = '';
				
				if(oResult.status) {
					oFeedback = {title : 'Success', icon : successIcon, message : oResult.message};
					$('body').feedback(oFeedback);
					setTimeout(function() {
						window.location.href = BASEURL + 'transfers/company_ongoing';
					}, 2000);
				} else {
					 oFeedback = {title : 'Fail', icon : failIcon, message : oResult.message};
					 $('body').feedback(oFeedback);
				}
				
			}
		}
		
		//console.log(JSON.stringify(oOptions));
		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	function _addProduct(oParams) 
	{
		var oProductToDisplay = {},
		sClonedTemplate = oParams.sClonedTemplate,
		iSelectedProduct = oParams.iSelectedProduct,

		oProducts = cr8v_platform.localStorage.get_local_storage({name : "productList"});
		for(var a in oProducts) {
			if(oProducts[a].id == iSelectedProduct.val()) {
				oProductToDisplay = oProducts[a];
			}
		}

		var renderDetails = {
			name : "company_create_product_display",
			data : {
				product_details : oProductToDisplay,
				cloned_template : sClonedTemplate
			}
		};
		
		_render(renderDetails);

	}

	function _getOngoingProductDetails(oParams) {
		//DISPLAYING OF ONGOING PRODUCT DETAILS
	}

	function _setCurrentTransfer(iTransfer_id) {
		var oCurrentTransfer = {},
		oData = {
			transfer_log_id : iTransfer_id
		};

		// for(var a in oTransferList) {
		// 	if(oTransferList[a].id == iTransfer_id) {
		// 		oCurrentTransfer = oTransferList[a];
		// 	}
		// }

		cr8v_platform.localStorage.delete_local_storage({name : "currentTransferCompanyGoods"});
		// cr8v_platform.localStorage.set_local_storage({name : "currentTransferCompanyGoods", data : oCurrentTransfer});
		cr8v_platform.localStorage.set_local_storage({name : "currentTransferCompanyGoods", data : oData});
	}

	function _getCurrentTransfer() {
		var oCurrentTransfer = {};

		oCurrentTransfer = cr8v_platform.localStorage.get_local_storage({name : "currentTransferCompanyGoods"});
		return oCurrentTransfer;
	}

	function _getTransferNumber() {
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "transfers/generate_transfer_number",
			success : function(oResult) {
				var sTranferNumber = oResult.data.toString(),
				sNewTransferNumber = sTranferNumber.toString(),
				sCurrentTransferNumber = null,
				iToLimit = 9;

				while(sNewTransferNumber.length < iToLimit) {
					sNewTransferNumber = "0" + sNewTransferNumber.toString();
				}
				_render({name : "company_create_transfer_number", data : sNewTransferNumber});
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getModeOfTransfer() {
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "transfers/get_mode_of_transfer",
			success : function(oResult) {
				_UI_companyCreateModeOfTransfer({name : "display", data : oResult.data});
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getCurrentDateTime() {
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "transfers/get_date_time",
			success : function(oResult) {
				_setCurrentDateTime(oResult.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _setCurrentDateTime(oCurrentDateTime) {
		var currentDateTime = oCurrentDateTime;
		var oData = {
			name : "current_date_time",
			data : oCurrentDateTime
		};

		cr8v_platform.localStorage.delete_local_storage({name : "current_date_time"});
		cr8v_platform.localStorage.set_local_storage(oData);

		_UI_companyCreateAddNote();
	}

	function _getAllTransferNote(iCompanyTranferId) {
		var oOptions = {
			type : "GET",
			data : {company_transfer_id : iCompanyTranferId},
			returnType : "json",
			url : BASEURL + "transfers/get_all_company_transfer_notes",
			success : function(oResult) {
				_UI_displayCompleteTransferNotes(oResult.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getUnitOfMeasures() {
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "transfers/get_all_unit_of_measures",
			success : function(oResult) {
				_UI_unitOfMeasureDropDown(oResult.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _documentEvents(bTemporary)
	{
		var btnUploadDocs = $('[modal-target="upload-documents"]'),
			transferLogId = 0;

		btnUploadDocs.off('click');

		var extraData = { "transfer_log_id" : transferLogId };

		if(bTemporary === true){
			extraData["temporary"] = true;
		}

		btnUploadDocs.cr8vFileUpload({
			url : BASEURL + "transfers/upload_document",
			accept : "pdf,csv",
			filename : "attachment",
			extraField : extraData,
			onSelected : function(bIsValid){
				if(bIsValid){
					//selected valid
				}
			},
			success : function(oResponse){
				if(oResponse.status){
					$('[cr8v-file-upload="modal"] .close-me').click();
					var oDocs = oResponse.data[0]["documents"];
					if(bTemporary){
						oUploadedDocuments[Object.keys(oUploadedDocuments).length] = oResponse.data[0]["documents"][0];
						_displayDocuments(oUploadedDocuments);
					}else{
						_displayDocuments(oDocs);
					}
				}
			},
			error : function(){
				$("body").feedback({title : "Message", message : "Was not able to upload, file maybe too large", type : "danger", icon : failIcon});
			}
		});
	}

	function _checkSelectedProducts(oParams) {
		/* USAGE
		*	
		*	_checkSelectedProducts({
		*  		element : $("#company_create_product_details"),
		*  		dropdown : $("#company_create_product_dropdown")
		*	});
		*
		*/

		var uiParent = oParams.element,
		uiDropDown = oParams.dropdown,
		arrProducts = [],
		sNewOptions = '',
		sAllProducts = '',
		iCounter = 0,
		oProductList = cr8v_platform.localStorage.get_local_storage({name : 'productList'});

		//FINDS ALL THE SELECTED PRODUCTS AND STORES THEM INTO AN ARRAY.
		uiParent.find('.product-list-div').each(function() {	
			if($(this).data("id") != undefined) {
				arrProducts[ iCounter ] = {product_id : $(this).data("id")};
				++iCounter;
			}
		});
		
		//BUILDS THE OPTION WITH ALL THE PRODUCTS.
		for(var b in oProductList) {
			sAllProducts += '<option value="' + oProductList[b].id + '">' + oProductList[b].name + '</option>'
		}

		// SETS THE NEW OPTION TO THE SELECT ELEMENT.
		uiDropDown.html(sAllProducts);

		// REMOVES THE SELECTED PRODUCTS FROM THE PRODUCT DROPDOWN
		uiDropDown.find('option').each(function(index,element){
			for(var a in arrProducts) {
				if(arrProducts[a].product_id == $(this).val()) {
					$(this).remove();
				}
			}
		});
		
		// REMOVES THE SELECTED PRODUCTS FROM THE PRODUCT DROPDOWN
		// uiDdOptions.each(function() {
		// 	for(var a in arrProducts) {
		// 		if(arrProducts[a].product_id == $(this).val()) {
		// 			$(this).remove();
		// 		}
		// 	}	
		// });
	
		//UPDATES THE CUSTOM DROPDOWN OPTIONS.
		var uiSelect = uiDropDown;
		var uiSiblings = uiSelect.siblings();
		uiSiblings.remove();
		sNewOptions = uiSelect.html();
		uiSelect.removeClass("frm-custom-dropdown-origin");
		uiSelect.html("");
		uiSelect.html(sNewOptions);
		uiSelect.transformDD();
		CDropDownRetract.retractDropdown(uiSelect);
	}

	function _completeTransfer(oParams) {
		var sDateStart = $("#date_start"),
		sTimeStart = $("#time_start"),
		sDateEnd = $("#date_end"),
		sTimeEnd = $("#time_end"),
		uiWorkerList = $('.add_worker_list'),
		uiEquipmentList = $('.add_equipment_list'),
		uiWorkerDiv = $('.add-worker-div'),
		uiEquipmentDiv = $(".equipment-div"),
		iWorkerCounter = 0,
		iEquipmentCounter = 0,
		oWorkers = [],
		oEquipment = [],
		iTransferId = oParams.id,
		sPageUrl = oParams.page;
		// uiCompleteTransfer = $('.modal-complete-transfer');

		var sTransferStart = sDateStart.val() != "" && sTimeStart.val() != "" ? sDateStart.val() + ' ' + sTimeStart.val() : 'null';
		var sTransferEnd = sDateEnd.val() != "" && sTimeEnd.val() != "" ? sDateEnd.val() + ' ' + sTimeEnd.val() : 'null';

		uiWorkerDiv.each(function() {
			oWorkers[ iWorkerCounter ] = {
				name : $(this).find('.worker-name').val(),
				worker_type : $(this).find('.worker-designation').val()
			};
			++iWorkerCounter;
		});

		uiEquipmentDiv.each(function() {
			oEquipment[ iEquipmentCounter ] = {
				name : $(this).find('.equipment-name').val()
			}
			++iEquipmentCounter;
		});

		var oPostData = {
			// transfer_start : sDateStart.val() + ' ' + sTimeStart.val(),
			transfer_start : sTransferStart,
			// transfer_end : sDateEnd.val() + ' ' + sTimeEnd.val(),
			transfer_end : sTransferEnd,
			transfer_log_id : iTransferId,
			workers : oWorkers,
			equipments : oEquipment
		};

		var oOptions = {
			type : "POST",
			data : oPostData,
			returntype : "json",
			url : BASEURL + "transfers/complete_transfer",
			success : function(oResult) {
				// console.log(JSON.stringify(oResult.data));
				if(oResult.status) {
					var oFeedback = {
						icon : successIcon,
						message : oResult.message,
						speed : 'mid',
						title : 'Success!'
					};

					$('body').feedback(oFeedback);
					$('.modal-complete-transfer').removeClass('showed');
					
					setTimeout(function(){
						window.location.href = BASEURL + 'transfers/' + sPageUrl;	
					}, 3000);
				} else {
					var oFeedback = {
						icon : failIcon,
						message : oResult.message,
						speed : 'mid',
						title : 'Fail!'
					};
					$('body').feedback(oFeedback);
				}		
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
		//console.log(oOptions);
	}

	function _displayDocuments(oDocs)
	{
		var bChangeBg = false,
			iCountDocument = 0;

		var uiContainer = $("#displayDocuments"),
			sTemplate = '<div class="table-content position-rel tbl-dark-color uploaded_document">'+
							'<div class="content-show padding-all-10">'+
								'<div class="width-85per display-inline-mid padding-left-10">'+
									'<i class="fa font-30 display-inline-mid width-50px icon"></i>'+
									'<p class=" display-inline-mid doc-name">Document Name</p>'+
								'</div>'+
								'<p class=" display-inline-mid date-time"></p>'+
							'</div>'+
							'<div class="content-hide" style="height: 50px;">'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30">View</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn">Download</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30">Print</button>'+
								'</a>'+
							'</div>'+
						'</div>';

		uiContainer.html("");
		for(var i in oDocs){
			var uiList = $(sTemplate),
				ext = (oDocs[i]["document_path"].substr(oDocs[i]["document_path"].lastIndexOf('.') + 1)).toLowerCase();

			if(ext == 'csv'){
				uiList.find(".icon").addClass('fa-file-excel-o');
			}else if(ext == 'pdf'){
				uiList.find(".icon").addClass('fa-file-pdf-o');
			}

			uiList.data(oDocs[i]);
			uiList.find(".doc-name").html(oDocs[i]["document_name"]);
			uiList.find(".date-time").html(oDocs[i]["date_added_formatted"]);
			if(bChangeBg){
				uiList.removeClass('tbl-dark-color');
				bChangeBg = false;
			}else{
				bChangeBg = true;
			}

			uiContainer.append(uiList);
		}

		cr8v_platform.localStorage.set_local_storage({
			name : "dataUploadedDocument",
			data : oDocs
		});
	}

	function _generateRandomName()
    {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for( var i=0; i < 5; i++ )
		  text += possible.charAt(Math.floor(Math.random() * possible.length));

		return text;
 	}

 	function _getAllWorkerType(oParams) {
 		var oOptions = {
 			type : "GET",
 			returnType : "json",
 			data : {test : true},
 			url : BASEURL + "transfers/get_all_worker_type",
 			success : function(oResult) {
 				// console.log(oResult.data);
 				// oWorkerTypes = oResult.data;
 				_setAllWorkerType(oResult.data);
 			}
 		}

 		cr8v_platform.CconnectionDetector.ajax(oOptions);
 	}

 	function _getWorkerTypeList(callBack) {
 		var oOptions = {
 			type : 'GET',
 			returnType : 'json',
 			data : {test : true},
 			url : BASEURL + 'transfers/get_all_worker_type',
 			success : function(oResult) {
 				if(typeof(callBack == 'function')) {
 					callBack(oResult.data);
 				}
 			}
 		}

 		cr8v_platform.CconnectionDetector.ajax(oOptions);
 	}

 	function _setAllWorkerType(oData) {
 		var oLocalData = {
 			name : "worker_types",
 			data : oData
 		},
 		oWorkerTypes = oData;

 		cr8v_platform.localStorage.delete_local_storage({name : 'worker_types'});
 		cr8v_platform.localStorage.set_local_storage(oLocalData);
 		return oWorkerTypes;
 	}

 	function _getTransferBatches(iTransferLogId, callBack) 
 	{
 		var oOptions = {
 			type : "GET",
 			data : {transfer_log_id : iTransferLogId},
 			returnType : "json",
 			url : BASEURL + "transfers/get_transfer_batches",
 			success : function(oResult) {
 				if(typeof(callBack) == 'function') {
 					callBack(oResult.data);
 				}
 			}
 		}

 		cr8v_platform.CconnectionDetector.ajax(oOptions);
 	}

 	function _setTransferBatches(oParams) 
 	{
 		var oBatches = oParams.data,
 		oData = {
 			name : "current_transfer_batches",
 			data : oBatches
 		};

 		cr8v_platform.localStorage.delete_local_storage({name : "current_transfer_batches"});
 		cr8v_platform.localStorage.set_local_storage(oData);
 	}

 	function _getTransferWorkers(iTransferLogId, callBack) 
 	{
 		var oOptions = {
 			type : "GET",
 			data : {transfer_log_id : iTransferLogId},
 			returnType : "json",
 			url : BASEURL + "transfers/get_transfer_workers",
 			success : function(oResult) {
 				if(typeof(callBack) == 'function') {
 					callBack(oResult.data);
 				}
 			}
 		};

 		cr8v_platform.CconnectionDetector.ajax(oOptions);
 	}

 	function _setTransferWorkers(oParams) 
 	{
 		var oWorkers = oParams.data,
 		oData = {
 			name : "current_transfer_workers",
 			data : oWorkers
 		};

 		cr8v_platform.localStorage.delete_local_storage({name : "current_transfer_workers"});
 		cr8v_platform.localStorage.set_local_storage(oData);
 	}

 	function _getTransferEquipments(iTransferLogId, callBack) {
 		var oOptions = {
 			type : "GET",
 			data : {transfer_log_id : iTransferLogId},
 			returnType : "json",
 			url : BASEURL + "transfers/get_transfer_equipments",
 			success : function(oResult) {
 				if(typeof(callBack) == 'function') {
 					callBack(oResult.data);
 				}
 			}
 		};

 		cr8v_platform.CconnectionDetector.ajax(oOptions);
 	}

 	function _setTransferEquipments(oParams) {
 		var oEquipments = oParams.data,
 		oData = {
 			name : "current_transfer_equipments",
 			data : oEquipments
 		};

 		cr8v_platform.localStorage.delete_local_storage({name : "current_transfer_equipments"});
 		cr8v_platform.localStorage.set_local_storage(oData);
 	}

 	function _getTransferDocuments(oParams) {
 		var oOptions = {
 			type : "GET",
 			data : {transfer_log_id : oParams.id},
 			returnType : "json",
 			url : BASEURL + "transfers/get_transfer_documents",
 			success : function(oResult) {
 				// console.log(oResult.data);
 				_UI_transferDocuments({data : oResult.data});
 			}
 		}

 		cr8v_platform.CconnectionDetector.ajax(oOptions);
 	}

 	function _addNote() {
 		var sTemplate = _buildTemplate({name : "add_note"}),
 		sNoteSubject = $("#note_subject"),
 		sNoteMessage = $("#note_message"),
 		sCurrentDateTime = {},
 		oTemplate = null,
 		uiModal = $('.modal-add-note');

 		// if(cr8v_platform.localStorage.get_local_storage({name : "current_date_time"}) == undefined) {
 			
 		// }

 		_getCurrentDateTime();
 		sCurrentDateTime = cr8v_platform.localStorage.get_local_storage({name : "current_date_time"});
 		
 		oTemplate = $(sTemplate);
 		oTemplate.find('.note-subject').html(sNoteSubject.val());
 		oTemplate.find('.note-text').html(sNoteMessage.val());
 		oTemplate.find('.note-date').attr("data-record", sCurrentDateTime.to_record);
 		oTemplate.find('.note-date').html('Date/Time: ' + sCurrentDateTime.to_display);
 		
 		var oRenderDetails = {
 			name : "add_note",
 			data : oTemplate
 		};
 		
		uiModal.removeClass('showed');
		$('body').removeAttr("style");
 		_render(oRenderDetails);
 	}

 	function _showTransferRecord(iCurrentTransferId) {	
		$.when( _setCurrentTransfer(iCurrentTransferId) ).done(function() {
	       window.open(BASEURL + 'transfers/company_ongoing', "windowName", "height=800,width=1200");
	 	});
	}

	function _getEditTransfer() {
	 	var oEditTransfer = cr8v_platform.localStorage.get_local_storage({name : "edit_transfer"});

	 	return oEditTransfer;
	}

	function _getAllModeOfTransfer() {
	 	var oOptions = {
	 		type : "GET",
	 		data : {test : true},
	 		returnType : "json",
	 		url : BASEURL + "transfers/get_mode_of_transfer",
	 		success : function(oResult) {
	 			var oData = {
					name : "mode_of_transfer",
					data : oResult.data
				};

				cr8v_platform.localStorage.delete_local_storage({name : "mode_of_transfer"});
				cr8v_platform.localStorage.set_local_storage(oData);
	 		}
	 	}

	 	cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	//==================================================//
	//=============ADD BATCH MODAL FUNCTIONS============//
	//==================================================//
	function _buildAddBatchModal(oParams) 
	{
		var iProductId = oParams.product_id,
		iCurrentProductId = $('#add_batch_modal').data('id'),
		oProductDetails = {},
		oProductBatches = oParams.product_batches,
		oBatchOptions = _buildBatchDropDown(oProductBatches),
		oQuantityToTransfer = {},
		uiModal = $('#add_batch_modal'),
		uiSelect = $('#add_batch_name'),
		uiSiblings = uiSelect.siblings(),
		uiProductSkuAndName = $('#add_product_sku_name'),
		uiOriginName = $('.batch-storage-name'),
		uiProductDiv = $('#ongoing_product_list').find('.product-list-div[data-id="'+ iCurrentProductId +'"]'),
		inputRadioPiece = uiProductDiv.find('.radio-piece'),
		inputRadioBulk = uiProductDiv.find('.radio-bulk'),
		inputUnitOfMeasure = uiProductDiv.find('.unit-of-meausure option:selected'),
		btnSubmitAddBatch = $('#company_create_submit_add_batch');

		// LOOP FOR GETTING THE CURRENT PRODUCT'S DETAILS.
		for(var a in oProducts) {
			if(oProducts[a].id == iProductId) {
				oProductDetails = oProducts[a];
			}
		}

		// INSERT THE BATCHES INTO THE SELECT TAG.
		uiSiblings.remove();
		uiSelect.removeClass("frm-custom-dropdown-origin");
		uiSelect.html("");
		uiSelect.html(oBatchOptions);
		uiSelect.transformDD();
		CDropDownRetract.retractDropdown(uiSelect);

		uiProductSkuAndName.html('SKU No. ' + oProductDetails.sku + ' - ' + oProductDetails.name);

		//==================================================//
		//=================ADD BATCH EVENTS=================//
		//==================================================//

		// SELECT BATCH EVENT.
		uiSelect.off('change').on('change', function() {
			var uiSelectedOption = $('#add_batch_name option:selected'),
			oBatchDetails = uiSelectedOption.data('batch_details'),
			iLocationId = oBatchDetails.location_id,
			oLocationBreadCrumb = {},
			oData = {
				batch_quantity : oBatchDetails.batch_quantity,
				unit_of_measure_name : oBatchDetails.unit_of_measure,
				unit_of_measure_id : oBatchDetails.unit_of_measure_id
			},
			oConvertionDetails = {
				batch_details : oBatchDetails,
				product_id : iProductId
			};

			uiOriginName.html(oBatchDetails.storage_name);
			oLocationBreadCrumb = _buildLocationBreadCrumb({id : iLocationId});
			oData['location_breadcrumb'] = oLocationBreadCrumb;
			_displayModalLocationBreadCrumbs(oData);
			oQuantityToTransfer = _displayModalQty(oConvertionDetails);

			uiModal.data('quantity_to_transfer', oQuantityToTransfer);
			uiModal.data('origin_storage', oBatchDetails.storage_id_number);
			uiModal.data('origin_location', oBatchDetails.location_id);
			uiModal.data('batch_name', oBatchDetails.batch_name);
			uiModal.data('batch_id', oBatchDetails.batch_id);
		});

		// SUBMIT BATCH EVENT. 
		btnSubmitAddBatch.off('click').on('click',function() {
			var oBatchTransfer = uiModal.data(),
			oDestination = {},
			oData = {
				transfer_details : oBatchTransfer
			};
			
			// oDestination = _getBreadCrumbsHierarchy();
			// oData['destination_breadcrumb'] = oDestination;
			_displayBatchTransfer(oData);
		});

		//==================================================//
		//===============END ADD BATCH EVENTS===============//
		//==================================================//
	}

	function _buildBatchDropDown(oProductBatches) 
	{
		var sBatchOptions = '',
		oBatchOptions = null;

		for(var a in oProductBatches) {
			sBatchOptions += '<option value="' + oProductBatches[a].batch_id + '">' + oProductBatches[a].batch_name + '</option>';
		}

		oBatchOptions = $(sBatchOptions);
		
		// ITERATE THROUGH THE OPTIONS AND BIND THEIR RESPECTIVE BATCH INFORMATION.
		oBatchOptions.each(function() {
			for(var b in oProductBatches) {
				if(oProductBatches[b].batch_id == $(this).val()) {
					$(this).data('batch_details', oProductBatches[b]);
				}
			}
		});

		return oBatchOptions;
	}

	function _displayModalLocationBreadCrumbs(oParams) 
	{
		var oLocationBreadCrumb = oParams.location_breadcrumb,
		uiParent = $('#sub_location_batch_list'),
		uiBatchLocation = uiParent.find('.location-breadcrumb'),
		uiBatchQty = uiParent.find('.batch-quantity'),
		iLocationLength = parseInt(Object.keys(oLocationBreadCrumb).length) - parseInt(1),
		sLocation = '',
		sQuantity = '',
		sUnitOfMeasure = '';
	
		// LOOP FOR DISPLAYING THE LOCATION BREADCRUMB.
		for(var a in oLocationBreadCrumb) {
			sLocation += '<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> ' + oLocationBreadCrumb[a].name + '  </p>';

			if(a != iLocationLength) {
				sLocation += '<i class="display-inline-mid font-14 padding-right-10">&gt;</i>';
			}
		}

		uiBatchLocation.html(sLocation);
	}

	function _displayModalQty(oParams) 
	{
		var oBatch = oParams.batch_details,
		iProductId = oParams.product_id,
		iUnitOfMeasureId = null,
		iBagId = null,
		uiProductDiv = $('#ongoing_product_list').find('.product-list-div[data-id="' + iProductId + '"]'),
		inputRadioPiece = uiProductDiv.find('.radio-piece'),
		inputRadioBulk = uiProductDiv.find('.radio-bulk'),
		inputUnitOfMeasure = uiProductDiv.find('.unit_of_measure_dropdown option:selected'),
		sQuantity = '',
		sUnitOfMeasureName = '',
		sQuantityToTransfer = '',
		uiModal = $('#add_batch_modal'),
		uiBatchQuantity = uiModal.find('.batch-quantity'),
		oQuantityToTransfer = {};

		for(var a in oUNITOFMEASURELIST) {
			if(oUNITOFMEASURELIST[a].name == 'bag') {
				iBagId = oUNITOFMEASURELIST[a].id;
			}
		}

		if(inputRadioBulk.prop('checked') == true) {
			if(oBatch.unit_of_measure_id == inputUnitOfMeasure.val()) {
				sQuantity = oBatch.batch_quantity;
				sQuantity = parseFloat(sQuantity).toFixed(2);
				iUnitOfMeasureId = inputUnitOfMeasure.val();
				sUnitOfMeasureName = inputUnitOfMeasure.text();
			} else {
				sQuantity = UnitConverter.convert(oBatch.unit_of_measure, inputUnitOfMeasure.text(), oBatch.batch_quantity);
				sQuantity = parseFloat(sQuantity).toFixed(2);
				iUnitOfMeasureId = inputUnitOfMeasure.val();
				sUnitOfMeasureName = inputUnitOfMeasure.text();
			}
		} else {
			if(oBatch.unit_of_measure_id == iBagId) {
				sQuantity = oBatch.batch_quantity;
				sQuantity = parseFloat(sQuantity).toFixed(2);
				iUnitOfMeasureId = iBagId;
				sUnitOfMeasureName = 'bag';
			} else {
				sQuantity = UnitConverter.convert(oBatch.unit_of_measure, 'bag', oBatch.batch_quantity);
				sQuantity = parseFloat(sQuantity).toFixed(2);
				iUnitOfMeasureId = iBagId;
				sUnitOfMeasureName = inputUnitOfMeasure.text();
			}
		}

		sQuantityToTransfer = sQuantity + ' ' + sUnitOfMeasureName;
		uiBatchQuantity.html(sQuantityToTransfer);
		oQuantityToTransfer['quantity'] = sQuantity;
		oQuantityToTransfer['unit_of_measure'] = sUnitOfMeasureName;
		oQuantityToTransfer['unit_of_measure_id'] = iUnitOfMeasureId;
		return oQuantityToTransfer;
	}

	function _displayBatchTransfer(oParams)
	{
		// var oDestination = oParams.destination_breadcrumb,
		var oDestination = _getBreadCrumbsHierarchy();
		oOrigin = {},
		oTransfer = {},
		oTransferDetails = oParams.transfer_details,
		oQuantityToTransfer = oTransferDetails.quantity_to_transfer,
		iBatchId = oTransferDetails.batch_id,
		sBatchName = oTransferDetails.batch_name,
		sDestination = '',
		sOrigin = '',
		iOriginLocationId = oTransferDetails.origin_location,
		iOriginStorageId = oTransferDetails.origin_storage,
		iProductId = oTransferDetails.product_id,
		iDestinationLocationId = null,
		iDestinationStorageId = null,
		uiModal = $('#add_batch_modal'),
		uiProductList = $('#ongoing_product_list'),
		uiParent = uiProductList.find('.product-list-div[data-id="' + iProductId + '"]'),
		uiTemplate = uiParent.find('.ongoing-batch-template'),
		uiBatchList = uiParent.find('.product-batch-list'),
		uiClonedTemplate = null,
		oOrigin = _buildLocationBreadCrumb({id : iOriginLocationId});

		for(var x in oDestination) {
			var iCounter = parseInt(x) + parseInt(1);
			
			if(x == 0) {
				iDestinationStorageId = oDestination[x].id;
				oTransfer['destination_storage_id'] = iDestinationStorageId;
			}
			if(Object.keys(oDestination).length == iCounter) {
				iDestinationLocationId = oDestination[x].id;
				oTransfer['destination_location_id'] = iDestinationLocationId;
			}
		}

		for(var a in oDestination) {
			var iCounter = parseInt(a) + parseInt(1);

			sDestination += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oDestination[a].name + '</p>';
			if(Object.keys(oDestination).length != iCounter) {
				sDestination += '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>';
			}
		}

		for(var b in oOrigin) {
			var iCounter = parseInt(b) + parseInt(1);

			sOrigin += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oOrigin[b].name + '</p>';
			if(Object.keys(oOrigin).length != iCounter) {
				sOrigin += '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>';
			}
		}

		oTransfer['batch_id'] = iBatchId;
		oTransfer['quantity'] = oQuantityToTransfer.quantity;
		oTransfer['origin_location_id'] = iOriginLocationId;
		oTransfer['unit_of_measure'] = oQuantityToTransfer.unit_of_measure_id;
		uiClonedTemplate = uiTemplate.clone();
		uiClonedTemplate.removeClass('ongoing-batch-template');
		uiClonedTemplate.addClass('ongoing-batch-cloned');
		uiClonedTemplate.addClass('new-batch');
		uiClonedTemplate.find('.ongoing-batch-name').html(sBatchName);
		uiClonedTemplate.find('.ongoing-quantity').html(oQuantityToTransfer.quantity + ' ' + oQuantityToTransfer.unit_of_measure);
		uiClonedTemplate.find('.batch-origin').html(sOrigin);
		uiClonedTemplate.find('.batch-destination').html(sDestination);
		uiClonedTemplate.data('transfer_details', oTransfer);
		uiClonedTemplate.show();
		uiModal.removeClass('showed');

		var btnRemoveBatch = uiClonedTemplate.find('.btn-remove-batch');

		uiBatchList.append(uiClonedTemplate);

		btnRemoveBatch.off('click').on('click', function() {
			var btnParent = $(this).closest('.new-batch');

			btnParent.remove();
		});
	}
	//==================================================//
	//===========END ADD BATCH MODAL FUNCTIONS==========//
	//==================================================//

	function _addProductToList(oParams) {
		/**
		* _addProductToList
		* @description This function will process the selected product and place it in the DOM.
		* @dependencies 
		* @param N/A
		* @response N/A
		* @criticality N/A
		* @software_architect N/A
		* @developer Dru Moncatar
		* @method_id N/A
		*/

	 	var iProductId = oParams.product_id,
	 	uiProductListDiv = $('.' + oParams.div_class),
	 	uiOriginalTemplate = oParams.template_object,
	 	uiClonedTemplate = null,
	 	oProducts = cr8v_platform.localStorage.get_local_storage({name : "productList"}),
	 	oCurrentProduct = null;
	 	_getUnitOfMeasures();

	 	// GETS THE DETAILS FOR THE CURRENT PRODUCT.
	 	for(var a in oProducts) {
	 		if(oProducts[a].id == iProductId) {
	 			oCurrentProduct = oProducts[a];
	 		}
	 	}

	 	// CLEANS UP THE CLONED TEMPLATE.
	 	uiClonedTemplate = uiOriginalTemplate.clone();
	 	uiClonedTemplate.removeClass(oParams.template_class);
	 	uiClonedTemplate.addClass('product-cloned-template');
	 	uiClonedTemplate.addClass('product-list-div');
	 	uiClonedTemplate.attr('data-id', iProductId);
	 	uiClonedTemplate.find('.product-sku-name').html('SKU# ' + oCurrentProduct.sku + ' - ' + oCurrentProduct.name);
	 	uiClonedTemplate.find('.product-image').html('<img src="' + oCurrentProduct.image + '" alt="" class="height-100percent width-100percent">')
	 	uiClonedTemplate.show();
    	uiClonedTemplate.find('.unit_of_measure_dropdown').removeAttr('disabled');

	 	// CLONES THE BATCH TEMPLATE.
	 	var oProductBatchTemplate = uiClonedTemplate.find('.product-batch-template'), 
	 	btnRemoveProduct = uiClonedTemplate.find('.remove-product'),
	 	btnAddBatch = uiClonedTemplate.find('.btn-add-batch'),
	 	btnBulk = uiClonedTemplate.find(".radio-bulk"),
		btnPiece = uiClonedTemplate.find(".radio-piece"),
		uiBody = $('body');

	 	// INSERTS THE CLONED TEMPLATE INTO THE DOM.
	 	uiProductListDiv.append(uiClonedTemplate);

	 	// EVENT FOR TRANSFORMING THE DROPDOWN.
	 	uiBody.off('mouseover').on('mouseover', function() {
	 		uiUnitOfMeasure = $(".unit_of_measure_dropdown"),
			uiUnitRemoveDropdown = $(".unit-remove-dropdown").find(".frm-custom-dropdown");
			uiUnitRemoveDropdown.remove();
			uiUnitOfMeasure.show().css({
	        	"padding" : "3px 4px",
	        	"width" :  "70px"
	    	});
	 	});

	 	// EVENT FOR CHOOSING BULK AS TRANSFER METHOD.
	 	btnBulk.off("click").on("click", function() {
			// var uiParent = $(this).closest(".display-inline-top"),
			var uiParent = $(this).closest(".product-list-div"),
			uiQtyToTransferContainer = uiParent.find(".padding-all-15:nth-child(3)"),
			uiParentDiv = $(this).closest('.product-list-div'),
			siblingAddBatch = uiParentDiv.find('.btn_add_company_transfer_batch');

			if($(this).prop("checked")) {
				siblingAddBatch.removeClass("disabled");
			}

			uiUnitOfMeasure = $(".unit_of_measure_dropdown"),
			uiUnitRemoveDropdown = $(".unit-remove-dropdown").find(".frm-custom-dropdown");
			uiUnitRemoveDropdown.remove();
			uiUnitOfMeasure.show().css({
				"padding" : "3px 4px",
				"width" :  "70px"
			});
			uiParent.find('.unit_of_measure_dropdown').removeAttr('disabled');
			uiParent.data('transfer_method', 1);
		});

	 	// EVENT FOR CHOOSING PIECE AS TRANSFER METHOD.
		btnPiece.off("click").on("click", function() {
			// var uiParent = $(this).closest(".display-inline-top"),
			var uiParent = $(this).closest(".product-list-div"),
			uiQtyToTransferContainer = uiParent.find(".padding-all-15:nth-child(3)"),
			uiParentDiv = $(this).closest('.product-list-div'),
			siblingAddBatch = uiParentDiv.find('.btn_add_company_transfer_batch');

			if($(this).prop("checked")) {
				siblingAddBatch.removeClass("disabled");
			}

			uiUnitOfMeasure = $(".unit_of_measure_dropdown"),
			uiUnitRemoveDropdown = $(".unit-remove-dropdown").find(".frm-custom-dropdown");
			uiUnitRemoveDropdown.remove();
			uiUnitOfMeasure.show().css({
				"padding" : "3px 4px",
				"width" :  "70px"
			});
			uiParent.find('.unit_of_measure_dropdown').attr('disabled', true);
			uiParent.data('transfer_method', 2);
		});

		// EVENT FOR REMOVING PRODUCT ON THE LIST.
		btnRemoveProduct.off('click').on('click', function() {
			var thisParent =  $(this).closest('.product-list-div');
			
			thisParent.remove();
			_checkSelectedProducts({
				element : $(".edit-product-master-list"),
				dropdown : $("#edit_product_list_dropdown")
			});
		});

		// EVENT FOR ADDING A BATCH FOR A CERTAIN PRODUCT.
		btnAddBatch.off('click').on('click', function() {
			var modalAddBatch = $('#add_batch_modal'),
			btnCloseModal = $('.close-add-batch-modal'),
			inputBatchDropDown = $('#add_batch_name'),
			btnSubmitAddBatch = $('#company_create_submit_add_batch'),
			uiContainer = $('.edit-product-list'),
			uiParentDiv = uiProductListDiv.find('.product-list-div[data-id="' + oCurrentProduct.id + '"]'),
			uiProductSkuAndName = modalAddBatch.find('#add_product_sku_name');

			modalAddBatch.data('product_details', oCurrentProduct);
			modalAddBatch.data('id', oCurrentProduct.id);
			uiProductSkuAndName.html('SKU No. ' + oCurrentProduct.sku + ' - ' + oCurrentProduct.name);

			// GET ALL BATCHES FOR A CERTAIN PRODUCT AND DECLARES IT GLOBALLY.
			_getProductBatches(oCurrentProduct.id, function(oResult) {
				oCURRENTPRODUCTBATCHES = oResult,
				oBatchList = oCURRENTPRODUCTBATCHES,
				sBatches = '<option value="">Select a batch</option>',
				arrBatch = [],
				oBatch = {};

				inputBatchDropDown.html(sBatches);

				// BUILDS THE PRODUCT BATCHES AS OPTION FOR THE SELECT PRODUCT BATCHES.
				for(var a in oCURRENTPRODUCTBATCHES) {
					sBatches += '<option value="' + oCURRENTPRODUCTBATCHES[a].batch_id + '">' + oCURRENTPRODUCTBATCHES[a].batch_name + '</option>';
				}

				// TRANSFORMS THE DROPDOWN.
				var uiSiblings = inputBatchDropDown.siblings();
				uiSiblings.remove();
				inputBatchDropDown.removeClass("frm-custom-dropdown-origin");
				inputBatchDropDown.html("");
				inputBatchDropDown.html(sBatches);
				inputBatchDropDown.transformDD();
				CDropDownRetract.retractDropdown(inputBatchDropDown);

				// FINDS ALL OPTIONS IN THE DROPDOWN AND INSERTS THE BATCH DETAILS IN THEM.
				inputBatchDropDown.find('option').each(function() {

					for(var b in oCURRENTPRODUCTBATCHES) {
						if($(this).val() == oCURRENTPRODUCTBATCHES[b].batch_id) {
							$(this).data('batch_details', oCURRENTPRODUCTBATCHES[b]);
						}
					}

				});

				// SELECT BATCH EVENT.
				inputBatchDropDown.off('change').on('change', function() {
					
					var iSelectedBatch = $(this).val(),
					uiStorageName = $('.batch-storage-name'),
					iStorageId = null,
					oCurrentProduct = $('#add_batch_modal').data('product_details'),
					oCurrentBatchDetails = $('#add_batch_name option:selected').data('batch_details'),			
					uiParentDiv = $('.edit-product-list').find('.product-list-div[data-id="' + oCurrentProduct.id + '"]');
					// .product-list-div[data-id="' + iProductId + '"]
					// inputRadioPiece = $('.edit-product-list');

					// GETS THE STORAGE ID OF THE CURRENT BATCH SELECTED.
					for(var a in oCURRENTPRODUCTBATCHES) {
						if(oCURRENTPRODUCTBATCHES[a].batch_id == iSelectedBatch) {
							iStorageId = oCURRENTPRODUCTBATCHES[a].location_id;
						}
					}

					// GETS ALL THE STORAGES AND DECLARES IT GLOBALLY.
					getAllStorage(function(oResult){
						oStorages = oResult;

						for(var b in oStorages) {
							if(oStorages[b].id == oCurrentBatchDetails.storage_id_number) {
								uiStorageName.html(oStorages[b].name);
								uiStorageName.data('id', oStorages[b].id);

								// if(inputRadioBulk.prop('checked')) {
								// 	alert('bulk');
								// }

								// if(inputRadioPiece.prop('checked')) {
								// 	alert('piece');
								// }
							}
						}

						_buildLocationHierarchy({id : iSelectedBatch, name : "add_batch_modal"});
					});		
				});

				// EVENT FOR ADDING THE BATCH TO THE BATCH DIV.
				btnSubmitAddBatch.off("click").on("click", function() {

					// DECLARES THE NEED UI VARIABLES
					var uiProductBatchDisplay = uiClonedTemplate.find('.product-batch-display'),
					uiProductBatchCloned = oProductBatchTemplate.clone(),
					uiBatchOrigin = uiProductBatchCloned.find('.batch-origin'),
					uiBatchDestination = uiProductBatchCloned.find('.batch-destination'),
					uiBatchName = uiProductBatchCloned.find('.batch-name'),
					uiBatchQuantity = uiProductBatchCloned.find('.batch-quantity'),
					sBatchOrigin = '',
					sBatchDestination = '';

					// CHANGES THE CLASS OF THE CLONED UI TO PREVENT RE-CLONING.
					uiProductBatchCloned.removeClass('product-batch-template');
					uiProductBatchCloned.addClass('product-batch-cloned');
					uiProductBatchCloned.show();

					// BUILDS THE INFORMATION FROM THE SELECTED BATCH.
					var oSelectedBatch = $('#add_batch_name option:selected').data('batch_details'),
					oBatchLocation = oSelectedBatch.location_id;
					oOriginBreadCrumbs = _buildLocationBreadCrumb({id: oBatchLocation}),
					iOriginLength = parseInt(Object.keys(oOriginBreadCrumbs).length) - parseInt(1),
					oDestination = _getBreadCrumbsHierarchy(),
					iDestinationLength = parseInt(Object.keys(oDestination).length) - parseInt(1),
					oDestinationBreadCrumbs = _buildLocationBreadCrumb({id : oDestination[iDestinationLength].id});
					
					// BUILDS THE BREADCRUMB FOR THE ORIGIN OF THE BATCH.
					for(var a in oOriginBreadCrumbs) {
						if(a != iOriginLength) {
							sBatchOrigin += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oOriginBreadCrumbs[a].name + '</p>'
							+ '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>';
						} else {
							sBatchOrigin += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oOriginBreadCrumbs[a].name + '</p>';
						}			
					}

					// BUILDS THE BREADCRUMB FOR THE DESTINATION OF THE BATCH.
					for(var b in oDestinationBreadCrumbs) {
						if(b != iDestinationLength) {
							sBatchDestination += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oDestinationBreadCrumbs[b].name + '</p>'
							+ '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>';
						} else {
							sBatchDestination += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oDestinationBreadCrumbs[b].name + '</p>';
						}
					}

					var oUnitOfMeasure = cr8v_platform.localStorage.get_local_storage({name : 'unit_of_measures'}),
					iConvertedQuantity =  $('#add_batch_modal').find('.batch-quantity').data('quantity'),
					sUnitOfMeasureText = '';

					if(uiParentDiv.find('.radio-bulk').prop('checked')) {
						iConvertedUnitOfMeasureId = $('#add_batch_modal').find('.batch-quantity').data('unit_of_measure');
					} else {
						iConvertedUnitOfMeasureId = 8; //BAG
					}

					for(var a in oUnitOfMeasure) {
						if(iConvertedUnitOfMeasureId == oUnitOfMeasure[a].id) {
							sUnitOfMeasureText = oUnitOfMeasure[a].name;
						}
					}
					
					// MANIPULATES TO THE DOM WITH THE BATCH INFORMATION.
					uiBatchName.html(oSelectedBatch.batch_name);
					// uiBatchQuantity.html(oSelectedBatch.batch_quantity + ' ' + oSelectedBatch.unit_of_measure);
					uiBatchQuantity.html(iConvertedQuantity + ' ' + sUnitOfMeasureText);
					uiBatchOrigin.html(sBatchOrigin);
					uiBatchDestination.html(sBatchDestination);
					uiProductBatchCloned.data('batch_details', oSelectedBatch);
					uiProductBatchCloned.data('converted_qty', iConvertedQuantity);
					uiProductBatchCloned.data('unit_of_measure_id', iConvertedUnitOfMeasureId);
					uiProductBatchCloned.data('unit_of_measure_name', sUnitOfMeasureText);
					uiProductBatchCloned.data('origin', oOriginBreadCrumbs);
					uiProductBatchCloned.data('destination', oDestinationBreadCrumbs);
					uiProductBatchDisplay.append(uiProductBatchCloned);
					modalAddBatch.removeClass('showed');

					_UI_DisplayTotalQtyToTransfer({
						page : 'company_ongoing_edit',
						product_id : oCurrentProduct.id
					});

					//_checkSelectedProducts({
					//	element : $(".edit-product-list"),
					//	dropdown : $("#add_batch_name")
					//});
				});

				modalAddBatch.addClass('showed');

			});

			//CLOSES THE MODAL.
			btnCloseModal.off('click').on('click', function() {
				modalAddBatch.removeClass('showed');
				modalAddBatch.removeData('product_details');
			});

		});

		// CHECKS ALL THE SELECTED PRODUCTS AND REMOVES THE FROM THE DROPDOWN.
		_checkSelectedProducts({
			element : $(".edit-product-master-list"),
			dropdown : $("#edit_product_list_dropdown")
		});

	}

	function _getProductBatches(iProductId, callBack) {
		var oOptions = {
			type : "GET",
			data : {product_id : iProductId},
			returnType : "json",
			url : BASEURL + "transfers/get_product_batches",
			success : function(oResult) {
				if(typeof callBack == 'function') {
					if(Object.keys(oResult.data).length > 0) {
						callBack(oResult.data);
					} else {
						callBack("No batches found.");
					}		
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getProductTransfers(iTransferLogId, callBack) {
		var oOptions = {
			type : "GET",
			data : {transfer_log_id : iTransferLogId},
			returnType : "json",
			url : BASEURL + "transfers/get_company_product_transfers",
			success : function(oResult) {
				if(typeof callBack == 'function') {
					callBack(oResult.data);
				} else {
					callBack("No transfer found.");
				}
			}
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllLocation(oParams) {
		// var sPage = oParams.page,
		oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "transfers/get_all_locations",
			success : function(oResult) {
				// console.log(oResult.data);
				oLocations = oResult.data;
			}
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _buildLocationHierarchy(oParams) {
		var iBatchListId = oParams.id,
		oCurrentLocation = null,
		oLocationList = {},
		iCounter = 0,
		iCounterTwo = 0,
		iLocationId = "",
		arr = [];

		var sCurrentPath = window.location.href;

		if(sCurrentPath.indexOf("company_ongoing_edit") > -1) {

			//GETS THE LOCATION ID OF THE SENT BATCH.
			for(var x in oBatchList) {
				//console.log(oBatchList[x].id);
				//console.log(oBatchList);
				if(oBatchList[x].batch_id == iBatchListId) {
					iLocationId = oBatchList[x].location_id;
				}
			}

			//GETS THE CURRENT LOCATION'S DATA.
			for(var a in oLocations) {
				if(oLocations[a].id == iLocationId) {
					oCurrentLocation = oLocations[a];
				}
			}

			oLocationList[iCounter] = oCurrentLocation; 
			++iCounter;

			var keyCounter = Object.keys(oLocations).length;

			//BUILDS THE ARRAY FOR THE LOCATION HIERARCHY.
			for(var b = 0; b < keyCounter;) {
				if(oLocations[b].id == oCurrentLocation.parent_id) {
					oLocationList[iCounter] = oLocations[b];
					oCurrentLocation = oLocations[b];
					++iCounter;
					b = -1;
				}
				++b;
			}

			//CONVERT THE OBJECT TO AN ARRAY.
			for(var i in oLocationList){
				arr.push(oLocationList[i]);
			}

			
		} else {

					//GETS THE LOCATION ID OF THE SENT BATCH.
			for(var x in oBatchList) {
				// console.log(oBatchList[x].id);
				//console.log(oBatchList);
			if(oBatchList[x].id == iBatchListId) {
					iLocationId = oBatchList[x].location_id;
				}
			}

			//GETS THE CURRENT LOCATION'S DATA.
			for(var a in oLocations) {
				if(oLocations[a].id == iLocationId) {
					oCurrentLocation = oLocations[a];
				}
			}

			oLocationList[iCounter] = oCurrentLocation; 
			++iCounter;

			var keyCounter = Object.keys(oLocations).length;

			//BUILDS THE ARRAY FOR THE LOCATION HIERARCHY.
			for(var b = 0; b < keyCounter;) {
				if(oLocations[b].id == oCurrentLocation.parent_id) {
					oLocationList[iCounter] = oLocations[b];
					oCurrentLocation = oLocations[b];
					++iCounter;
					b = -1;
				}
				++b;
			}

			//CONVERT THE OBJECT TO AN ARRAY.
			for(var i in oLocationList){
				arr.push(oLocationList[i]);
			}

				
		}


		//GETS THE LOCATION ID OF THE SENT BATCH.
		// for(var x in oBatchList) {
		// 	// console.log(oBatchList[x].id);
		// 	console.log(oBatchList);
		// 	if(oBatchList[x].id == iBatchListId) {
		// 		iLocationId = oBatchList[x].location_id;
		// 	}
		// }

		// //GETS THE CURRENT LOCATION'S DATA.
		// for(var a in oLocations) {
		// 	if(oLocations[a].id == iLocationId) {
		// 		oCurrentLocation = oLocations[a];
		// 	}
		// }

		// oLocationList[iCounter] = oCurrentLocation; 
		// ++iCounter;

		// var keyCounter = Object.keys(oLocations).length;

		// //BUILDS THE ARRAY FOR THE LOCATION HIERARCHY.
		// for(var b = 0; b < keyCounter;) {
		// 	if(oLocations[b].id == oCurrentLocation.parent_id) {
		// 		oLocationList[iCounter] = oLocations[b];
		// 		oCurrentLocation = oLocations[b];
		// 		++iCounter;
		// 		b = -1;
		// 	}
		// 	++b;
		// }

		// //CONVERT THE OBJECT TO AN ARRAY.
		// for(var i in oLocationList){
		// 	arr.push(oLocationList[i]);
		// }

		var oProductLocations = arr.reverse();
		_UI_subLocation({page : "create_batch", data : oProductLocations});	
	}

	
	function _buildLocationBreadCrumb(oParams) {
		/**
		* _buildLocationBreadCrumb
		* @description Builds the hierarchy of transfers/batch location and stores it
		* to an object.
		* @dependencies 
		* @param N/A
		* @response N/A
		* @criticality N/A
		* @software_architect N/A
		* @developer Dru Moncatar
		* @method_id N/A
		*/

		var iLocationId = oParams.id,
		oCurrentLocation = null,
		oLocationList = {},
		iCounter = 0,
		iCounterTwo = 0,
		arr = [];

		// _getLocations(function(oResult) {
		// 	oLocations = oResult;

			//GETS THE CURRENT LOCATION'S DATA.
			for(var a in oLocations) {
				if(oLocations[a].id == iLocationId) {
					oCurrentLocation = oLocations[a];
				}
			}

			oLocationList[iCounter] = oCurrentLocation; 
			++iCounter;

			var keyCounter = Object.keys(oLocations).length;

			//BUILDS THE ARRAY FOR THE LOCATION HIERARCHY.
			for(var b = 0; b < keyCounter;) {
				if(oLocations[b].id == oCurrentLocation.parent_id) {
					oLocationList[iCounter] = oLocations[b];
					oCurrentLocation = oLocations[b];
					++iCounter;
					b = -1;
				}
				++b;
			}

			//CONVERT THE OBJECT TO AN ARRAY.
			for(var i in oLocationList){
				arr.push(oLocationList[i]);
			}

			var oProductLocations = arr.reverse();

			return oProductLocations;
		// });
	
	}

	function _getLocations(callBack) {
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "transfers/get_all_locations",
			success : function(oResult) {
				if(typeof callBack == 'function') {
					callBack(oResult.data);		
				}
			}
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getAllStorage(callBack){
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "storages/get_all_storages",
			success : function(oResult) {
				oStorages = oResult;
				if(typeof callBack == 'function'){
					callBack(oResult);
				}
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllBatches(callBack) {
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "transfers/get_all_batches",
			success : function(oResult) {
				// console.log(oResult.data);
				oBatchList = oResult.data;
				if(typeof callBack == 'function') {
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _displayBatchStorages(oStorages)
	{
		var batchModal = $('[modal-id="add-batch"]'),
			breadCrumbsHierarchy = $("#breadCrumbsHierarchy"),
			batchDisplay = $("#batchDisplay"),
			oSelectedUI = [],
			oDisplayedData = {},
			oToPlaceData = {},
			doubleClickLevel = 0;

		batchDisplay.html("");

		var sTemplate = '<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor hier-item">'+
				'<div class="display-inline-mid width-20percent overflow-hide half-border-radius">'+
					'<img src="" alt="images" class="width-100percent">'+
				'</div>'+
				'<div class="display-inline-mid">'+
					'<p class="font-14 font-400 location-name"></p>'+
					'<p class="font-12 font-400 italic qty-in-location"> </p>'+
				'</div>'+
			'</div>';

		var sBackArrow = '<i style="margin-right:4px;" class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small padding-right-10 border-black back-arrow"></i>',
			sNextIcon = '<span class="display-inline-mid padding-all-5 greater-than">&gt;</span>',
			sItemBread = '<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid item-bread"></p>',
			sMainBread = '<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5"> </p>',
			sActiveBread = '<span class="active-bread"></span>';
		

		var setActiveEvent = function(uiThis){
			batchModal.find(".bggray-light").removeClass('bggray-light active-hier');
			
			var uiTraget = uiThis,
				oData = uiTraget.data();

			if(!uiTraget.hasClass("hier-item")){
				uiTraget = uiTraget.closest(".hier-item");
				oData = uiTraget.data();
			}

			if(uiTraget.closest(".hier-item").length > 0){
				uiTraget.closest(".hier-item").addClass('bggray-light active-hier');
			}else if(uiTraget.hasClass("hier-item")){
				uiTraget.addClass('bggray-light');
			}

			if(Object.keys(oSelectedUI).length > 0){
				oToPlaceData = oData;
				var	uiNext = $(sNextIcon),
					uiBread = $(sItemBread);
				breadCrumbsHierarchy.find(".temporary").remove();
				uiNext.addClass("temporary");
				uiBread.addClass("temporary");
				breadCrumbsHierarchy.append(uiNext);
				uiBread.html(oData.name);
				uiBread.data(oData);
				breadCrumbsHierarchy.append(uiBread);
			}

		}

		var displayBreadCrumbsStorage = function(){
			var uiBread = $(sMainBread),
				oData = selectedUI.data();
			uiBread.html(oData.name);
			
			breadCrumbsHierarchy.find(".greater-than").remove();
			

			breadCrumbsHierarchy.append(uiBread);
		}

		var displayBreadCrumbsItems = function(){
			breadCrumbsHierarchy.find(".greater-than").remove();
			breadCrumbsHierarchy.find(".item-bread").remove();
			breadCrumbsHierarchy.find(".bggray-7cace5").remove();

			for(var i in oSelectedUI){
				var uiMainBread = $(sMainBread),
					uiNext = $(sNextIcon),
					uiBread = $(sItemBread);
				 if(i == "0"){
				 	uiMainBread.html(oSelectedUI[i]["name"]);
				 	uiMainBread.data(oSelectedUI[i]);
					breadCrumbsHierarchy.append(uiMainBread);
				 }else{
					breadCrumbsHierarchy.append(uiNext);
					uiBread.html(oSelectedUI[i]["name"]);
					uiBread.data(oSelectedUI[i]);
					breadCrumbsHierarchy.append(uiBread);
				 }
			}

		}

		var selectHierarchy = function(uiThis){
			var oData = uiThis.data(),
				iLevel = oData.iLevel,
				sQtyInLocationParent = oData.sQtyInLocationParent,
				sFirstLevelID = "",
				oDataToDisplay = {};
			
			if(oData.children.length > 0){
				oSelectedUI[doubleClickLevel] = oData;
				doubleClickLevel++;
				oDisplayedData[doubleClickLevel] = oData.children;
				displayChildren(oData.children);
				displayBreadCrumbsItems();
			}
			
			
		}

		var displayChildren = function(oChildren){
			
			if(Object.keys(oChildren).length > 0){
				batchDisplay.html("");
			}
			for(var i in oChildren){
				var uiItem = $(sTemplate),
					data = oChildren[i];
				
				data["children"] = data.children;
				uiItem.find(".location-name").html(data.name);
				// uiItem.find("img").attr("src", CGetProfileImage.getDefault(data.name));
				uiItem.find("img").attr("src", BASEURL+"assets/images/folder-circle-blue.png");
				uiItem.find(".qty-in-location").html("Number of Locations inside = "+Object.keys(data.children).length);
				uiItem.data(data);
				uiItem.attr("location-id", data.id);

				uiItem.single_double_click(
					function(){
						setActiveEvent($(this));
					},
					function(){
						selectHierarchy($(this));
					}
				);

				batchDisplay.append(uiItem);
			}

		}

		var uiBackArrow = $(sBackArrow);

		uiBackArrow.on("click", function(){
			if(doubleClickLevel > 0){
				doubleClickLevel = doubleClickLevel - 1;
				delete oSelectedUI[doubleClickLevel];
				if(doubleClickLevel > 0){
					displayChildren(oDisplayedData[doubleClickLevel]);
				}else if(doubleClickLevel == 0){
					displayStorages(oStorages);
				}

				displayBreadCrumbsItems();
			}
		});

		breadCrumbsHierarchy.html("").append(uiBackArrow);	
		
		var makeHierarchy = function(oData){
			var flat = {};
			for(var i in oData){
				var k = oData[i]["id"];
				oData[i]["children"] = [];
				flat[k] = oData[i];
			}

			for (var i in flat) {
				var parentkey = flat[i].parent_id;
				if(flat[parentkey]){
					flat[parentkey].children.push(flat[i]);
				}
			}

			var root = [];
			for (var i in flat) {
				var parentkey = flat[i].parent_id;
				if (!flat[parentkey]) {
					root.push(flat[i]);
				}
			}
			return root;
		};

		var displayStorages = function(oStorages){
			batchDisplay.html("");
			currentDisplayedData = [];
			for(var i in oStorages){
				var uiItem = $(sTemplate),
					data = oStorages[i],
					oHierarchy = makeHierarchy(data.location);
				
				data["children"] = oHierarchy[0]["children"];
				uiItem.find(".location-name").html(data.name);
				// uiItem.find("img").attr("src", CGetProfileImage.getDefault(data.name));
				uiItem.find("img").attr("src", BASEURL+"assets/images/folder-circle-blue.png");
				uiItem.find(".qty-in-location").html(data.capacity).number(true, 2).append(" "+data.measure_name);
				uiItem.attr("storage-id", data.id);
				batchDisplay.append(uiItem);
				uiItem.data(data);
				uiItem.data("sQtyInLocationParent", uiItem.find(".qty-in-location").html());
				
				uiItem.single_double_click(
					function(){
						setActiveEvent($(this));
					},
					function(){
						selectHierarchy($(this));
					}
				);
			}

			oDisplayedData = {};
			oDisplayedData[0] = oStorages;
		}

		displayStorages(oStorages);

	}

	function _getBreadCrumbsHierarchy()
	{
		var breadCrumbsHierarchy = $("#breadCrumbsHierarchy"),
			uiMainBread = breadCrumbsHierarchy.find(".bggray-7cace5"),
			oMainBread = uiMainBread.data(),
			oResponse = {};
		
		oResponse[0] = oMainBread;

		var uiNextAllBread = uiMainBread.nextAll(".item-bread");
		$.each(uiNextAllBread, function(){
			var thisData = $(this).data();
			oResponse[Object.keys(oResponse).length] = thisData;
		});

		if(oResponse[0] == undefined) {
			oResponse = undefined;
			return oResponse;
		} else {
			return oResponse;
		}
		// oResponse[0] == undefined ? oResponse = undefined : return oResponse;
		// return oResponse;

	}

	function _buildDestinationHierarchy(oParams) {
		//console.log(oParams);

		var oDestination = oParams.data,
		sDestination = {};

		for(var a in oDestination) {
			if(oDestination[a] != undefined) {
				sDestination[a] = {
					id : oDestination[a].id,
					name : oDestination[a].name
				};
			}		
		}	
	
		return sDestination;
	}

	function _getOngoingDetails(iTransferId, callBack) {
		oOptions = {
			type : 'GET',
			data : {transfer_log_id : iTransferId},
			returnType : 'json',
			url : BASEURL + 'transfers/company_get_ongoing_transfers',
			success : function(oResult) {
				if(typeof callBack == 'function') {
					if(Object.keys(oResult.data).length > 0) {
						callBack(oResult.data);
					} else {
						callBack("No batches found.");
					}		
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getTransferBatchList(iTransferLogId, callBack) {
		var oOptions = {
			type : "GET",
			data : {transfer_log_id : iTransferLogId},
			url : BASEURL + 'transfers/company_get_transfer_batches_list',
			returnType : 'json',
			success : function(oResult) {
				if(typeof callBack == 'function') {
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getTransferDetails(iTransferId, callBack)  {
		var oOptions = {
			type : "GET",
			data : {transfer_log_id : iTransferId},
			url : BASEURL + 'transfers/company_get_transfer_details',
			returnType : 'json',
			success : function(oResult) {
				if(typeof callBack == 'function') {
					callBack(oResult.data);
				}

				// console.log(oResult.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllWorkers(iTransferLogId, callBack) {
		var oOptions = {
			type : "GET",
			data : {transfer_log_id : iTransferLogId},
			url : BASEURL + 'transfers/get_all_workers',
			returnType : 'json',
			success : function(oResult) {
				if(typeof callBack == 'function') {
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllEquipments(iTransferLogId, callBack) {
		var oOptions = {
			type : "GET",
			data : {transfer_log_id : iTransferLogId},
			url : BASEURL + 'transfers/get_all_equipments',
			returnType : 'json',
			success : function(oResult) {
				if(typeof callBack == 'function') {
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _calculateTotalQty(oParams) {
		var oBatches = oParams.data,
		iTotalQtyToTransfer = 0;
		
		// console.log(oBatches);

		for(var a in oBatches) {
			var oBatchQuantity = oBatches[a].quantity;
			iTotalQtyToTransfer = parseFloat(iTotalQtyToTransfer) + parseFloat(oBatchQuantity);
		}

		return iTotalQtyToTransfer;
	}

	function _getUnitOfMeasureList(callBack) {
		var oOptions = {
			type : "GET",
			data : {test : true},
			url : BASEURL + 'transfers/get_all_unit_of_measures',
			returnType : 'json',
			success : function(oResult) {
				if(typeof callBack == 'function') {
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllModeOfTransferList(callBack) {
		var oOptions = {
			type : "GET",
			data : {test : true},
			url : BASEURL + 'transfers/get_mode_of_transfer',
			returnType : 'json',
			success : function(oResult) {
				if(typeof callBack == 'function') {
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	//==================================================//
	//====================UI METHODS====================//
	//==================================================//

	function _UI_companyTransfer() {
		var sTemplate = "",
		currentTransfer = {},
		renderDetails = {},
		templateDetails = {};

		// arrTransferList = cr8v_platform.localStorage.get_local_storage({name : "transferCompanyGoodList"});
		arrTransferList = oTRANSFERLIST;

		// console.log(arrTransferList);

		for(var a in arrTransferList) {
			currentTransfer = arrTransferList[a];
			templateDetails = {
				name : "company_transfer",
				data : {
					id : currentTransfer.id,
					status  : currentTransfer.status
				}
			};
			sTemplate = _buildTemplate(templateDetails);
			renderDetails = { name : "company_transfer", data : currentTransfer, template : sTemplate };
			_render(renderDetails);
		}
	}

	function _UI_company_complete(oTransferRecord) {
		// var oCurrentTransfer = _getCurrentTransfer(),
		var iTransferId = oCurrentTransfer.id,
		oWorkers = {},
		oTransferBatches = {},
		oEquipments = {};

		// GET ALL TRANSFER'S WORKERS.
		_getTransferWorkers(iTransferId, function(oResult) {
			oWorkers = oResult;

			// GET ALL TRANSFER'S EQUIPMENTS.
			_getTransferEquipments(iTransferId, function(oResult) {
				oEquipments = oResult;

				// GET ALL TRANSFER'S BATCHES.
				_getTransferBatches(iTransferId, function(oResult) {
					oTransferBatches = oResult;

					var renderDetails = {
						name : "company_complete",
						data : oCurrentTransfer,
						products : oTransferBatches,
						workers : oWorkers,
						equipments : oEquipments,
						transfer_record : oTransferRecord
					};

					_render(renderDetails);
				});
				
			});

		});
		
		// _getTransferWorkers({id : iTransferId});
		// _getTransferEquipments({id : iTransferId});
		// _getTransferBatches({id : iTransferId}),
		// oBranches = {},
		// oWorkers = {},
		// oEquipments = {};

		// console.log(oTransferRecord);

		// var renderDetails = {
		// 	name : "company_complete",
		// 	data : oCurrentTransfer,
		// 	products : cr8v_platform.localStorage.get_local_storage({name : "current_transfer_batches"}),
		// 	workers : cr8v_platform.localStorage.get_local_storage({name : "current_transfer_workers"}),
		// 	equipments : cr8v_platform.localStorage.get_local_storage({name : "current_transfer_equipments"}),
		// 	transfer_record : oTransferRecord
		// };

		// _render(renderDetails);
		
	}

	function _UI_company_ongoing() {
		//var oCurrentTransfer = _getCurrentTransfer();
		var renderDetails = {
			name : "company_ongoing",
			data : oCurrentTransfer
		};
		_render(renderDetails);
	}

	function _UI_companyCreateProductDropdown() {
		var sProductList = "",
		renderDetails = {
			name : "company_create_product_dropdown",
			data : ""
		};
		arrProducts = cr8v_platform.localStorage.get_local_storage({name : "productList"});
		
		var prodTimeinterval = {};

		prodTimeinterval = setInterval(function(){
			arrProducts = cr8v_platform.localStorage.get_local_storage({name : "productList"});
			if(arrProducts !== null){
				for(var a in arrProducts) {
					sProductList += '<option value="' + arrProducts[a].id + '">' + arrProducts[a].name + '</option>';
				}

				renderDetails["data"] = sProductList;
				_render(renderDetails);
				clearInterval(prodTimeinterval);
			}

		},500);

		
	}

	function _UI_companyCreateProductDisplay() {
		var sProductDisplay = "",
		renderDetails = {
			name : "company_create_product_display",
			data : ""
		};
	}

	function _UI_companyCreateModeOfTransfer(oParams) {
		var oElementData = oParams.data;
		var oElement = {
			"get" : function(oData) {
				_getModeOfTransfer();
			},
			"display" : function(oData) {
				var oList = "";

				for(var a in oElementData) {
					oList += '<option value="' + oElementData[a].id + '">' + oElementData[a].name + '</option>';
				}
				_render({name : "company_create_mode_of_transfer", data : oList});
			}
		}

		if (typeof (oElement[oParams.name]) == "function") 
		{
			oElement[oParams.name](oParams.name);
		}
	}

	function _UI_companyCreateAddBatch() {
		var iSelectedProduct = $("#company_create_product_display"),
		inputPiece = $("#qty_to_transfer_PIECE"),
		inputKg = $("#qty_to_transfer_KG"),
		oSelectedProduct =  {},
		modalAddBatch = $("#add_batch_modal"),
		sUnitOfMeasure = $(".unit_of_measure_dropdown option:selected").text();
		
		oProducts = cr8v_platform.localStorage.get_local_storage({name : "productList"});

		for(var a in oProducts) {
			if(oProducts[a].id == iSelectedProduct.data("id")) {
				oSelectedProduct = oProducts[a];
			}
		}

		if(inputPiece.val().length > 0) {
			oSelectedProduct["transfer_balance"] = inputPiece.val();
			oSelectedProduct["transfer_unit_of_measure"] = "pcs";
		} else {
			oSelectedProduct["transfer_balance"] = inputKg.val();
			oSelectedProduct["transfer_unit_of_measure"] = sUnitOfMeasure;
		}

		modalAddBatch.addClass("showed");
		_render({name : "add_batch_modal", data : oSelectedProduct});
	}

	function _UI_companyCreateSubmitAddBatch(oParams) {
		_buildTemplate({name : "company_create_batch_list", data: oParams});
	}

	function _UI_companyCreateAddNote() {
		var oPostData = {
			note_text : $("#add_note_text").val(),
			note_subject : $("#add_note_subject").val(),
			note_date : currentDateTime.to_display,
			note_date_record : currentDateTime.to_record
		};

		_buildTemplate({name : "company_create_add_note", data : oPostData});
	}

	function _UI_displayCompleteTransferNotes(oNotes) {
		for(var a in oNotes) {
			var oTemplateDetails = {
				name : "company_complete_view_note_list",
				data : oNotes[a]
			};
			var sTemplate = _buildTemplate(oTemplateDetails);
			_render({name : "company_complete_view_note_list", data : sTemplate});
		}
	}

	function _UI_companyCreateAddBatchSubLocation() {
		var uiSubLocationLastChild = $("#company_create_add_batch_sub_location :last-child");
		
		if(uiSubLocationLastChild.hasClass("bg-light-gray")) {
			_buildTemplate({name : "company_create_add_batch_sub_location", data : "white_bg"});
		} else {
			_buildTemplate({name : "company_create_add_batch_sub_location", data : "gray_bg"});
		}
	}

	function _UI_unitOfMeasureDropDown(oResult) {
		var oUnitOfMeasure = oResult,
		sTemplate = "";

		for(var a in oUnitOfMeasure) {
			if(oUnitOfMeasure[a].name != "pc") {
				sTemplate += '<option value="' + oUnitOfMeasure[a].id + '">' + oUnitOfMeasure[a].name + '</option>'
			}
		}
		_render({name : "unit_of_measure_dropdown", data : sTemplate});
	}

	function _UI_completeTransferWorkerTypes() {
		var sTemplate = "";

		for(var a in oWORKERTYPELIST) {
			// sTemplate += '<option value="' + oWorkerTypes[a].id + '">' + oWorkerTypes[a].name + '</option>';
			sTemplate += '<option value="' + oWORKERTYPELIST[a].id + '">' + oWORKERTYPELIST[a].name + '</option>';
		}

		return sTemplate;
		// _render({name : 'complete_transfer_worker_types', data : sTemplate});
	}

	function _UI_transferDocuments(oParams) {
		var oDocuments = oParams.data,
		sDarkTemplate = _buildTemplate({name : 'transfer_document', data : 'dark'}),
		sLightTemplate = _buildTemplate({name : 'transfer_document', data : 'light'}),
		sCurrentTemplate = "",
		uiDocumentDiv = $("#complete_document_div");

		for(var a in oDocuments) {
			sCurrentTemplate = a%2 == 0 ? $(sDarkTemplate) : $(sLightTemplate);

			var uiDocumentName = sCurrentTemplate.find('.document-name'),
			uiDocumentDateTime = sCurrentTemplate.find('.document-date-time');

			uiDocumentName.html(oDocuments[a].document_name);
			uiDocumentDateTime.html(oDocuments[a].date_added);

			var oRenderDetails = {
				name : 'transfer_document',
				data : sCurrentTemplate
			};
			_render(oRenderDetails);
		}
	}

	function _UI_OngoingEdit(oParams) {
		var oEditTransfer = oParams.data,
		sProductList = "",
		sModeOfTransfer = "",
		// oProducts = cr8v_platform.localStorage.get_local_storage({name : 'productList'}),
		oModeOfTransfer = cr8v_platform.localStorage.get_local_storage({name : 'mode_of_transfer'}),
		oRenderDetails = {
			name : "ongoing_edit",
			transfer_details : oEditTransfer
		};
		
		for(var a in oProducts) {
			sProductList += '<option value="' + oProducts[a].id + '">' + oProducts[a].name + '</option>';
		}

		for(var b in oMODEOFTRANSFERLIST) {
			sModeOfTransfer += '<option value="' + oMODEOFTRANSFERLIST[b].id + '">' + oMODEOFTRANSFERLIST[b].name + '</option>';
		}

		oRenderDetails["product_dropdown"] = sProductList;
		oRenderDetails["mode_of_transfer_dropdown"] = sModeOfTransfer;

		_render(oRenderDetails);
		// console.log(oRenderDetails);
		//alert(JSON.stringify(oEditTransfer));
	}

	function _UI_batchDropDown(oParams) {
		var oBatches = oCURRENTPRODUCTBATCHES;

		var oElement = {
			"create_batch" : function() {
				var uiDropdown = $("#add_batch_name"),
				sBatches = "";

				// if(Object.keys(oBatches).length > 0) {
				if(oBatches != 'No batches found.') {
					sBatches = '';
					for(var a in oBatches) {
						sBatches += '<option value="' + oBatches[a].batch_id + '"">' + oBatches[a].batch_name + '</option>';
					}
				} else {
					sBatches = '<option value="">No batches found.</option>'
				}

				var uiSiblings = uiDropdown.siblings();
				uiSiblings.remove();
				uiDropdown.removeClass("frm-custom-dropdown-origin");
				uiDropdown.html("");
				uiDropdown.html(sBatches);
				uiDropdown.transformDD();
				CDropDownRetract.retractDropdown(uiDropdown);

				$("#add_batch_name option").each(function() {
					for(var a in oBatches) {
						if($(this).val() == oBatches[a].batch_id) {
							$(this).data('batch_details', oBatches[a]);
						}
					}
				});
			}
		}

		if (typeof (oElement[oParams.page]) == "function") 
		{
			oElement[oParams.page](oParams.page);
		}
	}

	function _UI_subLocation(oParams) {
		var oLocations = oParams.data;

		//console.log(oParams);

		var oElement = {

			"create_batch" : function() {
				var sCurrentPath = window.location.href;

				if(sCurrentPath.indexOf("company_ongoing_edit") > -1) { 
					var oCurrentLocationDetails = $("#add_batch_name option:selected").data('batch_details');
					var sTemplate = _buildTemplate({name : 'sub_location'});	
					var uiParentDiv = $('#company_create_add_batch_sub_location'),
					iLocationLength = Object.keys(oLocations).length,
					iProductId = $('#add_batch_modal').data('id'),
					uiProductDiv = $('.edit-product-list').find('.product-list-div[data-id="' + iProductId + '"]'),
					inputUnitOfMeasure = uiProductDiv.find('.unit_of_measure_dropdown option:selected'),
					inputRadioPiece = uiProductDiv.find('.radio-piece'),
					inputRadioBulk = uiProductDiv.find('.radio-bulk');

				} else {
					var oCurrentLocationDetails = $("#add_batch_name option:selected").data('batch_details');
					var sTemplate = _buildTemplate({name : 'sub_location'});	
					var uiParentDiv = $('#company_create_add_batch_sub_location'),
					iLocationLength = Object.keys(oLocations).length,
					iProductId = $('#add_batch_modal').data('id'),
					uiProductDiv = $('#company_create_product_details').find('.product-list-div[data-id="' + iProductId + '"]'),
					inputUnitOfMeasure = uiProductDiv.find('.unit_of_measure_dropdown option:selected'),
					inputRadioPiece = uiProductDiv.find('.radio-piece'),
					inputRadioBulk = uiProductDiv.find('.radio-bulk');

				}

				// var oCurrentLocationDetails = $("#add_batch_name option:selected").data('batch_details');
				// var sTemplate = _buildTemplate({name : 'sub_location'});	
				// var uiParentDiv = $('#company_create_add_batch_sub_location'),
				// iLocationLength = Object.keys(oLocations).length,
				// iProductId = $('#add_batch_modal').data('id'),
				// uiProductDiv = $('#company_create_product_details').find('.product-list-div[data-id="' + iProductId + '"]'),
				// inputUnitOfMeasure = uiProductDiv.find('.unit_of_measure_dropdown option:selected'),
				// inputRadioPiece = uiProductDiv.find('.radio-piece'),
				// inputRadioBulk = uiProductDiv.find('.radio-bulk');

				uiParentDiv.html('<table class="tbl-4c3h ">'
				+	'<thead>'
				+		'<tr>'
				+			'<th class="black-color width-65percent">Location Address</th>'
				+			'<th class="black-color width-20percent">Quantity</th>'
				+		'</tr>'
				+	'</thead>'
				+ '</table>');

				if(Object.keys(oLocations).length > 0) {
					var oTemplate = $(sTemplate),
					sBreadCrumb = "";

					for(var a in oLocations) {
						var iChecker = parseInt(a) + parseInt(1);
						if(iChecker == iLocationLength) {
							sBreadCrumb += '<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> ' + oLocations[a].name + '  </p>';
						} else {
							sBreadCrumb += '<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> ' + oLocations[a].name + '  </p>'
							+ '<i class="display-inline-mid font-14 padding-right-10">&gt;</i>';	
						}
					}
					
					sBreadCrumb += '<div class="clear"></div>';
					oTemplate.find('.location-breadcrumb').html(sBreadCrumb);
					oTemplate.find('.batch-quantity').html(oCurrentLocationDetails.batch_quantity);
					

					if(inputRadioBulk.prop('checked')) {
						if(inputUnitOfMeasure.text() != oCurrentLocationDetails.unit_of_measure) {
							var sConvertedQty = UnitConverter.convert(oCurrentLocationDetails.unit_of_measure, inputUnitOfMeasure.text(), oCurrentLocationDetails.batch_quantity);
							
							oTemplate.find('.batch-quantity').html(sConvertedQty + ' ' +  inputUnitOfMeasure.text());
							oTemplate.find('.batch-quantity').data('quantity', sConvertedQty);
							oTemplate.find('.batch-quantity').data('unit_of_measure', inputUnitOfMeasure.val());
						} else {
							oTemplate.find('.batch-quantity').html(oCurrentLocationDetails.batch_quantity + ' ' + oCurrentLocationDetails.unit_of_measure);
							oTemplate.find('.batch-quantity').data('quantity', oCurrentLocationDetails.batch_quantity);
							oTemplate.find('.batch-quantity').data('unit_of_measure', inputUnitOfMeasure.val());
						}	
					}

					if(inputRadioPiece.prop('checked')) {
						if('bag' != oCurrentLocationDetails.unit_of_measure) {
							var sConvertedQty = UnitConverter.convert(oCurrentLocationDetails.unit_of_measure, 'bag', oCurrentLocationDetails.batch_quantity);
							
							oTemplate.find('.batch-quantity').html(sConvertedQty + ' ' +  'bags');
							oTemplate.find('.batch-quantity').data('quantity', sConvertedQty);
							oTemplate.find('.batch-quantity').data('unit_of_measure', inputUnitOfMeasure.val());
						} else {
							oTemplate.find('.batch-quantity').html(oCurrentLocationDetails.batch_quantity + ' ' + 'bag');
							oTemplate.find('.batch-quantity').data('quantity', oCurrentLocationDetails.batch_quantity);
							oTemplate.find('.batch-quantity').data('unit_of_measure', 8);
						}
					}
					// CONVERTION OF UNIT OF MEASURE VALUES.
					

					uiParentDiv.append(oTemplate);
					uiParentDiv.data('breadcrumb', oLocations);
					// _UI_DisplayTotalQtyToTransfer();
				}	
			},
			"batch_destination" : function() {
				var uiParentDiv = $('#company_create_add_batch_sub_location'),
				iLocationLength = Object.keys(oLocations).length,
				sDestinationCrumb = "";

				for(var a in oLocations) {
					var iChecker = parseInt(a) + parseInt(1);
					if(iChecker == iLocationLength) {
						sDestinationCrumb += '<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> ' + oLocations[a].name + '  </p>';
					} else {
						sDestinationCrumb += '<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> ' + oLocations[a].name + '  </p>'
						+ '<i class="display-inline-mid font-14 padding-right-10">&gt;</i>';	
					}
				}

				sDestinationCrumb += '<div class="clear"></div>';
			},
			"ongoing_batches" : function() {

			}
		}

		if (typeof (oElement[oParams.page]) == "function") 
		{
			oElement[oParams.page](oParams.page);
		}
	}

	function _UI_DisplayTotalQtyToTransfer(oParams) 
	{
		var sPage = oParams.page,
		iProductId = oParams.product_id;

		oElement = {
			"company_ongoing_edit" : function() {
				var uiProductList = $('.edit-product-list'),
				uiParentDiv = uiProductList.find('.product-list-div[data-id="' + iProductId + '"]'),
				uiTotalQtyToTransfer = uiParentDiv.find('.total-qty-to-transfer'),
				uiNewBatches = uiParentDiv.find('.product-batch-cloned'),
				iTotalQtyToTransfer = 0;

				uiNewBatches.each(function(){
					var thisBatch = $(this),
					iQty = thisBatch.data('converted_qty'),
					sUnitOfMeasure = thisBatch.data('unit_of_measure_name');

					iTotalQtyToTransfer = parseFloat(iTotalQtyToTransfer) + parseFloat(iQty);
					iTotalQtyToTransfer.toFixed(2);
					uiTotalQtyToTransfer.html(iTotalQtyToTransfer + ' ' + sUnitOfMeasure);
				});
			}
		}

		if (typeof (oElement[sPage]) == "function") 
		{
			oElement[sPage](sPage);
		}
	}

	//==================================================//
	//================END OF UI METHODS=================//
	//==================================================//
	return {
		getAllProducts : getAllProducts,
		getAllTransfers : getAllTransfers,
		bindEvents : bindEvents,
		_getTransferBatchList : _getTransferBatchList,
		_getTransferRecord : _getTransferRecord
	};
	
})();

$(document).ready(function() {
	CTransferCompanyGoods.bindEvents();
});