var CStorages = (function() {
	var oStorages = null,
		oHierarchyFetchedData = {},
		iCurrentEditingHierarchy = 0,
		oSavedDataHierarchy = {},
		failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg";


	// ================================================== //
	// ==================PUBLIC METHODS================== //
	// ================================================== //

	function bindEvents() {
		var sCurrentPath = window.location.href;

		if(sCurrentPath.indexOf("storage_location_management") > -1) {
			_UI_storageLocationManagement(oStorages);

			$(".view_storage_details").off("click").on("click", function() {
				_setCurrentStorage($(this).data("id"));
			});
		}
		
		if(sCurrentPath.indexOf("add_storage_location_management") > -1) {
			_getStorageType();

			var uploadInstance = $('#upload_photo').cr8vUpload({
				url : BASEURL + "storages/upload",
				// backgroundPhotoUrl : "",
				// titleText : "Change Photo",
				accept : "jpg,png",
				autoUpload : false,
				onSelected : function(bValid){
					if(bValid){
						cr8v_platform.localStorage.set_local_storage({
							name : "add_product_image",
							data : {value : "true"}
						});
					}else{
						cr8v_platform.localStorage.set_local_storage({
							name : "add_product_image",
							data : {value : "false"}
						});
					}
				},
				success : function(data, textStatus, jqXHR){
					_add(data.data.location);
					// alert(data.data.path);
				},
				error : function(jqXHR, textStatus, errorThrown){
					
				}
			});

			$("#submitAddStorage").off("click").on("click", function() {
				$("#addStorageForm").submit();
			});

			$("#addStorageForm").cr8vformvalidation({
				'preventDefault' : true,
			    'data_validation' : 'datavalid',
			    'input_label' : 'labelinput',
			    'onValidationError': function(arrMessages) {
			        // alert(JSON.stringify(arrMessages)); //shows the errors
		        	$("body").feedback({title : "Error!", message : "Input is not valid.", icon : failIcon});
		        },
			    'onValidationSuccess': function() {
		        	uploadInstance.startUpload();
		        }
			});
		}

		if(sCurrentPath.indexOf("view_storage_location") > -1) {
			_UI_viewStorageLocation();
		}

		if(sCurrentPath.indexOf("storage_location_edit_new_storage") > -1) {
			var uploadInstance = $('#upload_photo').cr8vUpload({
				url : BASEURL + "products/upload",
				// backgroundPhotoUrl : "",
				// titleText : "Change Photo",
				accept : "jpg,png",
				autoUpload : false,
				onSelected : function(bValid){
					if(bValid){
						cr8v_platform.localStorage.set_local_storage({
							name : "add_product_image",
							data : {value : "true"}
						});
					}else{
						cr8v_platform.localStorage.set_local_storage({
							name : "add_product_image",
							data : {value : "false"}
						});
					}
				},
				success : function(data, textStatus, jqXHR){
					_add(data.data.location);
					// alert(data.data.path);
				},
				error : function(jqXHR, textStatus, errorThrown){
					
				}
			});
			_UI_editStorageLocation();

			$("#submit_edit_storage").off("click").on("click", function() {
				// alert("test");
				_edit();
			});

			
		}

		/* STORAGE HIERARCHY EVENTS */

		var uiAnchorAdd = $('[modal-target="add-storage-hierarchy"]');

		var _hierarchyEvents = function(oHierarchyData, mode){
			var inputName = $('#hierarchyName'),
				hierarchyDisplay = $('#hierarchyDisplay'),
				plusHierarchy = $('#plusHierarchy'),
				subtractHierarchy = $('#subtractHierarchy'),
				moveupHierarchy = $('#moveupHierarchy'),
				movedownHierarchy = $('#movedownHierarchy'),
				submitHierarchy = $('#addHierarchy'),
				sDefaultName = "Level";
				oAllHierarchy = (oHierarchyData) ? oHierarchyData : { 0 : sDefaultName + " 1" },
				$template = $('<p data-position="1" class="font-400 border-bottom-medium padding-bottom-10 padding-top-5 "><span>1.</span> <input type="text" first-value="Level 1" value="Level 1" class="width-200px remove-border-radius "></p>'),
				uiFirstClone = $template.clone(),
				cloneInput = uiFirstClone.find("input"),
				oDeletedHierarchy = {};

			subtractHierarchy.prop("disabled", true);

			submitHierarchy.html("Add Hierarchy");

			hierarchyDisplay.html(uiFirstClone);

			var methods = {
				"editHierarchy" : function(ui){
					var uiParent = ui,
						uiThisSpan = uiParent.find('span'),
						sPosition = uiParent.attr('data-position'),
						uiInputHierarchy = uiParent.find('input');

					if(mode == "create"){
						uiThisSpan.html(sPosition+". ");
						uiInputHierarchy.show();
					}else if(mode == "edit"){

					}
				},
				"editHierarchyEvent" : function(uiInput){
					var uiParent = uiInput.closest('p[data-position]'),
						iPosition = parseInt(uiParent.attr('data-position')) - 1,
						value = uiInput.val(),
						sFirstValue = uiInput.attr('first-value');
					
					if(mode == "create"){
						oAllHierarchy[iPosition] = value;
					}else if(mode == "edit"){
						oAllHierarchy[iPosition]['name'] = value;
					}
					console.log(oAllHierarchy);
				},
				"focusOut" : function(uiInput){
					var uiParent = uiInput.closest('p[data-position]'),
						iPosition = parseInt(uiParent.attr('data-position')) - 1,
						value = uiInput.val(),
						sFirstValue = uiInput.attr('first-value');

					if(uiInput.val().trim().length == 0){
						uiInput.val(sFirstValue);
					}
				},
				"listHierarchy" : function(){
					hierarchyDisplay.html("");
					for(var i in oAllHierarchy){
						var len = parseInt(i),
							uiTemplateClone = $template.clone(),
							uiInput = uiTemplateClone.find('input'),
							uiSpan = uiTemplateClone.find('span'),
							uiParent = uiTemplateClone,
							sPosition = uiParent.attr('[data-position]');
						
						if(mode == 'create'){
							uiSpan.html((len + 1)+".");
							uiInput.val(oAllHierarchy[i]);
							uiInput.attr('first-value', oAllHierarchy[i]);
							uiParent.attr('data-position', (len + 1));
						}else if(mode == 'edit'){
							uiSpan.html((len + 1)+".");
							uiInput.val(oAllHierarchy[i]['name']);
							uiInput.attr('first-value', oAllHierarchy[i]['name']);
							if(oAllHierarchy[i]['id']){
								uiInput.attr('hier-id', oAllHierarchy[i]['id']);
							}else if(oAllHierarchy[i]['parent_id']){
								uiInput.attr('parent-id', oAllHierarchy[i]['parent_id']);
							}
							uiParent.attr('data-position', (len + 1));
						}

						uiInput.off("keyup").on("keyup", function(e){
							methods.editHierarchyEvent($(this));
						});

						uiInput.off('focusout').on('focusout', function(){
							methods.focusOut($(this));
						});

						uiParent.off('dblclick.editHierarchy').on('dblclick.editHierarchy', function(){
							methods.editHierarchy($(this));
						});

						hierarchyDisplay.append(uiTemplateClone);
					}
				}
			};

			uiFirstClone.off('dblclick.editHierarchy').on('dblclick.editHierarchy', function(){
				methods.editHierarchy(uiFirstClone);
			});

			cloneInput.off("keyup").on("keyup", function(e){
				methods.editHierarchyEvent($(this));
			});

			cloneInput.off('focusout').on('focusout', function(){
				methods.focusOut($(this));
			});

			plusHierarchy.off("click.plusHierarchy").on("click.plusHierarchy", function(){
				$(this).prop('disabled', true);

				var len = Object.keys(oAllHierarchy).length,
					uiTemplateClone = $template.clone(),
					uiInput = uiTemplateClone.find('input'),
					uiSpan = uiTemplateClone.find('span'),
					uiParent = uiTemplateClone,
					sPosition = uiParent.attr('[data-position]');

				if(mode == 'create'){
					oAllHierarchy[len] = sDefaultName + " " +(len + 1);
				}else if(mode == 'edit'){
					var beforeCurrentHier = oAllHierarchy[len - 1];
					if(beforeCurrentHier){
						var iID = beforeCurrentHier['id'];
						iID = (iID) ? iID : "refer";
						oAllHierarchy[len] = {
							'name' : sDefaultName + " " +(len + 1),
							'parent_id' :  iID
						};
					}else{
						oAllHierarchy[0] = {
							'name' : sDefaultName + " " +1,
							'id' : 0
						};
					}
				}

				methods.listHierarchy();

				$(this).prop('disabled', false);
			});

			subtractHierarchy.off("click.subtractHierarchy").on("click.subtractHierarchy", function(){
				$(this).prop('disabled', true);

				var len = Object.keys(oAllHierarchy).length,
					index = parseInt(len) - 1,
					uiParent = $('p[data-position="'+len+'"]'),
					uiInput = uiParent.find('input'),
					uiSpan = uiParent.find('span'),
					sPosition = uiParent.attr('data-position');

				if(mode == 'edit' && (oAllHierarchy[index].hasOwnProperty("id"))){
					oDeletedHierarchy[Object.keys(oDeletedHierarchy).length] = oAllHierarchy[index];
				}

				delete oAllHierarchy[index];

				uiParent.remove();

				$(this).prop('disabled', false);
			});

			submitHierarchy.off("click.submitHierarchy").on("click.submitHierarchy", function(){
				if(inputName.val().trim().length > 0 && Object.keys(oAllHierarchy).length > 0){
					var oData = { name : inputName.val(), hierarchy : JSON.stringify(oAllHierarchy) },
						sUrl = "";

					if(mode == "create"){
						sUrl = BASEURL + "Storages/add_storage_hierarchy";
					}else if(mode == "edit"){
						sUrl = BASEURL + "Storages/update_storage_hierarchy";
						oData.hierarchy_id = iCurrentEditingHierarchy;
						oData.deleted_hierarchy = JSON.stringify(oDeletedHierarchy);
					}

					var oOptions = {
						type : "POST",
						data : oData,
						url : sUrl,
						success : function(oResult) {
							if(oResult.status){
								var options = {
								  form : "box",
								  autoShow : true,
								  type : "success",
								  title  : "Hierarchy",
								  message : "Data added",
								  speed : "slow",
								  withShadow : true
								};
								$("body").feedback(options);

								$('.modal-close.close-me:visible').trigger("click");

								CStorages.get();
							}else{
								var options = {
								  form : "box",
								  autoShow : true,
								  type : "danger",
								  title  : "Invalid Format",
								  message : oResult.message,
								  speed : "slow",
								  withShadow : true,
								  icon :  BASEURL + "assets/js/plugins/feedback/images/fail.svg"
								};
								$("body").feedback(options);
							}
						}
					}
					
					cr8v_platform.CconnectionDetector.ajax(oOptions);
				}else{
					var options = {
					  form : "box",
					  autoShow : true,
					  type : "danger",
					  title  : "Invalid Format",
					  message : "Please add name and hierarcy storage",
					  speed : "slow",
					  withShadow : true,
					  icon :  BASEURL + "assets/js/plugins/feedback/images/fail.svg"
					};
					$("body").feedback(options);
				}
			});
	
			if(mode == 'edit'){
				submitHierarchy.html("Update Hierarchy");
				methods.listHierarchy();
			}else{
				inputName.val("");
			}

		}


		uiAnchorAdd.off("click.addhierarchy").on("click.addhierarchy", function(){
			_hierarchyEvents(undefined, "create");
		});

		$('body').off('click.editHierarchy').on('click.editHierarchy', 'button[edit-hierarchy-storage]', function(e){
			var uiBtn = $(this),
				sId = uiBtn.attr('edit-hierarchy-storage'),
				inputName = $('#hierarchyName');

			$("body").css({overflow:'hidden'});
			$("div[modal-id='add-storage-hierarchy']").addClass("showed");

			$("div[modal-id='add-storage-hierarchy'] .close-me").off('click.closemodal').on("click.closemodal",function(){
				$("body").css({'overflow-y':'initial'});
				$("div[modal-id='add-storage-hierarchy']").removeClass("showed");
				$('#hierarchyTrashContainer').hide();
			});

			$('#hierarchyTrashContainer').show();
			var editData = {};
			for(var i in oHierarchyFetchedData){
				if(oHierarchyFetchedData[i]['id'] == sId){
					iCurrentEditingHierarchy = parseInt(sId);
					editData = oHierarchyFetchedData[i];
				}
			}

			var oHierNames = {};
			var len = Object.keys(oHierNames).length;
			
			oHierNames[len] = {
				'name' : editData['hierarchy_name'],
				'id' : editData['id']
			};
			
			var functionGetChildren = function(oChildren){
				for(var n in oChildren){
					var lenInner = Object.keys(oHierNames).length;
					oHierNames[lenInner] = {
						'name' : oChildren[n]['hierarchy_name'],
						'id' : oChildren[n]['id']
					};
					if(oChildren[n]['children'].length > 0){
						functionGetChildren(oChildren[n]['children']);
					}
				}
			};
			
			if(editData['children'].length > 0){
				functionGetChildren(editData['children']);
			}

			$('#hierarchyName').val(editData['main_name']);

			_hierarchyEvents(oHierNames, "edit");

			var uiTrash = $('#hierarchyTrashContainer').find("i.default-cursor.gray-color");
			uiTrash.off("click.trashstorage").on("click.trashstorage", function(){
				var oOptions = {
					type : "POST",
					data : {
						main_hierarchy : iCurrentEditingHierarchy
					},
					url : BASEURL + "Storages/delete_storage_hierarchy",
					success : function(oResult) {
						if(oResult.status){
							var options = {
							  form : "box",
							  autoShow : true,
							  type : "success",
							  title  : "Hierarchy",
							  message : "Data deleted",
							  speed : "slow",
							  withShadow : true
							};
							$("body").feedback(options);

							$('.modal-close.close-me:visible').trigger("click");

							CStorages.get();
						}
					}
				}
				
				cr8v_platform.CconnectionDetector.ajax(oOptions);
			});

		});

		/* END OF STORAGE HIERARCHY EVENTS */
	}

	function getAllStorages() {
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "storages/get_all_storages",
			success : function(oResult) {
				CStorages.setAllStorage(oResult);
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function get() {
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "Storages/get_hierarchy",
			success : function(oResult) {
				if(oResult.status){
					oHierarchyFetchedData = oResult.data;
					_displayStorageHierarchy(oResult.data);
				}
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function setAllStorage(oResult) {
		oStorages = oResult;
		_setStorageList(oStorages);
		return oStorages;
	}

	// ================================================== //
	// ===============END OF PUBLIC METHODS============== //
	// ================================================== //

	// ================================================== //
	// =================PRIVATE METHODS================== //
	// ================================================== //


	function _generateHierarchyChildrenNames(oData){
		var arrResponse = [];

		for(var i in oData){
			arrResponse.push(oData[i]['hierarchy_name']);
			if(oData[i]['children'].length > 0){
				arrResponse.push(_generateHierarchyChildrenNames(oData[i]['children']));
			}
		}

		return arrResponse;

	}

	function _displayStorageHierarchy(oData){
		var uiContainer = $('div.hierarchy-table'),
			uiTable = uiContainer.find('table'),
			uiSiblings = uiTable.siblings();

		uiSiblings.remove();
		for(var i in oData){
			var arrNamesChildren = [];
			arrNamesChildren.push(oData[i]['hierarchy_name']);
			for(var x in oData[i]['children']){
				var arrChildreNames = _generateHierarchyChildrenNames(oData[i]['children']);
				arrNamesChildren.push(arrChildreNames);
			}

			var sTemplate = '<div class="table-content position-rel">'+
					'<div class="content-show height-auto width-100per padding-top-20 padding-bottom-20 ">'+
						'<div class="text-center width-50per display-inline-mid">'+
							'<p>'+oData[i].main_name+'</p>'+
						'</div>'+
						'<div class="text-center width-50per display-inline-mid  ">'+
							'<p class="font-12">'+arrNamesChildren.join(',')+'</p>'+
						'</div>'+
					'</div>'+
				'<div>'+

				'<div class="content-hide" style="height: 56px;">'+
					'<button class="btn general-btn modal-trigger" modal-target="edit-storage-hierarchy" edit-hierarchy-storage="'+oData[i]['id']+'">Edit Storage Hierarchy</button>'+
				'</div>';

			var uiTemplate = $(sTemplate);
			uiTemplate.insertAfter(uiTable);
		}

		/* FOR ADD STORAGE SELECT HIERARCHY DROPDOWN UI */
		var uiSelect = $("#hierarchySelect"),
			uiSiblings = uiSelect.siblings();

		uiSiblings.remove();
		uiSelect.removeClass("frm-custom-dropdown-origin");
		uiSelect.html("");
		for(var i in oData){
			var sOption = '<option value="'+oData[i]['id']+'">'+oData[i]['main_name']+'</option>';
			uiSelect.append(sOption);
		}

		uiSelect.transformDD();


		/* FOR ADD STORAGE SPECIFICATION HIERARCHY EVENT */
		
		var storageSpecMethods = {

			selectedPosition : 0,

			topTemplate : function(){
				return '<div class="panel-heading  acc-dark-color hover-under-heading">'+
					'<a class="collapsed " href="javascript:void(0)">'+
						'<i class="fa fa-caret-down font-20 black-color"></i>'+
						'<h4 class="panel-title  font-15 active black-color font-500 first-acc margin-left-5 name"></h4>	'+								
					'</a>'+								
				'</div>';
			},

			lowerTemplate : function(){
				return '<div class="panel-collapse collapse in">'+
					'<div class="panel-body no-padding-all">'+
						'<div class="sample-acc">'+
						'</div>'+
					'</div>'+
				'</div>';
			},

			listTemplate : function(){
				return '<div class="acc-light-color padding-all-10  item-on-hover">'+
							'<i class="fa fa-caret-down font-20 display-inline-mid margin-right-10"></i>'+
							'<p class="display-inline-mid margin-right-10 font-15 font-500  black-color name"></p> '+
							'<p class="italic gray-color display-inline-mid margin-right-10 description"></p>'+
						'</div>';
			},

			ui : {
				displayContainer : [],
				addListHierarchy : [],
				removeListHierarchy : []
			},

			selectedHierarchy : "",

			hierarchyData : {},

			setHierarchyData : function(){
				for(var i in oHierarchyFetchedData){
					if(oHierarchyFetchedData[i]["id"] == storageSpecMethods.selectedHierarchy){
						storageSpecMethods.hierarchyData = oHierarchyFetchedData[i];
					}
				}

				console.log(storageSpecMethods.hierarchyData);
			},

			onSelectEvent : function(){
				$("#hierarchySelect").off("change").on("change", function(){
					storageSpecMethods.selectedHierarchy = $("#hierarchySelect").val();
					storageSpecMethods.setHierarchyData();
					storageSpecMethods.ui.displayContainer.html("");
					storageSpecMethods.selectedPosition = 0;
					oSavedDataHierarchy = {};
				});
			},

			makeid : function()
			{
			    var text = "";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			    for( var i=0; i < 5; i++ )
			        text += possible.charAt(Math.floor(Math.random() * possible.length));

			    return text;
			},

			addEvent : function(){
				var add = storageSpecMethods.ui.addListHierarchy,
					remove = storageSpecMethods.ui.removeListHierarchy;

				add.off("click.addhierarchylist").on("click.addhierarchylist", function(){
					var ui = storageSpecMethods.ui,
						$topTemplate = $(storageSpecMethods.topTemplate()),
						$lowerTemplate = $(storageSpecMethods.lowerTemplate()),
						oData = storageSpecMethods.hierarchyData;

					if(storageSpecMethods.selectedPosition === 0){
						$topTemplate.hover(function(){
							$('body').css('cursor', 'pointer');
						}, function(){
							$('body').css('cursor', 'default');
						});

						$topTemplate.data("hierarchyPosition", 1);

						$topTemplate.off('click.selectedlist').on('click.selectedlist', function(){
							$(this).css({
								"background-color" : "rgb(255, 239, 189)"
							});

							$('.active-hierarchy-list').css("background-color", "");
							$('.active-hierarchy-list').removeClass('active-hierarchy-list');
							
							$(this).addClass('active-hierarchy-list');
							storageSpecMethods.selectedPosition = 1;
						});

						$topTemplate.find('.name').html(oData['hierarchy_name']);
						$topTemplate.appendTo(ui.displayContainer);
						storageSpecMethods.selectedPosition = -1;

						var selfID = storageSpecMethods.makeid();
						oSavedDataHierarchy[Object.keys(oSavedDataHierarchy).length] = { "name" :  oData['hierarchy_name'], "locations" : {}, "selfID" : selfID };
						$topTemplate.data("selfID", selfID);

					}else{
						var oHierNames = {};
						var len = Object.keys(oHierNames).length;

						oHierNames[len] = {
							'name' : oData['hierarchy_name'],
							'id' : oData['id']
						};

						var functionGetChildren = function(oChildren){
							for(var n in oChildren){
								var lenInner = Object.keys(oHierNames).length;
								oHierNames[lenInner] = {
									'name' : oChildren[n]['hierarchy_name'],
									'id' : oChildren[n]['id']
								};
								if(oChildren[n]['children'].length > 0){
									functionGetChildren(oChildren[n]['children']);
								}
							}
						};

						if(oData['children'].length > 0){
							functionGetChildren(oData['children']);
						}

						if(oHierNames[storageSpecMethods.selectedPosition]){
							var $active = $('.active-hierarchy-list'),
								parentSelfID = $('.active-hierarchy-list').data("selfID"),
								oSaveData = {};

							if($active.data("hierarchyPosition") == 1){
								$lowerTemplate.insertAfter($active);
							}

							var uiListInside = $lowerTemplate.find('.sample-acc'),
								oDataHier = oHierNames[storageSpecMethods.selectedPosition],
								$listTemplate = $(storageSpecMethods.listTemplate()),
								marginLeft = (storageSpecMethods.selectedPosition * 10) + 5;

							$listTemplate.find('i').css("margin-left", marginLeft+"px");
							$listTemplate.find('.name').html(oDataHier.name);

							$listTemplate.data("hierarchyPosition", (storageSpecMethods.selectedPosition + 1));

							$listTemplate.off('click.selectedlist').on('click.selectedlist', function(){
								$(this).css({
									"background-color" : "rgb(255, 239, 189)"
								});

								$('.active-hierarchy-list').css("background-color", "");
								$('.active-hierarchy-list').removeClass('active-hierarchy-list');
								
								$(this).addClass('active-hierarchy-list');
								var dataPosition = $listTemplate.data("hierarchyPosition");
								storageSpecMethods.selectedPosition = dataPosition;
							});

							$listTemplate.hover(function(){
								$('body').css('cursor', 'pointer');
							}, function(){
								$('body').css('cursor', 'default');
							});

							var newSelfID = storageSpecMethods.makeid();

							$listTemplate.data('selfID' , newSelfID);

							var insertHierarchyData = function(parentID, oDataHierarchy, oSaveData){
								var oLoopData = (oDataHierarchy) ? oDataHierarchy : oSavedDataHierarchy;

								for(var i in oLoopData){
									if(parentID == oLoopData[i]['selfID']){
										var lent = Object.keys(oLoopData[i]['locations']).length,
											bSave = true;
										for(var n in oLoopData[i]['locations']){
											if(oLoopData[i]['locations'][n]['selfID'] == newSelfID){
												bSave = false;
											}
										}
										if(bSave){
											oLoopData[i]['locations'][lent] = { "name" :  oDataHier.name, "locations" : {}, "selfID" : newSelfID };
										}
										break;
									}else{
										if(Object.keys(oLoopData[i]['locations']).length > 0){
											for(var x in oLoopData[i]['locations']){
												insertHierarchyData(parentID, oLoopData[i]['locations']);
											}
										}
									}
								}
							};
 
							insertHierarchyData(parentSelfID, undefined, oSaveData);

							console.log(oSavedDataHierarchy);

							if($active.data("hierarchyPosition") == 1){
								uiListInside.append($listTemplate);
							}else{
								$listTemplate.insertAfter($active);
							}

							console.log(oSaveData);
						}
					}
				});

				$('body').off('click.selectHierarchyList').on('click.selectHierarchyList', function(e){
					if(!$(e.target).hasClass('active-hierarchy-list')){
						if( ($(e.target).attr('id') != 'addListHierarchy') ){
							$('.active-hierarchy-list').css("background-color", "");
							$('.active-hierarchy-list').removeClass('active-hierarchy-list');
							storageSpecMethods.selectedPosition = 0;
						}
					}
				});

				remove.off("click.removehierarchylist").on("click.removehierarchylist", function(){
					if($('.active-hierarchy-list').length > 0){
						var uiActive = $('.active-hierarchy-list'),
							activeHierarchyPosition = uiActive.data('hierarchyPosition'),
							uiSiblings = $('.active-hierarchy-list').siblings(),
							thisSelfID = "";


						thisSelfID = uiActive.data("selfID");

						var removeHierarchyData = function(selfId, oDataHierarchy){
							var oLoopData = (oDataHierarchy) ? oDataHierarchy : oSavedDataHierarchy;
							for(var i in oLoopData){
								if(selfId == oLoopData[i]['selfID']){
									delete oLoopData[i];
								}else{
									if(Object.keys(oLoopData[i]['locations']).length > 0){
										for(var x in oLoopData[i]['locations']){
											removeHierarchyData(thisSelfID, oLoopData[i]['locations']);
										}
									}
								}
							}
						};

						removeHierarchyData(thisSelfID);

						$.each(uiSiblings, function(){
							if($(this).data('hierarchyPosition') > activeHierarchyPosition){
								$(this).remove();
							}
							if($(this).hasClass(".panel-heading")){
								return false;
							}
						});
						$('.active-hierarchy-list').remove();

						console.log(oSavedDataHierarchy);
					}
				});
			},


			init : function(){
				storageSpecMethods.onSelectEvent();
				storageSpecMethods.selectedHierarchy = $("#hierarchySelect").val();
				storageSpecMethods.setHierarchyData();
				storageSpecMethods.ui.displayContainer = $("#storageHIerarchyListDisplay");
				storageSpecMethods.ui.addListHierarchy = $("#addListHierarchy");
				storageSpecMethods.ui.removeListHierarchy = $("#removeListHierarchy");
				storageSpecMethods.ui.displayContainer.html("");
				storageSpecMethods.addEvent();
			}

		};

		storageSpecMethods.init();

		/* END FOR ADD STORAGE SPECIFICATION HIERARCHY EVENT */
	}

	function _render(oParams) {
		// console.log(oParams);
		var oElement = {
			"storage_location_management" : function(oData) 
			{
				$("#storage_location_list").append(oParams.data);
			},
			"add_storage_location_management" : function(oData) 
			{
				$("#edit_storage")
			},
			"view_storage_location" : function(oData) {
				var uiStorage = oParams.data;
				$("#view_storage_id").html(uiStorage.id);
				$("#view_storage_name").html(uiStorage.name);
				$("#view_storage_address").html(uiStorage.address);
				$("#view_storage_description").html(uiStorage.description);
				$("#view_storage_image").attr("src", uiStorage.image);
			},
			"storage_location_edit_new_storage" : function(oData) {
				var uiStorage = oParams.data;
				var uiImage = uiStorage.image.indexOf("view_storage_location") > -1
				$("#edit_storage_id").val(uiStorage.id);
				$("#edit_storage_name").val(uiStorage.name);
				$("#edit_storage_address").val(uiStorage.address);
				$("#crumb_edit_storage_id").html("ID " + uiStorage.id);
				$("#crumb_edit_storage_name").html(uiStorage.name);
				$("#submit_edit_storage").attr("data-id", uiStorage.id);
				// $('.upload-wrapper').css('background-image', 'url(http://localhost/apollo/uploads/images/XgHcQ1EV5RPjVEhWshZm.jpg)');
				$('.upload-wrapper').css('background-image', 'url(' + uiStorage.image + ')');
				_getStorageType();
			},
			"storageType" : function(oData) {
				console.log(oParams.data);
				var uiSelect = $("#storageType");
				var uiSiblings = uiSelect.siblings();
				uiSiblings.remove();
				uiSelect.removeClass("frm-custom-dropdown-origin");
				$("#storageType").html(oParams.data);
				$("#storageType").transformDD();
			}
		}

		if (typeof (oElement[oParams.name]) == "function") 
		{
			oElement[oParams.name](oParams.name);
		}
	}

	function _add(sImage) {
		var oOptions = {
			type : "POST",
			data : { 
				name : $("#storageName").val(),
				address : $("#storageAddress").val(),
				company_id : 1,
				storage_type : $("#storageType option:selected").val(),
				image : sImage,
				description : $("#storageDescription").val(),
				locations : JSON.stringify(oSavedDataHierarchy)
			},
			url : BASEURL + "storages/add",
			success : function(oResult) {
				if(oResult > 0) {
					var oFeedback = { title : "Success!", message : "Storage added.", speed : "slow" };
					$("body").feedback(oFeedback);
				} else {
					var oFeedback = { title : "Fail!", message : "Storage not added.", speed : "slow", icon : failIcon};
					$("body").feedback(oFeedback);
				}
			}
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _edit() {
		var oOptions = {
			type : "POST",
			data : {
				id : $("#submit_edit_storage").data("id"),
				name : $("#edit_storage_name").val(),
				address : $("#edit_storage_address").val(),
				storage_type : $("#storageType option:selected").val()
			},
			url : BASEURL + "storages/edit",
			success : function(oResult) {
				if(oResult > 0) {
					var oFeedback = { title : "Success!", message : "Storage updated.", speed : "slow" };
					$("body").feedback(oFeedback);
				} else {
					var oFeedback = { title : "Fail!", message : "Storage not updated.", speed : "slow", icon : failIcon};
					$("body").feedback(oFeedback);
				}
				// console.log(oResult);
			}
		};
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _buildTemplate(oParams) {
		var oTemplate = {},
		oElement = {
			"storage_location_management" : function(oData) 
			{
				var sTemplate_head = "",
				sTemplate_content = "",
				sTemplate_footer = "";

				sTemplate_head = '<div class="tbl-like">'
				+	'<div class="first-tbl height-auto text-left">';
							// <p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">123456</p>
							// <p class="width-25percent font-400 font-14 display-inline-mid f-none text-center ">Warehouse 1</p>
							// <p class="width-15percent font-400 font-14 display-inline-mid f-none text-center ">Solid, Liquid, Gas</p>
							// <p class="width-15percent font-400 font-14 display-inline-mid f-none text-center ">Warehouse</p>
							// <p class="width-28percent font-400 font-14 display-inline-mid f-none text-center ">123 Something something St., Kapuso Sub., Siyudad City</p>
				sTemplate_content = '</div>'
				+	'<div class="hover-tbl height-100per">'
				+		'<a href="' + BASEURL + 'storage_location_management/view_storage_location">';
								// <button class="btn general-btn ">View Storage Settings</button>
				sTemplate_footer =	'</a>'
				+	'</div>'
				+'</div>';
				oTemplate = {
					head : sTemplate_head,
					content : sTemplate_content,
					footer : sTemplate_footer
				};
			},
			"add_storage_location_management" : function(oData) 
			{

			},
			"view_storage_location" : function(oData) 
			{

			},
			"storage_location_edit" : function(oData) {

			}
		}

		if (typeof (oElement[oParams.name]) == "function") 
		{
			oElement[oParams.name](oParams.name);
		}
		return oTemplate;
	}

	function _setCurrentStorage(iStorage_id) {
		var oData = {};
		for(var a in oStorages) {
			if(oStorages[a].id == iStorage_id) {
				oData = {
					name : "currentStorage",
					data : oStorages[a]
				};
			}
		}
		
		cr8v_platform.localStorage.delete_local_storage({name : "currentStorage"});
		cr8v_platform.localStorage.set_local_storage(oData);
	}

	function _getCurrentStorage() {
		var oCurrentStorage = {};

		oCurrentStorage = cr8v_platform.localStorage.get_local_storage();
		return oCurrentStorage;
	}

	function _updateCurrentStorage() {
		getAllStorage(); //UPDATES THE LOCAL STORAGE WITH THE NEW DB RECORDS.
		var oCurrentStorage = _getCurrentStorage();

		for(var a in oStorages) {
			if(oStorages[a].id == current_product.id) {
				_setCurrentStorage(oStorages[a]);
			}
		}
	}

	function _getStorageType() {
		var oOptions = {
			type : "GET",
			data : {test : true},
			url : BASEURL + "storages/get_storage_type",
			success : function(oResult) {
				_UI_storageType(oResult);
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _setStorageList(oStorages) {
		var oStorageList = {};

		if(oStorages === null
		|| oStorages === undefined
		|| oStorages == "") {
			getAllStorages();
		}
		oStorageList = {name : "storageList", data : oStorages};
		cr8v_platform.localStorage.delete_local_storage({name : "storageList"});
		cr8v_platform.localStorage.set_local_storage(oStorageList);
		// console.log(oStorages);
		// testStorage = oStorages;
	}

	// ================================================== //
	// ==============END OF PRIVATE METHODS============== //
	// ================================================== //

	// ================================================== //
	// ====================UI METHODS==================== //
	// ================================================== //

	function _UI_storageLocationManagement() {
		var oTemplate = _buildTemplate({name : "storage_location_management"}),
		sStorageList = "";
		var arrStorages = cr8v_platform.localStorage.get_local_storage({name : "storageList"});

		for(var a in arrStorages) {
			sStorageList += oTemplate.head;
			sStorageList += '<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">' + arrStorages[a].id + '</p>'
			+	'<p class="width-25percent font-400 font-14 display-inline-mid f-none text-center ">' + arrStorages[a].name + '</p>'
			+	'<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center ">Solid, Liquid, Gas</p>'
			+	'<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center ">' + arrStorages[a].type_of_storage + '</p>'
			+	'<p class="width-28percent font-400 font-14 display-inline-mid f-none text-center ">' + arrStorages[a].address + '</p>';
			sStorageList += oTemplate.content;
			sStorageList += '<button class="btn general-btn view_storage_details" data-id="' + arrStorages[a].id + '">View Storage Settings</button>';
			sStorageList += oTemplate.footer;
		}

		_render({name : "storage_location_management", data : sStorageList});
	}

	function _UI_viewStorageLocation() {
		var currentStorage = {};
		currentStorage = cr8v_platform.localStorage.get_local_storage({name : "currentStorage"});
		_render({name : "view_storage_location", data : currentStorage});
	}

	function _UI_editStorageLocation() {
		var currentStorage = {};
		currentStorage = cr8v_platform.localStorage.get_local_storage({name : "currentStorage"});
		_render({name : "storage_location_edit_new_storage", data : currentStorage});
	}

	function _UI_storageType(oType) {
		var sElement = "";

		for(var a in oType) {
			sElement += '<option value="' + oType[a].id + '">' + oType[a].name + '</option>';
		}
		_render({name : "storageType", data : sElement});
	}

	// ================================================== //
	// ===============ENDS OF UI METHODS================= //
	// ================================================== //

	if(oStorages === null
	|| oStorages === undefined
	|| oStorages == "") {
		getAllStorages();
	}
	return {
		// buildTemplate : buildTemplate,
		getAllStorages : getAllStorages,
		bindEvents : bindEvents,
		get : get,
		setAllStorage : setAllStorage
	};
})();

$(document).ready(function() {
	CStorages.bindEvents();
	CStorages.get();
});