var CWithdrawalCompany = (function() {
	var oWithdrawals = {},
	oProducts = {},
	oProductBatches = {},
	oUnitOfMeasure = {},
	oLocations = {},
	oWithdrawalDocuments = {},
	oWithdrawalNotes = {},
	oWithdrawalDetails = {},
	oWithdrawalProducts = {},
	oCurrentDateTime = {},
	oUploadedDocuments = {},

	sGeneratedWithdrawalNumber = '',
	sCurrentPath = '',

	uiGenerateViewModal = $('.modal-generate-view'),
	uiBatchModal = $('.batch-modal'),
	uiNoteModal = $('.note-modal'),
	uiNoteTemplate = $('.note-template'),
	uiCompleteRecordModal = $('.complete-product-modal'),
	uiCompletedModal = $('.completed-record-modal'),
	uiUpdatedRecordModal = $('.modal-updated-record'),
	uiUpdateRecordModal = $('.confirm-edit-record'),

	failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg",
	successIcon = BASEURL + "assets/js/plugins/feedback/images/success.svg";

	function bindEvents() {
		sCurrentPath = window.location.href;
		
		if(sCurrentPath.indexOf('company_record_list') > -1) {
			var oParams = {
				page : 'record_list'
			};

			// GET ALL WITHDRAWALS.
			_getAllWithdrawals(function(oResult) {
				_render(oParams);
			});
		}

		if(sCurrentPath.indexOf('withdrawal_company_create') > -1) {
			var oParams = {
				page : 'create_record'
			};

			// GET ALL WITHDRAWAL PRODUCTS.
			_getAllWithdrawalProducts(function(oResult) {

				// GENERATE NEW WITHDRAWAL NUMBER.
				_generateWithdrawalNumber(function(oResult) {

					// GET ALL UNIT OF MEASURE.
					_getAllUnitOfMeasure(function(oResult) {

						// GET ALL LOCATIONS
						_getAllLocations(function(oResult) {
							_render(oParams);
							_documentEvents(true);
						});

					});
					
				});

			});
		}

		if(sCurrentPath.indexOf('withdrawal_company_ongoing') > -1) {
			var oWithdrawalId = cr8v_platform.localStorage.get_local_storage({name : 'current_withdrawal'}),
			oParams = {
				page : 'view_ongoing'
			};
			
			_getAllProducts(function(oResult) {

				_getWithdrawalDetails(oWithdrawalId, function(oResult) {
					
					// GET ALL UNIT OF MEASURE.
					_getAllUnitOfMeasure(function(oResult) {

						// GET ALL LOCATIONS
						_getAllLocations(function(oResult) {
							_render(oParams);
							_documentEvents(true);
						});

					});

				});

			});		
		}

		if(sCurrentPath.indexOf('withdrawal_company_completed') > -1) {
			var oWithdrawalId = cr8v_platform.localStorage.get_local_storage({name : 'current_withdrawal'}),
			oData = {
				page : 'view_completed'
			};

			_getAllProducts(function(oResult) {
				
				_getWithdrawalDetails(oWithdrawalId, function(oResult) {
					
					_getAllUnitOfMeasure(function(oResult) {

						_getAllLocations(function(oResult) {
							_render(oData);
						});
						
					});
					
				});

			});
		}

		if(sCurrentPath.indexOf('withdrawal_company_edit')> -1) {
			var oWithdrawalId = cr8v_platform.localStorage.get_local_storage({name : 'current_withdrawal'}),
			oData = {
				page : 'edit_record'
			};

			_getAllWithdrawalProducts(function(oResult) { 

				_getWithdrawalDetails(oWithdrawalId, function(oResult) {
					
					// GET ALL UNIT OF MEASURE.
					_getAllUnitOfMeasure(function(oResult) {

						// GET ALL LOCATIONS
						_getAllLocations(function(oResult) {
							_render(oData);
							_documentEvents(true);
						});

					});

				});

			});
		}
	}

	function _render(oParams) {

		var oElement = {

			'record_list' : function() {
				//SHOWS THE GENERATING VIEW MODAL.
				uiGenerateViewModal.addClass('showed');

				var oWithdrawal = {},
				uiWithdrawalList = $('.withdrawal-list-div'),
				uiOngoingTemplate = $('.withdrawal-ongoing-template'),
				uiCompletedTemplate = $('.withdrawal-complete-template'),
				uiClonedTemplate = {},
				iWithdrawalLength = 0,
				oTemplate = {};

				// LOOP THROUGH ALL WITHDRAWALS.
				if(typeof(oWithdrawals) == 'object') {
					iWithdrawalLength = parseInt(Object.keys(oWithdrawals).length) - parseInt(1);	

					for(var a in oWithdrawals) {
						var sProductList = '';

						oTemplate['template'] = oWithdrawals[a].status == 0 ? uiOngoingTemplate : uiCompletedTemplate;
						oTemplate['details'] = oWithdrawals[a];
						oWithdrawalProducts = oWithdrawals[a].products;

						for(var b in oWithdrawalProducts) {
							sProductList += '<li>#' + oWithdrawalProducts[b].product_sku + ' - ' + oWithdrawalProducts[b].product_name + '</li>'
							
						}

						uiClonedTemplate = _buildRecordDiv(oTemplate);
						uiClonedTemplate.find('.withdrawal-item-list').html(sProductList);
						uiWithdrawalList.append(uiClonedTemplate);

						if(a == iWithdrawalLength) {
							_closeGeneratingModal(500);
						}
					}

				} else {

					uiGenerateViewModal.find('.modal-message').html('No Records found.');
					uiGenerateViewModal.addClass('showed');
					_closeGeneratingModal(1500);
				}
			},

			'create_record' : function() {
				//SHOWS THE GENERATING VIEW MODAL.
				uiGenerateViewModal.addClass('showed');

				var btnAddItem = $('.btn-add-item'),
				btnCreateWithdrawal = $('.btn-create-withdrawal'),
				btnCloseNoteModal = uiNoteModal.find('.close-note'),
				btnSubmitNote = uiNoteModal.find('.submit-note'),
				uiWithdrawalNumber = $('.withdrawal-number'),
				uiProductTemplate = $('.product-original-template'),
				uiProductList = $('.product-list'),
				uiClonedTemplate = {},
				iSelectedProduct = null;

				uiWithdrawalNumber.html('Withdrawal No . ' + sGeneratedWithdrawalNumber);
				uiWithdrawalNumber.data('withdrawal_number', sGeneratedWithdrawalNumber);
				_buildProductDropDown();
				_closeGeneratingModal(500);

				btnAddItem.off('click').on('click', function(e) {
					e.preventDefault();
					var oData = {
						product_template : uiProductTemplate,
						product_id : $('.product-list-dropdown').val()
					};

					uiClonedTemplate = _addItemToList(oData);
					uiClonedTemplate.attr('data-id', oData.product_id);
					uiProductList.append(uiClonedTemplate);

					var btnRemoveProduct = uiClonedTemplate.find('.remove-product'),
					btnBulk = uiClonedTemplate.find('.radio-bulk'),
					btnPiece = uiClonedTemplate.find('.radio-piece'),
					btnAddBatch = uiClonedTemplate.find('.btn-add-batch'),
					uiProductDiv = $('.product-list').find('.product-list-div[data-id="' + oData.product_id + '"]'),
					inputUnitOfMeasure = uiClonedTemplate.find('.unit-of-measure-dropdown');

					btnBulk.attr('name', 'bag-or-bulk-' + oData.product_id);
					btnPiece.attr('name', 'bag-or-bulk-' + oData.product_id);

					

					// REMOVES THE PRODUCT'S DIV
					btnRemoveProduct.off('click').on('click', function() {
						var uiParent = $(this).closest('.product-cloned-template');
						uiParent.remove();
						
						// REMOVES THE ALREADY SELECTED PRODUCT FROM IT'S DROPDOWN.
						_checkSelectedProducts({
							element : uiProductList,
							dropdown : $('.product-list-dropdown')
						});
						
					});

					// EVENT FOR CLICK THE BULK RADIO BUTTON
					btnBulk.off('click').on('click',function() {
						inputUnitOfMeasure.removeAttr('disabled');

						if(btnBulk.prop('checked') == true 
						|| btnPiece.prop('checked') == true) {
							btnAddBatch.removeClass('disabled');
						} else {
							btnAddBatch.addClass('disabled');
						}

						// SETS DATA ON THE PRODUCT DIV.
						uiProductDiv.data('loading_method', 1);
						uiProductDiv.data('unit_of_measure', inputUnitOfMeasure.val());

						// EVENT FOR UNIT OF MEASURE CHANGE...
						inputUnitOfMeasure.off('change').on('change', function() {
							uiProductDiv.data('unit_of_measure', inputUnitOfMeasure.val());
						});

					});

					btnBulk.trigger("click");
					btnAddBatch.removeClass('disabled');

					// EVENT FOR CLICK THE PIECE RADIO BUTTON
					btnPiece.off('click').on('click',function() {
						inputUnitOfMeasure.attr('disabled', true);

						if(btnBulk.prop('checked') == true 
						|| btnPiece.prop('checked') == true) {
							btnAddBatch.removeClass('disabled');
						} else {
							btnAddBatch.addClass('disabled');
						}

						// SETS DATA ON THE PRODUCT DIV.
						uiProductDiv.data('loading_method', 2);
						uiProductDiv.data('unit_of_measure', 8); // TEMPORARY, REPRESENTING BAG.

					});

					// EVENT FOR ADDING A BATCH.
					btnAddBatch.off('click').on('click', function() {
						var thisButton = $(this),
						uiParent = thisButton.closest('.product-list-div'),
						iProductId = uiParent.data('id');

						uiBatchModal.find(".modal-body").find(".modal-head").find(".text-left").html("Add Batch");

						// alert(iProductId);
						uiBatchModal.addClass('showed');
						uiBatchModal.data('product_id', iProductId);
						_buildBatchModal({page : 'create_record'});
					});

					// REMOVES THE ALREADY SELECTED PRODUCT FROM IT'S DROPDOWN.
					_checkSelectedProducts({
				  		element : uiProductList,
				  		dropdown : $('.product-list-dropdown')
					});
				});

				btnCreateWithdrawal.off('click').on('click', function() {
					//$('.confirm-create-record').addClass('showed');
					_createWithdrawalRecord();
				});

				btnCloseNoteModal.off('click').on('click', function() {
					_closeNote();
				});

				btnSubmitNote.off('click').on('click', function() {
					_addNote({page : 'create_record'});
				});
			},

			'edit_record' : function() {
				//SHOWS THE GENERATING VIEW MODAL.
				uiGenerateViewModal.addClass('showed');

				var btnCloseNoteModal = uiNoteModal.find('.close-note'),
				btnAddItem = $('.btn-add-item'),
				btnSubmitNote = uiNoteModal.find('.submit-note'),
				btnSubmitEdit = $('.btn-submit-edit-record'),
				btnConfirmEdit = $('.btn-confirm-edit-record'),
				btnCancelEdit = $('.btn-cancel-edit-record'),
				uiWithdrawalNumber = $('.withdrawal-number'),
				uiOldProductList = $('.old-product-list'),
				uiProductList = $('.product-list'),
				uiInputDiv = $('.input-div'),
				inputRequestor = $('.requestor'),
				inputDesignation = $('.designation'),
				inputDepartment = $('.department'),
				inputReasonForWithdrawal = $('.reason-for-withdrawal'),
				inputTextFields = $('.edit-input'),
				iDeletedProductCount = 0,
				oWithdrawalProducts = oWithdrawalDetails.products,
				iWithdrawalProductLength = parseInt(Object.keys(oWithdrawalProducts).length) - parseInt(1),
				oDeletedOldProducts = {},
				oProductElement = {},
				oNoteDetails = {
					notes : oWithdrawalDetails.notes
				},
				oDocumentDetails = {
					documents : oWithdrawalDetails.documents
				};

				_buildProductDropDown();
				_buildNoteDisplay(oNoteDetails);
				_buildDocumentDisplay(oDocumentDetails);
				uiOldProductList.data('deleted_product_count', iDeletedProductCount);
				

				// LOOP FOR DISPLAYING OLD PRODUCTS.
				for(var a in oWithdrawalProducts) {
					oProductElement = _displayOldProducts({product : oWithdrawalProducts[a]});
					uiProductList.append(oProductElement);

					if(a == iWithdrawalProductLength) {
						_closeGeneratingModal(500);
					}
				}

				uiUpdateRecordModal.find('.withdrawal-number-display').html(oWithdrawalDetails.withdrawal_number);
				uiWithdrawalNumber.html(oWithdrawalDetails.withdrawal_number);
				uiWithdrawalNumber.data('withdrawal_log_id', oWithdrawalDetails.id);
				inputRequestor.val(oWithdrawalDetails.requestor);
				inputDesignation.val(oWithdrawalDetails.designation);
				inputDepartment.val(oWithdrawalDetails.department);
				inputReasonForWithdrawal.val(oWithdrawalDetails.request_for_withdrawal);

				// BINDS EVENTS FOR THE PAGE.
				btnCloseNoteModal.off('click').on('click', function() {
					_closeNote();
				});

				btnAddItem.off('click').on('click', function() {
					// FUNCTION FOR BUILDING THE ADDED PRODUCT.
					_submitAddProduct();
				});

				btnSubmitNote.off('click').on('click', function() {
					_addNote({page : 'edit_record'});
				});

				btnCloseNoteModal.off('click').on('click', function() {
					_closeNote();
				});

				btnSubmitEdit.off('click').on('click', function() {
					uiUpdateRecordModal.addClass('showed');
				});

				btnConfirmEdit.off('click').on('click', function() {
					_edit();
				});

				btnCancelEdit.off('click').on('click', function() {
					uiUpdateRecordModal.removeClass('showed');
				});

				inputTextFields.off('change').on('change', function() {
					uiInputDiv.addClass('changed');
				});

				_checkSelectedProducts({
					edit_record : 'edit_record',
					element : uiProductList,
					dropdown : $('.product-list-dropdown')
				});
			},

			'view_ongoing' : function() {
				//SHOWS THE GENERATING VIEW MODAL.
				uiGenerateViewModal.addClass('showed');

				var btnEditRecord = $('.btn-edit-record'),
				btnCompleteRecord = $('.btn-complete-withdrawal'),
				uiWithdrawalNumber = $('.withdrawal-number'),
				uiDateTimeIssued = $('.date-time-issued'),
				uiRequestor = $('.requestor'),
				uiRequestForWithdrawal = $('.request-for-withdrawal'),
				uiDesignation = $('.designation'),
				uiDepartment = $('.department'),
				uiProductList = $('.product-list'),
				uiProductTemplate = $('.product-original-template'),
				btnCloseNoteModal = uiNoteModal.find('.close-note'),
				btnSubmitNote = uiNoteModal.find('.submit-note'),
				oProductDisplay = {
					page : 'view_ongoing',
					products : oWithdrawalDetails.products
				},
				oNoteDetails = {
					notes : oWithdrawalDetails.notes
				},
				oDocumentDetails = {
					documents : oWithdrawalDetails.documents
				};

				//console.log(oWithdrawalDetails);

				_getWithdrawalProductDetails(oWithdrawalDetails['id'], function(oResult) {

					//console.log(oWithdrawalProducts);
					oProductDisplay['products'] = oWithdrawalProducts;
					
					uiWithdrawalNumber.html(oWithdrawalDetails.withdrawal_number);
					uiWithdrawalNumber.data('withdrawal_log_id', oWithdrawalDetails.id);
					uiDateTimeIssued.html(oWithdrawalDetails.display_date);
					uiRequestor.html(oWithdrawalDetails.requestor);
					uiRequestForWithdrawal.html(oWithdrawalDetails.request_for_withdrawal);
					uiDesignation.html(oWithdrawalDetails.designation);
					uiDepartment.html(oWithdrawalDetails.department);
					uiCompleteRecordModal.attr('data-id', oWithdrawalDetails.id);

					_buildProductWithdrawalDisplay(oProductDisplay);
					_buildNoteDisplay(oNoteDetails);
					_buildDocumentDisplay(oDocumentDetails);

					btnEditRecord.off('click').on('click', function() {
						window.location.href = BASEURL + 'withdrawals/withdrawal_company_edit';
					});

					btnCompleteRecord.off('click').on('click', function() {
						_buildCompleteRecordModal({withdrawal_id : oWithdrawalDetails.id, page : 'view_ongoing'});
					});

					btnSubmitNote.off('click').on('click', function() {
						_addNote({page : 'view_ongoing'});
					});

					btnCloseNoteModal.off('click').on('click', function() {
						_closeNote();
					});

				});
			},

			'view_completed' : function() {
				//SHOWS THE GENERATING VIEW MODAL.
				uiGenerateViewModal.addClass('showed');

				var uiWithdrawalNumber = $('.withdrawal-number'),
				uiDateTimeIssued = $('.date-time-issued'),
				uiRequestor = $('.requestor'),
				uiDesignation = $('.designation'),
				uiRequestForWithdrawal = $('.reason-for-withdrawal'),
				uiDepartment = $('.department'),
				oProductDisplay = {
					page : 'view_completed',
					products : oWithdrawalDetails.products
				},
				oNoteDetails = {
					notes : oWithdrawalDetails.notes
				},
				oDocumentDetails = {
					documents : oWithdrawalDetails.documents
				};

				_buildProductWithdrawalDisplay(oProductDisplay);
				_buildNoteDisplay(oNoteDetails);
				_buildDocumentDisplay(oDocumentDetails);

				uiWithdrawalNumber.html(oWithdrawalDetails.withdrawal_number);
				uiDateTimeIssued.html(oWithdrawalDetails.display_date);
				uiRequestor.html(oWithdrawalDetails.requestor);
				uiDesignation.html(oWithdrawalDetails.designation);
				uiRequestForWithdrawal.html(oWithdrawalDetails.request_for_withdrawal);
				uiDepartment.html(oWithdrawalDetails.department);
			}
		}

		if (typeof (oElement[oParams.page]) == "function") 
		{
			oElement[oParams.page](oParams.page);
		}
	}

	function _generateWithdrawalNumber(callBack) {
		/*
		* developer : Dru Moncatar
		* description : Generates an incremental withdrawal number via php call.
		* criticality : CRITICAL
		* page usage :
		* - create record
		*/

		/*
		*	USAGE
		*	_generateWithdrawalNumber(function(oResult) {
		*		sGeneratedWithrawalNumber = oResult;
		*	});
		*/
		var oOptions = {
			type : 'POST',
			data : {post_type : 1},
			returnType : 'json',
			url : BASEURL + 'withdrawals/generate_withdrawal_number',
			success : function(oResult) {
				if(typeof(callBack) == 'function') {
					var sWithdrawalNumber = oResult.data;
					sGeneratedWithdrawalNumber = oResult.data;
					while(sWithdrawalNumber.length < 9) {
						sWithdrawalNumber = '0' + sWithdrawalNumber;
						if(sWithdrawalNumber.length == 9) {
							sGeneratedWithdrawalNumber = sWithdrawalNumber;
						}
					}
					callBack(sWithdrawalNumber);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	//==================================================//
	//========GETTING VALUES OF GLOBAL VARIABLES========//
	//==================================================//

	function _getAllWithdrawals(callBack) {
		var oOptions = {
			type : 'GET',
			data : {test : true},
			url : BASEURL + 'withdrawals/company_get_all_withdrawals',
			returnType : 'json',
			success : function(oResult) {
				
				if(typeof(callBack) == 'function') {
					if(oResult.status) {
						oWithdrawals = oResult.data;
						callBack(oResult.data);
					} else {
						oWithdrawals = 'No records found.';
						callBack(oWithdrawals);
					}
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllUnitOfMeasure(callBack) {
		var oOptions = {
			type : 'GET',
			data : {test : true},
			returnType : 'json',
			url : BASEURL + 'withdrawals/get_all_unit_of_measure',
			success : function(oResult) {
				if(typeof(callBack) == 'function') {
					oUnitOfMeasure = oResult.data;
					callBack(oUnitOfMeasure);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllWithdrawalProducts(callBack) {
		var oOptions = {
			type : 'GET',
			data : {test : true},	
			url : BASEURL + 'withdrawals/company_get_all_products',
			returnType : 'json',
			success : function(oResult) {

				if(typeof(callBack) == 'function') {
					oProducts = oResult.data;
					callBack(oResult.data);
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllLocations(callBack) {
		var oOptions = {
			type : 'GET',
			data : {test : true},	
			url : BASEURL + 'withdrawals/get_all_locations',
			returnType : 'json',
			success : function(oResult) {

				if(typeof(callBack) == 'function') {
					oLocations = oResult.data;
					callBack();
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getCurrentDateTime(callBack) {
		var oOptions = {
			type : 'GET',
			data : {test : true},	
			url : BASEURL + 'withdrawals/get_current_date_time',
			returnType : 'json',
			success : function(oResult) {

				if(typeof(callBack) == 'function') {
					oCurrentDateTime = oResult.data;
					callBack();
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllProducts(callBack) {
		var oOptions = {
			type : 'GET',
			data : {test : true},	
			url : BASEURL + 'withdrawals/get_all_products',
			returnType : 'json',
			success : function(oResult) {

				if(typeof(callBack) == 'function') {
					oProducts = oResult.data;
					callBack();
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getWithdrawalDetails(oParams, callBack) {
		var iWithdrawalId = oParams.withdrawal_id,
		oOptions = {
			type : 'GET',
			data : {id : iWithdrawalId},
			returnType : 'json',
			url : BASEURL + 'withdrawals/get_company_withdrawal_details',
			success : function(oResult) {
				if(typeof(callBack) == 'function') {
					oWithdrawalDetails = oResult.data;
					callBack(oWithdrawalDetails);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getWithdrawalProductList(oParams, callBack) {
		var iWithdrawalId = oParams.id,
		oOptions = {
			type : 'GET',
			data : {withdrawal_id : iWithdrawalId},
			returnType : 'json',
			url : BASEURL + 'withdrawals/get_company_product_list',
			success : function(oResult) {
				if(typeof(callBack) == 'function') {
					oWithdrawalProducts = oResult.data;
					callBack();
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getProductBatches(iProductId, callBack) {
		var oOptions = {
			type : 'GET',
			data : {product_id : iProductId},
			url : BASEURL + 'withdrawals/get_company_product_batches',
			returnType : 'json',
			success : function(oResult) {
				if(typeof(callBack) == 'function') {
					oProductBatches = oResult.data;
					callBack();
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getWithdrawalProductDetails(iWithdrawalLogId, callBack) {
		var oOptions = {
			type : 'GET',
			data : { withdrawal_log_id : iWithdrawalLogId },
			url : BASEURL + 'withdrawals/get_company_withdrawal_products_and_batches',
			returnType : 'json',
			success : function(oResult) {
				if(typeof(callBack) == 'function') {
					// console.log(oResult.data);
					oWithdrawalProducts = oResult.data;
					callBack(oResult.data);
				}
			}
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	//==================================================//
	//=====END GETTING VALUES OF GLOBAL VARIABLES=======//
	//==================================================//


	//==================================================//
	//========FUNCTIONS USED BY MULTIPLE PAGES==========//
	//==================================================//
	
	
	function _addItemToList(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Adds the selected product on the product_list
		* criticality : CRITICAL
		* page usage :
		* - create record
		* - edit record
		*/

		/*
		*	USAGE
		*	var oData = {
		*		product_template : $('your template to clone'),
		*		product_id : $('.product-list-dropdown').val()
		*	};
		*
		*	uiClonedTemplate = _addItemToList(oData);
		*/

		var oProductDetails = {},
		iProductId = oParams.product_id,
		uiTemplate = oParams.product_template,
		uiClonedTemplate = uiTemplate.clone(),
		oFeedback = {},
		inputUnitOfMeasure = uiClonedTemplate.find('.unit-of-measure-dropdown');


		if(iProductId != '' 
		&& iProductId != 'undefined' 
		&& iProductId != 'null'
		&& iProductId != null
		&& iProductId != undefined) {

			for(var a in oProducts) {
				if(oProducts[a].id == iProductId) {
					oProductDetails = oProducts[a];
				}
			}

			_buildUnitOfMeasureDropDown({select : inputUnitOfMeasure});
			uiClonedTemplate.removeClass('product-original-template');
			uiClonedTemplate.addClass('product-cloned-template');
			uiClonedTemplate.addClass('product-list-div');
			uiClonedTemplate.find('.product-sku-name').html('SKU# ' + oProductDetails.sku + ' - ' + oProductDetails.name);
			uiClonedTemplate.find('.product-description').html(oProductDetails.description);
			uiClonedTemplate.find('.product-image').html('<img src="' + oProductDetails.image + '" alt="" class="height-100percent width-230px">');
			uiClonedTemplate.show();
			return uiClonedTemplate;

		} else {

			oFeedback = {
				'title' : 'Error!',
				'message' : 'Please select a valid product',
				'speed' : 'slow',
				'icon' : failIcon
			};
			$('body').feedback(oFeedback);

		}
	}

	function _addNote(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Adds the note to the designated div.
		* criticality : CRITICAL
		* page usage :
		* - create record
		* - view ongoing
		* - edit record
		*/

		/*
		*	USAGE
		*	_addNote({page : 'create_record'});
		*/

 		var uiCloned = uiNoteTemplate.clone(),
 		uiNoteList = $('.note-list'),
 		uiSubject = uiCloned.find('.note-subject-display'),
 		uiDateTime = uiCloned.find('.note-date-time-display'),
 		uiMessage = uiCloned.find('.note-message-display'),
 		inputSubject = uiNoteModal.find('.note-subject'),
 		inputMessage = uiNoteModal.find('.note-message'),
 		oElement = {
 			
 			'create_record' : function() {

		 		_getCurrentDateTime(function(oResult) {
					
					var sMessage = "",
						iMessageCount = inputMessage.val().length;

						if(iMessageCount > 300)
						{
							sMessage = inputMessage.val().substr(0, 300);
							sMessage +="...";
						}else{
							sMessage = inputMessage.val();
						}
						

		 			uiCloned.removeClass('note-template');
		 			uiCloned.addClass('note-cloned').data({"message":inputMessage.val()});
		 			uiCloned.show();
		 			uiSubject.html("Subject: "+inputSubject.val());
		 			uiDateTime.html('Date/Time: ' + oCurrentDateTime.to_display);
		 			uiDateTime.data('date-time', oCurrentDateTime.to_record);
		 			uiMessage.html(sMessage);
		 			uiNoteList.append(uiCloned);	
		 			$('body').removeAttr('style');
		 			_closeNote();

		 			_triggerNoteLessMore();

		 		});
 			},

 			'view_ongoing' : function() {

 				_getCurrentDateTime(function(oResult) {

					var sMessage = "",
						iMessageCount = inputMessage.val().length;

						if(iMessageCount > 300)
						{
							sMessage = inputMessage.val().substr(0, 300);
							sMessage +="...";
						}else{
							sMessage = inputMessage.val();
						}

		 			uiCloned.removeClass('note-template');
		 			uiCloned.addClass('note-cloned').data({"message":inputMessage.val()});
		 			uiCloned.show();
		 			uiSubject.html("Subject: "+inputSubject.val());
		 			uiDateTime.html('Date/Time: ' + oCurrentDateTime.to_display);
		 			uiDateTime.data('date-time', oCurrentDateTime.to_record);
		 			uiMessage.html(sMessage);
		 			uiNoteList.append(uiCloned);	
		 			$('body').removeAttr('style');

		 			var oOptions = {
		 				type : 'POST',
		 				data : {
		 					subject : $('.note-subject').val(),
 							note : $('.note-message').val(),
 							date_created : oCurrentDateTime.to_record,
 							withdrawal_log_id : $('.withdrawal-number').data('withdrawal_log_id'),
 							user_id : 1 //temporary
		 				},
		 				url : BASEURL + 'withdrawals/add_note',
		 				returnType : 'json',
		 				success : function(oResult) {
		 					var oFeedback = { speed : 'mid' };

		 					if(oResult.status) {
		 						oFeedback['message'] = oResult.message;
		 						oFeedback['icon'] = successIcon;
		 						oFeedback['title'] = 'Success';
		 						$('body').feedback(oFeedback);
		 					} else {
		 						oFeedback['message'] = oResult.message;
		 						oFeedback['icon'] = failIcon;
		 						oFeedback['title'] = 'Fail';
		 						$('body').feedback(oFeedback);
		 					}

		 					_closeNote();

		 					_triggerNoteLessMore();
		 				}
		 			}

		 			cr8v_platform.CconnectionDetector.ajax(oOptions);
		 		});

 			},

 			'edit_record' : function() {

 				_getCurrentDateTime(function(oResult) {

					var sMessage = "",
						iMessageCount = inputMessage.val().length;

						if(iMessageCount > 300)
						{
							sMessage = inputMessage.val().substr(0, 300);
							sMessage +="...";
						}else{
							sMessage = inputMessage.val();
						}
 					
		 			uiCloned.removeClass('note-template');
		 			uiCloned.addClass('note-cloned').data({"message":inputMessage.val()});
		 			uiCloned.show();
		 			uiSubject.html("Subject: "+inputSubject.val());
		 			uiDateTime.html('Date/Time: ' + oCurrentDateTime.to_display);
		 			uiDateTime.data('date-time', oCurrentDateTime.to_record);
		 			uiMessage.html(sMessage);
		 			uiNoteList.append(uiCloned);	
		 			$('body').removeAttr('style');

		 			var oOptions = {
		 				type : 'POST',
		 				data : {
		 					subject : $('.note-subject').val(),
 							note : $('.note-message').val(),
 							date_created : oCurrentDateTime.to_record,
 							withdrawal_log_id : $('.withdrawal-number').data('withdrawal_log_id'),
 							user_id : 1 //temporary
		 				},
		 				url : BASEURL + 'withdrawals/add_note',
		 				returnType : 'json',
		 				success : function(oResult) {
		 					var oFeedback = { speed : 'mid' };

		 					if(oResult.status) {
		 						oFeedback['message'] = oResult.message;
		 						oFeedback['icon'] = successIcon;
		 						oFeedback['title'] = 'Success';
		 						$('body').feedback(oFeedback);
		 					} else {
		 						oFeedback['message'] = oResult.message;
		 						oFeedback['icon'] = failIcon;
		 						oFeedback['title'] = 'Fail';
		 						$('body').feedback(oFeedback);
		 					}

		 					_closeNote();

		 					_triggerNoteLessMore();
		 				}
		 			}

		 			cr8v_platform.CconnectionDetector.ajax(oOptions);
		 		});

 			}
 		};

 		if(typeof(oElement[oParams.page]) == "function") {
 			oElement[oParams.page](oParams.page);
 		}
 	}

	function _buildProductDropDown() {
		/*
		* developer : Dru Moncatar
		* description : Builds the product dropdowndown 
		* criticality : CRITICAL
		* page usage :
		* - create record
		* - edit record
		*/

		/*
		*	USAGE
		*	_buildProductDropDown();
		*/

		var sProductList = '',
		oProductList = {},
		uiSelect = $('.product-list-dropdown'),
		uiSiblings = uiSelect.siblings();
		
		for(var a in oProducts) {
			sProductList += '<option value="' + oProducts[a].id + '">' + oProducts[a].name + '</option>';
			oProductList[a] = $(sProductList);
		}

		uiSiblings.remove();
		uiSelect.append(sProductList);
		uiSelect.show().css({
			"padding" : "3px 4px",
			"width" :  "220px"
		});
	}

	function _buildUnitOfMeasureDropDown(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Builds the unit of measure dropdowndown 
		* criticality : CRITICAL
		* page usage :
		* - create record
		* - edit record
		* - view ongoing
		*/

		/*
		*	USAGE
		*	_buildUnitOfMeasureDropDown({select : $('#yourselectelement')});
		*/

		var sUnitOfMeasure = '',
		uiSelect = oParams.select,
		uiSiblings = uiSelect.siblings();

		for(var a in oUnitOfMeasure) {
			if(oUnitOfMeasure[a].name != 'bag' 
			&& oUnitOfMeasure[a].name != 'Bag'
			&& oUnitOfMeasure[a].name != 'Bags'
			&& oUnitOfMeasure[a].name != 'bags') {
				sUnitOfMeasure += '<option value="' + oUnitOfMeasure[a].id + '">' + oUnitOfMeasure[a].name + '</option>';
			}
		}

		uiSiblings.remove();
		uiSelect.html(sUnitOfMeasure);
		uiSelect.show().css({
			"padding" : "3px 4px",
			"width" :  "100px"
		});
	}

	function _buildBatchModal(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Builds the view for the batch modal.
		* criticality : CRITICAL
		* page usage :
		* - create record
		* - edit record
		* - view ongoing
		*/

		/*
		*	USAGE
		*	_buildBatchModal({page : 'view_ongoing'});
		*/

		var iProductId = uiBatchModal.data('product_id'),
		iBatchId = null,
		iBatchQty = null,
		oProductDetails = {},
		oCurrentBatch = {},
		oCurrentProduct = {},
		oConvertedQty = {},
		oWithdrawalBatch = {},
		oOldBatchDetails = {},
		oNewBatchDetails = {},
		oLocation = {},
		oData = {},

		iLocationId = null,
		sLocation = '',
		sStatus = '',

		uiProductDiv = {},
		uiElement = {},
		uiProductSkuName = uiBatchModal.find('.modal-product-sku-name'),
		uiStorageName = uiBatchModal.find('.modal-storage-name'),
		uiSubLocationList = uiBatchModal.find('.modal-sub-location'),
		uiProductDiv = $('.product-list').find('.product-list-div[data-id="' + iProductId + '"]'),
		uiGrayTemplate = uiProductDiv.find('.batch-gray-template'),
		uiWhiteTemplate = uiProductDiv.find('.batch-white-template'),
		uiSubLocationTemplate = uiBatchModal.find('.modal-sub-location-template'),
		inputBatchDropDown = uiBatchModal.find('.modal-batch-dropdown'),

		btnCloseModal = uiBatchModal.find('.modal-close'),
		btnSubmitAddBatch = uiBatchModal.find('.btn-submit-add-batch'),
		
		oElement = {
			
			'view_ongoing' : function() {

				for(var a in oProducts) {
					if(oProducts[a].id == iProductId) {
						oProductDetails = oProducts[a];
					}
				}

				_getProductBatches(iProductId, function(oResult) {
					_buildBatchDropDown();
					uiProductSkuName.html('SKU No. ' + oProductDetails.sku + ' - ' + oProductDetails.name);
					
					if(oParams.batch_id != undefined || oParams.batch_id != 'undefined') {
						inputBatchDropDown.val(oParams.batch_id);
						oOldBatchDetails = oParams['batch_details'];
						oWithdrawalBatch = oParams['batch_details'];

						for(var b in oProductBatches) {
							if(oProductBatches[b].batch_name == oWithdrawalBatch.batch_name) {
								oCurrentBatch = oProductBatches[b];
							}
						}

						oConvertedQty = _buildBatchConvertedQty({
							batch_id : oParams.batch_id,
							product_id : oCurrentProduct.product_id
						});

						iLocationId = oCurrentBatch.location_id;
						oLocationBreadCrumb = _buildLocationBreadCrumb({id : iLocationId});
						sLocationBreadCrumb = _buildLocationDisplay({name : 'modal_display', location : oLocationBreadCrumb});
						uiSubLocationTemplate.html(sLocationBreadCrumb);
						$('.batch-available-qty').html(oCurrentBatch.batch_quantity + ' ' + oCurrentBatch.unit_of_measure);
						$('.batch-withdraw-amount').val(oWithdrawalBatch.qty);
						$('.modal-batch-dropdown').val(oCurrentBatch.batch_id);
						uiStorageName.html(oCurrentBatch.storage_name);
					}
					
				});	

				btnCloseModal.off('click').on('click', function() {
					uiBatchModal.removeClass('showed');
					$('.modal-sub-location-template').html('<div class="modal-sub-location-template width-60percent display-inline-mid padding-all-20"><p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Batch Location </p><div class="clear"></div></div>');
					$('.batch-available-qty').html('');
				});

				btnSubmitAddBatch.off('click').on('click', function() {
					oNewBatchDetails = {
						batch_id : oOldBatchDetails['batch_id'],
						batch_name : oOldBatchDetails['batch_name'],
						id : oOldBatchDetails['id'],
						location_id : oOldBatchDetails['location_id'],
						qty : $('.batch-withdraw-amount').val(),
						unit_of_measure_id : oOldBatchDetails['unit_of_measure_id'],
						withdrawal_product_id : oOldBatchDetails['withdrawal_product_id']
					};
					oData['new_batch_details'] = oNewBatchDetails;
					oData['old_batch_details'] = oOldBatchDetails;
					oData['element'] = oParams.element;

					_saveBatchChanges(oData);
				});

				inputBatchDropDown.off('change').on('change', function() {
					var iBatchId = inputBatchDropDown.val(),
					oData = {
						product_id : iProductId,
						batch_id : iBatchId
					};

					_buildBatchModalDisplay(oData);
				});

				
			},

			'create_record' : function() {

				for(var a in oProducts) {
					if(oProducts[a].id == iProductId) {
						oProductDetails = oProducts[a];
					}
				}

				_getProductBatches(iProductId, function(oResult) {
					_buildBatchDropDown();
					uiProductSkuName.html('SKU No. ' + oProductDetails.sku + ' - ' + oProductDetails.name);

				});	

				btnCloseModal.off('click').on('click', function() {
					uiBatchModal.removeClass('showed');
					$('.modal-sub-location-template').html('<div class="modal-sub-location-template width-60percent display-inline-mid padding-all-20"><p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Batch Location </p><div class="clear"></div></div>');
					$('.batch-available-qty').html('');
				});

				btnSubmitAddBatch.off('click').on('click', function() {
					oData = {
						icon : successIcon,
						speed : 'mid',
						message : 'Batch updated.',
						title : 'Success!'
					};

					sStatus = $(this).data('status') != undefined ? $(this).data('status') : null;
					if(sStatus != null) {
						uiElement = oParams.element;
						uiParent = uiElement.closest('.batch-cloned-template');
						uiParent.remove();
						$(this).removeData('status');
						$('body').feedback(oData);
					}

					
					_submitAddBatch({white_template : uiWhiteTemplate, gray_template : uiGrayTemplate});
				});

				inputBatchDropDown.off('change').on('change', function() {
					var iBatchId = inputBatchDropDown.val(),
					oData = {
						product_id : iProductId,
						batch_id : iBatchId
					};

					_buildBatchModalDisplay(oData);
				});


				if(oParams.current_batch != undefined) {

					// EDIT BATCH CODES.
					oWithdrawalBatch = oParams.current_batch;
					uiBatchModal.find('.btn-submit-add-batch').data('status', 'edit');
					iBatchId = oWithdrawalBatch['batch_details']['batch_id'];
					iBatchQty = oWithdrawalBatch['qty_to_withdraw'];
					oLocationBreadCrumb = oWithdrawalBatch.location_breadcrumb;
					sLocationBreadCrumb = _buildLocationDisplay({name : 'modal_display', location : oLocationBreadCrumb});

					uiBatchModal.addClass('showed');
					uiBatchModal.find('h4').text('Generating view...');
					
					setTimeout(function() {

						uiBatchModal.find('h4').text('Edit Batch');
						uiBatchModal.find('.modal-batch-dropdown').val(iBatchId);
						uiBatchModal.find('.batch-withdraw-amount').val(iBatchQty);
						uiSubLocationTemplate.html(sLocationBreadCrumb);
						uiSubLocationTemplate.data('batch_details', oWithdrawalBatch['batch_details']);
						uiSubLocationTemplate.data('location_breadcrumb', oWithdrawalBatch['location_breadcrumb']);

					}, 800);
				}
			}
		};

		if(typeof(oElement[oParams.page]) == 'function') {
			oElement[oParams.page](oParams.page);
		}	
	}

	function _buildLocationBreadCrumb(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Builds the hierarchy of transfers/batch location and stores it at an object.
		* criticality : CRITICAL
		* page usage :
		* - create record
		* - edit record
		* - view ongoing
		*/

		/*
		*	USAGE
		*	_buildLocationBreadCrumb({id : iLocationId});
		*/

		var iLocationId = oParams.id,
		oCurrentLocation = null,
		oLocationList = {},
		iCounter = 0,
		iCounterTwo = 0,
		arr = [],
		oReversedLocations = {},
		keyCounter = 0;

		//GETS THE CURRENT LOCATION'S DATA.
		for(var a in oLocations) {
			if(oLocations[a].id == iLocationId) {
				oCurrentLocation = oLocations[a];
			}
		}

		oLocationList[iCounter] = oCurrentLocation; 
		++iCounter;

		keyCounter = Object.keys(oLocations).length;

		//BUILDS THE ARRAY FOR THE LOCATION HIERARCHY.
		for(var b = 0; b < keyCounter;) {
			if(oLocations[b].id == oCurrentLocation.parent_id) {
				oLocationList[iCounter] = oLocations[b];
				oCurrentLocation = oLocations[b];
				++iCounter;
				b = -1;
			}
			++b;
		}

		//CONVERT THE OBJECT TO AN ARRAY.
		for(var i in oLocationList){
			arr.push(oLocationList[i]);
		}

		// REVERSES THE ARRAY ARRANGEMENT.
		oReversedLocations = arr.reverse();

		return oReversedLocations;
	}

	function _buildLocationDisplay(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Builds the display for the location hierchy.
		* criticality : n/a
		* page usage :
		* - create record
		* - edit record
		* - view ongoing
		*/

		/*
		*	USAGE
		*	_buildLocationDisplay({location : oLocation, name : 'modal_display'});
		*/

		var sLocation = '',
		oLocation = oParams.location,
		iLocationLength = parseInt(Object.keys(oLocation).length) - parseInt(1),
		oElement = {
			'modal_display' : function() {
				for(var a in oLocation) {
					sLocation += '<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">' + oLocation[a].name + '</p>'
					if(a != iLocationLength) {
						sLocation += '<i class="display-inline-mid font-14 padding-right-10">&gt;</i>';
					}
				}
			},
			'product_display' : function() {
				for(var a in oLocation) {
					sLocation += '<p class="font-14 font-400 display-inline-mid padding-right-10">' + oLocation[a].name + '</p>';
					if(a != iLocationLength) {
						sLocation += '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>';
					}										
				}
			}
		}

		if (typeof (oElement[oParams.name]) == "function") 
		{
			oElement[oParams.name](oParams.name);
		}

		return sLocation;	
	}

	function _buildProductWithdrawalDisplay(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Displays the record's products into a div.
		* page usage :
		* - create record
		* - edit record
		*/

		/*
		*	USAGE
		*	_buildProductWithdrawalDisplay({page : 'your_page', products : 'oProducts'});
		*/

		var uiProductList = $('.product-list'),
		uiTemplate = {},
		uiCloned = {},
		uiGrayTemplate = {},
		uiWhiteTemplate = {},
		iBatchCount = parseInt(0),
		oProductDetails = {},
		oLocation = {},
		sLocation = '',
		sUnitOfMeasure = '',
		sLoadingMethod = '',
		oElement = {
			
			'view_ongoing' : function() {
				var btnEditProduct = {},
				btnCancelEdit = {},
				btnEditBatch = {},
				btnRemoveBatch = {},
				uiParent = {},
				uiEditBulk = {},
				uiEditPiece = {},
				uiUnitOfMeasureDropDown = {},
				oData = {},
				uiTemplate = $('.product-original-template'),
				iProductId = null,
				iWithdrawalProductLength = parseInt(Object.keys(oWithdrawalProducts).length) - parseInt(1);

				for(var a in oWithdrawalProducts) {
					// console.log(oWithdrawalProducts[a]);
					for(var b in oProducts) {
						if(oProducts[b].id == oWithdrawalProducts[a].product_id) {
							oProductDetails = oProducts[b];
						}
					}

					for(var d in oUnitOfMeasure) {
						if(oUnitOfMeasure[d].id == oWithdrawalProducts[a].unit_of_measure_id) {
							sUnitOfMeasure = oUnitOfMeasure[d].name;
						}
					}

					// ================================================== //
					// ===============PRODUCT MANIPULATION=============== //
					// ================================================== //
					sLoadingMethod = oWithdrawalProducts[a].loading_method == 1 ? 'By bulk' : 'By Piece';
					uiCloned = uiTemplate.clone();
					uiCloned.removeClass('product-original-template');
					uiCloned.addClass('product-list-div');
					uiCloned.attr('data-id', oWithdrawalProducts[a].product_id);
					uiCloned.data('product_id', oWithdrawalProducts[a].product_id);
					uiCloned.find('.product-sku-name').html('SKU #' + oProductDetails.sku + ' - ' + oProductDetails.name);
					uiCloned.find('.product-image').html('<img src="' + oProductDetails.image + '" alt="" class="height-100percent width-230px">');
					uiCloned.find('.product-unit-of-measure').html(sUnitOfMeasure);
					uiCloned.find('.product-transfer-method').html(sLoadingMethod);
					uiCloned.find('.total-qty-to-transfer').html(oWithdrawalProducts[a].qty + ' ' + sUnitOfMeasure);
					uiCloned.show();
					// ================================================== //
					// =============END PRODUCT MANIPULATION============= //
					// ================================================== //

					uiGrayTemplate = uiCloned.find('.batch-gray-template'),
					uiWhiteTemplate = uiCloned.find('.batch-white-template');
					oProductBatches = oWithdrawalProducts[a].batches;

					// ================================================== //
					// ===============BATCH MANIPULATION================= //
					// ================================================== //
					for(var c in oProductBatches) {

						if(parseInt(iBatchCount) % parseInt(2) == 0) {
							var uiClonedBatch = uiGrayTemplate.clone();
							uiClonedBatch.removeClass('batch-gray-template');
							iBatchCount = parseInt(iBatchCount) + parseInt(1);
						} else {
							var uiClonedBatch = uiWhiteTemplate.clone();
							uiClonedBatch.removeClass('batch-white-template');
							iBatchCount = parseInt(iBatchCount) + parseInt(1);
						}

						oLocation = _buildLocationBreadCrumb({id : oProductBatches[c].location_id});
						sLocation = _buildLocationDisplay({name : 'product_display', location : oLocation});
						uiClonedBatch.removeClass('.batch-gray-template');
						uiClonedBatch.addClass('batch-cloned');
						uiClonedBatch.find('.withdraw-batch-breadcrumb').html(sLocation);
						uiClonedBatch.find('.batch-withdrawal-number').html(oProductBatches[c].batch_name);
						uiClonedBatch.find('.withdraw-batch-qty').html(oProductBatches[c].qty + ' ' + sUnitOfMeasure);
						uiClonedBatch.data(oProductBatches[c]);
						uiCloned.find('.product-batches-list').append(uiClonedBatch);

						uiClonedBatch.find('.btn-hover-storage').css({"height":'100%'});
					}
					uiCloned.find('.second-text').text(oWithdrawalProducts[a].qty + ' ' + sUnitOfMeasure);
					uiCloned.find('.batch-cloned').show();
					// ================================================== //
					// =============END BATCH MANIPULATION=============== //
					// ================================================== //

					

					btnEditProduct = uiCloned.find('.withdraw-edit-btn');
					btnCancelEdit = uiCloned.find('.hide-cancel');
					btnEditBatch = uiCloned.find('.btn-edit-batch');
					btnRemoveBatch = uiCloned.find('.btn-remove-batch');
					btnPanelCollapse = uiCloned.find('.panel-title');

					// ================================================== //
					// ================EVENT BINDING===================== //
					// ================================================== //
					btnEditBatch.off('click').on('click', function() {
						oData['element'] = $(this);
						oData['page'] = 'view_ongoing';
						oData['product'] = oWithdrawalProducts[a];

						_editBatch(oData);
					});

					btnRemoveBatch.off('click').on('click', function() {
						oData['element'] = $(this);
						oData['page'] = 'view_ongoing';

						_removeBatch(oData);
					});

					btnEditProduct.off('click').on('click', function() {

						if($(this).text() == 'Edit') {
							// alert('edit');
						} else {
							var iProductId = $(this).closest('.product-list-div').data('id');
							_saveBatchChanges({product_id : iProductId});
						}
						uiParent = $(this).closest('.product-list-div');
						$(this).text("Save Changes");

						uiParent.find(".hide-cancel").show(100);

						uiParent.find(".transfer-add-batch").show();
						uiParent.find(".ongoing-edit-hide").show()

						uiParent.find(".transfer-display-piece").hide(100)
						uiParent.find(".transfer-hide-piece").show(100);


						uiParent.find(".transfer-edit-information").attr("disabled", true);
						uiParent.find(".complete-transfer-btn").attr("disabled", true);

						uiParent.find('.qty-transfer').removeClass('width-20percent');
						uiParent.find('.qty-transfer').addClass('width-15percent');

						uiParent.find('.qty-to-transfer').removeClass('width-125px');
						uiParent.find('.qty-to-transfer').addClass('width-100px');

						console.log(oWithdrawalProducts[a]);
						uiUnitOfMeasureDropDown = uiParent.find('.unit_of_measure_dropdown');

						_buildUnitOfMeasureDropDown({select : uiUnitOfMeasureDropDown});

						uiEditBulk = uiParent.find('.radio-bulk');
						uiEditPiece = uiParent.find('.radio-piece');
						uiEditPiece.attr('name', 'bag-or-bulk-' + oWithdrawalProducts[a].product_id);
						uiEditBulk.attr('name', 'bag-or-bulk-' + oWithdrawalProducts[a].product_id);

						if(oWithdrawalProducts[a].loading_method == 1) {
							uiEditBulk.prop('checked', true);
							uiUnitOfMeasureDropDown.removeClass('disabled');
							uiUnitOfMeasureDropDown.removeAttr('disabled');
							uiUnitOfMeasureDropDown.val(oWithdrawalProducts[a].unit_of_measure_id);
						} else {
							uiEditPiece.prop('checked', true);
							uiUnitOfMeasureDropDown.addClass('disabled');
							uiUnitOfMeasureDropDown.attr('disabled', true);
						}

						uiEditBulk.off('click').on('click', function() {
							uiUnitOfMeasureDropDown.removeClass('disabled');
							uiUnitOfMeasureDropDown.removeAttr('disabled');
						});

						uiEditPiece.off('click').on('click', function() {
							uiUnitOfMeasureDropDown.addClass('disabled');
							uiUnitOfMeasureDropDown.attr('disabled', true);
						});
					});

					btnCancelEdit.off('click').on('click', function() {
						var uiParent = $(this).closest('.product-list-div');
						$(this).hide(100);
						uiParent.find('.withdraw-edit-btn').text('Edit');
						uiParent.find(".transfer-ongoing-btn").text("Edit");

						uiParent.find(".transfer-add-batch").hide(100);
						uiParent.find(".ongoing-edit-hide").hide(100)


						uiParent.find(".transfer-display-piece").show(100)
						uiParent.find(".transfer-hide-piece").hide(100);

						uiParent.find(".transfer-edit-information").attr("disabled", false);
						uiParent.find(".complete-transfer-btn").attr("disabled", false);
					});

					btnPanelCollapse.off("click").on("click", function() {		
						$(".panel-title i").removeClass("fa-caret-right").addClass("fa-caret-down");
						var uiParentDiv = $(this).closest('.product-list-div');
						var ps = uiParentDiv.find(".panel-collapse");
						var ph = ps.find(".panel-body").outerHeight();

						if(ps.hasClass("in")){
							uiParentDiv.find("h4").addClass("active");
						}

						uiParentDiv.find("a").off("click").on("click",function(e){
							e.preventDefault();
							ps.css({height:ph});

							if(ps.hasClass("in")){
								uiParentDiv.find("h4").removeClass("active");
								uiParentDiv.find(".fa").removeClass("fa-caret-down").addClass("fa-caret-right");
								ps.removeClass("in");
								uiParentDiv.find('.withdraw-edit-btn').fadeOut(300);
							}else{
								uiParentDiv.find("h4").addClass("active");
								uiParentDiv.find(".fa").removeClass("fa-caret-right").addClass("fa-caret-down");
								ps.addClass("in");
								uiParentDiv.find('.withdraw-edit-btn').show();
							}

							setTimeout(function(){
								ps.removeAttr("style")
							},500);
						});
					});
					// ================================================== //
					// ==============END EVENT BINDING=================== //
					// ================================================== //
					
					uiCloned.find('.panel-collapse ').removeClass('in');
					uiCloned.find('.withdraw-edit-btn').hide();
					uiCloned.find('.panel-title').removeClass('active');
					
					uiProductList.append(uiCloned);
					if(a == iWithdrawalProductLength) {
						_closeGeneratingModal(500);
					}
				}
			},

			'view_completed' : function() {
				var btnEditProduct = {},
				btnCancelEdit = {},
				btnEditBatch = {},
				btnRemoveBatch = {},
				uiParent = {},
				uiEditBulk = {},
				uiEditPiece = {},
				uiUnitOfMeasureDropDown = {},
				oData = {},
				uiTemplate = $('.product-original-template'),
				iWithdrawalProductLength = parseInt(Object.keys(oWithdrawalProducts).length) - parseInt(1);

				//SHOWS THE GENERATING VIEW MODAL.
				uiGenerateViewModal.addClass('showed');

				for(var a in oWithdrawalProducts) {
					
					for(var b in oProducts) {
						if(oProducts[b].id == oWithdrawalProducts[a].product_id) {
							oProductDetails = oProducts[b];
						}
					}

					for(var d in oUnitOfMeasure) {
						if(oUnitOfMeasure[d].id == oWithdrawalProducts[a].unit_of_measure_id) {
							sUnitOfMeasure = oUnitOfMeasure[d].name;
						}
					}

					console.log(oUnitOfMeasure);

					// ================================================== //
					// ===============PRODUCT MANIPULATION=============== //
					// ================================================== //
					sLoadingMethod = oWithdrawalProducts[a].loading_method == 1 ? 'By bulk' : 'By Piece';
					uiCloned = uiTemplate.clone();
					uiCloned.removeClass('product-original-template');
					uiCloned.addClass('product-list-div');
					uiCloned.attr('data-id', oWithdrawalProducts[a].product_id);
					uiCloned.data('product_id', oWithdrawalProducts[a].product_id);
					uiCloned.find('.product-sku-name').html('SKU #' + oProductDetails.sku + ' - ' + oProductDetails.name);
					uiCloned.find('.product-image').html('<img src="' + oProductDetails.image + '" alt="" class="height-100percent width-230px">');
					uiCloned.find('.product-description').html(oProductDetails.description);
					uiCloned.find('.product-unit-of-measure').html(sUnitOfMeasure);
					uiCloned.find('.product-transfer-method').html(sLoadingMethod);
					uiCloned.find('.total-qty-to-transfer').html(oWithdrawalProducts[a].qty + ' ' + sUnitOfMeasure);
					uiCloned.show();
					// ================================================== //
					// =============END PRODUCT MANIPULATION============= //
					// ================================================== //

					uiGrayTemplate = uiCloned.find('.batch-gray-template'),
					uiWhiteTemplate = uiCloned.find('.batch-white-template');
					oProductBatches = oWithdrawalProducts[a].batches;

					// ================================================== //
					// ===============BATCH MANIPULATION================= //
					// ================================================== //
					for(var c in oProductBatches) {

						if(parseInt(iBatchCount) % parseInt(2) == 0) {
							var uiClonedBatch = uiGrayTemplate.clone();
							uiClonedBatch.removeClass('batch-gray-template');
							iBatchCount = parseInt(iBatchCount) + parseInt(1);
						} else {
							var uiClonedBatch = uiWhiteTemplate.clone();
							uiClonedBatch.removeClass('batch-white-template');
							iBatchCount = parseInt(iBatchCount) + parseInt(1);
						}

						oLocation = _buildLocationBreadCrumb({id : oProductBatches[c].location_id});
						sLocation = _buildLocationDisplay({name : 'product_display', location : oLocation});
						uiClonedBatch.removeClass('.batch-gray-template');
						uiClonedBatch.addClass('batch-cloned');
						uiClonedBatch.find('.withdraw-batch-breadcrumb').html(sLocation);
						uiClonedBatch.find('.batch-withdrawal-number').html(oProductBatches[c].batch_name);
						uiClonedBatch.find('.withdraw-batch-qty').html(oProductBatches[c].qty + ' ' + sUnitOfMeasure);
						uiClonedBatch.data(oProductBatches[c]);
						uiCloned.find('.product-batches-list').append(uiClonedBatch);
					}
					uiCloned.find('.second-text').text(oWithdrawalProducts[a].qty + ' ' + sUnitOfMeasure);
					uiCloned.find('.batch-cloned').show();
					// ================================================== //
					// =============END BATCH MANIPULATION=============== //
					// ================================================== //

					btnPanelCollapse = uiCloned.find('.panel-title');
					uiCloned.find('.panel-collapse ').removeClass('in');
					uiCloned.find('.panel-title').removeClass('active');

					uiProductList.append(uiCloned);
					if(a == iWithdrawalProductLength) {
						_closeGeneratingModal(500);
					}

					btnPanelCollapse.off("click").on("click", function() {		
						$(".panel-title i").removeClass("fa-caret-right").addClass("fa-caret-down");
						var uiParentDiv = $(this).closest('.product-list-div');
						var ps = uiParentDiv.find(".panel-collapse");
						var ph = ps.find(".panel-body").outerHeight();

						if(ps.hasClass("in")){
							uiParentDiv.find("h4").addClass("active");
						}

						uiParentDiv.find("a").off("click").on("click",function(e){
							e.preventDefault();
							ps.css({height:ph});

							if(ps.hasClass("in")){
								uiParentDiv.find("h4").removeClass("active");
								uiParentDiv.find(".fa").removeClass("fa-caret-down").addClass("fa-caret-right");
								ps.removeClass("in");
								uiParentDiv.find('.withdraw-edit-btn').fadeOut(300);
							}else{
								uiParentDiv.find("h4").addClass("active");
								uiParentDiv.find(".fa").removeClass("fa-caret-right").addClass("fa-caret-down");
								ps.addClass("in");
								uiParentDiv.find('.withdraw-edit-btn').show();
							}

							setTimeout(function(){
								ps.removeAttr("style")
							},500);
						});
					});

				}
			}
		};

		oWithdrawalProducts = oParams.products;
		if(typeof(oElement[oParams.page]) == 'function') {
			oElement[oParams.page](oParams.page);
		}
	}

	function _buildNoteDisplay(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Displays the record's notes into a div.
		* criticality : n/a
		* page usage :
		* - view ongoing
		* - view completed
		*/

		/*
		*	USAGE
		*	_buildNoteDisplay({notes : oNote});
		*/

		var uiNoteList = $('.note-list'),
		uiNoteTemplate = $('.note-template'),
		uiNoteClone = {},
		oNotes = oParams.notes;

		for(var a in oNotes) {

			var sMessage = "",
				iMessageCount = oNotes[a].note.length;

				if(iMessageCount > 300)
				{
					sMessage = oNotes[a].note.substr(0, 300);
					sMessage +="...";
				}else{
					sMessage = oNotes[a].note;
				}

			uiNoteClone = uiNoteTemplate.clone();
			uiNoteClone.removeClass('note-template');
			uiNoteClone.addClass('note-cloned').data({"message":oNotes[a].note});
			uiNoteClone.show();
			uiNoteClone.find('.note-subject-display').html("Subject: "+oNotes[a].subject);
			uiNoteClone.find('.note-date-time-display').html(oNotes[a].date_created);
			uiNoteClone.find('.note-message-display').html(sMessage);
			uiNoteList.append(uiNoteClone);

			_triggerNoteLessMore();
		}
	}

	function _buildDocumentDisplay(oParams) {
		var uiDocumentList = $('.document-list'),
		uiDocumentTemplate = $('.document-template'),
		uiDocumentClone = {},
		oDocuments = oParams.documents;

		for(var a in oDocuments) {
			uiDocumentClone = uiDocumentTemplate.clone();
			uiDocumentClone.removeClass('document-template');
			uiDocumentClone.addClass('document-cloned');
			
			uiDocumentClone.data(oDocuments[a]);

			uiDocumentClone.show();
			uiDocumentClone.find('.document-name').html(oDocuments[a]['document_name']);
			uiDocumentClone.find('.document-date').html(oDocuments[a]['date_added']);
			a % 2 != 0 ? uiDocumentClone.removeClass('tbl-dark-color') : '';
			uiDocumentList.append(uiDocumentClone);

			uiDocumentClone.find('.content-hide').css({'height':'100%'});


		}

		_downloadDocument(oDocuments);
	}

	function _downloadDocument(oDocs)
	{
		
		var	btnDownloadDocument = $(".download-document"),
			btnViewDocument = $(".view-document");


		btnDownloadDocument.off("mouseenter.download").on("mouseenter.download", function(e){
			e.preventDefault();
			var uiDownloadThis = $(this),
				uiParentContainer = uiDownloadThis.closest('.table-content.position-rel'),
				uiGetDataPath = uiParentContainer.data("document_path"),
				uiGetDataName = uiParentContainer.data("document_name");

				uiDownloadThis.attr("href", uiGetDataPath);
				uiDownloadThis.attr("download", uiGetDataName);

		});


		btnViewDocument.off("click.viewDocument").on("click.viewDocument", function(e){
			e.preventDefault();

			
			var uiParentContainer = $(this).closest('.table-content.position-rel'),
				uiGetDataPath = uiParentContainer.data("document_path");

				console.log(uiGetDataPath);

				window.open(uiGetDataPath, '_blank');
		});
	}

	function _calculateTotalWithdrawal(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Calculates the sum of all withdrawal for a certain product.
		* criticality : n/a
		* page usage :
		* - view ongoing
		* - create record
		*/

		/*
		*	USAGE
		*	_calculateTotalWithdrawal({notes : oNote});
		*/


		var iProductId = oParams.product_id,
		uiProductDiv = $('.product-list').find('.product-list-div[data-id="' + iProductId + '"]'),
		uiBatches = uiProductDiv.find('.batch-cloned-template'),
		iTotalQtyToWithdraw = parseInt(0),
		sUnitOfMeasure = '',
		oData = {};

		uiBatches.each(function() {
			var thisBatch = $(this),
			iBatchQty = thisBatch.data('qty_to_withdraw'),
			sBatchQty = thisBatch.data('unit_of_measure');

			iTotalQtyToWithdraw = parseFloat(iTotalQtyToWithdraw) + parseFloat(iBatchQty);
			sUnitOfMeasure = sBatchQty;
		});

		oData['total_quantity'] = iTotalQtyToWithdraw;
		oData['unit_of_measure'] = sUnitOfMeasure;
		return oData;
	}

	function _checkSelectedProducts(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Checks all product in the product list.. 
		* then removes them from the product dropdown.
		* criticality : n/a
		* page usage :
		* - create record
		* - edit record
		*/

		/* USAGE
		*	_checkSelectedProducts({
		*  		element : $("#company_create_product_details"), // WHERE ALL ADDED PRODUCTS ARE DISPLAYED.
		*  		dropdown : $("#company_create_product_dropdown") // THE DROPDOWN TO BE ADJUSTED.
		*	});
		*/

		var uiParent = oParams.element,
		uiDropDown = oParams.dropdown,
		arrProducts = [],
		sNewOptions = '',
		sAllProducts = '',
		iCounter = 0;

		
		if(oParams.edit_record != undefined || oParams.edit_record != null) {

			//FINDS ALL THE SELECTED PRODUCTS AND STORES THEM INTO AN ARRAY.
			uiParent.find('.product-list-div').each(function() {	
				if($(this).data("id") != undefined) {
					arrProducts[ iCounter ] = {product_id : $(this).data("id")};
					++iCounter;
				}
			});

			uiParent.find('.old-product-cloned').each(function() {
				if($(this).data('id') != undefined) {
					arrProducts[ iCounter ] = {product_id : $(this).data('id')};
					++iCounter;
				}
			});
			
			//BUILDS THE OPTION WITH ALL THE PRODUCTS.
			for(var b in oProducts) {
				sAllProducts += '<option value="' + oProducts[b].id + '">' + oProducts[b].name + '</option>'
			}

			// SETS THE NEW OPTION TO THE SELECT ELEMENT.
			uiDropDown.html(sAllProducts);

			// REMOVES THE SELECTED PRODUCTS FROM THE PRODUCT DROPDOWN
			uiDropDown.find('option').each(function(index,element){
				for(var a in arrProducts) {
					if(arrProducts[a].product_id == $(this).val()) {
						$(this).remove();
					}
				}
			});

		} else {

			uiParent.find('.product-list-div').each(function() {	
				if($(this).data("id") != undefined) {
					arrProducts[ iCounter ] = {product_id : $(this).data("id")};
					++iCounter;
				}
			});
			
			//BUILDS THE OPTION WITH ALL THE PRODUCTS.
			for(var b in oProducts) {
				sAllProducts += '<option value="' + oProducts[b].id + '">' + oProducts[b].name + '</option>'
			}

			// SETS THE NEW OPTION TO THE SELECT ELEMENT.
			uiDropDown.html(sAllProducts);

			// REMOVES THE SELECTED PRODUCTS FROM THE PRODUCT DROPDOWN
			uiDropDown.find('option').each(function(index,element){
				for(var a in arrProducts) {
					if(arrProducts[a].product_id == $(this).val()) {
						$(this).remove();
					}
				}
			});

		}
	}

 	function _closeNote() {
 		/*
		* developer : Dru Moncatar
		* description : Closes the note div and clears all input in it.
		* criticality : n/a
		* page usage :
		* - create record
		* - edit record
		* - view_ongoing
		*/

		/* USAGE
		*	_closeNote();
		*/
 		var inputNoteSubject = $('.note-subject'),
 		inputNoteMessage = $('.note-message');

 		inputNoteSubject.val('');
 		inputNoteMessage.val('');
 		uiNoteModal.removeClass('showed');
 	}

	function _editBatch(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Handles the editting of the batch for a certain product.
		* criticality : n/a
		* page usage :
		* - view ongoing
		*/

		/* USAGE
		*	_editBatch(
		*		page : 'view_ongoing' // THE PAGE WHERE THE FUNCTION IS USED.
		*		element : $('.yourbatchdiv') // THE DIV OF THE BATCH TO BE EDITTED.
		*	);
		*/

		var uiElement = oParams.element,
		uiParent = {},
		uiProductDiv = {},
		iProductId = null,
		oBatchDetails = {},
		oData = {},
		oElement = {
			'view_ongoing' : function() {
				var uiBatchDropDown = uiBatchModal.find('.modal-batch-dropdown');

				uiParent = uiElement.closest('.batch-cloned');
				uiProductDiv = uiParent.closest('.product-list-div');
				iProductId = uiProductDiv.data('product_id');
				uiBatchModal.data('product_id', iProductId);
				oBatchDetails = uiParent.data();
				uiBatchModal.addClass('showed');

				oData['page'] = 'view_ongoing';
				oData['batch_id'] = oBatchDetails.batch_id;
				oData['batch_details'] = oBatchDetails;
				oData['product'] = oParams.product;
				oData['element'] = oParams.element;

				_buildBatchModal(oData);
			},

			'create_record' : function() {

			}
		};

		if(typeof(oElement[oParams.page]) == 'function') {
			oElement[oParams.page](oParams.page);
		}
	}

 	function _getNewNotes(oParams, callBack) {
 		/*
		* developer : Dru Moncatar
		* description : Collects all the newly added note on a record.
		* criticality : n/a
		* page usage :
		* - create record
		*/

		/* USAGE
		* 	_getNewNotes(function(oResult) {});
		*/

 		var uiNoteList = $('.note-list'),
 		oNoteDetails = {},
 		iCounter = parseInt(0),
 		oElement = {
 			
 			'create_record' : function() {
 				var uiNotes = uiNoteList.find('.note-cloned');

 				uiNotes.each(function() {
 					oNoteDetails[ iCounter ] = {
 						subject : $(this).find('.note-subject-display').html(),
 						note : $(this).find('.note-message-display').html(),
 						date_created : $(this).find('.note-date-time-display').data('dateTime')
 					};
 					++iCounter;
 				});

 				oWithdrawalNotes = oNoteDetails;
 				callBack();
 			},

 			'edit_record' : function() {

 			}
 		};

 		if(typeof(oElement[oParams.page]) == "function") {
 			oElement[oParams.page](oParams.page);
 		}
 	}

	function _getNewProducts(oParams, callBack) {
		/*
		* developer : Dru Moncatar
		* description : Collects all newly add product for a record.
		* criticality : CRITICAL
		* page usage :
		* - create record
		* - edit_record
		*/

		/* USAGE
		* oData = {
		*	page : 'view_ongoing' // THE PAGE WHERE THE FUNCTION IS USED.
		*	element : $('.yourbatchdiv') // THE DIV OF THE BATCH TO BE EDITTED.
		* };
		*	_getNewProducts(oData, function(){});
		*/

		var uiProductList = $('.product-list'),
		oNewProductList = {},
		thisProduct = {},
		iCounter = parseInt(0),
		oElement = {
			'create_record' : function() {
				var uiProducts = uiProductList.find('.product-list-div');

				uiProducts.each(function() {
					thisProduct = $(this);
					
					_getBatchOfProducts({product_div : thisProduct}, function(oResult) {

						oNewProductList[ iCounter ] = {
							product_id : thisProduct.data('id'),
							loading_method : thisProduct.data('loading_method'),
							unit_of_measure_id : thisProduct.data('unit_of_measure'),
							qty : thisProduct.data('total_quantity'),
							status : 0,
							product_batches : oProductBatches
						};

						++iCounter;	
					});
					
				});

				oWithdrawalProducts = oNewProductList;
				callBack();
			},

			'edit_record' : function() {
				var uiProducts = uiProductList.find('.product-list-div');

				uiProducts.each(function() {
					thisProduct = $(this);
					
					_getBatchOfProducts({product_div : thisProduct}, function(oResult) {

						oNewProductList[ iCounter ] = {
							product_id : thisProduct.data('id'),
							loading_method : thisProduct.data('loading_method'),
							unit_of_measure_id : thisProduct.data('unit_of_measure'),
							qty : thisProduct.data('total_quantity'),
							status : 0,
							product_batches : oProductBatches
						};

						++iCounter;	
					});
					
				});

				oWithdrawalProducts = oNewProductList;
				callBack();
			}
		}

		if(typeof(oElement[oParams.page]) == "function") {
 			oElement[oParams.page](oParams.page);
 		}
	}

	function _getNewDocuments(oParams, callBack) {
		var uiParent = $('#displayDocuments'),
		oDocuments = uiParent.find('.uploaded_document'),
		oDocumentDetails = {},
		oNewDocuments = {},
		iCounter = 0;

		oDocuments.each(function() {
			oDocumentDetails = $(this).data();
			oNewDocuments[ iCounter ] = {
				document_name : oDocumentDetails['document_name'],
				document_path : oDocumentDetails['document_path'],
				date_added : oDocumentDetails['date_added']
			};
			++iCounter;
		});

		oWithdrawalDocuments = oNewDocuments;
		callBack();
	}

	function _getBatchOfProducts(oParams, callBack) {
		/*
		* developer : Dru Moncatar
		* description : Collects all the newly added batch for a certain product.
		* criticality : CRITICAL
		* page usage :
		* - create record
		* - edit_record
		*/

		/* USAGE
		* oData = {
		*	product_div : $('.product_list') // THE DIV WHERE ALL PRODUCTS ARE DISPLAYED
		* };
		* 	_getBatchOfProducts(oData, function(oResult) {});
		*/

		var uiProductDiv = oParams.product_div,
		uiBatchList = uiProductDiv.find('.product-batches-list'),
		uiBatches = uiBatchList.find('.batch-cloned-template'),
		oBatchList = {},
		oBatchDetails = {},
		iCounter = parseInt(0);

		uiBatches.each(function() {
			oBatchDetails = $(this).data();

			oBatchList[ iCounter ] = {
				qty : oBatchDetails.qty_to_withdraw,
				unit_of_measure_id : oBatchDetails.unit_of_measure_id,
				location_id : oBatchDetails.batch_details.location_id,
				batch_id : oBatchDetails.batch_details.batch_id
			};

			++iCounter;
		});

		oProductBatches = oBatchList;
		callBack();
	}

	function _removeBatch(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Removes a batch on a certain product.
		* criticality : CRITICAL
		* page usage :
		* - create record
		* - view ongoing
		*/

		/* USAGE
		* oData = {
		*	element : $('.your_batch) // THE DIV OF THE BATCH TO BE REMOVED.
		* };
		* 	_removeBatch(oData);
		*/

		var uiElement = oParams.element,
		uiParent = {},
		uiProductDiv = {},
		sOldTotalQty = null,
		sNewTotalQty = null,
		iDeletedBatchesCount = null,
		oDeletedBatches = {},
		oBatchDetails = {},
		oFeedback = {
			icon : successIcon,
			speed : 'mid',
			title : 'success',
			message : 'Batch removed.'
		},
		oElement = {

			'view_ongoing' : function() {
				uiParent = uiElement.closest('.batch-cloned');
				uiProductDiv = uiParent.closest('.product-list-div');
				oBatchDetails = uiParent.data();
				sOldTotalQty = uiProductDiv.find('.total-withdrawal-qty').html();
				sOldTotalQty = sOldTotalQty.split(' ');
				sNewTotalQty = parseFloat(sOldTotalQty) - parseFloat(oBatchDetails.qty);

				iDeletedBatchesCount = uiProductDiv.data('deleted_batches') != undefined ? parseInt(Object.keys(uiProductDiv.data('deleted_batches'))) + parseInt(1) : 0;
				uiProductDiv.find('.total-withdrawal-qty').html(sNewTotalQty + ' ' + sOldTotalQty[1]);
				oDeletedBatches[ iDeletedBatchesCount ] = oBatchDetails;

				console.log(oBatchDetails);
				console.log(oDeletedBatches);
				uiParent.remove();
			},

			'create_record' : function() {
				uiParent = uiElement.closest('.batch-cloned');

				uiParent.remove();
			}
		};


		$('body').feedback(oFeedback);
		if(typeof(oElement[oParams.page]) == 'function') {
			oElement[oParams.page](oParams.page);
		}
	}

	function _showNewRecord() {
		/*
		* developer : Dru Moncatar
		* description : Event for showing the newly created record.
		* page usage :
		* - create record
		*/

		/* USAGE
		* _showNewRecord();
		*/

		var uiCreatedRecordModal = $('.modal-created-record'),
		iWithdrawalId = uiCreatedRecordModal.data('withdrawal_id'),
		oData = {
			name : 'current_withdrawal',
			data : {withdrawal_id : iWithdrawalId}
		};

		cr8v_platform.localStorage.delete_local_storage({name : 'current_withdrawal'});
		cr8v_platform.localStorage.set_local_storage(oData);
		setTimeout(function() {
			window.location.href = BASEURL + 'withdrawals/withdrawal_company_ongoing';
		}, 100);
	}

	function _setCurrentWithdrawal(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Stores the id of the current withdrawal in the local storage.
		* page usage :
		* - create record
		*/

		/* USAGE
		* _setCurrentWithdrawal({withdrawal_id : iWithdrawalId});
		*/

		var iWithdrawalId = oParams.withdrawal_id;

		cr8v_platform.localStorage.delete_local_storage({name : 'current_withdrawal'});
		cr8v_platform.localStorage.set_local_storage({name : 'current_withdrawal', data : {withdrawal_id : iWithdrawalId}});
	}

	function _submitAddProduct() {
		/*
		* developer : Dru Moncatar
		* description : Adds the product on the product list
		* page usage :
		* - create record
		* - edit record
		*/

		/* USAGE
		* _submitAddProduct();
		*/

		var btnAddItem = $('.btn-add-item'),
		btnCreateWithdrawal = $('.btn-create-withdrawal'),
		btnCloseNoteModal = uiNoteModal.find('.close-note'),
		btnSubmitNote = uiNoteModal.find('.submit-note'),
		
		uiWithdrawalNumber = $('.withdrawal-number'),
		uiProductTemplate = $('.product-original-template'),
		uiProductList = $('.product-list'),
		uiClonedTemplate = {},
		
		iSelectedProduct = null,
		sCurrentPath = window.location.href,
		oData = {
			product_template : uiProductTemplate,
			product_id : $('.product-list-dropdown').val()
		};

		uiClonedTemplate = _addItemToList(oData);
		uiClonedTemplate.attr('data-id', oData.product_id);
		uiProductList.append(uiClonedTemplate);

		var btnRemoveProduct = uiClonedTemplate.find('.remove-product'),
		btnBulk = uiClonedTemplate.find('.radio-bulk'),
		btnPiece = uiClonedTemplate.find('.radio-piece'),
		btnAddBatch = uiClonedTemplate.find('.btn-add-batch'),
		uiProductDiv = $('.product-list').find('.product-list-div[data-id="' + oData.product_id + '"]'),
		inputUnitOfMeasure = uiClonedTemplate.find('.unit-of-measure-dropdown');

		btnBulk.attr('name', 'bag-or-bulk-' + oData.product_id);
		btnPiece.attr('name', 'bag-or-bulk-' + oData.product_id);

		// REMOVES THE PRODUCT'S DIV
		btnRemoveProduct.off('click').on('click', function() {
			var uiParent = $(this).closest('.product-cloned-template');
			uiParent.remove();
			
			// REMOVES THE ALREADY SELECTED PRODUCT FROM IT'S DROPDOWN.
			if(sCurrentPath.indexOf('withdrawal_company_edit') > -1) {

				_checkSelectedProducts({
					edit_record : 'edit_record',
					element : uiProductList,
					dropdown : $('.product-list-dropdown')
				});

			} else {

				_checkSelectedProducts({
					element : uiProductList,
					dropdown : $('.product-list-dropdown')
				});
				 
			}
			
		});

		// EVENT FOR CLICK THE BULK RADIO BUTTON
		btnBulk.off('click').on('click',function() {
			inputUnitOfMeasure.removeAttr('disabled');

			if(btnBulk.prop('checked') == true 
			|| btnPiece.prop('checked') == true) {
				btnAddBatch.removeClass('disabled');
			} else {
				btnAddBatch.addClass('disabled');
			}

			// SETS DATA ON THE PRODUCT DIV.
			uiProductDiv.data('loading_method', 1);
			uiProductDiv.data('unit_of_measure', inputUnitOfMeasure.val());

			// EVENT FOR UNIT OF MEASURE CHANGE...
			inputUnitOfMeasure.off('change').on('change', function() {
				uiProductDiv.data('unit_of_measure', inputUnitOfMeasure.val());
			});

		});

		// EVENT FOR CLICK THE PIECE RADIO BUTTON
		btnPiece.off('click').on('click',function() {
			inputUnitOfMeasure.attr('disabled', true);

			if(btnBulk.prop('checked') == true 
			|| btnPiece.prop('checked') == true) {
				btnAddBatch.removeClass('disabled');
			} else {
				btnAddBatch.addClass('disabled');
			}

			// SETS DATA ON THE PRODUCT DIV.
			uiProductDiv.data('loading_method', 2);
			uiProductDiv.data('unit_of_measure', 8); // TEMPORARY, REPRESENTING BAG.

		});

		// EVENT FOR ADDING A BATCH.
		btnAddBatch.off('click').on('click', function() {
			var thisButton = $(this),
			uiParent = thisButton.closest('.product-list-div'),
			iProductId = uiParent.data('id');

			// alert(iProductId);
			uiBatchModal.addClass('showed');
			uiBatchModal.data('product_id', iProductId);
			_buildBatchModal({page : 'create_record'});
		});

		// REMOVES THE ALREADY SELECTED PRODUCT FROM IT'S DROPDOWN.
		if(sCurrentPath.indexOf('withdrawal_company_edit') > -1) {

			_checkSelectedProducts({
				edit_record : 'edit_record',
				element : uiProductList,
				dropdown : $('.product-list-dropdown')
			});

		} else {

			_checkSelectedProducts({
				element : uiProductList,
				dropdown : $('.product-list-dropdown')
			});

		}
	}

	function _saveBatchChanges(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Saves the batch changes into an 
		* object and places it into the product's div.
		* page usage :
		* - edit record
		*/

		/* USAGE
		* _saveBatchChanges({
		*	old_batch_details : oOldDetails // THE BATCH'S OLD DETAILS
		*	new_batch_details : oNewDetails // THE BATCH'S UPDATED DETAILS
		*	element : $('your_batch_div') // YOUR BATCH'S DIV.
		* });
		*/

		var oOldBatchDetails = oParams['old_batch_details'],
		oNewBatchDetails = oParams['new_batch_details'],
		uiElement = oParams['element'];
		uiParent = uiElement.closest('.batch-cloned'),
		uiProductDiv = uiParent.closest('.product-list-div'),
		uiBatchQty = uiParent.find('.withdraw-batch-qty'),
		uiTotalQty = uiProductDiv.find('.total-withdrawal-qty');
		sTotalQty = uiTotalQty.text().split(' '),
		sBatchQty = uiBatchQty.text().split(' '),
		sNewTotalQty = '';

		sNewTotalQty = parseFloat(sTotalQty[0]) - parseInt(oOldBatchDetails.qty);
		sNewTotalQty = parseFloat(sNewTotalQty) + parseInt(oNewBatchDetails.qty);

		uiBatchQty.html(oNewBatchDetails.qty + ' ' + sBatchQty[1]);
		uiTotalQty.html(sNewTotalQty.toFixed(0) + ' ' + sTotalQty[1]);

		uiBatchModal.removeClass('showed');

		console.log(sBatchQty);
		console.log(oOldBatchDetails);
		console.log(oNewBatchDetails);
	}

	function _documentEvents(bTemporary)
	{
		var btnUploadDocs = $('[modal-target="upload-documents"]'),
			transferLogId = 0;

		btnUploadDocs.off('click');

		var extraData = { "transfer_log_id" : transferLogId };

		if(bTemporary === true){
			extraData["temporary"] = true;
		}

		btnUploadDocs.cr8vFileUpload({
			url : BASEURL + "transfers/upload_document",
			accept : "pdf,csv",
			filename : "attachment",
			extraField : extraData,
			onSelected : function(bIsValid){
				if(bIsValid){
					//selected valid
				}
			},
			success : function(oResponse){
				if(oResponse.status){
					$('[cr8v-file-upload="modal"] .close-me').click();
					var oDocs = oResponse.data[0]["documents"];
					if(bTemporary){
						sCurrentPath = window.location.href;
						oUploadedDocuments[Object.keys(oUploadedDocuments).length] = oResponse.data[0]["documents"][0];
						_displayDocuments(oUploadedDocuments);

						//if(sCurrentPath.indexOf('withdrawal_company_ongoing') > -1) {
							_saveDocumentDetails(oDocs[0]);
						//}
					}else{
						_displayDocuments(oDocs);
					}
				}
			},
			error : function(){
				$("body").feedback({title : "Message", message : "Was not able to upload, file maybe too large", type : "danger", icon : failIcon});
			}
		});
	}

	function _displayDocuments(oDocs)
	{
		var bChangeBg = false,
			iCountDocument = 0;

		var uiContainer = $("#displayDocuments"),
			sTemplate = '<div class="table-content position-rel tbl-dark-color uploaded_document">'+
							'<div class="content-show padding-all-10">'+
								'<div class="width-85per display-inline-mid padding-left-10">'+
									'<i class="fa font-30 display-inline-mid width-50px icon"></i>'+
									'<p class=" display-inline-mid doc-name">Document Name</p>'+
								'</div>'+
								'<p class=" display-inline-mid date-time"></p>'+
							'</div>'+
							'<div class="content-hide" style="height: 50px;">'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30">View</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn">Download</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30">Print</button>'+
								'</a>'+
							'</div>'+
						'</div>';

		uiContainer.html("");
		for(var i in oDocs){
			var uiList = $(sTemplate),
				ext = (oDocs[i]["document_path"].substr(oDocs[i]["document_path"].lastIndexOf('.') + 1)).toLowerCase();

			if(ext == 'csv'){
				uiList.find(".icon").addClass('fa-file-excel-o');
			}else if(ext == 'pdf'){
				uiList.find(".icon").addClass('fa-file-pdf-o');
			}

			uiList.data(oDocs[i]);
			uiList.find(".doc-name").html(oDocs[i]["document_name"]);
			uiList.find(".date-time").html(oDocs[i]["date_added"]);
			// uiList.find(".date-time").html(oDocs[i]["date_added_formatted"]);
			if(bChangeBg){
				uiList.removeClass('tbl-dark-color');
				bChangeBg = false;
			}else{
				bChangeBg = true;
			}

			uiContainer.append(uiList);
		}

		cr8v_platform.localStorage.set_local_storage({
			name : "dataUploadedDocument",
			data : oDocs
		});
	}

	function _closeGeneratingModal(iTime) {
		setTimeout(function() {
			uiGenerateViewModal.removeClass('showed');
		}, iTime);
	}

	function _saveDocumentDetails(oParams) {
		var oData = {
			withdrawal_log_id : $('.withdrawal-number').data('withdrawal_log_id'),
			document_name : oParams.document_name,
			document_path : oParams.document_path,
			date_added : oParams.date_added
		},
		oFeedback = {
			speed : 'mid'
		},
		oOptions = {
			type : 'POST',
			data : oData,
			url : BASEURL + 'withdrawals/save_document_details',
			returnType : 'json',
			success : function(oResult) {
				if(oResult.status) {
					oFeedback['title'] = 'Success!';
					oFeedback['message'] = oResult.message;
					oFeedback['icon'] = successIcon;
				} else {
					oFeedback['title'] = 'Error!';
					oFeedback['message'] = oResult.message;
					oFeedback['icon'] = failIcon;
				}

				$('body').feedback(oFeedback);
			}
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	//==================================================//
	//======END FUNCTIONS USED BY MULTIPLE PAGES========//
	//==================================================//


	//==================================================//
	//============ONGOING RECORD FUNCTIONS==============//
	//==================================================//

	function _buildRecordDiv(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Builds the div for the product list
		* page usage :
		* - company_record_list
		*/

		/* USAGE
		* _buildRecordDiv({
		*	template : $('#yourtemplate') // THE TEMPLATE TO BE USED.
		*	details : oDetails // THE DETAILS OF THE RECORD.
		* });
		*/

		var oTemplate = oParams.template,
		oWithdrawal = oParams.details,
		oCloned = oTemplate.clone(),
		oData = {},
		btnViewRecord = {},
		btnViewCompleted = {},
		btnCompleteRecord = {},
		btnConfirmComplete = $('.btn-confirm-complete'),
		btnCancelComplete = $('.btn-cancel-complete'),
		btnViewCompletedRecord = $('.btn-show-completed-record'),
		btnModalClose = $('.modal-close'),
		uiCompleteRecordModal = $('.complete-product-modal'),
		uiParent = {},
		iWithdrawalId = null;


		if(oWithdrawal.status == 0) {
			oCloned.removeClass('withdrawal-ongoing-template');
			btnCompleteRecord = oCloned.find('.btn-complete-record');
		} else {
			oCloned.removeClass('withdrawal-complete-template');
		}
	
		oCloned.addClass('withdrawal-cloned');
		oCloned.attr('data-id', oWithdrawal.id);
		oCloned.find('.withdrawal-number').html(oWithdrawal.withdrawal_number);
		oCloned.find('.withdrawal-date').html(oWithdrawal.date_created);
		oCloned.find('.hover-tbl').css({ 'height' : '100%' });
		oCloned.show();

		// SET THE TEMPLATE'S BUTTONS.	
		btnViewRecord = oCloned.find('.btn-view-record');
		btnViewCompleted = oCloned.find('.btn-view-completed');
		
		// RECORD LIST EVENTS.
		btnViewRecord.off('click').on('click', function() {
			uiParent = $(this).closest('.withdrawal-cloned');
			iWithdrawalId = uiParent.data('id');
			oData = {
				name : 'current_withdrawal',
				data : {withdrawal_id : iWithdrawalId}
			};

			cr8v_platform.localStorage.delete_local_storage({name : 'current_withdrawal'});
			cr8v_platform.localStorage.set_local_storage(oData);
			setTimeout(function() {
				window.location.href = BASEURL + 'withdrawals/withdrawal_company_ongoing';
			}, 100);			
		});

		btnViewCompleted.off('click').on('click', function() {
			uiParent = $(this).closest('.withdrawal-cloned');
			iWithdrawalId = uiParent.data('id');
			oData = {
				name : 'current_withdrawal',
				data : {withdrawal_id : iWithdrawalId}
			};
			
			cr8v_platform.localStorage.delete_local_storage({name : 'current_withdrawal'});
			cr8v_platform.localStorage.set_local_storage(oData);
			setTimeout(function() {
				window.location.href = BASEURL + 'withdrawals/withdrawal_company_completed';
			}, 100);			
		});

		btnModalClose.off('click').on('click', function() {
			var uiModalParent = $(this).closest('.modal-container');

			if(uiModalParent.hasClass('completed-record-modal')) {
				
				uiModalParent.removeClass('showed');
				setTimeout(function() {
					window.location.href = BASEURL + 'withdrawals/company_record_list';
				}, 800);

			} else {
				
				uiModalParent.removeClass('showed');	

			}
		});

		if(oWithdrawal.status == 0) {

			btnCompleteRecord.off('click').on('click', function() {
				var oData = {
					page : 'record_list',
					parent : $(this).closest('.withdrawal-cloned')
				};

				_buildCompleteRecordModal(oData);
			});

			btnViewCompletedRecord.off('click').on('click', function() {
				var oData = {
					withdrawal_id : uiCompleteRecordModal.data('id')
				};

				_setCurrentWithdrawal(oData);
				setTimeout(function() {
					window.location.href = BASEURL + 'withdrawals/withdrawal_company_completed';
				},300);
			});

		}

		return oCloned;
	}

	function _updateProductBatches(oParams) {
		var oDeletedBatches = {},
		oUpdatedBatches = {},
		uiProductDiv = {};
	}

	//==================================================//
	//==========END ONGOING RECORD FUNCTIONS============//
	//==================================================//


	//==================================================//
	//==============BATCH MODAL FUNCTIONS===============//
	//==================================================//

	function _buildBatchDropDown(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Builds the product's batch select dropdown.
		* criticality : CRITICAL
		* page usage :
		* - create_record
		* - view_ongoing
		* parameters : 
		* n/a
		*/

		/*
		* USAGE
		* _buildBatchDropDown();
		*/

		var sBatchList = '<option value="">Select batch</option>',
		uiSelect = $('.modal-batch-dropdown'),
		uiSiblings = uiSelect.siblings();

		if(Object.keys(oProductBatches).length > 0) {
			for(var a in oProductBatches) {
				sBatchList += '<option value="' + oProductBatches[a].batch_id + '">' + oProductBatches[a].batch_name + '</option>';
			}
		} else {
			sBatchList = '<option>No batch found</option>'
		}
		
		uiSiblings.remove();
		uiSelect.html(sBatchList);
		uiSelect.show().css({
			"padding" : "3px 4px",
			"width" :  "220px"
		});
	}

	function _buildBatchModalDisplay(oParams) {
		var iBatchId = oParams.batch_id,
		iProductId = oParams.product_id,
		oBatchDetails = {},
		oFeedback = {},
		oConvertedQty = {},
		oLocationBreadCrumb = {},
		sLocationBreadCrumb = '',
		uiSubLocation = $('.modal-sub-location-template'),
		uiBatchAvailableQty = uiBatchModal.find('.batch-available-qty'),
		uiBatchStorageName = $('.modal-storage-name '),
		uiProductDiv = $('.product-list').find('.product-list-div[data-id="' + iProductId + '"]'),
		inputRadioBulk = uiProductDiv.find('.radio-bulk'),
		inputRadioPiece = uiProductDiv.find('.radio-piece'),
		inputQtytoWithdraw = uiBatchModal.find('.batch-withdraw-amount');

		// inputQtytoWithdraw.decimal(true, 2);

		if(iBatchId == '') {
			// IF NO VALID BATCH IS SELECTED.
			oFeedback = {
				'title' : 'Error!',
				'message' : 'Please select a valid batch.',
				'speed' : 'slow',
				'icon' : failIcon
			};

			$('body').feedback(oFeedback);
			uiBatchStorageName.html('');
			uiBatchAvailableQty.html('');
			uiSubLocation.html('<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Batch Location </p>');

		} else {

			for(var a in oProductBatches) {
				if(iBatchId == oProductBatches[a].batch_id) {
					oBatchDetails = oProductBatches[a];
				}
			}

			if(inputRadioBulk.prop('checked')) {
				// alert('bulk is checked');
			} else {
				// alert('piece is checked');
			}

			oConvertedQty = _buildBatchConvertedQty({
				batch_id : iBatchId,
				product_id : iProductId
			});

			oLocationBreadCrumb = _buildLocationBreadCrumb({id : oBatchDetails.location_id});
			sLocationBreadCrumb = _buildLocationDisplay({name : 'modal_display', location : oLocationBreadCrumb});

			uiSubLocation.data('batch_details', oBatchDetails);
			uiSubLocation.data('location_breadcrumb', oLocationBreadCrumb);
			uiSubLocation.html(sLocationBreadCrumb);
			uiSubLocation.append('<div class="clear"></div>');
			uiBatchStorageName.html(oBatchDetails.storage_name);
			uiBatchAvailableQty.data(oConvertedQty);
			uiBatchAvailableQty.html(oConvertedQty.quantity + ' ' + oConvertedQty.unit_of_measure);
		}	
	}

	function _submitAddBatch(oParams) {
		var iWithdrawaAmount = $('.batch-withdraw-amount').val(),
		inputBatchDropDown = $('.modal-batch-dropdown option:selected'),
		iProductId = uiBatchModal.data('product_id'),
		uiSubLocation = $('.modal-sub-location-template'),
		uiSubLocationQty = $('.batch-available-qty'),
		inputSubLocationQtyToWithdraw = $('.batch-withdraw-amount'),
		iQtyToWithdraw = parseFloat(inputSubLocationQtyToWithdraw.val()),
		oBatchDetails = uiSubLocation.data('batch_details'),
		oLocation = uiSubLocation.data('location_breadcrumb'),
		sLocation = '',
		uiProductDiv = $('.product-list').find('.product-list-div[data-id="' + iProductId + '"]'),
		uiTotalQtyToWithdraw = uiProductDiv.find('.total-withdrawal-qty'),
		uiGrayTemplate = oParams.gray_template,
		uiWhiteTemplate = oParams.white_template,
		uiBatchTemplate = {},
		uiBatchList = uiProductDiv.find('.product-batches-list'),
		oModalWithdrawalDetails = uiBatchModal.find('.batch-available-qty').data(),
		iBatchCount = Object.keys(uiBatchList.find('.batch-cloned-template')).length,
		oTotalDetails = { product_id : iProductId },
		oTotalQtyToWithdraw = {},
		oCurrentBatchDetails = {},
		oData = {},
		btnEditBatch = {},
		btnRemoveBatch = {},
		uiParent = {};

		if(parseInt(iBatchCount) % parseInt(2) == 0) {
			var uiClonedTemplate = uiGrayTemplate.clone();
			uiClonedTemplate.removeClass('batch-gray-template');
		} else {
			var uiClonedTemplate = uiWhiteTemplate.clone();
			uiClonedTemplate.removeClass('batch-white-template');
		}

		sLocation = _buildLocationDisplay({name : 'product_display', location : oLocation});
		
		uiClonedTemplate.data('qty_to_withdraw', iQtyToWithdraw.toFixed(2));
		uiClonedTemplate.data('batch_details', oBatchDetails);
		uiClonedTemplate.data('location_breadcrumb', oLocation);

		uiSubLocationQty.html('');
		inputSubLocationQtyToWithdraw.val('');
		uiSubLocation.html('<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Batch Location </p><div class="clear"></div>');
		uiSubLocation.removeData();
		uiClonedTemplate.removeClass('batch-template');
		uiClonedTemplate.addClass('batch-cloned-template');
		uiClonedTemplate.find('.withdraw-batch-breadcrumb').html(sLocation);
		uiClonedTemplate.find('.batch-withdrawal-number').html(oBatchDetails.batch_name);
		uiClonedTemplate.find('.withdraw-batch-qty').html(iQtyToWithdraw.toFixed(0) + ' ' + oModalWithdrawalDetails.unit_of_measure);
		uiClonedTemplate.show();
		uiClonedTemplate.data(oModalWithdrawalDetails);

		btnRemoveBatch = uiClonedTemplate.find('.btn-batch-remove');
		btnEditBatch = uiClonedTemplate.find('.btn-batch-edit');
		
		// ================================================== //
		// =================BATCH EVENTS===================== //
		// ================================================== //
		
		uiClonedTemplate.off('mouseover').on('mouseover', function() {
			uiClonedTemplate.find('.btn-hover-storage').addClass('showed');
			uiClonedTemplate.find('.btn-hover-storage').css({
				'opacity' : '1'
			});
			uiClonedTemplate.find('.btn-hover-storage').css({
				'height' : '100%'
			});
		});

		uiClonedTemplate.off('mouseleave').on('mouseleave', function() {
			uiClonedTemplate.find('.btn-hover-storage').removeClass('showed');
			uiClonedTemplate.find('.btn-hover-storage').hide();
			uiClonedTemplate.find('.btn-hover-storage').css({
				'opacity' : '0'
			});
		});

		btnRemoveBatch.off('click').on('click', function() {
			oData = {
				title : 'Success!',
				message : 'Batch removed.',
				speed : 'mid',
				icon : successIcon
			};

			uiParent = $(this).closest('.batch-cloned-template');
			uiParent.remove();
			$('body').feedback(oData);

			oTotalQtyToWithdraw = _calculateTotalWithdrawal(oTotalDetails);
			uiTotalQtyToWithdraw.html(oTotalQtyToWithdraw.total_quantity.toFixed(0) + ' ' + oTotalQtyToWithdraw.unit_of_measure);
			uiProductDiv.data('total_quantity', oTotalQtyToWithdraw.total_quantity);
		});

		btnEditBatch.off('click').on('click', function() {
			uiParent = $(this).closest('.batch-cloned-template');
			oCurrentBatchDetails = uiParent.data();
			oData['page'] = 'create_record';
			oData['current_batch'] = oCurrentBatchDetails;
			oData['element'] = $(this);


			_buildBatchModal(oData);
		});

		// ================================================== //
		// ================END BATCH EVENTS================== //
		// ================================================== //

		uiBatchList.append(uiClonedTemplate);
		uiBatchModal.removeClass('showed');

		oTotalQtyToWithdraw = _calculateTotalWithdrawal(oTotalDetails);
		uiTotalQtyToWithdraw.html(oTotalQtyToWithdraw.total_quantity.toFixed(0) + ' ' + oTotalQtyToWithdraw.unit_of_measure);
		uiProductDiv.data('total_quantity', oTotalQtyToWithdraw.total_quantity);
		uiBatchModal.find('.batch-withdraw-amount').val('');
	}

	function _buildBatchConvertedQty(oParams) {
		/*
		* developer : Dru Moncatar
		* description : Converts the batch quantity.
		* criticality : CRITICAL
		* page usage :
		* - view ongoing
		* parameters : 
		* - batch_id
		* - product_id
		*/

		/*
		* USAGE
		* oConvertedQty = _buildBatchConvertedQty({
		*	batch_id : oParams.batch_id,
		*	product_id : oCurrentProduct.product_id
		* });
		*/

		var iBatchId = oParams.batch_id,
		iProductId = oParams.product_id,
		iQtyBalance = null,
		iUnitOfMeasureId = null,
		iBatchUnitOfMeasure = null,
		sBatchUnitOfMeasure = '',
		sUnitOfMeasure = '',
		oBatchDetails = {},
		uiProductDiv = $('.product-list').find('.product-list-div[data-id="' + iProductId + '"]'),
		inputRadioBulk = uiProductDiv.find('.radio-bulk'),
		inputRadioPiece = uiProductDiv.find('.radio-piece'),
		inputUnitOfMeasure = uiProductDiv.find('.unit-of-measure-dropdown option:selected'),
		oData = {};

		for(var a in oProductBatches) {
			if(oProductBatches[a].batch_id == iBatchId) {
				oBatchDetails = oProductBatches[a];
				iBatchUnitOfMeasure = oBatchDetails.unit_of_measure_id;
				sBatchUnitOfMeasure = oBatchDetails.unit_of_measure;
			}
		}

		// IF THE BULK BUTTON IS SELECTED..
		if(inputRadioBulk.prop('checked')) {


			if(sBatchUnitOfMeasure == 'bag') { // IF THE BATCH IS BY PIECE, CONVERT..
				
				iQtyBalance = UnitConverter.convert(oBatchDetails.unit_of_measure, inputUnitOfMeasure.text(), oBatchDetails.batch_quantity);
				iUnitOfMeasureId = inputUnitOfMeasure.val();
				sUnitOfMeasure = inputUnitOfMeasure.text();
				console.log(iQtyBalance + ' ' + sUnitOfMeasure);

			} else { // IF BATCH IS BY BULK

				if(iBatchUnitOfMeasure == inputUnitOfMeasure.val()) { // IF BATCH AND SELECTED UNIT OF MEASURE HAS THE SAME VALUE.
					
					iQtyBalance = oBatchDetails.batch_quantity;
					iUnitOfMeasureId = oBatchDetails.unit_of_measure_id;
					sUnitOfMeasure = oBatchDetails.unit_of_measure;

					console.log(iQtyBalance + ' ' + sUnitOfMeasure);

				} else { // IF NOT THE SAME.. CONVERT...

					iQtyBalance = UnitConverter.convert(oBatchDetails.unit_of_measure, inputUnitOfMeasure.text(), oBatchDetails.batch_quantity);
					sUnitOfMeasure = inputUnitOfMeasure.text();
					iUnitOfMeasureId = inputUnitOfMeasure.val();
					console.log(iQtyBalance + ' ' + sUnitOfMeasure);

				}

			}

		} else { // IF PIECE BUTTON IS SELECTED

			if(sBatchUnitOfMeasure == 'bag') { // IF BATCH IS BY BAG..
				
				iUnitOfMeasureId = iBatchUnitOfMeasure;
				sUnitOfMeasure = 'bag';
				iQtyBalance = oBatchDetails.batch_quantity;

			} else { // IF NOT, CONVERT..

				iQtyBalance = UnitConverter.convert(oBatchDetails.unit_of_measure, 'bag', oBatchDetails.batch_quantity);
				sUnitOfMeasure = 'bag';
				iUnitOfMeasureId = inputUnitOfMeasure.val();

			}
		}

		oData['unit_of_measure'] = sUnitOfMeasure;
		oData['unit_of_measure_id'] = iUnitOfMeasureId;
		oData['quantity'] = iQtyBalance;
		return oData;
	}

	//==================================================//
	//============END BATCH MODAL FUNCTIONS=============//
	//==================================================//


	//==================================================//
	//=======CREATE WITHDRAWAL RECORD FUNCTIONS=========//
	//==================================================//

	function _createWithdrawalRecord() {
		var uiConfirmModal = $('.confirm-create-record'),
		uiCreatedRecordModal = $('.modal-created-record'),
		uiWithdrawalNumber = $('.withdrawal-number-display'),
		
		btnConfirmCreateRecord = uiConfirmModal.find('.btn-confirm-create-record'),
		btnCancelCreateRecord = uiConfirmModal.find('.btn-cancel-create-record'),
		btnCloseCreatedModal = uiCreatedRecordModal.find('.close-modal'),
		btnShowCreatedRecord = uiCreatedRecordModal.find('.btn-show-created-record'),

		iProductContainer = $(".product-list").find(".product-cloned-template").length,
		iBatchContainer = $(".product-batches-list").find(".batch-cloned-template").length,
		
		inputRequestor = $('#item_requestor').val(),
		inputDesignation = $('#designation').val(),
		inputDepartment = $('#department').val(),
		inputReason = $('#reason_for_withdrawal').val(),
		inputWithdrawalNumber = $('.withdrawal-number').data(),
		iWithdrawalNumber = inputWithdrawalNumber.withdrawal_number,
		
		oPage = {page : 'create_record'},
		oData = {
			withdrawal_number : iWithdrawalNumber,
			user_id : 1,
			type : 1,
			requestor : inputRequestor,
			designation : inputDesignation,
			department : inputDepartment,
			request_for_withdrawal : inputReason
		},
		oOptions = {
			type : 'POST',
			data : oData,
			returnType : 'json',
			url : BASEURL + 'withdrawals/create_company_withdrawal',

			success : function(oResult) {
				// console.log(oResult);
				if(oResult.status) {

					uiCreatedRecordModal.addClass('showed');
					uiCreatedRecordModal.data('withdrawal_id', oResult.data);
					uiConfirmModal.removeClass('showed');

				} else {

					var oFeedback = {
						icon : failIcon,
						message : oResult.message,
						speed : mid,
						title : 'Fail!'
					};

					$('body').feedback(oFeedback);

				}
			}
		};

		// SHOWS THE CONFIRM MODAL.
		uiConfirmModal.addClass('showed');
		uiWithdrawalNumber.html(iWithdrawalNumber);

		// EVENT FOR CREATING RECORD AFTER CONFIRMATION.
		btnConfirmCreateRecord.off('click').on('click', function() {
				var oError = [];

				(iProductContainer == 0) ? oError.push("Select Item First") : "";
				(iBatchContainer == 0) ? oError.push("Item Batch Required") : "";
				(inputRequestor == "") ? oError.push("Item Requestor Required") : "";
				(inputDesignation == "") ? oError.push("Designation Required") : "";
				(inputDepartment == "") ? oError.push("Department Required") : "";
				(inputReason == "") ? oError.push("Reason for Withdrawal Required") : "";

				if(oError.length > 0)
				{
					var oFeedback = {icon : failIcon, message : oError.join("<br />"), speed : 'Slow', title : 'Creating Withdrawal Company Goods',  type : "danger"};

					$('body').feedback(oFeedback);
				}else{

					// GET ALL NEW NOTES.
					_getNewNotes(oPage, function(oResult) {
						oOptions['data']['notes'] = oWithdrawalNotes;

						// GET ALL NEW PRODUCTS
						_getNewProducts(oPage, function(oResult) {
							oOptions['data']['products'] = oWithdrawalProducts;

							//GETS ALL NEW DOCUMENTS.
							_getNewDocuments(oPage, function(oResult) {
								oOptions['data']['documents'] = oWithdrawalDocuments;

								cr8v_platform.CconnectionDetector.ajax(oOptions);
								//console.log(oOptions);
							});

						});

					});

				}




			
		});

		// EVENT FOR CANCEL CONFIRM RECORD.
		btnCancelCreateRecord.off('click').on('click', function() {
			uiConfirmModal.removeClass('showed');
		});

		// EVENT FOR CLOSING THE CREATED MODAL.
		btnCloseCreatedModal.off('click').on('click', function() {
			uiCreatedRecordModal.removeClass('showed');
			setTimeout(function() {
				window.location.href = BASEURL + 'withdrawals/withdrawal_company_create';
			}, 800);
		});

		// EVENT FOR SHOWING THE CREATED RECORD.
		btnShowCreatedRecord.off('click').on('click', function() {
			_showNewRecord();	
		});
	}

	//==================================================//
	//=====END CREATE WITHDRAWAL RECORD FUNCTIONS=======//
	//==================================================//


	//==================================================//
	//=========EDIT WITHDRAWAL RECORD FUNCTIONS=========//
	//==================================================//

	function _displayOldProducts(oParams) {
		var oProductDetails = oParams.product,
		uiTemplate = $('.old-product-template'),
		uiOldProductList = $('.old-product-list'),
		uiCloned = {},
		btnParent = {},
		btnRemoveOldProduct = {},
		oDeletedOldProducts = {},
		iDeletedProductCount = uiOldProductList.data('deleted_product_count');

		console.log(oParams);

		uiCloned = uiTemplate.clone();
		uiCloned.removeClass('old-product-template');
		uiCloned.addClass('old-product-cloned');
		uiCloned.find('.old-product-image').html('<img src="' + oProductDetails.product_image + '" alt="" class="height-100percent width-100percent" >');
		uiCloned.find('.old-product-sku-name').html('SKU# ' + oProductDetails.product_sku + ' - ' + oProductDetails.product_name);
		uiCloned.attr('data-id', oProductDetails.product_id);
		uiCloned.show();

		btnRemoveOldProduct = uiCloned.find('.btn-remove-old-product');
		btnRemoveOldProduct.off('click').on('click', function() {
			btnParent = $(this).closest('.old-product-cloned');
			btnParent.remove();
			iDeletedProductCount = parseInt(iDeletedProductCount) + parseInt(1);

			oDeletedOldProducts[iDeletedProductCount] = { product_id : btnParent.data('id') };
			uiOldProductList.data('deleted_product_count', iDeletedProductCount);
			uiOldProductList.data('deleted_product_list', oDeletedOldProducts);

			_checkSelectedProducts({
		  		element : uiOldProductList,
		  		dropdown : $('.product-list-dropdown')
			});
		});

		return uiCloned;
	}

	function _edit(oParams) {
		var oNewProductList = {},
		uiOldProductList = $('.old-product-list'),
		oPage = {page : 'edit_record'},
		oData = {
			deleted_products : {},
			new_products : {},
			input_status : $('.input-div').hasClass('changed') ? 'changed' : 'not_changed',
			requestor : $('.requestor').val(),
			designation : $('.designation').val(),
			department : $('.department').val(),
			reason : $('.reason-for-withdrawal').val(),
			withdrawal_log_id : $('.withdrawal-number').data('withdrawal_log_id')
		},
		oOptions = {
			type : 'POST',
			data : {},
			returnType : 'json',
			url : BASEURL + 'withdrawals/company_edit_record',

			success : function(oResult) {
				var oFeedback = {
					speed : 'mid',
					message : oResult.message
				};

				if(oResult.status) {
					
					uiUpdatedRecordModal.addClass('showed');
					uiUpdatedRecordModal.find('.withdrawal-number-display').html($('.confirm-edit-record').find('.withdrawal-number-display').text());
					$('.confirm-edit-record').removeClass('showed');
					cr8v_platform.localStorage.delete_local_storage({name : 'current_withdrawal'});
					cr8v_platform.localStorage.set_local_storage({name : 'current_withdrawal', data : {withdrawal_id : $('.withdrawal-number').data('withdrawal_log_id')}});

					setTimeout(function() {
						window.location.href = BASEURL + 'withdrawals/withdrawal_company_ongoing';
					}, 2000);

				} else {

					oFeedback['title'] = 'Fail!';
					oFeedback['icon'] = failIcon;
					$('body').feedback(oFeedback);

				}	
			}
		}
		
		_getNewProducts(oPage, function(oResult) {
			oData['new_products'] = Object.keys(oWithdrawalProducts).length > 0 ? oWithdrawalProducts : 'null';
			oData['deleted_products'] = uiOldProductList.data('deleted_product_list') != undefined ? uiOldProductList.data('deleted_product_list') : 'null';
			oOptions['data'] = oData;

			cr8v_platform.CconnectionDetector.ajax(oOptions);
		});
	}

	//==================================================//
	//=======END EDIT WITHDRAWAL RECORD FUNCTIONS=======//
	//==================================================//


	//==================================================//
	//======COMPLETE WITHDRAWAL RECORD FUNCTIONS========//
	//==================================================//

	function _buildCompleteRecordModal(oParams) {
		var uiParent = {},
		btnConfirmComplete = $('.btn-confirm-complete'),
		btnCancelComplete = $('.btn-cancel-complete'),
		oElement = {

			'record_list' : function() {
				uiParent = oParams.parent;
				iWithdrawalId = uiParent.data('id');

				var uiParentWithdrawalNumber = uiParent.find('.withdrawal-number').text();

				uiCompleteRecordModal.addClass('showed');
				uiCompleteRecordModal.attr('data-id', iWithdrawalId);

				btnConfirmComplete.off('click').on('click', function() {
					_completeWithdrawalRecord({page : 'record_list', withdrawal_id : iWithdrawalId, withdrawal_number : uiParentWithdrawalNumber});
				});

				btnCancelComplete.off('click').on('click', function() {
					var uiModalParent = $(this).closest('.modal-container');
					
					if(uiModalParent.hasClass('completed-record-modal')) {
						
						uiModalParent.removeClass('showed');
						setTimeout(function() {
							window.location.href = BASEURL + 'withdrawals/company_record_list';
						}, 800);

					} else {
						
						uiModalParent.removeClass('showed');	

					}
					
				});
			},

			'view_ongoing' : function() {
				iWithdrawalId = oParams.withdrawal_id;
				iWithdrawalNumber  = $('.breadcrumbs').find('.withdrawal-number').text();

				uiCompleteRecordModal.addClass('showed');
				uiCompleteRecordModal.attr('data-id', iWithdrawalId);

				btnConfirmComplete.off('click').on('click', function() {
					_completeWithdrawalRecord({page : 'view_ongoing', withdrawal_id : iWithdrawalId, withdrawal_number : iWithdrawalNumber});
				});

				btnCancelComplete.off('click').on('click', function() {
					var uiModalParent = $(this).closest('.modal-container');
					
					if(uiModalParent.hasClass('completed-record-modal')) {
						
						uiModalParent.removeClass('showed');
						setTimeout(function() {
							window.location.href = BASEURL + 'withdrawals/company_record_list';
						}, 800);

					} else {
						
						uiModalParent.removeClass('showed');	

					}
					
				});
			}
		}
		

		if(typeof(oElement[oParams.page]) == 'function') {
			oElement[oParams.page](oParams.page);
		}
	}

	function _completeWithdrawalRecord(oParams) {
		var iWithdrawalId = oParams.withdrawal_id,
		iWithdrawalNumber = oParams.withdrawal_number,
		uiCompleteRecordModal = $('.complete-product-modal'),
		uiCompletedModal = $('.completed-record-modal'),
		oOptions = {
			type : 'POST',
			data : {withdrawal_id : iWithdrawalId},
			returnType : 'json',
			url : BASEURL + 'withdrawals/company_complete_withdrawal',

			success : function(oResult) {
				if(oResult.status) {
					
					if(oParams.page == 'view_ongoing') {
						
						uiCompleteRecordModal.removeClass('showed');
						uiCompletedModal.addClass('showed');
						uiCompletedModal.find('.withdrawal_number').html(iWithdrawalNumber);
						uiCompletedModal.find('.complete-message').append(' REDIRECTING...');
						_setCurrentWithdrawal({withdrawal_id : iWithdrawalId});
						setTimeout(function() {
							window.location.href = BASEURL + 'withdrawals/withdrawal_company_completed';
						},1500);

					} else {

						uiCompleteRecordModal.removeClass('showed');
						uiCompletedModal.addClass('showed');
						uiCompletedModal.find('.withdrawal_number').html(iWithdrawalNumber);	

					}
					

				} else {

					var oFeedback = {
						message : oResult.message,
						title : 'Fail!',
						speed : 'mid',
						icon : failIcon
					};

					$('body').feedback(oFeedback);

				}
			}
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}


	function _triggerNoteLessMore()
	{
		var uiBtn = $(".show-notes-messge-length");

		uiBtn.off("click.showNotes").on("click.showNotes", function(e){
			e.preventDefault();
			var uiThis = $(this),
				uiParent = uiThis.closest(".notes-contain"),
				sMessage = uiParent.data("message"),
				uiMessage = uiParent.find(".show-notes-message");

				if(uiThis.text() == "Show More")
				{
					uiMessage.html(sMessage);
					uiThis.text("Show Less");
					
				}else{
					var sMessageContainer = "",
						iMessageCount = sMessage.length;

						if(iMessageCount > 300)
						{
							sMessageContainer = sMessage.substr(0, 300);
							sMessageContainer +="...";
						}else{
							sMessageContainer = sMessage;
						}

						uiMessage.html(sMessageContainer);
						uiThis.text("Show More");

				}

		});

	}

	//==================================================//
	//=====END COMPLETE WITHDRAWAL RECORD FUNCTIONS=====//
	//==================================================//

	return {
		bindEvents : bindEvents
	};

})();

$(document).ready(function() {

	CWithdrawalCompany.bindEvents();

});