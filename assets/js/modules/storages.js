var CStorages = (function() {
    var oStorages = null,
        oAllStorageTypes = {},
        oHierarchyFetchedData = {},
        iCurrentEditingHierarchy = 0,
        oSavedDataHierarchy = {},
        failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg";


    // ================================================== //
    // ==================PUBLIC METHODS================== //
    // ================================================== //

    function bindEvents() {
        var sCurrentPath = window.location.href;

        if (sCurrentPath.indexOf("storage_location_management") > -1) {
            _UI_storageLocationManagement(oStorages);

            $(".view_storage_details").off("click").on("click", function() {
                _setCurrentStorage($(this).data("id"));
            });
        }

        cr8v_platform.localStorage.set_local_storage({
            name: "add_product_image",
            data: {
                value: "false"
            }
        });

        if (sCurrentPath.indexOf("add_storage_location_management") > -1) {
            _getStorageType();
            _UI_unitOfMeasure();

            var uploadInstance = $('#upload_photo').cr8vUpload({
                url: BASEURL + "storages/upload",
                // backgroundPhotoUrl : "",
                // titleText : "Change Photo",
                accept: "jpg,png",
                autoUpload: false,
                onSelected: function(bValid) {
                    if (bValid) {
                        cr8v_platform.localStorage.set_local_storage({
                            name: "add_product_image",
                            data: {
                                value: "true"
                            }
                        });
                    } else {
                        cr8v_platform.localStorage.set_local_storage({
                            name: "add_product_image",
                            data: {
                                value: "false"
                            }
                        });
                    }
                },
                success: function(data, textStatus, jqXHR) {
                    _add(data.data.location);
                    // alert(data.data.path);
                },
                error: function(jqXHR, textStatus, errorThrown) {

                }
            });

            $("#submitAddStorage").off("click").on("click", function() {
                $("#addStorageForm").submit();
            });

            $("#addStorageForm").cr8vformvalidation({
                'preventDefault': true,
                'data_validation': 'datavalid',
                'input_label': 'labelinput',
                'onValidationError': function(arrMessages) {
                    // alert(JSON.stringify(arrMessages)); //shows the errors
                    var arr = [];
                    for (var i in arrMessages) {
                        arr.push(arrMessages[i]['error_message']);
                    }
                    $("body").feedback({
                        title: "Error!",
                        message: arr.join("<br/>"),
                        icon: failIcon
                    });
                },
                'onValidationSuccess': function() {
                    var errorStorageSpecific = false,
                        locationContainer = $("#storageHIerarchyListDisplay"),
                        toJson = locationContainer.tree("toJson"),
                        oTree = JSON.parse(toJson);

                    if (oTree == null) {
                        errorStorageSpecific = true;
                    }
                    if (oTree) {
                        if (!oTree[0].hasOwnProperty('children')) {
                            errorStorageSpecific = true;
                        }
                    }
                    if (!errorStorageSpecific) {
                        var bHasImage = cr8v_platform.localStorage.get_local_storage({
                            name: "add_product_image"
                        });

                        if (bHasImage.value == "true") {
                            uploadInstance.startUpload();
                        } else {
                            _add();
                        }
                    } else {
                        $("body").feedback({
                            title: "Storage Specific",
                            message: "Must Set Storage Specific",
                            icon: failIcon
                        });
                    }
                }
            });
        }

        if (sCurrentPath.indexOf("view_storage_location") > -1) {
            _UI_viewStorageLocation();
        }

        if (sCurrentPath.indexOf("storage_location_edit_new_storage") > -1) {
            var currentStorage = {};
            currentStorage = cr8v_platform.localStorage.get_local_storage({
                name: "currentStorage"
            });
            _UI_unitOfMeasure();
            _UI_editStorageCategory({
                sAction: "get"
            });

            cr8v_platform.localStorage.delete_local_storage({
                name: "add_product_image"
            });

            var uploadInstance = $('#upload_photo').cr8vUpload({
                url: BASEURL + "products/upload",
                backgroundPhotoUrl: currentStorage.image,
                titleText: "Change Photo",
                accept: "jpg,png",
                autoUpload: false,
                onSelected: function(bValid) {
                    if (bValid) {
                        cr8v_platform.localStorage.set_local_storage({
                            name: "add_product_image",
                            data: {
                                value: "true"
                            }
                        });
                    } else {
                        cr8v_platform.localStorage.set_local_storage({
                            name: "add_product_image",
                            data: {
                                value: "false"
                            }
                        });
                    }
                },
                success: function(data, textStatus, jqXHR) {
                    _edit(data.data.location);
                    // alert(data.data.path);
                },
                error: function(jqXHR, textStatus, errorThrown) {

                }
            });

            _UI_editStorageLocation();

            $("#formEditStorage").cr8vformvalidation({
                'preventDefault': true,
                'data_validation': 'datavalid',
                'input_label': 'labelinput',
                'onValidationError': function(arrMessages) {
                    // alert(JSON.stringify(arrMessages)); //shows the errors
                    var arr = [];
                    for (var i in arrMessages) {
                        arr.push(arrMessages[i]['error_message']);
                    }
                    $("body").feedback({
                        title: "Error!",
                        message: arr.join(","),
                        icon: failIcon,
                        speed: "slow"
                    });
                },
                'onValidationSuccess': function() {
                    var errorStorageSpecific = false,
                        locationContainer = $("#storageHIerarchyListDisplay"),
                        toJson = locationContainer.tree("toJson"),
                        oTree = JSON.parse(toJson);

                    if (oTree == null) {
                        errorStorageSpecific = true;
                    }
                    if (oTree) {
                        if (!oTree[0].hasOwnProperty('children')) {
                            errorStorageSpecific = true;
                        }
                    }
                    if (!errorStorageSpecific) {
                        var oHasImage = cr8v_platform.localStorage.get_local_storage({
                            name: "add_product_image"
                        });
                        if (oHasImage && oHasImage.value) {
                            uploadInstance.startUpload();
                        } else {
                            _edit(currentStorage.image);
                        }
                    } else {
                        $("body").feedback({
                            title: "Storage Specific",
                            message: "Must Set Storage Specific",
                            icon: failIcon,
                            speed: "slow"
                        });
                    }
                }
            });

            $("#submit_edit_storage").off("click").on("click", function() {
                $("#formEditStorage").submit();
            });

            $(".delete-button").off("click").on("click", function(e) {
                $("#trashModal").addClass("showed");
            });

            var oInt = {},
                iTimes = 0;

            oInt = setInterval(function() {
                iTimes++;
                if (iTimes == 100) {
                    clearInterval(oInt);
                }
                if (Object.keys(oHierarchyFetchedData).length > 0) {
                    /* FOR ADD STORAGE SELECT HIERARCHY DROPDOWN UI */
                    var uiSelect = $("#hierarchySelect"),
                        uiSiblings = uiSelect.siblings();

                    uiSiblings.remove();
                    uiSelect.removeClass("frm-custom-dropdown-origin");
                    uiSelect.html("");
                    for (var i in oHierarchyFetchedData) {
                        var sOption = '<option value="' + oHierarchyFetchedData[i]['id'] + '">' + oHierarchyFetchedData[i]['main_name'] + '</option>';
                        uiSelect.append(sOption);
                    }

                    uiSelect.transformDD();
                    CDropDownRetract.retractDropdown(uiSelect);

                    _triggerSelectedDropDowns();

                    clearInterval(oInt);
                }

            }, 500);


            $("#confirmDelete").off("click").on("click", function() {
                // alert("confirm");
                $("#trashModal").hide();
                _delete();
            });

        }

        /* STORAGE HIERARCHY EVENTS */

        var oTreeData = [];

        if (sCurrentPath.indexOf("system_settings") > -1) {
            var uiAnchorAdd = $('[modal-target="add-storage-hierarchy"]');

            var _hierarchyEvents = function(oHierarchyData, mode) {
                var inputName = $('#hierarchyName'),
                    hierarchyDisplay = $('#hierarchyDisplay'),
                    plusHierarchy = $('#plusHierarchy'),
                    subtractHierarchy = $('#subtractHierarchy'),
                    moveupHierarchy = $('#moveupHierarchy'),
                    movedownHierarchy = $('#movedownHierarchy'),
                    submitHierarchy = $('#addHierarchy'),
                    sDefaultName = "Level";
                oAllHierarchy = (oHierarchyData) ? oHierarchyData : {
                    0: sDefaultName + " 1"
                },
                $template = $('<p data-position="1" class="font-400 border-bottom-medium padding-bottom-10 padding-top-5 "><span>1.</span> <input type="text" first-value="Level " value="" class="width-200px remove-border-radius " id="level"></p>'),
                uiFirstClone = $template.clone(),
                cloneInput = uiFirstClone.find("input"),
                oDeletedHierarchy = {};

                subtractHierarchy.prop("disabled", true);

                submitHierarchy.html("Add Hierarchy");

                hierarchyDisplay.html(uiFirstClone);

                var methods = {
                    "editHierarchy": function(ui) {
                        var uiParent = ui,
                            uiThisSpan = uiParent.find('span'),
                            sPosition = uiParent.attr('data-position'),
                            uiInputHierarchy = uiParent.find('input');

                        if (mode == "create") {
                            uiThisSpan.html(sPosition + ". ");
                            uiInputHierarchy.show();
                        } else if (mode == "edit") {

                        }
                    },
                    "editHierarchyEvent": function(uiInput) {
                        var uiParent = uiInput.closest('p[data-position]'),
                            iPosition = parseInt(uiParent.attr('data-position')) - 1,
                            value = uiInput.val(),
                            sFirstValue = uiInput.attr('first-value');

                        if (mode == "create") {
                            oAllHierarchy[iPosition] = value;
                        } else if (mode == "edit") {
                            oAllHierarchy[iPosition]['name'] = value;
                        }
                        console.log(oAllHierarchy);
                    },
                    "focusOut": function(uiInput) {
                        var uiParent = uiInput.closest('p[data-position]'),
                            iPosition = parseInt(uiParent.attr('data-position')) - 1,
                            value = uiInput.val(),
                            sFirstValue = uiInput.attr('first-value');

                        if (uiInput.val().trim().length == 0) {
                            uiInput.val(sFirstValue);
                        }
                    },
                    "listHierarchy": function() {
                        hierarchyDisplay.html("");
                        for (var i in oAllHierarchy) {
                            var len = parseInt(i),
                                uiTemplateClone = $template.clone(),
                                uiInput = uiTemplateClone.find('input'),
                                uiSpan = uiTemplateClone.find('span'),
                                uiParent = uiTemplateClone,
                                sPosition = uiParent.attr('[data-position]');

                            if (mode == 'create') {
                                uiSpan.html((len + 1) + ".");
                                uiInput.val(oAllHierarchy[i]);
                                uiInput.attr('first-value', oAllHierarchy[i]);
                                uiParent.attr('data-position', (len + 1));
                            } else if (mode == 'edit') {
                                uiSpan.html((len + 1) + ".");
                                uiInput.val(oAllHierarchy[i]['name']);
                                uiInput.attr('first-value', oAllHierarchy[i]['name']);
                                if (oAllHierarchy[i]['id']) {
                                    uiInput.attr('hier-id', oAllHierarchy[i]['id']);
                                } else if (oAllHierarchy[i]['parent_id']) {
                                    uiInput.attr('parent-id', oAllHierarchy[i]['parent_id']);
                                }
                                uiParent.attr('data-position', (len + 1));
                            }

                            uiInput.off("keyup").on("keyup", function(e) {
                                methods.editHierarchyEvent($(this));
                            });

                            uiInput.off('focusout').on('focusout', function() {
                                methods.focusOut($(this));
                            });

                            uiParent.off('dblclick.editHierarchy').on('dblclick.editHierarchy', function() {
                                methods.editHierarchy($(this));
                            });

                            hierarchyDisplay.append(uiTemplateClone);
                        }
                    }
                };

                uiFirstClone.off('dblclick.editHierarchy').on('dblclick.editHierarchy', function() {
                    methods.editHierarchy(uiFirstClone);
                });

                cloneInput.off("keyup").on("keyup", function(e) {
                    methods.editHierarchyEvent($(this));
                });

                cloneInput.off('focusout').on('focusout', function() {
                    methods.focusOut($(this));
                });

                plusHierarchy.off("click.plusHierarchy").on("click.plusHierarchy", function() {
                    $(this).prop('disabled', true);

                    var len = Object.keys(oAllHierarchy).length,
                        uiTemplateClone = $template.clone(),
                        uiInput = uiTemplateClone.find('input'),
                        uiSpan = uiTemplateClone.find('span'),
                        uiParent = uiTemplateClone,
                        sPosition = uiParent.attr('[data-position]');

                    if (mode == 'create') {
                        oAllHierarchy[len] = sDefaultName + " " + (len + 1);
                    } else if (mode == 'edit') {
                        var beforeCurrentHier = oAllHierarchy[len - 1];
                        if (beforeCurrentHier) {
                            var iID = beforeCurrentHier['id'];
                            iID = (iID) ? iID : "refer";
                            oAllHierarchy[len] = {
                                'name': sDefaultName + " " + (len + 1),
                                'parent_id': iID
                            };
                        } else {
                            oAllHierarchy[0] = {
                                'name': sDefaultName + " " + 1,
                                'id': 0
                            };
                        }
                    }

                    methods.listHierarchy();

                    $(this).prop('disabled', false);
                });

                subtractHierarchy.off("click.subtractHierarchy").on("click.subtractHierarchy", function() {
                    $(this).prop('disabled', true);

                    var len = Object.keys(oAllHierarchy).length,
                        index = parseInt(len) - 1,
                        uiParent = $('p[data-position="' + len + '"]'),
                        uiInput = uiParent.find('input'),
                        uiSpan = uiParent.find('span'),
                        sPosition = uiParent.attr('data-position');

                    if (mode == 'edit' && (oAllHierarchy[index].hasOwnProperty("id"))) {
                        oDeletedHierarchy[Object.keys(oDeletedHierarchy).length] = oAllHierarchy[index];
                    }

                    delete oAllHierarchy[index];

                    uiParent.remove();

                    $(this).prop('disabled', false);
                });

                submitHierarchy.off("click.submitHierarchy").on("click.submitHierarchy", function() {
                    if (inputName.val().trim().length > 0 && Object.keys(oAllHierarchy).length > 0) {
                        var oData = {
                                name: inputName.val(),
                                hierarchy: JSON.stringify(oAllHierarchy)
                            },
                            sUrl = "";

                        if (mode == "create") {
                            sUrl = BASEURL + "storages/add_storage_hierarchy";
                        } else if (mode == "edit") {
                            sUrl = BASEURL + "storages/update_storage_hierarchy";
                            oData.hierarchy_id = iCurrentEditingHierarchy;
                            oData.deleted_hierarchy = JSON.stringify(oDeletedHierarchy);
                        }

                        var oOptions = {
                            type: "POST",
                            data: oData,
                            url: sUrl,
                            success: function(oResult) {
                                if (oResult.status) {
                                    var options = {
                                        form: "box",
                                        autoShow: true,
                                        type: "success",
                                        title: "Hierarchy",
                                        message: "Data added",
                                        speed: "slow",
                                        withShadow: true
                                    };
                                    $("body").feedback(options);

                                    $('.modal-close.close-me:visible').trigger("click");

                                    CStorages.get();
                                } else {
                                    var options = {
                                        form: "box",
                                        autoShow: true,
                                        type: "danger",
                                        title: "Invalid Format",
                                        message: oResult.message,
                                        speed: "slow",
                                        withShadow: true,
                                        icon: BASEURL + "assets/js/plugins/feedback/images/fail.svg"
                                    };
                                    $("body").feedback(options);
                                }
                            }
                        }

                        cr8v_platform.CconnectionDetector.ajax(oOptions);
                    } else {
                        var options = {
                            form: "box",
                            autoShow: true,
                            type: "danger",
                            title: "Invalid Format",
                            message: "Please add name and hierarcy storage",
                            speed: "slow",
                            withShadow: true,
                            icon: BASEURL + "assets/js/plugins/feedback/images/fail.svg"
                        };
                        $("body").feedback(options);
                    }
                });

                if (mode == 'edit') {
                    submitHierarchy.html("Save Changes");
                    methods.listHierarchy();
                } else {
                    inputName.val("");
                }

            }


            uiAnchorAdd.off("click.addhierarchy").on("click.addhierarchy", function() {
                _hierarchyEvents(undefined, "create");

                 $("#storage-hierarchy-title").text("Add Storage Hierarchy");
            });

            var oForceAdd = cr8v_platform.localStorage.get_local_storage({
                "name": "forceAddHier"
            });
            if (oForceAdd && oForceAdd.value) {
                uiAnchorAdd.click();
                cr8v_platform.localStorage.delete_local_storage({
                    "name": "forceAddHier"
                });
            }

            $('body').off('click.editHierarchy').on('click.editHierarchy', 'button[edit-hierarchy-storage]', function(e) {
                var uiBtn = $(this),
                    sId = uiBtn.attr('edit-hierarchy-storage'),
                    inputName = $('#hierarchyName');



                $("body").css({
                    overflow: 'hidden'
                });
                $("div[modal-id='add-storage-hierarchy']").addClass("showed");

               $("#storage-hierarchy-title").text("Edit Storage Hierarchy");

                $("div[modal-id='add-storage-hierarchy'] .close-me").off('click.closemodal').on("click.closemodal", function() {
                    $("body").css({
                        'overflow-y': 'initial'
                    });
                    $("div[modal-id='add-storage-hierarchy']").removeClass("showed");
                    $('#hierarchyTrashContainer').hide();
                });


                

                $('#hierarchyTrashContainer').show();
                var editData = {};
                for (var i in oHierarchyFetchedData) {
                    if (oHierarchyFetchedData[i]['id'] == sId) {
                        iCurrentEditingHierarchy = parseInt(sId);
                        editData = oHierarchyFetchedData[i];
                    }
                }

                var oHierNames = {};
                var len = Object.keys(oHierNames).length;

                oHierNames[len] = {
                    'name': editData['hierarchy_name'],
                    'id': editData['id']
                };

                var functionGetChildren = function(oChildren) {
                    for (var n in oChildren) {
                        var lenInner = Object.keys(oHierNames).length;
                        oHierNames[lenInner] = {
                            'name': oChildren[n]['hierarchy_name'],
                            'id': oChildren[n]['id']
                        };
                        if (oChildren[n]['children'].length > 0) {
                            functionGetChildren(oChildren[n]['children']);
                        }
                    }
                };

                if (editData['children'].length > 0) {
                    functionGetChildren(editData['children']);
                }

                $('#hierarchyName').val(editData['main_name']);

                _hierarchyEvents(oHierNames, "edit");

                var uiTrash = $('#hierarchyTrashContainer').find("i.default-cursor.gray-color");
                uiTrash.off("click.trashstorage").on("click.trashstorage", function() {
                    var oOptions = {
                        type: "POST",
                        data: {
                            main_hierarchy: iCurrentEditingHierarchy
                        },
                        url: BASEURL + "storages/delete_storage_hierarchy",
                        success: function(oResult) {
                            if (oResult.status) {
                                var options = {
                                    form: "box",
                                    autoShow: true,
                                    type: "success",
                                    title: "Hierarchy",
                                    message: "Data deleted",
                                    speed: "slow",
                                    withShadow: true
                                };
                                $("body").feedback(options);

                                $('.modal-close.close-me:visible').trigger("click");

                                CStorages.get();
                            }
                        }
                    }

                    cr8v_platform.CconnectionDetector.ajax(oOptions);
                });

            });
        }

        if ((sCurrentPath.indexOf("add_storage_location_management") > -1) || sCurrentPath.indexOf("storage_location_edit_new_storage") > -1) {
            var uiSelectCapacityMeasure = $("#capacityMeasure"),
                uiSelectUnitOfMeasure = $("#selectUnitOfMeasure"),
                maximumStorage = $("#maximumStorage"),
                highSetPoint = $("#highSetPoint"),
                lowSetPoint = $("#lowSetPoint"),
                uiParent = uiSelectUnitOfMeasure.parent(),
                allUnitOfMeasure = {};

            maximumStorage.number(true, 2);
            highSetPoint.number(true, 2);
            lowSetPoint.number(true, 2);

            uiSelectCapacityMeasure.on("change", function() {
                var val = $(this).val(),
                    filteredUOM = {};
                if (val.toLowerCase() == 'weight') {
                    for (var i in allUnitOfMeasure) {
                        if (allUnitOfMeasure[i]['name'] == 'kg' || allUnitOfMeasure[i]['name'] == 'mt') {
                            filteredUOM[Object.keys(filteredUOM).length] = allUnitOfMeasure[i];
                        }
                    }
                } else if (val.toLowerCase() == 'volume') {
                    for (var i in allUnitOfMeasure) {
                        if (allUnitOfMeasure[i]['name'] == 'm3' || allUnitOfMeasure[i]['name'] == 'L') {
                            filteredUOM[Object.keys(filteredUOM).length] = allUnitOfMeasure[i];
                        }
                    }
                }

                manipulateOptions(filteredUOM);

            });

            var selectUnitOfMeasureChangeEvent = function() {
                uiSelectUnitOfMeasure.off("change").on("change", function() {
                    var val = $(this).find("option:selected").html();
                    $('.unit-of-measure-display').html(val);
                });
            }

            var manipulateOptions = function(data) {
                uiParent.find(".frm-custom-dropdown").remove();
                uiSelectUnitOfMeasure.removeClass("frm-custom-dropdown-origin");
                uiSelectUnitOfMeasure.html("");
                for (var i in data) {
                    uiSelectUnitOfMeasure.append("<option value='" + data[i]['id'] + "'>" + data[i]['name'] + "</option>");
                }
                uiSelectUnitOfMeasure.transformDD();
                CDropDownRetract.retractDropdown(uiSelectUnitOfMeasure);

                uiSelectUnitOfMeasure.trigger('change');

                selectUnitOfMeasureChangeEvent();
            }

            getUnitofMeasure(function(data) {

                allUnitOfMeasure = data;

                manipulateOptions(data);

                uiSelectCapacityMeasure.trigger('change');

            });

        }

        /* END OF STORAGE HIERARCHY EVENTS */
    }

    function getAllStorages() {
        var oOptions = {
            type: "GET",
            data: {
                test: true
            },
            returnType: "json",
            url: BASEURL + "storages/get_all_storages",
            success: function(oResult) {
                CStorages.setAllStorage(oResult);
            }
        }
        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    function get() {
        var oOptions = {
            type: "GET",
            data: {
                data: "none"
            },
            url: BASEURL + "storages/get_hierarchy",
            success: function(oResult) {
                if (oResult.status) {
                    if (Object.keys(oResult.data).length == 0) {
                        if (window.location.href.indexOf("add_storage_location_management") > -1) {
                            $('div[modal-id="no-storage-hierarchy"]').addClass("showed");
                            cr8v_platform.localStorage.set_local_storage({
                                "name": "forceAddHier",
                                "data": {
                                    "value": true
                                }
                            });

                            setTimeout(function() {
                                window.location.href = BASEURL + "settings/system_settings";
                            }, 500);

                            return false;
                        }
                    }
                    oHierarchyFetchedData = oResult.data;
                    _displayStorageHierarchy(oResult.data);
                }
            }
        }
        cr8v_platform.CconnectionDetector.ajax(oOptions);

        // get all types of Category
        var oAjaxConfig = {
            type: "GET",
            url: BASEURL + "categories/get_category_type",
            data: {
                params: 'adsds'
            },
            async: false,
            success: function(oResponse) {
                cr8v_platform.localStorage.set_local_storage({
                    "name": "all_category_type",
                    "data": oResponse.data
                });

                var uiCategoryContainer = $("#categoryTypeContainer");
                uiCategoryContainer.html("");

                for (var i in oResponse.data) {
                    var cat = oResponse.data[i],
                        html = '<div class="padding-bottom-10 ">';
                    html += '<input type="checkbox"  id="' + cat.id + '" datavalid="checkbox-atleast-one" labelinput="Storage Category" class="no-margin-all display-inline-mid default-cursor">';
                    html += '<label for ="storage-solid" class="default-cursor font-400 font-14 padding-left-10"  >' + cat.name + '</label>';
                    html += '</div>';
                    uiCategoryContainer.append(html);
                }
            }
        }

        cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);
    }

    function setAllStorage(oResult) {
        oStorages = oResult;
        _setStorageList(oStorages);
        return oStorages;
    }

    function getUnitofMeasure(callBack) {
        var oOptions = {
            type: "POST",
            data: {
                data: "none"
            },
            url: BASEURL + "products/get_unit_of_measure",
            success: function(oResult) {
                if (typeof callBack == 'function') {
                    callBack(oResult.data);
                }
            }
        }

        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    // ================================================== //
    // ===============END OF PUBLIC METHODS============== //
    // ================================================== //

    // ================================================== //
    // =================PRIVATE METHODS================== //
    // ================================================== //


    function _triggerSelectedDropDowns() {
        if (sCurrentPath.indexOf("storage_location_edit_new_storage") > -1) {
            var currentStorage = cr8v_platform.localStorage.get_local_storage({
                name: "currentStorage"
            });
            if (currentStorage == null) {
                window.location.href = BASEURL + "storage_location_management/";
            }
            console.log(currentStorage);

            var arrSelections = ["#capacityMeasure", "#selectUnitOfMeasure", "#storageType", "#hierarchySelect"];

            for (var i in arrSelections) {
                var sThis = arrSelections[i],
                    uiThisSelect = $(sThis),
                    uiParentSelect = uiThisSelect.closest(".select");

                if (sThis == '#capacityMeasure') {
                    uiParentSelect.find('[data-value="' + currentStorage.capacity_measurement + '"]').click();
                }

                if (sThis == '#selectUnitOfMeasure') {
                    uiParentSelect.find('[data-value="' + currentStorage.measurement_id + '"]').click();
                }

                if (sThis == '#storageType') {
                    uiParentSelect.find('[data-value="' + currentStorage.storage_type + '"]').click();
                }

                if (sThis == '#hierarchySelect') {
                    uiParentSelect.find('[data-value="' + currentStorage.storage_hierarchy.id + '"]').click();
                }
            }
        }
    }

    function _generateHierarchyChildrenNames(oData) {
        var arrResponse = [];

        for (var i in oData) {
            arrResponse.push(oData[i]['hierarchy_name']);
            if (oData[i]['children'].length > 0) {
                arrResponse.push(_generateHierarchyChildrenNames(oData[i]['children']));
            }
        }

        return arrResponse;

    }

    var triggerOnCreateLi = function(oDataChild) {
        for (var i in oDataChild) {
            oDataChild[i].onCreateLi(oDataChild[i], $(oDataChild[i]["element"]));
            if (oDataChild[i]["children"].length > 0) {
                triggerOnCreateLi(oDataChild[i]["children"]);
            }
        }
    }

    var maindNodeID = "";

    function _getStorageSpecMethods() {
        var storageSpecMethods = {

            setTreeInit: function(oTreeData) {

                var iInterval = {};
                iInterval = setInterval(function() {
                    if ($("#hierarchySelect").find("option").length > 0) {
                        setTimeout(function() {
                            storageSpecMethods.init();
                            storageSpecMethods.ui.displayContainer.tree(oTreeData);
                            var mainNode = storageSpecMethods.ui.displayContainer.find('ul[role="tree"] li:first-child').data("node");
                            maindNodeID = mainNode.id;
                            triggerOnCreateLi(mainNode.children);

                            clearInterval(iInterval);
                        }, 200);
                    }
                }, 500);
            },

            selectedPosition: -1,

            topTemplate: function() {
                return '<div class="panel-heading  acc-dark-color hover-under-heading top-template">' +
                    '<a class="collapsed " href="javascript:void(0)">' +
                    '<i class="fa fa-caret-down font-20 black-color"></i>' +
                    '<h4 class="panel-title  font-15 active black-color font-500 first-acc margin-left-5 name"></h4>	' +
                    '</a>' +
                    '</div>';
            },

            lowerTemplate: function() {
                return '<div class="panel-collapse collapse in lower-template">' +
                    '<div class="panel-body no-padding-all">' +
                    '<div class="sample-acc">' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            },

            listTemplate: function() {
                return '<div class="acc-light-color padding-all-10  item-on-hover">' +
                    '<i class="fa fa-caret-down font-20 display-inline-mid margin-right-10"></i>' +
                    '<p class="display-inline-mid margin-right-10 font-15 font-500  black-color name"></p> ' +
                    '<p class="italic gray-color display-inline-mid margin-right-10 description"></p>' +
                    '</div>';
            },

            ui: {
                displayContainer: [],
                addListHierarchy: [],
                removeListHierarchy: []
            },

            selectedHierarchy: "",

            hierarchyData: {},

            setHierarchyData: function() {
                for (var i in oHierarchyFetchedData) {
                    if (oHierarchyFetchedData[i]["id"] == storageSpecMethods.selectedHierarchy) {
                        storageSpecMethods.hierarchyData = oHierarchyFetchedData[i];
                    }
                }

                console.log(storageSpecMethods.hierarchyData);
            },

            onSelectEvent: function() {
                $("#hierarchySelect").off("change").on("change", function() {
                    if (storageSpecMethods.selectedHierarchy != $("#hierarchySelect").val()) {
                        storageSpecMethods.selectedHierarchy = $("#hierarchySelect").val();
                        storageSpecMethods.setHierarchyData();
                        storageSpecMethods.selectedPosition = -1;
                        var uiParent = $("#storageHIerarchyListDisplay").closest(".sample-accordion"),
                            newContainer = $("<div id='storageHIerarchyListDisplay' class='accordion_custom border-top-none no-margin-bottom'></div>");
                        uiParent.html(newContainer);
                        storageSpecMethods.ui.displayContainer = newContainer;
                    }

                });
            },

            makeid: function() {
                var text = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                for (var i = 0; i < 5; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));

                return text;
            },

            displayList: function() {

            },

            addEvent: function() {
                var add = storageSpecMethods.ui.addListHierarchy,
                    remove = storageSpecMethods.ui.removeListHierarchy,
                    uiStorageName = ($('#storageName').length > 0) ? $('#storageName') : $("#edit_storage_name"),
                    uiStorageHierList = $('#storageHIerarchyListDisplay');

                add.off("click.addhierarchylist").on("click.addhierarchylist", function(e) {
                    var ui = storageSpecMethods.ui,
                        uiContainer = ui.displayContainer,
                        storageName = uiStorageName.val(),
                        makeid = storageSpecMethods.makeid();

                    if (storageName.trim().length > 0) {
                        var oTree = uiContainer.tree('loadData');
                        if (oTree && oTree.find("ul.jqtree_common li").length == 0) {
                            uiContainer.replaceWith("<div class='accordion_custom border-top-none no-margin-bottom' id='storageHIerarchyListDisplay'></div>");
                            uiContainer = $("#storageHIerarchyListDisplay");
                        }
                        if (oTree == null) {
                            oTreeData = [{
                                label: storageName,
                                id: makeid,
                                children: []
                            }];
                            maindNodeID = makeid;
                            uiContainer.tree({
                                data: oTreeData
                            });
                        } else {
                            var uiContainer = $("#storageHIerarchyListDisplay"),
                                hierData = storageSpecMethods.hierarchyData,
                                selectedNode = uiContainer.tree('getSelectedNode'),
                                selectedNodeId = selectedNode.id,
                                iMaxLevel = 0,
                                iSelectedLevel = 0,
                                margin = 20;

                            var maxLevelLoop = function(oDataLoop) {
                                for (var i in oDataLoop) {
                                    iMaxLevel = iMaxLevel + 1;
                                    if (Object.keys(oDataLoop[i]["children"]).length > 0) {
                                        maxLevelLoop(oDataLoop[i]["children"]);
                                    }
                                }
                            }

                            maxLevelLoop([hierData]);

                            var getSelectedLevel = function(data) {
                                if (data.parent) {
                                    iSelectedLevel = iSelectedLevel + 1;
                                    getSelectedLevel(data.parent);
                                }
                            }

                            getSelectedLevel(selectedNode);


                            var getSelectedHierName = function(iSelLev) {
                                var myLevel = 0,
                                    selectedName = "";
                                var loop = function(oData) {
                                    myLevel = myLevel + 1;
                                    for (var x in oData) {
                                        if (myLevel == iSelLev) {
                                            selectedName = oData[x]["hierarchy_name"];
                                        } else {
                                            loop(oData[x]["children"]);
                                        }
                                    }
                                }
                                loop([hierData]);
                                return selectedName;
                            }

                            var selectedHierName = getSelectedHierName(iSelectedLevel);

                            if (selectedNode && (iMaxLevel >= iSelectedLevel)) {
                                var parentID = selectedNodeId,
                                    parentNode = uiContainer.tree('getNodeById', parentID);
                                uiContainer.tree(
                                    'appendNode', {
                                        label: 'Level',
                                        autoOpen: true,
                                        id: makeid,
                                        onCreateLi: function(node, $li) {
                                            var span = $li.find('div.jqtree-element span.jqtree-title'),
                                                innerSpan = span.find("span");

                                            innerSpan.remove();

                                            span.attr("span-name", node.name);
                                            span.append('<span style="color:#888; font-style: italic; font-size:12px;"> (' + selectedHierName + ') </span>');

                                            span.off("dblclick").on("dblclick", function(e) {
                                                var uiList = $(this).closest('li'),
                                                    uiContainer = $("#storageHIerarchyListDisplay"),
                                                    nodeParent = uiList.data('node'),
                                                    toSaveNodeData = nodeParent,
                                                    thisVal = $(this).attr("span-name");
                                                input = $("<input type='text' class='t-small padding-all-10' />");
                                                input.on("keyup", function(ev) {
                                                    if (ev.keyCode == 13 && $(this).val().trim().length > 0) {
                                                        toSaveNodeData.label = $(this).val();
                                                        uiContainer.tree('updateNode', nodeParent, toSaveNodeData);
                                                        var mainNode = uiContainer.tree("getNodeById", maindNodeID);
                                                        triggerOnCreateLi(mainNode.children);
                                                    }
                                                });
                                                input.on("focusout", function(ev) {
                                                    if ($(this).val().trim().length > 0) {
                                                        toSaveNodeData.label = $(this).val();
                                                        uiContainer.tree('updateNode', nodeParent, toSaveNodeData);
                                                        var mainNode = uiContainer.tree("getNodeById", maindNodeID);
                                                        triggerOnCreateLi(mainNode.children);
                                                    }
                                                });
                                                input.val(thisVal);
                                                $(this).html(input);
                                            });
                                        }

                                    },
                                    parentNode
                                );

                                var createdNode = uiContainer.tree("getNodeById", makeid),
                                    parentNode = createdNode.parent;
                                uiContainer.tree('updateNode', parentNode, {
                                    autoOpen: true,
                                });

                                var mainNode = uiContainer.tree("getNodeById", maindNodeID);

                                triggerOnCreateLi(mainNode.children);

                                if ($(parentNode.element).find("a.jqtree-toggler").hasClass("jqtree-closed")) {
                                    $(parentNode.element).find("a.jqtree-toggler").click();
                                }

                            }
                        }
                    }
                });

                remove.off("click.removehierarchylist").on("click.removehierarchylist", function() {
                    var ui = storageSpecMethods.ui,
                        uiContainer = ui.displayContainer,
                        oTree = uiContainer.tree('loadData');
                    if (oTree != null) {
                        var active = oTree.find('li.jqtree-selected'),
                            activeNode = active.data("node");
                        uiContainer.tree('removeNode', activeNode);
                    }
                });

                uiStorageName.off('keyup.changestoragename').on('keyup.changestoragename', function(e) {
                    if ($("#storageHIerarchyListDisplay").find('ul[role="tree"]').length > 0) {
                        var sVal = $(this).val();
                        if (sVal.trim().length > 0) {
                            var uiContainer = $("#storageHIerarchyListDisplay"),
                                mainNode = uiContainer.tree("getNodeById", maindNodeID),
                                children = mainNode.children;
                            uiContainer.tree('updateNode', mainNode, {
                                label: sVal,
                                children: children
                            });

                            triggerOnCreateLi(mainNode.children);
                        }
                    }
                });

            },


            init: function() {
                storageSpecMethods.onSelectEvent();
                storageSpecMethods.selectedHierarchy = $("#hierarchySelect").val();
                storageSpecMethods.setHierarchyData();
                storageSpecMethods.ui.displayContainer = $("#storageHIerarchyListDisplay");
                storageSpecMethods.ui.addListHierarchy = ($('#addListHierarchy').length > 0) ? $('#addListHierarchy') : $("#addListHierarchyEdit");
                storageSpecMethods.ui.removeListHierarchy = ($("#removeListHierarchy").length > 0) ? $("#removeListHierarchy") : $("#removeListHierarchyEdit");
                storageSpecMethods.ui.displayContainer.html("");
                storageSpecMethods.addEvent();
            }

        };

        return storageSpecMethods;
    }

    function _displayStorageHierarchy(oData) {
        var uiContainer = $('div.hierarchy-table'),
            uiTable = uiContainer.find('table'),
            uiSiblings = uiTable.siblings(),
            sStripe = 'white';

        uiSiblings.remove();
        for (var i in oData) {
            var arrNamesChildren = [];

            arrNamesChildren.push(oData[i]['hierarchy_name']);
            for (var x in oData[i]['children']) {
                var arrChildreNames = _generateHierarchyChildrenNames(oData[i]['children']);
                arrNamesChildren.push(arrChildreNames);
            }

            var sTemplate = '<div class="table-content position-rel tbl-dark-color">' +
                '<div class="content-show height-auto width-100per padding-top-20 padding-bottom-20 ">' +
                '<div class="text-center width-50per display-inline-mid">' +
                '<p>' + oData[i].main_name + '</p>' +
                '</div>' +
                '<div class="text-center width-50per display-inline-mid  ">' +
                '<p class="font-12">' + arrNamesChildren.join(',') + '</p>' +
                '</div>' +
                '</div>' +
                '<div>' +

                '<div class="content-hide" style="height: 100%;">' +
                '<button class="btn general-btn modal-trigger" modal-target="edit-storage-hierarchy" edit-hierarchy-storage="' + oData[i]['id'] + '" style="width: 100px;">Edit</button>' +
                '</div>';
               
            var uiTemplate = $(sTemplate);
            
            if(sStripe == 'dark'){
                uiTemplate.removeClass('tbl-dark-color');
                sStripe = 'white';
            }else{
                 sStripe = 'dark';
            }

            uiTemplate.insertAfter(uiTable);
        }

        /* FOR ADD STORAGE SELECT HIERARCHY DROPDOWN UI */
        var uiSelect = $("#hierarchySelect"),
            uiSiblings = uiSelect.siblings();

        uiSiblings.remove();
        uiSelect.removeClass("frm-custom-dropdown-origin");
        uiSelect.html("");
        for (var i in oData) {
            var sOption = '<option value="' + oData[i]['id'] + '">' + oData[i]['main_name'] + '</option>';
            uiSelect.append(sOption);
        }

        uiSelect.transformDD();
        CDropDownRetract.retractDropdown(uiSelect);
        uiSelect.trigger("change");


        /* FOR ADD STORAGE SPECIFICATION HIERARCHY EVENT */
        if (window.location.pathname.indexOf("add_storage_location_management") > -1) {
            var storageSpecMethods = _getStorageSpecMethods();
            storageSpecMethods.init();
        }

        /* END FOR ADD STORAGE SPECIFICATION HIERARCHY EVENT */
    }

    var makeHierarchy = function(oData, bCreateLi) {
        var flat = {};
        for (var i in oData) {
            var toSave = {
                "id": oData[i]["id"],
                "label": oData[i]["name"],
                "children": [],
                "parent_id": oData[i]["parent_id"],
                "auto_open": true
            };
            if (bCreateLi) {
                toSave["onCreateLi"] = function(node, $li) {
                    var span = $li.find('div.jqtree-element span.jqtree-title'),
                        innerSpan = span.find("span");

                    innerSpan.remove();

                    span.attr("span-name", node.name);
                    span.append('<span style="color:#888; font-style: italic; font-size:12px;"> </span>');

                    span.off("dblclick").on("dblclick", function(e) {
                        var uiList = $(this).closest('li'),
                            uiContainer = $("#storageHIerarchyListDisplay"),
                            nodeParent = uiList.data('node'),
                            toSaveNodeData = nodeParent,
                            thisVal = $(this).attr("span-name");
                        input = $("<input type='text' class='t-small padding-all-10' />");
                        input.on("keyup", function(ev) {
                            if (ev.keyCode == 13 && $(this).val().trim().length > 0) {
                                toSaveNodeData.label = $(this).val();
                                uiContainer.tree('updateNode', nodeParent, toSaveNodeData);
                                var mainNode = uiContainer.tree("getNodeById", maindNodeID);
                                triggerOnCreateLi(mainNode.children);
                            }
                        });
                        input.on("focusout", function(ev) {
                            if ($(this).val().trim().length > 0) {
                                toSaveNodeData.label = $(this).val();
                                uiContainer.tree('updateNode', nodeParent, toSaveNodeData);
                                var mainNode = uiContainer.tree("getNodeById", maindNodeID);
                                triggerOnCreateLi(mainNode.children);
                            }
                        });
                        input.val(thisVal);
                        $(this).html(input);
                    });
                }
            }
            var k = oData[i]["id"];
            flat[k] = toSave;
        }

        for (var i in flat) {
            var parentkey = flat[i].parent_id;
            if (flat[parentkey]) {
                flat[parentkey].children.push(flat[i]);
            }
        }

        var root = [];
        for (var i in flat) {
            var parentkey = flat[i].parent_id;
            if (!flat[parentkey]) {
                root.push(flat[i]);
            }
        }
        return root;
    };

    function _render(oParams) {
        // console.log(oParams);
        var oElement = {
            "storage_location_management": function(oData) {
                $("#storage_location_list").append(oParams.data);
            },
            "add_storage_location_management": function(oData) {
                $("#edit_storage")
            },
            "view_storage_location": function(oData) {
                var uiStorage = oParams.data;
                $("#view_storage_id").html(uiStorage.storage_id);
                $("#view_storage_name").html(uiStorage.name);
                $("#view_storage_address").html(uiStorage.address);
                $("#view_storage_description").html(uiStorage.description);
                $("#view_storage_image").attr("src", uiStorage.image);
                var oLocations = oParams.data.location,
                    arrHierarchyLocation = [],
                    accColor = "dark";

                arrHierarchyLocation = makeHierarchy(oLocations);

                var uiContainer = $("#storageHIerarchyListDisplayViewMode"),
                    uiStorageHierName = $("#hierarchyName"),
                    uiStorageType = $("#storageTypeName");

                _getStorageType(function() {
                    uiStorageHierName.html(oParams.data.storage_hierarchy.main_name);

                    for (var i in oAllStorageTypes) {
                        if (oAllStorageTypes[i]["id"] == oParams.data.storage_type) {
                            uiStorageType.html(oAllStorageTypes[i]["name"]);
                        }
                    }
                });

                uiContainer.tree({
                    autoOpen: true,
                    data: arrHierarchyLocation,
                    selectable: false
                });


                var oStorageDetails = oParams.data,
                    uiStorageId = $("#view_storage_id"),
                    uiStorageName = $("#view_storage_name"),
                    uiStorageAddress = $("#view_storage_address"),
                    uiStorageDescription = $("#view_storage_description"),
                    uiStorageImage = $("#view_storage_image"),
                    uiStorageCapacity = $("#view_storage_capacity"),
                    uiCapacityMeasurement = $("#view_capacity_measurement"),
                    uiStorageMeasurement = $(".view_storage_measurement"),
                    uiStorageCategories = $("#view_storage_categories"),
                    uiStorageHighSetPoint = $("#view_high_setpoint"),
                    uiStorageLowSetPoint = $("#view_low_setpoint"),
                    oStorageCategories = oStorageDetails.storage_category,
                    arrStorageCategories = [],
                    sStorageCategories = "",
                    iCounter = 0;

                for (var b in oStorageCategories) {
                    arrStorageCategories.push(oStorageCategories[b]["name"]);
                }

                sStorageCategories = arrStorageCategories.join(",");

                console.log(oStorageDetails);
                uiStorageId.html(oStorageDetails.storage_id);
                uiStorageName.html(oStorageDetails.name);
                uiStorageAddress.html(oStorageDetails.address);
                uiStorageDescription.html(oStorageDetails.description);
                uiStorageImage.attr("src", oStorageDetails.image);
                uiStorageCapacity.html(oStorageDetails.capacity);
                uiStorageCategories.html(sStorageCategories);
                uiStorageMeasurement.html(oStorageDetails.measure_name);
                uiStorageHighSetPoint.html(oStorageDetails.high_setpoint);
                uiStorageLowSetPoint.html(oStorageDetails.low_setpoint);
                uiCapacityMeasurement.html(oStorageDetails.capacity_measurement);

            },
            "storage_location_edit_new_storage": function(oData) {
                var uiStorage = oParams.data,
                    iImage = uiStorage.image.indexOf("view_storage_location") > -1,
                    uiStorageName = $("#edit_storage_name"),
                    uiStorageDescription = $("#edit_storage_description"),
                    uiCategoryContainer = $("#categoryTypeContainerEdit"),
                    capacityMeasure = $("#capacityMeasure"),
                    maximumStorage = $("#maximumStorage"),
                    highSetPoint = $("#highSetPoint"),
                    lowSetPoint = $("#lowSetPoint"),
                    selectUnitOfMeasure = $("#selectUnitOfMeasure");


                uiCategoryContainer.html("");

                $("#edit_storage_id").val(uiStorage.storage_id);
                uiStorageDescription.val(uiStorage.description);
                uiStorageName.val(uiStorage.name);
                $("#edit_storage_address").val(uiStorage.address);
                $("#crumb_edit_storage_id").html("ID " + uiStorage.id);
                $("#crumb_edit_storage_name").html(uiStorage.name);
                $("#submit_edit_storage").attr("data-id", uiStorage.id);
                // $('.upload-wrapper').css('background-image', 'url(http://localhost/apollo/uploads/images/XgHcQ1EV5RPjVEhWshZm.jpg)');
                $('.upload-wrapper').css('background-image', 'url(' + uiStorage.image + ')');
                _getStorageType();


                maximumStorage.val(uiStorage.capacity);
                highSetPoint.val(uiStorage.high_setpoint);
                lowSetPoint.val(uiStorage.low_setpoint);

                var storageSpecMethods = _getStorageSpecMethods(),
                    uiContainer = $("#storageHIerarchyListDisplay"),
                    uiStorageHierName = $("#hierarchyName"),
                    uiStorageType = $("#storageTypeName"),
                    parentID = "";
                var oLocations = oParams.data.location,
                    arrHierarchyLocation = [],
                    accColor = "dark";
                // get all types of Category
                var oAjaxConfig = {
                    type: "GET",
                    url: BASEURL + "categories/get_category_type",
                    data: {
                        params: 'adsds'
                    },
                    async: false,
                    success: function(oResponse) {
                        cr8v_platform.localStorage.set_local_storage({
                            "name": "all_category_type",
                            "data": oResponse.data
                        });

                        for (var i in oResponse.data) {
                            var cat = oResponse.data[i],
                                html = '<div class="padding-bottom-10 ">';
                            html += '<input type="checkbox"  id="' + cat.id + '" datavalid="checkbox-atleast-one" labelinput="Storage Category" class="no-margin-all display-inline-mid default-cursor">';
                            html += '<label for ="storage-solid" class="default-cursor font-400 font-14 padding-left-10"  >' + cat.name + '</label>';
                            html += '</div>';

                            var $elem = $(html);

                            for (var x in uiStorage.storage_category) {
                                if (uiStorage.storage_category[x]["category_id"] == cat.id) {
                                    $elem.find("input").prop("checked", true);
                                }
                            }

                            uiCategoryContainer.append($elem);
                        }
                        arrHierarchyLocation = makeHierarchy(oLocations, true);
                        storageSpecMethods.init();

                        storageSpecMethods.setTreeInit({
                            autoOpen: true,
                            data: arrHierarchyLocation
                        });
                    }
                }
                cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);

            },
            "storageType": function(oData) {
                // console.log(oParams.data);
                var uiSelect = $("#storageType");
                var uiSiblings = uiSelect.siblings();
                uiSiblings.remove();
                uiSelect.removeClass("frm-custom-dropdown-origin");
                $("#storageType").html(oParams.data);
                $("#storageType").transformDD();
                CDropDownRetract.retractDropdown( $("#storageType"));
            },
            "unit_of_measure": function(oData) {
                var uiStorageMeasureSelect = $("#storageMeasure"),
                    uiSiblings = uiStorageMeasureSelect.siblings();

                uiSiblings.remove();
                uiStorageMeasureSelect.removeClass("frm-custom-dropdown-origin");
                uiStorageMeasureSelect.html(oParams.data);
                uiStorageMeasureSelect.transformDD();
                CDropDownRetract.retractDropdown(uiStorageMeasureSelect);
            },
            "edit_storage_category": function(oData) {
                var uiCategories = oParams.data,
                    uiSolidCheckBox = $("#storage-solid"),
                    uiLiquidCheckBox = $("#storage-liquid"),
                    uiGasCheckBox = $("#storage-gas"),
                    oCategories = JSON.parse(uiCategories);

                for (var a in oCategories) {
                    if (oCategories[a].category_type_id == 1) {
                        uiSolidCheckBox.prop("checked", true);
                    }
                    if (oCategories[a].category_type_id == 2) {
                        uiLiquidCheckBox.prop("checked", true);
                    }
                    if (oCategories[a].category_type_id == 3) {
                        uiGasCheckBox.prop("checked", true);
                    }
                }
            }
        }

        if (typeof(oElement[oParams.name]) == "function") {
            oElement[oParams.name](oParams.name);
        }
    }

    function _reverseDataHierarchy(oData) {



        var reverse = function(oData) {
            var arrReversedKeys = Object.keys(oData).reverse();
            var oNewData = {};
            arrReversedKeys.forEach(function(element, index, array) {
                oNewData[Object.keys(oNewData).length] = oData[element];
            });
            return oNewData;
        }

        var oToReturn = {};

        var loop = function(oLoop) {
            for (var i in oLoop) {
                if (Object.keys(oLoop[i]["locations"]).length > 0) {
                    loop(oLoop[i]["locations"]);
                    oLoop[i]["locations"] = reverse(oLoop[i]["locations"]);
                }
            }
            return oLoop;
        }
        var oReverse = loop(oData);
        return oReverse

    }

    function _add(sImage) {
        var sStorageID = $("#storageID"),
            uiCategoryContainer = $("#categoryTypeContainer"),
            uiCatInputs = uiCategoryContainer.find("input[type='checkbox']"),
            selectUnitOfMeasure = $("#selectUnitOfMeasure").val(),
            capacityMeasure = $('#capacityMeasure').val(),
            maximumStorage = $("#maximumStorage").val(),
            highSetPoint = $("#highSetPoint").val(),
            lowSetPoint = $("#lowSetPoint").val(),
            hierarchyID = $("#hierarchySelect").val(),
            oCategories = {};

        $.each(uiCatInputs, function() {
            if ($(this).prop("checked")) {
                oCategories[Object.keys(oCategories).length] = {
                    "value": $(this).attr("id")
                };
            }
        });

        var sJSON = $("#storageHIerarchyListDisplay").tree("toJson"),
            oTree = JSON.parse(sJSON),
            newSaveHierarchy = {};

        var loopChildSetData = function(oData, oParentData) {
            var oChildren = {};
            for (var i in oData.children) {
                var len = Object.keys(oChildren).length;
                oChildren[len] = {
                    "name": oData.children[i]["name"],
                    "locations": {},
                    "type": $("#storageType").val()
                }
            }

            oParentData.locations = oChildren;

            for (var i in oData.children) {
                loopChildSetData(oData.children[i], oParentData.locations[i]);
            }


        }


        for (var x in oTree) {
            newSaveHierarchy[0] = {
                "name": oTree[x]["name"],
                "locations": {},
                "type": $("#storageType").val()
            }

            loopChildSetData(oTree[x], newSaveHierarchy[0]);
        }


        var oOptions = {
            type: "POST",
            data: {
                storage_id: sStorageID.val(),
                name: $("#storageName").val(),
                address: $("#storageAddress").val(),
                company_id: 1,
                storage_type: $("#storageType").val(),
                image: sImage,
                description: $("#storageDescription").val(),
                capacity_measure: capacityMeasure,
                measurement_id: selectUnitOfMeasure,
                locations: JSON.stringify(newSaveHierarchy),
                categories: JSON.stringify(oCategories),
                maximum_storage: maximumStorage,
                high_setpoint: highSetPoint,
                low_setpoint: lowSetPoint,
                hierarchy_id: hierarchyID
            },
            url: BASEURL + "storages/add_storages",
            beforeSend: function() {
                $('[modal-id="add-storage"]').addClass('showed');
            },
            success: function(oResult) {
                if (oResult.status) {
                    var oFeedback = {
                        title: "Success!",
                        message: "Storage added.",
                        speed: "slow"
                    };
                    $("body").feedback(oFeedback);
                    setTimeout(function() {
                        window.location.reload();
                    }, 500);
                } else {
                    var oFeedback = {
                        title: "Fail!",
                        message: "Storage not added.",
                        speed: "slow",
                        icon: failIcon
                    };
                    $("body").feedback(oFeedback);
                }
            }
        };

        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    function _edit(sImage) {
        var sStorageID = $("#edit_storage_id"),
            uiCategoryContainer = $("#categoryTypeContainerEdit"),
            uiCatInputs = uiCategoryContainer.find("input[type='checkbox']"),
            capacityMeasure = $('#capacityMeasure').val(),
            maximumStorage = $("#maximumStorage").val(),
            highSetPoint = $("#highSetPoint").val(),
            lowSetPoint = $("#lowSetPoint").val(),
            oCategories = {};

        $.each(uiCatInputs, function() {
            if ($(this).prop("checked")) {
                oCategories[Object.keys(oCategories).length] = {
                    "value": $(this).attr("id")
                };
            }
        });

        var sJSON = $("#storageHIerarchyListDisplay").tree("toJson"),
            oTree = JSON.parse(sJSON),
            newSaveHierarchy = {};

        var loopChildSetData = function(oData, oParentData) {
            var oChildren = {};
            for (var i in oData.children) {
                var len = Object.keys(oChildren).length;
                oChildren[len] = {
                    "name": oData.children[i]["name"],
                    "locations": {},
                    "type": $("#storageType").val()
                }
            }

            oParentData.locations = oChildren;

            for (var i in oData.children) {
                loopChildSetData(oData.children[i], oParentData.locations[i]);
            }


        }


        for (var x in oTree) {
            newSaveHierarchy[0] = {
                "name": oTree[x]["name"],
                "locations": {},
                "type": $("#storageType").val()
            }

            loopChildSetData(oTree[x], newSaveHierarchy[0]);
        }

        var oOptions = {
            type: "POST",
            data: {
                id: $("#submit_edit_storage").data("id"),
                storage_id: sStorageID.val(),
                name: $("#edit_storage_name").val(),
                address: $("#edit_storage_address").val(),
                image: sImage,
                storage_type: $("#storageType").val(),
                capacity: $("#edit_storage_capacity").val(),
                description: $("#edit_storage_description").val(),
                measurement_id: $("#selectUnitOfMeasure").val(),
                capacity_measure: capacityMeasure,
                locations: JSON.stringify(newSaveHierarchy),
                categories: JSON.stringify(oCategories),
                maximum_storage: maximumStorage,
                high_setpoint: highSetPoint,
                low_setpoint: lowSetPoint,
                hierarchy_id: $("#hierarchySelect").val()
            },
            url: BASEURL + "storages/edit",
            beforeSend: function() {
                $('[modal-id="update-storage"]').addClass('showed');
            },
            success: function(oResult) {
                if (oResult.status) {
                    var oFeedback = {
                        title: "Success!",
                        message: "Storage updated.",
                        speed: "slow"
                    };
                    $("body").feedback(oFeedback);
                    cr8v_platform.localStorage.set_local_storage({
                        "name": "currentStorage",
                        "data": oResult.data
                    });
                    window.location.reload();
                } else {
                    var oFeedback = {
                        title: "Fail!",
                        message: "Storage not updated.",
                        speed: "slow",
                        icon: failIcon
                    };
                    $("body").feedback(oFeedback);
                }
            }
        };
        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    function _delete() {
        var oCurrentStorage = _getCurrentStorage(),
            iStorage_id = oCurrentStorage.id,
            oOptions = {
                type: "POST",
                data: {
                    storage_id: iStorage_id
                },
                returnType: "json",
                url: BASEURL + "storages/delete",
                success: function(oResult) {
                    if (oResult > 0) {
                        var oFeedback = {
                            title: "Success!",
                            message: "Storage deleted."
                        };
                        $("body").feedback(oFeedback);
                        getAllStorages();
                        setTimeout(function() {
                            window.location = BASEURL + "storage_location_management";
                        }, 1000);
                    } else {
                        var oFeedback = {
                            title: "Error!",
                            message: "Storage not deleted.",
                            icon: failIcon
                        };
                        $("body").feedback(oFeedback);
                    }
                }
            };
        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    function _buildTemplate(oParams) {
        var oTemplate = {},
            oElement = {
                "storage_location_management": function(oData) {
                    var sTemplate_head = "",
                        sTemplate_content = "",
                        sTemplate_footer = "";

                    sTemplate_head = '<div class="tbl-like">' + '<div class="first-tbl height-auto text-left">';
                    // <p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">123456</p>
                    // <p class="width-25percent font-400 font-14 display-inline-mid f-none text-center ">Warehouse 1</p>
                    // <p class="width-15percent font-400 font-14 display-inline-mid f-none text-center ">Solid, Liquid, Gas</p>
                    // <p class="width-15percent font-400 font-14 display-inline-mid f-none text-center ">Warehouse</p>
                    // <p class="width-28percent font-400 font-14 display-inline-mid f-none text-center ">123 Something something St., Kapuso Sub., Siyudad City</p>
                    sTemplate_content = '</div>' + '<div class="hover-tbl height-100per">' + '<a href="' + BASEURL + 'storage_location_management/view_storage_location">';
                    // <button class="btn general-btn ">View Storage Settings</button>
                    sTemplate_footer = '</a>' + '</div>' + '</div>';
                    oTemplate = {
                        head: sTemplate_head,
                        content: sTemplate_content,
                        footer: sTemplate_footer
                    };
                },
                "add_storage_location_management": function(oData) {

                },
                "view_storage_location": function(oData) {

                },
                "storage_location_edit": function(oData) {

                }
            }

        if (typeof(oElement[oParams.name]) == "function") {
            oElement[oParams.name](oParams.name);
        }
        return oTemplate;
    }

    function _setCurrentStorage(iStorage_id) {
        var oData = {};
        for (var a in oStorages) {
            if (oStorages[a].id == iStorage_id) {
                oData = {
                    name: "currentStorage",
                    data: oStorages[a]
                };
            }
        }

        cr8v_platform.localStorage.delete_local_storage({
            name: "currentStorage"
        });
        cr8v_platform.localStorage.set_local_storage(oData);
    }

    function _getCurrentStorage() {
        var oCurrentStorage = {};

        oCurrentStorage = cr8v_platform.localStorage.get_local_storage({
            name: "currentStorage"
        });
        return oCurrentStorage;
    }

    function _updateCurrentStorage() {
        getAllStorage(); //UPDATES THE LOCAL STORAGE WITH THE NEW DB RECORDS.
        var oCurrentStorage = _getCurrentStorage();

        for (var a in oStorages) {
            if (oStorages[a].id == current_product.id) {
                _setCurrentStorage(oStorages[a]);
            }
        }
    }

    function _getStorageType(callBack) {
        var oOptions = {
            type: "GET",
            data: {
                test: true
            },
            url: BASEURL + "storages/get_storage_type",
            success: function(oResult) {
                oAllStorageTypes = oResult.data;
                _UI_storageType(oAllStorageTypes);

                if (typeof callBack == 'function') {
                    callBack();
                }
            }
        }
        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    function _setStorageList(oStorages) {
        var oStorageList = {};

        oStorageList = {
            name: "storageList",
            data: oStorages
        };
        cr8v_platform.localStorage.delete_local_storage({
            name: "storageList"
        });
        cr8v_platform.localStorage.set_local_storage(oStorageList);
    }

    function getAllUnitsOfMeasure() {
        var oOptions = {
            type: "GET",
            data: {
                test: true
            },
            returnType: "json",
            url: BASEURL + "storages/get_all_unit_of_measures",
            success: function(oResult) {
                _setUnitOfMeasureList(oResult);
            }
        }
        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    function _setUnitOfMeasureList(oResult) {
        var oMeasures = oResult;

        cr8v_platform.localStorage.delete_local_storage({
            name: "measureList"
        });
        cr8v_platform.localStorage.set_local_storage({
            name: "measureList",
            data: oMeasures
        });
    }

    function _getStorageUnitOfMeasure(iStorage_id) {
        var oOptions = {
            type: "GET",
            data: {
                storage_id: iStorage_id
            },
            returnType: "json",
            url: BASEURL + "storages/get_all_storage_category",
            success: function(oResult) {
                _UI_editStorageCategory({
                    sAction: "set",
                    oData: oResult
                });
            }
        }
        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    // ================================================== //
    // ==============END OF PRIVATE METHODS============== //
    // ================================================== //

    // ================================================== //
    // ====================UI METHODS==================== //
    // ================================================== //

    function _UI_storageLocationManagement() {
        var oTemplate = _buildTemplate({
                name: "storage_location_management"
            }),
            sStorageList = "";
        var arrStorages = cr8v_platform.localStorage.get_local_storage({
            name: "storageList"
        });
        var iCounter = parseInt(0);

        for (var a in arrStorages) {
            var oCategories = arrStorages[a].storage_category,
                sCategories = "",
                arrCategoriesName = [];
            iCounter = 0;

            for (var b in oCategories) {
                arrCategoriesName.push(oCategories[b]["name"]);
            }

            sCategories = arrCategoriesName.join(",");

            sStorageList += oTemplate.head;
            sStorageList += '<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">' + arrStorages[a].storage_id + '</p>' + '<p class="width-25percent font-400 font-14 display-inline-mid f-none text-center ">' + arrStorages[a].name + '</p>' + '<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center ">' + sCategories + '</p>' + '<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center ">' + arrStorages[a].type_of_storage + '</p>' + '<p class="width-28percent font-400 font-14 display-inline-mid f-none text-center ">' + arrStorages[a].address + '</p>';
            sStorageList += oTemplate.content;
            sStorageList += '<button class="btn general-btn view_storage_details" data-id="' + arrStorages[a].id + '">View Storage Settings</button>';
            sStorageList += oTemplate.footer;
        }

        _render({
            name: "storage_location_management",
            data: sStorageList
        });
    }

    function _UI_viewStorageLocation() {
        var currentStorage = {};

        currentStorage = cr8v_platform.localStorage.get_local_storage({
            name: "currentStorage"
        });
        _render({
            name: "view_storage_location",
            data: currentStorage
        });
    }

    function _UI_editStorageLocation() {
        var currentStorage = {};

        currentStorage = cr8v_platform.localStorage.get_local_storage({
            name: "currentStorage"
        });
        _render({
            name: "storage_location_edit_new_storage",
            data: currentStorage
        });
    }

    function _UI_storageType(oType) {
        var sElement = "";

        for (var a in oType) {
            sElement += '<option value="' + oType[a].id + '">' + oType[a].name + '</option>';
        }
        _render({
            name: "storageType",
            data: sElement
        });
    }

    function _UI_unitOfMeasure() {
        var sMeasureList = "",
            oMeasureList = {};

        oMeasureList = cr8v_platform.localStorage.get_local_storage({
            name: "measureList"
        });
        for (var a in oMeasureList) {
            sMeasureList += '<option value=' + oMeasureList[a].id + '>' + oMeasureList[a].name + '</option>'
        }

        _render({
            name: "unit_of_measure",
            data: sMeasureList
        });
    }

    function _UI_editStorageCategory(oAction) {
        if (oAction.sAction == "get") {
            var oCurrentStorage = {},
                iStorageId = null;

            oCurrentStorage = cr8v_platform.localStorage.get_local_storage({
                name: "currentStorage"
            });
            iStorageId = oCurrentStorage.id;
            _getStorageUnitOfMeasure(iStorageId);
        }
        if (oAction.sAction == "set") {
            _render({
                name: "edit_storage_category",
                data: oAction.oData
            });
        }
        // console.log(oCurrentStorage);
    }

    // ================================================== //
    // ===============ENDS OF UI METHODS================= //
    // ================================================== //


    /*if(oStorages === null
	|| oStorages === undefined
	|| oStorages == "") {
		getAllStorages();
	}*/

    return {
        // buildTemplate : buildTemplate,
        getAllStorages: getAllStorages,
        bindEvents: bindEvents,
        get: get,
        setAllStorage: setAllStorage,
        getAllUnitsOfMeasure: getAllUnitsOfMeasure,
        getUnitofMeasure: getUnitofMeasure
    };
})();

$(document).ready(function() {

     var data = $('#highSetPoint').val();
        var test = parseInt(data);
        console.log(test);
    if (window.location.href.indexOf("system_settings") > -1 || window.location.href.indexOf("storage_location_management") > -1) {
        CStorages.bindEvents();
        CStorages.get();
        CStorages.getAllStorages();
    }
   
});