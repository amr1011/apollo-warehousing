var CLoadingType = (function() {
	var oAllLoadType = {};

	/* PUBLIC METHODS */
	function get(){
		var oOptions = {
			type : "GET",
			data : {"param" : "none"},
			url : BASEURL + "settings/getAllType",
			success : function(oResultType) {
				
				if(oResultType.status === true){
					oAllLoadType = oResultType.data;
					_displayLoadingType(oResultType.data);

					cr8v_platform.localStorage.set_local_storage({
						name : "getAllLoadingTypeData",
						data: oAllLoadType
					});
				}
			}
		};
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}


	function bindEvents(sAction, ui) {
			if(sAction === 'editLoadingType')
			{
				var uiBtn = $(".trigger-button");


				uiBtn.off('click.editLoadingType').on('click.editLoadingType', function(){
					var uiThis = $(this).attr("data-id");


					 $("body").css({overflow:'hidden'});
					 var tm = $(this).attr("modal-target");

					 $("div[modal-id~='"+tm+"']").addClass("showed");

					 $("div[modal-id~='"+tm+"'] .close-me").on("click",function(){
					 	$("body").css({'overflow-y':'initial'});
					 	$("div[modal-id~='"+tm+"']").removeClass("showed");
					 });

					 var oGetAllLoadingType = cr8v_platform.localStorage.get_local_storage({name : "getAllLoadingTypeData"});

					 for(var x in oGetAllLoadingType)
					 {
						if(oGetAllLoadingType[x]["id"] == uiThis)
					 	{
					 		var oCollectData = {};

					 			oCollectData = {
					 				id:uiThis,
					 				name:oGetAllLoadingType[x]["name"]
									
								}

					 			CVesselList.renderElement("updatevessel", oCollectData);

					 			cr8v_platform.localStorage.set_local_storage({
					 			"name": "currentLoadingType",
					 			"data" : oCollectData
					 			});

					 			 $("#editTypeName").val(oCollectData.name);

					 			 

					 	}

					 }


				});
			}


		
		

		$("#AddLoadingTypeForm").cr8vformvalidation({
			'preventDefault' : true,
		    'data_validation' : 'datavalid',
		    'input_label' : 'labelinput',
		    'onValidationError': function(arrMessages) {

		        var options = {
					  form : "box",
					  autoShow : true,
					  type : "danger",
					  title  : "Add Type Name",
					  message : "Please fill up required field",
					  speed : "slow",
					  withShadow : true
					}
					
					$("body").feedback(options);
		        },

		    'onValidationSuccess': function() {
		       
		        add();

		        var options = {
					  form : "box",
					  autoShow : true,
					  type : "success",
					  title  : "Add Type Name",
					  message : "Successfully Added",
					  speed : "slow",
					  withShadow : true
					}
					
					$("body").feedback(options);

		    }
		});//end

		var uiAddVesselSubmit = $("#addLoadingTypeBtn");
		uiAddVesselSubmit.off("click.AddType").on("click.AddType", function(){
			$("#AddLoadingTypeForm").submit();
		})//end

		


		$("#EditLoadingTypeForm").cr8vformvalidation({

			'preventDefault' : true,
		    'data_validation' : 'datavalid',
		    'input_label' : 'labelinput',
		    'onValidationError': function(arrMessages) {
		        // alert(JSON.stringify(arrMessages)); //shows the errors

		        var options = {
					  form : "box",
					  autoShow : true,
					  type : "danger",
					  title  : "Updated Area Name",
					  message : "Please fill up required field",
					  speed : "slow",
					  withShadow : true
					}
					
					$("body").feedback(options);
		        },
		    'onValidationSuccess': function() {

		    	var iAreaname = cr8v_platform.localStorage.get_local_storage({name : "currentLoadingType"}),
		    		iTypeId = parseInt(iAreaname.id);

					update(iTypeId);
		        
				        var options = {
							  form : "box",
							  autoShow : true,
							  type : "success",
							  title  : "Edit Type Name",
							  message : "Successfully Updated",
							  speed : "slow",
							  withShadow : true
							}
							
							$("body").feedback(options);
							
		    }
		});

		var uiUpdateVessel = $( "#updateType");				
		uiUpdateVessel.off("click.updateVesselBtn").on("click.updateVesselBtn", function(){
	
		$("#EditLoadingTypeForm").submit();
			
		});


		var uiTrashBtn = $('#deleteTypeName');
		uiTrashBtn.off("click.delete").on("click.delete", function(){
			$("div.delete-modal").addClass("showed");

			$("div.delete-modal .close-me").off("click").on("click",function(){
				$("body").css({'overflow-y':'initial'});
				$("div.delete-modal").removeClass("showed");
			});
		});

		$('#confirmDelete').off("click").on("click", function(){
			var iAreaname = cr8v_platform.localStorage.get_local_storage({name : "currentLoadingArea"}),
		    		iAreaId = parseInt(iAreaname.id);
					_delete(iAreaId);

		});

	}

	function add() {	
		var name = $('#typeName').val();

		var oAjaxConfig = {
			type: "POST",
			url : BASEURL+"settings/addType",
			dataType: "json",
			data:{ "name": name},
			success: function(response)
			{
				$('div[modal-id="add-loading-type"] .close-me').trigger("click");
				$('#typeName').val("");
				get();
				
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);

		cr8v_platform.localStorage.delete_local_storage({name:"setCurrentImage"});
	}

	function update(iTypeId) {
		var iAreaname = cr8v_platform.localStorage.get_local_storage({name : "currentLoadingType"}),
		iTypeId = parseInt(iAreaname.id);
		
		var editName = $('#editTypeName').val();
			

		
		var oAjaxConfig = {
			type: "POST",
			url : BASEURL+"settings/updateType",
			dataType: "json",
			data:{ "name": editName,"id":iTypeId},
			success: function(response)
			{
				var options = {
					  form : "box",
					  autoShow : true,
					  type : "success",
					  title  : "Updated Vessel",
					  message : "Successfully Updated",
					  speed : "slow",
					  withShadow : true
					}

					$("body").feedback(options);
				$('div[modal-id="edit-loading-type"] .close-me').trigger("click");
				$('#editTypeName').val("");
				get();
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);
		cr8v_platform.localStorage.delete_local_storage({name:"currentEditVessel"});

	}


	function renderElement(sAction, oData){
					
	}

	/* END PUBLIC METHODS */

	/* PRIVATE METHODS */
	

	function _delete(iTypeId){
		var iTypename = cr8v_platform.localStorage.get_local_storage({name : "currentLoadingType"}),
		iTypeId = parseInt(iTypename.id);
		var oDataAjax = {
			type : "POST",
			url : BASEURL+"settings/deleteType",
			data : { "id":iTypeId },
			success: function(response)
			{
				var options = {
					  form : "box",
					  autoShow : true,
					  type : "success",
					  title  : "Delete Area Name",
					  message : "Successfully Deleted",
					  speed : "slow",
					  withShadow : true
					}
					$("body").feedback(options);
					
					$('div[modal-id="edit-vessel"] .close-me').trigger("click");
					$(".close-me").trigger("click");

					get();
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oDataAjax);
	}



	function _getLoadingTypeTemplate() {
		var sHtml =	'<div class="display-loading-area table-content position-rel" >';
				sHtml += '<div class="content-show height-auto width-100per padding-top-20 padding-bottom-20 background">';
						sHtml += '<div class="text-center width-100per display-inline-mid">';
							sHtml += '<p class="type-name"></p>';
						sHtml += '</div>';
				sHtml += '</div>';
				sHtml += '<div class="content-hide" style="height:100%;">';
				sHtml += '<button class="btn general-btn trigger-button modal-trigger" modal-target="edit-loading-type" style="width: 100px;">Edit</button>';
				sHtml += '</div>';
			sHtml += '</div>';
		return $(sHtml);
	}


	function _displayLoadingType(oAllLoadType){
		var uiLoadingAreaContainer = $("#displayLoadingType"),
			uiDataContainer = $(".trigger-button"),
			sStripe = 'white';
		uiLoadingAreaContainer.html("");

		for(var i in oAllLoadType){
			var singleAreaName = oAllLoadType[i],
				name = singleAreaName.name;
				uiTemplate = _getLoadingTypeTemplate();

				var uiSetId = uiTemplate.find(".trigger-button");

				uiSetId.attr("data-id", singleAreaName.id);

			
			
			uiTemplate.find(".type-name").append(name);

			if(sStripe == 'white'){
				uiTemplate.find('.background').addClass('tbl-dark-color');
				sStripe = 'dark';
			}else{
				sStripe = 'white';
			}

			uiLoadingAreaContainer.append(uiTemplate);

			bindEvents("editLoadingType", uiTemplate);
		}
	}
	/* END PRIVATE METHODS */

	return {
		get : get,
		add : add,
		update : update,
		bindEvents : bindEvents,
		renderElement : renderElement
	}

})();

$(document).ready(function() {
	
	if(sCurrentPath.indexOf("system_settings") > -1){

		CLoadingType.get();
		CLoadingType.bindEvents();

	}

});

