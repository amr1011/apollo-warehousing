var CUsers = (function() {
	var oUsers = {},
	oUserDetails = {},
	oUserPermissions = {},
	oRoles = {},
	oRoleDetails = {},
	oTeams = {},
	oTeamDetails = {},
	oPermissions = {},
	oPermissionDetails = {},

	uiCloned = {},
	uiParent = {},
	uiTemplate = {},
	uiTeamList = $('.team-list'),
	uiUserList = $('.user-list'),
	uiGeneratingViewModal = $('.modal-generate-view'),
	uiAddTeamModal = $('.add-team-modal'),
	uiEditTeamModal = $('.edit-team-modal'),
	uiDeleteTeamModal = $('.delete-team-modal'),
	uiUserModal = $('.user-modal'),
	uiEditUserModal = $('.edit-user-modal'),
	uiDeleteUserModal = $('.delete-user-modal'),

	iObjectLastKey = 0,
	iCurrentTeamId = null,
	iCurrentUserId = null,

	failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg",
	successIcon = BASEURL + "assets/js/plugins/feedback/images/success.svg",

	uiCancelCreate = $(".cancel-go-back"),
	
	oFeedback = {
		speed : 'mid',
		icon : failIcon,
		title : 'Fail!',
		message : ''
	};

	function bindEvents() {
		var oData = {};

		if(BASEURL + 'team_management/view_team' == window.location.href) {
			oData['page'] = 'view_team';

			_getCurrentTeam(function() {

				_getTeamUsers(iCurrentTeamId, function() {
					
					_getTeamDetails(iCurrentTeamId, function() {
						
						_getAllRoles(function() {
							_render(oData);
						});
						
					});

				});

			});

			$('.close-me').off('click').on('click', function() {
				uiParent = $(this).closest('.modal-container');
				uiParent.removeClass('showed');
			});

			$('.show-password').on('click', function() {

				$('.show-password').addClass('hidden');
				$('.hide-password').removeClass('hidden');
				$('#password').prop("type", "text");
			});

			$('.hide-password').on('click', function() {


				$('.show-password').removeClass('hidden');
				$('.hide-password').addClass('hidden');
				$('#password').prop("type", "password");
			});

			$('.show-password').on('click', function() {

				$('.show-password').addClass('hidden');
				$('.hide-password').removeClass('hidden');
				$('#edit_password').prop("type", "text");
			});

			$('.hide-password').on('click', function() {


				$('.show-password').removeClass('hidden');
				$('.hide-password').addClass('hidden');
				$('#edit_password').prop("type", "password");
			});
		}

		if(BASEURL + 'team_management' == window.location.href) {
			oData['page'] = 'team_management';

			console.log(sCurrentPath.indexOf('team_management') > - 2);

			_getAllTeam(function() {
				_render(oData);
			});

			$('.close-me').off('click').on('click', function() {
				uiParent = $(this).closest('.modal-container');
				uiParent.removeClass('showed');
			});
		}

		//$('.close-me').off('click').on('click', function() {
			//uiParent = $(this).closest('.modal-container');
			//uiParent.removeClass('showed');
		//});

		

		uiCancelCreate.off("click.cancelCreate").on("click.cancelCreate", function(){

			$("body").css({overflow:'hidden'});
				
			$("#cancelModal").addClass("showed");

			$("#cancelModal .close-me").on("click",function(){
				$("body").css({'overflow-y':'initial'});
				$("#cancelModal").removeClass("showed");
			});

			var uiConfirmCancelBtn = $("#confirmCancel");

			 uiConfirmCancelBtn.off('click.confirmedcancel').on('click.confirmedcancel', function(){
			 	window.location.href = BASEURL+"receiving/company_goods";
			 });
		});


	}

	function _render(oParams) {
		var oElement = {
			
			'view_team' : function() {
				uiGeneratingViewModal.addClass('showed');

				var oData = {},
				uiClonedUser = {},
				uiTeamName = $('.team-name'),
				btnAddUser = $('#submit_add_user'),
				btnEditTeam = $('.btn-edit-team'),
				btnDeleteTeam = $('.btn-delete-team'),
				inputTeamName = $('#team_name'),
				setImage="";
				iUserId = null;

				uiTeamName.html(oTeamDetails[0]['name']);
				inputTeamName.attr('placeholder', oTeamDetails[0]['name']);
				_buildRoleDropDown($('.user-role-dd'));



				if(typeof(oUsers) != 'object') {

					oData['message'] = 'No users found.';
					oData['time'] = 2000;
					_closeGeneratingViewModal(oData);

				} else {
					
					iObjectLastKey = parseInt(Object.keys(oUsers).length) - parseInt(1);
					oData['time'] = 500;
					uiTemplate = $('.user-template');

					
					for(var a in oUsers) {
						if(oUsers[a]['image'] == ""){

							setImage = "../assets/images/default-prod-img.png";
						}
						else{
							setImage = oUsers[a]['image'];
						}

						uiCloned = uiTemplate.clone();
						uiCloned.removeClass('user-template');
						uiCloned.addClass('cloned-user');
						uiCloned.find('.user-name').html(oUsers[a].fname + ' ' + oUsers[a].lname);
						uiCloned.find('.user-image').attr('src' , setImage);
						uiCloned.show();
						uiCloned.css('display', 'inline-block');
						uiCloned.data('user_id', oUsers[a].id);
						uiUserList.append(uiCloned);

						iObjectLastKey == a ? _closeGeneratingViewModal(oData) : '';
					}

					uiClonedUser = $('.cloned-user');
					uiClonedUser.off('click').on('click', function() {
						iUserId = $(this).data('user_id');
						_buildUserModal(iUserId);
					});

				}

				btnAddUser.off('click').on('click', function() {
					_addUser();
				});

				btnEditTeam.off('click').on('click', function() {
					_editTeam();
				});

				btnDeleteTeam.off('click').on('click', function() {
					_deleteTeam();
				});		
			},

			'team_management' : function() {
				uiGeneratingViewModal.addClass('showed');

				var oData = {},
				inputTeamName = $('#team_name'),
				btnAddTeam = $('#submit_add_team');
				
				if(typeof(oTeams) != 'object') {
					
					oData['message'] = 'No team records found, please add a team.';
					oData['time'] = 2000;
					_closeGeneratingViewModal(oData);
					
				} else {
					var sFirstChar = '',
					sMemberCount = '';

					iObjectLastKey = parseInt(Object.keys(oTeams).length) - parseInt(1);
					oData['time'] = 500;
					uiTemplate = $('.team-template');

					for(var a in oTeams) {

						sMemberCount = oTeams[a]['member_count'] > 1 &&  oTeams[a]['member_count'] != 0 ? ' members' : ' member';
						sFirstChar = _getFirstLetter(oTeams[a]['name']);
						uiCloned = uiTemplate.clone();
						uiCloned.removeClass('team-template');
						uiCloned.addClass('cloned-team');
						uiCloned.show();
						uiCloned.css('display', 'inline-block');
						uiCloned.find('.team-name').html(oTeams[a]['name']);
						uiCloned.find('img').attr('src', BASEURL + 'assets/images/profile/profile_' + sFirstChar + '.png');
						uiCloned.find('.team-number-of-members').html(oTeams[a]['member_count'] + sMemberCount);
						uiCloned.data('team_id', oTeams[a]['id']);
						uiTeamList.append(uiCloned);

						iObjectLastKey == a ? _closeGeneratingViewModal(oData) : '';

					}

					$('.cloned-team').off('click').on('click', function() {
						_setCurrentTeam($(this).data('team_id'));
						setTimeout(function() {
							window.location.href = BASEURL + 'team_management/view_team';
						},300);
					});

				}

				btnAddTeam.off('click').on('click', function() {
					_addTeam();
				});
			}
		};

		cr8v_platform.localStorage.set_local_storage({
						name:"setAddCurrentSelectedImage",
						data: {value:'../assets/images/default-prod-img.png'}
					});

		var userUpload = $(".upload-user").cr8vUpload({
				url: BASEURL+"team_management/upload",
				backgroundPhotoUrl: '../assets/images/default-prod-img.png',
				accept: "jpg,png",
				autoUpload: true,
				onSelected : function(bValid)
				{
					// if(bValid)
					// {
					// 	cr8v_platform.localStorage.set_local_storage({
					// 		name : "add_consignee_image",
					// 		data : {value:"true"}
					// 	});
					// }else{
					// 	cr8v_platform.localStorage,set_local_storage({
					// 		name : "add_consignee_image",
					// 		data : {value:"false"}
					// 	});
					// }
				},
				success: function(data, textStatus, jqXHR)
				{
					// _add(data.data.location);
					var	imagePath = data.data.location;
					cr8v_platform.localStorage.set_local_storage({
						name:"setAddCurrentSelectedImage",
						data: {value:imagePath}
					});
					var ogetit = cr8v_platform.localStorage.get_local_storage({name:"setAddCurrentSelectedImage"});
					console.log(ogetit);

				},
				error: function(jqXHR, textStatus, errorThrown)
				{

				}
			});

		if(typeof(oElement[oParams.page]) == 'function') {
			oElement[oParams.page](oParams.page);
		}
	}

	//==================================================//
	//========GETTING VALUES OF GLOBAL VARIABLES========//
	//==================================================//

	function _getAllTeam(callBack) {
		var oOptions = {
			type : 'GET',
			data : {get : true},
			url : BASEURL + 'users/get_all_team',
			returnType : 'json',
			success : function(oResult) {
				oTeams = oResult.status == true ? oResult.data : 'No records found.';
				callBack(oTeams);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllUsers(callBack) {
		var oOptions = {
			type : 'GET',
			data : {get : true},
			url : BASEURL + 'users/get_all_users',
			returnType : 'json',
			success : function(oResult) {
				oUsers = oResult.status == true ? oResult.data : 'No records found.';
				callBack(oUsers);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllRoles(callBack) {
		var oOptions = {
			type : 'GET',
			data : {get : true},
			url : BASEURL + 'users/get_all_roles',
			returnType : 'json',
			success : function(oResult) {
				oRoles = oResult.status == true ? oResult.data : 'No records found.';
				callBack(oUsers);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllPermissions(callBack) {
		var oOptions = {
			type : 'GET',
			data : {get : true},
			url : BASEURL + 'users/get_all_permissions',
			returnType : 'json',
			success : function(oResult) {
				oPermissions = oResult.status == true ? oResult.data : 'No records found.';
				callBack(oPermissions);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getTeamUsers(iTeamId, callBack) {
		var oOptions = {
			type : 'GET',
			data : {team_id : iTeamId},
			url : BASEURL + 'users/get_team_users',
			returnType : 'json',
			success : function(oResult) {
				oUsers = oResult.status == true ? oResult.data : 'No users found.';
				callBack(oUsers);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getUserDetails(iUserId, callBack) {
		var oOptions = {
			type : 'GET',
			data : {user_id : iUserId},
			url : BASEURL + 'users/get_user_details',
			returnType : 'json',
			success : function(oResult) {
				oUserDetails = oResult.status == true ? oResult.data : 'No user details found.';
				callBack(oUserDetails);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getTeamDetails(iTeamId, callBack) {
		var oOptions = {
			type : 'GET',
			data : {team_id : iTeamId},
			url : BASEURL + 'users/get_team_details',
			returnType : 'json',
			success : function(oResult) {
				oTeamDetails = oResult.status == true ? oResult.data : 'No team record found.';
				callBack(oTeamDetails);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	//==================================================//
	//=====END GETTING VALUES OF GLOBAL VARIABLES=======//
	//==================================================//


	//==================================================//
	//===============TEAM FUNCTIONALITIES===============//
	//==================================================//

	function _setCurrentTeam(iTeamId) {
		var oData = {
			name : 'current_team',
			data : {team_id : iTeamId}
		};

		cr8v_platform.localStorage.delete_local_storage({name : 'current_team'});
		cr8v_platform.localStorage.set_local_storage(oData);
	}

	function _getCurrentTeam(callBack) {
		var oData = {
			name : 'current_team'
		};

		iCurrentTeamId = cr8v_platform.localStorage.get_local_storage(oData);
		iCurrentTeamId = iCurrentTeamId['team_id'];
		callBack();
	}

	function _addTeam() {
		var oOptions = {
			type : 'POST',
			data : {team_name : $('#team_name').val()},
			url : BASEURL + 'users/add_team',
			returnType : 'json',
			success : function(oResult) {
				oFeedback['message'] = oResult.message;

				if(!oResult.status) {
					
					$('body').feedback(oFeedback);

				} else {
					
					oFeedback['title'] = 'Success!';
					oFeedback['icon'] = successIcon;
					_notifyAndRefresh({
						modal : uiAddTeamModal,
						feedback : oFeedback,
						link : BASEURL + 'team_management',
						time : 800
					});

				}
			}	
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _editTeam() {
		var btnSubmitEdit = $('#submit_edit_team'),
		oOptions = {
			type : 'POST',
			data : {},
			url : BASEURL + 'users/edit_team',
			returnType : 'json',
			success : function(oResult) {
				oFeedback['message'] = oResult.message;

				if(!oResult.status) {
					
					$('body').feedback(oFeedback);

				} else {
					
					oFeedback['title'] = 'Success!';
					oFeedback['icon'] = successIcon;
					_notifyAndRefresh({
						modal : uiEditTeamModal,
						feedback : oFeedback,
						link : BASEURL + 'team_management/view_team',
						time : 800
					});

				}
			}
		}
		
		uiEditTeamModal.addClass('showed');
		btnSubmitEdit.off('click').on('click', function() {
			oOptions['data'] = {team_id : oTeamDetails[0]['id'], team_name : $('#team_name').val()};
			cr8v_platform.CconnectionDetector.ajax(oOptions);
		});
	}

	function _deleteTeam() {
		var btnConfirmDelete = $('.btn-confirm-delete-team'),
		oOptions = {
			type : 'POST',
			data : { team_id : oTeamDetails[0]['id'] },
			url : BASEURL + 'users/delete_team',
			returnType : 'json',
			success : function(oResult) {
				oFeedback['message'] = oResult.message;

				if(!oResult.status) {
					uiDeleteTeamModal.removeClass('showed');
					$('body').feedback(oFeedback);
				} else {
					oFeedback['icon'] = successIcon;
					oFeedback['title'] = 'Success!';
					_notifyAndRefresh({
						modal : uiDeleteTeamModal,
						feedback : oFeedback,
						link : BASEURL + 'team_management',
						time : 800
					});
				}
			}
		}

		uiDeleteTeamModal.addClass('showed');

		btnConfirmDelete.off('click').on('click', function() {
			cr8v_platform.CconnectionDetector.ajax(oOptions);
		});
	}

	//==================================================//
	//=============END TEAM FUNCTIONALITIES=============//
	//==================================================//


	//==================================================//
	//===============USER FUNCTIONALITIES===============//
	//==================================================//

	function _setCurrentUser(iUserId) {
		var oData = {
			name : 'current_user',
			data : {user_id : iUserId}
		};

		cr8v_platform.localStorage.delete_local_storage({name : 'current_user'});
		cr8v_platform.localStorage.set_local_storage(oData);
	}

	function _getCurrentUser(callBack) {
		var oData = {
			name : 'current_user'
		};

		iCurrentUserId = cr8v_platform.localStorage.get_local_storage(oData);
		iCurrentUserId = iCurrentUserId['user_id'];
		callBack();
	}

	function _addUser() {
		
		_getCurrentTeam(function() {

			_buildUserPermissionArray(function() {


				var ogetImage = cr8v_platform.localStorage.get_local_storage({name:"setAddCurrentSelectedImage"});


				var oData = {
					fname : $('#fname').val(),
					lname : $('#lname').val(),
					username : $('#username').val(),
					password : $('#password').val(),
					role : $('.user-role-dd').val(),
					user_image: ogetImage.value,
					team_id : iCurrentTeamId,
					permissions : oUserPermissions
				},
				oOptions = {
					type : 'POST',
					data : oData,
					url : BASEURL + 'users/add_user',
					returnType : 'json',
					success : function(oResult) {
						oFeedback['message'] = oResult.message;

						if(!oResult.status) {

							$('body').feedback(oFeedback);	

						} else {

							oFeedback['icon'] = successIcon;
							oFeedback['title'] = 'Success!';
							_notifyAndRefresh({
								modal : uiUserModal,
								feedback : oFeedback,
								link : BASEURL + 'team_management/view_team',
								time : 800
							});


						cr8v_platform.localStorage.delete_local_storage({name:"setAddCurrentSelectedImage"});

						}

					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);

			});

		});	
	}

	function _deleteUser() {
		var oOptions = {
			type : 'POST',
			data : {user_id : oUserDetails[0]['id']},
			url : BASEURL + 'users/delete_user',
			returnType : 'json',
			success : function(oResult) {
				oFeedback['message'] = oResult.message;

				if(!oResult.status) {

					$('body').feedback(oFeedback);

				} else {

					oFeedback['icon'] = successIcon;
					oFeedback['title'] = 'Success!';
					uiEditUserModal.removeClass('showed');
					_notifyAndRefresh({
						modal : uiDeleteUserModal,
						feedback : oFeedback,
						link : BASEURL + 'team_management/view_team',
						time : 800
					});

				}
			}
		},
		btnConfirmDelete = $('.btn-confirm-delete-user');

		uiDeleteUserModal.addClass('showed');
		uiDeleteUserModal.find('.user-name').html(oUserDetails[0]['fname'] + ' ' + oUserDetails[0]['lname']);

		btnConfirmDelete.off('click').on('click', function() {
			cr8v_platform.CconnectionDetector.ajax(oOptions);
		});
	}

	function _editUser() {
		var oSelectedPermissions = {},
		oNewPermissions = {},
		oOldPermissions = {},
		oDeletedPermissions = {},
		oNewUserData = {},
		oOldUSerData = {},
		iCounter = 0,
		iSameCount = 0,
		ogetImage = cr8v_platform.localStorage.get_local_storage({name:"setUpdateSelectedUserImage"}),
		inputUserPermissions = $('.edit-user-permission'),
		oData = {
			user_id : uiEditUserModal.data('id')
		},
		oOptions = {
			type : 'POST',
			data : {},
			url : BASEURL + 'users/edit_user',
			returnType : 'json',
			success : function(oResult) {
				oFeedback['message'] = oResult['message'];

				if(!oResult.status) {

					$('body').feedback(oFeedback);

				} else {

					oFeedback['icon'] = successIcon;
					oFeedback['title'] = 'Success!';

					_notifyAndRefresh({
						modal : uiEditUserModal,
						feedback : oFeedback,
						link : BASEURL + 'team_management/view_team',
						time : 800
					});

					cr8v_platform.localStorage.delete_local_storage({name:"setUpdateSelectedUserImage"});

				}
				
			}
		};

		oOldUSerData = uiEditUserModal.data();
		oOldPermissions = oOldUSerData['permissions'];

		inputUserPermissions.each(function() {
			if($(this).prop('checked')) {
				oSelectedPermissions[ iCounter ] = $(this).data('id');
				iCounter++;
			}
		});

		if(!(Object.keys(oOldPermissions).length > 0)) {

			oNewPermissions = oSelectedPermissions;

		} else {
			
			iCounter = 0;
				
			for(var c in oSelectedPermissions) {

				for(var b in oOldPermissions) {

					if(oOldPermissions[b]['id'] == oSelectedPermissions[c]) {

						++iSameCount;

					}

				}

				if(iSameCount == 0) {

					oNewPermissions[ iCounter ] = oSelectedPermissions[c];
					++iCounter;

				}

				iSameCount = 0;
			}

			iCounter = 0;

			for(var e in oOldPermissions) {
				
				for(var d in oSelectedPermissions) {
					
					if(oOldPermissions[e]['id'] == oSelectedPermissions[d]) {

						++iSameCount;

					}

				}

				if(iSameCount == 0) {

					oDeletedPermissions[ iCounter ] = oOldPermissions[e]['id'];
					++iCounter;
				}
				iSameCount = 0;

			}
						

		}

		oData['image'] = ogetImage.value;
		oData['fname'] = $('#edit_fname').val();
		oData['lname'] = $('#edit_lname').val();
		oData['username'] = $('#edit_username').val();
		oData['password'] = $('#edit_password').val().length == 0 ? 'null' : $('#edit_password').val();
		oData['role'] = $('#edit_user_role').val();
		oData['deleted_permissions'] = Object.keys(oDeletedPermissions).length > 0 ? oDeletedPermissions : 'null';
		oData['new_permissions'] = Object.keys(oNewPermissions).length > 0 ? oNewPermissions : 'null';
		oOptions['data'] = oData;

		//console.log(oData);

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _buildUserModal(iUserId) {
		var btnEditUser = $('.btn-edit-user'),
		inputUserPermissions = $('.view-user-permission'),
		setImage="",
		oUserPermissions = {};
		
		_getUserDetails(iUserId, function(oResult) {

						if(oUserDetails[0]['image'] == ""){

							setImage = "../assets/images/default-prod-img.png";
						}
						else{
							setImage = oUserDetails[0]['image'];
						}

			oUserPermissions = oUserDetails[0]['permissions'];
			uiUserModal.addClass('showed');
			uiUserModal.find('.user-fname').html(oUserDetails[0]['fname']);
			uiUserModal.find('.user-lname').html(oUserDetails[0]['lname']);
			uiUserModal.find('.user-role').html(oUserDetails[0]['user_role']);
			uiUserModal.find('.user-username').html(oUserDetails[0]['username']);
			uiUserModal.find('.user-pic').attr('src',setImage);
			uiUserModal.find('.user-password').data('password', oUserDetails[0]['password']);
			inputUserPermissions.attr('checked', false);

			inputUserPermissions.each(function() {
				for(var a in oUserPermissions) {
					if(oUserPermissions[a]['id'] == $(this).data('id')) {
						$(this).attr('checked', true);
					}
				}
			});

		});

		btnEditUser.off('click').on('click', function() {
			uiUserModal.removeClass('showed');
			_buildEditUserModal();
		});
	}

	function _buildEditUserModal() {
		var inputUserName = $('#edit_username'),
		inputFName = $('#edit_fname'),
		inputLName = $('#edit_lname'),
		inputPassword = $('#edit_password'),
		changeImage = $('.hover-edit-imghere'),
		inputRole = $('#edit_user_role'),
		inputUserPermissions = $('.edit-user-permission'),
		btnSubmitEdit = $('.btn-submit-edit-user'),
		btnDeleteUSer = $('.btn-delete-user'),
		oSelectedPermissions = {},
		setImage = "";


		_getAllRoles(function() {
			oSelectedPermissions = oUserDetails[0]['permissions'];
			_buildRoleDropDown($('#edit_user_role'));
			
			inputUserName.val(oUserDetails[0]['username']);
			inputFName.val(oUserDetails[0]['fname']);
			inputLName.val(oUserDetails[0]['lname']);
			changeImage.attr('src', oUserDetails[0]['image']);

			inputRole.find('option').each(function() {
				if($(this).text() == oUserDetails[0]['user_role']) {
					inputRole.val($(this).val());
				}
			});
			inputUserPermissions.each(function() {
				for(var a in oSelectedPermissions) {
					if(oSelectedPermissions[a]['id'] == $(this).data('id')) {
						$(this).prop('checked', true);
					}
				}
			});

			cr8v_platform.localStorage.set_local_storage({
									name:"setUpdateSelectedUserImage",
									data: {value:oUserDetails[0]['image']}
								});

			if(oUserDetails[0]['image'] == ""){

							setImage = "../assets/images/default-prod-img.png";
						}
						else{
							setImage = oUserDetails[0]['image'];
						}

			

			var userUpload = $(".change-img").cr8vUpload({
						url: BASEURL+"team_management/upload",
						backgroundPhotoUrl: setImage,
						accept: "jpg,png",
						autoUpload: true,
						onSelected : function(bValid)
						{
							if(bValid)
							{
								cr8v_platform.localStorage.set_local_storage({
									name : "update_user_image",
									data : {value:"true"}
								});
							}else{
								cr8v_platform.localStorage.set_local_storage({
									name : "update_user_image",
									data : {value:"false"}
								});
							}
						},
						success: function(data, textStatus, jqXHR)
						{

							var imagePath = data.data.location;
								cr8v_platform.localStorage.set_local_storage({
									name:"setUpdateSelectedUserImage",
									data: {value:imagePath}
								});
						},
						error: function(jqXHR, textStatus, errorThrown)
						{

						}
					});

			uiEditUserModal.data(oUserDetails[0]);
			uiEditUserModal.addClass('showed');
		});


		
		btnSubmitEdit.off('click').on('click', function() {
			_editUser();
		});

		btnDeleteUSer.off('click').on('click', function() {
			_deleteUser();
		});

		
	}

	function _buildRoleDropDown(uiElementDd) {
		/*
		* developer : Dru Moncatar
		* description : Builds the product dropdowndown 
		* criticality : CRITICAL
		* page usage :
		* - create record
		* - edit record
		*/

		/*
		*	USAGE
		*	_buildProductDropDown();
		*/

		var sRoleList = '',
		// uiSelect = $('.user-role-dd'),
		uiSelect = uiElementDd,
		uiSiblings = uiSelect.siblings();
		
		for(var a in oRoles) {
			sRoleList += '<option value="' + oRoles[a].id + '">' + oRoles[a].name + '</option>';
			oRoles[a] = $(sRoleList);
		}

		uiSiblings.remove();
		uiSelect.html('');
		uiSelect.append(sRoleList);
		uiSelect.show().css({
			"padding" : "3px 4px",
			"width" :  "200px"
		});
	}

	function _buildUserPermissionArray(callBack) {
		var uiCheckBox = $('.permission-box'),
		iCounter = 0,
		oCheckedPermissions = {};

		uiCheckBox.each(function() {
			if($(this).prop('checked')) {
				oCheckedPermissions[ iCounter ] = $(this).data('id');
				iCounter++;
			}
		});

		oUserPermissions = oCheckedPermissions;
		callBack(oUserPermissions);
	}

	//==================================================//
	//=============END USER FUNCTIONALITIES=============//
	//==================================================//


	//==================================================//
	//================GLOBAL FUNCTIONS==================//
	//==================================================//

	function _closeGeneratingViewModal(oParams) {
		var sMessage = oParams.message != undefined ? oParams.message : 'Generating view, please wait...';

		uiGeneratingViewModal.find('.modal-message').html(sMessage);
		setTimeout(function() {
			uiGeneratingViewModal.removeClass('showed');
		},oParams.time);
	}

	function _getFirstLetter(sString) {
		var sFirstChar = sString.charAt(0);
		sFirstChar = sFirstChar.toLowerCase();

		return sFirstChar;
	}

	function _notifyAndRefresh(oParams) {
		var uiModal = oParams.modal,
		oFeedback = oParams.feedback,
		iTime = oParams.time,
		sLink = oParams.link;

		// uiModal.hide();
		uiModal.removeClass('showed');
		$('body').feedback(oFeedback);
		setTimeout(function() {
			window.location.href = sLink;
		},iTime);
	}

	//==================================================//
	//==============END GLOBAL FUNCTIONS================//
	//==================================================//

	return{
		bindEvents : bindEvents
	};

})();

$(document).ready(function() {

	CUsers.bindEvents();

});