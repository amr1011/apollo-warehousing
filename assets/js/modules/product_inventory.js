var CProductInventory = (function(){
	var failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg",
		productInventoryInfo = "",
		oLocations = "",
		oStorages = "",
		oStorageInfo;


	function getProductInventory()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "inventory/get_all_products_inventory",
			returnType : "json",
			beforeSend: function(){
				$('#generate-data-view').addClass('showed');
			},
			success : function(oResult) {

				if(Object.keys(oResult.data).length == 0){
					$('#generate-data-view').removeClass('showed');

					CProductInventory.renderElements("displayNoProductInventories");

				}else{
					// console.log(JSON.stringify(oResult.data));
					productInventoryInfo = oResult.data;
					CProductInventory.renderElements("displayProductInventories", oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getSelectedProductInventoryRecords() {
		var iProductId = cr8v_platform.localStorage.get_local_storage({name:"productInfoID"});
		oOptions = {
			type : "GET",
			data : {product_id : iProductId.value},
			returnType : 'json',
			url : BASEURL + 'inventory/get_product_inventory_records',
			beforeSend: function(){
				$('#generate-data-view').addClass('showed');
			},
			success : function(oResult) {
				// 	console.log(JSON.stringify(oResult.data));
				CProductInventory.renderElements("displaySelectedProductRecord", oResult.data);
				
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function test(oParams) {
		// var iProductId = oParams.product_id;
		// oOptions = {
		// 	type : "GET",
		// 	data : {product_id : iProductId},
		// 	returnType : 'json',
		// 	url : BASEURL + 'inventory/get_product_inventory_records',
		// 	beforeSend: function(){
		// 		$('#generate-data-view').addClass('showed');
		// 	},
		// 	success : function(oResult) {
		// 		// 	console.log(JSON.stringify(oResult.data));
		// 		console.log(JSON.stringify(oResult.data));
		// 		// CProductInventory.renderElements("displaySelectedProductRecord", oResult.data);
				
		// 	}
		// }

		// cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function date_test() {
		// var iProductId = oParams.product_id;
		// oOptions = {
		// 	type : "GET",
		// 	data : {test : true},
		// 	returnType : 'json',
		// 	url : BASEURL + 'inventory/calculate_days_remaining',
		// 	success : function(oResult) {
		// 		// console.log(JSON.stringify(oResult.data));
		// 		console.log(JSON.stringify(oResult.data));
		// 		// CProductInventory.renderElements("displaySelectedProductRecord", oResult.data);	
		// 	}
		// }

		// cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getAllStorages()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "inventory/get_all_storages",
			returnType : "json",
			success : function(oResult) {

				if(oResult.data === null){

				}else{
					// console.log(JSON.stringify(oResult.data));
					oStorages = oResult.data;
					
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getAllLocation()
	{
		
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "transfers/get_all_locations",
			returnType : "json",
			success : function(oResponse) {

				if(oResponse.data === null){

				}else{

					oLocations = oResponse.data;
					cr8v_platform.localStorage.set_local_storage({
						name : "getAllLocationsName",
						data : oResponse.data
					});
					
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	function getallStorageLocationInfo()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "inventory/get_all_storage_location_info",
			returnType : "json",
			beforeSend: function(){
				$('#generate-data-view').addClass('showed');
			},
			success : function(oResult) {

				if(oResult.data === null){

				}else{
					// console.log(JSON.stringify(oResult.data));
					oStorageInfo = oResult.data;
					CProductInventory.renderElements("displayStorageLocationInfo", oResult.data);
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _saveUpdateBatchStatus(oGatherData)
	{
		var oOptions = {
			type : "POST",
			data : { "data" : JSON.stringify(oGatherData) },
			url : BASEURL + "inventory/save_update_batch_status",
			success : function(oResult) {
				console.log(JSON.stringify(oResult.data));
				CProductInventory.renderElements("renderUpdatusStatusBatch", oResult.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _saveRemoveBatch(oGatherData)
	{
		var oOptions = {
			type : "POST",
			data : { "data" : JSON.stringify(oGatherData) },
			url : BASEURL + "inventory/save_remove_batch",
			success : function(oResult) {
				console.log(JSON.stringify(oResult.data));
				CProductInventory.renderElements("renderRemovedBatch", oResult.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}
	
	function _saveBatchAdjustmentQuatity(oNewAdjustQuantity)
	{
		var oOptions = {
			type : "POST",
			data : { "data" : JSON.stringify(oNewAdjustQuantity) },
			url : BASEURL + "inventory/save_batch_adjustment_quatity",
			success : function(oResult) {
					console.log(JSON.stringify(oResult.data));

				CProductInventory.renderElements("renderAdjustedBatch", oResult.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _searchUserPassword(sPasswordInput, ui, callBack)
	{


		var oOptions = {
			type : "POST",
			data : { "password" : sPasswordInput },
			url : BASEURL + "users/search_user_password",
			beforeSend : function(){
				ui.html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="font-size: 18px; color: #fff;"></i>');
			},
			success : function(oResult) {
				ui.html('Enter');
				if(typeof callBack == 'function'){
					callBack(oResult);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	function _saveReturningBatch(oNewReturnBatch)
	{
		console.log(JSON.stringify(oNewReturnBatch));
		var oOptions = {
			type : "POST",
			data : { "data" : JSON.stringify(oNewReturnBatch) },
			url : BASEURL + "inventory/save_returning_batch",
			success : function(oResult) {
				// console.log(JSON.stringify(oResult.data));
				CProductInventory.renderElements("renderReturnedBatch", oResult.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _saveSplitBatch(oNewSplitBatchRecord)
	{

		var oOptions = {
			type : "POST",
			data : { "data" : JSON.stringify(oNewSplitBatchRecord) },
			url : BASEURL + "inventory/save_split_batch",
			success : function(oResult) {
				// 	console.log(JSON.stringify(oResult.data));
				CProductInventory.renderElements("renderSplitBatch", oResult.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _saveRepackBatch(oBuildNewData)
	{
		var oOptions = {
			type : "POST",
			data : { "data" : JSON.stringify(oBuildNewData) },
			url : BASEURL + "inventory/save_repack_batch",
			success : function(oResult) {
					console.log(JSON.stringify(oResult.data));
				CProductInventory.renderElements("renderRepackBatch", oResult.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}


	function _createCollapseProduct(uiTemplate)
	{
		// var btnTriggerCollapse = $(".trigger-collapse");

		// btnTriggerCollapse.off("click.collapeIt").on("click.collapeIt", function(e){
		// 	e.preventDefault();

				// iProductId = uiParent.data("product_id");

				// accordion
				$(".panel-group .panel-heading").each(function(){
					var ps = $(this).next(".panel-collapse");
					var ph = ps.find(".panel-body").outerHeight();

					if(ps.hasClass("in")){
						$(this).find("h4").addClass("active")
					}

					$(this).find("a").off("click").on("click",function(e){
						e.preventDefault();
						ps.css({height:ph});

						if(ps.hasClass("in")){
							$(this).find("h4").removeClass("active");
							$(this).find(".fa").removeClass("fa-caret-down").addClass("fa-caret-right");
							ps.removeClass("in");
						}else{
							$(this).find("h4").addClass("active");
							$(this).find(".fa").removeClass("fa-caret-right").addClass("fa-caret-down");
							ps.addClass("in");
						}

						setTimeout(function(){
							ps.removeAttr("style")
						},500);
					});
				});

		//});

	}

	function buildNewLocationBatch(locationID)
	{
		 var iLocationId = locationID,
		    oCurrentLocation = null,
		    oLocationList = {},
		    iCounter = 0,
		    iCounterTwo = 0;


		    if(iLocationId != 0)
		    {
		    	for(var a in oLocations) {
			      if(oLocations[a].id == iLocationId) {
			        oCurrentLocation = oLocations[a];
			      }
			    }

			    oLocationList[iCounter] = oCurrentLocation;
			    ++iCounter;

			    var keyCounter = Object.keys(oLocations).length;

			    for(var b = 0; b < keyCounter;) {
			      if(oLocations[b].id == oCurrentLocation.parent_id) {
			        oLocationList[iCounter] = oLocations[b];
			        oCurrentLocation = oLocations[b];
			        ++iCounter;
			        b = -1;
			      }
			      ++b;
			    }


			    var createObjLocation = [];

			    for(var x in oLocationList)
			    {
			    	createObjLocation.push(oLocationList[x]);

			    }

			    var reverseIt = createObjLocation.reverse();

			    return reverseIt;
			   

			}
    
	}

	function _displayAllListProduct(oData)
	{
		var uiListProductViews = $("#list-product-views"),
			uiTemplate = $(".list-display-item");

			uiListProductViews.html("");

		for(var x in oData)
		{
			var item = oData[x],
				uiCloneTemplate = uiTemplate.clone();

				uiCloneTemplate.removeClass("list-display-item").show();
				uiCloneTemplate.data(item);
				uiCloneTemplate.find(".display-list-name").html(item.product_name);
				uiCloneTemplate.find(".display-list-sku").html(item.sku);
				uiCloneTemplate.find(".display-list-batches").html(item.total_batches);
				uiCloneTemplate.find(".display-list-total-quantity").html(item.total_quantity+" KG");

				uiListProductViews.append(uiCloneTemplate);

				_triggerDisplayProductInfo(uiCloneTemplate);

		}
	}

	function _displayGridListProduct(oData)
	{
		var uiGridProductViews = $("#grid-product-views"),
			uiTemplate = $(".grid-display-item");

			uiGridProductViews.html("");

		for(var x in oData)
		{
			var item = oData[x],
				uiCloneTemplate = uiTemplate.clone();

				uiCloneTemplate.removeClass("grid-display-item").show();
				uiCloneTemplate.data(item);
				uiCloneTemplate.find(".display-grid-image").attr("src", item.image);
				uiCloneTemplate.find(".display-grid-name").html(item.product_name);
				uiCloneTemplate.find(".display-grid-sku").html(item.sku);
				uiCloneTemplate.find(".display-grid-batches").html(item.total_batches);
				uiCloneTemplate.find(".display-grid-total-quantity").html(item.total_quantity);

				uiGridProductViews.append(uiCloneTemplate);

				_triggerDisplayProductInfo(uiCloneTemplate);

		}

		setTimeout(function(){
			$('#generate-data-view').removeClass('showed');
		},500);
	}

	function _triggerDisplayProductInfo(ui)
	{
		var uiParent = ui.closest(".record-item"),
			oItemData = uiParent.data(),
			uiViewProductDetailsBtn = uiParent.find(".view-product-details");

			uiViewProductDetailsBtn.off("click.viewList").on("click.viewList", function(){
				// console.log(JSON.stringify(oItemData));

				cr8v_platform.localStorage.set_local_storage({
					name : "productInfoID",
					data : {"value":oItemData.product_id}
				});

				setTimeout(function(){
					window.location.href = BASEURL+"products/stock_product_activity";
				}, 400);

			});

	}

	function _displaySelectedProductRecords(oData)
	{
		// console.log(JSON.stringify(oData));

		var uiDisplayProductName = $("#display-product-name"),
			uiDisplayProductNameSku = $("#display-product-name-sku"),
			uiDisplayProductTotalInventory = $("#display-product-total-inventory"),
			uiDisplayProductCategory = $("#display-product-category"),
			uiDisplayProductUnitPrice = $("#display-product-unit-price"),
			uiDisplayProductShelfLife = $("#display-product-shelf-life"),
			uiDisplayProductQuantityMinimum = $("#display-product-quantity-minimum"),
			uiDisplayProductDescription = $("#display-product-description"),
			uiDisplayProductQuantityMaximum = $("#display-product-quantity-maximum"),
			uiDisplayProductImage = $("#display-product-image");

			uiDisplayProductName.html(oData["product"]["name"]);
			uiDisplayProductNameSku.html("SKU # "+oData["product"]["sku"]+" - "+oData["product"]["name"]);
			uiDisplayProductTotalInventory.html(oData["product"]["total_quantity"]+" KG");
			uiDisplayProductCategory.html(oData["product"]["category_name"]);
			uiDisplayProductUnitPrice.html(oData["product"]["unit_price"]+" Php");
			uiDisplayProductShelfLife.html(oData["product"]["product_shelf_life"]+" Days");
			uiDisplayProductQuantityMinimum.html(oData["product"]["threshold_min"]+" MT");
			uiDisplayProductDescription.html(oData["product"]["description"]);
			uiDisplayProductQuantityMaximum.html(oData["product"]["threshold_maximum"]+" MT");
			uiDisplayProductImage.attr("src", oData["product"]["image"]);

			_displaySelectedProductBatchRecord(oData["product_batches"]);

			_displaySelectedProductActivityRecord(oData["product_activities"]);

			_displaySelectedProductConsigneeAssignRecord(oData["consignee_assignments"]);

			


		setTimeout(function(){
			$('#generate-data-view').removeClass('showed');
		},500);
	}


	function _displaySelectedProductBatchRecord(oData)
	{
		var uiDisplayProductStorages = $("#display-product-storages").find(".selected-storage");

		for(var x in oData)
		{
			var item = oData[x],
				oBatchLocation = buildNewLocationBatch(item.batch_location),
				sBatchLocation = _displayLocationItem(oBatchLocation);

				$.each(uiDisplayProductStorages, function(){
					var uiThis = $(this),
						uiParent = uiThis.closest(".selected-storage"),
						oItemdata = uiParent.data();

						// console.log(item.batch_storage);
						// console.log(JSON.stringify(oItemdata));
	 
						if(item.batch_storage == oItemdata.id)
						{

							var uiDisplayBatchesItem = uiParent.find(".display-batches-item"),
								uiTemplate = uiParent.find(".display-batches-here");

								

								var uiCloneTemplate = uiTemplate.clone();

								var oCollectData = {
									batch_id : item.batch_id,
									batch_name : item.batch_name,
									batch_quantity : item.batch_quantity,
									converted_qty_kg : item.converted_qty_kg,
									batch_location : item.batch_location,
									batch_storage : item.batch_storage,
									batch_unit_of_measure : item.batch_unit_of_measure,
									unit_of_measure_name : item.unit_of_measure_name,
									storage_assignment : item.batch_storage_assignment,
									repack_batch_qty : item.repack_batch_qty

								}

							

								uiCloneTemplate.removeClass("display-batches-here").show();
								uiCloneTemplate.data(oCollectData);
								uiCloneTemplate.find(".display-batch-name").html(item.batch_name);
								uiCloneTemplate.find(".display-batch-date").html(item.receiving_date_received);
								uiCloneTemplate.find(".display-batch-location").html(sBatchLocation);
								uiCloneTemplate.find(".display-batch-quantity").html(item.converted_qty_kg+" KG");
								uiCloneTemplate.find(".display-batch-shelf-life").html(item.days_left);
								uiCloneTemplate.find(".display-batch-vessel").html(item.vessel_name);

								if(item.storage_name != "stockroom")
								{
									uiCloneTemplate.find(".display-batch-bag-quantity").html();
								}


								if(item.batch_status == "0")
								{
									uiCloneTemplate.find(".display-batch-status").addClass("green-color");
									uiCloneTemplate.find(".display-batch-status").removeClass("yellow-color");
									uiCloneTemplate.find(".display-batch-status").removeClass("red-color");
									uiCloneTemplate.find(".display-batch-status").html("Unrestricted");
								}else if(item.batch_status == "1")
								{
									uiCloneTemplate.find(".display-batch-status").addClass("yellow-color");
									uiCloneTemplate.find(".display-batch-status").removeClass("green-color");
									uiCloneTemplate.find(".display-batch-status").removeClass("red-color");
									uiCloneTemplate.find(".display-batch-status").html("Quality Assurance");
								}else if(item.batch_status == "2")
								{
									uiCloneTemplate.find(".display-batch-status").addClass("red-color");
									uiCloneTemplate.find(".display-batch-status").removeClass("green-color");
									uiCloneTemplate.find(".display-batch-status").removeClass("yellow-color");
									uiCloneTemplate.find(".display-batch-status").html("Restricted");
								}

								uiParent.find(".display-batches-item").find(".display-batches-here").hide();

								uiDisplayBatchesItem.append(uiCloneTemplate);

								_makeHoverHeight(uiCloneTemplate);

								_triggerBatchSplit(uiCloneTemplate);

								_triggerBatchRepack(uiCloneTemplate);

								_triggerBatchAdjustQuantity(uiCloneTemplate);

								_triggerBatchUpdateStatus(uiCloneTemplate);

								_triggerBatchRemove(uiCloneTemplate);

								_triggerBatchReturn(uiCloneTemplate);

						}

						
				});

		}

		_collectAllTotalPerStorage();


	}

	function _collectAllTotalPerStorage()
	{
		var uiDisplayProductStorages = $("#display-product-storages").find(".selected-storage");

		$.each(uiDisplayProductStorages, function(){
				var uiThis = $(this),
					uiParent = uiThis.closest(".selected-storage"),
					uiDisplayBatchesItem = uiParent.find(".display-batches-item").find(".batches-displayed"),
					uiDisplayStorageCurrentQuantity = uiParent.find(".display-storage-current-quantity"),
					uiDisplayStorageCapacityQuantity = uiParent.find(".display-storage-capacity-quantity"),
					oItemData = uiParent.data(),
					iTotal = 0;


					$.each(uiDisplayBatchesItem, function(){
						var uiThis = $(this),
							uiSecondParent = uiThis.closest(".batches-displayed:not(.display-batches-here)"),
							oSecondItemData = uiSecondParent.data();

							if(typeof oSecondItemData == 'undefined'){
								
							}else{
								var iConvert = parseFloat(oSecondItemData["batch_quantity"]),
									iConvertedTotal = UnitConverter.convert(oSecondItemData["unit_of_measure_name"], "kg", iConvert);

									iTotal += parseFloat(iConvertedTotal);
							}

					});

					uiDisplayStorageCurrentQuantity.html(iTotal);

					var iStorageCapacity =  UnitConverter.convert(oItemData["measurement_name"], "kg", parseFloat(oItemData["capacity"]));
					uiDisplayStorageCapacityQuantity.html(iStorageCapacity);

		});
	}

	function _displaySelectedProductConsigneeAssignRecord(oData)
	{
		var uiDisplayConsigneeRecords = $("#display-consignee-records"),
			uiTemplate = $(".display-consignee-items");

			uiDisplayConsigneeRecords.html("");

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-consignee-items").show();
					uiCloneTemplate.find(".display-consignee-name").html(item.consignee_name);

					uiCloneTemplate.find(".display-consignee-vessel").html("");
					uiCloneTemplate.find(".display-consignee-quantity").html("");
						
						for(var i in item.vessel_info)
						{
							var vessels = item.vessel_info[i];
							uiCloneTemplate.find(".display-consignee-vessel").append("<p>"+vessels.vessel_name+"</p>");
						}

						for(var i in item.consignee_quantity_info)
						{
							var quantities = item.consignee_quantity_info[i];
							uiCloneTemplate.find(".display-consignee-quantity").append("<p>"+quantities.batch_quantity+" KG </p>");
						}					
					

					uiDisplayConsigneeRecords.append(uiCloneTemplate);
			}
	}

	function _displaySelectedProductActivityRecord(oData)
	{
		// console.log(JSON.stringify(oData));


		if(Object.keys(oData["product_receiving"]).length > 0)
		{
			_displaySelectedProductReceivingRecord(oData["product_receiving"]);
		}else{
			
			$("#display-main-receiving-record").hide();
		}
		

		if(Object.keys(oData["transfers"]).length > 0)
		{
			_displaySelectedProductTransferRecord(oData["transfers"]);
		}else{
			$("#display-main-transfer-record").hide();
		}

		if(Object.keys(oData["product_withdrawal"]).length > 0)
		{
			_displaySelectedProductWithdrawalRecord(oData["product_withdrawal"]);
		}else{
			$("#display-main-withdrawals-record").hide();
		}

		if(Object.keys(oData["batch_adjusment"]).length > 0)
		{
			_displaySelectedBatchAdjustmentRecord(oData["batch_adjusment"]);
		}else{
			$("#display-main-adjustment-div").hide();
		}

		if(Object.keys(oData["batch_retun"]).length > 0)
		{
			_displaySelectedBatchReturnRecord(oData["batch_retun"]);
		}else{
			$("#display-main-return-div").hide();
		}

		if(Object.keys(oData["batch_remove"]).length > 0)
		{
			_displaySelectedBatchRemoveRecord(oData["batch_remove"]);
		}else{
			$("#display-main-remove-div").hide();
		}

		if(Object.keys(oData["batch_update_status"]).length > 0)
		{
			_displaySelectedBatchUpdateStatusRecord(oData["batch_update_status"]);
		}else{
			$("#display-main-status-div").hide();
		}

		if(Object.keys(oData["batch_split"]).length > 0)
		{
			_displaySelectedBatchSplitRecord(oData["batch_split"]);
		}else{
			$("#display-main-split-div").hide();
		}

		if(Object.keys(oData["batch_repack"]).length > 0)
		{
			_displaySelectedBatchRepackRecord(oData["batch_repack"]);
		}else{
			$("#display-main-repack-div").hide();
		}

	}

	function _displaySelectedBatchRepackRecord(oData)
	{
		var uiDisplayMainRepackRecord = $("#display-main-repack-record"),
			uiTemplate = $(".display-main-repack-item");

			uiDisplayMainRepackRecord.html("");	

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-main-repack-item").show();
					
					uiCloneTemplate.find(".display-repack-batch-date-created").html(item.date_created);
					uiCloneTemplate.find(".display-repack-batch-user").html(item.user_name);
					uiCloneTemplate.find(".display-repack-batch-name").html(item.batch_name);
					uiCloneTemplate.find(".display-repack-batch-old-qty").html(item.old_quantity+" Bags");
					uiCloneTemplate.find(".display-repack-batch-new-qty").html(item.quantity+" Bags");
					uiCloneTemplate.find(".display-repack-batch-user-pic").attr("src", CGetProfileImage.getDefault(item.user_name));

					uiDisplayMainRepackRecord.append(uiCloneTemplate);

			}		
	}

	function _displaySelectedBatchSplitRecord(oData)
	{
		var uiDisplayMainSplitRecord = $("#display-main-split-record"),
			uiTemplate = $(".display-main-split-item");
			
			uiDisplayMainSplitRecord.html("");

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-main-split-item").show();
					uiCloneTemplate.find(".display-split-batch-date-created").html(item.date_created);
					uiCloneTemplate.find(".display-split-batch-user").html(item.user_name);
					uiCloneTemplate.find(".display-split-batch-name").html(item.batch_name);
					uiCloneTemplate.find(".display-split-batch-user-pic").attr("src", CGetProfileImage.getDefault(item.user_name));

					uiDisplayMainSplitRecord.append(uiCloneTemplate);
					
					_displaySplitBatchNewRecords(uiCloneTemplate, item.batch_split_info);
					
			}



	}

	function _displaySplitBatchNewRecords(ui, oData)
	{
		var uiDisplaySplitBatchNewBatches = ui.find(".display-split-batch-new-batches"),
			uiTemplate = ui.find(".display-split-batch-new-batches-item");

			uiDisplaySplitBatchNewBatches.html("");
					
			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.find(".display-split-batch-name").html(item.batch_name);
					uiCloneTemplate.find(".display-split-batch-quality").html(item.quantity+" "+item.unit_of_measure_name.toUpperCase());

					uiDisplaySplitBatchNewBatches.append(uiCloneTemplate);
			}
	}

	function _displaySelectedBatchUpdateStatusRecord(oData)
	{
		var uiDisplayMainStatusRecord = $("#display-main-status-record"),
			uiTemplate = $(".display-main-status-item");

			uiDisplayMainStatusRecord.html("");

			//  console.log(JSON.stringify(oData));

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-main-status-item").show();
					uiCloneTemplate.find(".display-status-batch-date-created").html(item.date_created);
					uiCloneTemplate.find(".display-status-batch-user").html(item.user_name);
					uiCloneTemplate.find(".display-status-batch-name").html(item.batch_name);
					uiCloneTemplate.find(".display-status-batch-status").html(item.status);
					uiCloneTemplate.find(".display-status-batch-reason").html(item.reason);
					uiCloneTemplate.find(".display-status-batch-user-pic").attr("src", CGetProfileImage.getDefault(item.user_name));

					uiDisplayMainStatusRecord.append(uiCloneTemplate);
			}
	}

	function _displaySelectedBatchRemoveRecord(oData)
	{
		var uiDisplayMainRemoveRecord = $("#display-main-remove-record"),
			uiTemplate = $(".display-main-remove-item");
			
		uiDisplayMainRemoveRecord.html("");

		for(var x in oData)
		{
			var item = oData[x],
				uiCloneTemplate = uiTemplate.clone();

				uiCloneTemplate.removeClass("display-main-remove-item").show();
				uiCloneTemplate.find(".display-remove-batch-date-created").html(item.date_created);
				uiCloneTemplate.find(".display-remove-batch-user-pic").attr("src", CGetProfileImage.getDefault(item.user_name));
				uiCloneTemplate.find(".display-remove-batch-user").html(item.user_name);
				uiCloneTemplate.find(".display-remove-batch-name").html(item.batch_name);
				uiCloneTemplate.find(".display-remove-batch-reason").html(item.reason);

				uiDisplayMainRemoveRecord.append(uiCloneTemplate);
		}
	}

	function _displaySelectedBatchReturnRecord(oData)
	{
		var uiDisplayMainReturnRecord = $("#display-main-return-record"),
			uiTemplate = $(".display-main-return-item");
			
			uiDisplayMainReturnRecord.html("");

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-main-return-item").show();
					uiCloneTemplate.find(".display-return-batch-date-created").html(item.date_created);
					uiCloneTemplate.find(".display-return-batch-user-pic").attr("src", CGetProfileImage.getDefault(item.user_name));
					uiCloneTemplate.find(".display-return-batch-user").html(item.user_name);
					uiCloneTemplate.find(".display-return-batch-quantity").html(item.returning_qty+" KG");
					uiCloneTemplate.find(".display-return-batch-reason").html(item.reason);
					uiCloneTemplate.find(".display-return-batch-name").html(item.batch_name);

					uiDisplayMainReturnRecord.append(uiCloneTemplate);
			}

	}

	function _displaySelectedBatchAdjustmentRecord(oData)
	{
		var uiDisplayMainAdjustmentRecord = $("#display-main-adjustment-record"),
			uiTemplate = $(".display-main-adjustment-item");

			uiDisplayMainAdjustmentRecord.html("");

			for(var x in oData)
			{
				var item = oData[x],
				uiCloneTemplate = uiTemplate.clone();

				uiCloneTemplate.removeClass("display-main-adjustment-item").show();
				uiCloneTemplate.find(".display-adjust-batch-date-created").html(item.date_created);
				uiCloneTemplate.find(".display-adjust-batch-user-pic").attr("src", CGetProfileImage.getDefault(item.user_name));
				uiCloneTemplate.find(".display-adjust-batch-user").html(item.user_name);
				uiCloneTemplate.find(".display-adjust-batch-name").html(item.batch_name);
				uiCloneTemplate.find(".display-adjust-batch-prev-qty").html(item.old_qty+" KG");
				uiCloneTemplate.find(".display-adjust-batch-new-qty").html(item.new_qty+" KG");
				uiCloneTemplate.find(".display-adjust-batch-reason").html(item.reason);

				uiDisplayMainAdjustmentRecord.append(uiCloneTemplate);
			}
	}

	function _displaySelectedProductWithdrawalRecord(oData)
	{
		var uiDisplayWithdrawalRecord = $(".display-withdrawal-record"),
			uiTemplate = $(".display-withdrawal-items");

			uiDisplayWithdrawalRecord.html("");

			for(var x in oData){
				var withdraw = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-withdrawal-items").show();
					uiCloneTemplate.find(".display-withdrawal-num").html(_convertNumber(withdraw.withdrawal_number));
					uiCloneTemplate.find(".display-withdrawal-atl").html(withdraw.atl_number);
					uiCloneTemplate.find(".display-withdrawal-cdr").html(withdraw.cdr_no);
					uiCloneTemplate.find(".display-withdrawal-origin").html(withdraw.vessel_origin);
					uiCloneTemplate.find(".display-withdrawal-consignee").html(withdraw.consignee_name);
					uiCloneTemplate.find(".display-withdrawal-mode-travel").html(withdraw.mode_travel);
					uiCloneTemplate.find(".display-withdrawal-date").html(withdraw.withdrawal_date_created);

					uiCloneTemplate.find(".display-withdrawal-user").html(withdraw.user_name);

					uiCloneTemplate.find(".display-withdrawal-user-pic").attr("src", CGetProfileImage.getDefault(withdraw.user_name));
					

					for(var i in withdraw.batch_info)
					{
						var batches = withdraw.batch_info[i];

						uiCloneTemplate.find(".display-withdrawal-batch-name").html("<p>"+batches.batch_name+"</p>");
					}

					
					uiDisplayWithdrawalRecord.append(uiCloneTemplate);

			}

	}

	function _displaySelectedProductReceivingRecord(oData)
	{
		var uiDisplayReceivingRecord = $(".display-receiving-record"),
			uiTemplate = $(".display-receiving-items");

			uiDisplayReceivingRecord.html("");

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					if(item.receiving_id !== null)
					{

						uiCloneTemplate.removeClass("display-receiving-items").show();
						uiCloneTemplate.find(".display-receiving-date-created").html(item.receiving_date_created);
						uiCloneTemplate.find(".display-receiving-num").html(_convertNumber(item.receiving_number));
						uiCloneTemplate.find(".display-receiving-po").html(item.receiving_purchase_order);
						uiCloneTemplate.find(".display-receiving-date-received").html(item.receiving_date_received);
						uiCloneTemplate.find(".display-receiving-origin").html(item.receiving_origin);

						uiCloneTemplate.find(".display-receiving-user").html(item.user_name);

						uiCloneTemplate.find(".display-receiving-user-pic").attr("src", CGetProfileImage.getDefault(item.user_name));


						for(var i in item.batch_info)
						{
							var batches = item.batch_info[i];
							uiCloneTemplate.find(".display-receiving-batch").append("<p>"+batches.batch_name+"</p>");
						}

						uiDisplayReceivingRecord.append(uiCloneTemplate);
					}
			}
			 
	}

	function _displaySelectedProductTransferRecord(oData)
	{
		var uiDisplayTransferRecord = $(".display-transfer-record"),
			uiTemplate = $(".display-transfer-item");

			uiDisplayTransferRecord.html("");

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					oOldLocation = buildNewLocationBatch(item.origin_container),
					oNewLocation = buildNewLocationBatch(item.destination_container),
					sOldLocation = _displayLocationItem(oOldLocation),
					sNewLocation = _displayLocationItem(oNewLocation);

					uiCloneTemplate.removeClass("display-transfer-item").show();
					uiCloneTemplate.find(".display-transfer-date-issued").html(item.log_issued_date);
					uiCloneTemplate.find(".display-transfer-num").html(item.to_number);
					uiCloneTemplate.find(".display-transfer-batch-name").html(item.batch_name);
					uiCloneTemplate.find(".display-transfer-origin").html(item.vessel_name);
					uiCloneTemplate.find(".display-transfer-batch-origin").html(sOldLocation);
					uiCloneTemplate.find(".display-transfer-batch-destination").html(sNewLocation);

					uiCloneTemplate.find(".display-transfer-user").html(item.user_name);

					uiCloneTemplate.find(".display-transfer-user-pic").attr("src", CGetProfileImage.getDefault(item.user_name));

					uiDisplayTransferRecord.append(uiCloneTemplate);
			}

	}

	function _convertNumber(Numbers)
	{
		var sTranferNumber = Numbers,
	        sNewTransferNumber = sTranferNumber.toString(),
	        sCurrentTransferNumber = null,
	        iToLimit = 9;

        while(sNewTransferNumber.length < iToLimit) {
          sNewTransferNumber = "0" + sNewTransferNumber.toString();
        }

         // console.log(sNewTransferNumber);
         return sNewTransferNumber;
		
	}



	function _triggerBatchReturn(ui)
	{
		var btnReturnBatchModal = ui.find('[modal-target="return-batch"]');


		btnReturnBatchModal.off("click.showModal").on("click.showModal", function(){
			var uiThis = $(this),
				uiParent = uiThis.closest(".batches-displayed"),
				oItemData = uiParent.data(),
				uiDisplayReturnBatchName = $("#display-return-batch-name"),
				uiDisplayReturnBatchQuantity = $("#display-return-batch-quantity");

				uiDisplayReturnBatchName.html(oItemData["batch_name"]);
				uiDisplayReturnBatchQuantity.html(oItemData["converted_qty_kg"]+" KG");

			$("#return-batch-modal").data(oItemData);

			$("body").css({overflow:'hidden'});
						
			$("#return-batch-modal").addClass("showed");

			$("#return-batch-modal .close-me").on("click",function(){
				$("body").css({'overflow-y':'initial'});
				$("#return-batch-modal").removeClass("showed");
			});

			_setNewReturnBatch(uiParent, oItemData);



		});

	}

	function _setNewReturnBatch(ui, oData)
	{
		var uiParent = ui.closest(".batches-displayed"),
			uiDisplayReturnBatchQuantityEntry = $("#display-return-batch-quantity-entry"),
			uiDisplayReturnBatchReason = $("#display-return-batch-reason"),
			btnReturnBatchModalSave = $("#return-batch-modal-save"),
			uiDisplayBatchQuantity = uiParent.find(".display-batch-quantity");

			uiDisplayReturnBatchQuantityEntry.val("");
			uiDisplayReturnBatchReason.val("");

			uiDisplayReturnBatchQuantityEntry.number(true, 2);
			btnReturnBatchModalSave.off("click.saveReturn").on("click.saveReturn", function(){
				var oError = [];

				(uiDisplayReturnBatchQuantityEntry.val().trim() == "" || uiDisplayReturnBatchQuantityEntry.val().trim() == 0.00) ? oError.push("Return Quantity is required") : "";
				(uiDisplayReturnBatchReason.val().trim() == "") ? oError.push("Please add reason for batch return") : "";

				if(oError.length > 0)
				{
					$("body").feedback({title : "Return Batch", message : oError.join("<br />"), type : "danger", icon : failIcon});
				}else{
					

					var uiDisplaySuccessReturnBatchName = $("#display-success-return-batch-name"),
						uiDisplaySuccessReturnBatchReturnedQuantity = $("#display-success-return-batch-returned-quantity"),
						uiEntryQty = parseFloat(uiDisplayReturnBatchQuantityEntry.val().trim()),
						iConvertQtyForOld = oData["converted_qty_kg"],
						iConvertedQty = parseFloat(oData["converted_qty_kg"].replace(",","")),
						iTotal = 0;

						if(uiEntryQty > iConvertedQty)
						{
							$("body").feedback({title : "Return Batch", message : "Returning Quantity must not greater than the original quantity", type : "danger", icon : failIcon});

						}else{
							

								$('[modal-id="return-batch-confirmation"]').addClass("showed");

								$('[modal-id="return-batch-confirmation"] .close-me').on("click",function(){
									$("body").css({'overflow-y':'initial'});
									$('[modal-id="return-batch-confirmation"]').removeClass("showed");
								});

								$("#return-batch-modal").removeClass("showed");

								uiDisplaySuccessReturnBatchName.html(oData["batch_name"]);
								uiDisplaySuccessReturnBatchReturnedQuantity.html(uiDisplayReturnBatchQuantityEntry.val().trim()+" KG");

								iTotal = iConvertedQty - uiEntryQty;

								uiDisplayBatchQuantity.html(Number(iTotal).toLocaleString('en')+" KG");

								uiParent.data("converted_qty_kg", Number(iTotal).toLocaleString('en'));

								var oNewReturnBatch = {
									batch_id : oData["batch_id"],
									batch_name : oData["batch_name"],
									return_quantity : uiDisplayReturnBatchQuantityEntry.val().trim(),
									total : iTotal,
									reason : uiDisplayReturnBatchReason.val().trim(),
									unit_of_measure_name : oData["unit_of_measure_name"],
									unit_of_measure_id : oData["batch_unit_of_measure"]
								}

									_saveReturningBatch(oNewReturnBatch);
						}


						


				}


			});

			




	}

	function _triggerBatchRemove(ui)
	{
		var btnRemoveBatchModal = ui.find('[modal-target="remove-batch"]');


		btnRemoveBatchModal.off("click.showModal").on("click.showModal", function(){
			var uiThis = $(this),
				uiParent = uiThis.closest(".batches-displayed")
				oItemData = uiParent.data(),
				btnRemoveBatchModalSave = $("#remove-batch-modal-save"),
				uiRemoveText = $("#remove-batch-textarea");

				uiRemoveText.val("");

			$("#remove-batch-modal").data(oItemData);

			$("body").css({overflow:'hidden'});
						
			$("#remove-batch-modal").addClass("showed");

			$("#remove-batch-modal .close-me").on("click",function(){
				$("body").css({'overflow-y':'initial'});
				$("#remove-batch-modal").removeClass("showed");
			});

			btnRemoveBatchModalSave.off("click");

			btnRemoveBatchModalSave.off("click.removeBatch").on("click.removeBatch", function(){
				var uiRemoveBatchTextarea = $("#remove-batch-textarea");

					if(uiRemoveBatchTextarea.val().trim() == "")
					{
						$("body").feedback({title : "Remove Batch", message : "Please add reason for removing batch", type : "danger", icon : failIcon});
					}else{
						// console.log(JSON.stringify(oItemData));

						// $("body").css({'overflow-y':'initial'});
						$("#remove-batch-modal").removeClass("showed");


						$("body").css({overflow:'hidden'});
						
						$('#trashModal').addClass("showed");

						$('#trashModal .close-me').on("click",function(){
							//$("body").css({'overflow-y':'initial'});
							$('#trashModal').removeClass("showed");
							$("#remove-batch-modal").addClass("showed");
						});

						var btnConfirmDelete = $("#confirmDelete");

						btnConfirmDelete.off("click");

						btnConfirmDelete.off("click.remove").on("click.remove", function(){
							var oGatherData = {
								batch_id : oItemData["batch_id"],
								batch_name : oItemData["batch_name"],
								reason : uiRemoveBatchTextarea.val().trim()
							}

							_saveRemoveBatch(oGatherData);

							uiParent.remove();
							
							
							
							$(".close-me").trigger("click");

							$("body").css({overflow:'hidden'});
						
							$('[modal-id="remove-batch-confirmation"]').addClass("showed");

							$("#remove-batch-modal").removeClass("showed");

							$('[modal-id="remove-batch-confirmation"] .close-me').on("click",function(){
								$("body").css({'overflow-y':'initial'});
								$('[modal-id="remove-batch-confirmation"]').removeClass("showed");
							});



							var uiRemoveBatchConfirmationCaption = $("#remove-batch-confirmation-caption");

								uiRemoveBatchConfirmationCaption.html(oItemData["batch_name"]);

						});

					}
			});



		});

	}

	function _triggerBatchUpdateStatus(ui)
	{
		var btnUpdateStatusModal = ui.find('[modal-target="update-status"]');


		btnUpdateStatusModal.off("click.showModal").on("click.showModal", function(){
			var uiThis = $(this),
				uiParent = uiThis.closest(".batches-displayed"),
				uiDisplayBatchStatus = uiParent.find(".display-batch-status"),
				btnUpdateStatusModalSave = $("#update-status-modal-save"),
				uiDisplayBatchNameNewStatus = $("#display-batch-name-new-status"),
				uiDisplayBatchNewStatus = $("#display-batch-new-status"),
				uibatchReasonClear = $("#batch-update-reason"),
				oItemData = uiParent.data();

				uibatchReasonClear.val("");

			$("#update-status-modal").data(oItemData);

			$("body").css({overflow:'hidden'});
						
			$("#update-status-modal").addClass("showed");

			$("#update-status-modal .close-me").on("click",function(){
				$("body").css({'overflow-y':'initial'});
				$("#update-status-modal").removeClass("showed");
			});

			btnUpdateStatusModalSave.off("click");

			btnUpdateStatusModalSave.off("click.save").on("click.save", function(e){
				e.preventDefault();



				var uiThis = $(this),
					uiSecondParent = uiThis.closest("#update-status-modal"),
					uiNewStatus = uiSecondParent.find(".select-batch-status"),
					uibatchReason = uiSecondParent.find("#batch-update-reason"),
					sNewStatus = "",
					oError = [];

					(uibatchReason.val().trim() == "") ? oError.push("Reason for status change is required") : "";

					if(oError.length > 0)
					{
						$("body").feedback({title : "Update Status Batch", message : oError.join("<br />"), type : "danger", icon : failIcon});
					}else{

						$(".close-me").trigger("click");

						$("body").feedback({title : "Update Status Batch", message : "Successfully Updated", type : "success"});

						var oGatherData = {
							batch_id : oItemData["batch_id"],
							batch_name : oItemData["batch_name"],
							status : uiNewStatus.val(),
							reason : uibatchReason.val().trim()
						}

						_saveUpdateBatchStatus(oGatherData);

						if(uiNewStatus.val() == "0")
						{
							uiDisplayBatchStatus.addClass("green-color");
							uiDisplayBatchStatus.removeClass("yellow-color");
							uiDisplayBatchStatus.removeClass("red-color");
							uiDisplayBatchStatus.html("Unrestricted");

							sNewStatus = "Unrestricted";

						}else if(uiNewStatus.val() == "1")
						{
							uiDisplayBatchStatus.addClass("yellow-color");
							uiDisplayBatchStatus.removeClass("green-color");
							uiDisplayBatchStatus.removeClass("red-color");
							uiDisplayBatchStatus.html("Quality Assurance");

							sNewStatus = "Quality Assurance";

						}else if(uiNewStatus.val() == "2")
						{
							uiDisplayBatchStatus.addClass("red-color");
							uiDisplayBatchStatus.removeClass("yellow-color");
							uiDisplayBatchStatus.removeClass("green-color");
							uiDisplayBatchStatus.html("Restricted");

							sNewStatus = "Restricted";
						}

						uiDisplayBatchNameNewStatus.html(oItemData["batch_name"]);
						uiDisplayBatchNewStatus.html(sNewStatus);

						$("body").css({overflow:'hidden'});
						
						$('[modal-id="update-status-confirmation"]').addClass("showed");

						$('[modal-id="update-status-confirmation"] .close-me').on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$('[modal-id="update-status-confirmation"]').removeClass("showed");
						});


					}

			});


		});

	}

	function _triggerBatchAdjustQuantity(ui)
	{
		var btnAdjustQuantityModal = ui.find('[modal-target="adjust-quantity"]');


		btnAdjustQuantityModal.off("click.showModal").on("click.showModal", function(){
			var uiThis = $(this),
				uiParent = uiThis.closest(".batches-displayed"),
				oItemData = uiParent.data(),
				uiDisplayAdjustBatchName = $("#display-adjust-batch-name"),
				uiDisplayAdjustBatchQuantity = $("#display-adjust-batch-quantity"),
				uiDisplayAdjustBatchState = $("#display-adjust-batch-state"),
				uiDisplayAdjustBatchReason = $("#display-adjust-batch-reason"),
				uiTextDecrease = $("#text-decrease"),
				uiTextIncrease = $("#text-increase");

				// console.log(JSON.stringify(oItemData));

				uiDisplayAdjustBatchName.html(oItemData["batch_name"]);
				uiDisplayAdjustBatchQuantity.html(oItemData["converted_qty_kg"]+" KG");

				uiDisplayAdjustBatchState.html("");
				uiDisplayAdjustBatchReason.val("");
				uiTextDecrease.val("");
				uiTextIncrease.val("");

				_adjustingNewBatchQuantity(uiParent, oItemData);
				
				

			$("#adjust-quantity-modal").data(oItemData);

			$("body").css({overflow:'hidden'});
						
			$("#adjust-quantity-modal").addClass("showed");

			$("#adjust-quantity-modal .close-me").on("click",function(){
				$("body").css({'overflow-y':'initial'});
				$("#adjust-quantity-modal").removeClass("showed");
			});

		});

	}

	function _adjustingNewBatchQuantity(ui, oItemData)
	{
		uiParent = ui.closest(".batches-displayed"),
		uiTextIncrease = $("#text-increase"),
		uiTextDecrease = $("#text-decrease"),
		uiDisplayAdjustBatchState = $("#display-adjust-batch-state"),
		uiDisplayAdjustBatchReason = $("#display-adjust-batch-reason"),
		btnAdjustQuantityModalSave = $("#adjust-quantity-modal-save"),
		iNewQty = 0,
		sNewAdjustStatus = "";
		
		

		uiTextIncrease.number(true, 2);
		uiTextDecrease.number(true, 2);

		uiTextIncrease.off("keyup.keyType").on("keyup.keyType", function(){
			var uiThis = $(this),
				iValue = uiThis.val().trim();

				uiDisplayAdjustBatchState.html('');
				if(iValue != "")
				{
					uiDisplayAdjustBatchState.html("+"+parseFloat(iValue)+" KG");
					iNewQty = parseFloat(iValue);
					sNewAdjustStatus = "add";
				}else{
					uiDisplayAdjustBatchState.html("0 KG");
					iNewQty = 0;
					sNewAdjustStatus = "";
				}
				uiDisplayAdjustBatchState.removeClass("red-color");
				uiDisplayAdjustBatchState.addClass("green-color");

		});

		uiTextDecrease.off("keyup.keyType").on("keyup.keyType", function(){
			var uiThis = $(this),
				iValue = uiThis.val().trim();

				uiDisplayAdjustBatchState.html('');
				if(iValue != "")
				{
					uiDisplayAdjustBatchState.html("-"+parseFloat(iValue)+" KG");
					iNewQty = parseFloat(iValue);
					sNewAdjustStatus = "subtract";
				}else{
					uiDisplayAdjustBatchState.html("0 KG");
					iNewQty = 0;
					sNewAdjustStatus = "";
				}
				uiDisplayAdjustBatchState.removeClass("green-color");
				uiDisplayAdjustBatchState.addClass("red-color");

			
		});


		btnAdjustQuantityModalSave.off("click.saveAdjustment").on("click.saveAdjustment", function(){
			var oError = [];

			
			(iNewQty == "") ? oError.push("Adjustment Quantity is required") : "";
			(uiDisplayAdjustBatchReason.val().trim() == "") ? oError.push("Plese add reason for batch adjustment") : "";

			if(oError.length > 0)
			{
				$("body").feedback({title : "Adjust Quantity", message : oError.join("<br />"), type : "danger", icon : failIcon});
			}else{
				var uiDisplayBatchQuantity = uiParent.find(".display-batch-quantity"),
					iAdjustQty = parseFloat(iNewQty, 2),
					iConvertQtyForOld = oItemData["converted_qty_kg"],
					iConvertedQty = parseFloat(oItemData["converted_qty_kg"].replace(",","")),
					iTotal = 0;


					$("body").css({overflow:'hidden'});
						
					$('[modal-id="adjust-quantity-password"]').addClass("showed");

					$('[modal-id="adjust-quantity-password"] .close-me').on("click",function(){
						$("body").css({'overflow-y':'initial'});
						$('[modal-id="adjust-quantity-password"]').removeClass("showed");
						$('#adjust-quantity-modal').addClass("showed");
					});

					$('#adjust-quantity-modal').removeClass("showed");


					var btnAdjustQuantitySearchPassword = $("#adjust-quantity-search-password"),
						uiSetSearchUserPassword = $("#set-search-user-password");
						
						uiSetSearchUserPassword.val("");


					btnAdjustQuantitySearchPassword.off("click.searchPass").on("click.searchPass", function(){
						var sPasswordInput = uiSetSearchUserPassword.val().trim();

						if(sPasswordInput == "")
						{
							$("body").feedback({title : "Adjust Quantity Password", message :"Password Required", type : "danger", icon : failIcon});
						}else{



							_searchUserPassword(sPasswordInput, $(this), function(oResult){


								if(oResult == "0")
								{
									$("body").feedback({title : "Adjust Quantity Password", message :"Password Incorrect", type : "danger", icon : failIcon});
								}else{

									if(sNewAdjustStatus == "add")
										{
											iTotal = iConvertedQty + iAdjustQty;
										}else if(sNewAdjustStatus == "subtract")
										{
											iTotal = iConvertedQty - iAdjustQty;
										}

										var oNewAdjustQuantity = {
											batch_id : oItemData["batch_id"],
											batch_name : oItemData["batch_name"],
											adjusted_qty : iAdjustQty,
											old_qty : iConvertedQty,
											total : iTotal,
											measurement_name : oItemData["unit_of_measure_name"],
											method : sNewAdjustStatus,
											reason : uiDisplayAdjustBatchReason.val().trim()
										}

										_saveBatchAdjustmentQuatity(oNewAdjustQuantity);


										uiDisplayBatchQuantity.html(Number(iTotal).toLocaleString('en')+" KG");

										uiParent.data("converted_qty_kg", Number(iTotal).toLocaleString('en'));

									
										$("body").css({overflow:'hidden'});
						
										$('[modal-id="adjust-quantity-confirmation"]').addClass("showed");

										$('[modal-id="adjust-quantity-confirmation"] .close-me').on("click",function(){
											$("body").css({'overflow-y':'initial'});
											$('[modal-id="adjust-quantity-confirmation"]').removeClass("showed");
										});

										$('[modal-id="adjust-quantity-password"]').removeClass("showed");

										var uiDisplayAdjustmentBatchName = $("#display-adjustment-batch-name"),
											uiDisplayNewAdjustedQty = $("#display-new-adjusted-qty"),
											uiDisplayOldAdjustedQty = $("#display-old-adjusted-qty");

											uiDisplayAdjustmentBatchName.html(oItemData["batch_name"]);
											uiDisplayNewAdjustedQty.html(oItemData["converted_qty_kg"]+" KG");
											uiDisplayOldAdjustedQty.html(iConvertQtyForOld+" KG");


									}

									

							});

							
						}

					});






			}


		});


	}

	function _triggerBatchSplit(ui)
	{
		var btnSplitModal = ui.find('[modal-target="split-batch"]');


		btnSplitModal.off("click.showModal").on("click.showModal", function(){
			var uiThis = $(this),
				uiParent = uiThis.closest(".batches-displayed")
				oItemData = uiParent.data(),
				uiDisplaySplitBatchName = $("#display-split-batch-name"),
				uiDisplaySplitBatchQuantity = $("#display-split-batch-quantity"),
				uiSetSplitQunatityInput = $(".set-split-qunatity-input"),
				uiSetSplitBatchNameInput = $(".set-split-batch-name-input");

				uiSetSplitQunatityInput.number(true, 2);

				var iResult = UnitConverter.convert("kg", oItemData["unit_of_measure_name"], oItemData["converted_qty_kg"]);

				uiDisplaySplitBatchQuantity.html(iResult+" "+oItemData["unit_of_measure_name"].toUpperCase());

				uiDisplaySplitBatchName.html(oItemData["batch_name"]);


				var uiUnitOfMeasure = $(".unit-of-measure"),
					uiParentDropdown = uiUnitOfMeasure.parent(),
					uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');

					uiSiblingDropdown.remove();
					uiUnitOfMeasure.removeClass("frm-custom-dropdown-origin");

					var unit = '<option value="'+oItemData["batch_unit_of_measure"]+'">'+oItemData["unit_of_measure_name"].toUpperCase()+'</option>';
					uiUnitOfMeasure.append(unit);

					uiUnitOfMeasure.transformDD();

					var uiDropdown = $(".select-dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt");

					uiDropdown.attr("readonly", true);







// 			console.log(JSON.stringify(oItemData));

			$("#split-batch-modal").data(oItemData);

			$("body").css({overflow:'hidden'});
						
			$("#split-batch-modal").addClass("showed");

			$("#split-batch-modal .close-me").on("click",function(){
				$("body").css({'overflow-y':'initial'});
				$("#split-batch-modal").removeClass("showed");

				$(".cancel-split").remove();
			
				uiSetSplitQunatityInput.val("");
				uiSetSplitBatchNameInput.val("");
			});

			_displaySplitAddBatch(uiParent, oItemData);
			_checkSplitBatchInfo(oItemData, uiParent);


		});

	}

	function _displaySplitAddBatch(ui, oItemData)
	{
		

		var btnSplitBatchAdd = $("#split-batch-add"),
			uiSplitAddBatchRecord = $("#split-add-batch-record"),
			iCount = 2;

		btnSplitBatchAdd.off("click.add").on("click.add", function(){
				
				var sHtml = '<div class="padding-all-5 bggray-white margin-bottom-10 split-add-batch-item cancel-split">'+
							'	<p class="font-14 font-400 black-color no-margin-ver display-inline-mid insert-number">'+iCount+'. </p>'+
							'	<input type="text" class="width-400px display-inline-mid set-split-batch-name-input" placeholder="Batch Name">'+
							'	<div class="padding-top-10 padding-bottom-10 padding-left-25">'+
							'		<p class="font-14 font-400 black-color no-margin-ver display-inline-mid">Quantity: </p>'+
							'		<input type="text" class="t-small display-inline-mid margin-right-7 set-split-qunatity-input">'+
							'		<div class="select medium select-dropdown">'+
							'			<select class="unit-of-measure">'+
							'				<option value="'+oItemData["batch_unit_of_measure"]+'">'+oItemData["unit_of_measure_name"].toUpperCase()+'</option>'+
							'			</select>'+
							'		</div>'+
							'	</div>'+
							'</div>';

				
				uiSplitAddBatchRecord.append(sHtml);

				iCount++;



				$("select").transformDD();

				var uiDropdown = $(".select-dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt"),
					uiSetSplitQunatityInput = $(".set-split-qunatity-input");

					uiDropdown.attr("readonly", true);
					uiSetSplitQunatityInput.number(true, 2);

						_checkSplitBatchInfo(oItemData, ui);
					
		});
	}

	function _checkSplitBatchInfo(oItemData, ui)
	{
		var uiDisplayBatchSplit = ui.find(".display-batch-quantity");
			uiParent = $("#split-batch-modal"),
			btnSplitBatchModalSave = $("#split-batch-modal-save"),
			uiSplitAddBatchRecord = $("#split-add-batch-record").find(".split-add-batch-item");
			

			btnSplitBatchModalSave.off("click.saveIt").on("click.saveIt", function(){
				var oError = [],
					oNewQty = [],
					oNewSplitBatch = [];

				$.each(uiSplitAddBatchRecord, function(){
					var uiThis = $(this),
						uiInsertNumber = uiThis.find(".insert-number"),
						uiSetSplitBatchNameInput = uiThis.find(".set-split-batch-name-input"),
						uiSetSplitQunatityInput = uiThis.find(".set-split-qunatity-input");

						if(uiSetSplitBatchNameInput.val().trim() == "" || uiSetSplitQunatityInput.val() == "0.00" || uiSetSplitQunatityInput.val() == "")
						{
							oError.push("Incomplete record in number "+uiInsertNumber.text().split('.').join(""));

						}
						oNewQty.push(parseFloat(uiSetSplitQunatityInput.val()));
						
						var oNewBatches = {
							batch_name : uiSetSplitBatchNameInput.val().trim(),
							batch_quantity : uiSetSplitQunatityInput.val()
						}

						oNewSplitBatch.push(oNewBatches);

				});


				if(oError.length > 0)
				{
					$("body").feedback({title : "Split Batch", message : oError.join("<br />"), type : "danger", icon : failIcon});
				}else{

					var iTotal = 0,
						iOldConvert = parseFloat(oItemData["converted_qty_kg"].replace(",","")),
						iOldTotal = UnitConverter.convert("kg", oItemData["unit_of_measure_name"], iOldConvert);

					$.each(oNewQty,function() {
						iTotal += parseFloat(this);
					});

					if(iTotal > iOldTotal)
					{
						$("body").feedback({title : "Split Batch", message : "Total Quantity must not greater than Batch Quantity", type : "danger", icon : failIcon});
					}
					else{
						var iLeftQty = iOldTotal - iTotal;

						var oNewSplitBatchRecord = {
							batch_old_id : oItemData["batch_id"],
							batch_old_name : oItemData["batch_name"],
							unit_of_measure : oItemData["batch_unit_of_measure"],
							unit_of_measure_name : oItemData["unit_of_measure_name"],
							batch_info : oNewSplitBatch
						}

						_saveSplitBatch(oNewSplitBatchRecord);

						_displaySplitModalConfirm(oNewSplitBatchRecord);

						uiDisplayBatchSplit.html(Number(iLeftQty).toLocaleString('en')+" KG");
						ui.data("converted_qty_kg", Number(iLeftQty).toLocaleString('en'));

						$("body").css({overflow:'hidden'});
						
						$('[modal-id="split-batch-confirmation"]').addClass("showed");

						$('[modal-id="split-batch-confirmation"] .close-me').on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$('[modal-id="split-batch-confirmation"]').removeClass("showed");
						});

						$("#split-batch-modal").removeClass("showed");

					}

				}
			
			});

			
	}

	function _displaySplitModalConfirm(oData)
	{
		var uiDisplaySplitSuccessBatchName = $("#display-split-success-batch-name"),
			uiDisplaySplitSuccessRecord = $("#display-split-success-record"),
			uiTemplate = $(".display-split-success-item");

			uiDisplaySplitSuccessBatchName.html(oData["batch_old_name"]);
			uiDisplaySplitSuccessRecord.html("");

			for(var x in oData["batch_info"])
			{
				var item = oData["batch_info"][x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.find(".display-split-success-batch-name-new").html(item.batch_name);
					uiCloneTemplate.find(".display-split-success-batch-quantity").html(item.batch_quantity +oData["unit_of_measure_name"].toUpperCase());

					uiDisplaySplitSuccessRecord.append(uiCloneTemplate);
			}


	}

	function _triggerBatchRepack(ui)
	{
		var btnRepackModal = ui.find('[modal-target="repack-batch"]');

		btnRepackModal.off("click.showModal").on("click.showModal", function(){
			var uiThis = $(this),
				uiParent = uiThis.closest(".batches-displayed"),
				oItemData = uiParent.data(),
				iCurrentQty = parseFloat(oItemData["converted_qty_kg"].replace(",","")),
				iConvertToBag = UnitConverter.convert("kg", "bag", iCurrentQty),
				uiDisplayRepackBatchName = $("#display-repack-batch-name"),
				uiDisplayRepackBatchQuantity = $("#display-repack-batch-quantity"),
				check_repack_old_qty = "";

				uiDisplayRepackBatchName.html(oItemData["batch_name"]);
				
							
				if(oItemData["repack_batch_qty"] == "")
				{
					uiDisplayRepackBatchQuantity.html(iConvertToBag+" Bags");
				}else{
					uiDisplayRepackBatchQuantity.html(oItemData["repack_batch_qty"]+" Bags");
					
				}

				

			$("#repack-batch-modal").data(oItemData);

			$("body").css({overflow:'hidden'});
						
			$("#repack-batch-modal").addClass("showed");

			$("#repack-batch-modal .close-me").on("click",function(){
				$("body").css({'overflow-y':'initial'});
				$("#repack-batch-modal").removeClass("showed");
			});

			_checkRepackBatchInfo(uiParent, oItemData);

		});

	}

	function _checkRepackBatchInfo(uiParent, oItemData)
	{
		var uiSetRepackBatchQuantity = $("#set-repack-batch-quantity"),
			btnRepackBatchModalSave = $("#repack-batch-modal-save"),
			iCurrentQty = parseFloat(oItemData["converted_qty_kg"].replace(",","")),
			iConvertToBag = UnitConverter.convert("kg", "bag", iCurrentQty);
			
			uiSetRepackBatchQuantity.val("");
			uiSetRepackBatchQuantity.number(true, 2);

			var uiDropdown = $(".select-dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt");
			uiDropdown.attr("readonly", true);

			btnRepackBatchModalSave.off("click.check").on("click.check", function(){
				
				if(uiSetRepackBatchQuantity.val() == "" || uiSetRepackBatchQuantity.val() == 0.00)
				{
					$("body").feedback({title : "Repack Batch", message : "Quantity must not empty", type : "danger", icon : failIcon});
				}else{
					var iNewBagQty = parseFloat(uiSetRepackBatchQuantity.val());
					// console.log(JSON.stringify(oItemData));
					// console.log(iConvertToBag);
					// console.log(iNewBagQty);

					// if(iNewBagQty > iConvertToBag)
					// 	{
					// 	$("body").feedback({title : "Repack Batch", message : "Inputed quantity must not greater than original quantity", type : "danger", icon : failIcon});
					
						var repack_old_qty = "";
							
						if(oItemData["repack_batch_qty"] == "")
						{
							repack_old_qty = iConvertToBag;
						}else{
							repack_old_qty = oItemData["repack_batch_qty"];
						}

						var oBuildNewData = {
							batch_id : oItemData["batch_id"],
							batch_name : oItemData["batch_name"],
							quantity : iNewBagQty,
							repack_batch_qty : repack_old_qty
						}

						_saveRepackBatch(oBuildNewData);

						_displayConfirmedRepackBatch(oBuildNewData);

						uiParent.data("repack_batch_qty", iNewBagQty);

						$("body").css({overflow:'hidden'});
						
						$('[modal-id="repack-batch-confirmation"]').addClass("showed");

						$('[modal-id="repack-batch-confirmation"] .close-me').on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$('[modal-id="repack-batch-confirmation"]').removeClass("showed");
						});

						$("#repack-batch-modal").removeClass("showed");
					}

			});
			
	}

	function _displayConfirmedRepackBatch(oData)
	{
		var uiDisplaySuccessRepackBatchName = $("#display-success-repack-batch-name"),
			uiDisplaySuccessRepackBatchNewQty = $("#display-success-repack-batch-new-qty"),
			uiDisplaySuccessRepackBatchOldQty = $("#display-success-repack-batch-old-qty");

			uiDisplaySuccessRepackBatchName.html(oData.batch_name);
			uiDisplaySuccessRepackBatchNewQty.html(oData.quantity);
			uiDisplaySuccessRepackBatchOldQty.html(oData.repack_batch_qty);

			// console.log(JSON.stringify(oData));

			
	}

	function _makeHoverHeight(ui)
	{
		var uiParent = ui.closest(".batches-displayed");

		uiParent.off("mouseenter.enter").on("mouseenter.enter", function(){
			var uiHover =  $(this).find(".hover-tbl");

				uiHover.css({"height":"100%"});
		});
	}

	function _displayLocationItem(oData)
 	{

 		// console.log(JSON.stringify(oData));

 		var sMadeLocation = "",
 			iCount = 1,
 			iGetCountLocation = Object.keys(oData).length;

 			for(var x in oData)
 			{
 				if(iCount == iGetCountLocation){
 					sMadeLocation += '<p class="font-14 font-400 f-none display-inline-mid width-initial"> '+oData[x]["name"]+'</p>';
 				}else{
 					sMadeLocation += '<p class="font-14 font-400 f-none display-inline-mid width-initial"> '+oData[x]["name"]+'</p>'+
 									'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>';
 				}
 				iCount++;
 				
 			}

 			return sMadeLocation;

 	}

	function _displayStorages(oData)
	{
		var uiDisplayProductStorages = $("#display-product-storages");
			// uiTemplate = $(".display-selected-storage");

			uiDisplayProductStorages.html("");

		for(var x in oStorages)
		{		
			var iCount=0

			for(var i in oData)
			{
				if(oStorages[x]["id"] == oData[i]["batch_storage"])
				{
					iCount++;
				}
			}

			if(iCount != 0)
			{
				var store = oStorages[x];
				// 	uiCloneTemplate = uiTemplate.clone();

				// 	uiCloneTemplate.removeClass("display-selected-storage").show();
				// 	uiCloneTemplate.data(store);
				// 	uiCloneTemplate.find(".display-storage-name").html(store.name);

				// 	uiDisplayProductStorages.append(uiCloneTemplate);

				// 	_createCollapseProduct(uiCloneTemplate)

				var sHtml = _chooseTableForStorage(oStorages[x]["storage_type_name"]),
					uiTemplate = $(sHtml).clone();
// 					uiMain = uiTemplate.closest(".display-selected-storage"),
// 					uiParent = uiMain.parent();

					uiTemplate.data(store);
					uiTemplate.find(".display-storage-name").html(store.name);

					uiDisplayProductStorages.append(uiTemplate);
					_createCollapseProduct(uiTemplate);

					// console.log(uiTemplate);


			}


		}

	}

	function _chooseTableForStorage(chooseUI)
	{
		var sStockRoom ='<div class="panel-group margin-top-20 display-selected-storage selected-storage">'+
						'	<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">'+
						'		<div class="panel-heading panel-heading">'+
						'			<a class="collapsed active black-color default-cursor f-left font-20 font-bold trigger-collapse" href="javascript:void(0)">'+	
						'				<i class="fa fa-caret-down margin-right-5"></i>'+
						'				<span class="display-storage-name">Stock Room 1</span>'+
						'			</a>'+
						'			<p class="f-right font-15 font-500 "><span class="font-green vertical-baseline display-storage-current-quantity">12600</span>/<span class="display-storage-capacity-quantity">50000</span> KG in Storage</p>'+
						'			<div class="clear"></div>'+
						'		</div>'+
						'		<div class="panel-collapse collapse in" >'+
						'			<div class="panel-body bggray-white font-0">'+
						'				<table class="tbl-4c3h margin-top-20 text-left">'+
						'					<thead>'+
						'						<tr>'+
						'							<th class="black-color font-14 font-500 no-padding-left text-center  width-14percent">Batch Name.</th>'+
						'							<th class="black-color font-14 font-500 no-padding-left text-center width-14percent ">Batch Date</th>'+
						'							<th class="black-color font-14 font-500 no-padding-left text-center width-30percent">Location</th>'+
						'							<th class="black-color font-14 font-500 no-padding-left text-center width-14percent">Quantity</th>'+
						'							<th class="black-color font-14 font-500 no-padding-left text-center width-14percent">Remaining Shelf Life</th>'+
						'							<th class="black-color font-14 font-500 no-padding-left text-center width-14percent">Status</th>'+												
						'						</tr>'+
						'					</thead>'+
						'				</table>'+
						'				<span class="display-batches-item">'+
						'					<div class="tbl-like text-left display-batches-here batches-displayed">'+
						'						<div class="first-tbl mngt-tbl-content height-auto min-height-50px font-0">'+
						'							<div class="display-inline-mid width-14percent text-center">'+
						'								<p class="font-400 f-none font-14 width-100percent display-batch-name"></p>'+
						'							</div>'+
						'							<div class="display-inline-mid width-14percent text-center">'+
						'								<p class="font-400 f-none font-14 width-100percent display-batch-date"></p>'+
						'							</div>'+
						'							<div class="display-inline-mid width-30percent text-left display-batch-location">'+	
						'							</div>'+
						'							<div class="display-inline-mid width-14percent text-center">'+
						'								<p class="font-400 f-none font-14 width-100percent display-batch-quantity"></p>'+
						'							</div>'+
						'							<div class="display-inline-mid width-14percent text-center">'+
						'								<p class="font-400 f-none font-14 width-100percent display-batch-shelf-life"></p>'+
						'							</div>'+
						'							<div class="display-inline-mid width-14percent text-center">'+
						'								<p class="font-400 f-none font-14 width-100percent green-color display-batch-status"></p>'+
						'							</div>'+
						'						</div>'+
						'						<div class="hover-tbl text-center" >'+
						'							<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>'+
						'							<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>'+
						'							<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>'+
						'							<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>'+
						'							<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>'+
						'							<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>'+
						'						</div>'+
						'					</div>'+
						'				</span>'+
						'			</div>'+
						'		</div>'+
						'	</div>'+
						'</div>';

		var sWarehouseSilo ='<div class="panel-group margin-top-20 display-selected-storage selected-storage">'+
							'	<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">'+
							'		<div class="panel-heading panel-heading">'+
							'			<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="javascript:void(0)">'+
							'				<i class="fa fa-caret-down margin-right-5"></i>'+
							'				<span class="display-storage-name">Stock Room 1</span>'+
							'			</a>'+
							'			<p class="f-right font-15 font-500 "><span class="font-green vertical-baseline display-storage-current-quantity">12600</span>/<span class="display-storage-capacity-quantity">50000</span> KG in Storage</p>'+
							'			<div class="clear"></div>'+
							'		</div>'+
							'		<div class="panel-collapse collapse in">'+
							'			<div class="panel-body bggray-white font-0">'+
							'				<table class="tbl-4c3h margin-top-20 text-left height-auto min-height-50px">'+
							'					<thead>'+
							'						<tr>'+
							'							<th class="black-color font-14 font-500 no-padding-left text-center no-padding-top padding-bottom-5   width-10percent" rowspan="2">Batch Name</th>'+
							'							<th class="black-color font-14 font-500 no-padding-left text-center no-padding-top padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>'+
							'							<th class="black-color font-14 font-500 no-padding-left text-center no-padding-top padding-bottom-5  width-10percent" rowspan="2">Vessel</th>'+
							'							<th class="black-color font-14 font-500 no-padding-left text-center no-padding-top padding-bottom-5  width-20percent" rowspan="2">Location</th>'+
							'							<th class="black-color font-14 font-500 no-padding-left text-center padding-top-20 padding-bottom-5  width-20percent" colspan="3">Quantity</th>'+
							'							<th class="black-color font-14 font-500 no-padding-left text-center no-padding-top padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>'+
							'							<th class="black-color font-14 font-500 no-padding-left text-center no-padding-top padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>'+
							'						</tr>'+
							'						<tr>'+
							'							<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>'+
							'							<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>'+
							'							<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>'+
							'						</tr>'+
							'					</thead>'+
							'				</table>'+
							'				<span class="display-batches-item">'+
							'					<div class="tbl-like text-left display-batches-here batches-displayed">'+
							'						<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">'+
							'							<div class="display-inline-mid width-10percent text-center">'+
							'								<p class="font-400 f-none font-14 width-100percent display-batch-name">12345678</p>'+
							'							</div>'+
							'							<div class="display-inline-mid width-10percent text-center">'+
							'								<p class="font-400 f-none font-14 width-100percent display-batch-date">01-Mar-2016</p>'+
							'							</div>'+
							'							<div class="display-inline-mid width-10percent text-center">'+
							'								<p class="font-400 f-none font-14 width-100percent display-batch-vessel">MV CHINO</p>'+
							'							</div>'+
							'							<div class="display-inline-mid width-20percent text-left  padding-top-5 display-batch-location">'+
							'								<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>'+
							'								<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>'+
							'								<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>'+
							'								<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>'+
							'								<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>'+
							'								<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>'+
							'								<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>'+
							'								<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>'+
							'								<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>'+				
							'							</div>'+
							'							<div class="display-inline-mid width-10percent text-center">'+
							'								<p class="font-400 f-none font-14 width-100percent display-batch-bulk-quantity">50 KG</p>'+
							'							</div>'+
							'							<div class="display-inline-mid width-10percent text-center">'+
							'								<p class="font-400 f-none font-14 width-100percent display-batch-bag-quantity">700 KG<br>(14pcs)</p>'+
							'							</div>'+
							'							<div class="display-inline-mid width-10percent text-center ">'+
							'								<p class="font-400 f-none font-14 width-100percent display-batch-quantity">700 KG</p>'+
							'							</div>'+
							'							<div class="display-inline-mid width-10percent text-center">'+
							'								<p class="font-400 f-none font-14 width-100percent display-batch-shelf-life">300 days</p>'+
							'							</div>'+
							'							<div class="display-inline-mid width-10percent text-center">'+
							'								<p class="font-400 f-none font-14 width-100percent green-color display-batch-status">Unrestricted</p>'+
							'							</div>'+
							'						</div>'+
							'						<div class="hover-tbl text-center">'+
							'							<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>'+
							'							<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>'+
							'							<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust quantity</button>'+
							'							<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>'+
							'							<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>'+
							'							<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>'+
							'						</div>'+
							'					</div>'+
							'				</span>'+
							'			</div>'+
							'		</div>'+
							'	</div>'+
							'</div>';

			if(chooseUI == 'stockroom')
			{
				return sStockRoom;
			}else{
				return sWarehouseSilo;
			}
	}

	function _displayNoProduct()
	{
		var uiListProductViews = $("#list-product-views").find(".selected-list-item"),
			uiGridProductViews = $("#grid-product-views").find(".selected-grid-item");

		uiListProductViews.hide();
		uiGridProductViews.hide();


	}

	function _triggerProductsOrLocation()
	{
		var btnSelectProdOrlocation = $(".wh-select").find(".stocks").find(".frm-custom-dropdown").find(".frm-custom-dropdown-option").find(".option");

		btnSelectProdOrlocation.off("click.select").on("click.select", function(){
			var uiThis = $(this),
				sValue = uiThis.attr("data-value");

				if(sValue == "prod")
				{
					window.location.href = BASEURL+"products/stock_inventory";
				}else if(sValue == "sto"){
					window.location.href = BASEURL+"products/stock_storage_location";
				}
		});
	}

	function _displayStoragesInfo(oData)
	{
		 // console.log(JSON.stringify(oData));

		var uiDisplayStorageRecord = $("#display-storage-record"),
			uiTemplate = $(".display-storage-item");

			uiDisplayStorageRecord.html("");

		for(var x in oData)
		{
			var item = oData[x],
				uiCloneTemplate = uiTemplate.clone();

				var setStorageData = {
					storage_id : item.storage_id,
					storage_name : item.storage_name,
					storage_capacity : item.storage_capacity,
					storage_total_quantity : item.storage_total_quantity

				}

				uiCloneTemplate.removeClass("display-storage-item").show();
				uiCloneTemplate.data(setStorageData);
				uiCloneTemplate.find(".display-storage-name").html(item.storage_name);
				uiCloneTemplate.find(".display-storage-quantity").html(item.storage_total_quantity);
				uiCloneTemplate.find(".display-storage-capacity").html(item.storage_capacity);
				
				uiDisplayStorageRecord.append(uiCloneTemplate);

				_displayStorageProductInfo(item.product_info, uiCloneTemplate);

				_createCollapseProduct(uiTemplate);

		}

		setTimeout(function(){
			$('#generate-data-view').removeClass('showed');
		},500);
	}

	function _displayStorageProductInfo(oData, ui)
	{
		var uiDisplayProductRecord = ui.find(".display-product-record"),
			uiTemplate = ui.find(".display-product-item");

			uiDisplayProductRecord.html("");

		for(var x in oData)
		{
			var item = oData[x],
				uiCloneTemplate = uiTemplate.clone();

				var setProductData = {
					product_id : item.product_id,
					product_name : item.product_name,
					sku : item.sku,
					total_prod_quantity : item.total_prod_quantity

				}

				uiCloneTemplate.removeClass("display-product-item").show();
				uiCloneTemplate.data(setProductData);
				uiCloneTemplate.find(".display-product-sku-name").html("SKU #"+item.sku+" - "+item.product_name);
				uiCloneTemplate.find(".display-product-total-quantity").html(item.total_prod_quantity);
				
				uiDisplayProductRecord.append(uiCloneTemplate);

				_displayStorageProductBatchInfo(item.batch_info, uiCloneTemplate);
		}
	}

	function _displayStorageProductBatchInfo(oData, ui)
	{
		var uiDisplayBatchRecord = ui.find(".display-batch-record"),
			uiTemplate = ui.find(".display-batch-item");

			uiDisplayBatchRecord.html("");		
	
			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					oBatchLocation = buildNewLocationBatch(item.location_id),
					sBatchLocation = _displayLocationItem(oBatchLocation);

					uiCloneTemplate.removeClass("display-batch-item").show();
					uiCloneTemplate.data(item);
					uiCloneTemplate.find(".display-batch-name").html(item.batch_name);
					uiCloneTemplate.find(".display-batch-date").html(item.date_received);
					uiCloneTemplate.find(".display-batch-quantity").html(item.converted_qty_kg+" KG");
					uiCloneTemplate.find(".display-batch-location").html(sBatchLocation);
					uiCloneTemplate.find(".display-batch-life").html(item.days_left);

					if(item.status == "0")
					{
						uiCloneTemplate.find(".display-batch-status").addClass("green-color");
						uiCloneTemplate.find(".display-batch-status").removeClass("yellow-color");
						uiCloneTemplate.find(".display-batch-status").removeClass("red-color");

						uiCloneTemplate.find(".display-batch-status").html("Unrestricted");
						
					}else if(item.status == "1")
					{
						uiCloneTemplate.find(".display-batch-status").addClass("yellow-color");
						uiCloneTemplate.find(".display-batch-status").removeClass("green-color");
						uiCloneTemplate.find(".display-batch-status").removeClass("red-color");

						uiCloneTemplate.find(".display-batch-status").html("Quality Assurance");

					}else if(item.status == "2")
					{
						uiCloneTemplate.find(".display-batch-status").addClass("red-color");
						uiCloneTemplate.find(".display-batch-status").removeClass("green-color");
						uiCloneTemplate.find(".display-batch-status").removeClass("yellow-color");

						uiCloneTemplate.find(".display-batch-status").html("Restricted");
					}

				uiDisplayBatchRecord.append(uiCloneTemplate);

				_makeHoverHeight(uiCloneTemplate);


				_triggerBatchSplit(uiCloneTemplate);

				_triggerBatchRepack(uiCloneTemplate);

				_triggerBatchAdjustQuantity(uiCloneTemplate);

				_triggerBatchUpdateStatus(uiCloneTemplate);

				_triggerBatchRemove(uiCloneTemplate);

				_triggerBatchReturn(uiCloneTemplate);

					
			}
	}

	function _displayAdjustedBatch(oData)
	{
		
		var uiDisplayMainAdjustmentRecord = $("#display-main-adjustment-record");


		if($('#display-main-adjustment-div').css('display') == 'none'){
			// console.log("it is hidden");
			$("#display-main-adjustment-div").show();
			uiDisplayMainAdjustmentRecord.html("");
		}

			var sHtml = '<div class="padding-all-10 dashboard-icon ">'+
						'	<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-adjust-batch-date-created">'+oData["date_created"]+'</p>'+
						'	<div class="margin-left-50 side-border">'+	
						'		<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">'+
						'			<img src="'+CGetProfileImage.getDefault(oData["user_name"])+'" alt="user profile-pic" class="width-50px display-inline-mid display-adjust-batch-user-pic">'+
						'			<p class="display-inline-mid margin-left-10 "><span class="display-adjust-batch-user">'+oData["user_name"]+'</span> has adjusted a batch</p>'+
						'			<table class="tbl-4c3h margin-top-20">'+
						'				<thead>'+
						'					<tr>'+
						'						<th class=" font-14 font-bold black-color no-padding-left">Batch Name</th>'+
						'						<th class=" font-14 font-bold black-color no-padding-left">Previous Qty</th>'+
						'						<th class=" font-14 font-bold black-color no-padding-left">New QTY</th>'+
						'						<th class=" font-14 font-bold black-color no-padding-left">Reason</th>'+
						'					</tr>'+
						'				</thead>'+
						'				<tbody>'+
						'					<tr>'+
						'						<td class="font-14 font-400 black-color text-center no-padding-left display-adjust-batch-name">'+oData["batch_name"]+'</td>'+
						'						<td class="font-14 font-400 black-color text-center no-padding-left display-adjust-batch-prev-qty">'+oData["old_qty"]+' KG</td>'+
						'						<td class="font-14 font-400 black-color text-center no-padding-left display-adjust-batch-new-qty">'+oData["new_qty"]+' KG</td>'+
						'						<td class="font-14 font-400 black-color text-center no-padding-left display-adjust-batch-reason">'+oData["reason"]+'</td>'+
						'					</tr>'+
						'				</tbody>'+
						'			</table>'+
						'		</div>'+
						'	</div>'+
						'	<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>'+
						'</div>';

			
			uiDisplayMainAdjustmentRecord.prepend(sHtml);

	}

	function _displayReturnedBatch(oData)
	{
		var uiDisplayMainReturnDiv = $("#display-main-return-record");

		if($('#display-main-return-div').css('display') == 'none'){
			// console.log("it is hidden");
			$("#display-main-return-div").show();
			uiDisplayMainReturnDiv.html("");
		}

		var sHtml = '<div class="padding-all-10 dashboard-icon display-main-return-item">'+
					'	<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-return-batch-date-created">'+oData["date_created"]+'</p>'+
					'	<div class="margin-left-50 side-border">'+	
					'		<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">'+
					'			<img src="'+CGetProfileImage.getDefault(oData["user_name"])+'" alt="user profile-pic" class="width-50px display-inline-mid display-return-batch-user-pic">'+
					'			<p class="display-inline-mid margin-left-10"><span class="display-return-batch-user">'+oData["user_name"]+'</span> has returned a part of Batch Name <span class="display-return-batch-name">'+oData["batch_name"]+'</span></p>'+
					'			<table class="tbl-4c3h margin-top-20">'+
					'				<thead>'+
					'					<tr>'+
					'						<th class=" font-14 font-bold black-color no-padding-left">Quantity to Return</th>'+
					'						<th class=" font-14 font-bold black-color no-padding-left">Reason</th>'+
					'					</tr>'+
					'				</thead>'+
					'				<tbody>'+
					'					<tr>'+
					'						<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-return-batch-quantity">'+oData["returning_qty"]+' KG</td>'+
					'						<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-return-batch-reason">'+oData["reason"]+'</td>'+
					'					</tr>'+
					'				</tbody>'+
					'			</table>'+
					'		</div>'+
					'	</div>'+
					'	<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>'+
					'</div>';

					uiDisplayMainReturnDiv.prepend(sHtml);
	}

	function _displayRemovedBatch(oData)
	{
		var uiDisplayMainRemoveDiv = $("#display-main-remove-record");

		if($('#display-main-remove-div').css('display') == 'none'){
			// console.log("it is hidden");
			$("#display-main-remove-div").show();
			uiDisplayMainRemoveDiv.html("");
		}

		var sHtml = '<div class="padding-all-10 dashboard-icon display-main-remove-item">'+
					'	<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-remove-batch-date-created">'+oData["date_created"]+'</p>'+
					'	<div class="margin-left-50 side-border">'+	
					'		<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">'+
					'			<img src="'+CGetProfileImage.getDefault(oData["user_name"])+'" alt="user profile-pic" class="width-50px display-inline-mid display-remove-batch-user-pic">'+
					'			<p class="display-inline-mid margin-left-10"><span class="display-remove-batch-user">'+oData["user_name"]+'</span> has removed a Batch</p>'+
					'			<table class="tbl-4c3h margin-top-20">'+
					'				<thead>'+
					'					<tr>'+
					'						<th class=" font-14 font-bold black-color no-padding-left">Batch Name</th>'+
					'						<th class=" font-14 font-bold black-color no-padding-left">Reason</th>'+
					'					</tr>'+
					'				</thead>'+
					'				<tbody>'+
					'					<tr>'+
					'						<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-remove-batch-name">'+oData["batch_name"]+'</td>'+
					'						<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-remove-batch-reason">'+oData["reason"]+'</td>'+
					'					</tr>'+
					'				</tbody>'+
					'			</table>'+
					'		</div>'+
					'	</div>'+
					'	<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>'+
					'</div>';

					uiDisplayMainRemoveDiv.prepend(sHtml);

	}

	function _displayUpdatusStatusBatch(oData)
	{
		var uiDisplayMainStatusRecord = $("#display-main-status-record");

		if($('#display-main-status-div').css('display') == 'none'){
			// console.log("it is hidden");
			$("#display-main-status-div").show();
			uiDisplayMainStatusRecord.html("");
		}

		var sHtml = '<div class="padding-all-10 dashboard-icon display-main-status-item">'+
					'	<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-status-batch-date-created">'+oData["date_created"]+'</p>'+
					'	<div class="margin-left-50 side-border">'+	
					'		<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">'+
					'			<img src="'+CGetProfileImage.getDefault(oData["user_name"])+'" alt="user profile-pic" class="width-50px display-inline-mid display-status-batch-user-pic">'+
					'			<p class="display-inline-mid margin-left-10"><span class="display-status-batch-user">'+oData["user_name"]+'</span> has removed a Batch</p>'+
					'			<table class="tbl-4c3h margin-top-20">'+
					'				<thead>'+
					'					<tr>'+
					'						<th class=" font-14 font-bold black-color no-padding-left">Batch Name</th>'+
					'						<th class=" font-14 font-bold black-color no-padding-left">Status</th>'+
					'						<th class=" font-14 font-bold black-color no-padding-left">Reason</th>'+
					'					</tr>'+
					'				</thead>'+
					'				<tbody>'+
					'					<tr>'+
					'						<td class="font-14 font-400 black-color text-center no-padding-left width-20percent display-status-batch-name">'+oData["batch_name"]+'</td>'+
					'						<td class="font-14 font-400 black-color text-center no-padding-left width-30percent display-status-batch-status">'+oData["status"]+'</td>'+
					'						<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-status-batch-reason">'+oData["reason"]+'</td>'+
					'					</tr>'+
					'				</tbody>'+
					'			</table>'+
					'		</div>'+
					'	</div>'+
					'	<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>'+
					'</div>';

					uiDisplayMainStatusRecord.prepend(sHtml);
			
	}

	function _displaySplitBatch(oData)
	{
		
		var uiDisplayMainSplitRecord = $("#display-main-split-record");

		if($('#display-main-split-div').css('display') == 'none'){
			// console.log("it is hidden");
			$("#display-main-split-div").show();
			uiDisplayMainSplitRecord.html("");
		}


		var sHtml = '<div class="padding-all-10 dashboard-icon">'+
					'	<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-split-batch-date-created">'+oData["date_created"]+'</p>'+
					'	<div class="margin-left-50 side-border">'+	
					'		<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">'+
					'			<img src="'+CGetProfileImage.getDefault(oData["user_name"])+'" alt="user profile-pic" class="width-50px display-inline-mid display-split-batch-user-pic">'+
					'			<p class="display-inline-mid margin-left-10"><span class="display-split-batch-user">'+oData["user_name"]+'</span> has split a part of Batch Name <span class="display-split-batch-name">'+oData["batch_name"]+'</span></p>'+
					'			<table class="tbl-4c3h margin-top-20">'+
					'				<thead>'+
					'					<tr>'+
					'						<th class=" font-14 font-bold black-color no-padding-left">Batch Name</th>'+
					'						<th class=" font-14 font-bold black-color no-padding-left">Quantity</th>'+
					'					</tr>'+
					'				</thead>'+
					'				<tbody class="display-split-batch-new-batches">'+
					'				</tbody>'+
					'			</table>'+
					'		</div>'+
					'	</div>'+
					'	<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>'+
					'</div>';
			var uiGetSthml = $(sHtml);
			
			uiDisplayMainSplitRecord.prepend(uiGetSthml);

			_displaySelectedSplitBatch(oData["batch_split_info"], uiGetSthml);
	}

	function _displaySelectedSplitBatch(oData, ui)
	{
		var uiDisplaySplitBatchNewBatchesItem = ui.find(".display-split-batch-new-batches");

		for(var x in oData)
		{
			var sHtml = '<tr class="display-split-batch-new-batches-item">'+
						'	<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-split-batch-name">'+oData[x]["batch_name"]+'</td>'+
						'	<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-split-batch-quality">'+oData[x]["batch_quantity"]+" "+oData[x]["unit_of_measure_name"].toUpperCase()+'</td>'+
						'</tr>';
			uiDisplaySplitBatchNewBatchesItem.append(sHtml);

		}
	}

	function _displayRepackBatch(oData)
	{
		var uiDisplayMainRepackRecord = $("#display-main-repack-record");
		
		if($('#display-main-repack-div').css('display') == 'none'){
			// console.log("it is hidden");
			$("#display-main-repack-div").show();
			uiDisplayMainRepackRecord.html("");
		}

		var sHtml = '<div class="padding-all-10 dashboard-icon">'+
					'	<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-repack-batch-date-created">'+oData["date_created"]+'</p>'+
					'	<div class="margin-left-50 side-border">'+	
					'		<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">'+
					'			<img src="'+CGetProfileImage.getDefault(oData["user_name"])+'" alt="user profile-pic" class="width-50px display-inline-mid display-repack-batch-user-pic">'+
					'			<p class="display-inline-mid margin-left-10"><span class="display-repack-batch-user">'+oData["user_name"]+'</span> has repacked Batch Name <span class="display-repack-batch-name">'+oData["batch_name"]+'</span></p>'+
					'			<table class="tbl-4c3h margin-top-20">'+
					'				<thead>'+
					'					<tr>'+
					'						<th class=" font-14 font-bold black-color no-padding-left">Previous Quantity</th>'+
					'						<th class=" font-14 font-bold black-color no-padding-left">New Quantity</th>'+
					'					</tr>'+
					'				</thead>'+
					'				<tbody>'+
					'					<tr>'+
					'						<td class="font-14 font-400 black-color text-center no-padding-left display-repack-batch-old-qty">'+oData["old_quantity"]+' Bags</td>'+
					'						<td class="font-14 font-400 black-color text-center no-padding-left display-repack-batch-new-qty">'+oData["quantity"]+' Bags</td>'+
					'					</tr>'+
					'				</tbody>'+
					'			</table>'+
					'		</div>'+
					'	</div>'+
					'	<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>'+
					'</div>';

			
			uiDisplayMainRepackRecord.prepend(sHtml);

		
	}



	function bindEvents(sAction, ui)
	{
		if(sCurrentPath.indexOf("products/stock_inventory") > -1)
		{
			
			_triggerProductsOrLocation();
		}
		else if(sCurrentPath.indexOf("products/stock_storage_location") > -1)
		{
			_triggerProductsOrLocation();
		}

	}

	function renderElements(sType, oData)
	{
		if(sCurrentPath.indexOf("products/stock_inventory") > -1)
		{
			if(sType === 'displayProductInventories' && Object.keys(oData).length > 0)
			{
				_displayAllListProduct(oData);
				_displayGridListProduct(oData);
			}
			else if(sType === 'displayNoProductInventories')
			{
				_displayNoProduct();
			}
			
		}
		else if(sCurrentPath.indexOf("products/stock_product_activity") > -1)
		{
			if(sType === 'displaySelectedProductRecord' && Object.keys(oData).length > 0)
			{
				_displayStorages(oData["product_batches"]);
				setTimeout(function(){
					_displaySelectedProductRecords(oData);
				},400);
				

				// 	console.log(JSON.stringify(oData));
			}
			else if(sType === 'renderAdjustedBatch' && Object.keys(oData).length > 0)
			{
				_displayAdjustedBatch(oData);
			}
			else if(sType === 'renderReturnedBatch')
			{
				_displayReturnedBatch(oData);
			}
			else if(sType === 'renderRemovedBatch')
			{
				_displayRemovedBatch(oData);
			}
			else if(sType === 'renderUpdatusStatusBatch')
			{
				_displayUpdatusStatusBatch(oData);
			}
			else if(sType === 'renderSplitBatch')
			{
				_displaySplitBatch(oData);
			}
			else if(sType === 'renderRepackBatch')
			{
				_displayRepackBatch(oData);
			}
		}
		else if(sCurrentPath.indexOf("products/stock_storage_location") > -1)
		{
			if(sType === 'displayStorageLocationInfo' && Object.keys(oData).length > 0)
			{
				_displayStoragesInfo(oData);

			}
		}
		

	}

	return {
		bindEvents : bindEvents,
		renderElements : renderElements,
		getProductInventory : getProductInventory,
		getSelectedProductInventoryRecords : getSelectedProductInventoryRecords,
		getAllLocation : getAllLocation,
		getAllStorages : getAllStorages,
		getallStorageLocationInfo : getallStorageLocationInfo,
		test : test,
		date_test : date_test
	}

})();


$(document).ready(function(){


	if(sCurrentPath.indexOf("products/stock_inventory") > -1)
	{	
		CProductInventory.getProductInventory();
		CProductInventory.bindEvents();
	}
	else if(sCurrentPath.indexOf("products/stock_product_activity") > -1)
	{
		CProductInventory.getAllLocation();
		CProductInventory.getAllStorages();

		setTimeout(function(){

			CProductInventory.getSelectedProductInventoryRecords();
		}, 700);


		var uiUpdateStatusBatch = $("#update-status-modal").detach(),
			uiRemoveBatchModal = $("#remove-batch-modal").detach(),
			uiAdjustQuantityModal = $("#adjust-quantity-modal").detach(),
			uiAdjustQuantityPassword = $('[modal-id="adjust-quantity-password"]').detach(),
			uiReturnBatchModal = $("#return-batch-modal").detach(),
			uiSplitBatchModal = $("#split-batch-modal").detach(),
			uiRepackBatchModal = $("#repack-batch-modal").detach();

			$(".main-section").append(uiUpdateStatusBatch);
			$(".main-section").append(uiRemoveBatchModal);
			$(".main-section").append(uiAdjustQuantityModal);
			$(".main-section").append(uiAdjustQuantityPassword);
			$(".main-section").append(uiReturnBatchModal);
			$(".main-section").append(uiSplitBatchModal);
			$(".main-section").append(uiRepackBatchModal);

		
	}
	else if(sCurrentPath.indexOf("products/stock_storage_location") > -1)
	{
		CProductInventory.getAllLocation();
		CProductInventory.getAllStorages();
	

		setTimeout(function(){

				CProductInventory.getallStorageLocationInfo();
				CProductInventory.bindEvents();
		}, 700);

		var uiUpdateStatusBatch = $("#update-status-modal").detach(),
			uiRemoveBatchModal = $("#remove-batch-modal").detach(),
			uiAdjustQuantityModal = $("#adjust-quantity-modal").detach(),
			uiAdjustQuantityPassword = $('[modal-id="adjust-quantity-password"]').detach(),
			uiReturnBatchModal = $("#return-batch-modal").detach(),
			uiSplitBatchModal = $("#split-batch-modal").detach(),
			uiRepackBatchModal = $("#repack-batch-modal").detach();

			$(".main-section").append(uiUpdateStatusBatch);
			$(".main-section").append(uiRemoveBatchModal);
			$(".main-section").append(uiAdjustQuantityModal);
			$(".main-section").append(uiAdjustQuantityPassword);
			$(".main-section").append(uiReturnBatchModal);
			$(".main-section").append(uiSplitBatchModal);
			$(".main-section").append(uiRepackBatchModal);
		
	}

});


