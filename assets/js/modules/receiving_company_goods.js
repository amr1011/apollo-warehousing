var CReceivingCompanyGoods = (function() {
	var oCompanyGoods = {},
		oStorages = {},
		oGlobalItems = {},
		bFirstLoadedItems = false,
		oFirstLoadedItems = {},
		oItemReceivingBalance = {},
		oBatchPerItem = {},
		oUploadedDocuments = {},
		bBatchEditMode = false,
		sBatchEditKey = "",
		oMeasurements = {},
		failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg";

	// ================================================== //
	// ===================PUBLIC METHODS================= //
	// ================================================== //

	function bindEvents() {
		var sCurrentPath = window.location.href;

		var btnSearch = $('.dropdown-search').find('.btn general-btn'),
			btnSearhGoods = $('.dropdown-search').find('#search-goods');


		btnSearch.click(function(){
		    alert("The paragraph was clicked.");
		});

		//Search Company Goods
		btnSearhGoods.off('click.searchGoods').on('click.searchGoods', function(){
			searchAllCompanyGoods();
		});



		if(sCurrentPath.indexOf("company_goods") > -1) {
			getAllCompanyGoods(function(oData){
				oCompanyGoods = oData;
				_displayListCompanyGoods(oCompanyGoods);
			});
		}

		if(sCurrentPath.indexOf("stock_create") > -1) {
			cr8v_platform.localStorage.set_local_storage({
				"name" : "receiving_stock_number",
				"data" : { "number" : $('.receiving-number').html() }
			});

			var showModalConfirmCreate = function()
			{
				var uiModal = $('[modal-id="confirm-create-record"]'),
					uiText = uiModal.find('.modal-content.text-left .font-14.font-400'),
					oReceivingNumber = cr8v_platform.localStorage.get_local_storage({ "name" : "receiving_stock_number" });

				uiModal.addClass("showed");
				uiModal.find(".close-me").off("click").on("click",function(){
					$("body").css({'overflow-y':'initial'});
					uiModal.removeClass("showed");
				});
				$("body").css({overflow:'hidden'});

				uiText.html("Are you sure you want to create Receiving No. "+oReceivingNumber.number+"?");

				$('#submit_create_record').off("click").on("click", function(){
					_add();
				});
			};

			var uiFormCreate = $("#submitReceiving");

			uiFormCreate.cr8vformvalidation({
				'preventDefault' : true,
			    'data_validation' : 'datavalid',
			    'input_label' : 'labelinput',
			    'onValidationError': function(arrMessages) {
			        $("body").feedback({title : "Error!", message : "Please complete the form", type : "danger", icon : failIcon });
		        },
			    'onValidationSuccess': function() {
			    	var oSelectedItems = _itemsEvent("getItems"),
			    		len = Object.keys(oSelectedItems).length,
			    		sMessage = "",
			    		error = 0;
			    	if(!len){
			    		error++;
			    		sMessage = "Please select item";
			    	}else{
			    		for(var i in oSelectedItems){
			    			if(oSelectedItems[i]["item_vendor_name"].length == 0){
			    				error++;
			    			}
			    			if(oSelectedItems[i]["item_loading_method"] == "piece"){
				    			if(oSelectedItems[i]["item_qty_by_piece"].length == 0){
				    				error++;
				    			}
			    			}else if(oSelectedItems[i]["item_loading_method"] == "bulk"){
			    				if(oSelectedItems[i]["item_qty_unit_of_measure"].length == 0){
				    				error++;
				    			}
			    			}
			    		}
			    		sMessage = "Add details to your item";
			    	}

			    	console.log(oSelectedItems);

			    	if(error > 0){
			    		$("body").feedback({title : "Item", message : sMessage, type : "danger", icon : failIcon });
			    	}else{
			    		//SAVE RECORD
			    		showModalConfirmCreate();
			    	}
		        }
			});



			var uiCreateRecordBtn = $('[modal-target="confirm-create-record"]');
			uiCreateRecordBtn.off("click");
			uiCreateRecordBtn.click(function(event) {
				uiFormCreate.submit();
			});

			_itemsEvent();
			_addNotesEvent();
			_documentEvents(true);
		}

		if(sCurrentPath.indexOf("stock_view_edit") > -1) {
			var oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "receivingCompanyGoodsRecord"});

			if(oRecord != null){
				getSingleCompanyGoods({ "receiving_id" : oRecord.receivingId }, function(oReceivingData){
					_displayEdit(oReceivingData);
				});
			}
		}

		if(sCurrentPath.indexOf("view_company_goods") > -1)
		{
			var oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "receivingCompanyGoodsRecord"});

			if(oRecord != null){
				getSingleCompanyGoods({ "receiving_id" : oRecord.receivingId }, function(oReceivingData){
					_displayView(oReceivingData);
				});
			}

			

			//cr8v_platform.localStorage.delete_local_storage({"name" : "receivingCompanyGoodsRecord"});
		}
	}

	function getAllCompanyGoods(callBack, searchCompany) {
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "receiving/get_all_company_goods",
			success : function(oResult) {
				cr8v_platform.localStorage.set_local_storage({
				"name" : "search_company_goods",
				"data" : oResult.data
			});

				searchCompany = oResult.data;
				if(typeof callBack == 'function'){
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}




	function searchTemplate() {
		var shtml = '<div class="tbl-like hover-transfer-tbl record-list">'+
				'	<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">'+
				'		<div class="display-inline-mid text-center width-15percent text-left">'+
				'			<p class=" font-400 font-14 f-none text-center no-padding-left width-100percent receiving-number"></p>'+
				'		</div>'+
				'		<div class="display-inline-mid text-center width-15percent ">'+
				'			<p class=" font-400 font-14 f-none width-100percent no-padding-left date-created"></p>'+
				'		</div>'+
				'		<div class=" display-inline-mid font-400 text-center width-40percent">'+
				'			<p class=" font-400 font-14 f-none width-100percent no-padding-left item-description"></p>'+
				'		</div>'+
				'		<div class="font-400 display-inline-mid text-center width-15percent ">'+
				'			<div class="owner-img width-30per display-inline-mid">'+
				'				<img src="" alt="Profile Owner" class="width-60percent user-img">'+
				'			</div>'+
				'			<p class="width-150px font-400 font-14 display-inline-mid f-none capitalize user-fullname"></p>'+
				'		</div>'+
				'		<div class="display-inline-mid text-center width-15percent">'+
				'			<p class=" font-400 font-14 f-none width-100percent capitalize status">Ongoing</p>'+
				'		</div>'+
				'	</div>'+
				'	<div class="hover-tbl button-holder" style="height: 100%;"> '+
				'	</div>'+
				'</div>';
		return shtml;
	}


	function searchAllCompanyGoods() {
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "receiving/search_company_goods",
			success : function(oResult) {
				oSearchData = oResult.data;

				_displaySearchData(oSearchData);

			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}



	function _displaySearchData(oSearchData){
		console.log(oSearchData[0].date_received);	 
		var uiSearchDataContainer = $("#receivingList");

		$("#receivingList").html("");
	
		console.log(JSON.stringify(oSearchData));

		for(var i in oSearchData){
			
			receiving_number = oSearchData[i].receiving_number,
			date_received = oSearchData[i].date_received;
			console.log(receiving_number + " + " + date_received);
			uiTemplate = searchTemplate();
			

				
				$("#receivingList").find('.first-tbl').find(".receiving-number").html(receiving_number);
				$("#receivingList").find('.first-tbl').find(".date-created").html(date_received);

			uiSearchDataContainer.append(uiTemplate);
			
			
			
		}
	}

	
	



	function getSingleCompanyGoods(oData, callBack)
	{
		var oOptions = {
			type : "POST",
			data : oData,
			returnType : "json",
			url : BASEURL + "receiving/get_single_company_goods",
			beforeSend: function(){
				$('[modal-id="generate-view"]').addClass('showed');
			},
			success : function(oResult) {
				if(oResult.status == false)
				{
					$('.modal-id="no-receiving-data"').addClass('showed');
					window.location.href = BASEURL+"receiving/stock_receiving"
				}
				if(typeof callBack == 'function'){
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	// ================================================== //
	// ==============END OF PRIVATE METHODS============== //
	// ================================================== //

	// ================================================== //
	// ==================PRIVATE METHODS================= //
	// ================================================== //

	function _displayEdit(oReceivingData)
	{
		var data = oReceivingData[0],
			items = data.items;

		console.log(data);

		var uiModalGenerating = $('[modal-id="generate-view"]'),
			uiModalRemoveExisting = $('[modal-id="confirm-remove-exisiting"]'),
			uiModalConfirm = $('[modal-id="confirm-update"]'),
			uiBread = $(".edit-record-no-bread"),
			uiTitle = $(".receiving-no-title"),
			btnCancel = $("#btnCancel"),
			btnSave = $("#btnSave"),
			form = $("#formBasic"),
			poNumber = $("#poNumber"),
			purchaseOfficer = $("#purchaseOfficer"),
			courrierName = $("#courrierName"),
			invoiceNumber = $("#invoiceNumber"),
			invoiceDate = $("#invoiceDate"),
			uiExistingContainer = $(".exisiting-container"),
			exisitingTemplate = $(".existing-item-template"),
			uiItemContainer = $(".item-list-container"),
			itemTemplate = $(".item-template"),
			selectItem = $("#selectItem"),
			addItem = $("#addItem"),
			selectContainer = $(".select-item"),
			inputSelectItem = selectContainer.find("input");

		var methods = {
			"setToSelect" : function(oData, callBack){
				var uiSelectParent = selectItem.parent(),
					uiCustomDD = uiSelectParent.find(".frm-custom-dropdown");

				selectItem.removeClass('transform-dd frm-custom-dropdown-origin')
				uiCustomDD.remove();
				selectItem.html("");
				for(var i in oData){
					var option = $("<option value='"+oData[i]["id"]+"'>#"+oData[i]["sku"]+" "+oData[i]["name"]+"</option>");
					selectItem.append(option);
				}


				selectItem.transformDD();
				CDropDownRetract.retractDropdown(selectItem);

				selectItem.data("items", {});
				selectItem.data("items", oData);

				if(typeof callBack == 'function'){
					callBack();
				}
			},
			"searchItems" : function(sKey, callBack){
				var oOptions = {
					type : "POST",
					data : {
						key : sKey,
						limit : 30,
						sorting : "asc",
						sort_field : "name"
					},
					url : BASEURL + "products/search",
					returnType : "json",
					success : function(oResult) {
						if(typeof callBack == 'function'){
							callBack(oResult.data);
						}
					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);
			},
			"displayDetails" : function(){
				uiBread.html("Edit Record No. "+data.receiving_number);
				poNumber.val(data.purchase_order);
				poNumber.attr("prev", data.purchase_order);
				purchaseOfficer.val(data.officer);
				purchaseOfficer.attr("prev", data.officer);
				courrierName.val(data.courier);
				courrierName.attr("prev", data.courier);
				invoiceNumber.val(data.invoice_number);
				invoiceNumber.attr("prev", data.invoice_number);
				invoiceDate.val(data.invoice_date_formatted_2);
				invoiceDate.attr("prev", data.invoice_date_formatted_2);
				uiTitle.html("Receiving No. "+data.receiving_number);
			},
			"getDetails" : function(){
				var arrExplodedDate = invoiceDate.val().split("/"),
					sDate = "",
					arrDate = [];

				for(var i = arrExplodedDate.length - 1; i>=0; i--){
					arrDate.push(arrExplodedDate[i]);
				}

				var arrDateMonth = arrDate[2],
					arrDateDay = arrDate[1];

				arrDate[1] = arrDateMonth;
				arrDate[2] = arrDateDay;

				sDate = arrDate.join("-");

				return {
					"po_number" : poNumber.val(),
					"officer" : purchaseOfficer.val(),
					"courier" : courrierName.val(),
					"invoice_number" : invoiceNumber.val(),
					"invoice_date" : sDate
				};
			},
			"deletedExisistingItems" :  {},
			"removeExistingItem" : function(e){
				var uiParent = $(e.target).closest('.existing-item'),
					parentData = uiParent.data(),
					btnConfirm = uiModalRemoveExisting.find(".confirm-btn"),
					btnClose = uiModalRemoveExisting.find(".close-me"),
					modalItemName = uiModalRemoveExisting.find(".item-name");

				uiModalRemoveExisting.addClass('showed');

				modalItemName.html("#"+parentData.product_sku +" "+parentData.product_name);

				btnClose.off("click").on("click", function(){
					uiModalRemoveExisting.removeClass('showed');
				});

				btnConfirm.off("click").on("click", function(){
					var len = Object.keys(methods.deletedExisistingItems).length;
					methods.deletedExisistingItems[len] = parentData;

					uiParent.remove();
					btnClose.click();
				});	
			},
			"exisistingItemsFirst" : {}, 
			"displayExistingItems" : function(oItems){
				uiExistingContainer.html("");
				methods.exisistingItemsFirst = oItems;
				for(var i in oItems){
					var item = oItems[i],
						ui = exisitingTemplate.clone(),
						remove = ui.find(".remove"),
						skuName = ui.find(".sku-name"),
						image = ui.find(".image");

					ui.removeClass('existing-item-template');
					ui.show();
					uiExistingContainer.append(ui);

					ui.data(item);

					skuName.html("#"+item.product_sku+" "+item.product_name);

					image.css({
						"background-image" : "url("+item.product_image+")"
					});

					remove.off("click").on("click", methods.removeExistingItem);

					if( (Object.keys(oItems).length - 1) == parseInt(i) ){
						setTimeout(function(){
							uiModalGenerating.removeClass('showed');
						},1000);
					}
				}
			},
			"radioClick" : function(e){
				var uiParent = $(this).closest('[prod-id]'),
					prodIDParent = uiParent.attr('prod-id'),
					uiInstancesItem = $('[prod-id="'+prodIDParent+'"]'),
					uiInputPiece = uiInstancesItem.find(".qty-piece"),
					uiInputReceive = uiInstancesItem.find(".qty-receive"),
					selectContainer = uiInstancesItem.find(".select-uom-container"),
					selectFrmDropdown = selectContainer.find(".frm-custom-dropdown");

				if($(this).attr("data-value") == "bulk"){
					selectFrmDropdown.css({
						"pointer-events" : ""
					});
					uiInputPiece.prop("disabled", true);
					uiInputReceive.prop("disabled", false);
					var b = uiInstancesItem.find("[data-value='bulk']"),
						p = uiInstancesItem.find("[data-value='piece']");
					b.prop("checked", true);
					b.attr("checked", "checked");
					p.removeAttr("checked");
				}else{
					selectFrmDropdown.css({
						"pointer-events" : "none"
					});
					uiInputPiece.prop("disabled", false);
					uiInputReceive.prop("disabled", true);
					
					var b = uiInstancesItem.find("[data-value='bulk']"),
						p = uiInstancesItem.find("[data-value='piece']");
					p.prop("checked", true);
					p.attr("checked", "checked");
					b.removeAttr("checked");
				}
			},
			"selectChange" : function(e){
				var uiParent = $(this).closest('[prod-id]'),
					prodIDParent = uiParent.attr('prod-id'),
					uiInstancesItem = $('[prod-id="'+prodIDParent+'"]'),
					uiInputPiece = uiInstancesItem.find(".qty-piece"),
					uiInputReceive = uiInstancesItem.find(".qty-receive"),
					selectContainer = uiInstancesItem.find(".select-uom-container"),
					selectFrmDropdown = selectContainer.find(".frm-custom-dropdown"),
					selects = uiParent.find(".select-uom"),
					selecteValue = selects.val(),
					selectedText = selects.find("option:selected").html();

				$.each(uiInstancesItem, function(){
					var uomContainer = $(this).find(".select-uom-container"),
						ddText = uomContainer.find(".dd-txt");
					
					uomContainer.find(".select-uom").val(selecteValue);
					ddText.val(selectedText);
				});

			},
			"findSameInstance" : function(prodID, newUi){
				var uiInstances = $('[prod-id="'+prodID+'"]'),
					radios = uiInstances.find('.radio-bulk-piece'),
					selects = uiInstances.find(".select-uom"),
					newUiRadios = newUi.find(".radio-bulk-piece"),
					newSelect = newUi.find(".select-uom")
				
				radios.off("click").on("click", methods.radioClick);
				newUiRadios.off("click").on("click", methods.radioClick);
				selects.off("change").on("change", methods.selectChange);
				newSelect.off("change").on("change", methods.selectChange);

				if(uiInstances.length > 0){
					var radioChecked = uiInstances.find(".radio-bulk-piece:checked"),
						sVal = radioChecked.attr("data-value"),
						newRadio = newUi.find(".radio-bulk-piece[data-value='"+sVal+"']");
					newRadio.trigger("click");
				}

				selects.trigger("change");

			},
			"makeid" : function(){
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
					text = "";

			    for( var i=0; i < 5; i++ )
			        text += possible.charAt(Math.floor(Math.random() * possible.length));

			    return text;
			},
			"getAddedItems" : function(){
				var uiAddedItems = uiItemContainer.find(".added-item:not(.item-template)"),
					oResponse = {};
				
				$.each(uiAddedItems, function(){
					var ui = $(this),
						data = ui.data(),
						vendor = ui.find(".vendor-name"),
						bulk = ui.find('[data-value="bulk"]'),
						piece = ui.find('[data-value="piece"]'),
						uom = ui.find(".select-uom"),
						qtyPiece = ui.find(".qty-piece"),
						qtyToReceive = ui.find(".qty-receive"),
						iQty = parseFloat(qtyToReceive.val()),
						sLoadingMethod = "bulk",
						sUomId = uom.val(),
						oUOM = methods.oUOM;

					if(piece.attr("checked") == "checked"){
						sLoadingMethod = "piece";
						iQty = parseFloat(qtyPiece.val());

						for(var i in oUOM){
							if(oUOM[i]["name"] == 'pc'){
								sUomId = oUOM[i]["id"];
							}
						}
					}

					oResponse[Object.keys(oResponse).length] = {
						"item_id" : data.id,
						"vendor" : vendor.val(),
						"qty" : iQty,
						"loading_method" : sLoadingMethod,
						"unit_of_measure_id" : sUomId
					}
				});
				
				return oResponse;
			},
			"addItem" : function(){

				var itemSelected = selectItem.val(),
					itemsData = selectItem.data("items"),
					selectedData = {},
					ui = itemTemplate.clone(),
					remove = ui.find(".remove"),
					skuName = ui.find(".sku-name"),
					image = ui.find(".image"),
					close = ui.find(".close"),
					qtyToReceive = ui.find(".qty-receive"),
					qtyPiece = ui.find(".qty-piece"),
					qtyInventory = ui.find(".qty-inventory"),
					agingDays = ui.find(".aging-days"),
					selectUOMContainer = ui.find(".select-uom-container"),
					selectUOM = $("<select class='select-uom'></select>"),
					sRadioName = 'bulk-or-piece',
					radio = ui.find('.radio-bulk-piece'),
					oUOM = methods.oUOM;

				for(var i in itemsData){
					if(itemsData[i]["id"] == itemSelected){
						selectedData = itemsData[i];
					}
				}

				ui.removeClass('item-template');
				ui.show();

				ui.attr("prod-id", selectedData.id);

				radio.attr("name", sRadioName+"-"+methods.makeid());

				ui.data(selectedData);

				skuName.html("#"+selectedData.sku+" "+selectedData.name);
				image.css({
					"background-image" : "url("+selectedData.image+")"
				});

				close.off("click").on("click", function(e){
					var uiParent = $(e.target).closest('.added-item');
					uiParent.remove();
				});

				qtyToReceive.number(true, 2);
				qtyPiece.number(true, 2);
				qtyInventory.number(true, 2);
				agingDays.html(selectedData.product_shelf_life);
				agingDays.number(true, 2);
				agingDays.append(" Days");

				selectUOMContainer.append(selectUOM);

				for(var i in oUOM){
					if(oUOM[i]["name"] != "pc"){
						selectUOM.append('<option value="'+oUOM[i]["id"]+'">'+oUOM[i]["name"]+'</option>');
					}
				}

				selectUOM.transformDD();
				CDropDownRetract.retractDropdown(selectUOM);

				uiItemContainer.append(ui);

				methods.findSameInstance(selectedData.id, ui);

			},
			"oUOM" : {},
			"getUOM" : function(callBack){
				var oOptions = {
					type : "POST",
					data : { data : "none" },
					url : BASEURL + "products/get_unit_of_measure",
					returnType : "json",
					success : function(oResult) {
						if(typeof callBack == 'function'){
							callBack(oResult.data);
						}
					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);
			},
			"updateModal" : function(){
				var close = uiModalConfirm.find(".close-me"),
					btnConfirm = uiModalConfirm.find(".confirm-btn"),
					uiModalUpdateing = $('[modal-id="updating-data"]');

				uiModalConfirm.addClass('showed');

				close.off("click").on("click", function(){
					uiModalConfirm.removeClass('showed');
				});

				btnConfirm.off("click").on("click", function(){
					
					var oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "receivingCompanyGoodsRecord"}),
						receivingId = oRecord.receivingId,
						oDeletedItems = methods.deletedExisistingItems,
						oAddedItems = methods.getAddedItems(),
						oInfo = methods.getDetails();
					
					var oOptions = {
						type : "POST",
						data : {
							"receiving_id" : receivingId,
							"info" : JSON.stringify(oInfo),
							"deleted_items" : JSON.stringify(oDeletedItems),
							"added_items" : JSON.stringify(oAddedItems)
						},
						url : BASEURL + "receiving/update_products_and_record",
						returnType : "json",
						beforeSend: function(){
							uiModalConfirm.removeClass('showed');
							uiModalUpdateing.addClass("showed");
						},
						success : function(oResult) {
							if(oResult.status){
								uiModalUpdateing.find(".message").html("Update Successful, reloading...");
								window.location.reload();
							}
						}
					}

					cr8v_platform.CconnectionDetector.ajax(oOptions);

					console.log(oDeletedItems);
					console.log(oAddedItems);
					console.log(oInfo);

				});
			},
			"isInExistingDisplay" : function(id){
				var uiExisisting = $(".existing-item:not(.existing-item-template)"),
					bReturn = false;
				$.each(uiExisisting, function(){
					var data = $(this).data();
					if(data.id == id){
						uiExisisting = true;
					}
				});
				return uiExisisting;
			},
			"compareDetails" : function(){
				var error = 0,
					bHasChanges = false;
				if(poNumber.val() == poNumber.attr("prev") && bHasChanges === false){
					error++;
				}else{
					bHasChanges = true;
				}
				if(purchaseOfficer.val() == purchaseOfficer.attr("prev") && bHasChanges === false){
					error++;
				}else{
					bHasChanges = true;
				}
				if(courrierName.val() == courrierName.attr("prev") && bHasChanges === false){
					error++;
				}else{
					bHasChanges = true;
				}
				if(invoiceNumber.val() == invoiceNumber.attr("prev") && bHasChanges === false){
					error++;
				}else{
					bHasChanges = true;
				}
				if(invoiceDate.val() == invoiceDate.attr("prev") && bHasChanges === false){
					error++;
				}else{
					bHasChanges = true;
				}

				var lenExist = Object.keys(methods.exisistingItemsFirst).length,
					uiExisisting = $(".existing-item:not(.existing-item-template)"),
				    uiAdded = $(".added-item:not(.item-template)"),
				    existCounter = 0;

				if(lenExist == uiExisisting.length && bHasChanges === false){
					for(var i in methods.exisistingItemsFirst){
						var item = methods.exisistingItemsFirst[i],
							bIsExist = methods.isInExistingDisplay(item.id);
						if(bIsExist){
							existCounter++;
						}
					}
				}else{
					bHasChanges = true;
				}

				if(lenExist == existCounter && bHasChanges === false){
					error++;
				}

				if(uiAdded.length > 0){
					bHasChanges = true;
				}

				if(bHasChanges){
					return true;
				}else{
					return false;
				}
				
			},
			"validateAddedItems" : function(){
				var bReturn = true,
					error = 0,
					uiAdded = $(".added-item:not(.item-template)");

				if(uiAdded.length > 0){
					$.each(uiAdded, function() {
						var ui = $(this),
							vendor = ui.find(".vendor-name"),
							qtyToReceive = ui.find(".qty-receive"),
							qtyPiece = ui.find(".qty-piece"),
							radioBulk = ui.find(".radio-bulk-piece[data-value='bulk']"),
							radioPiece = ui.find(".radio-bulk-piece[data-value='piece']"),
							loadingMethod = ui.find(".radio-bulk-piece[checked]").attr("data-value");
						
						if(vendor.val().trim().length == 0){
							error++;
						}

						if(radioBulk.attr("checked") && qtyToReceive.val().trim().length == 0){
							error++;
						}

						if(radioPiece.is(":checked") && qtyPiece.val().trim().length == 0){
							error++;
						}
					});
				}

				if(error > 0){
					bReturn = false;
				}

				return bReturn;
			},
			"formInit" : function(){
				form.cr8vformvalidation({
					'preventDefault' : true,
				    'data_validation' : 'datavalid',
				    'input_label' : 'labelinput',
				    'onValidationError': function(arrMessages) {
				    	var arr = [];
				    	for(var i in arrMessages){
				    		arr.push(arrMessages[i]["error_message"]);
				    	}
				        $("body").feedback({title : "Error!", message :  arr.join(","), type : "danger", icon : failIcon });
			        },
				    'onValidationSuccess': function() {
				    	var uiExisisting = $(".existing-item:not(.existing-item-template)"),
				    		uiAdded = $(".added-item:not(.item-template)"),
				    		compareData = methods.compareDetails(),
				    		bValidAdded = methods.validateAddedItems(),
				    		errorMsg = [];

				    	if(uiAdded.length == 0 && uiExisisting.length == 0){
				    		errorMsg.push("Please leave a exisisting product or add new product");
				    	}

				    	if(!compareData){
				    		errorMsg.push("Nothing is changed");
				    	}

				    	if(!bValidAdded){
				    		errorMsg.push("Please complete items' details");
				    	}

				    	if(errorMsg.length == 0){
				    		methods.updateModal();
				    	}else{
				    		$("body").feedback({title : "Error!", message :  errorMsg.join(","), type : "danger", icon : failIcon });
				    	}
			        }
				});
			},
			"formSubmit" : function(){
				form.submit();
			}
		};

		uiModalGenerating.addClass('showed');

		addItem.off("click").on("click", methods.addItem);

		var iTime = {};

		methods.displayDetails();
		methods.displayExistingItems(items);
		methods.searchItems("", function(data){
			methods.setToSelect(data, function(){
				var selectContainer = $(".select-item"),
					inputSelectItem = selectContainer.find("input");

				inputSelectItem.off("keyup.searchitem").on("keyup.searchitem", function(){
					var val = $(this).val(),
						sKey = "";

					inputSelectItem.attr("value", val);
					if(val.trim().length > 0){
						sKey = val;
					}

					clearTimeout(iTime);

					iTime = setTimeout(function(){
						methods.searchItems(sKey, function(data){
							var uiSelectOptionContainer = selectContainer.find(".frm-custom-dropdown-option");
							uiSelectOptionContainer.html("");

							for(var i in data){
								uiSelectOptionContainer.append("<div class='option' data-value='"+data[i]["id"]+"'>#"+data[i]["sku"]+" "+data[i]["name"]+"</div>");
							}

							uiSelectOptionContainer.show();

							selectItem.data("items", {});
							selectItem.data("items", data);
						});
					},700);
				});
			});
		});

		methods.getUOM(function(data){
			methods.oUOM = data;
		});

		btnCancel.off("click").on("click", function(){
			window.location.href = BASEURL + "receiving/view_company_goods";
		});

		methods.formInit();

		btnSave.off("click").on("click", methods.formSubmit);

	}

	function _displayView(oData)
	{
		var oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "receivingCompanyGoodsRecord"}),
			receivingId = oRecord.receivingId;
		var data = oData[0],
			displayReceivingNumber = $("#displayReceivingNumber"),
			displayPoNumber = $("#displayPoNumber"),
			displayOfficer = $("#displayOfficer"),
			displayCourierName = $("#displayCourierName"),
			displayInvoiceNumber = $("#displayInvoiceNumber"),
			displayInvoiceDate = $("#displayInvoiceDate"),
			displayCreatedDate = $("#displayCreatedDate"),
			displayStatus = $("#displayStatus");

		
		displayReceivingNumber.html("Edit Record No. "+data.receiving_number);
		displayPoNumber.html(data.purchase_order);
		displayOfficer.html(data.officer);
		displayCourierName.html(data.courier);
		displayInvoiceNumber.html(data.invoice_number);
		displayInvoiceDate.html(data.invoice_date_formatted);
		displayCreatedDate.html(data.date_created_formatted);


		if(data.status == "0" || data.status == "2"){
			displayStatus.html("Ongoing");
			if(data.status == "2"){
				$("#enterProductDetails").replaceWith('<button class="btn general-btn click-check-btn display-inline-mid receiving-stock-disable" id="completeRecord">Complete Receiving Record</button>');
			}else{
				$("#enterProductDetails").replaceWith('<button class="btn general-btn display-inline-mid min-width-100px modal-trigger complete-receiving-record" modal-target="receiving-record" disabled="disabled">Complete Receiving Record</button>');
			}
		}else if(data.status == "1"){
			displayStatus.html("Complete");
			$("#enterProductDetails").remove();
			$("#editRecord").remove();
			$('[modal-target="upload-documents"]').remove();
			$('[modal-target="add-note"]').remove();
		}

		_displayItems(data.items, data);
		_displayNotes(data.notes, data);
		_documentEvents();
		_displayDocuments(data.documents, data);

		var oOptions = {
			type : "POST",
			data : { data : "none" },
			url : BASEURL + "products/get_unit_of_measure",
			returnType : "json",
			success : function(oResult) {
				oMeasurements = oResult.data;
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _displayDocuments(oDocs, oRecord)
	{
		var bChangeBg = false,
			status = (oRecord) ? oRecord.status : '';
		var uiContainer = $("#displayDocuments"),
			sTemplate = '<div class="table-content position-rel tbl-dark-color">'+
							'<div class="content-show padding-all-10">'+
								'<div class="width-85per display-inline-mid padding-left-10">'+
									'<i class="fa font-30 display-inline-mid width-50px icon"></i>'+
									'<p class=" display-inline-mid doc-name">Document Name</p>'+
								'</div>'+
								'<p class=" display-inline-mid date-time"></p>'+
							'</div>'+
							'<div class="content-hide" style="height: 50px;">'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30 view-document">View</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid download-document">'+
									'<button class="btn general-btn">Download</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30">Print</button>'+
								'</a>'+
							'</div>'+
						'</div>';

		uiContainer.html("");
		for(var i in oDocs){
			var uiList = $(sTemplate),
				ext = (oDocs[i]["document_path"].substr(oDocs[i]["document_path"].lastIndexOf('.') + 1)).toLowerCase();

			if(ext == 'csv'){
				uiList.find(".icon").addClass('fa-file-excel-o');
			}else if(ext == 'pdf'){
				uiList.find(".icon").addClass('fa-file-pdf-o');
			}

			uiList.data(oDocs[i]);
			uiList.find(".doc-name").html(oDocs[i]["document_name"]);
			uiList.find(".date-time").html(oDocs[i]["date_added_formatted"]);
			if(bChangeBg){
				uiList.removeClass('tbl-dark-color');
				bChangeBg = false;
			}else{
				bChangeBg = true;
			}

			uiContainer.append(uiList);
		}

		_downloadDocument(oDocs);
	}

	function _downloadDocument(oDocs)
	{
		
		var	btnDownloadDocument = $(".download-document"),
			btnViewDocument = $(".view-document");


		btnDownloadDocument.off("mouseenter.download").on("mouseenter.download", function(e){
			e.preventDefault();
			var uiDownloadThis = $(this),
				uiParentContainer = uiDownloadThis.closest('.table-content.position-rel'),
				uiGetDataPath = uiParentContainer.data("document_path"),
				uiGetDataName = uiParentContainer.data("document_name");

				uiDownloadThis.attr("href", uiGetDataPath);
				uiDownloadThis.attr("download", uiGetDataName);

		});


		btnViewDocument.off("click.viewDocument").on("click.viewDocument", function(e){
			e.preventDefault();
			var uiParentContainer = $(this).closest('.table-content.position-rel'),
				uiGetDataPath = uiParentContainer.data("document_path");

				window.open(uiGetDataPath, '_blank');
		});
	}
	
	function _documentEvents(bTemporary)
	{
		var btnUploadDocs = $('[modal-target="upload-documents"]'),
			oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "receivingCompanyGoodsRecord"}),
			receivingId = (oRecord !== null) ? oRecord.receivingId : '';
		
		if(btnUploadDocs.length > 0){
			btnUploadDocs.off('click');

			var extraData = { "receiving_id" : receivingId };

			if(bTemporary === true){
				extraData["temporary"] = true;
			}

			btnUploadDocs.cr8vFileUpload({
				url : BASEURL + "receiving/upload_document",
				accept : "pdf,csv",
				filename : "attachment",
				extraField : extraData,
				onSelected : function(bIsValid){
					if(bIsValid){
						//selected valid
					}
				},
				success : function(oResponse){
					if(oResponse.status){
						$('[cr8v-file-upload="modal"] .close-me').click();
						var oDocs = oResponse.data[0]["documents"];
						if(bTemporary){
							oUploadedDocuments[Object.keys(oUploadedDocuments).length] = oResponse.data[0]["documents"][0];
							_displayDocuments(oUploadedDocuments);
						}else{
							_displayDocuments(oDocs);
						}
					}
				},
				error : function(){
					$("body").feedback({title : "Message", message : "Was not able to upload, file maybe too large", type : "danger", icon : failIcon});
				}
			});
		}

		
	}

	function _displayBatches(prodID, bHideHover)
	{
		var oBatch = oBatchPerItem[prodID],
			productContainer = $(".record-item[prod-id='"+prodID+"']"),
			recordData = productContainer.data(),
			uiAssignmentDetails = productContainer.find(".assignment-display"),
			uiTable = uiAssignmentDetails.find(".tbl-4c3h"),
			uiTableSibling = uiTable.nextAll(),
			uiHibeAddBatchBtn = uiAssignmentDetails.find(".hide-method"),
			uiSpanReceivingTotal = productContainer.find(".received-quantity"),
			uiSpanDiscrepancy = productContainer.find(".discrepancy"),
			receivingData = productContainer.find(".qty-to-receive").data(),
			qtyToReceive = parseFloat(receivingData.quantity),
			uom = receivingData["data-uom"];
		
		uiTableSibling.remove();
		uiHibeAddBatchBtn.show();

		var template = {
			container : "<div class='transfer-prod-location font-0 bggray-white'></div>",
			total : '<div class="total-amount-storage margin-top-10"> <p class="first-text">Total</p><p class="second-text"> </p> <div class="clear"></div></div>',
			hier_text : '<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>',
			hier_arrow : '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>',
			list : '<div class="storage-assign-panel position-rel">'+
						'<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change">'+
							'<div class="width-25percent display-inline-mid text-center">'+
								'<p class="font-14 font-400 batch-name"> </p>'+
							'</div>'+
							'<div class="width-50percent text-center display-inline-mid height-auto line-height-25 hierarchy">'+											
							'</div>'+
							'<div class="width-25percent display-inline-mid text-center">'+
								'<p class="font-14 font-400 quantity"></p>'+
							'</div>'+
						'</div>'+
						'<div class="btn-hover-storage" style="height: 55px; display: none; opacity: 1;">'+
							'<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>'+
							'<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>'+
						'</div>'+
					'</div>',
			not_yet_assigned : '<div class="transfer-prod-location font-0 bggray-white position-rel">'+
                                '<div class="width-100percent padding-top-10 padding-bottom-10 show-ongoing-content margin-bottom-20 background-change">'+
                                    '<div class="width-100percent display-inline-mid text-center">'+
                                        '<p class="font-14 font-400 italic gray-color" gray-color="true">Not Yet Assigned</p>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'
		};
		
		var uiContainer = $(template.container),
			stripChange = false,
			iTotal = 0,
			unitOfMeasure = "";
		
		uiContainer.insertAfter(uiTable);

		if(Object.keys(oBatch).length > 0){
			for(var i in oBatch){
				var uiListContainer = $(template.list),
					oHierarchy = oBatch[i]["hierarchy"],
					uom = oBatch[i]["uom"],
					batch_amount = oBatch[i]["batch_amount"],
					uiRemoveBatch = uiListContainer.find("[modal-target='remove-batch']"),
					uiEditBatch = uiListContainer.find("[modal-target='edit-batch']");

				uiListContainer.data(oBatch[i]);
				
				uiRemoveBatch.attr("batch-key", i);
				uiEditBatch.attr("batch-key", i);

				uiEditBatch.off('click').on('click', function(e){
					var uiThis = $(this),
						uiList = uiThis.closest(".storage-assign-panel"),
						uiThisParent = uiThis.closest(".transfer-prod-location"),
						uiParent = uiThis.closest(".record-item"),
						uiParentData = uiParent.data(),
						batchData = uiParentData.batch,
						batchKey = uiThis.attr("batch-key"),
						uiTotalContainer = uiParent.find(".total-amount-storage"),
						oBatchData = {};
					
					sBatchEditKey = batchKey;
					bBatchEditMode = true;

					for(var i in batchData){
						if(i == batchKey){
							oBatchData = batchData[i];	
						}
					}
					_addBatchEvent(e, function(){

						var breadCrumbsHierarchy = $("#breadCrumbsHierarchy"),
							uiBreadParent = breadCrumbsHierarchy.parent(),
							batchDisplay = $("#batchDisplay"),
							oHierarchy = oBatchData.hierarchy,
							oQueue = {};
						
						$(".input-batch-name").val(oBatchData.batch_name);
						$(".input-batch-amount").val(oBatchData.batch_amount);

						batchDisplay.css("opacity", 0);
						var uiLoader = $("<div loader='true'></div>");
						uiLoader.css({
							"position" : "absolute",
							"top" : "0px",
							"left" : "0px",
							"width" : "100%",
							"height" : "100%",
							"background-color" : "#444",
							"color" : "#fff",
							"text-align" : "center"
						});
						var uiSpinner = $('<div><i class="fa fa-cog fa-spin fa-3x fa-fw margin-bottom"></i>Loading..</div>');
						uiSpinner.css({
							"position" : "absolute",
							"width" : "30px",
							"height" : "30px",
							"top" : "40%",
							"left" : "50%",
							"margin-left" : "-15px",
							"margin-right" : "-15px"
						});
						uiLoader.html(uiSpinner);
						uiBreadParent.append(uiLoader);
						uiBreadParent.css("position", "relative");

						for(var i in oHierarchy){
							oQueue[i] = {};
							if(i == "0"){
								oQueue[i]["selector"] = "[storage-id='"+oHierarchy[i]["id"]+"']";
							}else{
								oQueue[i]["selector"] = "[location-id='"+oHierarchy[i]["id"]+"']";
							}

							oQueue[i]["callback"] = function(){
								var oThis = this, 
									selector = oThis.selector,
									uiLocation = $(selector);

								uiLocation.click();
								setTimeout(function(){
									if(typeof oThis.nextcall == 'function'){
										uiLocation.click();
										setTimeout(function(){
											var oNext = oThis.nextobj;
											oThis.nextcall.apply(oNext);

											if(oNext.hasOwnProperty("last")){
												if(oNext.last){
													setTimeout(function(){
														batchDisplay.css("opacity", 1);
														$("[loader='true']").remove();
													},1300);
												}
											}
										},500);
									}
								}, 30);
							}
						}
						
						var timeoutCall = function(obj){
							setTimeout(function(){
								var que = obj,
									callback = que.callback;
								callback.apply(que);
							}, 500);
						}
						
						for(var i in oQueue){
							var queNext = oQueue[parseInt(i) + 1];
							if(queNext){
								oQueue[i]['callback'].bind(queNext);
								oQueue[i]['nextobj'] = queNext;
								oQueue[i]['nextcall'] = oQueue[parseInt(i) + 1]['callback'];
								oQueue[i]['nextcall_from'] = oQueue[i]["selector"];
								oQueue[i]['nextcall_to'] = oQueue[parseInt(i) + 1]["selector"];
							}else{
								oQueue[i]['last'] = true;
							}
							timeoutCall(oQueue[i]);
						}
						 

					});
				});

				uiRemoveBatch.off('click').on('click', function(){
					var uiThis = $(this),
						uiList = uiThis.closest(".storage-assign-panel"),
						uiThisParent = uiThis.closest(".transfer-prod-location"),
						uiParent = uiThis.closest(".record-item"),
						uiParentData = uiParent.data(),
						uom = uiParentData.product_unit_of_measure,
						batchData = uiParentData.batch,
						batchKey = uiThis.attr("batch-key"),
						uiTotalContainer = uiParent.find(".total-amount-storage"),
						uiDiscrepancy = uiParent.find(".discrepancy"),
						uiReceivingTotal = uiParent.find(".received-quantity"),
						iBatchAmount = 0;
					
					console.log(batchData);
					for(var i in batchData){
						if(i == batchKey){
							iBatchAmount = parseFloat(batchData[i]["batch_amount"]);
							delete batchData[i];	
						}
					}

					var counter = 0,
						newBatchData = {};
					for(var i in batchData){
						newBatchData[counter] = batchData[i];
						counter++;
					}

					uiParent.data("batch", newBatchData);
					uiList.remove();

					if(Object.keys(newBatchData).length == 0){
						var uiNotYetAssigned = $(template.not_yet_assigned);

						batchData = {};
						uiTotalContainer.remove();

						uiNotYetAssigned.insertAfter(uiTable);

						oItemReceivingBalance[uiParentData.id] = parseFloat(uiParentData.quantity);

						uiDiscrepancySpan = uiDiscrepancy.find("span");
						uiDiscrepancySpan.html($.number(0, 2));
						
						uiReceivingTotal.html($.number(0, 2) + " "+uom);

					}else{
						var iBal = parseFloat(oItemReceivingBalance[uiParentData.id]),
							iTotal = 0;

						iTotal = iBal + iBatchAmount;

						oItemReceivingBalance[uiParentData.id] = iTotal;
						
						oBatchPerItem[uiParentData.id] = newBatchData;
						
						_displayBatches(uiParentData.id);
					}

					//oItemReceivingBalance[oRecord.id];
				});


				unitOfMeasure = uom;
				
				if(stripChange){
					uiListContainer.find(".background-change").removeClass("background-change");
				}

				uiListContainer.find(".batch-name").html(oBatch[i]["batch_name"]);
				
				for(var x in oHierarchy){
					var uiHierContainer = uiListContainer.find(".hierarchy"),
						uiHierArrow = $(template.hier_arrow),
						uiHierText = $(template.hier_text);
					
					uiHierText.html(oHierarchy[x]["name"]);
					uiHierContainer.append(uiHierText);
					if((Object.keys(oHierarchy).length - 1) != x){
						uiHierContainer.append(uiHierArrow);	
					}
				}

				uiListContainer.find(".quantity").html($.number(batch_amount, 2)+" "+uom);
				
				if(bHideHover){
					uiListContainer.css({
						"pointer-events" : "none"
					});
				}

				uiContainer.append(uiListContainer);

				if(stripChange){
					stripChange = false;
				}else{
					stripChange = true;
				}

				iTotal = iTotal + parseFloat(batch_amount);
			}
			var uiTotal = $(template.total);
			
			uiTotal.find(".second-text").html($.number(iTotal, 2)+" "+unitOfMeasure);
			uiTotal.insertAfter(uiContainer);

			var receivingTotal = uiSpanReceivingTotal.html(),
				discrepancy = uiSpanDiscrepancy.html(),
				iReceivingTotal = parseFloat(receivingTotal),
				iDiscrepancy = parseFloat(discrepancy),
				iReceivingTotalChanged = iTotal,
				computedReceiving = qtyToReceive - iTotal,
				uiCaseSelect = productContainer.find(".cause-of-discrepancy").find(".frm-custom-dropdown");
			
			uiSpanReceivingTotal.html(iTotal);
			uiSpanReceivingTotal.data("receiving-total", iTotal);
			uiSpanReceivingTotal.number(true, 2);
			uiSpanReceivingTotal.append(" "+uom);
			
			if(computedReceiving == 0){
				uiSpanDiscrepancy.html(0);
				productContainer.find(".discrepancy").data("discrepancy", {
					"value" : 0,
					"type" : "none"
				});
				productContainer.find(".remarks-textbox textarea").prop("disabled", true);
				uiCaseSelect.attr("style", "pointer-events:none;");
			}else{
				if(computedReceiving < 0){
					uiSpanDiscrepancy.html('<i class="fa fa-arrow-up" aria-hidden="true" style="font-size:11px; margin-right:3px; margin-bottom:2px;"></i>'+$.number(Math.abs(computedReceiving), 2) +" "+uom);
					productContainer.find(".discrepancy").data("discrepancy", {
						"value" : Math.abs(computedReceiving),
						"type" : "up"
					});
				}else{
					uiSpanDiscrepancy.html('<i class="fa fa-arrow-down" aria-hidden="true" style="font-size:11px; margin-right:3px; margin-bottom:2px;"></i>'+$.number(computedReceiving, 2)+" "+uom);
					productContainer.find(".discrepancy").data("discrepancy", {
						"value" : computedReceiving,
						"type" : "down"
					});
				}

				productContainer.find(".remarks-textbox textarea").prop("disabled", false);

				uiCaseSelect.removeAttr("style");
			}

			productContainer.data("batch", oBatch);
		}else{
			uiContainer.append($(template.not_yet_assigned));
			productContainer.find(".discrepancy").data("discrepancy", 0);
			uiSpanReceivingTotal.data("receiving-total", 0);
		}

		
	}

	function _getBreadCrumbsHierarchy()
	{
		var breadCrumbsHierarchy = $("#breadCrumbsHierarchy"),
			uiMainBread = breadCrumbsHierarchy.find(".bggray-7cace5"),
			oMainBread = uiMainBread.data(),
			oResponse = {};
		
		oResponse[0] = oMainBread;

		var uiNextAllBread = uiMainBread.nextAll(".item-bread");
		$.each(uiNextAllBread, function(){
			var thisData = $(this).data();
			oResponse[Object.keys(oResponse).length] = thisData;
		});

		return oResponse;

	}

	function _addBatchEvent(e, callBack)
	{
		var uiTarget = $(e.target),
			uiList = uiTarget.closest('.record-item'),
			oRecord = uiList.data(),
			batchModal = $('[modal-id="add-batch"]'),
			batchModalClose = batchModal.find(".close-me"),
			confirmAddBatch = $("#confirmAddBatch"),
			itemReceivingBalance = oItemReceivingBalance[oRecord.id];
		

		batchModal.addClass("showed");

		if(bBatchEditMode){
			var batchAmount = parseFloat(oBatchPerItem[oRecord.id][sBatchEditKey]["batch_amount"]);
			itemReceivingBalance = itemReceivingBalance + batchAmount;
		}


		var uiNameSku = batchModal.find(".product-name-sku"),
			uiInputName = batchModal.find(".input-batch-name"),
			uiToReceive = batchModal.find(".to-receive"),
			uiInputAmount = batchModal.find(".input-batch-amount"),
			uiReceiveBal = batchModal.find(".receiving-balance"),
			iReceivingTotalBal = parseFloat(itemReceivingBalance),
			timeoutType = {};

		uiNameSku.html("SKU No. "+oRecord.product_sku+" - "+oRecord.product_name);
		uiToReceive.html($.number(oRecord.quantity, 0)+" "+oRecord.product_unit_of_measure);
		uiReceiveBal.html($.number(itemReceivingBalance, 0)+" "+oRecord.product_unit_of_measure);
		uiInputAmount.number(true, 0);

		uiInputName.val("");
		uiInputAmount.val("");

		uiInputAmount.off('keyup.changeamount').on('keyup.changeamount', function(){
			var uiThis = $(this),
				sThisVal = uiThis.val(),
				iThisVal = parseFloat(sThisVal),
				iBalance = parseFloat(itemReceivingBalance),
				uom = oRecord.product_unit_of_measure;

			clearTimeout(timeoutType);

			timeoutType = setTimeout(function(){
				if(sThisVal.trim().length > 0){
					var iTotal = iBalance - iThisVal;
					if(iTotal > iBalance){
						iTotal = iBalance;
						uiReceiveBal.html('<i class="fa fa-arrow-up" aria-hidden="true"></i>'+$.number(iTotal, 0)+" "+uom);
						//iReceivingTotalBal = iTotal;
					}else if(iTotal < 0){

						uiReceiveBal.html('<i class="fa fa-arrow-up" aria-hidden="true"></i>'+$.number(Math.abs(iTotal), 0)+" "+uom);
						//uiThis.val(iBalance);
						//iReceivingTotalBal = 0;

					}else{
						uiReceiveBal.html($.number(iTotal, 0)+" "+uom);
						//iReceivingTotalBal = iTotal;
					}
				}else{
					uiReceiveBal.html($.number(iBalance, 0)+" "+uom);
					//iReceivingTotalBal = iBalance;
				}
				clearTimeout(timeoutType);
			},500);
			

		});

		console.log(oRecord);
		console.log(oStorages);

		_displayBatchStorages(oStorages);

		batchModalClose.off('click').on('click', function(){
			batchModal.removeClass("showed");
			bBatchEditMode = false;
		});

		confirmAddBatch.off('click').on('click', function(){
			var uiSelectedLocation = $(".item-bread.temporary"),
				oLocation = uiSelectedLocation.data(),
				error = 0,
				arrErrorMsg = [],
				recordBatch = oBatchPerItem[oRecord.id],
				len = Object.keys(recordBatch).length;

			if(oItemReceivingBalance[oRecord.id] == 0 && !bBatchEditMode){
				error++;
				arrErrorMsg.push("Receiving Balance Already Been Used");
			}else{
				if(!oLocation){
					error++;
					arrErrorMsg.push("Select Location");
				}


			if(uiInputName.val().trim().length == 0){
				error++;
				arrErrorMsg.push("Enter Batch Name");
			}

				if(uiInputName.val().trim().length == 0){
					error++;
					arrErrorMsg.push("Enter Batch Name");
				}
				
			}

			if(error > 0){
				$("body").feedback({title : "Message", message : arrErrorMsg.join(","), type : "danger", icon : failIcon});
			}else{
				// Add batch
				if(!bBatchEditMode){
					oBatchPerItem[oRecord.id][len] = {
						"location" : oLocation,
						"item_id" : oRecord.product_id,
						"batch_name" : uiInputName.val(),
						"batch_amount" : uiInputAmount.val(),
						"uom" : oRecord.product_unit_of_measure,
						"uom_id" : oRecord.unit_of_measure_id,
						"hierarchy" : _getBreadCrumbsHierarchy()
					};
				}else{
					oBatchPerItem[oRecord.id][sBatchEditKey] = {
						"location" : oLocation,
						"item_id" : oRecord.product_id,
						"batch_name" : uiInputName.val(),
						"batch_amount" : uiInputAmount.val(),
						"uom" : oRecord.product_unit_of_measure,
						"uom_id" : oRecord.unit_of_measure_id,
						"hierarchy" : _getBreadCrumbsHierarchy()
					}
					bBatchEditMode = false;
				}
				oItemReceivingBalance[oRecord.id] = iReceivingTotalBal;
				

				$('[modal-target="add-batch"]').show();
				$("[modal-id='add-batch'] .close-me:visible").click();

				_displayBatches(oRecord.id);
			}

		});

		setTimeout(function(){
			if(typeof callBack == 'function'){
				callBack();
			}
		}, 100);

	}

	function _displayBatchStorages(oStorages)
	{
		var batchModal = $('[modal-id="add-batch"]'),
			breadCrumbsHierarchy = $("#breadCrumbsHierarchy"),
			batchDisplay = $("#batchDisplay"),
			oSelectedUI = [],
			oDisplayedData = {},
			oToPlaceData = {},
			doubleClickLevel = 0;

		batchDisplay.html("");

		var sTemplate = '<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor hier-item">'+
				'<div class="display-inline-mid width-20percent overflow-hide half-border-radius">'+
					'<img src="" alt="images" class="width-100percent">'+
				'</div>'+
				'<div class="display-inline-mid">'+
					'<p class="font-14 font-400 location-name"></p>'+
					'<p class="font-12 font-400 italic qty-in-location"> </p>'+
				'</div>'+
			'</div>';

		var sBackArrow = '<i style="margin-right:4px;" class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small padding-right-10 border-black back-arrow"></i>',
			sNextIcon = '<span class="display-inline-mid padding-all-5 greater-than">&gt;</span>',
			sItemBread = '<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid item-bread"></p>',
			sMainBread = '<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5"> </p>',
			sActiveBread = '<span class="active-bread"></span>';
		

		var setActiveEvent = function(uiThis){
			batchModal.find(".bggray-light").removeClass('bggray-light active-hier');
			
			var uiTraget = uiThis,
				oData = uiTraget.data();

			if(!uiTraget.hasClass("hier-item")){
				uiTraget = uiTraget.closest(".hier-item");
				oData = uiTraget.data();
			}

			if(uiTraget.closest(".hier-item").length > 0){
				uiTraget.closest(".hier-item").addClass('bggray-light active-hier');
			}else if(uiTraget.hasClass("hier-item")){
				uiTraget.addClass('bggray-light');
			}

			if(Object.keys(oSelectedUI).length > 0){
				oToPlaceData = oData;
				var	uiNext = $(sNextIcon),
					uiBread = $(sItemBread);
				breadCrumbsHierarchy.find(".temporary").remove();
				uiNext.addClass("temporary");
				uiBread.addClass("temporary");
				breadCrumbsHierarchy.append(uiNext);
				uiBread.html(oData.name);
				uiBread.data(oData);
				breadCrumbsHierarchy.append(uiBread);
			}

		}

		var displayBreadCrumbsStorage = function(){
			var uiBread = $(sMainBread),
				oData = selectedUI.data();
			uiBread.html(oData.name);
			
			breadCrumbsHierarchy.find(".greater-than").remove();
			

			breadCrumbsHierarchy.append(uiBread);
		}

		var displayBreadCrumbsItems = function(){
			breadCrumbsHierarchy.find(".greater-than").remove();
			breadCrumbsHierarchy.find(".item-bread").remove();
			breadCrumbsHierarchy.find(".bggray-7cace5").remove();

			for(var i in oSelectedUI){
				var uiMainBread = $(sMainBread),
					uiNext = $(sNextIcon),
					uiBread = $(sItemBread);
				 if(i == "0"){
				 	uiMainBread.html(oSelectedUI[i]["name"]);
				 	uiMainBread.data(oSelectedUI[i]);
					breadCrumbsHierarchy.append(uiMainBread);
				 }else{
					breadCrumbsHierarchy.append(uiNext);
					uiBread.html(oSelectedUI[i]["name"]);
					uiBread.data(oSelectedUI[i]);
					breadCrumbsHierarchy.append(uiBread);
				 }
			}

		}

		var selectHierarchy = function(uiThis){
			var oData = uiThis.data(),
				iLevel = oData.iLevel,
				sQtyInLocationParent = oData.sQtyInLocationParent,
				sFirstLevelID = "",
				oDataToDisplay = {};
			
			if(oData.children.length > 0){
				oSelectedUI[doubleClickLevel] = oData;
				doubleClickLevel++;
				oDisplayedData[doubleClickLevel] = oData.children;
				displayChildren(oData.children);
				displayBreadCrumbsItems();
			}
			
			
		}

		var displayChildren = function(oChildren){
			
			if(Object.keys(oChildren).length > 0){
				batchDisplay.html("");
			}
			for(var i in oChildren){
				var uiItem = $(sTemplate),
					data = oChildren[i];
				
				data["children"] = data.children;
				uiItem.find(".location-name").html(data.name);
				uiItem.find(".qty-in-location").html("Number of Locations inside = "+Object.keys(data.children).length);
				//uiItem.find("img").attr("src", CGetProfileImage.getDefault(data.name));
				uiItem.find("img").attr("src", BASEURL+"assets/images/folder-circle-blue.png");
				uiItem.data(data);
				uiItem.attr("location-id", data.id);

				uiItem.single_double_click(
					function(){
						setActiveEvent($(this));
					},
					function(){
						selectHierarchy($(this));
					}
				);

				batchDisplay.append(uiItem);
			}

		}

		var uiBackArrow = $(sBackArrow);

		uiBackArrow.on("click", function(){
			if(doubleClickLevel > 0){
				doubleClickLevel = doubleClickLevel - 1;
				delete oSelectedUI[doubleClickLevel];
				if(doubleClickLevel > 0){
					displayChildren(oDisplayedData[doubleClickLevel]);
				}else if(doubleClickLevel == 0){
					displayStorages(oStorages);
				}

				displayBreadCrumbsItems();
			}
		});

		breadCrumbsHierarchy.html("").append(uiBackArrow);	
		
		var makeHierarchy = function(oData){
			var flat = {};
			for(var i in oData){
				var k = oData[i]["id"];
				oData[i]["children"] = [];
				flat[k] = oData[i];
			}

			for (var i in flat) {
				var parentkey = flat[i].parent_id;
				if(flat[parentkey]){
					flat[parentkey].children.push(flat[i]);
				}
			}

			var root = [];
			for (var i in flat) {
				var parentkey = flat[i].parent_id;
				if (!flat[parentkey]) {
					root.push(flat[i]);
				}
			}
			return root;
		};

		var displayStorages = function(oStorages){
			batchDisplay.html("");
			currentDisplayedData = [];
			for(var i in oStorages){
				var uiItem = $(sTemplate),
					data = oStorages[i],
					oHierarchy = makeHierarchy(data.location);
				
				data["children"] = oHierarchy[0]["children"];
				uiItem.find(".location-name").html(data.name);
				//uiItem.find("img").attr("src", CGetProfileImage.getDefault(data.name));
				uiItem.find("img").attr("src", BASEURL+"assets/images/folder-circle-blue.png");
				uiItem.find(".qty-in-location").html(data.capacity).number(true, 2).append(" "+data.measure_name);
				uiItem.attr("storage-id", data.id);
				batchDisplay.append(uiItem);
				uiItem.data(data);
				uiItem.data("sQtyInLocationParent", uiItem.find(".qty-in-location").html());
				
				uiItem.single_double_click(
					function(){
						setActiveEvent($(this));
					},
					function(){
						selectHierarchy($(this));
					}
				);
			}

			oDisplayedData = {};
			oDisplayedData[0] = oStorages;
		}

		displayStorages(oStorages);

	}

	function _enterProductDetailsEvent(ui, record)
	{
		var data = ui.data(),
			btnUpdate = ui.find(".update-product-details"),
			btnEnter = ui.find(".enter-product-details"),
			btnEdit = ui.find(".item-edit"),
			btnCancelChanges = ui.find(".item-cancel-save-changes"),
			btnSaveChanges = ui.find(".item-save-changes"),
			oToUpdateBatch = {},

		enterEvent =  function(e){
			var uiParent = $(e.target).closest('.record-item'),
				recordData = uiParent.data(),
				uiCollapseIn = uiParent.find('.panel-collapse'),
				btnEnterThis = uiParent.find(".enter-product-details"),
				btnEditThis = uiParent.find(".item-edit"),
				btnUpdate = uiParent.find(".update-product-details"),
				btnCancelChangesThis = uiParent.find(".item-cancel-save-changes"),
				btnSaveChangesThis = uiParent.find(".item-save-changes"),
				uiAllGrayColored = uiParent.find(".gray-color"),
				sUom = recordData.product_unit_of_measure,
				uiEnterReceivingQty = uiParent.find(".received-quantity"),
				spanReceivingTotal = uiParent.find(".receiving-total"),
				receivingTotal = $.number(spanReceivingTotal.html(), 2),
				uiDiscrepancy = uiParent.find('.discrepancy'),
				iDiscrepancy = uiDiscrepancy.data().discrepancy,
				uiCauseDiscrepancy = uiParent.find('.cause-of-discrepancy'),
				uiReason = uiParent.find('.remarks-textbox'),
				sReason = uiReason.html(),
				uiSelectCause = $('<select class="select-cause"><option value="other">other</option><option value="na">n/a</option></select>'),
				uiInput = $("<input type='text' class='super-width-100px t-small f-left ' />"),
				uiAddBatchContainer = uiParent.find(".assignment-display .hide-method"),
				btnAddBatch = uiAddBatchContainer.find('[modal-target="add-batch"]'),
				uiSpanReceivingTotal = $("<span class='receiving-total'>0</span>"),
				uiSpanDiscrepancy = $('<span>'+iDiscrepancy+'</span'),
				uiTextArea = $("<textarea></textarea>"),
				bHasDiscrepancy = false,
				bHasBatches = false;
			

			btnEnterThis.hide();
			btnSaveChangesThis.show();
			btnEditThis.hide();
			btnCancelChangesThis.show();

			uiCollapseIn.addClass("in");		

			uiAllGrayColored.removeClass('gray-color').attr("gray-color", "true");


			uiAddBatchContainer.show();
			btnAddBatch.show();
			btnAddBatch.off("click").on("click", _addBatchEvent);


			if(recordData.hasOwnProperty("discrepancy")){
				if(Object.keys(recordData.discrepancy).length > 0){
					bHasDiscrepancy = true;
				}
			}

			if(recordData.hasOwnProperty("batch")){
				if(Object.keys(recordData.batch).length > 0){
					bHasBatches = true;
				}
			}
			
			switch(bHasDiscrepancy){
				case true :
					uiCauseDiscrepancy.html(recordData.discrepancy.cause);
					uiReason.html(recordData.discrepancy.remarks);
					uiEnterReceivingQty.html(recordData.discrepancy.received_quantity + " " +sUom);
				break;

				case false :
					uiCauseDiscrepancy.html("");
					uiDiscrepancy.html("<span>0.00</span> "+sUom);
					uiEnterReceivingQty.html("0.00 " +sUom);
					uiCauseDiscrepancy.html("<div class='select small'></div>");
					uiCauseDiscrepancy.find("div").html(uiSelectCause);
					uiSelectCause.transformDD();
					CDropDownRetract.retractDropdown(uiSelectCause);
					uiReason.html(uiTextArea);
				break;
			}

			switch(bHasBatches){
				case true :
					_displayBatches(recordData.id);
				break;

				case false :
				break;
			}

			uiSelectCause.off('change').on('change', function(){
				var uiThis = $(this),
					uiParent = uiThis.closest('.record-item'),
					uiTextArea = uiParent.find('textarea.text-area-reason'),
					sThisVal = uiThis.val();

				if(sThisVal == 'na'){
					uiTextArea.prop('disabled', true);
				}else{
					uiTextArea.prop('disabled', false);
				}
			});
		},

		cancelEvent = function(e){
			var uiParent = $(e.target).closest('.record-item'),
				btnEnterThis = uiParent.find(".enter-product-details"),
				btnEditThis = uiParent.find(".item-edit"),
				btnCancelChangesThis = uiParent.find(".item-cancel-save-changes"),
				btnSaveChangesThis = uiParent.find(".item-save-changes"),
				uiAllGrayColored = uiParent.find("[gray-color='true']"),
				sUom = uiParent.find('.qty-to-receive').data("data-uom"),
				recordData = uiParent.data(),
				uiCauseDiscrepancy = uiParent.find('.cause-of-discrepancy'),
				selectCause = uiParent.find("select.select-cause"),
				uiReason = uiParent.find('.remarks-textbox'),
				uiEnterReceivingQty = uiParent.find(".received-quantity"),
				sReason = uiReason.find("textarea").val(),
				receivingTotal = uiParent.find(".receiving-total"),
				uiDiscrepancy = uiParent.find(".discrepancy"),
				spanDiscrepancy =  uiDiscrepancy.find("span"),
				bHasDiscrepancy = false,
				bHasBatches = false;
			
			btnEnterThis.show();
			btnEditThis.show();
			

			if(recordData.hasOwnProperty("discrepancy")){
				if(Object.keys(recordData.discrepancy).length > 0){
					bHasDiscrepancy = true;
				}
			}

			if(recordData.hasOwnProperty("batch")){
				if(Object.keys(recordData.batch).length > 0){
					bHasBatches = true;
				}
			}
			
			switch(bHasDiscrepancy){
				case true :
					uiCauseDiscrepancy.html(recordData.discrepancy.cause);
					uiReason.html(recordData.discrepancy.remarks);
				break;

				case false :
					uiCauseDiscrepancy.html("");
					uiEnterReceivingQty.addClass("gray-color");
					uiDiscrepancy.attr("gray-color", "true").addClass("gray-color");
					uiDiscrepancy.html("<span gray-color='true' class='gray-color'>0.00</span> "+sUom);
					uiReason.html("");
				break;
			}

			switch(bHasBatches){
				case true :
					_displayBatches(recordData.id);
				break;

				case false :
				break;
			}

			

			btnCancelChangesThis.hide();
			btnSaveChangesThis.hide();

			uiAllGrayColored.addClass('gray-color');

			uiParent.find(".assignment-display .hide-method").hide();
		},

		completeProdDetailsEvent = function(e){
			var error = 0,
				uiItemContainer = $("#displayItemList"),
				uiItems = uiItemContainer.find("[prod-id]"),
				errorMessage = [],
				oProductsData = {};

			$.each(uiItems, function(){
				var uiThis = $(this),
					data = uiThis.data(),
					productName = data.product_name,
					oBatch = data.batch,
					uiReceiveQuantity = parseFloat(uiThis.find(".receiving-total").html()),
					uiDiscrepancy = uiThis.find(".discrepancy"),
					dataDiscrepancy = uiDiscrepancy.data("discrepancy"),
					uiSelectCause = uiThis.find("select.select-cause"),
					uiReason = uiThis.find("textarea.text-area-reason"),
					iLen = Object.keys(oProductsData).length;
				
				oProductsData[iLen] = {
					"receiving_item_record_id" : data.id,
					"product_id" : data.product_id
				};

				if(!oBatch){
					error++;
					errorMessage.push("Add Batch For -"+productName);
				}else{
					oProductsData[iLen]["batch"] = oBatch;
				}

				if(uiReceiveQuantity.val().trim().length == 0){
					error++;
					errorMessage.push("Add Received Quantity -"+productName);
				}else{
					oProductsData[iLen]["received_quantity"] = uiReceiveQuantity.val();
				}

				if(!dataDiscrepancy){
					error++;
					errorMessage.push("Add Discrepancy -"+productName);
				}else{
					if(dataDiscrepancy.status == "up" || dataDiscrepancy.status == "down"){
						if(uiReason.val().trim().length == 0){
							error++;
							errorMessage.push("Add Reason For Discrepancy -"+productName);
						}
					}else{
						var discrepancyValue = parseFloat(dataDiscrepancy.value);
						if(discrepancyValue > 0 && uiReason.val().trim().length == 0){
							error++;
							errorMessage.push("Add Reason For Discrepancy -"+productName);
						}
					}
					
					oProductsData[iLen]["discrepancy_type"] = dataDiscrepancy.status;
					oProductsData[iLen]["discrepancy_value"] = dataDiscrepancy.value;
					oProductsData[iLen]["discrepancy_reason"] = uiReason.val();
					oProductsData[iLen]["discrepancy_cause"] = uiSelectCause.val();
				}

				if(oItemReceivingBalance[data.id]){
					var parsedData = parseFloat(oItemReceivingBalance[data.id]);
					if(parsedData != 0){
						error++;
						errorMessage.push("Storage Assignment Ivalid -"+productName);
					}
				}else if(oItemReceivingBalance[data.id] != 0){
					error++;
					errorMessage.push("Storage Assignment Ivalid -"+productName);
				}

			});
			
			if(error > 0){
				$("body").feedback({title : "Invalid Input", message : errorMessage.join(","), type : "danger", icon : failIcon});
			}else{
				var uiModal = $('[modal-id="are-you-sure"]'),
					uiClose = uiModal.find(".close-me"),
					uiConfirm = uiModal.find("button.submit-btn");
				
				uiModal.addClass("showed");

				uiConfirm.off("click").on("click", function(){
					uiClose.click();
					_completeProductDetails(oProductsData);
				});

				uiClose.off("click").on("click", function(){
					uiModal.removeClass("showed");
				});
			}
		},

		saveProductDetails = function(e){
			var uiParent = $(e.target).closest('.record-item'),
				recordData = uiParent.data(),
				uiReceivedTotal = uiParent.find(".received-quantity"),
				uiDiscrepancy = uiParent.find(".discrepancy"),
				uiBatches = uiParent.find(".storage-assign-panel"),
				uiQtyToReceive = uiParent.find(".qty-to-receive"),
				qtyToReceive = parseFloat(uiQtyToReceive.data().quantity),
				receiveTotal =  0,
				error = 0,
				errorMessage = [];

			if(uiBatches.length == 0){
				error++;
				errorMessage.push("Must add batch");
			}

			if(!uiDiscrepancy.data().hasOwnProperty("discrepancy")){
				error++;
				errorMessage.push("Must set discrepancy");
			}

			if(!uiReceivedTotal.data().hasOwnProperty("receivingTotal")){
				error++;
				errorMessage.push("Must set received quantity");
			}else{
				receiveTotal = parseFloat(uiReceivedTotal.data().receivingTotal);
				if(receiveTotal != qtyToReceive){
					if(uiParent.find(".remarks-textbox textarea").val().trim().length == 0){
						error++;
						errorMessage.push("Must set remarks");	
					}
				}
			}


			if(error > 0){
				$("body").feedback({title : "Invalid values", message : errorMessage.join(","), type : "danger", icon : failIcon});
			}else{
				var oProductData = {},
					oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "receivingCompanyGoodsRecord"}),
					receivingId = oRecord.receivingId,
					data = uiParent.data(),
					productName = data.product_name,
					oBatch = data.batch,
					receivingTotal = parseFloat(uiReceivedTotal.data().receivingTotal),
					uiDiscrepancy = uiParent.find(".discrepancy"),
					dataDiscrepancy = uiDiscrepancy.data("discrepancy"),
					uiSelectCause = uiParent.find("select.select-cause"),
					uiReason = uiParent.find(".remarks-textbox textarea"),
					iLen = Object.keys(oProductData).length,
					uiModalConfirm = $('[modal-id="confirm-complete-product"]'),
					uiClose = uiModalConfirm.find(".close-me"),
					uiConfirm = uiModalConfirm.find("button.submit-btn"),
					uiModalCompleting = $('[modal-id="complete-single-product-details"]'),
					uiCompletingMessage = uiModalCompleting.find(".message");
				
				uiModalConfirm.addClass("showed");
			
				var sDiscrepancyValue = (dataDiscrepancy.value) ? dataDiscrepancy.value : "",
					sDiscrepancyType = (dataDiscrepancy.type) ? dataDiscrepancy.type : "",
					sDiscrepancyReason = (uiReason.val()) ? uiReason.val() : ""

				oProductData[iLen] = {
					"receiving_item_record_id" : data.id,
					"product_id" : data.product_id,
					"received_quantity" : receivingTotal,
					"batch" : oBatch,
					"discrepancy_value" : sDiscrepancyValue,
					"discrepancy_type" : sDiscrepancyType,
					"discrepancy_cause" : uiSelectCause.val(),
					"discrepancy_reason" : sDiscrepancyReason
				};

				uiConfirm.off("click").on("click", function(){
					uiClose.click();
					localStorage.setItem("itemKeepShowed", data.id);
					var oOptions = {
						type : "POST",
						data : {
							"receiving_id" : receivingId,
							"receiving_record" : JSON.stringify(oProductData)
						},
						url : BASEURL + "receiving/complete_product_details",
						returnType : "json",
						beforeSend: function(){
							uiModalCompleting.addClass('showed');
						},
						success : function(oResult) {
							if(oResult.status){
								uiCompletingMessage.html("Produuct details added");

								setTimeout(function(){
									uiModalCompleting.removeClass('showed');
								}, 1500);

								_displayItems(oResult.data[0].items, oResult.data[0]);

							}else{
								uiModal.removeClass('showed');
								$("body").feedback({title : "Invalid Input", message : oResult.join(","), type : "danger", icon : failIcon});
							}
						}
					}

					cr8v_platform.CconnectionDetector.ajax(oOptions);
				});

				uiClose.off("click").on("click", function(){
					uiModalConfirm.removeClass("showed");
				});
				
			}
		},

		updateCancelEvent = function(e){
			var uiParent = $(e.target).closest('.record-item'),
				oRecord = uiParent.data(),
				btnUpdate = uiParent.find(".update-product-details"),
				btnUpdateCancel = uiParent.find(".update-cancel-save-changes"),
				btnUpdateSaveChanges = uiParent.find(".update-save-changes"),
				btnEditThis = uiParent.find(".item-edit"),
				uiAddBatchContainer = uiParent.find(".assignment-display .hide-method"),
				btnAddBatch = uiAddBatchContainer.find('[modal-target="add-batch"]'),
				uiCauseDiscrepancy = uiParent.find('.cause-of-discrepancy'),
				uiReason = uiParent.find('.remarks-textbox'),
				uiTextarea = uiReason.find("textarea");

			btnUpdate.show();
			btnEditThis.show();

			btnUpdateSaveChanges.hide();
			btnUpdateCancel.hide();

			uiAddBatchContainer.hide();
			btnAddBatch.hide();

			uiReason.html(oRecord.discrepancy.remarks);
			uiCauseDiscrepancy.html(oRecord.discrepancy.cause);
			
			var sBatch = localStorage.getItem("toUpdateBatch"),
				oBatchData = JSON.parse(sBatch);
			
			oBatchPerItem[oRecord.id] = oBatchData;

			uiParent.data("batch", oBatchData);

			_displayBatches(oRecord.id);
			
			var uiBatches = uiParent.find(".storage-assign-panel");
			uiBatches.css({ "pointer-events" : "none" });
			
		},

		saveUpdateProductDetails = function(e){
			var uiParent = $(e.target).closest('.record-item'),
				recordData = uiParent.data(),
				btnEnterThis = uiParent.find(".enter-product-details"),
				btnEditThis = uiParent.find(".item-edit"),
				btnUpdate = uiParent.find(".update-product-details"),
				btnCancelChangesThis = uiParent.find(".item-cancel-save-changes"),
				btnSaveChangesThis = uiParent.find(".item-save-changes"),
				btnUpdateCancel = uiParent.find(".update-cancel-save-changes"),
				btnUpdateSaveChanges = uiParent.find(".update-save-changes"),
				uiRemarks = uiParent.find(".remarks-textbox textarea"),
				sRemarks = uiRemarks.val(),
				sUom = recordData.product_unit_of_measure,
				qtyToReceive = parseFloat(recordData.quantity);

			var error = 0,
				batchTotal = 0,
				arrErrorMsg = [];

			if(Object.keys(recordData.batch).length == 0){
				error++;
				arrErrorMsg.push("Must add batch");
			}

			for(var i in recordData.batch){
				var b = recordData.batch[i];

				batchTotal += parseFloat(b["batch_amount"]);
			}

			if(batchTotal != qtyToReceive){
				if(sRemarks.trim().length == 0){
					error++;
					arrErrorMsg.push("Must set remarks");
				}
			}


			if(error > 0){
				$("body").feedback({title : "Invalid Input", message : arrErrorMsg.join(","), type : "danger", icon : failIcon});
			}else{
				var uiModal = $('[modal-id="modal-update-product-details"]'),
					sMessage = uiModal.find(".message").html(),
					oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "receivingCompanyGoodsRecord"}),
					receivingId = oRecord.receivingId,
					uiReceiveQuantity = uiParent.find(".received-quantity"),
					receivingQuantity = uiReceiveQuantity.data("receivingTotal"),
					uiDiscrepancy = uiParent.find(".discrepancy"),
					discrepancyData = uiDiscrepancy.data("discrepancy"),
					uiCaseSelect = uiParent.find(".cause-of-discrepancy select");

				var oProductData = {
					"batch" : recordData.batch,
					"storage_assignment" : recordData.storage_assignment[0],
					"receiving_qty" : receivingQuantity,
					"discrepancy" : discrepancyData.value,
					"discrepancy_type" : discrepancyData.type,
					"cause" : uiCaseSelect.val(),
					"remarks" : sRemarks
				};

				//console.log(JSON.stringify(recordData.batch));

				var oOptions = {
						type : "POST",
						data : {
							"receiving_id" : receivingId,
							"receiving_product_details" : JSON.stringify(oProductData)
						},
						url : BASEURL + "receiving/update_product_details",
						returnType : "json",
						beforeSend: function(){
							uiModal.addClass('showed');
						},
						success : function(oResult) {
							if(oResult.status){
								uiModal.find(".message").html("Success!");
								localStorage.setItem("itemKeepShowed", recordData.id)
								setTimeout(function(){
									_displayItems(oResult.data[0].items, oResult.data[0]);
									setTimeout(function(){
										uiModal.removeClass('showed');
										uiModal.find(".message").html(sMessage);
									},500);
								}, 1000);
							}
						}
					}

				cr8v_platform.CconnectionDetector.ajax(oOptions);
			}
		},

		updateProductEvent = function(e){
			var uiParent = $(e.target).closest('.record-item'),
				recordData = uiParent.data(),
				btnEnterThis = uiParent.find(".enter-product-details"),
				btnEditThis = uiParent.find(".item-edit"),
				btnUpdate = uiParent.find(".update-product-details"),
				btnCancelChangesThis = uiParent.find(".item-cancel-save-changes"),
				btnSaveChangesThis = uiParent.find(".item-save-changes"),
				btnUpdateCancel = uiParent.find(".update-cancel-save-changes"),
				btnUpdateSaveChanges = uiParent.find(".update-save-changes"),
				sUom = recordData.product_unit_of_measure,
				uiEnterReceivingQty = uiParent.find(".received-quantity"),
				uiDiscrepancy = uiParent.find('.discrepancy'),
				iDiscrepancy = uiDiscrepancy.data().discrepancy,
				uiCauseDiscrepancy = uiParent.find('.cause-of-discrepancy'),
				uiReason = uiParent.find('.remarks-textbox'),
				sReason = uiReason.html(),
				uiSelectCause = $('<select class="select-cause"><option value="other">other</option><option value="na">n/a</option></select>'),
				uiSelectContainer = $('<div class="select medium"></div>'),
				uiInput = $("<input type='text' class='super-width-100px t-small f-left ' />"),
				uiAddBatchContainer = uiParent.find(".assignment-display .hide-method"),
				btnAddBatch = uiAddBatchContainer.find('[modal-target="add-batch"]'),
				uiSpanReceivingTotal = $("<span class='receiving-total'>0</span>"),
				uiSpanDiscrepancy = $('<span>'+iDiscrepancy+'</span'),
				uiTextArea = $("<textarea></textarea>"),
				uiBatches = uiParent.find(".storage-assign-panel"),
				bHasDiscrepancy = false,
				bHasBatches = false;

			localStorage.setItem("toUpdateBatch", JSON.stringify(recordData.batch));

			btnUpdate.hide();
			btnEditThis.hide();

			btnUpdateSaveChanges.show();
			btnUpdateCancel.show();

			btnUpdateSaveChanges.off("click").on("click", saveUpdateProductDetails)

			uiAddBatchContainer.show();
			btnAddBatch.show();

			uiBatches.css({ "pointer-events" : "" });

			btnUpdateCancel.off('click').on('click', updateCancelEvent);

			btnAddBatch.off("click").on("click", _addBatchEvent);

			uiTextArea.val(sReason);
			uiReason.html(uiTextArea);

			uiSelectContainer.html(uiSelectCause);
			uiCauseDiscrepancy.html(uiSelectContainer);
			uiSelectCause.transformDD();

		},

		editItemCancelEvent = function(e){
			var uiParent = $(e.target).closest('.record-item'),
				recordData = uiParent.data(),
				btnEditThis = uiParent.find(".item-edit"),
				btnCancel = uiParent.find(".item-edit-cancel-changes"),
				uiLoadingMethod = uiParent.find(".loading-method"),
				uiQtyToReceive = uiParent.find(".qty-to-receive"),
				classList = uiLoadingMethod.data("classList"),
				btnSave = uiParent.find(".item-edit-save-changes"),
				btnVisible = uiParent.find("[btn-visible='true']");

			btnCancel.hide();
			btnSave.hide();

			btnVisible.removeAttr('btn-visible');
			btnVisible.show();
			btnEditThis.show();

			uiLoadingMethod.attr("class", classList);
			uiLoadingMethod.html("By "+recordData.loading_method);

			uiQtyToReceive.html(recordData.quantity+" "+recordData.product_unit_of_measure);
			uiQtyToReceive.parent().children(".width-15percent").removeClass("width-15percent").addClass("width-20percent");
		},

		saveEditItem = function(e){
			var uiParent = $(e.target).closest(".record-item"),
				recordData = uiParent.data(),
				btnCancel = uiParent.find(".item-edit-cancel-changes"),
				btnSave = uiParent.find(".item-edit-save-changes"),
				uiModal = $('[modal-id="modal-update-product-details"]'),
				sMessage = uiModal.find(".message").html(),
				oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "receivingCompanyGoodsRecord"}),
				receivingId = oRecord.receivingId,
				sLoadingMethod = uiParent.find('[name="by-piece-bulk"]:checked').attr("data-value"),
				oItemData = {
					"record_id" : recordData.id,
					"item_id" : recordData.product_id,
					"item_name" : recordData.product_name,
					"item_sku" : recordData.product_sku,
					"item_loading_method" : sLoadingMethod,
					"item_unit_of_measure" : uiParent.find("[unit-of-measure]").val(),
					"item_qty_unit_of_measure" : uiParent.find('.quantity-unit-of-measure').val(),
					"item_qty_by_piece" : uiParent.find('.quantity-by-piece').val()
				};
			
			var oOptions = {
					type : "POST",
					data : {
						"receiving_id" : receivingId,
						"receiving_item_data" : JSON.stringify(oItemData)
					},
					url : BASEURL + "receiving/update_receiving_item",
					returnType : "json",
					beforeSend: function(){
						btnCancel.prop("disabled", true);
						btnSave.prop("disabled", true);
						uiModal.addClass('showed');
					},
					success : function(oResult) {
						if(oResult.status){
							uiModal.find(".message").html("Success!");
							localStorage.setItem("itemKeepShowed", recordData.id)
							setTimeout(function(){
								_displayItems(oResult.data[0].items, oResult.data[0]);
								setTimeout(function(){
									uiModal.removeClass('showed');
									uiModal.find(".message").html(sMessage);
								},500);
							}, 1000);
						}
					}
				}

			cr8v_platform.CconnectionDetector.ajax(oOptions);
		},

		editItemEvent = function(e){
			var uiParent = $(e.target).closest('.record-item'),
				recordData = uiParent.data(),

				recordQtyToReceive = recordData.quantity,
				recordUOM = recordData.unit_of_measure_id,
				recordLoadingMethod = recordData.loading_method,
				
				uiCollapseIn = uiParent.find(".panel-collapse"),
				btnEditThis = uiParent.find(".item-edit"),
				btnCancel = uiParent.find(".item-edit-cancel-changes"),
				btnSave = uiParent.find(".item-edit-save-changes"),
				btnVisible = (uiParent.find(".update-product-details").is(":visible")) ? uiParent.find(".update-product-details") : uiParent.find(".enter-product-details"),
				uiLoadingMethod = uiParent.find(".loading-method"),
				uiQtyToReceive = uiParent.find(".qty-to-receive"),
				arrLoadingClasses = uiLoadingMethod[0].classList,
				loadingMethodLeft = $('<div class="f-left width-15percent"> <input type="radio" class="display-inline-mid width-40px default-cursor by-bulk" data-value="bulk" name="by-piece-bulk"> <label for="bulk" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>	</div>'),
				loadingMethodRight = $('<div class="f-left width-15percent margin-left-10">	<input type="radio" class="display-inline-mid width-40px default-cursor by-piece" data-value="piece" name="by-piece-bulk"> <label for="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Piece</label> </div>');

			var sTemplate = '<div class="f-left text-left font-0 hide-receiving" style="display: block;">'+
						       '<input class="display-inline-mid width-70px height-26 t-small quantity-unit-of-measure" name="bag-or-bulk" type="text">'+
						        '<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">'+
						        	'<select unit-of-measure style="display: inline-block; padding: 3px 4px; width: 50px;"> </select>'+
								'</p>'+
						        '<input class="display-inline-mid width-70px height-26 t-small quantity-by-piece" name="bag-or-bulk" type="text">'+
						        '<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">Piece</p>'+
						        '<div class="clear"></div>'+
						    '</div>',
				uiHtmlToReceive = $(sTemplate);

			var uiSelect = uiHtmlToReceive.find("select"),
				uiInputQty = uiHtmlToReceive.find(".quantity-unit-of-measure"),
				uiInputPiece = uiHtmlToReceive.find(".quantity-by-piece"),
				meas = {};

				uiCollapseIn.addClass("in");
			
			uiInputQty.number(true, 0);
			uiInputPiece.number(true, 0);
			uiInputQty.val(recordQtyToReceive);

			for(var i in oMeasurements){
				meas = {};
				meas = oMeasurements[i];
				if(meas.name != 'pc'){
					if(recordUOM == meas.id){
						uiSelect.append("<option selected value='"+meas.id+"'>"+meas.name+"</option>");
					}else{
						uiSelect.append("<option value='"+meas.id+"'>"+meas.name+"</option>");
					}	
				}else{
					if(recordUOM == meas.id){
						uiInputQty.val("");
						uiInputPiece.val(recordQtyToReceive);
					}
				}
				
			}
			
			uiQtyToReceive.html(uiHtmlToReceive);

			btnCancel.show();
			btnSave.show();
			
			btnSave.off("click").on("click", saveEditItem);

			btnVisible.hide();
			btnEditThis.hide();

			uiLoadingMethod.data("classList", arrLoadingClasses.toString());
			uiLoadingMethod.attr("class", "loading-method");
			uiLoadingMethod.html(loadingMethodLeft);
			uiLoadingMethod.append(loadingMethodRight);
			uiLoadingMethod.append("<div class='clear'></div>");

			btnCancel.off('click').on('click', editItemCancelEvent);
			btnVisible.attr('btn-visible', 'true');


			var qtyParent = uiQtyToReceive.parent(),
				qtyLabelContainer = qtyParent.children("p.f-left:nth-child(3)"),
				radios = uiParent.find('[name="by-piece-bulk"]'),
				radioSelected = uiParent.find("[data-value='"+recordLoadingMethod+"']");

			uiQtyToReceive.removeClass('width-25percent');
			qtyLabelContainer.removeClass('width-20percent').addClass('width-15percent');
			
			radios.off('change').on('change', function(){
				var val = $(this).attr("data-value");
				if(val == 'bulk'){
					uiQtyToReceive.find(".quantity-unit-of-measure").prop("disabled", false);
					uiQtyToReceive.find("select").prop("disabled", false);
					uiQtyToReceive.find(".quantity-by-piece").prop("disabled", true);
				}else{
					uiQtyToReceive.find(".quantity-unit-of-measure").prop("disabled", true);
					uiQtyToReceive.find("select").prop("disabled", true);
					uiQtyToReceive.find(".quantity-by-piece").prop("disabled", false);
				}
			});

			radioSelected.prop("checked", true);
			radioSelected.trigger("change");
		};

		if(data.hasOwnProperty("batch")){
			if(Object.keys(data.batch).length > 0 && Object.keys(data.discrepancy).length > 0 ){
				btnUpdate.show();
				btnEnter.hide();
			}
		}
		
		btnUpdate.off("click").on("click", updateProductEvent);
		btnSaveChanges.off('click').on('click', saveProductDetails);
		btnEnter.off("click").on("click", enterEvent);
		btnCancelChanges.off("click").on("click", cancelEvent);
		btnEdit.off("click").on("click", editItemEvent);

		if(record.status == '1'){
			btnUpdate.hide();
			btnEdit.hide();
		}
	}


	function _completeProductDetails(oProductsData)
	{
		var oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "receivingCompanyGoodsRecord"}),
			receivingId = oRecord.receivingId,
			uiModal = $('[modal-id="complete-product-details"]');

		var oOptions = {
			type : "POST",
			data : {
				"receiving_id" : receivingId,
				"receiving_record" : JSON.stringify(oProductsData)
			},
			url : BASEURL + "receiving/complete_product_details",
			returnType : "json",
			beforeSend: function(){
				uiModal.addClass('showed');
			},
			success : function(oResult) {
				if(oResult.status){
					uiModal.removeClass('showed');
					$("div[modal-id='product-details']").addClass('showed');
					$("div[modal-id='product-details']").find(".close-me").off('click').on('click', function(){
						$("div[modal-id='product-details']").removeClass('showed');
					});
					$("div[modal-id='product-details']").find(".modal-footer button").off('click').on('click', function(){
						window.location.reload();
					});
				}else{
					uiModal.removeClass('showed');
					$("body").feedback({title : "Invalid Input", message : oResult.join(","), type : "danger", icon : failIcon});
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _displayItems(oItems, oMainRecord)
	{
		var uiContainer = $("#displayItemList"),
			uiTemplate = $('.item-template'),
			status = oMainRecord.status,
			itemKeepShowed = localStorage.getItem("itemKeepShowed");
			
		for(var i in oItems){
			var item = oItems[i];
			
			var uiCloneTemplate = uiTemplate.clone();
			uiCloneTemplate.removeClass("item-template").show();
			uiCloneTemplate.data(item);
			uiCloneTemplate.find(".product-name-sku").html(" SKU #"+item.product_sku+" - "+item.product_name);
			uiCloneTemplate.find(".item-img").css("background-image", "url("+item.product_image+")");
			uiCloneTemplate.find(".vendor-name").html(item.vendor_name);
			uiCloneTemplate.find(".price").html(item.product_unit_price+" php");
			uiCloneTemplate.find(".loading-method").html("By "+item.loading_method);
			uiCloneTemplate.find(".qty-to-receive").html($.number(item.quantity, 0)+" "+item.product_unit_of_measure).data({
				'data-uom' : item.product_unit_of_measure,
				'quantity' : item.quantity
			});

			uiCloneTemplate.attr("prod-id", item.id);

			if($(".record-item[prod-id='"+item.id+"']").length > 0){
				$(".record-item[prod-id='"+item.id+"']").replaceWith(uiCloneTemplate);
			}else{
				uiContainer.append(uiCloneTemplate);
			}

			oItemReceivingBalance[item.id] = item.quantity;
			oBatchPerItem[item.id] = {};

			if(item.hasOwnProperty("storage_assignment")){
				if(Object.keys(item.storage_assignment).length > 0){
					for(var x in item.storage_assignment){
						var storageAssignment = item.storage_assignment[x],
							batch = storageAssignment.batch;

						for(var n in batch){
							var recordBatch = oBatchPerItem[item.id],
							len = Object.keys(recordBatch).length;
							
							oBatchPerItem[item.id][len] = {
								"location" : batch[n]["location"][Object.keys(batch[n]["location"]).length - 1],
								"item_id" : item.product_id,
								"batch_name" : batch[n]["name"],
								"batch_amount" : batch[n]["quantity"],
								"uom" : item.product_unit_of_measure,
								"uom_id" : item.unit_of_measure_id,
								"hierarchy" : batch[n]["location"]
							};
						}
					}

					
					if(Object.keys(item.discrepancy).length > 0){
						_displayBatches(item.id, true);
					}else{
						_displayBatches(item.id);
					}

				}
			}

			//console.log(item);
			if(Object.keys(oBatchPerItem[item.id]).length > 0){
				uiCloneTemplate.find(".gray-color").removeClass("gray-color");
			}
			
			uiCloneTemplate.find('[modal-target="add-batch"]').hide();
			uiCloneTemplate.find(".received-quantity").html(item.discrepancy.received_quantity);
			//uiCloneTemplate.find(".discrepancy").html(item.discrepancy.discrepancy_quantity);
			uiCloneTemplate.find(".cause-of-discrepancy").html(item.discrepancy.cause);
			uiCloneTemplate.find(".remarks-textbox").html(item.discrepancy.remarks);

			_enterProductDetailsEvent(uiCloneTemplate, oMainRecord);


			var aCollpase = uiCloneTemplate.find("a.colapsed");
			
			aCollpase.off("click").on("click", function(e){
				e.preventDefault();
				var panelHeading = $(e.target).closest(".panel-heading"),
					panelCollapse = panelHeading.siblings(".panel-collapse"),
					panelBody = panelCollapse.find(".panel-body"),
					ph = panelBody.outerHeight();

				if(panelCollapse.hasClass("in")){
					panelHeading.find("h4").addClass("active")
				}
				
				//panelHeading.css({height:ph});

				if(panelCollapse.hasClass("in")){
					panelHeading.find("h4").removeClass("active");
					panelHeading.find(".fa").removeClass("fa-caret-down").addClass("fa-caret-right");
					panelCollapse.removeClass("in");
				}else{
					panelHeading.find("h4").addClass("active");
					panelHeading.find(".fa").removeClass("fa-caret-right").addClass("fa-caret-down");
					panelCollapse.addClass("in");
				}

				setTimeout(function(){
					panelCollapse.removeAttr("style")
				},500);
			});

			if(item.id != itemKeepShowed){
				aCollpase.trigger('click');
				localStorage.removeItem("itemKeepShowed");
			}

			if(Object.keys(item.discrepancy).length > 0){
				uiCloneTemplate.find(".exclamation-mark").hide();
				uiCloneTemplate.find(".check-mark").show();
			}

			if((parseInt(i) + 1) == Object.keys(oItems).length){
				_checkIfItemsDetailsComplete(true, 1000);
				var scrollTo = localStorage.getItem("scrollTo");
				if(scrollTo !== null){
					var uiItemsCreated = $(".record-item:not(.item-template)"),
						errorFind = 0,
						firstIncompleteUi = [];

					$.each(uiItemsCreated, function(){

						if($(this).find(".storage-assign-panel").length == 0){
							errorFind++;
						}

						var uiReceiveQuantity = $(this).find(".received-quantity"),
							uiDiscrepancy = $(this).find(".discrepancy");
						if(uiReceiveQuantity.data() == null ||  uiReceiveQuantity.data().receivingTotal == 0){
							errorFind++;
						}

						if(!uiDiscrepancy.data()){
							errorFind++;
						}
						
						if(errorFind > 0){
							firstIncompleteUi = $(this);
							return false;
						}

					});
					
					firstIncompleteUi.find("a.colapsed").click();
					setTimeout(function(){
						$('html,body').animate({
				          scrollTop: (firstIncompleteUi.offset().top) - 200
				        }, 1000);
					},1000);

					localStorage.removeItem("scrollTo");
				}
			}

			if(status == "1"){
				uiCloneTemplate.find(".exclamation-mark").remove();
				uiCloneTemplate.find(".check-mark").remove();
				uiCloneTemplate.find(".button-holder").html("");
			}

		}

		
		console.log("oMainRecord", oMainRecord);
		console.log("oBatchPerItem", oBatchPerItem);
		console.log("oItemReceivingBalance", oItemReceivingBalance);

	}

	function _checkIfItemsDetailsComplete(bTime, time)
	{
		var uiItems = $(".record-item:not(.item-template)"),
			error = 0;
		
		var action = function(){
			$.each(uiItems, function(){

				if($(this).find(".storage-assign-panel").length == 0){
					error++;
				}

				var uiReceiveQuantity = $(this).find(".received-quantity"),
					uiDiscrepancy = $(this).find(".discrepancy");
				if(uiReceiveQuantity.data() == null ||  uiReceiveQuantity.data().receivingTotal == 0){
					error++;
				}

				if(!uiDiscrepancy.data()){
					error++;
				}
			});

			if(error == 0){
				var btn = $(".semi-main").find(".complete-receiving-record");
				btn.removeAttr('disabled');

				btn.off("click").on("click", _completeRecord);
			}
		}
		
		if(bTime){
			setTimeout(function(){
				action();
			}, time);
		}else{
			action();
		}
	}

	function _completeRecord(e){
		var uiModal = $('[modal-id="complete-receiving-modal"]'),
			uiModalCompleting = $('[modal-id="completing-receiving-modal"]'),
			close = uiModal.find(".close-me"),
			submit = uiModal.find(".submit-btn");

		close.off("click").on("click", function(){
			uiModal.removeClass('showed');
		});

		submit.off("click").on("click", function(){
			var oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "receivingCompanyGoodsRecord"}),
				receivingId = oRecord.receivingId;
			var oOptions = {
				type : "POST",
				data : { "receiving_id" : receivingId},
				url : BASEURL + "receiving/complete_record",
				beforeSend : function(){
					uiModal.removeClass('showed');
					uiModalCompleting.addClass('showed');
				},
				success : function(oResult) {
					if(oResult.status){
						uiModalCompleting.find(".message").html("Success, reloading..");
						setTimeout(function(){
							window.location.reload();
						},500);
					}
				}
			}
			cr8v_platform.CconnectionDetector.ajax(oOptions);
		});

		uiModal.addClass('showed');
	}

	function _displayNotes(oNotes, oRecord)
	{
		var uiContainer = $("#noteContainer"),
			uiTemplate = $('.note-template');

		for(var i in oNotes){
			var note = oNotes[i],
				uiCloneTemplate = uiTemplate.clone();

			uiCloneTemplate.find(".subject").html("Subject : "+note.subject);
			uiCloneTemplate.find(".message").html(note.note);
			uiCloneTemplate.find(".datetime").html("Date/Time : "+note.date_formatted);
			uiCloneTemplate.removeClass("note-template").show();
			uiContainer.append(uiCloneTemplate);
		}

		setTimeout(function(){
			$('[modal-id="generate-view"]').removeClass('showed');
		},500);

		_addNotesEvent();
	}

	function _itemsEvent(sRequest)
	{
		var methods = {
			itemTemplate : function(){
				
				var sHtml = '<div class="border-full padding-all-20 item-list">'+
				'				<div class="bggray-white height-190px">'+
				'					<div class="height-100percent display-inline-mid width-230px">'+
				'						<img src="../assets/images/holcim.jpg" alt="" class="height-100percent width-100percent img-holder">'+
				'					</div>'+
				'					<div class="display-inline-top width-75percent padding-left-10">'+
				'						<div class="padding-all-15 bg-light-gray">'+
				'							<p class="font-16 font-bold f-left prod-sku-name "></p>'+
				'							<div class="f-right width-20px margin-left-10">'+
				'								<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor close-img">'+
				'							</div>'+
				'							<div class="clear"></div>'+
				'						</div>'+
				'						<div class="padding-all-15 text-left ">'+
				'							<p class="f-left no-margin-all width-15percent font-500">Vendor</p>'+
				'							<p class="f-left no-margin-all width-30percent"><input type="text" class="vendor-name isplay-inline-mid t-medium" style="width:170px !important;" placeholder="Vendor Name..." /></p>'+
				'							<p class="f-left no-margin-all width-20percent font-500">Qty in Inventory</p>'+
				'							<p class="f-left no-margin-all width-20percent"> no record </p>							'+
				'							<div class="clear"></div>'+
				'						</div>'+
				'						<div class="padding-all-15 text-left bg-light-gray">'+
				'							<p class="f-left no-margin-all width-20percent font-500">Unit Price</p>'+
				'							<p class="f-left no-margin-all width-25percent">100.00 Php</p>'+
				'							<p class="f-left no-margin-all width-20percent font-500">Loading Method</p>'+
				'							<div class="hide-receiving" style="display: block;">'+
				'								<div class="f-left width-15percent">'+
				'									<input type="radio" class="display-inline-mid width-40px default-cursor by-bulk" checked data-value="bulk">'+
				'									<label for="bulk" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>'+
				'								</div>'+
				'								<div class="f-left width-15percent margin-left-10">	'+
				'									<input type="radio" class="display-inline-mid width-40px default-cursor by-piece" data-value="piece">'+
				'									<label for="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Piece</label>'+
				'								</div>'+
				'							</div>'+
				'							<div class="clear"></div>'+
				'						</div>'+

				'						<div class="padding-all-15 text-left">'+
				'							<p class="f-left no-margin-all width-20percent font-500">Description</p>'+
				'							<p class="f-left no-margin-all width-25percent prod-description"> </p>'+
				'							<p class="f-left no-margin-all width-15percent font-500">Qty to Receive</p>'+
				'							<div class="width-40percent f-left text-left font-0 hide-receiving" style="display: block;">'+
				'								<input type="text" class="display-inline-mid width-70px height-26 t-small quantity-unit-of-measure" name="bag-or-bulk" >'+
				'								<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10"><select unit-of-measure></select></p>'+
				'								<input type="text" class="display-inline-mid width-70px height-26 t-small quantity-by-piece" name="bag-or-bulk" >'+
				'								<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10 ">Piece</p>'+
				'								<div class="clear"></div>'+
				'							</div>'+
				'						</div>'+
				'						<div class="clear"></div>'+
				'					</div>'+
				'				</div>'+
				'			</div>';
					

				return sHtml;
			},
			displayToSelect : function(oData, bShowResult){
				var oTimeOut = {},
					uiSelect = $('#selectItem'),
					uiParent = uiSelect.parent(),
					uiSibling = uiParent.find('.frm-custom-dropdown');
				uiSibling.remove();

				uiSelect.removeClass("frm-custom-dropdown-origin");

				uiSelect.html("");

				for(var i in oData){
					var sHtml = '<option value="'+oData[i]['id']+'">#'+oData[i]['sku']+' '+oData[i]['name']+'</option>';
					uiSelect.append(sHtml);
				}

				uiSelect.transformDD();

				CDropDownRetract.retractDropdown(uiSelect);




				var uiInput = uiParent.find('input.dd-txt');

				if(bShowResult){
					uiParent.find('.frm-custom-icon').click();
					uiInput.focus();
				}

				uiInput.off("keyup").on("keyup", function(e){
					var val = $(this).val();
					uiInput.attr("value",val);
					
					clearTimeout(oTimeOut);

					if(val.length > 0 && e.keyCode != 8 && e.keyCode != 48){
						oTimeOut = setTimeout(function(){
							methods.search(val, true);
						}, 800);
					}

				});
			},
			getItem : function(id){
				for(var i in oItems){
					if(oItems[i]["id"] == id){
						return oItems[i];
					}
				}
			},
			search : function(sKey, bShowResult){
				var oOptions = {
					type : "POST",
					data : {
						key : sKey,
						limit : 30,
						sorting : "asc",
						sort_field : "name"
					},
					url : BASEURL + "products/search",
					returnType : "json",
					success : function(oResult) {
						if(!bFirstLoadedItems){
							oFirstLoadedItems = oResult.data;
							bFirstLoadedItems = true;
						}
						oItems = oResult.data;
						if(Object.keys(oFirstLoadedItems).length == 0 && bFirstLoadedItems){
							$('div[modal-id="redirect-product-management"]').addClass("showed");
							setTimeout(function(){
								window.location.href = BASEURL + "products/add_product";
							},1000);
							return false;
						}
						methods.displayToSelect(oItems, bShowResult);
					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);
			},
			getUniofMeasure : function(callBack){
				var oOptions = {
					type : "POST",
					data : { data : "none" },
					url : BASEURL + "products/get_unit_of_measure",
					returnType : "json",
					success : function(oResult) {
						if(typeof callBack == 'function'){
							callBack(oResult.data);
						}
					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);
			},
			makeid : function()
			{
			    var text = "";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			    for( var i=0; i < 5; i++ )
			        text += possible.charAt(Math.floor(Math.random() * possible.length));

			    return text;
			},
			findSameInstance: function(id, ui){
				var uiSameItem = $("[item-list-id='"+id+"']"),
					radioSelected = "",
					selectMeasure = "";

				if(uiSameItem.length > 0){
					$.each(uiSameItem, function(i, elem) {
						var uiSelect = $(this).find("[unit-of-measure]");
						uiSelect.on("change", function(){
							var sThisVal = $(this).val(),
								uiParent = $(this).closest('[item-list-id]'),
								sItemId = uiParent.attr('item-list-id'),
								uiSameInstance = $('[item-list-id="'+sItemId+'"]');

							$.each(uiSameInstance, function(){
								$(this).find("[unit-of-measure]").val(sThisVal);
							});
						});

						$(this).find("input[type='radio'].by-bulk").on("click", function(){
							var sThisVal = $(this).val(),
								uiParent = $(this).closest('[item-list-id]'),
								sItemId = uiParent.attr('item-list-id'),
								uiSameInstance = $('[item-list-id="'+sItemId+'"]');

							$.each(uiSameInstance, function(){
								var uiParent = $(this).closest('div.item-list'),
									inputQtyByPiece = uiParent.find("input.quantity-by-piece"),
									inputyQtyMeasure = uiParent.find("input.quantity-unit-of-measure"),
									radioBulk = uiParent.find("input[type='radio'].by-bulk"),
									radioPiece = uiParent.find("input[type='radio'].by-piece"),
									selectMeasure = uiParent.find("[unit-of-measure]");

								radioBulk.prop("checked", true);
								inputQtyByPiece.prop("disabled", true);
								inputyQtyMeasure.prop("disabled", false);
								selectMeasure.prop("disabled", false);
							});
						});

						$(this).find("input[type='radio'].by-piece").on("click", function(){
							var sThisVal = $(this).val(),
								uiParent = $(this).closest('[item-list-id]'),
								sItemId = uiParent.attr('item-list-id'),
								uiSameInstance = $('[item-list-id="'+sItemId+'"]');

							$.each(uiSameInstance, function(){
								var uiParent = $(this).closest('div.item-list'),
									inputQtyByPiece = uiParent.find("input.quantity-by-piece"),
									inputyQtyMeasure = uiParent.find("input.quantity-unit-of-measure"),
									radioBulk = uiParent.find("input[type='radio'].by-bulk"),
									radioPiece = uiParent.find("input[type='radio'].by-piece"),
									selectMeasure = uiParent.find("[unit-of-measure]");

								radioPiece.prop("checked", true);
								inputyQtyMeasure.prop("disabled", true);
								inputQtyByPiece.prop("disabled", false);
								selectMeasure.prop("disabled", true);

							});
						});

						if(i == 0){
							radioSelected = $(this).find("input[type='radio']:checked").attr("data-value");
							selectMeasure = $(this).find("[unit-of-measure]").val();
						}
					});

					ui.find("[unit-of-measure]").val(selectMeasure);
					ui.find("input[type='radio'][data-value='"+radioSelected+"']").click();
				}
			},
			oItems : function(){

			},
			addItem : function(){
				var uiAddItemBtn = $("#addItem"),
					uiSelect = $('#selectItem'),
					uiListContainer = $("#productList");

				uiAddItemBtn.on("click", function(){
					if(uiSelect.val().trim().length > 0){
						var oItem = methods.getItem(uiSelect.val()),
							uiTemplate = $(methods.itemTemplate()),
							oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
							sRandomID = methods.makeid();

						uiTemplate.data({
							"itemId" : oItem.id,
							"itemName" : oItem.name,
							"itemSku" : oItem.sku
						});
						uiTemplate.attr("item-list-id", oItem.id);
						uiTemplate.find(".img-holder").attr("src", oItem.image);
						uiTemplate.find("input[type='radio'].by-bulk").attr("name", "by-piece-bulk-"+sRandomID);
						uiTemplate.find("input[type='radio'].by-bulk").on("click", function(){
							var uiParent = $(this).closest('div.item-list'),
								inputyQtyMeasure = uiParent.find("input.quantity-unit-of-measure"),
								inputQtyByPiece = uiParent.find("input.quantity-by-piece"),
								selectMeasure = uiParent.find("[unit-of-measure]");

							inputQtyByPiece.prop("disabled", true);
							inputyQtyMeasure.prop("disabled", false);
							selectMeasure.prop("disabled", false);

						});

						uiTemplate.find("input[type='radio'].by-piece").attr("name", "by-piece-bulk-"+sRandomID);
						uiTemplate.find("input[type='radio'].by-piece").on("click", function(){
							var uiParent = $(this).closest('div.item-list'),
								inputyQtyMeasure = uiParent.find("input.quantity-unit-of-measure"),
								inputQtyByPiece = uiParent.find("input.quantity-by-piece"),
								selectMeasure = uiParent.find("[unit-of-measure]");

							inputQtyByPiece.prop("disabled", false);
							inputyQtyMeasure.prop("disabled", true);
							selectMeasure.prop("disabled", true);

						});

						uiTemplate.find(".prod-description").html(oItem.description);
						uiTemplate.find(".prod-sku-name ").html("SKU #"+oItem.sku+" - "+oItem.name);
						uiTemplate.find(".close-img").on("click", function(e){
							var uiParent = $(e.target).closest('.border-full'),
								sItemId = uiParent.data("itemId");
							uiParent.remove();
						});

						for(var i in oUnitOfMeasures){
							if(oUnitOfMeasures[i]['name'].toLowerCase() != 'pc'){
								var shtml = '<option value="'+oUnitOfMeasures[i]['id']+'">'+oUnitOfMeasures[i]['name']+'</option>';
								uiTemplate.find("[unit-of-measure]").append(shtml);
							}
						}

						uiTemplate.find("[unit-of-measure]").show().css({
							"padding" : "3px 4px",
							"width" :  "50px"
						});
						// uiTemplate.find("[unit-of-measure]").transformDD();

						uiTemplate.find('.quantity-unit-of-measure').number(true, 2);
						uiTemplate.find('.quantity-by-piece').number(true, 0);

						uiListContainer.append(uiTemplate);

						methods.findSameInstance(oItem.id, uiTemplate);
					}
				});
			}
		};
		
		if(!sRequest){
			methods.getUniofMeasure(function(oData){
				cr8v_platform.localStorage.set_local_storage({
					"name" : "unit_of_measures",
					"data" : oData
				});
			});
			methods.search("");
			methods.addItem();
		}

		if(sRequest == "getItems"){
			var uiListOfItems = $("#productList .item-list"),
				oData = {};
			$.each(uiListOfItems, function() {
				var uiThis = $(this),
					sLoadingMethod = uiThis.find("input[type='radio']:checked").attr("data-value");
				oData[Object.keys(oData).length] = {
					"item_id" : uiThis.data("itemId"),
					"item_name" : uiThis.data("itemName"),
					"item_sku" : uiThis.data("itemSku"),
					"item_loading_method" : sLoadingMethod,
					"item_unit_of_measure" : uiThis.find("[unit-of-measure]").val(),
					"item_qty_unit_of_measure" : uiThis.find('.quantity-unit-of-measure').val(),
					"item_qty_by_piece" : uiThis.find('.quantity-by-piece').val(),
					"item_vendor_name" : uiThis.find('.vendor-name').val()
				};
			});

			return oData;
		}
	}

	function _addNotesEvent(){
		var uiBtnAddNote = $("#confirmNote"),
			uiNoteSubject = $("#noteSubject"),
			uiNoteMessage = $("#noteMessage");

		var noteTemplate = function(){

				var sHtml = '<div class="border-full padding-all-10 margin-left-18 margin-top-10 note-list">'+
				'		<div class="border-bottom-small border-gray padding-bottom-10">'+
				'			<p class="f-left font-14 font-400 subject"></p>'+
				'			<p class="f-right font-14 font-400 date-time"></p>'+
				'			<div class="clear"></div>'+
				'		</div>'+
				'		<p class="font-14 font-400 no-padding-left padding-all-10 message"> </p>'+
				'		<a href="" class="f-right padding-all-10  font-400"> </a>'+
				'		<div class="clear"></div>'+
				'	</div>';
	
			return sHtml;
		}

		uiBtnAddNote.click(function(){

			var uiContainer = $("#noteContainer"),
				subj = uiNoteSubject.val(),
				message = uiNoteMessage.val();

			if(subj.trim().length > 0 && message.trim().length){
				var oOptions = {
					type : "POST",
					data : { data : "none" },
					url : BASEURL + "receiving/get_current_date_time",
					returnType : "json",
					success : function(oResult) {
						var uiTemplate = $(noteTemplate());

						uiTemplate.find(".subject").html("Subject : "+ subj);
						uiTemplate.find(".subject").data("subject", subj);
						uiTemplate.find(".message").html(message);
						uiTemplate.find(".message").data("message", message);
						uiTemplate.find(".date-time").html("Date/Time : "+oResult.data.date_formatted);
						uiTemplate.find(".date-time").data("datetime", oResult.data.date);

						uiContainer.append(uiTemplate);
						uiTemplate.addClass('new-note');

						uiNoteSubject.val("");
						uiNoteMessage.val("");

						$('.modal-close.close-me:visible').click();


						if(window.location.href.indexOf("view_company_goods") > -1)
						{
							var oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "receivingCompanyGoodsRecord"}),
								receivingId = oRecord.receivingId,
								oNotes = _getNotes();
							var oOptions = {
								type : "POST",
								data : { receiving_id : receivingId, notes : JSON.stringify(oNotes) },
								url : BASEURL + "receiving/add_notes_to_receiving",
								returnType : "json",
								success : function(oResult) {
								}
							}

							cr8v_platform.CconnectionDetector.ajax(oOptions);
						}
					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);
			}

		});
	}

	function _getNotes(){
		var oData = {},
			uiNotes = $("#noteContainer .note-list.new-note");

		$.each(uiNotes, function(){
			var uiThis = $(this);
			oData[Object.keys(oData).length] = {
				"subject" : uiThis.find('.subject').data("subject"),
				"message" : uiThis.find('.message').data("message"),
				"datetime" : uiThis.find('.date-time').data('datetime')
			};
		});

		return oData;
	}

	function _render(oParams) {
		var oElement = {
			"stock_receiving" : function(oData) {
				var uiStockReceivingList = $("#stock_receiving_list");

				uiStockReceivingList.append(oParams.data);
			},
			"stock_view_ongoing" : function(oData) {
				var uiPONumber = $("#view_ongoing_po_number"),
				oGoodsDetails = oParams.data,
				uiPurchaseOfficer = $("#view_ongoing_purchase_officer"),
				uiCourier = $("#view_ongoing_courier_name"),
				uiInvoice = $("#view_ongoing_invoice"),
				uiInvoiceDate = $("#view_ongoing_invoice_date"),
				uiDateIssued = $("#view_ongoing_date_issued"),
				uiStatus = $("#view_ongoing_status");

				uiPONumber.html(oGoodsDetails.purchase_order),
				uiDateIssued.html(oGoodsDetails.date_received),
				uiStatus.html(oGoodsDetails.status);
			},
			"stock_view_edit" : function(oData) {
				var oGoodsDetails = oParams.data,
				uiPurchaseOrder = $("#stock_edit_PO"),
				uiPurchaseOfficer = $("#stock_edit_purchase_officer"),
				uiCourier = $("#stock_edit_courier"),
				uiInvoiceNumber = $("#stock_edit_invoice_number"),
				uiInvoiceDate = $("#stock_edit_invoice_date");

				uiPurchaseOrder.val(oGoodsDetails.purchase_order),
				uiInvoiceDate.val(oGoodsDetails.date_received);
			}
		}

		if(typeof (oElement[oParams.name]) == "function") {
			oElement[oParams.name](oParams.name);
		}
	}

	function _setCompanyGoodList(oResult) {
		var oCompanyGoodList = {},
		oCompanyGoods = oResult;

		if(oCompanyGoods === null
		|| oCompanyGoods === undefined
		|| JSON.stringify(oCompanyGoods) == "{}") {
			getAllCompanyGoods();
		}

		oCompanyGoodList = { name : "receiving_companyGoodList", data : oCompanyGoods };
		cr8v_platform.localStorage.delete_local_storage({name : "receiving_companyGoodList"});
		cr8v_platform.localStorage.set_local_storage(oCompanyGoodList);
		return oCompanyGoods;
	}

	function _getCompanyGoodList() {
		return oCompanyGoods;
	}

	function _setCurrentCompanyGoods(iReceiving_id) {
		var arrCompanyGoods = _getCompanyGoodList(),
		oCompanyGood = {};

		for(var a in arrCompanyGoods) {
			if(arrCompanyGoods[a].id == iReceiving_id) {
				oData = {
					name : "currentReceivingCompanyGood",
					data : arrCompanyGoods[a]
				};
				oCurrentGood = arrCompanyGoods[a];
			}
		}

		cr8v_platform.localStorage.delete_local_storage({name : "currentReceivingCompanyGood"});
		cr8v_platform.localStorage.set_local_storage(oData);
	}

	function _getCurrentCompanyGoods() {
		var oCurrentGood = {};

		oCompanyGood = cr8v_platform.localStorage.get_local_storage({name : "currentReceivingCompanyGood"});
		return oCompanyGood;
	}

	function _getLocalCompanyGoodList() {
		var oCompanyGoodList = {};

		oCompanyGoodList = cr8v_platform.localStorage.get_local_storage({name : "receiving_companyGoodList"});
		return oCompanyGoodList;
	}
	
	function _buildTemplate(oParams) {
		var oTemplate = {},
		sTemplate = "",
		oElement = {
			"receiving_company_good_list" : function(oData) {

			},
			"product_list" : function(oData) {

			},
			"note_list" : function(oData) {

			},
			"stock_view_ongoing" : function(oData) {

			},
			"stock_view_product_list" : function(oData) {

			}
		}

		if(typeof (oElement[oParams.name]) == "function") {
			element[oParams.name](oParams.name);
		}
		return oTemplate;
	}

	function _add() {
		var po_number = $("#stock_create_po_number"),
			oReceivingNumber = cr8v_platform.localStorage.get_local_storage({ "name" : "receiving_stock_number" }),
			sReceivingNum = oReceivingNumber.number,
			oItems = _itemsEvent("getItems"),
			oNotes = _getNotes(),
			officer = $("#stock_create_officer"),
			courier = $("#stock_create_courier"),
			invoice_number = $("#stock_create_invoice_number"),
			invoice_date = $("#stock_create_invoice_date"),
			oUploadedDocuments = {};

		var uiUploadedDocs = $("#displayDocuments").find(".table-content");
		$.each(uiUploadedDocs, function(){
			var data = $(this).data();
			oUploadedDocuments[Object.keys(oUploadedDocuments).length] = data;
		});

		var arrExplodedDate = invoice_date.val().split("/"),
			sDate = "",
			arrDate = [];

		for(var i = arrExplodedDate.length - 1; i>=0; i--){
			arrDate.push(arrExplodedDate[i]);
		}

		var arrDateMonth = arrDate[2],
			arrDateDay = arrDate[1];

		arrDate[1] = arrDateMonth;
		arrDate[2] = arrDateDay;

		sDate = arrDate.join("-");

		var sPONumber = po_number.val(),
			sOfficer = officer.val(),
			sCourier = courier.val(),
			sInvoiceNum = invoice_number.val(),
			sItems = JSON.stringify(oItems),
			sNotes = JSON.stringify(oNotes);

		cr8v_platform.CconnectionDetector.ajax({
			type : "POST",
			url : BASEURL + "receiving/add_company_receiving",
			data : { 
				"receiving_number" : sReceivingNum,
				"po_number" : sPONumber,
				"officer" : sOfficer,
				"courier" : sCourier,
				"invoice_number" : sInvoiceNum,
				"invoice_date" : sDate,
				"items" : sItems,
				"notes" : sNotes,
				"documents" : JSON.stringify(oUploadedDocuments)
			},
			beforeSend: function(){
				$('[modal-id="confirm-create-record"] .modal-content > p').html("Creating record...");
				$('[modal-id="confirm-create-record"] .modal-footer button').hide();
				$('div.modal-close.close-me:visible').hide();
				//modal-footer
			},
			success : function(oResult) {
				if(oResult.status){
					$('[modal-id="confirm-create-record"]').removeClass('showed');
					$('[modal-id="created-record"]').addClass('showed');
					$('[modal-id="created-record"] .message').html("Receiving No. "+sReceivingNum+" has been created.");
					$('[modal-id="created-record"] div.close-me').off('click').on('click', function(){
						window.location.reload();
					});
					$('[modal-id="created-record"] .modal-footer button[type="button"]').off('click').on('click', function(){
						cr8v_platform.localStorage.set_local_storage({
							"name" : "receivingCompanyGoodsRecord",
							"data" : { "receivingId" : oResult.data }
						});
						window.location.href = BASEURL + "receiving/view_company_goods";
					});
				}else{
					$("body").feedback({title : "Message", message : oResult.message, type : "danger", icon : failIcon});
				}
			}
		});
	}

	function _getAllProducts() {
		return CProducts.getAllProducts();
	}

	// ================================================== //
	// ==============END OF PRIVATE METHODS============== //
	// ================================================== //

	// ================================================== //

	// ====================UI METHODS==================== //
	// ================================================== //

	function _displayListCompanyGoods(oData)
	{
		var methods = {
			"getTemplate" : function(){
				var shtml = '<div class="tbl-like hover-transfer-tbl record-list">'+
				'	<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">'+
				'		<div class="display-inline-mid text-center width-15percent text-left">'+
				'			<p class=" font-400 font-14 f-none text-center no-padding-left width-100percent receiving-number"></p>'+
				'		</div>'+
				'		<div class="display-inline-mid text-center width-15percent ">'+
				'			<p class=" font-400 font-14 f-none width-100percent no-padding-left date-created"></p>'+
				'		</div>'+
				'		<div class=" display-inline-mid font-400 text-center width-40percent">'+
				'			<ul class=" text-left text-indent-20 padding-left-50 font-14 width-100percent item-list-container">'+
				'			</ul>'+
				'		</div>'+
				'		<div class="font-400 display-inline-mid text-center width-15percent ">'+
				'			<div class="owner-img width-30per display-inline-mid">'+
				'				<img src="" alt="Profile Owner" class="width-60percent user-img">'+
				'			</div>'+
				'			<p class="width-150px font-400 font-14 display-inline-mid f-none capitalize user-fullname"></p>'+
				'		</div>'+
				'		<div class="display-inline-mid text-center width-15percent">'+
				'			<p class=" font-400 font-14 f-none width-100percent capitalize status">Ongoing</p>'+
				'		</div>'+
				'	</div>'+
				'	<div class="hover-tbl button-holder" style="height: 100%;"> '+
				'	</div>'+
				'</div>';
				return shtml;
			},
			"displayItems" : function(oItems, ui){
				var uiContainer = ui.find(".item-list-container"),
					len = Object.keys(oItems).length;
				for(var i in oItems){
					if(parseInt(i) < 2){
						var sHtml = '<li>';
							sHtml += "#"+oItems[i]["product_sku"]+"-"+oItems[i]["product_name"];
						sHtml += '</li>';
						uiContainer.append(sHtml);
					}
				}

				if(len > 2){
					var remain = len - 2;
					uiContainer.append("<li> "+remain+" more items </li>");
				}
			},
			"getButtons" : function(sStatus, data){
				switch(sStatus){
					case "0" :
						var bDetailsCompleted = false,
							sHtml = '';
						if(data.hasOwnProperty("items")){
							var items = data.items,
								errorDiscrepancy = 0;
							for(var i in items){
								if(items[i].hasOwnProperty("discrepancy")){
									if(Object.keys(items[i]["discrepancy"]).length == 0){
										errorDiscrepancy++;
									}
								}else{
									errorDiscrepancy++;
								}
							}
							if(errorDiscrepancy == 0){
								bDetailsCompleted = true;
							}
						}

						if(bDetailsCompleted){
							sHtml = '<a href="javascript:void(0)"> <button class="btn general-btn margin-right-10 view-record">View Record</button></a>'+
									'<a href="javascript:void(0)"><button class="btn general-btn complete-receiving-record">Complete Receiving Record</button></a>';
						}else{
							sHtml = '<a href="javascript:void(0)"> <button class="btn general-btn margin-right-10 view-record">View Record</button></a>'+
									'<a href="javascript:void(0)"><button class="btn general-btn enter-product-details">Enter Product Details</button></a>';
						}
						
						return sHtml;
					break;
					case "1" :
						//completed in db
						//completed in ui
						var sHtml = '<a href="javascript:void(0)"> <button class="btn general-btn margin-right-10 view-record">View Record</button></a>';
						return sHtml;
					break;
					case "2" :
						//on going in db
						//for completion in ui
						var sHtml = '<a href="javascript:void(0)"> <button class="btn general-btn margin-right-10 view-record">View Record</button></a>'+
									'<a href="javascript:void(0)"><button class="btn general-btn complete-receiving-record">Complete Receiving Record</button></a>';
						return sHtml;
					break;
				}
			},
			"addClickEvent" : function(ui, sStatus, data){
				var uiViewRecord = ui.find('button.view-record'),
					uiEnterDetails = ui.find('button.enter-product-details'),
					uiComplete = ui.find('button.complete-receiving-record');

				uiViewRecord.click(function(){
					var uiParent = $(this).closest('.record-list'),
						oItemData = uiParent.data();
					cr8v_platform.localStorage.set_local_storage({
						"name" : "receivingCompanyGoodsRecord",
						"data" : oItemData
					});
					window.location.href = BASEURL + "receiving/view_company_goods";
				});

				uiEnterDetails.click(function(){
					var uiParent = $(this).closest('.record-list'),
						oItemData = uiParent.data();
					cr8v_platform.localStorage.set_local_storage({
						"name" : "receivingCompanyGoodsRecord",
						"data" : oItemData
					});
					localStorage.setItem("scrollTo", "true");
					window.location.href = BASEURL + "receiving/view_company_goods";
				});

				uiComplete.click(function(){
					var uiParent = $(this).closest('.record-list'),
						oItemData = uiParent.data(),
						receivingId = oItemData.receivingId;
					
					var uiModal = $('[modal-id="complete-receiving-modal"]'),
						uiModalCompleting = $('[modal-id="completing-receiving-modal"]'),
						close = uiModal.find(".close-me"),
						submit = uiModal.find(".submit-btn");

					close.off("click").on("click", function(){
						uiModal.removeClass('showed');
					});

					submit.off("click").on("click", function(){
						var oOptions = {
							type : "POST",
							data : { "receiving_id" : receivingId},
							url : BASEURL + "receiving/complete_record",
							beforeSend : function(){
								uiModal.removeClass('showed');
								uiModalCompleting.addClass('showed');
							},
							success : function(oResult) {
								if(oResult.status){
									uiModalCompleting.find(".message").html("Success, reloading..");
									setTimeout(function(){
										window.location.reload();
									},500);
								}
							}
						}
						cr8v_platform.CconnectionDetector.ajax(oOptions);
					});

					uiModal.addClass('showed');
				});
			}
		};

		var uiContainer = $("#receivingList"),
			uiDisplayTotalRow = $("#total-row-consignee"),
			iTotal = Object.keys(oData).length;

			uiDisplayTotalRow.html("Total Records: "+iTotal);

		

		for(var i in oData){
			var $template = $(methods.getTemplate()),
				data = oData[i],
				sButtons = methods.getButtons(data["status"], data);

			$template.data({
				"receivingId" : data.id,
				"receivingNumber" : data.receiving_number
			});

			$template.find(".button-holder").html(sButtons);

			methods.addClickEvent($template, data.status, data);

			$template.find(".receiving-number").html(data.receiving_number);
			$template.find(".date-created").html(data.date_created_formatted);

			methods.displayItems(data.items, $template);

			if(data.owner.hasOwnProperty("image")){
				$template.find(".user-img").attr("src", data.owner.image);
			}else{
				var defaultPath = CGetProfileImage.getDefault(data.owner.first_name);
				$template.find(".user-img").attr("src", defaultPath);
			}

			$template.find(".user-fullname").html(data.owner.first_name+" "+data.owner.last_name);

			if(data.status == "1"){
				$template.find(".status").html("Complete");
			}

			uiContainer.append($template);
		}
	}
	
	function getAllStorage(){
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "storages/get_all_storages",
			success : function(oResult) {
				oStorages = oResult;
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}


	// ================================================== //
	// ==================END OF UI METHODS=============== //
	// ================================================== //
	return {
		getAllCompanyGoods : getAllCompanyGoods,
		bindEvents : bindEvents,
		getAllStorage : getAllStorage,
		displaySearchData :_displaySearchData
	};
})();

$(document).ready(function() {
	


	$('.dropdown-search').find('.btn general-btn font-bold').click(function() {
		  alert( "test" );
		});

	var myOptions = {
			    val1 : 'text1',
			    val2 : 'text2'
			};
			var mySelect = $('#selectCompanyGoods');
			$.each(myOptions, function(val, text) {
			    mySelect.append(
			        $('<option></option>').val(val).html(text)
			    );
			});

			

	// if(window.location.href.indexOf("stock_receiving") > -1){
		// CReceivingCompanyGoods.getAllCompanyGoods();
		// CReceivingCompanyGoods.bindEvents();
	// }

	// if(window.location.href.indexOf("stock_create") > -1){
		// CReceivingCompanyGoods.bindEvents();
	// }
	if(window.location.href.indexOf("receiving") > -1){
		CReceivingCompanyGoods.bindEvents();
		CReceivingCompanyGoods.getAllStorage();
	
	}

	//if(window.location.href.indexOf("stock_create") > -1){
		//$('.cancel-go-back').click(function(){
			//window.location.href = BASEURL + "receiving/company_goods"
		//});
	//}

});