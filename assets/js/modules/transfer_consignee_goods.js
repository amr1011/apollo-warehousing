var CTransferConsigneeGoods = (function(){

	var failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg",
		oUploadedDocuments = {},
		oStorages = {},
		oBatchPerItem = {},
		sOldBatch = "",
		sNewQty = "",
		oLocations = "",
		oBatchInfos ="",
		sbatchMethod = "",
		iOldBatchID = "",
		oDuplicateProducts = "",
		oDuplicateEditProducts = "",
		oSelectedTranseferData = "",
		oEditSelectedTranseferData = "",
		sTransferMethod = "",
		oVesselList = "",
		oModeOfTransfer = "",
		oCollectUseProd = "",
		oCollectNewDisplayProdEdit = "",
		oStoreCompleteStransfer = "";


	function _add(oData)
	{
		var oGetTransferNumber = cr8v_platform.localStorage.get_local_storage({name:"getTransferNumber"})
			oGetDocument = cr8v_platform.localStorage.get_local_storage({name:"dataUploadedDocument"}),
			oGetNotes = cr8v_platform.localStorage.get_local_storage({name:"setConsigneeAddNoteStored"}),
			uiCloseSuccessModal = $("#created-record-consignee-modal").find("#close-success-modal"),
			oCollectData = {};

			oCollectData = {
				to_number : oGetTransferNumber.value,
				requested_by : oData.requested_by,
				reason : oData.reason,
				vessel_origin : oData.vessel_origin,
				mode_of_transfer : oData.mode_of_transfer,
				product_info : oData.product_info,
				document_info : oGetDocument,
				notes_info : oGetNotes
			}

			// console.log(JSON.stringify(oCollectData));


			var oOptions = {
				url : BASEURL+"transfers/add_new_transfer_consignee_goods",
				type : "POST",
				data : {"data":JSON.stringify(oCollectData)},
				success : function(oResponse)
				{
					
					cr8v_platform.localStorage.set_local_storage({
						name : "getNewTransferID",
						data : {"value":oResponse.data}
					});

					$("body").css({overflow:'hidden'});
					
					$("#created-record-consignee-modal").addClass("showed");

					$("#created-record-consignee-modal .close-me").on("click",function(){
						$("body").css({'overflow-y':'initial'});
						$("#created-record-consignee-modal").removeClass("showed");
					});

					uiCloseSuccessModal.off("click.closeSuccess").on("click.closeSuccess", function(){
						window.location.href = BASEURL+"transfers/consign_create";
					});

					
				}
			}
			cr8v_platform.CconnectionDetector.ajax(oOptions);

			cr8v_platform.localStorage.delete_local_storage({name:"getTransferNumber"});
			cr8v_platform.localStorage.delete_local_storage({name:"dataUploadedDocument"});
			cr8v_platform.localStorage.delete_local_storage({name:"setConsigneeAddNoteStored"});

		// console.log(JSON.stringify(oData));
		// console.log(JSON.stringify(oGetDocument));
		// console.log(JSON.stringify(oGetNotes));

	}

	function _saveSelectedProductData(oData)
	{
		var oOptions = {
			url : BASEURL+"transfers/save_selected_product_consignee",
			type : "POST",
			data : {"data":JSON.stringify(oData)},
			success : function(oResponse){
				console.log(oResponse.data);
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _updateTransferConsigneeProduct(oData)
	{
		var oOptions = {
			url : BASEURL+"transfers/update_transfer_consignee_product",
			type : "POST",
			data : {"data":JSON.stringify(oData)},
			success : function(oResponse)
			{
				
				console.log(oResponse);
				// window.location.href = BASEURL+"transfers/consign_create";
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);

		setTimeout(function(){
			window.location.href = BASEURL+"transfers/consignee_ongoing";
		},1000);

		

		cr8v_platform.localStorage.delete_local_storage({name:"getAllVesselName"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllProducts"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllModeOfTransfer"});
	}

	function _saveTransferConsigneeComplete(oDataTransfer)
	{
		var uiCloseSuccessModal = $('#completed-transfer-consignee-modal').find("#close-success-modal");

		var oOptions = {
			url : BASEURL+"transfers/save_transfer_consignee_complete",
			type : "POST",
			data : {"data":JSON.stringify(oDataTransfer)},
			success : function(oResponse)
			{
				
				console.log(oResponse);
				$("body").css({overflow:'hidden'});
					
				$("#completed-transfer-consignee-modal").addClass("showed");

				$("#completed-transfer-consignee-modal .close-me").on("click",function(){
					$("body").css({'overflow-y':'initial'});
					$("#completed-transfer-consignee-modal").removeClass("showed");
				});

				uiCloseSuccessModal.off("click.closeSuccess").on("click.closeSuccess", function(){
					window.location.href = BASEURL+"transfers/consign_transfer";
				});


			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);


		cr8v_platform.localStorage.delete_local_storage({name:"unit_of_measures"});
		cr8v_platform.localStorage.delete_local_storage({name:"setCurrentDateTime"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllVesselData"});
		cr8v_platform.localStorage.delete_local_storage({name:"dataUploadedDocument"});

		cr8v_platform.localStorage.delete_local_storage({name:"getAllProducts"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllModeOfTransfer"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllLocationsName"});
		cr8v_platform.localStorage.delete_local_storage({name:"dataUploadedDocument"});
		cr8v_platform.localStorage.delete_local_storage({name:"setConsigneeAddNoteStored"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllLocationsName"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllVesselName"});




	}

	function getTransferConsigneeComplete()
	{
		var transferID = cr8v_platform.localStorage.get_local_storage({name:"getNewTransferID"});

		var oOptions = {
			url : BASEURL+"transfers/get_transfer_consignee_complete",
			type : "POST",
			data : {"transfer_id":transferID.value},
			beforeSend: function(){
				$('#generate-data-view').addClass('showed');
			},
			success : function(oResponse){
				// console.log(JSON.stringify(oResponse.data));

				oStoreCompleteStransfer = oResponse.data;
				_displayTransferConsigneeComplete(oResponse.data);

			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getAllTransferConsigneeRecords()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "transfers/get_all_transfer_consignee_records",
			returnType : "json",
			beforeSend: function(){
				$('#generate-data-view').addClass('showed');
			},
			success : function(oResponse) {
				// console.log(JSON.stringify(oResponse.data));
				_displayAllTransferConsignee(oResponse.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}



	function generateTransferNumber() {
	    var oOptions = {
	      type : "GET",
	      data : {test : true},
	      returnType : "json",
	      url : BASEURL + "transfers/generate_transfer_number",
	      success : function(oResult) {
	        var sTranferNumber = oResult.data.toString(),
	        sNewTransferNumber = sTranferNumber.toString(),
	        sCurrentTransferNumber = null,
	        iToLimit = 9;

	        while(sNewTransferNumber.length < iToLimit) {
	          sNewTransferNumber = "0" + sNewTransferNumber.toString();
	        }

	         console.log(sNewTransferNumber);

	        cr8v_platform.localStorage.set_local_storage({
				name : "getTransferNumber",
				data : {"value":sNewTransferNumber}
			});

			renderElement("displayTransferNumber", sNewTransferNumber);


	      }
	    }

	    cr8v_platform.CconnectionDetector.ajax(oOptions);
	 }

	function getTransferData(callBack)
	{
		var transferID = cr8v_platform.localStorage.get_local_storage({name:"getNewTransferID"});
		
		var oOptions = {
			url : BASEURL+"transfers/get_transfer_data",
			type : "POST",
			data : {"transfer_id":transferID.value},
			beforeSend: function(){
				$('#generate-data-view').addClass('showed');
			},
			success : function(oResponse){
				
				// console.log(JSON.stringify(oResponse.data));
				oSelectedTranseferData = oResponse.data;

				if(typeof callBack == 'function'){
					callBack(oResponse.data);
				}

			},

		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	function getTransferDataEdit()
	{
		var transferID = cr8v_platform.localStorage.get_local_storage({name:"getNewTransferID"});
		
		var oOptions = {
			url : BASEURL+"transfers/get_transfer_data_edit",
			type : "POST",
			data : {"transfer_id":transferID.value},
			beforeSend: function(){
				$('#generate-data-view').addClass('showed');
			},
			success : function(oResponse){
				
				// console.log(JSON.stringify(oResponse.data));
				oEditSelectedTranseferData = oResponse.data;

				_displayEditTransferData(oResponse.data);

				if(typeof callBack == 'function'){
					callBack(oResponse.data);
				}

			},

		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getAllBatchForProduct()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "transfers/get_all_batch_consignee",
			returnType : "json",
			success : function(oResult) {

				if(oResult.data === null){

				}else{
					oBatchInfos = oResult.data;

					// console.log(JSON.stringify(oBatchInfos));
	
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getAllLocation()
	{
		
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "transfers/get_all_locations",
			returnType : "json",
			success : function(oResponse) {

				if(oResponse.data === null){

				}else{

					oLocations = oResponse.data;
					cr8v_platform.localStorage.set_local_storage({
						name : "getAllLocationsName",
						data : oResponse.data
					});
					
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	// function getAllLocationComplete()
	// {
		
	// 	var oOptions = {
	// 		type : "GET",
	// 		data : { data : "none" },
	// 		url : BASEURL + "transfers/get_all_locations",
	// 		returnType : "json",
	// 		success : function(oResponse) {

	// 			if(oResponse.data === null){

	// 			}else{

	// 				oLocations = oResponse.data;

	// 				cr8v_platform.localStorage.set_local_storage({
	// 					name : "getAllLocationsName",
	// 					data : oResponse.data
	// 				});
					
	// 			}

	// 		}
	// 	}

	// 	cr8v_platform.CconnectionDetector.ajax(oOptions);

	// }

	function getAllStorage()
	{
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "storages/get_all_storages",
			success : function(oResult) {
				oStorages = oResult;
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}


	function getVesselName()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "receiving/get_vessel_name",
			returnType : "json",
			success : function(oResult) {

				if(oResult.data === null){

				}else{
					// cr8v_platform.localStorage.set_local_storage({
					// 	name : "getAllVesselName",
					// 	data : oResult.data
					// });
					_displayAllVessel(oResult.data)
					
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getEditVesselName()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "receiving/get_vessel_name",
			returnType : "json",
			success : function(oResult) {

				if(oResult.data === null){

				}else{
					cr8v_platform.localStorage.set_local_storage({
						name : "getAllVesselName",
						data : oResult.data
					});
					oVesselList = oResult.data;

					if(typeof callBack == 'function'){
						callBack(oResult.data);
					}

					
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getModeOfTransfer()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "transfers/get_mode_of_transfer",
			returnType : "json",
			success : function(oResult) {

				if(oResult.data === null){

				}else{
					cr8v_platform.localStorage.set_local_storage({
						name : "getAllModeOfTransfer",
						data : oResult.data
					});

					if(sCurrentPath.indexOf('transfers/consign_create') > -1 )
					{
						_displayAllModeOfTransfers(oResult.data);
						
					}
					// console.log(JSON.stringify(oResult.data));

					oModeOfTransfer = oResult.data
					
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getProductlist()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "transfers/get_consignee_products",
			returnType : "json",
			success : function(oResponse) {

				if(oResponse.data === null){

				}else{
					cr8v_platform.localStorage.set_local_storage({
						name : "getAllProducts",
						data : oResponse.data
					});



					oDuplicateProducts = oResponse.data;

					_displayAllProductList(oResponse.data);
					
					// console.log(JSON.stringify(oResponse.data));
					
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getEditProdList()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "transfers/get_consignee_products_for_edit",
			returnType : "json",
			success : function(oResponse) {

				

				
					cr8v_platform.localStorage.set_local_storage({
						name : "getAllProducts",
						data : oResponse.data
					});

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}


	function getUnitOfMeasure()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "products/get_unit_of_measure",
			returnType : "json",
			success : function(oResult) {
				// console.log(JSON.stringify(oResult.data));
				if(oResult.data === null)
				{
					cr8v_platform.localStorage.set_local_storage({
						"name" : "unit_of_measures",
						"data" : ""
					});
				}else{
					cr8v_platform.localStorage.set_local_storage({
						"name" : "unit_of_measures",
						"data" : oResult.data
					});
				}

				if(typeof callBack == 'function'){
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _displayAllVessel(oData)
	{
		uiOriginVesselDropdownContainer = $(".origin-vessel-dropdown"),
		uiParentDropdown = uiOriginVesselDropdownContainer.parent(),
		uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');

		uiSiblingDropdown.remove();
		uiOriginVesselDropdownContainer.removeClass("frm-custom-dropdown-origin");

		if(oData === 0){
			sHtml ='<option value="">No Vessels Found</option>';
			uiOriginVesselDropdownContainer.append(sHtml);
		}else{
			for(var x in oData)
			{
				sHtml = '<option value="'+oData[x]['id']+'">'+oData[x]['name']+'</option>';
				uiOriginVesselDropdownContainer.append(sHtml);
			}

		}

		
		uiOriginVesselDropdownContainer.transformDD();
		CDropDownRetract.retractDropdown(uiOriginVesselDropdownContainer);

	}

	function _displayEditAllVessel(vesselID)
	{

		// console.log(JSON.stringify(oVesselList));
		var oData = "";
			uiOriginVesselDropdownContainer = $(".origin-vessel-dropdown"),
			uiParentDropdown = uiOriginVesselDropdownContainer.parent(),
			uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');


		uiSiblingDropdown.remove();
		uiOriginVesselDropdownContainer.removeClass("frm-custom-dropdown-origin");


		var setVesselInterval = setInterval(function(){
			var oData = cr8v_platform.localStorage.get_local_storage({name:"getAllVesselName"});
			if(oData !== null)
			{
				clearInterval(setVesselInterval);
				if(oData === 0){
					sHtml ='<option value="">No Vessels Found</option>';
					uiOriginVesselDropdownContainer.append(sHtml);
				}else{
					for(var x in oData)
					{
						if(vesselID == oData[x]['id'])
						{
							sHtml = '<option value="'+oData[x]['id']+'" selected>'+oData[x]['name']+'</option>';
							uiOriginVesselDropdownContainer.append(sHtml);
						}else{
							sHtml = '<option value="'+oData[x]['id']+'">'+oData[x]['name']+'</option>';
							uiOriginVesselDropdownContainer.append(sHtml);
						}
						
					}

				}

				
				uiOriginVesselDropdownContainer.transformDD();
				CDropDownRetract.retractDropdown(uiSelectConsignName);

			}
		}, 300);



		
	}

	function _displayAllModeOfTransfers(oData)
	{
		uiModeOfTransferDropdownContainer = $(".mode-of-transfer"),
		uiParentDropdown = uiModeOfTransferDropdownContainer.parent(),
		uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');

		uiSiblingDropdown.remove();
		uiModeOfTransferDropdownContainer.removeClass("frm-custom-dropdown-origin");

		if(oData === 0){
			sHtml ='<option value="">No Mode Of Transfer Found</option>';
			uiModeOfTransferDropdownContainer.append(sHtml);
		}else{
			for(var x in oData)
			{
				sHtml = '<option value="'+oData[x]['id']+'">'+oData[x]['name']+'</option>';
				uiModeOfTransferDropdownContainer.append(sHtml);
			}

		}

		
		uiModeOfTransferDropdownContainer.transformDD();
		CDropDownRetract.retractDropdown(uiModeOfTransferDropdownContainer);
	}

	function _displayEditAllModeOfTransfers(modeTransID)
	{
		var oData = "",
			uiModeOfTransferDropdownContainer = $(".mode-of-transfer"),
			uiParentDropdown = uiModeOfTransferDropdownContainer.parent(),
			uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');

			var setModeTransferInterval = setInterval(function(){
				oData = cr8v_platform.localStorage.get_local_storage({name:"getAllModeOfTransfer"});
				if(oData !== null)
				{
					clearInterval(setModeTransferInterval);
					uiSiblingDropdown.remove();
					uiModeOfTransferDropdownContainer.removeClass("frm-custom-dropdown-origin");

					if(oData === 0){
						sHtml ='<option value="">No Mode Of Transfer Found</option>';
						uiModeOfTransferDropdownContainer.append(sHtml);
					}else{
						for(var x in oData)
						{

							if(modeTransID == oData[x]['id'])
							{
								sHtml = '<option value="'+oData[x]['id']+'" selected>'+oData[x]['name']+'</option>';
								uiModeOfTransferDropdownContainer.append(sHtml);
							}else{
								sHtml = '<option value="'+oData[x]['id']+'">'+oData[x]['name']+'</option>';
								uiModeOfTransferDropdownContainer.append(sHtml);
							}
							
						}

					}

					
					uiModeOfTransferDropdownContainer.transformDD();
					CDropDownRetract.retractDropdown(uiModeOfTransferDropdownContainer);
				}
			}, 300);

		
	}



	function _displayAllProductList(oData)
	{
		uiProductListDropdownContainer = $(".product-list-dropdown"),
		uiParentDropdown = uiProductListDropdownContainer.parent(),
		uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');

		uiSiblingDropdown.remove();
		uiProductListDropdownContainer.removeClass("frm-custom-dropdown-origin");


		if(Object.keys(oData).length == 0){
			sHtml ='<option value="">No Products Found</option>';
			uiProductListDropdownContainer.append(sHtml);
		}else{
			for(var x in oData)
			{
				sHtml = '<option value="'+oData[x]['id']+'">'+oData[x]['name']+'</option>';
				uiProductListDropdownContainer.append(sHtml);
			}

		}

		
		uiProductListDropdownContainer.transformDD();
		CDropDownRetract.retractDropdown(uiProductListDropdownContainer);
	}

	function _displayLeftProductList(prodID)
	{
		console.log("dito");

		var uiProductListDropdownContainer = $(".product-list-dropdown"),
			uiParentDropdown = uiProductListDropdownContainer.parent(),
			uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');

		var oGetAllProducts = oDuplicateProducts;
		

		uiSiblingDropdown.remove();
		uiProductListDropdownContainer.removeClass("frm-custom-dropdown-origin");


		$.each(oDuplicateProducts, function(i, el){
		    if (this.id == prodID){
		       oDuplicateProducts.splice(i, 1);

		       $(".product-list-dropdown").load(location.href + " .product-list-dropdown", function(){

		       	_displayAllProductList(oDuplicateProducts);
		       });

		    }
		});
		

	}

	function _displayEditLeftProductList(oData)
	{
		var uiProductListDropdownContainer = $(".product-list-dropdown"),
			uiParentDropdown = uiProductListDropdownContainer.parent(),
			uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');
			oGetAllProducts = cr8v_platform.localStorage.get_local_storage({name:"getAllProducts"}),
			oStoredProd = [],
			oStoredSelectedProd = [];
			


		uiSiblingDropdown.remove();
		uiProductListDropdownContainer.removeClass("frm-custom-dropdown-origin");


		for(var x in oGetAllProducts)
		{
			var iCountIt = 0;

			for(var i in oData)
			{
				if(oData[i]["id"] == oGetAllProducts[x]["id"])
				{
					iCountIt++;
					oStoredSelectedProd.push(oGetAllProducts[x]);
					

				}
			}
			if(iCountIt == 0)
			{

				oStoredProd.push(oGetAllProducts[x]);
			}
		}

			oCollectUseProd = oStoredSelectedProd;
			oDuplicateProducts = oStoredProd;

	       $(".product-list-dropdown").load(location.href + " .product-list-dropdown", function(){

	       		_displayAllProductList(oDuplicateProducts);
	       });

	}

	function _documentEvents(bTemporary)
	{
		var btnUploadDocs =  $('[modal-target="upload-documents"]'),
			oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "getNewTransferID"}),
			receivingId = 0;

			if(oRecord !== null){

				transferId = oRecord.value
			}else{
				transferId = 0;
			}
			// console.log(receivingId);

		btnUploadDocs.off('click');

		var extraData = { "transfer_log_id" : transferId };

		if(bTemporary === true){
			extraData["temporary"] = true;
		}

		btnUploadDocs.cr8vFileUpload({
			url : BASEURL + "transfers/upload_document_consignee",
			accept : "pdf,csv",
			filename : "attachment",
			extraField : extraData,
			onSelected : function(bIsValid){
				if(bIsValid){
					//selected valid
				}
			},
			success : function(oResponse){
				if(oResponse.status){
					$('[cr8v-file-upload="modal"] .close-me').click();
					var oDocs = oResponse.data[0]["documents"];
					if(bTemporary){
						oUploadedDocuments[Object.keys(oUploadedDocuments).length] = oResponse.data[0]["documents"][0];
						_displayDocuments(oUploadedDocuments);
					}else{
						_displayDocuments(oDocs);
					}

					// console.log(JSON.stringify(oDocs));
				}
			},
			error : function(error){
				console.log(error);
				$("body").feedback({title : "Message", message : "Was not able to upload, file maybe too large", type : "danger", icon : failIcon});
			}
		});

		// console.log("documents in");
	}

	function _displayDocuments(oDocs)
	{
		// console.log(JSON.stringify(oDocs));

		var bChangeBg = false,
			iCountDocument = 0;

		var uiContainer = $("#displayDocuments"),
			sTemplate = '<div class="table-content position-rel tbl-dark-color">'+
							'<div class="content-show padding-all-10">'+
								'<div class="width-85per display-inline-mid padding-left-10">'+
									'<i class="fa font-30 display-inline-mid width-50px icon"></i>'+
									'<p class=" display-inline-mid doc-name">Document Name</p>'+
								'</div>'+
								'<p class=" display-inline-mid date-time"></p>'+
							'</div>'+
							'<div class="content-hide" style="height: 50px;">'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30 view-document">View</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid download-document">'+
									'<button class="btn general-btn ">Download</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30">Print</button>'+
								'</a>'+
							'</div>'+
						'</div>';

		uiContainer.html("");
		for(var i in oDocs){
			var uiList = $(sTemplate),
				ext = (oDocs[i]["document_path"].substr(oDocs[i]["document_path"].lastIndexOf('.') + 1)).toLowerCase();

			if(ext == 'csv'){
				uiList.find(".icon").addClass('fa-file-excel-o');
			}else if(ext == 'pdf'){
				uiList.find(".icon").addClass('fa-file-pdf-o');
			}

			uiList.data(oDocs[i]);
			uiList.find(".doc-name").html(oDocs[i]["document_name"]);
			uiList.find(".date-time").html(oDocs[i]["date_added_formatted"]);
			if(bChangeBg){
				uiList.removeClass('tbl-dark-color');
				bChangeBg = false;
			}else{
				bChangeBg = true;
			}


			uiContainer.append(uiList);
			
		}



		cr8v_platform.localStorage.set_local_storage({
			name : "dataUploadedDocument",
			data : oDocs
		});

		_downloadDocument(oDocs);

	}

	function _downloadDocument(oDocs)
	{
		
		var	btnDownloadDocument = $(".download-document"),
			btnViewDocument = $(".view-document");


		btnDownloadDocument.off("mouseenter.download").on("mouseenter.download", function(e){
			e.preventDefault();
			var uiDownloadThis = $(this),
				uiParentContainer = uiDownloadThis.closest('.table-content.position-rel'),
				uiGetDataPath = uiParentContainer.data("document_path"),
				uiGetDataName = uiParentContainer.data("document_name");

				uiDownloadThis.attr("href", uiGetDataPath);
				uiDownloadThis.attr("download", uiGetDataName);

		});


		btnViewDocument.off("click.viewDocument").on("click.viewDocument", function(e){
			e.preventDefault();
			var uiParentContainer = $(this).closest('.table-content.position-rel'),
				uiGetDataPath = uiParentContainer.data("document_path");

				window.open(uiGetDataPath, '_blank');
		});
	}


	function _displayUnitOfMeasure()
	{

		var oUnitOfMeasures = "",
			uiUnitOfMeasure = $(".unit-of-measure"),
			uiUnitRemoveDropdown = $(".unit-remove-dropdown").find(".frm-custom-dropdown");

			uiUnitRemoveDropdown.remove();

			var iMeasureInterval = setInterval(function(){
				oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"});

				if(oUnitOfMeasures !== null)
				{
					for(var x in oUnitOfMeasures)
					{
						if(oUnitOfMeasures[x]['name'] == "kg" || oUnitOfMeasures[x]['name'] == "mt")
						{
							sHtmlMeasure = '<option value="'+oUnitOfMeasures[x]['id']+'">'+oUnitOfMeasures[x]['name']+'</option>';
							uiUnitOfMeasure.append(sHtmlMeasure);
						}

					}
					uiUnitOfMeasure.show().css({
						"padding" : "3px 4px",
						"width" :  "50px"
					});

					// console.log(JSON.stringify(oUnitOfMeasures));
				}
				clearInterval(iMeasureInterval);

			}, 300);

	}

	function _getCurrentDateTime()
	{
		var oOptions = {
			type : "POST",
			data : { data : "none" },
			url : BASEURL + "receiving/get_current_date_time",
			returnType : "json",
			success : function(oResult) {

				cr8v_platform.localStorage.set_local_storage({
					name : "setCurrentDateTime",
					data : {"date":oResult.data.date, "dateFormat":oResult.data.date_formatted}
				})
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _addNewNotes()
	{
		_getCurrentDateTime();

		var uiAddRecievingConsigneeNotesSaveBtn = $("#addRecievingConsigneeNotesSave"),
			sAddReceivingConsigneeNoteSubject = $("#AddReceivingConsigneeNoteSubject"),
			sAddReceivingConsigneeNoteMessage = $("#AddReceivingConsigneeNoteMessage"),
			oGetSetRecievingConsigneeNotes = cr8v_platform.localStorage.get_local_storage({name:"setConsigneeAddNoteStored"}),
			oGetDateTime = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDateTime"}),
			oSetOdata = {},
			oGatheredData =[],
			iCount = 1,
			sDateGet = "",
			sDateFormatGet = "";

			var setCurrentInterval = setInterval(function(){
				var oGetDateTime = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDateTime"});
				if(oGetDateTime !== null)
				{
					sDateGet = oGetDateTime.date;
					sDateFormatGet = oGetDateTime.dateFormat;
					clearInterval(setCurrentInterval);
				}
			}, 300);

			
			//This is for validation 
			$("#addReceivingConsigneeNoteForm").cr8vformvalidation({
				'preventDefault' : true,
			    'data_validation' : 'datavalid',
			    'input_label' : 'labelinput',
			    'onValidationError': function(arrMessages) {
			        // alert(JSON.stringify(arrMessages)); //shows the errors
			        var options = {title : "Add Notes Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger"};
					$("body").feedback(options);
		        },
			    'onValidationSuccess': function() {
			    	oGatheredData =[];

			    	console.log(JSON.stringify(oGetSetRecievingConsigneeNotes));


			    	if(oGetSetRecievingConsigneeNotes === null)
			    	{
			    		oSetOdata = {
			    			id : iCount,
			    			userId: iConstUserId,
			    			subject : sAddReceivingConsigneeNoteSubject.val().trim(),
				    		message : sAddReceivingConsigneeNoteMessage.val().trim(),
				    		datetime : sDateGet,
				    		displayDate : sDateFormatGet
				    	}

				    	oGatheredData.push(oSetOdata);
				    	
			    	}
			    	else
			    	{
			    		for(var x in oGetSetRecievingConsigneeNotes)
			    		{
			    			oSetOdata = {
			    				id : iCount,
			    				userId: iConstUserId,
					    		subject : oGetSetRecievingConsigneeNotes[x]["subject"],
					    		message : oGetSetRecievingConsigneeNotes[x]["message"],
					    		datetime : oGetSetRecievingConsigneeNotes[x]["datetime"],
					    		displayDate :oGetSetRecievingConsigneeNotes[x]["displayDate"]
					    	}
					    	oGatheredData.push(oSetOdata);
					    	iCount++;
			    		}

			    		oSetOdata = {
			    			id : iCount,
			    			userId: iConstUserId,
			    			subject : sAddReceivingConsigneeNoteSubject.val().trim(),
				    		message : sAddReceivingConsigneeNoteMessage.val().trim(),
				    		datetime : sDateGet,
				    		displayDate : sDateFormatGet
				    	}

				    	oGatheredData.push(oSetOdata);

			    	}

			    	
			    	
			    	cr8v_platform.localStorage.set_local_storage({
				    		name : "setConsigneeAddNoteStored",
				    		data : oGatheredData
				    	});

			    	$(".modal-close.close-me").trigger("click");

			    	_displayConsigneeNewNotes(oGatheredData);

			    	
					var options = {title : "Add Notes", message : "Successfully Added", speed : "slow", withShadow : true,  type : "success"};
					$("body").feedback(options);
		        }
			});

		uiAddRecievingConsigneeNotesSaveBtn.off("click.AddRecievingConsigneeNotesSave").on("click.AddRecievingConsigneeNotesSave", function(){
			
			$("#addReceivingConsigneeNoteForm").submit();
			

		});
	}

	function _displayConsigneeNewNotes(oData)
	{
		var uiAddReceivingConsigneeNotesDisplay = $("#AddConsigneeNotesDisplay");

			uiAddReceivingConsigneeNotesDisplay.html("");

			for(var x in oData)
			{
			
				var sHtml = '<div class="border-full padding-all-10 margin-left-18 margin-top-10">'+
								'<div class="border-bottom-small border-gray padding-bottom-10">'+
									'<p class="f-left font-14 font-400">Subject: '+oData[x]["subject"]+'</p>'+
									'<p class="f-right font-14 font-400">Date/Time: '+oData[x]["displayDate"]+'</p>'+
									'<div class="clear"></div>'+
								'</div>'+
								'<p class="font-14 font-400 no-padding-left padding-all-10">'+oData[x]["message"]+'</p>'+
								// '<a href="" class="f-right padding-all-10 font-400">Show More</a>'+
								'<div class="clear"></div>'+
							'</div>';
					var oSetHtmlData = $(sHtml);
						oSetHtmlData.data({
							"notesId":oData[x]["id"]
						});
						uiAddReceivingConsigneeNotesDisplay.append(oSetHtmlData);
			}
	}

	function _saveNotesInfo()
	{
		var uiSaveTransferNotesBtn = $("#add-notes-save"),
			sAddNoteSubject = $("#addNoteSubject"),
			sAddNoteMessage = $("#addNoteMessage"),
			sDateGet = "",
			sDateFormatGet = "";


			var currentDateInterval = setInterval(function(){
				oGetDateTime = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDateTime"});
				
				if(oGetDateTime !== null)
				{
					sDateGet = oGetDateTime.date;
					sDateFormatGet = oGetDateTime.dateFormat;
				}
				clearInterval(currentDateInterval);
				
			}, 300);

			//This is for validation 
			$("#addTransferNoteForm").cr8vformvalidation({
				'preventDefault' : true,
			    'data_validation' : 'datavalid',
			    'input_label' : 'labelinput',
			    'onValidationError': function(arrMessages) {
			        // alert(JSON.stringify(arrMessages)); //shows the errors
			        var options = {title : "Add Transfer Notes Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger",};
					$("body").feedback(options);
		        },
			    'onValidationSuccess': function() {

			    	var setData = [{
			    		"subject":sAddNoteSubject.val(),
			    		"message":sAddNoteMessage.val(),
			    		"datetime":sDateGet
			    	}];
			    	// console.log(JSON.stringify(setData));

			    	
			    	$(".modal-close.close-me").trigger("click");

			    	_addNotes(setData);
					
		        }
			});

			uiSaveTransferNotesBtn.off("click.addNotesValidate").on("click.addNotesValidate", function(){
				$("#addTransferNoteForm").submit();
				console.log("ojsss");
			});
	}

	function _addNotes(oData)
	{

		// console.log(JSON.stringify(getCurrentSelectedReceivedConsignee));
		var iGetTransferID = cr8v_platform.localStorage.get_local_storage({name:"getNewTransferID"});

		var oOptions = {
			url : BASEURL+"transfers/add_selected_transfer_notes",
			type : "POST",
			data : {"note_information": JSON.stringify(oData), "transfer_id": iGetTransferID.value},
			async : false,
			success: function(oResponse)
			{
				var options = {title : "Add Transfer Notes", message : "Successfully Added", speed : "slow", withShadow : true, type : "success",};
				$("body").feedback(options);
				console.log(JSON.stringify(oResponse.data));

				$("#display-transfer-notes").load(location.href + " #display-transfer-notes", function(){
					
					_displayTransferNotes(oResponse.data);
				});

				
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	function _displayTransferNotes(oNoteItems)
	{


		var uiNotesContainer = $("#display-transfer-notes"),
			uiNotesTemplate = $(".display-transfer-notes-row");

			uiNotesContainer.html("");

			for(var x in oNoteItems)
			{
				var oNotes = oNoteItems[x];

				var uiNotesCloneTemplate = uiNotesTemplate.clone();
				uiNotesCloneTemplate.removeClass("display-transfer-notes-row").show();
				uiNotesCloneTemplate.data(oNotes);
				uiNotesCloneTemplate.find(".display-transfer-notes-subject").html("Subject: "+oNotes.subject);
				uiNotesCloneTemplate.find(".display-transfer-notes-date-created").html("Date/Time: "+oNotes.date_formatted);
				uiNotesCloneTemplate.find(".display-transfer-notes-message").html(oNotes.note);

				uiNotesContainer.append(uiNotesCloneTemplate);
				// console.log(JSON.stringify(oNotes));
			}

	}

	function _itemsEvent()
	{
		var methods = {
			displayToSelect : function(oData, bShowResult){
				var oTimeOut = {},
					uiSelect = $('#selectProductListSelect'),
					uiParent = uiSelect.parent(),
					uiSibling = uiParent.find('.frm-custom-dropdown');
				// uiSibling.remove();

				// uiSelect.removeClass("frm-custom-dropdown-origin");

				// uiSelect.html("");

				for(var i in oData){
					var sHtml = '<option value="'+oData[i]['id']+'">'+oData[i]['name']+'</option>';
					uiSelect.append(sHtml);
				}

				uiSelect.transformDD();
				CDropDownRetract.retractDropdown(uiSelect);



				var uiInput = uiParent.find('input.dd-txt');


				if(bShowResult){
					uiParent.find('.frm-custom-icon').click();
					uiInput.focus();
				}

				

				uiInput.off("keyup").on("keyup", function(e){
					var val = $(this).val();
					uiInput.attr("value",val);
					
					clearTimeout(oTimeOut);
					if(val.length > 0 && e.keyCode !=8 && e.keyCode !=48){
						oTimeOut = setTimeout(function(){
							methods.search(val, true);
						}, 3000);	
					}
					

				});
			},
			getItem : function(id){
				for(var i in oItems){
					if(oItems[i]["id"] == id){
						return oItems[i];
					}
				}
			},
			search : function(sKey, bShowResult){
				var oOptions = {
					type : "POST",
					data : {
						key : sKey,
						limit : 30,
						sorting : "asc",
						sort_field : "name"
					},
					url : BASEURL + "products/search",
					returnType : "json",
					success : function(oResult) {
						oItems = oResult.data;
						methods.displayToSelect(oItems, bShowResult);
					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);
			}
		};

		methods.search("");
	}

	function _createProductItem()
	{
		var btnAddSelectedProduct = $("#add-selected-product"),
			uiProductListDropdown = $(".product-list-dropdown"),
			oProductList = "",
			uidisplaySelectedProduct = $("#displaySelectedProduct"),
			oGatherDataFromProducts = {},
			oStoredDataFromProducts = [];

			var getProductInterval = setInterval(function(){
				oProductList = cr8v_platform.localStorage.get_local_storage({name:"getAllProducts"});
				
				if(oProductList !== null)
				{
					clearInterval(getProductInterval);
				}

			}, 300);




			btnAddSelectedProduct.off("click.addItem").on("click.addItem", function(e){
				e.preventDefault();

				var iProductListID = uiProductListDropdown.val(),
					uiParent = $("#displaySelectedProduct").find(".record-item"),
					iCountIt = 0;
					
						for(var x in oProductList)
						{
							if(iProductListID == oProductList[x]["id"])
							{
								oStoredDataFromProducts.push(oProductList[x]);
								_displayCreatedProductItem(oStoredDataFromProducts);
								
							}
						}




					_displayLeftProductList(iProductListID);

					oStoredDataFromProducts = [];
				
			});
	}

	function _displayCreatedProductItem(oData)
	{
		var uiDisplaySelectedProduct = $("#displaySelectedProduct"),
			uiTemplate = $(".item-display-product");

			// uiDisplaySelectedProduct.html("");
			// console.log(JSON.stringify(oData));

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("item-display-product").show();
					uiCloneTemplate.data(item);
					uiCloneTemplate.find(".set-product-image").attr("src", item.image);
					uiCloneTemplate.find(".set-product-sku-and-name").html("SKU# "+item.sku+" - "+item.name);
					uiCloneTemplate.find(".set-product-description").html(item.description);

					uiCloneTemplate.find(".set-product-by-bulk").attr("id", "byBulk"+item.id);
					uiCloneTemplate.find(".set-product-for-bulk").attr("for", "byBulk"+item.id);
					uiCloneTemplate.find(".set-product-by-bags").attr("id", "byBags"+item.id);
					uiCloneTemplate.find(".set-product-for-bags").attr("for", "byBags"+item.id);

					uiCloneTemplate.find(".set-product-by-bags").attr("name", "bag-or-bulk"+item.id);
					uiCloneTemplate.find(".set-product-by-bulk").attr("name", "bag-or-bulk"+item.id);

					// console.log(item.image);
					uiDisplaySelectedProduct.append(uiCloneTemplate);
					uiCloneTemplate.css({"display":"inline-block"});

					uiCloneTemplate.attr("prod-id", item.id);

					_showAddBatchModal(uiCloneTemplate);
					_deleteProductItem(uiCloneTemplate);
					_checkMethodToTransfer(uiCloneTemplate);
					

					oBatchPerItem[item.product_inventory_id] = {};

					sTransferMethod = "Bulk";
			}

	}

	function _checkMethodToTransfer(ui)
	{
		uiParent = ui,
		uiSetProductByBulk = uiParent.find(".set-product-by-bulk"),
		uiSetProductByBags = uiParent.find(".set-product-by-bags"),
		uiUnitOfMeasure = uiParent.find(".unit-of-measure");
		// uiMainParent = uiParent.closest(".record-item");

		uiSetProductByBulk.off("click.bulk").on("click.bulk", function(){
			var uiThis = $(this),
				uiMainParent = uiThis.closest(".record-item"),
				uiUnitOfMeasureSecond = uiMainParent.find(".unit-of-measure");

			uiUnitOfMeasureSecond.attr("disabled", false);
			uiUnitOfMeasureSecond.css({"background-color": "white"});
			sTransferMethod = "Bulk";

			uiMainParent.data("transfer_method", sTransferMethod);

		});

		uiSetProductByBags.off("click.bag").on("click.bag", function(){
			var uiThis = $(this),
				uiMainParent = uiThis.closest(".record-item"),
				uiUnitOfMeasureSecond = uiMainParent.find(".unit-of-measure");

			uiUnitOfMeasureSecond.attr("disabled", true);
			uiUnitOfMeasureSecond.css({"background-color": "gray"});
			sTransferMethod = "Bags";

			uiMainParent.data("transfer_method", sTransferMethod);
			
		});



	}

	function _deleteProductItem(uiCloneTemplate)
	{
		var uiParent = uiCloneTemplate,
			oItemData = uiCloneTemplate.data(),
			btnDeleteItem = uiParent.find(".deleteItem"),
			oGetProdData = cr8v_platform.localStorage.get_local_storage({name:"getAllProducts"});


			btnDeleteItem.off("click.delete").on("click.delete", function(){

				$("body").css({overflow:'hidden'});
					
				$("#trashModal").addClass("showed");

				$("#trashModal .close-me").on("click",function(){
					$("body").css({'overflow-y':'initial'});
					$("#trashModal").removeClass("showed");
				});

				var btnConfirmDelete = $("#confirmDelete");

				btnConfirmDelete.off("click.removeNow").on("click.removeNow", function(e){
					e.preventDefault();
					uiParent.remove();

					for(var x in oGetProdData)
					{
						if(oItemData.id == oGetProdData[x]["id"])
						{
							var oNewProd = {
								id : oGetProdData[x]["id"],
								name : oGetProdData[x]["name"],
								sku : oGetProdData[x]["sku"],
								description : oGetProdData[x]["description"],
								image : oGetProdData[x]["image"],
								product_inventory_id : oGetProdData[x]["product_inventory_id"],
							}
							oDuplicateProducts.push(oNewProd);

							$.each(oCollectUseProd, function(i, el){
							    if (this.id == oItemData.id){
							       oCollectUseProd.splice(i, 1);

							    }
							});

							$.each(oCollectNewDisplayProdEdit, function(i, el){
							    if (this.id == oItemData.id){
							       oCollectNewDisplayProdEdit.splice(i, 1);

							    }
							});

							
							
							$(".product-list-dropdown").load(location.href + " .product-list-dropdown", function(){

					       		_displayAllProductList(oDuplicateProducts);
					       	});
							


						}
					}


					$(".modal-close.close-me").trigger("click");

				});
				
			});
		
	}

	function _showAddBatchModal(ui)
	{
		var uiParent = ui,
			oItemData = uiParent.data(),
			oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
			btnAddBatch = uiParent.find('[modal-target="add-batch"]'),
			uiAddBatchModal = $('[modal-id="add-batch"]');

			btnAddBatch.off("click.showModal").on("click.showModal", function(){
				uiAddBatchModal.data(oItemData);

				// console.log(JSON.stringify(oItemData));

				_saveNewBatch();

				// console.log(oItemData);

				var uiDisplayBatchProductNameSku = uiAddBatchModal.find(".display-batch-product-name-sku"),
					uiDisplayBatchProductTransferBalance = uiAddBatchModal.find(".display-batch-product-transfer-balance"),
					uiDisplayBatchStorageLocation = uiAddBatchModal.find(".display-batch-storage-location"),
					uiSetBatchTitle = uiAddBatchModal.find("#set-batch-title"),
					uiThis = $(this),
					uiParentSecond = uiThis.closest(".item-record"),
					uiCheckMethodToUse = uiParentSecond.find('.method-select-radio'),
					uiSetProductByBulk = uiParentSecond.find(".set-product-by-bulk"),
					uiSetProductByBags = uiParentSecond.find(".set-product-by-bags"),
					uiUnitOfMeasureID = uiParentSecond.find(".unit-of-measure"),
					sResult = "";


					uiDisplayBatchStorageLocation.html("");

					uiSetBatchTitle.html("Add Batch");
			
					uiDisplayBatchProductNameSku.html("SKU No. "+oItemData.sku+" - "+oItemData.name);
					uiDisplayBatchProductTransferBalance.html(oItemData.unit_price);

					_displayBatchSelect(oItemData.batch_info, oItemData.id);



					// console.log(JSON.stringify(oItemData.batch_info));
					if(uiSetProductByBulk.is(":checked") === true)
					{
						sResult = uiSetProductByBulk.val();
		
					}else if(uiSetProductByBags.is(":checked") === true){
						sResult = uiSetProductByBags.val();
		
					}
					
					if(sResult == 'bulk')
					{
						for(var x in oUnitOfMeasures)
						{
							if(oUnitOfMeasures[x]["id"] == uiUnitOfMeasureID.val())
							{
								sbatchMethod = oUnitOfMeasures[x]["name"];
							}
						}

						
					}else if(sResult == 'bag')
					{

						sbatchMethod = 'bag';
					}
	
					// console.log(sbatchMethod);

					// _addBatchForProduct();
					_displayBatchStorages(oStorages);
					
					// console.log(JSON.stringify(oStorages));
					


				uiAddBatchModal.addClass("showed");

				$('[modal-id="add-batch"]').find(".close-me").off("click.closeModal").on("click.closeModal", function(){
					uiAddBatchModal.removeClass("showed");
				});

				

			});


	}
	function _displayBatchSelect(oData, prodID)
	{

		var oBatchInfo = oBatchInfos;
			uiBatchListDropdownContainer = $(".set-batch-name"),
			uiParentDropdown = uiBatchListDropdownContainer.parent(),
			uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');

			uiSiblingDropdown.remove();
			uiBatchListDropdownContainer.removeClass("frm-custom-dropdown-origin");

			uiBatchListDropdownContainer.html("");
			// console.log(JSON.stringify(oBatchInfo));

			if(Object.keys(oBatchInfo).length > 0){
				var sHtml = '';

				var iMainCout = 0;
				
				for(var x in oBatchInfo)
				{
					if(prodID == oBatchInfo[x]["product_id"])
					{
						var iCountIt = 0;

						for(var i in oData)
						{
							var obatchIDs = oData[i]["batch_id"];
							if(obatchIDs == oBatchInfo[x]['batch_id'])
							{
								iCountIt++;
							}
						}

						if(iCountIt == 0)
						{
								iMainCout++;
								sHtml += '<option value="'+oBatchInfo[x]['batch_id']+'">'+oBatchInfo[x]['name']+'</option>';
						}
	
					}
				}
				if(iMainCout == 0)
				{
					sHtml += '<option value="">No Available Batch Found</option>';

				}

				uiBatchListDropdownContainer.append(sHtml);
				
			}else{

				sHtml ='<option value="">No Batch Found</option>';
				uiBatchListDropdownContainer.append(sHtml);

			}

		
		uiBatchListDropdownContainer.transformDD();
		CDropDownRetract.retractDropdown(uiBatchListDropdownContainer);

		_selectStorageLocation(uiBatchListDropdownContainer);
	
	}

	function _displayEditBatchSelect(prodID, selectedBatch, oData)
	{

		var oBatchInfo = oBatchInfos;
			uiBatchListDropdownContainer =  $(".set-batch-name"),
			uiParentDropdown = uiBatchListDropdownContainer.parent(),
			uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');

			uiSiblingDropdown.remove();
			uiBatchListDropdownContainer.removeClass("frm-custom-dropdown-origin");

			uiBatchListDropdownContainer.html("");
			// console.log(JSON.stringify(oBatchInfo));

			var iMainCout = 0;

			if(Object.keys(oBatchInfo).length > 0){
				var sHtml = '<option value="">Select Batch Name</option>';
				
				for(var x in oBatchInfo)
				{
					if(prodID == oBatchInfo[x]["product_id"])
					{
						if(selectedBatch == oBatchInfo[x]["batch_id"])
						{
							sHtml += '<option value="'+oBatchInfo[x]['batch_id']+'" selected>'+oBatchInfo[x]['name']+'</option>';
						}

						var iCountIt = 0;

						for(var i in oData)
						{
							var obatchIDs = oData[i]["batch_id"];
							if(obatchIDs == oBatchInfo[x]['batch_id'])
							{
								iCountIt++;
							}
						}

						if(iCountIt == 0)
						{
								iMainCout++;
								sHtml += '<option value="'+oBatchInfo[x]['batch_id']+'">'+oBatchInfo[x]['name']+'</option>';
						}
						
					}
				}
				if(iMainCout == 0)
				{
					sHtml += '<option value="">No Other Batch Found</option>';

				}

				uiBatchListDropdownContainer.append(sHtml);
			}else{

				sHtml ='<option value="">No Batch Found</option>';
				uiBatchListDropdownContainer.append(sHtml);

			}

		
		uiBatchListDropdownContainer.transformDD();

		_selectStorageLocation(uiBatchListDropdownContainer);
	
	}

	function _saveNewBatch()
	{


		var uiModalBatch = $('[modal-id="add-batch"]'),
			oItemData = uiModalBatch.data(),
			btnConfirmAddBatch = $("#confirmAddBatch"),
			uiSetBatchName = $(".set-batch-name"),
			uiSetBatchTitle = $("#set-batch-title"),
			uiParent = $("#breadCrumbsHierarchy"),
			uiSetTemp = $(".item-bread.temporary"),
			uiGetTempData = uiSetTemp.data(),
			oError = [];

			btnConfirmAddBatch.off("click.addbatch").on("click.addbatch", function(){

				// console.log(JSON.stringify(uiGetTempData));
				// 	console.log(uiGetTempData);

				(uiSetBatchName.val() == "") ? oError.push("Select Batch Name First") : "";
				(typeof uiGetTempData == 'undefined') ? oError.push("Add Destination First") : "";

				

				// 	console.log(JSON.stringify(uiGetTempData));

				if(oError.length > 0)
				{
					$("body").feedback({title : "Add Batch", message : oError.join("<br />"), type : "danger", icon : failIcon});
				}else{

					var sBatchName = uiSetBatchTitle.text().trim(),
						oBuilbatch = "";


					if(Object.keys(sOldBatch).length > 0)
					{
						oBuilbatch = sOldBatch;
						// console.log(oBuilbatch);
					}else{
						oBuilbatch =  oItemData["old_batch_locations"];
						// console.log(oBuilbatch);
					}

					var oData = {
						product_id : oItemData.id,
						transfer_method : sTransferMethod,
						batch_id : uiSetBatchName.val(),
						old_batch : oBuilbatch,
						new_measurement : sbatchMethod,
						new_qty : sNewQty,
						new_location : uiGetTempData.id

					}

					// console.log(JSON.stringify(oData));


					if(sBatchName == "Add Batch"){

						_createNewBatchToDisplay(oData);

						$("body").feedback({title : "Add Batch", message : "Successfully Added", type : "success"});

					}else if(sBatchName == "Edit Batch"){
						_createUpdateBatchToDisplay(oData, oItemData.batch_id);

						$("body").feedback({title : "Edit Batch", message : "Successfully Updated", type : "success"});
					}

					// console.log(uiSetBatchName.text());


					$(".modal-close.close-me").trigger("click");
				}

				oError = [];

				
				
			});

	}

	function _createUpdateBatchToDisplay(oData, batch_id)
	{
		var iGetCountLocation = Object.keys(oData["old_batch"]).length,
			sMadeLocation = ""
			iCount = 1;

			// console.log(JSON.stringify(oData["old_batch"]));

		for(var x in oData["old_batch"])
		{
			if(iCount == iGetCountLocation){
 					sMadeLocation += '<p class="font-14 font-400 display-inline-mid padding-right-10 add-some-style">'+oData["old_batch"][x]["name"]+'</p>';
 				}else{
 					sMadeLocation += '<p class="font-14 font-400 display-inline-mid padding-right-10 add-some-style">'+oData["old_batch"][x]["name"]+'</p>'+
 									'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10 add-some-style"></i>';
 				}
 				iCount++;
		}

		var new_location_build = buildNewLocationBatch(oData["new_location"]),
			iNewCount = 1,
			sMadeNewLocation = "",
			iGetCountNewLocation = Object.keys(new_location_build).length;

		for(var x in new_location_build)
		{
			if(iNewCount == iGetCountNewLocation){
 					sMadeNewLocation += '<p class="font-14 font-400 display-inline-mid padding-right-10 add-some-style">'+new_location_build[x]["name"]+'</p>';
 				}else{
 					sMadeNewLocation += '<p class="font-14 font-400 display-inline-mid padding-right-10 add-some-style">'+new_location_build[x]["name"]+'</p>'+
 									'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10 add-some-style"></i>';
 				}
 				iNewCount++;
		}

		var oNewData = {
				product_id : oData["product_id"],
				transfer_method : oData["transfer_method"],
				batch_id : oData["batch_id"],
				old_batch : sMadeLocation,
				new_measurement : oData["new_measurement"],
				new_qty : oData["new_qty"],
				new_location : sMadeNewLocation,
				new_location_id :  oData["new_location"],
				old_location_id : iOldBatchID,
				old_batch_locations: oData["old_batch"],
				new_batch_locations: new_location_build
			}

			_displayUpdatedBatch(oNewData, batch_id);


	}

	function _createNewBatchToDisplay(oData)
	{


		var iGetCountLocation = Object.keys(oData["old_batch"]).length,
			sMadeLocation = ""
			iCount = 1;

			// console.log(JSON.stringify(oData["old_batch"]));

		for(var x in oData["old_batch"])
		{
			if(iCount == iGetCountLocation){
 					sMadeLocation += '<p class="font-14 font-400 display-inline-mid padding-right-10 add-some-style">'+oData["old_batch"][x]["name"]+'</p>';
 				}else{
 					sMadeLocation += '<p class="font-14 font-400 display-inline-mid padding-right-10 add-some-style">'+oData["old_batch"][x]["name"]+'</p>'+
 									'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10 add-some-style"></i>';
 				}
 				iCount++;
		}

		var new_location_build = buildNewLocationBatch(oData["new_location"]),
			iNewCount = 1,
			sMadeNewLocation = "",
			iGetCountNewLocation = Object.keys(new_location_build).length;

		for(var x in new_location_build)
		{
			if(iNewCount == iGetCountNewLocation){
 					sMadeNewLocation += '<p class="font-14 font-400 display-inline-mid padding-right-10 add-some-style">'+new_location_build[x]["name"]+'</p>';
 				}else{
 					sMadeNewLocation += '<p class="font-14 font-400 display-inline-mid padding-right-10 add-some-style">'+new_location_build[x]["name"]+'</p>'+
 									'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10 add-some-style"></i>';
 				}
 				iNewCount++;
		}

		var oNewData = {
				product_id : oData["product_id"],
				transfer_method : oData["transfer_method"],
				batch_id : oData["batch_id"],
				old_batch : sMadeLocation,
				new_measurement : oData["new_measurement"],
				new_qty : oData["new_qty"],
				new_location : sMadeNewLocation,
				new_location_id :  oData["new_location"],
				old_location_id : iOldBatchID,
				old_batch_locations: oData["old_batch"],
				new_batch_locations: new_location_build
			}

			_displayNewBatch(oNewData);


	}


	function _displayUpdatedBatch(oData, batch_ID)
	{
		// console.log(JSON.stringify(oData));

		var uiItemContainer = $(".record-item"),
			oBatchInfo = oBatchInfos,
			sGetBatchName = "";



			for(var x in oBatchInfo)
			{


				if(oBatchInfo[x]["batch_id"] == oData["batch_id"]){
					sGetBatchName = oBatchInfo[x]["name"];
					// console.log(sGetBatchName);
				}
			}


		$.each(uiItemContainer, function(){
			var uiThis = $(this),
				uiContainerID = uiThis.attr("prod-id");


				
			
				if(oData["product_id"] == uiContainerID)
				{

					

					var oNewBatch = {
						product_id : oData["product_id"],
						transfer_method : oData["transfer_method"],
						batch_id : oData["batch_id"],
						new_measurement : oData["new_measurement"],
						new_qty : oData["new_qty"],
						new_location_id :  oData["new_location_id"],
						old_location_id : iOldBatchID,
						old_batch_locations: oData["old_batch_locations"],
						new_batch_locations: oData["new_batch_locations"],
						old_batch : oData["old_batch"],
						new_batch : oData["new_location"],
					}

					var oBatchInfo = [];
					   oGetBatchInfo= uiThis.data();

					   $.each(oGetBatchInfo["batch_info"], function(i, el){
						    if (this.batch_id == batch_ID){
						        oGetBatchInfo["batch_info"].splice(i, 1);
						    }

						});


					   var uiDisplayNewBatchesOriginContainer = $(".display-new-batches-origin").find(".display-new-locations");

					   $.each(uiDisplayNewBatchesOriginContainer, function(){
					   		var uiThis = $(this),
					   			oSecondData = uiThis.data();

					   			if(oSecondData.batch_id == batch_ID)
					   			{
					   				uiThis.remove();
					   			}
					   });


					  
						

						if(oGetBatchInfo.hasOwnProperty("batch_info"))
						{
						  	if(Object.keys(oGetBatchInfo["batch_info"]).length > 0)
							{

								for(var x in oGetBatchInfo["batch_info"])
								{
									var oDataGather = {
										product_id : oGetBatchInfo["batch_info"][x]["product_id"],
										transfer_method : oGetBatchInfo["batch_info"][x]["transfer_method"],
										batch_id : oGetBatchInfo["batch_info"][x]["batch_id"],
										new_measurement : oGetBatchInfo["batch_info"][x]["new_measurement"],
										new_qty : oGetBatchInfo["batch_info"][x]["new_qty"],
										new_location_id :  oGetBatchInfo["batch_info"][x]["new_location_id"],
										old_location_id : oGetBatchInfo["batch_info"][x]["old_location_id"],
										old_batch_locations: oGetBatchInfo["batch_info"][x]["old_batch_locations"],
										new_batch_locations: oGetBatchInfo["batch_info"][x]["new_batch_locations"],
										old_batch : oGetBatchInfo["batch_info"][x]["old_batch"],
										new_batch : oGetBatchInfo["batch_info"][x]["new_location"]
									}
									oBatchInfo.push(oDataGather);
								}

								
								oBatchInfo.push(oNewBatch);
								
								
							}else{
								oBatchInfo.push(oNewBatch);
							}

					  	}else{
							oBatchInfo.push(oNewBatch);
						}

					

						uiThis.data("batch_info", oBatchInfo);

					var uiNewContainer = uiThis.find(".display-new-batches-origin");


						var uiTemplate ='<span  class="product-location display-new-locations" >'+
											'	<div class="f-left">'+
											'		<p class="display-inline-mid font-14 font-bold padding-right-20 padding-top-10">Batch Name:</p>'+
											'		<p class="display-inline-mid font-14 font-400 padding-top-10 display-new-batch-name"></p>'+
											'	</div>'+
											'	<p class="f-right font-14 font-bold padding-bottom-20 display-new-quantity"></p>'+
											'	<div class="clear"></div>'+
											'	<table class="tbl-4c3h">'+
											'		<thead>'+
											'			<tr>'+
											'				<th class="width-50percent black-color">Origin</th>'+
											'				<th class="width-50percent">Destination</th>'+
											'			</tr>'+
											'		</thead>'+
											'	</table>'+
											'		<div class=" font-0 tbl-dark-color margin-bottom-20 position-rel" >'+
											'			<div class="width-100percent ">'+
											'				<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25 display-batch-origin"></div>'+
											'				<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25 display-batch-destination"></div>'+
											'			</div>'+
											'			<div class="edit-hide" style="height: 100%; padding: 20px 0;">'+
											'				<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid edit-this-batch"  modal-target="edit-batch">Edit Batch</button>'+
											'				<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 remove-this-batch" >Remove Batch</button>'+
											'			</div>'+
											'		</div>'+
											'</span>';

							var uiThisTemp = $(uiTemplate);
							// uiNewContainer.append(uiThisTemp);
							// 			uiCloneTemplate.data(oData);


							// 	console.log(JSON.stringify(oData));
						
							var uiCloneTemplate = uiThisTemp.clone(),
								item = oData;
								// console.log(item);

							uiCloneTemplate.removeClass("product-location").show();
							uiCloneTemplate.data(oData);
							uiCloneTemplate.find(".display-batch-origin").html(item.old_batch);
							uiCloneTemplate.find(".display-batch-destination").html(item.new_location);
							uiCloneTemplate.find(".display-new-quantity").html("Quantity "+item.new_qty+" "+item.new_measurement );
							uiCloneTemplate.find(".display-new-batch-name").html(sGetBatchName);

						uiNewContainer.prepend(uiCloneTemplate);


						_showEditRemoveBatch(uiCloneTemplate);
						_removeSelectedBatch(uiCloneTemplate);
						_editSelectedBatch(uiCloneTemplate);


						_displayBatchStorages(oStorages);


						
							// var uiCloneTemplate = uiTemplate.clone();



							// uiCloneTemplate.removeClass("product-location").show();
							// uiCloneTemplate.data(oData);
							// uiCloneTemplate.find(".display-batch-origin").html(oData["old_batch"]);
							// uiCloneTemplate.find(".display-batch-destination").html(oData["new_location"]);
							// uiCloneTemplate.find(".display-new-quantity").html("Quantity "+oData["new_qty"]+" "+oData["new_measurement"].toUpperCase());
							// uiCloneTemplate.find(".display-new-batch-name").html(sGetBatchName);

							// uiNewContainer.prepend(uiCloneTemplate);

							// uiNewContainer.css({"display":"block"});

							// _showEditRemoveBatch(uiCloneTemplate);
							// _removeSelectedBatch(uiCloneTemplate);
							// _editSelectedBatch(uiCloneTemplate);


							// _displayBatchStorages(oStorages);
						
						
					// uiDisplayBatchOrigin.html(oData["old_batch"]);



				}

		});
	}


	function _displayNewBatch(oData)
	{
		// console.log(JSON.stringify(oData));

		var uiItemContainer = $(".record-item"),
			oBatchInfo = oBatchInfos,
			sGetBatchName = "";

			for(var x in oBatchInfo)
			{

				if(oBatchInfo[x]["batch_id"] == oData["batch_id"]){
					sGetBatchName = oBatchInfo[x]["name"];
					// console.log(sGetBatchName);
				}
			}


		$.each(uiItemContainer, function(){
			var uiThis = $(this),
				uiContainerID = uiThis.attr("prod-id");

				if(oData["product_id"] == uiContainerID)
				{			


					var oNewBatch = {
						product_id : oData["product_id"],
						transfer_method : oData["transfer_method"],
						batch_id : oData["batch_id"],
						new_measurement : oData["new_measurement"],
						new_qty : oData["new_qty"],
						new_location_id :  oData["new_location_id"],
						old_location_id : iOldBatchID,
						old_batch_locations: oData["old_batch_locations"],
						new_batch_locations: oData["new_batch_locations"],
						old_batch : oData["old_batch"],
						new_batch : oData["new_location"],

					}

					var oBatchInfo = [];
					   oGetBatchInfo= uiThis.data();


						if(oGetBatchInfo.hasOwnProperty("batch_info"))
						{
						  	if(Object.keys(oGetBatchInfo["batch_info"]).length > 0)
							{

								for(var x in oGetBatchInfo["batch_info"])
								{
									var oDataGather = {
										product_id : oGetBatchInfo["batch_info"][x]["product_id"],
										transfer_method : oGetBatchInfo["batch_info"][x]["transfer_method"],
										batch_id : oGetBatchInfo["batch_info"][x]["batch_id"],
										new_measurement : oGetBatchInfo["batch_info"][x]["new_measurement"],
										new_qty : oGetBatchInfo["batch_info"][x]["new_qty"],
										new_location_id :  oGetBatchInfo["batch_info"][x]["new_location_id"],
										old_location_id : oGetBatchInfo["batch_info"][x]["old_location_id"],
										old_batch_locations: oGetBatchInfo["batch_info"][x]["old_batch_locations"],
										new_batch_locations: oGetBatchInfo["batch_info"][x]["new_batch_locations"],
										old_batch : oGetBatchInfo["batch_info"][x]["old_batch"],
										new_batch : oGetBatchInfo["batch_info"][x]["new_batch"]
									}
									oBatchInfo.push(oDataGather);
								}

								
								oBatchInfo.push(oNewBatch);
								
								
							}else{
								oBatchInfo.push(oNewBatch);
							}

					  	}else{
							oBatchInfo.push(oNewBatch);
						}

				
						uiThis.data("batch_info", oBatchInfo);


						var uiNewContainer = uiThis.find(".display-new-batches-origin");

						var uiTemplate ='<span  class="product-location display-new-locations" >'+
											'	<div class="f-left">'+
											'		<p class="display-inline-mid font-14 font-bold padding-right-20 padding-top-10">Batch Name:</p>'+
											'		<p class="display-inline-mid font-14 font-400 padding-top-10 display-new-batch-name"></p>'+
											'	</div>'+
											'	<p class="f-right font-14 font-bold padding-bottom-20 display-new-quantity"></p>'+
											'	<div class="clear"></div>'+
											'	<table class="tbl-4c3h">'+
											'		<thead>'+
											'			<tr>'+
											'				<th class="width-50percent black-color">Origin</th>'+
											'				<th class="width-50percent">Destination</th>'+
											'			</tr>'+
											'		</thead>'+
											'	</table>'+
											'		<div class=" font-0 tbl-dark-color margin-bottom-20 position-rel" >'+
											'			<div class="width-100percent ">'+
											'				<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25 display-batch-origin"></div>'+
											'				<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25 display-batch-destination"></div>'+
											'			</div>'+
											'			<div class="edit-hide" style="height: 100%; padding: 20px 0;">'+
											'				<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid edit-this-batch"  modal-target="edit-batch">Edit Batch</button>'+
											'				<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 remove-this-batch" >Remove Batch</button>'+
											'			</div>'+
											'		</div>'+
											'</span>';

							var uiThisTemp = $(uiTemplate);
							// uiNewContainer.append(uiThisTemp);
							// 			uiCloneTemplate.data(oData);



						
							var uiCloneTemplate = uiThisTemp.clone(),
								item = oData;
								// console.log(item);

							uiCloneTemplate.removeClass("product-location").show();
							uiCloneTemplate.data(oData);
							uiCloneTemplate.find(".display-batch-origin").html(item.old_batch);
							uiCloneTemplate.find(".display-batch-destination").html(item.new_location);
							uiCloneTemplate.find(".display-new-quantity").html("Quantity "+item.new_qty+" "+item.new_measurement );
							uiCloneTemplate.find(".display-new-batch-name").html(sGetBatchName);

							uiNewContainer.append(uiCloneTemplate);


							_showEditRemoveBatch(uiCloneTemplate);
							_removeSelectedBatch(uiCloneTemplate);
							_editSelectedBatch(uiCloneTemplate);


							_displayBatchStorages(oStorages);
						

						



				}

		});
	}

	function _showEditRemoveBatch(ui)
	{
		_displayBatchStorages(oStorages);
		var uiParent = ui.closest(".display-new-locations"),
			uiEditHover = uiParent.find(".edit-hide");


			uiEditHover.off("mouseenter.hoverit").on("mouseenter.hoverit", function(){
				$(this).css({"opacity":"1"});
			});
			uiEditHover.off("mouseleave.hoverit").on("mouseleave.hoverit", function(){
				$(this).css({"opacity":"0"});

			});

	}

	function _removeSelectedBatch(ui)
	{
		
		var uiParent = ui.closest(".record-item"),
			oItemData = uiParent.data(),
			btnRemoveThisBatch = uiParent.find(".remove-this-batch");

			btnRemoveThisBatch.off("click.removeit").on("click.removeit", function(){
				var uiThis = $(this),
					uiSelectedBatch = uiThis.closest(".display-new-locations"),
					oSeletedItemData = uiSelectedBatch.data();
					
					$("body").css({overflow:'hidden'});

					$("#trashModal").addClass("showed");

					$("#trashModal .close-me").on("click",function(){
						$("body").css({'overflow-y':'initial'});
						$("#trashModal").removeClass("showed");
					});

					var btnconfirmDelete = $("#confirmDelete");

					btnconfirmDelete.off("click.remove").on("click.remove", function(){
						$.each(oItemData["batch_info"], function(i, el){
							if (this.batch_id == oSeletedItemData["batch_id"]){
							   oItemData["batch_info"].splice(i, 1);

							   uiSelectedBatch.remove();
							}
						});

						uiParent.data(oItemData);

						$(".modal-close.close-me").trigger("click");
					});


					

			});
	}

	function _editSelectedBatch(ui)
	{
		
		var uiParent = ui.closest(".record-item"),
			oItemData = uiParent.data(),
			oBatchInfo = oBatchInfos,
			uiParentSecond = ui.closest(".display-new-locations"),
			btnEditBatchModal = uiParentSecond.find('.edit-this-batch'),
			uiEditBatchModal = $('[modal-id="edit-batch"]'),
			uiAddBatchModal = $('[modal-id="add-batch"]'),
			oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"});


			btnEditBatchModal.off("click.showModal").on("click.showModal", function(){
				var uiThis = $(this),
					uiSelectedBatch = uiThis.closest(".display-new-locations"),
					oSeletedItemData = uiSelectedBatch.data();

					// console.log(JSON.stringify(oSeletedItemData));

					uiAddBatchModal.data(oSeletedItemData);

					// _displayBatchStorages(oStorages);

					// console.log(JSON.stringify(oSeletedItemData));

				$("body").css({overflow:'hidden'});
				
				$('[modal-id="add-batch"]').addClass("showed");

				$('[modal-id="add-batch"] .close-me').on("click",function(){
					$("body").css({'overflow-y':'initial'});
					$('[modal-id="add-batch"]').removeClass("showed");
					
				});


					var uiSetBatchTitle = $("#set-batch-title"),
						uiEditBatchProductNameSku = uiAddBatchModal.find(".display-batch-product-name-sku"),
						uiSetEditBatchName = uiAddBatchModal.find(".set-batch-name"),
						uiEditBatchStorageLocation = uiAddBatchModal.find(".display-batch-storage-location"),
						uiSetEditBatchQty = $("#set-new-batch-qty"),
						uiEditLocationItem = $("#location-item"),
						uiParentSecond = uiThis.closest(".item-record"),
						uiCheckMethodToUse = uiParentSecond.find('.method-select-radio'),
						uiSetProductByBulk = uiParentSecond.find(".set-product-by-bulk"),
						uiSetProductByBags = uiParentSecond.find(".set-product-by-bags"),
						uiUnitOfMeasureID = uiParentSecond.find(".unit-of-measure"),
						sResult = "";

						uiSetBatchTitle.html("Edit Batch");


						_displayEditBatchSelect(oSeletedItemData.product_id, oSeletedItemData.batch_id, oItemData["batch_info"]);

						uiEditBatchProductNameSku.html("SKU No. "+oItemData.sku+" - "+oItemData.name);
						uiSetEditBatchQty.html(oSeletedItemData.new_qty+" "+oSeletedItemData.new_measurement.toUpperCase());
						uiEditLocationItem.html(oSeletedItemData.old_batch);


						if(uiSetProductByBulk.is(":checked") === true)
						{
							sResult = uiSetProductByBulk.val();
			
						}else if(uiSetProductByBags.is(":checked") === true){
							sResult = uiSetProductByBags.val();
			
						}
						
						if(sResult == 'bulk')
						{
							for(var x in oUnitOfMeasures)
							{
								if(oUnitOfMeasures[x]["id"] == uiUnitOfMeasureID.val())
								{
									sbatchMethod = oUnitOfMeasures[x]["name"];
								}
							}

							
						}else if(sResult == 'bag')
						{

							sbatchMethod = 'bag';
						}

						var uiAddSomeStyle = $(".add-some-style");

						uiAddSomeStyle.css({"margin-bottom":"10px"});

						for(var x in oBatchInfo)
						{
							if(oBatchInfo[x]["batch_id"] == oSeletedItemData.batch_id)
							{

								uiEditBatchStorageLocation.html(oBatchInfo[x]["storage_name"]);
								_displayBatches(uiAddBatchModal, oSeletedItemData.product_id);
							}
						}


			});

	}

	function _displayBatches(ui, prodID)
	{
		_displayBatchStorages(oStorages);
		var uiParent = ui,
			oItemData = uiParent.data(),
			breadCrumbsHierarchy = $("#breadCrumbsHierarchy"),
			uiBreadParent = breadCrumbsHierarchy.parent(),
			batchDisplay = $("#batchDisplay"),
			oHierarchy = oItemData.new_batch_locations,
			oQueue = {},
			oBatchData = {};

			// console.log(JSON.stringify(oHierarchy));




		batchDisplay.css("opacity", 0);
		var uiLoader = $("<div loader='true'></div>");
		uiLoader.css({
			"position" : "absolute",
			"top" : "0px",
			"left" : "0px",
			"width" : "100%",
			"height" : "100%",
			"background-color" : "#444",
			"color" : "#fff",
			"text-align" : "center"
		});
		var uiSpinner = $('<div><i class="fa fa-cog fa-spin fa-3x fa-fw margin-bottom"></i>Loading..</div>');
		uiSpinner.css({
			"position" : "absolute",
			"width" : "30px",
			"height" : "30px",
			"top" : "40%",
			"left" : "50%",
			"margin-left" : "-15px",
			"margin-right" : "-15px"
		});
		uiLoader.html(uiSpinner);
		uiBreadParent.append(uiLoader);
		uiBreadParent.css("position", "relative");

		for(var i in oHierarchy){
			oQueue[i] = {};
			if(i == "0"){
				oQueue[i]["selector"] = "[storage-id='"+oHierarchy[i]["storage_id"]+"']";
			}else{
				oQueue[i]["selector"] = "[location-id='"+oHierarchy[i]["id"]+"']";
			}

			oQueue[i]["callback"] = function(){
				var oThis = this, 
					selector = oThis.selector,
					uiLocation = $(selector);

				uiLocation.click();
				setTimeout(function(){
					if(typeof oThis.nextcall == 'function'){
						uiLocation.click();
						setTimeout(function(){
							var oNext = oThis.nextobj;
							oThis.nextcall.apply(oNext);

							if(oNext.hasOwnProperty("last")){
								if(oNext.last){
									setTimeout(function(){
										batchDisplay.css("opacity", 1);
										$("[loader='true']").remove();
									},1300);
								}
							}
						},500);
					}
				}, 30);
			}
		}
		
		var timeoutCall = function(obj){
			setTimeout(function(){
				var que = obj,
					callback = que.callback;
				callback.apply(que);
			}, 500);
		}
		
		for(var i in oQueue){
			var queNext = oQueue[parseInt(i) + 1];
			if(queNext){
				oQueue[i]['callback'].bind(queNext);
				oQueue[i]['nextobj'] = queNext;
				oQueue[i]['nextcall'] = oQueue[parseInt(i) + 1]['callback'];
				oQueue[i]['nextcall_from'] = oQueue[i]["selector"];
				oQueue[i]['nextcall_to'] = oQueue[parseInt(i) + 1]["selector"];
			}else{
				oQueue[i]['last'] = true;
			}
			timeoutCall(oQueue[i]);
		}
	}

	function _selectStorageLocation(uiDropDown)
	{
		var uiBatchDropdown = uiDropDown.closest(".trigger-batch-dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-option").find(".option"),
			oBatchInfo = oBatchInfos,
			uiDisplayBatchStorageLocation = $(".display-batch-storage-location"),
			uiSetNewBatchQty =  $("#set-new-batch-qty"),
			uiDisplayLocations = $("#location-item"),
			uiGetValue = "";

			uiSetNewBatchQty.html("");
			uiDisplayLocations.html("");

		uiBatchDropdown.off("click.select").on("click.select", function(){
			var uiGetValue = $(this).attr("data-value"),
				iGetLocationID = "",
				oBatchQty = "";


			for(var x in oBatchInfo)
			{
				

				if(oBatchInfo[x]["batch_id"] ==  uiGetValue)
				{
					// console.log(JSON.stringify(oBatchInfo[x]));
					uiDisplayBatchStorageLocation.html(oBatchInfo[x]["storage_name"]);
					iGetLocationID = oBatchInfo[x]["location_id"];
					oBatchQty = oBatchInfo[x];
					// console.log(JSON.stringify(oBatchQty));
				}
			}

			// console.log(iGetLocationID);
			_buildLocationHierarchy(iGetLocationID);
			_convertBatchQty(oBatchQty);
			
		});


		uiBatchDropdown.trigger("click.select");
	}

	function _convertBatchQty(oData)
	{	

		var uiModalAddBatch = $('[modal-id="add-batch"]'),
 			uiParent = uiModalAddBatch.parent(),
 			uiSetNewBatchQty = uiParent.find("#set-new-batch-qty"),
			iResult = 0;

		iResult = UnitConverter.convert(oData["unit_of_measure_name"], sbatchMethod, oData["quantity"]);

		uiSetNewBatchQty.html(iResult+" "+sbatchMethod.toUpperCase());
		sNewQty = iResult;

		// console.log(oData["unit_of_measure_name"]);
		// console.log(sbatchMethod);
		// console.log(oData["quantity"]);
	}


	function buildNewLocationBatch(locationID)
	{
		 var iLocationId = locationID,
		    oCurrentLocation = null,
		    oLocationList = {},
		    iCounter = 0,
		    iCounterTwo = 0;

		    // console.log(iLocationId);

		    if(iLocationId != 0)
		    {
		    	for(var a in oLocations) {
			      if(oLocations[a].id == iLocationId) {
			        oCurrentLocation = oLocations[a];
			      }
			    }

			    oLocationList[iCounter] = oCurrentLocation;
			    ++iCounter;

			    var keyCounter = Object.keys(oLocations).length;

			    for(var b = 0; b < keyCounter;) {
			      if(oLocations[b].id == oCurrentLocation.parent_id) {
			        oLocationList[iCounter] = oLocations[b];
			        oCurrentLocation = oLocations[b];
			        ++iCounter;
			        b = -1;
			      }
			      ++b;
			    }


			    var createObjLocation = [];

			    for(var x in oLocationList)
			    {
			    	createObjLocation.push(oLocationList[x]);

			    }

			    var reverseIt = createObjLocation.reverse();

			    return reverseIt;
			   

			}
    
	}

	
	function _buildLocationHierarchy(locationID)
	{
	    var iLocationId = locationID,
	    oCurrentLocation = null,
	    oLocationList = {},
	    iCounter = 0,
	    iCounterTwo = 0;

	    // alert(iLocationId);

	    for(var a in oLocations) {
	      if(oLocations[a].id == iLocationId) {
	        oCurrentLocation = oLocations[a];
	      }
	    }

	    oLocationList[iCounter] = oCurrentLocation;
	    ++iCounter;

	    var keyCounter = Object.keys(oLocations).length;

	    for(var b = 0; b < keyCounter;) {
	      if(oLocations[b].id == oCurrentLocation.parent_id) {
	        oLocationList[iCounter] = oLocations[b];
	        oCurrentLocation = oLocations[b];
	        ++iCounter;
	        b = -1;
	      }
	      ++b;
	    }

 		// console.log(JSON.stringify(oLocationList));

	    var createObjLocation = [];

	    for(var x in oLocationList)
	    {
	    	createObjLocation.push(oLocationList[x]);

	    }

	    var reverseIt = createObjLocation.reverse();
	    _displayLocationItem(reverseIt);
	    iOldBatchID = iLocationId

	 
 	}

 	function _displayLocationItem(oData)
 	{

 		var uiModalAddBatch = $('[modal-id="add-batch"]'),
 			uiParent = uiModalAddBatch.parent(),
 			uiDisplayLocations = $("#location-item"),
 			sMadeLocation = "",
 			iCount = 1,
 			iGetCountLocation = Object.keys(oData).length;

 			for(var x in oData)
 			{
 				if(iCount == iGetCountLocation){
 					sMadeLocation += '<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10 "> '+oData[x]["name"]+'</p>';
 				}else{
 					sMadeLocation += '<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> '+oData[x]["name"]+'</p>'+
 									'<i class="display-inline-mid font-14 padding-right-10"><i class="fa fa-caret-right" aria-hidden="true"></i></i>';
 				}
 				iCount++;
 				
 			}

 			uiDisplayLocations.html(sMadeLocation);

 			sOldBatch = oData;


 	}

 	function _getBreadCrumbsHierarchy()
	{
		var breadCrumbsHierarchy = $("#breadCrumbsHierarchy"),
			uiMainBread = breadCrumbsHierarchy.find(".bggray-7cace5"),
			oMainBread = uiMainBread.data(),
			oResponse = {};
		
		oResponse[0] = oMainBread;

		var uiNextAllBread = uiMainBread.nextAll(".item-bread");
		$.each(uiNextAllBread, function(){
			var thisData = $(this).data();
			oResponse[Object.keys(oResponse).length] = thisData;
		});

		return oResponse;

	}

	function _displayBatchStorages(oStorages)
	{
		var batchModal = $('[modal-id="add-batch"]'),
			breadCrumbsHierarchy = $("#breadCrumbsHierarchy"),
			batchDisplay = $("#batchDisplay"),
			oSelectedUI = [],
			oDisplayedData = {},
			oToPlaceData = {},
			doubleClickLevel = 0;

		batchDisplay.html("");

		var sTemplate = '<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor hier-item">'+
				'<div class="display-inline-mid width-20percent overflow-hide half-border-radius">'+
					'<img src="" alt="images" class="width-100percent">'+
				'</div>'+
				'<div class="display-inline-mid">'+
					'<p class="font-14 font-400 location-name"></p>'+
					'<p class="font-12 font-400 italic qty-in-location"> </p>'+
				'</div>'+
			'</div>';

		var sBackArrow = '<i style="margin-right:4px;" class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small padding-right-10 border-black back-arrow"></i>',
			sNextIcon = '<span class="display-inline-mid padding-all-5 greater-than">&gt;</span>',
			sItemBread = '<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid item-bread"></p>',
			sMainBread = '<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5"> </p>',
			sActiveBread = '<span class="active-bread"></span>';
		

		var setActiveEvent = function(uiThis){
			batchModal.find(".bggray-light").removeClass('bggray-light active-hier');
			
			var uiTraget = uiThis,
				oData = uiTraget.data();

			if(!uiTraget.hasClass("hier-item")){
				uiTraget = uiTraget.closest(".hier-item");
				oData = uiTraget.data();
			}

			if(uiTraget.closest(".hier-item").length > 0){
				uiTraget.closest(".hier-item").addClass('bggray-light active-hier');
			}else if(uiTraget.hasClass("hier-item")){
				uiTraget.addClass('bggray-light');
			}

			if(Object.keys(oSelectedUI).length > 0){
				oToPlaceData = oData;
				var	uiNext = $(sNextIcon),
					uiBread = $(sItemBread);
				breadCrumbsHierarchy.find(".temporary").remove();
				uiNext.addClass("temporary");
				uiBread.addClass("temporary");
				breadCrumbsHierarchy.append(uiNext);
				uiBread.html(oData.name);
				uiBread.data(oData);
				breadCrumbsHierarchy.append(uiBread);
			}

			_saveNewBatch();

		}

		var displayBreadCrumbsStorage = function(){
			var uiBread = $(sMainBread),
				oData = selectedUI.data();
			uiBread.html(oData.name);
			
			breadCrumbsHierarchy.find(".greater-than").remove();
			

			breadCrumbsHierarchy.append(uiBread);
		}

		var displayBreadCrumbsItems = function(){
			breadCrumbsHierarchy.find(".greater-than").remove();
			breadCrumbsHierarchy.find(".item-bread").remove();
			breadCrumbsHierarchy.find(".bggray-7cace5").remove();

			for(var i in oSelectedUI){
				var uiMainBread = $(sMainBread),
					uiNext = $(sNextIcon),
					uiBread = $(sItemBread);
				 if(i == "0"){
				 	uiMainBread.html(oSelectedUI[i]["name"]);
				 	uiMainBread.data(oSelectedUI[i]);
					breadCrumbsHierarchy.append(uiMainBread);
				 }else{
					breadCrumbsHierarchy.append(uiNext);
					uiBread.html(oSelectedUI[i]["name"]);
					uiBread.data(oSelectedUI[i]);
					breadCrumbsHierarchy.append(uiBread);
				 }
			}
		}

		var selectHierarchy = function(uiThis){
			var oData = uiThis.data(),
				iLevel = oData.iLevel,
				sQtyInLocationParent = oData.sQtyInLocationParent,
				sFirstLevelID = "",
				oDataToDisplay = {};
			
			if(oData.children.length > 0){
				oSelectedUI[doubleClickLevel] = oData;
				doubleClickLevel++;
				oDisplayedData[doubleClickLevel] = oData.children;
				displayChildren(oData.children);
				displayBreadCrumbsItems();
			}
			
			
		}

		var displayChildren = function(oChildren){
			
			if(Object.keys(oChildren).length > 0){
				batchDisplay.html("");
			}
			for(var i in oChildren){
				var uiItem = $(sTemplate),
					data = oChildren[i];
				
				data["children"] = data.children;
				uiItem.find(".location-name").html(data.name);
				uiItem.find("img").attr("src", BASEURL+"assets/images/folder-circle-blue.png");
				uiItem.find(".qty-in-location").html("Number of Locations inside = "+Object.keys(data.children).length);
				// uiItem.find("img").attr("src", CGetProfileImage.getDefault(data.name));
				uiItem.data(data);
				uiItem.attr("location-id", data.id);

				uiItem.single_double_click(
					function(){
						setActiveEvent($(this));
					},
					function(){
						selectHierarchy($(this));
					}
				);

				batchDisplay.append(uiItem);
			}

		}

		var uiBackArrow = $(sBackArrow);

		uiBackArrow.on("click", function(){
			if(doubleClickLevel > 0){
				doubleClickLevel = doubleClickLevel - 1;
				delete oSelectedUI[doubleClickLevel];
				if(doubleClickLevel > 0){
					displayChildren(oDisplayedData[doubleClickLevel]);
				}else if(doubleClickLevel == 0){
					displayStorages(oStorages);
				}

				displayBreadCrumbsItems();
			}
		});

		breadCrumbsHierarchy.html("").append(uiBackArrow);	
		
		var makeHierarchy = function(oData){
			var flat = {};
			for(var i in oData){
				var k = oData[i]["id"];
				oData[i]["children"] = [];
				flat[k] = oData[i];
			}

			for (var i in flat) {
				var parentkey = flat[i].parent_id;
				if(flat[parentkey]){
					flat[parentkey].children.push(flat[i]);
				}
			}

			var root = [];
			for (var i in flat) {
				var parentkey = flat[i].parent_id;
				if (!flat[parentkey]) {
					root.push(flat[i]);
				}
			}
			return root;
		};

		var displayStorages = function(oStorages){
			batchDisplay.html("");
			currentDisplayedData = [];
			for(var i in oStorages){
				var uiItem = $(sTemplate),
					data = oStorages[i],
					oHierarchy = makeHierarchy(data.location);
				
				data["children"] = oHierarchy[0]["children"];
				uiItem.find(".location-name").html(data.name);
				// uiItem.find("img").attr("src", CGetProfileImage.getDefault(data.name));
				uiItem.find("img").attr("src", BASEURL+"assets/images/folder-circle-blue.png");
				uiItem.find(".qty-in-location").html(data.capacity).number(true, 2).append(" "+data.measure_name);
				batchDisplay.append(uiItem);
				uiItem.data(data);
				uiItem.data("sQtyInLocationParent", uiItem.find(".qty-in-location").html());
				uiItem.attr("storage-id", data.id);
				
				uiItem.single_double_click(
					function(){
						setActiveEvent($(this));
					},
					function(){
						selectHierarchy($(this));
					}
				);
			}

			oDisplayedData = {};
			oDisplayedData[0] = oStorages;
		}

		displayStorages(oStorages);
		
	}

	function _displayTransferInfo(oData)
	{
		// console.log(JSON.stringify(oData));
	
		_getCurrentDateTime();
		

		var uiDisplayOngoingTransferNumber = $("#display-ongoing-transfer-number"),
			uiDisplayOngoingTransferMode = $("#display-ongoing-transfer-mode"),
			uiDisplayOngoingTransferReason = $("#display-ongoing-transfer-reason"),
			uiDisplayOngoingTransferDateIssued = $("#display-ongoing-transfer-date-issued"),
			uiDisplayOngoingTransferRequested = $("#display-ongoing-transfer-requested"),
			uiDisplayOngoingTransferRequestedImg = $("#display-ongoing-transfer-requested-img");

			uiDisplayOngoingTransferNumber.html("Transfer No. "+oData["to_number"]);
			uiDisplayOngoingTransferMode.html(oData["mode_of_transfer"]);
			uiDisplayOngoingTransferReason.html(oData["reason"]);
			uiDisplayOngoingTransferDateIssued.html(oData["date_issued"]);
			uiDisplayOngoingTransferRequested.html(oData["requestor"]);
			uiDisplayOngoingTransferRequestedImg.attr("src", CGetProfileImage.getDefault(oData["requestor"]));

			_displayProducts(oData["product_info"]);

			_displayDocuments(oData["document_info"]);

			_displayTransferNotes(oData["notes_info"]);



		setTimeout(function(){
			$('#generate-data-view').removeClass('showed');
		},500);
	}

	function _displayProducts(oData)
	{


		var uiDisplayAllProductsContainer = $("#display-all-products"),
			uiTemplate = $(".display-this-item");

			uiDisplayAllProductsContainer.html("");

			// console.log(JSON.stringify(oData));

		for(var x in oData)
		{

			var item = oData[x];

			var uiCloneTemplate = uiTemplate.clone();
				uiCloneTemplate.removeClass("display-this-item").show();

				uiCloneTemplate.find(".set-product-sku-name").html("SKU #"+item.sku+" - "+item.name);
				uiCloneTemplate.find(".set-product-description").html(item.description);
				uiCloneTemplate.find(".set-product-method").html("By "+item.transfer_method);
				uiCloneTemplate.find(".unit-to-transfer").html(item.unit_measure_name.toUpperCase());
				uiCloneTemplate.find(".set-product-image").attr("src", item.image);


				uiCloneTemplate.find(".set-product-by-bulk").attr("id", "byBulk"+item.id);
				uiCloneTemplate.find(".set-product-for-bulk").attr("for", "byBulk"+item.id);
				uiCloneTemplate.find(".set-product-by-bags").attr("id", "byBags"+item.id);
				uiCloneTemplate.find(".set-product-for-bags").attr("for", "byBags"+item.id);

				uiCloneTemplate.find(".set-product-by-bags").attr("name", "bag-or-bulk"+item.id);
				uiCloneTemplate.find(".set-product-by-bulk").attr("name", "bag-or-bulk"+item.id);

				for(var i in item.batch_info)
				{
					if(item.batch_info[i]["batch_id"] == 0)
					{
						_triggerEditProductNew(uiCloneTemplate);
					}
				}

				// console.log(JSON.stringify(item));


				if(item.transfer_method == "Bulk")
				{
					uiCloneTemplate.find(".set-product-by-bulk").attr("checked", true);
					uiCloneTemplate.find(".set-product-by-bags").attr("checked", false);
					uiCloneTemplate.find(".unit-of-measure").attr("disabled", false).css({"background-color": "white"});
					sTransferMethod = "Bulk";


				}else if(item.transfer_method == "Bags")
				{

					uiCloneTemplate.find(".set-product-by-bulk").attr("checked", false);
					uiCloneTemplate.find(".set-product-by-bags").attr("checked", true);

					uiCloneTemplate.find(".unit-of-measure").attr("disabled", true).css({"background-color": "gray"});
					sTransferMethod = "Bags";

				}



				uiCloneTemplate.attr("prod-id", item.id);
				uiCloneTemplate.data(item);

			uiDisplayAllProductsContainer.append(uiCloneTemplate);

			_createCollapseProduct(uiCloneTemplate);

			_displayOngoingBatches(uiCloneTemplate, item);

			

			_triggerEditProduct(uiCloneTemplate);

			_displayOngoingUnitOfMeasure(uiCloneTemplate);

			_checkMethodToTransfer(uiCloneTemplate);

			_removeSelectedBatch(uiCloneTemplate);

			_showAddBatchModal(uiCloneTemplate);

			_saveSelectedProductOngoing(uiCloneTemplate);

			_displayBatchStorages(oStorages);
		}
	}


	function _displayOngoingBatches(ui, oData)
	{
		var uiParent = ui,
			uiDisplayNewBatchesOrigin = uiParent.find(".display-new-batches-origin"),
			uiSecondTemplate = uiParent.find(".product-location"),
			oStoredAllBatches = [];


			uiDisplayNewBatchesOrigin.html("");

			// console.log(JSON.stringify(oData["batch_info"]));

			for(var x in oData["batch_info"])
			{
				var batch = oData["batch_info"][x];
				

				

				if(batch.new_location_id != "0" && batch.old_location_id != "0")
				{
					// console.log(batch.new_location_id);

					// console.log(JSON.stringify(batch));
					

					var oNewLocation = "",
						oOldLocation = "",
						sNewLocation = "",
						sOldLocation = "";


							oNewLocation = buildNewLocationBatch(batch.new_location_id);
							oOldLocation = buildNewLocationBatch(batch.old_location_id);
							
							
								sNewLocation = buildViewLocationBatch(oNewLocation);
								sOldLocation = buildViewLocationBatch(oOldLocation);


								var uiCloneTemplate = uiSecondTemplate.clone();
									uiCloneTemplate.removeClass("product-location");

									uiCloneTemplate.find(".display-new-batch-name").html(batch.batch_name);
									uiCloneTemplate.find(".display-new-quantity").html("Quantity "+batch.quantity+" "+batch.unit_measure_name.toUpperCase());
									uiCloneTemplate.find(".display-batch-origin").html(sOldLocation);
									uiCloneTemplate.find(".display-batch-destination").html(sNewLocation);

									var oBatchMake = {
										product_id : oData["id"],
										id : oData["id"],
										transfer_method : oData["transfer_method"],
										batch_id : batch.batch_id,
										old_batch : sOldLocation,
										new_measurement : batch.unit_measure_name,
										new_qty : batch.quantity,
										new_location : sNewLocation,
										new_location_id : batch.new_location_id,
										old_location_id : batch.old_location_id,
										old_batch_locations : oOldLocation,
										new_batch_locations : oNewLocation,

									}

									uiCloneTemplate.data(oBatchMake);


									uiDisplayNewBatchesOrigin.append(uiCloneTemplate);

									

									var oGatherData = {
										batch_id : batch.batch_id,
										new_batch : sNewLocation,
										new_batch_locations : oNewLocation,
										new_location_id : batch.new_location_id,
										new_measurement : batch.unit_measure_name,
										new_qty : batch.quantity,
										old_batch : sOldLocation,
										old_batch_locations : oOldLocation,
										old_location_id : batch.old_location_id,
										product_id : oData["id"],
										transfer_method : oData["transfer_method"]
									}
									oStoredAllBatches.push(oGatherData);

									// _showEditRemoveBatch(uiCloneTemplate);

									_removeSelectedBatch(uiCloneTemplate);

									_editSelectedBatch(uiCloneTemplate);

									_displayBatchStorages(oStorages);

									


					
							
				




					

				}
	
			}


			uiParent.data("batch_info", oStoredAllBatches);



			

			oStoredAllBatches  =[];


	}

	function _saveSelectedProductOngoing(ui)
	{
		var uiParent = ui,
			oItemData = uiParent.data(),
			uiTransferOngoingBtn = uiParent.find(".transfer-ongoing-btn");

			uiTransferOngoingBtn.off("click.saveIt").on("click.saveIt", function(){
				var uiThis = $(this),
					btnTransferEditInformation = $(".transfer-edit-information"),
					btnCompleteTransferBtn  = $(".complete-transfer-btn"),
					uiSecondParent = uiThis.closest(".record-item"),
					btnTransferHideCancel = uiSecondParent.find(".transfer-hide-cancel"),
					uiTransferDisplayPiece = uiSecondParent.find(".transfer-display-piece"),
					uiTransferHidePiece = uiSecondParent.find(".transfer-hide-piece"),
					btnTransferAddBatch = uiSecondParent.find(".transfer-add-batch"),
					btnEditHide = uiSecondParent.find(".edit-hide");

				// console.log(uiThis.text());

				if(uiThis.text() == "Save Changes")
				{

					var oStoredBatches = [],
						iCount = 0;


					if(oItemData.hasOwnProperty("batch_info"))
					{
						if(Object.keys(oItemData["batch_info"]).length > 0)
						{
							for(var x in oItemData["batch_info"])
							{
								var oBatch = oItemData["batch_info"][x];

								var oGetBatches = {
									product_id : oBatch.product_id,
									batch_id : oBatch.batch_id,
									new_location_id : oBatch.new_location_id,
									new_measurement : oBatch.new_measurement,
									new_measurement_id : oItemData["unit_of_measure"],
									new_qty : oBatch.new_qty,
									old_location_id : oBatch.old_location_id,
									transfer_method : oBatch.transfer_method
								}
								oStoredBatches.push(oGetBatches);
							}


							var oSelectedProd = {
								transfer_id : oItemData["transfer_id"],
								id : oItemData["id"],
								unit_measure_name : oItemData["unit_measure_name"],
								unit_of_measure_id : oItemData["unit_of_measure"],
								transfer_method : oItemData["transfer_method"],
								batch_info : oStoredBatches
							}

							// console.log(JSON.stringify(oSelectedProd));


							_saveSelectedProductData(oSelectedProd);


							uiThis.text("Edit");

							btnTransferHideCancel.hide();

							// for edit Transfer BTN
							btnTransferEditInformation.attr("disabled", false);
							btnCompleteTransferBtn.attr("disabled", false);

							// for Transfer Method
							uiTransferDisplayPiece.show();
							uiTransferHidePiece.hide();

							//  for batch Button
							btnTransferAddBatch.hide();

							btnEditHide.off("mouseenter.enter").on("mouseenter.enter", function(){
								$(this).css({"opacity":"0"});
							});
							btnEditHide.off("mouseleave.leave").on("mouseleave.leave", function(){
								$(this).css({"opacity":"0"});
							});



						}else{
							$("body").feedback({title : "Save New Product Batch", message : "Add Batch First SKU#"+oItemData["sku"]+" - "+oItemData["name"], type : "danger", icon : failIcon});
							console.log("may error");
						}
					}

					

					
					
				}else if(uiThis.text() == "Edit")
				{
					uiThis.text("Save Changes");
				}

			});

			// console.log(JSON.stringify(oItemData));
	}


	function buildViewLocationBatch(oHierarchy)
	{
			// 	console.log(JSON.stringify(oHierarchy));

				var iNewCount = 1,
				sMadeNewLocation = "",
				iGetCountNewLocation = Object.keys(oHierarchy).length;

				for(var x in oHierarchy)
				{
					if(iNewCount == iGetCountNewLocation){
		 					sMadeNewLocation += '<p class="font-14 font-400 display-inline-mid padding-right-10 add-some-style">'+oHierarchy[x]["name"]+'</p>';
		 				}else{
		 					sMadeNewLocation += '<p class="font-14 font-400 display-inline-mid padding-right-10 add-some-style">'+oHierarchy[x]["name"]+'</p>'+
		 									'<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10 add-some-style"></i>';
		 				}
		 				iNewCount++;
				}

				return sMadeNewLocation;

	}



	function _triggerEditProduct(ui)
	{
		uiParent = ui,
		uiTransferOngoingBtnParent =  uiParent.find(".transfer-ongoing-btn"),
		btnTransferHideCancelParent = uiParent.find(".transfer-hide-cancel");
		

		uiTransferOngoingBtnParent.off("click.edit").on("click.edit", function(){

			var uiThis = $(this),
				btnTransferEditInformation = $(".transfer-edit-information"),
				btnCompleteTransferBtn  = $(".complete-transfer-btn"),
				uiSecondParent = uiThis.closest(".record-item"),
				btnTransferHideCancel = uiSecondParent.find(".transfer-hide-cancel"),
				uiTransferDisplayPiece = uiSecondParent.find(".transfer-display-piece"),
				uiTransferHidePiece = uiSecondParent.find(".transfer-hide-piece"),
				btnTransferAddBatch = uiSecondParent.find(".transfer-add-batch"),
				btnEditHide = uiSecondParent.find(".edit-hide");


				if(uiThis.text() == "Edit")
				{
					

					// for edit Transfer BTN
					btnTransferEditInformation.attr("disabled", true);
					btnCompleteTransferBtn.attr("disabled", true);

					// for Cancel Button
					btnTransferHideCancel.show();

					// for Transfer Method
					uiTransferDisplayPiece.hide();
					uiTransferHidePiece.show();

					//  for batch Button
					btnTransferAddBatch.show();

					btnEditHide.off("mouseenter.enter").on("mouseenter.enter", function(){
						$(this).css({"opacity":"1"});
					});
					btnEditHide.off("mouseleave.leave").on("mouseleave.leave", function(){
						$(this).css({"opacity":"0"});
					});
						
				}
				else if(uiThis.text() == "Save Changes")
				{
					// 	console.log(uiThis.text());
					// 	uiThis.text("Edit");
				}
				// 	console.log(uiThis.text());
				// 	uiThis.text("Edit");

				// 	btnTransferHideCancel.hide();

				// 	// for edit Transfer BTN
				// 	btnTransferEditInformation.attr("disabled", false);
				// 	btnCompleteTransferBtn.attr("disabled", false);

				// 	// for Transfer Method
				// 	uiTransferDisplayPiece.show();
				// 	uiTransferHidePiece.hide();

				// 	//  for batch Button
				// 	btnTransferAddBatch.hide();

				// 	btnEditHide.off("mouseenter.enter").on("mouseenter.enter", function(){
				// 		$(this).css({"opacity":"0"});
				// 	});
				// 	btnEditHide.off("mouseleave.leave").on("mouseleave.leave", function(){
				// 		$(this).css({"opacity":"0"});
				// 	});

				// }



			

		});

		btnTransferHideCancelParent.off("click.cancel").on("click.cancel", function(){

			var uiThis = $(this),
				btnTransferEditInformation = $(".transfer-edit-information"),
				btnCompleteTransferBtn  = $(".complete-transfer-btn"),
				uiAllProdConstainer = $(".record-item"),
				uiSecondParent = uiThis.closest(".record-item"),
				uiTransferOngoingBtn =  uiSecondParent.find(".transfer-ongoing-btn"),
				btnTransferHideCancel = uiSecondParent.find(".transfer-hide-cancel"),
				uiTransferDisplayPiece = uiSecondParent.find(".transfer-display-piece"),
				uiTransferHidePiece = uiSecondParent.find(".transfer-hide-piece"),
				btnTransferAddBatch = uiSecondParent.find(".transfer-add-batch"),
				btnEditHide = uiSecondParent.find(".edit-hide"),
				iCountBtn = 0;

				// for Cancel Button
				btnTransferHideCancel.hide();
				uiTransferOngoingBtn.show();
				uiTransferOngoingBtn.text("Edit");

				
				

				$.each(uiAllProdConstainer, function(){
					var uiContainThis = $(this);
						btnOngoingEdit = uiContainThis.find(".transfer-ongoing-btn").text();
						console.log(btnOngoingEdit);

						if(btnOngoingEdit == "Save Changes")
						{
							iCountBtn++;
						}
				});

				if(iCountBtn == 0)
				{
					// for edit Transfer BTN
					btnTransferEditInformation.attr("disabled", false);
					btnCompleteTransferBtn.attr("disabled", false);
				}

				

				// for Transfer Method
				uiTransferDisplayPiece.show();
				uiTransferHidePiece.hide();

				//  for batch Button
				btnTransferAddBatch.hide();

				btnEditHide.off("mouseenter.enter").on("mouseenter.enter", function(){
					$(this).css({"opacity":"0"});
				});
				btnEditHide.off("mouseleave.leave").on("mouseleave.leave", function(){
					$(this).css({"opacity":"0"});
				});
		});

	}

	function _triggerEditProductNew(ui)
	{
		ui.find(".panel-collapse.collapse").addClass("in");


		uiParent = ui,
		uiTransferOngoingBtnParent =  uiParent.find(".transfer-ongoing-btn"),
		btnTransferHideCancelParent = uiParent.find(".transfer-hide-cancel"),
		btnTransferHideCancel = uiParent.find(".transfer-hide-cancel"),
		uiTransferDisplayPiece = uiParent.find(".transfer-display-piece"),
		uiTransferHidePiece = uiParent.find(".transfer-hide-piece"),
		btnTransferAddBatch = uiParent.find(".transfer-add-batch"),
		btnEditHide = uiParent.find(".edit-hide"),
		btnTransferEditInformation = $(".transfer-edit-information"),
		btnCompleteTransferBtn  = $(".complete-transfer-btn");


		uiTransferOngoingBtnParent.text("Save Changes");

		// for edit Transfer BTN
		btnTransferEditInformation.attr("disabled", true);
		btnCompleteTransferBtn.attr("disabled", true);

		// for Cancel Button
		btnTransferHideCancel.show();

		// for Transfer Method
		uiTransferDisplayPiece.hide();
		uiTransferHidePiece.show();

		uiTransferHidePiece.css({"display":"inline-block"});

		//  for batch Button
		btnTransferAddBatch.show();

		btnEditHide.off("mouseenter.enter").on("mouseenter.enter", function(){
			$(this).css({"opacity":"1"});
		});
		btnEditHide.off("mouseleave.leave").on("mouseleave.leave", function(){
			$(this).css({"opacity":"0"});
		});





		

		// uiTransferOngoingBtnParent.off("click.edit").on("click.edit", function(){

		// 	var uiThis = $(this),
		// 		btnTransferEditInformation = $(".transfer-edit-information"),
		// 		btnCompleteTransferBtn  = $(".complete-transfer-btn"),
		// 		uiSecondParent = uiThis.closest(".record-item"),
		// 		btnTransferHideCancel = uiSecondParent.find(".transfer-hide-cancel"),
		// 		uiTransferDisplayPiece = uiSecondParent.find(".transfer-display-piece"),
		// 		uiTransferHidePiece = uiSecondParent.find(".transfer-hide-piece"),
		// 		btnTransferAddBatch = uiSecondParent.find(".transfer-add-batch"),
		// 		btnEditHide = uiSecondParent.find(".edit-hide");


		// 		if(uiThis.text() == "Edit")
		// 		{
		// 			uiThis.text("Save Changes");

		// 			// for edit Transfer BTN
		// 			btnTransferEditInformation.attr("disabled", true);
		// 			btnCompleteTransferBtn.attr("disabled", true);

		// 			// for Cancel Button
		// 			btnTransferHideCancel.show();

		// 			// for Transfer Method
		// 			uiTransferDisplayPiece.hide();
		// 			uiTransferHidePiece.show();

		// 			//  for batch Button
		// 			btnTransferAddBatch.show();

		// 			btnEditHide.off("mouseenter.enter").on("mouseenter.enter", function(){
		// 				$(this).css({"opacity":"1"});
		// 			});
		// 			btnEditHide.off("mouseleave.leave").on("mouseleave.leave", function(){
		// 				$(this).css({"opacity":"0"});
		// 			});
						
		// 		}else if(uiThis.text() == "Save Changes")
		// 		{
		// 			console.log(uiThis.text());
		// 			uiThis.text("Edit");

		// 			btnTransferHideCancel.hide();

		// 			// for edit Transfer BTN
		// 			btnTransferEditInformation.attr("disabled", false);
		// 			btnCompleteTransferBtn.attr("disabled", false);

		// 			// for Transfer Method
		// 			uiTransferDisplayPiece.show();
		// 			uiTransferHidePiece.hide();

		// 			//  for batch Button
		// 			btnTransferAddBatch.hide();

		// 			btnEditHide.off("mouseenter.enter").on("mouseenter.enter", function(){
		// 				$(this).css({"opacity":"0"});
		// 			});
		// 			btnEditHide.off("mouseleave.leave").on("mouseleave.leave", function(){
		// 				$(this).css({"opacity":"0"});
		// 			});

		// 		}



			

		// });

		// btnTransferHideCancelParent.off("click.cancel").on("click.cancel", function(){

		// 	var uiThis = $(this),
		// 		btnTransferEditInformation = $(".transfer-edit-information"),
		// 		btnCompleteTransferBtn  = $(".complete-transfer-btn"),
		// 		uiAllProdConstainer = $(".record-item"),
		// 		uiSecondParent = uiThis.closest(".record-item"),
		// 		uiTransferOngoingBtn =  uiSecondParent.find(".transfer-ongoing-btn"),
		// 		btnTransferHideCancel = uiSecondParent.find(".transfer-hide-cancel"),
		// 		uiTransferDisplayPiece = uiSecondParent.find(".transfer-display-piece"),
		// 		uiTransferHidePiece = uiSecondParent.find(".transfer-hide-piece"),
		// 		btnTransferAddBatch = uiSecondParent.find(".transfer-add-batch"),
		// 		btnEditHide = uiSecondParent.find(".edit-hide"),
		// 		iCountBtn = 0;

		// 		// for Cancel Button
		// 		btnTransferHideCancel.hide();
		// 		uiTransferOngoingBtn.show();
		// 		uiTransferOngoingBtn.text("Edit");

				
				

		// 		$.each(uiAllProdConstainer, function(){
		// 			var uiContainThis = $(this);
		// 				btnOngoingEdit = uiContainThis.find(".transfer-ongoing-btn").text();
		// 				console.log(btnOngoingEdit);

		// 				if(btnOngoingEdit == "Save Changes")
		// 				{
		// 					iCountBtn++;
		// 				}
		// 		});

		// 		if(iCountBtn == 0)
		// 		{
		// 			// for edit Transfer BTN
		// 			btnTransferEditInformation.attr("disabled", false);
		// 			btnCompleteTransferBtn.attr("disabled", false);
		// 		}

				

		// 		// for Transfer Method
		// 		uiTransferDisplayPiece.show();
		// 		uiTransferHidePiece.hide();

		// 		//  for batch Button
		// 		btnTransferAddBatch.hide();

		// 		btnEditHide.off("mouseenter.enter").on("mouseenter.enter", function(){
		// 			$(this).css({"opacity":"0"});
		// 		});
		// 		btnEditHide.off("mouseleave.leave").on("mouseleave.leave", function(){
		// 			$(this).css({"opacity":"0"});
		// 		});
		// });

	}

	function _displayOngoingUnitOfMeasure(ui)
	{

		var oUnitOfMeasures = "",
			oItemData = ui.data(),
			uiUnitOfMeasure = ui.find(".unit-of-measure");
			uiUnitRemoveDropdown = ui.find(".unit-remove-dropdown").find(".frm-custom-dropdown");

			uiUnitRemoveDropdown.remove();

			var iMeasureInterval = setInterval(function(){
				oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"});

				if(oUnitOfMeasures !== null)
				{
					for(var x in oUnitOfMeasures)
					{
						if(oUnitOfMeasures[x]['name'] == "kg" || oUnitOfMeasures[x]['name'] == "mt")
						{
							if(oItemData["unit_measure_name"] == oUnitOfMeasures[x]['name']){
								sHtmlMeasure = '<option value="'+oUnitOfMeasures[x]['id']+'" selected>'+oUnitOfMeasures[x]['name']+'</option>';
								uiUnitOfMeasure.append(sHtmlMeasure);
							}else{
								sHtmlMeasure = '<option value="'+oUnitOfMeasures[x]['id']+'">'+oUnitOfMeasures[x]['name']+'</option>';
								uiUnitOfMeasure.append(sHtmlMeasure);
							}

							
						}

					}
					uiUnitOfMeasure.show().css({
						"padding" : "3px 4px",
						"width" :  "50px"
					});

					// console.log(JSON.stringify(oUnitOfMeasures));
				}
				clearInterval(iMeasureInterval);

			}, 300);

	}

	function _displayEditTransferData(oData)
	{


		var uiDisplayOngoingTransferNumber = $("#display-ongoing-transfer-number"),
			uiDisplayTransferNumber = $("#display-transfer-number"),
			uiSetTransferRequested = $("#set-transfer-requested"),
			uiSetTransferReason = $("#set-transfer-reason");

			uiDisplayOngoingTransferNumber.html("Edit Transfer No. "+oData["to_number"]);
			uiDisplayTransferNumber.html("Transfer No. "+oData["to_number"]);
			uiSetTransferRequested.val(oData["requestor"]);
			uiSetTransferReason.val(oData["reason"]);

			_displayEditAllVessel(oData["vessel_origin_id"]);

			_displayEditAllModeOfTransfers(oData["mode_of_transfer_id"]);

			_displayEditOldProduct(oData["product_info"]);

			_displayEditLeftProductList(oData["product_info"]);




			setTimeout(function(){
				$('#generate-data-view').removeClass('showed');
			},500);
	}

	function _displayEditOldProduct(oData)
	{
		var uiDisplayOldProductContainer = $("#display-old-product"),
			uiCopyThis = $(".copy-this");

			uiDisplayOldProductContainer.html("");

		for(var x in oData)
		{
			var item = oData[x];

			var uiCloneTemplate = uiCopyThis.clone();

				uiCloneTemplate.removeClass("copy-this").show();
				uiCloneTemplate.find(".display-old-sku-name").html("SKU# "+item.sku+" - "+item.name);
				uiCloneTemplate.find(".display-image-old").attr("src", item.image);
				uiCloneTemplate.data(item);

				uiDisplayOldProductContainer.append(uiCloneTemplate);

				_deleteProductItem(uiCloneTemplate);

				// _removeEditSelectedProduct(uiCloneTemplate);

				_saveNewUpdateTransferConsignee();

				

				
		}

		
	}

	function _createNewProdEdit()
	{
		var btnEditSelectedProduct = $("#edit-selected-product"),
			uiProductListDropdown = $(".product-list-dropdown"),
			oGetAllProdEdit = cr8v_platform.localStorage.get_local_storage({name:"getAllProducts"});

			btnEditSelectedProduct.off("click.add").on("click.add", function(){
				// console.log(JSON.stringify(oCollectUseProd));

				for(var x in oGetAllProdEdit)
				{
					if(uiProductListDropdown.val() == oGetAllProdEdit[x]["id"])
					{
						_displayAddNewProdEdit(oGetAllProdEdit[x]);
						oCollectUseProd.push(oGetAllProdEdit[x]);
						_displayEditLeftProductList(oCollectUseProd);

					}
				}
			});


	}

	function _displayAddNewProdEdit(oData)
	{
		var uiDisplaySelectedProductContainer = $("#display-selected-product"),
			uiTemplate = $(".item-display-product");
			
				var item = oData,
					uiCloneTemplate = uiTemplate.clone();

				uiCloneTemplate.removeClass("item-display-product").show();
				uiCloneTemplate.find(".set-product-sku-and-name").html("SKU# "+item.sku+" - "+item.name);
				uiCloneTemplate.find(".set-product-image").css('background-image', 'url('+item.image+')');
				uiCloneTemplate.find(".set-product-description").html(item.description);
				uiCloneTemplate.data(item);

				// if(item.loading_method == "Bulk")
				// {
				// 	uiCloneTemplate.find(".set-product-by-bulk").attr("checked", true);
				// }else{
				// 	uiCloneTemplate.find(".set-product-by-bags").attr("checked", true);
				// }

				uiCloneTemplate.find(".set-product-by-bulk").attr("id", "byBulk"+item.id);
				uiCloneTemplate.find(".set-product-for-bulk").attr("for", "byBulk"+item.id);
				uiCloneTemplate.find(".set-product-by-bags").attr("id", "byBags"+item.id);
				uiCloneTemplate.find(".set-product-for-bags").attr("for", "byBags"+item.id);

				uiCloneTemplate.find(".set-product-by-bags").attr("name", "bag-or-bulk"+item.id);
				uiCloneTemplate.find(".set-product-by-bulk").attr("name", "bag-or-bulk"+item.id);

				sTransferMethod = "Bulk";

				uiDisplaySelectedProductContainer.append(uiCloneTemplate);
				
				_displayUnitOfMeasure();

				_deleteProductItem(uiCloneTemplate);

				_getNewDisplayedProdEdit();

				_checkMethodToTransfer(uiCloneTemplate)

				


	}

	function _getNewDisplayedProdEdit()
	{
		var uiGetNewDisplayProdEdit = $("#display-selected-product").find(".record-item:not(.item-display-product)"),
			oStoredNew = [];
		$.each(uiGetNewDisplayProdEdit, function(){
			var uiThis = $(this),
				oItemData = uiThis.data();

				oStoredNew.push(oItemData);
				// console.log(JSON.stringify(oItemData));
		});
		oCollectNewDisplayProdEdit = oStoredNew;

	}

	function _saveNewUpdateTransferConsignee()
	{
		var btnSaveTransferConsigneeEditSave = $(".save-transfer-consignee-edit-save"),
			uiSetTransferRequested = $("#set-transfer-requested"),
			uiSetTransferReason = $("#set-transfer-reason"),
			uioriginVesselDropdown = $(".origin-vessel-dropdown"),
			uiModeOfTransfer = $(".mode-of-transfer"),
			uiDisplayOldProductContainer = $("#display-old-product").find(".display-all-old-item:not(.copy-this)"),
			oTransferID = cr8v_platform.localStorage.get_local_storage({name:"getNewTransferID"});;
			

			btnSaveTransferConsigneeEditSave.off("click.saveUpdate").on("click.saveUpdate", function(){
				var oStoreOldProd = [],
					oStoreNewProd = [];

				$.each(uiDisplayOldProductContainer, function(){
					var uiThis = $(this),
						oItemData = uiThis.data(),
						oGatherDataOld = {};

						oGatherDataOld = {
							transfer_id : oItemData["transfer_id"],
							product_id : oItemData["id"],
						}


						oStoreOldProd.push(oGatherDataOld);
				});



				for(var x in oCollectNewDisplayProdEdit)
				{
					var oNewProdData = {
						product_id : oCollectNewDisplayProdEdit[x]["id"],
						product_inventory_id : oCollectNewDisplayProdEdit[x]["product_inventory_id"],
						product_qty : oCollectNewDisplayProdEdit[x]["product_qty"],
						loading_method : sTransferMethod,
						unit_of_measure_id : oCollectNewDisplayProdEdit[x]["unit_of_measure_id"],
					}
					oStoreNewProd.push(oNewProdData);

				}


				var NewEditData = {
					transfer_id : oTransferID.value,
					requested_by : uiSetTransferRequested.val().trim(),
					reason : uiSetTransferReason.val().trim(),
					vessel_origin : uioriginVesselDropdown.val(),
					mode_of_transfer : uiModeOfTransfer.val(),
					old_products : oStoreOldProd,
					new_products : oStoreNewProd
				}
				console.log(JSON.stringify(NewEditData));
				_updateTransferConsigneeProduct(NewEditData);

				$("body").feedback({title : "Update Products", message : "Successfully Updated", type : "success"});
			});

			


	}

	function _createCollapseProduct(uiTemplate)
	{
		var uidDisplayConsigneeSkuNameBtn = $(".set-product-sku-name");

		uidDisplayConsigneeSkuNameBtn.off("click.collapeIt").on("click.collapeIt", function(e){
			e.preventDefault();
			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark");
				// iProductId = uiParent.data("product_id");

				// accordion
				$(".panel-group .panel-heading").each(function(){
					var ps = $(this).next(".panel-collapse");
					var ph = ps.find(".panel-body").outerHeight();

					if(ps.hasClass("in")){
						$(this).find("h4").addClass("active")
					}

					$(this).find("a").off("click").on("click",function(e){
						e.preventDefault();
						ps.css({height:ph});

						if(ps.hasClass("in")){
							$(this).find("h4").removeClass("active");
							$(this).find(".fa").removeClass("fa-caret-down").addClass("fa-caret-right");
							ps.removeClass("in");
						}else{
							$(this).find("h4").addClass("active");
							$(this).find(".fa").removeClass("fa-caret-right").addClass("fa-caret-down");
							ps.addClass("in");
						}

						setTimeout(function(){
							ps.removeAttr("style")
						},500);
					});
				});
					// accordion for product-inventory 
					// $(".panel-title").off("click").on("click", function() {		
					// 	$(".panel-title i").removeClass("fa-caret-right").addClass("fa-caret-down");
					// });
					// $(".panel-title .caret-click").off("click").on("click", function() {
					// 	$(this).removeClass(".fa-caret-right").addClass(".fa-caret-down");
					// });


				// console.log(iProductId);
		});

	}

	function _generateOngoingTransferConsigneeWorker()
	{
		var btnAddWorker = $("#add-worker-btn"),
			uiAddWorkerListContainer = $("#add-worker-list"),
			uiAddSomeWorker = $(".add-some-worker"),
			uiRemoveWorker = $(".remove-worker");

			btnAddWorker.off("click.addWorker").on("click.addWorker", function(){

				var uiCloneTemplate = uiAddSomeWorker.clone();

					uiCloneTemplate.find(".worker-name").val("");
					uiCloneTemplate.find(".worker-destination").val("");

				// uiCloneTemplate.
				uiAddWorkerListContainer.append(uiCloneTemplate);

				var newRemoveWorkerBtn = uiCloneTemplate.find(".remove-worker");

				newRemoveWorkerBtn.off("click.removeWorker").on("click.removeWorker", function(){
					var uiThis = $(this),
						uiParent = uiThis.closest(".add-some-worker");
						uiParent.remove();
				});
				_generateOngoingTransferConsigneeComplete()

			});

			uiRemoveWorker.off("click.removeWorker").on("click.removeWorker", function(){
				var uiThis = $(this),
					uiParent = uiThis.closest(".add-some-worker");
					uiParent.remove();
			});

	}

	function _generateOngoingTransferConsigneeEquipmentUsed()
	{
		var btnAddEquipment = $("#add-equipment-btn"),
			uiAddEquipList = $("#add-equip-list"),
			uiAddSomeEquip = $(".add-some-equip"),
			btnRemoveEquipment = $(".remove-equipment");


			btnAddEquipment.off("click.addEquip").on("click.addEquip", function(){

				var uiCloneTemplate = uiAddSomeEquip.clone();

					uiCloneTemplate.find(".equip-name").val("");

					uiAddEquipList.append(uiCloneTemplate);

					var newRemoveEquipBtn = uiCloneTemplate.find(".remove-equipment");

					newRemoveEquipBtn.off("click.removeEquip").on("click.removeEquip", function(){
						var uiThis = $(this),
							uiParent = uiThis.closest(".add-some-equip");
							uiParent.remove();
					});

					_generateOngoingTransferConsigneeComplete()

			});

			btnRemoveEquipment.off("click.removeEquip").on("click.removeEquip", function(){
				var uiThis = $(this),
					uiParent = uiThis.closest(".add-some-equip");
					uiParent.remove();
			});
			

	}

	function _generateOngoingTransferConsigneeComplete()
	{
		var oTransferID = cr8v_platform.localStorage.get_local_storage({name:"getNewTransferID"});
			btnSaveCompleteTransferConsignee = $("#save-complete-transfer-consignee"),
			uiTransferDateStart = $("#transfer-date-start"),
			uiTransferTimeStart = $("#transfer-time-start"),
			uiTransferDateEnd = $("#transfer-date-end"),
			uiTransferTimeEnd = $("#transfer-time-end"),
			uiAddWorkerList = $("#add-worker-list").find(".add-some-worker"),
			uiAddEquipList = $("#add-equip-list").find(".add-some-equip");

			btnSaveCompleteTransferConsignee.off("click.complete").on("click.complete", function(e){
				e.preventDefault();

				var oError = [],
					iCountWorker = 0,
					iCountEquip = 0,
					oWorkerList = [],
					oEquipList = [];
					

					
					

			

				$.each(uiAddWorkerList, function(){
					var uiThis = $(this),
						uiWorkerName = uiThis.find(".worker-name"),
						uiWorkerDestination = uiThis.find(".worker-destination");




						// console.log(uiWorkerName.val());
						
						if(uiWorkerName.val().trim() == "" || uiWorkerDestination.val().trim() == "")
						{
							iCountWorker++;
						}else{
							var oGatherWorker = {
								name : uiWorkerName.val().trim(),
								destination : uiWorkerDestination.val().trim()
							}
							oWorkerList.push(oGatherWorker);
						}

				});

				if(iCountWorker != 0)
				{
					oError.push("Worker Name and Destination must not Empty");
				}

				$.each(uiAddEquipList, function(){
					var uiThis = $(this),
						uiEquipName = uiThis.find(".equip-name");

						// console.log(uiEquipName.val());

						if(uiEquipName.val().trim() == "")
						{
							iCountEquip++;
						}
						else
						{
							var oGatherEquip = {
								name : uiEquipName.val().trim()
							}
							oEquipList.push(oGatherEquip);
						}


				});

				if(iCountEquip != 0)
				{
					oError.push("Equipment Name must not Empty");
				}

				(uiTransferDateStart.val().trim() == "") ? oError.push("Transfer Date Start is required") : "";
				(uiTransferTimeStart.val().trim() == "") ? oError.push("Transfer Time Start is required") : "";
				(uiTransferDateEnd.val().trim() == "") ? oError.push("Transfer Date End is required") : "";
				(uiTransferTimeEnd.val().trim() == "") ? oError.push("Transfer Time End is required") : "";

				if(uiTransferDateStart.val() != "" && uiTransferDateEnd.val() != "" && uiTransferTimeStart.val() != "" && uiTransferTimeEnd.val() != "")
				{
					var sTransferDateStart = uiTransferDateStart.val().split('/').join("-"),
						sTransferDateEnd = uiTransferDateEnd.val().split('/').join("-"),
						sTransferTimeStart = _convertHours(uiTransferTimeStart.val()),
						sTransferTimeEnd = _convertHours(uiTransferTimeEnd.val()),
						sTransferDateTimeStart = sTransferDateStart+" "+sTransferTimeStart,
						sTransferDateTimeEnd = sTransferDateEnd+" "+sTransferTimeEnd,
						sTransferDateTimeStartConverted = _convertDateAndHoursToTimeStamp(sTransferDateTimeStart),
						sTransferDateTimeEndConverted = _convertDateAndHoursToTimeStamp(sTransferDateTimeEnd);

						if(sTransferDateTimeStartConverted > sTransferDateTimeEndConverted)
						{
							oError.push("Transfer Date End must greater than Transfer Date Start");
						}
				}


				


				if(Object.keys(oError).length > 0)
				{
					$("body").feedback({title : "Completing Transfer Consignee", message : oError.join("<br />"), type : "danger", icon : failIcon});

				}
				else
				{
					var oDataTransfer = {
						transfer_id : oTransferID.value,
						transfer_date_start : uiTransferDateStart.val().trim(),
						transfer_time_start : uiTransferTimeStart.val().trim(),
						transfer_date_end : uiTransferDateEnd.val().trim(),
						transfer_time_end : uiTransferTimeEnd.val().trim(),
						worker_info : oWorkerList,
						equipment_info : oEquipList
					}

					console.log(JSON.stringify(oDataTransfer));

					_saveTransferConsigneeComplete(oDataTransfer);

					$("body").feedback({title : "Completing Transfer Consignee", message : "Successfully Completed", type : "success"});


				}

			});


	}

	function _convertDateAndHoursToTimeStamp(sStartDateTime)
	{

		var datum = Date.parse(sStartDateTime);
				return datum/1000;

	}

	function _convertHours(time)
	{
		// Convert 10:15 PM to 22:15

		// var time = uiUnloadingEndTime.val();
		var hours = Number(time.match(/^(\d+)/)[1]);
		var minutes = Number(time.match(/:(\d+)/)[1]);
		var AMPM = time.match(/\s(.*)$/)[1];
		if(AMPM == "PM" && hours<12) hours = hours+12;
		if(AMPM == "AM" && hours==12) hours = hours-12;
		var sHours = hours.toString();
		var sMinutes = minutes.toString();
		if(hours<10) sHours = "0" + sHours;
		if(minutes<10) sMinutes = "0" + sMinutes;
		// console.log(sHours + ":" + sMinutes);
		var sGetHoursAndMins =  sHours + ":" + sMinutes;

		return sGetHoursAndMins;
	}

	function _displayTransferConsigneeComplete(oData)
	{
		// console.log(JSON.stringify(oData));

		var uiDisplayCompleteTransferNum = $("#display-complete-transfer-num"),
			uiDisplayCompleteTransferModeTransfer = $("#display-complete-transfer-mode-transfer"),
			uiDisplayCompleteTransferReason = $("#display-complete-transfer-reason"),
			uiDisplayCompleteTransferDateIssued = $("#display-complete-transfer-date-issued"),
			uiDisplayCompleteTransferRequestorImage = $("#display-complete-transfer-requestor-image"),
			uiDisplayCompleteTransferRequestorName = $("#display-complete-transfer-requestor-name"),
			uiDisplayCompleteTransferDateStart = $("#display-complete-transfer-date-start"),
			uiDisplayCompleteTransferDateDuration = $("#display-complete-transfer-date-duration"),
			uiDisplayCompleteTransferDateEnd = $("#display-complete-transfer-date-end");

			uiDisplayCompleteTransferNum.html("Transfer No. "+oData["to_number"]+" (Complete)");
			uiDisplayCompleteTransferModeTransfer.html(oData["mode_of_transfer"]);
			uiDisplayCompleteTransferReason.html(oData["reason"]);
			uiDisplayCompleteTransferDateIssued.html(oData["date_issued"]);
			uiDisplayCompleteTransferRequestorImage.attr("src", CGetProfileImage.getDefault(oData["requestor"]));
			uiDisplayCompleteTransferRequestorName.html(oData["requestor"]);
			uiDisplayCompleteTransferDateStart.html(oData["date_start"]);
			uiDisplayCompleteTransferDateDuration.html(oData["date_duration"]);
			uiDisplayCompleteTransferDateEnd.html(oData["date_complete"]);

			_displayTransferConsigneeCompleteWorker(oData["workers_info"]);

			_displayTransferConsigneeCompleteEquipment(oData["equipment_info"]);

			_displayDocuments(oData["documents_info"]);

			_displayTransferNotes(oData["notes_info"]);

			_displayTransferCompleteProducts(oData["product_info"]);

			_createCollapseProduct();


		setTimeout(function(){
			$('#generate-data-view').removeClass('showed');
		},500);

	}

	function _displayTransferConsigneeCompleteWorker(oWorkerData)
	{
		var uiDisplayWorkerListCompleteContainer = $("#display-worker-list-complete"),
			uiTemplate = $(".display-worker-contain"),
			iCountColor = 0;

			uiDisplayWorkerListCompleteContainer.html("");

			for(var x in oWorkerData)
			{
				var workers = oWorkerData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-worker-contain").show();
					uiCloneTemplate.find(".display-complete-worker-name").html(workers.workers_name);
					uiCloneTemplate.find(".display-complete-worker-type").html(workers.workers_type);

					if(iCountColor % 2 == 0)
					{
						uiCloneTemplate.addClass("bggray-middark");
					}

					iCountColor++;

					uiDisplayWorkerListCompleteContainer.append(uiCloneTemplate);

			}
	}

	function _displayTransferConsigneeCompleteEquipment(oEquipmentData)
	{
		var uiDisplayEquipListComplete = $("#display-equip-list-complete"),
			uiTemplate = $(".display-equip-contain"),
			iCountColor = 0;

			uiDisplayEquipListComplete.html("");

			for(var x in oEquipmentData)
			{
				var oEquip = oEquipmentData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-worker-contain").show();
					uiCloneTemplate.find(".display-complete-equipment-name").html(oEquip.name);

					if(iCountColor % 2 == 0)
					{
						uiCloneTemplate.addClass("bggray-middark");
					}

					iCountColor++;

					uiDisplayEquipListComplete.append(uiCloneTemplate);

			}
	}

	function _displayTransferCompleteProducts(oData)
	{
		var uiDisplayAllProducts = $("#display-all-products"),
			uiTemplate = $(".display-this-item");

			uiDisplayAllProducts.html("");
			// console.log(JSON.stringify(oData));

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-this-item").show();
					uiCloneTemplate.find(".set-product-sku-name").html("SKU #"+item.sku+" - "+item.product_name);
					uiCloneTemplate.find(".set-product-image").css('background-image', 'url('+item.image+')');
					uiCloneTemplate.find(".set-product-description").html(item.description);
					uiCloneTemplate.find(".set-product-method").html(item.transfer_method);
					uiCloneTemplate.find(".unit-to-transfer").html(item.unit_measure_name.toUpperCase());

					var iGetSumQuantity = 0,
						itotal = 0,
						oStoredQuantity = [];
					
					for(var i in oData[x]["batch_info"])
					{
						oStoredQuantity.push(oData[x]["batch_info"][i]["quantity"]);
						// console.log(oData[x]["batch_info"][i]["quantity"]);
					}

					$.each(oStoredQuantity,function() {
					    itotal += parseFloat(this);

					});

					uiCloneTemplate.find(".total-quantity").html($.number(itotal, 2) );

					uiDisplayAllProducts.append(uiCloneTemplate);

					_displayTransferProdBatchComplete(uiCloneTemplate, oData[x]["batch_info"]);

			}
	}

	function _displayTransferProdBatchComplete(ui, oData)
	{
		var uiParent = ui,
			uiDisplayNewBatchesOrigin = uiParent.find(".display-new-batches-origin"),
			uiSecondTemplate = uiParent.find(".product-location"),
			oStoredAllBatches = [];

			uiDisplayNewBatchesOrigin.html("");

			// console.log(JSON.stringify(oData));

			for(var x in oData)
			{
				var batch = oData[x];

				var oNewLocation = "",
					oOldLocation = "",
					sNewLocation = "",
					sOldLocation = "";

					oNewLocation = buildNewLocationBatch(batch.destination_container);
					oOldLocation = buildNewLocationBatch(batch.origin_container);

					
					sNewLocation = buildViewLocationBatch(oNewLocation);
					sOldLocation = buildViewLocationBatch(oOldLocation);

					var uiCloneTemplate = uiSecondTemplate.clone();
					uiCloneTemplate.removeClass("product-location");

					uiCloneTemplate.find(".display-new-batch-name").html(batch.batch_name);
					uiCloneTemplate.find(".display-new-quantity").html("Quantity "+batch.quantity+" "+batch.unit_of_measure_name.toUpperCase());
					uiCloneTemplate.find(".display-batch-origin").html(sOldLocation);
					uiCloneTemplate.find(".display-batch-destination").html(sNewLocation);

					uiDisplayNewBatchesOrigin.append(uiCloneTemplate);
										
			}


	}

	function _displayAllTransferConsignee(oData)
	{
		var uiDisplayAllRecordItem = $("#display-all-record-item"),
			uiTemplate = $(".display-per-item");

			uiDisplayAllRecordItem.html("");

			// console.log(JSON.stringify(oData));

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-per-item").show();
					uiCloneTemplate.find(".display-transfer-all-number").html(item.transfer_number);
					uiCloneTemplate.find(".display-transfer-all-date").html(item.transfer_date_issued);
					uiCloneTemplate.find(".display-transfer-all-image-profile").attr("src", CGetProfileImage.getDefault(item.username));
					uiCloneTemplate.find(".display-transfer-all-name").html(item.username);
					uiCloneTemplate.find(".display-transfer-all-status").html(item.status);
					uiCloneTemplate.data(item);

					if(item.status == "Complete")
					{
						uiCloneTemplate.find(".display-transfer-all-complete-transfer").hide();


					}else{
						uiCloneTemplate.find(".display-transfer-all-complete-transfer").show();
					}

					uiCloneTemplate.find(".hover-tbl.text-center").css({"height":"100%"});

					uiDisplayAllRecordItem.append(uiCloneTemplate);

					uiDisplayProductForTransfer(uiCloneTemplate, item.product_item);

					_viewSelectedTransfer(uiCloneTemplate);

					_triggerCompleteTransferModal(uiCloneTemplate);




			}


		setTimeout(function(){
			$('#generate-data-view').removeClass('showed');
		},500);			

	}

	function uiDisplayProductForTransfer(ui, oData)
	{
		var uiDisplayItemContainerTrans = ui.find(".display-item-container-trans"),
			uiTemplate = ui.find(".display-item-per-trans");

			uiDisplayItemContainerTrans.html("");

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-item-per-trans").show();
					uiCloneTemplate.html("#"+item.sku+" - "+item.product_name);

					uiDisplayItemContainerTrans.append(uiCloneTemplate);
			}
	}

	function _viewSelectedTransfer(uiCloneTemplate)
	{
		var btnDisplayTransferAllViewRecord = uiCloneTemplate.find(".display-transfer-all-view-record");

			btnDisplayTransferAllViewRecord.off("click.view").on("click.view", function(){
				var uiThis = $(this),
					uiParent = uiThis.closest(".displayed-item"),
					oItemData = uiParent.data();

					console.log(JSON.stringify(oItemData));

					if(oItemData["status"] == "Ongoing")
					{
						

						cr8v_platform.localStorage.set_local_storage({
							name : "getNewTransferID",
							data : {"value":oItemData["transfer_id"]}
						});
						setTimeout(function(){
							window.location.href = BASEURL+"transfers/consignee_ongoing";
						}, 300);
					}else{
						cr8v_platform.localStorage.set_local_storage({
							name : "getNewTransferID",
							data : {"value":oItemData["transfer_id"]}
						});
						setTimeout(function(){
							window.location.href = BASEURL+"transfers/consign_complete";
						}, 300);
					}


			});
	}

	function _triggerCompleteTransferModal(uiCloneTemplate)
	{
		var btnSaveCompleteTransferConsignee = uiCloneTemplate.find(".display-transfer-all-complete-transfer");

			btnSaveCompleteTransferConsignee.off("click.showModal").on("click.showModal", function(e){
					e.preventDefault();
					var uiThis = $(this),
						uiParent = uiThis.closest(".displayed-item"),
						oItemData = uiParent.data();
					

					$("body").css({overflow:'hidden'});
						
					$(".show-complete-transfer-modal").addClass("showed");

					$(".show-complete-transfer-modal .close-me").on("click",function(){
						$("body").css({'overflow-y':'initial'});
						$(".show-complete-transfer-modal").removeClass("showed");
					});

					cr8v_platform.localStorage.set_local_storage({
							name : "getNewTransferID",
							data : {"value":oItemData["transfer_id"]}
						});

				});
	}

	

	function bindEvents(sAction, ui)
	{
		if(sCurrentPath.indexOf("transfers/consign_create") > -1)
		{
			

			if(sAction === 'AddNewNotes')
			{
				_documentEvents(true);

				var uiAddReceivingConsigneeNoteButton = $("#AddReceivingConsigneeNote"),
					sAddReceivingConsigneeNoteSubject = $("#AddReceivingConsigneeNoteSubject"),
					sAddReceivingConsigneeNoteMessage = $("#AddReceivingConsigneeNoteMessage");

				uiAddReceivingConsigneeNoteButton.off("click.addNotes").on("click.addNotes", function(e){
					e.preventDefault();

					$("body").css({overflow:'hidden'});
					
					$("#show-modal-add-notes").addClass("showed");

					$("#show-modal-add-notes .close-me").on("click",function(){
						$("body").css({'overflow-y':'initial'});
						$("#show-modal-add-notes").removeClass("showed");
					});

					sAddReceivingConsigneeNoteSubject.val("");
					sAddReceivingConsigneeNoteMessage.val("");

					_addNewNotes();

				});

				
			}
			else if(sAction === 'AddProductList')
			{
				_displayUnitOfMeasure();

				_createProductItem();

				var btnBackToTransfer = $(".back-to-transfer");

				btnBackToTransfer.off("click.back").on("click.back", function(){
					window.location.href = BASEURL+"transfers/consign_transfer";
				});

			}else if(sAction === 'SaveNewTransferData')
			{
				var btnSaveNewTransferData = $("#save-new-transfer-data"),
					uiSetTransferRequested = $("#set-transfer-requested"),
					uiSetTransferReason = $("#set-transfer-reason"),
					uiOriginVesselDropdown = $(".origin-vessel-dropdown"),
					uiDisplayNewBatchName = $(".display-new-batch-name"),
					uiModeOfTransfer = $(".mode-of-transfer");



					btnSaveNewTransferData.off("click.saveNew").on("click.saveNew", function(e){
						e.preventDefault();
						var oError = [],
							iCounter = 0;
						
						console.log(uiModeOfTransfer.val());
						

						(uiSetTransferRequested.val().trim() == "") ? oError.push("Requested By is required") : "";
						(uiSetTransferReason.val().trim() == "") ? oError.push("Reason is required") : "";
						(uiOriginVesselDropdown.val() == "") ? oError.push("Origin Vessel is required") : "";
						(uiModeOfTransfer.val() == "") ? oError.push("Mode Of Transfer is required") : "";
						

						var uiItemContainer = $("#displaySelectedProduct").find(".record-item"),
								oGatheredDataNew = [];

								$.each(uiItemContainer, function(){
									var uiThis = $(this),
										oItemData = uiThis.data();

										if(Object.keys(oItemData).length > 0)
										{
											oGatheredDataNew.push(oItemData)
										}else{}
								});

						(Object.keys(oGatheredDataNew).length == 0) ? oError.push("Select Item First") : "";		

						if(oError.length !== 0)
						{
							$("body").feedback({title : "Adding Transfer Consignee Goods", message : oError.join("<br />"), type : "danger", icon : failIcon});
						}
						else
						{

							var oGatherTransferData = {
								requested_by : uiSetTransferRequested.val().trim(),
								reason : uiSetTransferReason.val().trim(),
								vessel_origin : uiOriginVesselDropdown.val(),
								mode_of_transfer : uiModeOfTransfer.val(),
								product_info : oGatheredDataNew
							}

							$("body").feedback({title : "Add New Transfer Record", message : "Successfully Added", type : "success"});

							_add(oGatherTransferData);
							
						}




					});


			}

		}
		else if(sCurrentPath.indexOf("transfers/consign_ongoing_edit") > -1)
		{

			if(sAction === "createNewEditProd")
			{
				_createNewProdEdit();
				
			}
		}
		else if(sCurrentPath.indexOf("transfers/consignee_ongoing") > -1)
		{
			if(sAction === "generateOngoingTransfer")
			{
				_generateOngoingTransferConsigneeWorker();

				_generateOngoingTransferConsigneeEquipmentUsed();

				_generateOngoingTransferConsigneeComplete();

			}
		}
		else if(sCurrentPath.indexOf("transfers/consign_transfer") > -1)
		{
			_generateOngoingTransferConsigneeWorker();

			_generateOngoingTransferConsigneeEquipmentUsed();

			_generateOngoingTransferConsigneeComplete();
		}
	}

	function renderElement(sType, oData)
	{
		if(sCurrentPath.indexOf("transfers/consign_create") > -1)
		{
			if(sType === 'displayTransferNumber')
			{
				$("#display-transfer-number").html("Transfer No. "+oData);
				$("#confirm-transfer-number").html(oData);
				$("#display-success-transfer-number").html(oData);
			}
		}
		else if(sCurrentPath.indexOf("transfers/consignee_ongoing") > -1)
		{
			if(sType === 'displayTransferInfo')
			{
				_documentEvents();

				_saveNotesInfo()

				getTransferData(function(oData){
					oSelectedTranseferData = oData;
					_displayTransferInfo(oData);
				});

			}
		}
		else if(sCurrentPath.indexOf("transfers/consign_ongoing_edit") > -1)
		{
			if(sType === "displayEditTransfer")
			{

				var setDuplicate = setInterval(function(){
					var oDuplicate = cr8v_platform.localStorage.get_local_storage({name:"getAllProducts"});
					if(oDuplicate !== null)
					{
						clearInterval(setDuplicate);
					}
				}, 300);


				var btnBackToOngoing = $(".back-to-ongoing");

				btnBackToOngoing.off("click.back").on("click.back", function(){
					window.location.href = BASEURL+"transfers/consignee_ongoing";
				});

			}

		}
		else if(sCurrentPath.indexOf("transfers/consign_complete") > -1)
		{

		}
	}

	return {
		bindEvents : bindEvents,
		renderElement : renderElement,
		getVesselName : getVesselName,
		getModeOfTransfer : getModeOfTransfer,
		getProductlist : getProductlist,
		getUnitOfMeasure : getUnitOfMeasure,
		generateTransferNumber : generateTransferNumber,
		getAllBatchForProduct : getAllBatchForProduct,
		getAllLocation : getAllLocation,
		getAllStorage : getAllStorage,
		getTransferData : getTransferData,
		getEditVesselName : getEditVesselName,
		getTransferDataEdit : getTransferDataEdit,
		getEditProdList : getEditProdList,
		getTransferConsigneeComplete : getTransferConsigneeComplete,
		getAllTransferConsigneeRecords : getAllTransferConsigneeRecords
	}

})();

$(document).ready(function(){

	if(sCurrentPath.indexOf("transfers/consign_create") > -1)
	{
		CTransferConsigneeGoods.getVesselName();
		CTransferConsigneeGoods.getModeOfTransfer();
		CTransferConsigneeGoods.getProductlist();
		CTransferConsigneeGoods.getUnitOfMeasure();
		CTransferConsigneeGoods.generateTransferNumber();
		CTransferConsigneeGoods.getAllBatchForProduct();
		CTransferConsigneeGoods.getAllLocation();
		CTransferConsigneeGoods.getAllStorage();

		var setUnitInterval = setInterval(function(){
			oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"});
			
			if(oUnitOfMeasures !== null)
			{
				CTransferConsigneeGoods.bindEvents("AddProductList");
				clearInterval(setUnitInterval);
			}

		}, 300);


		CTransferConsigneeGoods.bindEvents("AddNewNotes");
		
		CTransferConsigneeGoods.bindEvents("SaveNewTransferData");



		cr8v_platform.localStorage.delete_local_storage({name:"setConsigneeAddNoteStored"})
	}
	else if(sCurrentPath.indexOf("transfers/consignee_ongoing") > -1)
	{

		// CTransferConsigneeGoods.getTransferData();
		CTransferConsigneeGoods.getAllLocation();

		// CTransferConsigneeGoods.getAllLocation();

		var sSetMyLocationOngoingInterval = setInterval(function(){
			oLocations = cr8v_platform.localStorage.get_local_storage({name: "getAllLocationsName"});

			if(oLocations !== null)
			{
				CTransferConsigneeGoods.getUnitOfMeasure();
		
				CTransferConsigneeGoods.getAllStorage();
				CTransferConsigneeGoods.getAllBatchForProduct();
				

				var setUnitInterval = setInterval(function(){
					oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"});
					
					if(oUnitOfMeasures !== null)
					{
						CTransferConsigneeGoods.renderElement("displayTransferInfo");
						clearInterval(setUnitInterval);
					}

				}, 300);

				clearInterval(sSetMyLocationOngoingInterval);
			}

		}, 300);

		

		CTransferConsigneeGoods.bindEvents("generateOngoingTransfer");

		cr8v_platform.localStorage.delete_local_storage({name:"getAllVesselData"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllProducts"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllModeOfTransfer"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllLocationsName"});
		cr8v_platform.localStorage.delete_local_storage({name:"dataUploadedDocument"});
		cr8v_platform.localStorage.delete_local_storage({name:"setConsigneeAddNoteStored"});

	}
	else if(sCurrentPath.indexOf("transfers/consign_ongoing_edit") > -1)
	{

		CTransferConsigneeGoods.getEditProdList();
		

		var oProductInterval = setInterval(function(){
			oDuplicateProducts = cr8v_platform.localStorage.get_local_storage({name:"getAllProducts"});

			if(oDuplicateProducts !== null)
			{
				CTransferConsigneeGoods.getTransferDataEdit();
				CTransferConsigneeGoods.getEditVesselName();
				CTransferConsigneeGoods.getModeOfTransfer();
				CTransferConsigneeGoods.renderElement("displayEditTransfer");
				CTransferConsigneeGoods.bindEvents("createNewEditProd");

				clearInterval(oProductInterval);
			}
		}); 


	}
	else if(sCurrentPath.indexOf("transfers/consign_complete") > -1)
	{
		var transferID = cr8v_platform.localStorage.get_local_storage({name:"getNewTransferID"});
		
		CTransferConsigneeGoods.getAllLocation();

		var sSetMyLocationInterval = setInterval(function(){
			oLocations = cr8v_platform.localStorage.get_local_storage({name: "getAllLocationsName"});

			if(oLocations !== null)
			{
				CTransferConsigneeGoods.getAllStorage();
				CTransferConsigneeGoods.getTransferConsigneeComplete();
				
				CTransferConsigneeGoods.bindEvents();

				clearInterval(sSetMyLocationInterval);
			}

		}, 300);

		
		
	}
	else if(sCurrentPath.indexOf("transfers/consign_transfer") > -1)
	{
		CTransferConsigneeGoods.getAllTransferConsigneeRecords();
		CTransferConsigneeGoods.bindEvents();

		cr8v_platform.localStorage.delete_local_storage({name:"getAllProducts"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllModeOfTransfer"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllLocationsName"});
		cr8v_platform.localStorage.delete_local_storage({name:"dataUploadedDocument"});
		cr8v_platform.localStorage.delete_local_storage({name:"setConsigneeAddNoteStored"});
		cr8v_platform.localStorage.delete_local_storage({name:"unit_of_measures"});
		cr8v_platform.localStorage.delete_local_storage({name:"setCurrentDateTime"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllLocationsName"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllVesselName"});
	}

});



