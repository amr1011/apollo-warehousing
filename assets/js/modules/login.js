$(document).ready(function(){

	var uiForm = $('#formLogin'),
		uiUsername = uiForm.find('.username'),
		uiPassword = uiForm.find('.password'),
		uiBtn = uiForm.find('.btn.general-btn'),
		uiErrorMessage = uiForm.find(".error-message"),
		oTimeout = {};

	//HELPER FUNCTION CAPITALIZE FIRST LETTER
	function capitalizeFirstLetter(string) {
	    return string.charAt(0).toUpperCase() + string.slice(1);
	}

	uiForm.cr8vformvalidation({
		'preventDefault' : true,
	    'data_validation' : 'datavalid',
	    'input_label' : 'labelinput',
	    'onValidationError': function(oMessages) {
	    	var arrMessage = [];
	    	for(var i in oMessages){
	    		arrMessage.push(oMessages[i]["error_message"]);
	    	}

	    	displayErrorMessage(arrMessage.join(","));

	    	uiForm.find('input').prop('disabled', false);
			uiBtn.prop('disabled', false);
        },
	    'onValidationSuccess': function() {
	     	authenticateCredential(uiUsername.val(), uiPassword.val());
	    }
	});

	function submitForm(){
		uiForm.find('input').prop('disabled', true);
		uiBtn.prop('disabled', true);
		uiForm.submit();
	}

	uiBtn.click(function(){
		submitForm();
	});

	uiPassword.on('keyup', function(e){
		if(e.keyCode === 13){
			submitForm();
		}
	});

	function displayErrorMessage(message){
		clearTimeout(oTimeout);
		uiErrorMessage.html(capitalizeFirstLetter(message));
		oTimeout = setTimeout(function(){
			uiErrorMessage.html("");
		},3000);
	}

	function authenticateCredential(username, password){
		var oAjaxConfig = {
			type: "POST",
			url : BASEURL+"login/authenticate",
			dataType: "json",
			data: {
				'username' : username,
				'password' : password
			},
			success: function(oResponse)
			{
				if(oResponse.status === false){
					displayErrorMessage(oResponse.message);
					uiForm.find('input').prop('disabled', false);
					uiBtn.prop('disabled', false);
				}else{
					uiErrorMessage.addClass('green-color');
					displayErrorMessage(oResponse.message);
					window.location.href = BASEURL+"dashboard";
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);
	}

	

});