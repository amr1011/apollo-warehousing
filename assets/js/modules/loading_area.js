var CLoadingAreas = (function() {
    var oAllLoadArea = {};

    /* PUBLIC METHODS */
    function get() {
        var oOptions = {
            type: "GET",
            data: {
                "param": "none"
            },
            url: BASEURL + "settings/getAllArea",
            success: function(oResult) {
                if (oResult.status === true) {
                    oAllLoadArea = oResult.data;

                    _displayLoadingArea(oAllLoadArea);

                    cr8v_platform.localStorage.set_local_storage({
                        name: "getAllLoadingAreaData",
                        data: oAllLoadArea
                    });
                }
            },
            error: function() {
                get();
            }
        };
        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    function bindEvents(sAction, ui) {

        if (sAction === 'editLoadingArea') {
            var uiBtn = $(".trigger-button");


            uiBtn.off('click.editLoadingArea').on('click.editLoadingArea', function() {
                var uiThis = $(this).attr("data-id");

                $("body").css({
                    overflow: 'hidden'
                });
                var tm = $(this).attr("modal-target");

                $("div[modal-id~='" + tm + "']").addClass("showed");

                $("div[modal-id~='" + tm + "'] .close-me").on("click", function() {
                    $("body").css({
                        'overflow-y': 'initial'
                    });
                    $("div[modal-id~='" + tm + "']").removeClass("showed");
                });

                var oGetAllLoadingArea = cr8v_platform.localStorage.get_local_storage({
                    name: "getAllLoadingAreaData"
                });

                for (var x in oGetAllLoadingArea) {
                    if (oGetAllLoadingArea[x]["id"] == uiThis) {
                        var oCollectData = {};

                        oCollectData = {
                            id: uiThis,
                            name: oGetAllLoadingArea[x]["name"]
                        }

                        CVesselList.renderElement("updatevessel", oCollectData);

                        cr8v_platform.localStorage.set_local_storage({
                            "name": "currentLoadingArea",
                            "data": oCollectData
                        });

                        $("#editAreaName").val(oCollectData.name);
                    }
                }
            });
        }


        $("#AddLoadingAreaForm").cr8vformvalidation({
            'preventDefault': true,
            'data_validation': 'datavalid',
            'input_label': 'labelinput',
            'onValidationError': function(arrMessages) {

                var options = {
                    form: "box",
                    autoShow: true,
                    type: "danger",
                    title: "Add Area Name",
                    message: "Please fill up required field",
                    speed: "slow",
                    withShadow: true
                }

                $("body").feedback(options);
            },

            'onValidationSuccess': function() {

                add();

                var options = {
                    form: "box",
                    autoShow: true,
                    type: "success",
                    title: "Add Area Name",
                    message: "Successfully Added",
                    speed: "slow",
                    withShadow: true
                }

                $("body").feedback(options);

            }
        }); //end

        var uiAddVesselSubmit = $("#addLoadingAreaBtn");
        uiAddVesselSubmit.off("click.AddVessel").on("click.AddVessel", function() {;
            $("#AddLoadingAreaForm").submit();
        }) //end




        $("#EditLoadingAreaForm").cr8vformvalidation({

            'preventDefault': true,
            'data_validation': 'datavalid',
            'input_label': 'labelinput',
            'onValidationError': function(arrMessages) {
                // alert(JSON.stringify(arrMessages)); //shows the errors

                var options = {
                    form: "box",
                    autoShow: true,
                    type: "danger",
                    title: "Updated Area Name",
                    message: "Please fill up required field",
                    speed: "slow",
                    withShadow: true
                }

                $("body").feedback(options);
            },
            'onValidationSuccess': function() {

                var iAreaname = cr8v_platform.localStorage.get_local_storage({
                        name: "currentLoadingArea"
                    }),
                    iAreaId = parseInt(iAreaname.id);

                update(iAreaId);

                var options = {
                    form: "box",
                    autoShow: true,
                    type: "success",
                    title: "Edit Area Name",
                    message: "Successfully Updated",
                    speed: "slow",
                    withShadow: true
                }

                $("body").feedback(options);

            }
        });

        var uiUpdateVessel = $("#update-area-name");
        uiUpdateVessel.off("click.updateVesselBtn").on("click.updateVesselBtn", function() {
            $("#EditLoadingAreaForm").submit();

        });


        var uiTrashBtn = $('#deleteAreaName');
        uiTrashBtn.off("click.delete").on("click.delete", function() {
            $("div.delete-modal").addClass("showed");

            $("div.delete-modal .close-me").off("click").on("click", function() {
                $("body").css({
                    'overflow-y': 'initial'
                });
                $("div.delete-modal").removeClass("showed");
            });
        });

        $('#confirmDelete').off("click").on("click", function() {
            var iAreaname = cr8v_platform.localStorage.get_local_storage({
                    name: "currentLoadingArea"
                }),
                iAreaId = parseInt(iAreaname.id);
            _delete(iAreaId);


        });

    }

    function add() {
        var name = $('#areaName').val();

        var oAjaxConfig = {
            type: "POST",
            url: BASEURL + "settings/addArea",
            dataType: "json",
            data: {
                "name": name
            },
            success: function(response) {
                $('div[modal-id="add-loading-area"] .close-me').trigger("click");
                $('#areaName').val("");
                get();

            }
        }

        cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);

        cr8v_platform.localStorage.delete_local_storage({
            name: "setCurrentImage"
        });
    }

    function update(iAreaId) {
        var iAreaname = cr8v_platform.localStorage.get_local_storage({
                name: "currentLoadingArea"
            }),
            iAreaId = parseInt(iAreaname.id);

        var editName = $('#editAreaName').val();

        var oAjaxConfig = {
            type: "POST",
            url: BASEURL + "settings/updateArea",
            dataType: "json",
            data: {
                "name": editName,
                "id": iAreaId
            },
            success: function(response) {
                var options = {
                    form: "box",
                    autoShow: true,
                    type: "success",
                    title: "Updated Vessel",
                    message: "Successfully Updated",
                    speed: "slow",
                    withShadow: true
                }

                $("body").feedback(options);
                $('div[modal-id="edit-loading-area"] .close-me').trigger("click");
                $('#editAreaName').val("");
                get();
            }
        }
        cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);
        cr8v_platform.localStorage.delete_local_storage({
            name: "currentEditVessel"
        });

    }


    function renderElement(sAction, oData) {


    }

    /* END PUBLIC METHODS */

    /* PRIVATE METHODS */


    function _delete(iAreaId) {
        var iAreaname = cr8v_platform.localStorage.get_local_storage({
                name: "currentLoadingArea"
            }),
            iAreaId = parseInt(iAreaname.id);

        var oDataAjax = {
            type: "POST",
            url: BASEURL + "settings/deleteArea",
            data: {
                "id": iAreaId
            },
            success: function(response) {
                var options = {
                    form: "box",
                    autoShow: true,
                    type: "success",
                    title: "Delete Area Name",
                    message: "Successfully Deleted",
                    speed: "slow",
                    withShadow: true
                }
                $("body").feedback(options);

                $('div[modal-id="edit-vessel"] .close-me').trigger("click");
                $(".close-me").trigger("click");
                get();


            }
        }
        cr8v_platform.CconnectionDetector.ajax(oDataAjax);
    }



    function _getLoadingAreaTemplate() {
        var sHtml = '<div class="display-loading-area table-content position-rel" >';
        sHtml += '<div class="content-show height-auto width-100per padding-top-20 padding-bottom-20 background">';
        sHtml += '<div class="text-center width-100per display-inline-mid">';
        sHtml += '<p class="name"></p>';
        sHtml += '</div>';
        sHtml += '</div>';
        sHtml += '<div class="content-hide" style="height:100%;">';
        sHtml += '<button class="btn general-btn trigger-button modal-trigger" modal-target="edit-loading-area" style="width: 100px;">Edit</button>';
        sHtml += '</div>';
        sHtml += '</div>';

        $(".semi-main").find(".table-content:nth-child(even)").css({"background-color":"#fff !important"});
        return $(sHtml);
    }


    function _displayLoadingArea(oAllLoadArea) {
        var uiLoadingAreaContainer = $("#displayAreaName"),
            uiDataContainer = $(".trigger-button"),
            sStripe = 'white';

        uiLoadingAreaContainer.html("");

        for (var i in oAllLoadArea) {
            var singleAreaName = oAllLoadArea[i],
                name = singleAreaName.name;

            uiTemplate = _getLoadingAreaTemplate();

            var uiSetId = uiTemplate.find(".trigger-button");
            uiSetId.attr("data-id", singleAreaName.id);

            uiTemplate.find(".name").append(name);

            if (sStripe == 'white') {
                uiTemplate.find('.background').addClass('tbl-dark-color');
                sStripe = 'dark';
            } else {
                sStripe = 'white';
            }

            uiLoadingAreaContainer.append(uiTemplate);

            bindEvents("editLoadingArea", uiTemplate);
        }
    }
    /* END PRIVATE METHODS */

    return {
        get: get,
        add: add,
        update: update,
        bindEvents: bindEvents,
        renderElement: renderElement
    }

})();

$(document).ready(function() {

    if (sCurrentPath.indexOf("system_settings") > -1) {

        CLoadingAreas.get();
        CLoadingAreas.bindEvents();

    }

});