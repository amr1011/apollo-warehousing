var CVesselList = (function() {
	var oAllVessels = {},
		failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg";

	/* PUBLIC METHODS */
	function get(callBack){
		var oOptions = {
			type : "GET",
			data : {"x" : "none"},
			url : BASEURL + "vessel_list/get_all",
			success : function(oResult) {
				if(oResult.status === true){
					oAllVessels = oResult.data;
					cr8v_platform.localStorage.set_local_storage({
						name : "getAllVesselData",
						data: oAllVessels
					});
					if(typeof callBack == 'function'){
						callBack(oResult.data);
					}


				}
			}
		};
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}
	function bindEvents(sAction, ui) {

		if(ui == undefined){
			ui = $("body");
		}

		if(sAction == 'editVessel'){
			$("#EditVesselForm").cr8vformvalidation({
				'preventDefault' : true,
			    'data_validation' : 'datavalid',
			    'input_label' : 'labelinput',
			    'onValidationError': function(arrMessages) {
			        // alert(JSON.stringify(arrMessages)); //shows the errors
			        var options = {
						  form : "box",
						  autoShow : true,
						  type : "danger",
						  title  : "Add Vessel",
						  message : "Please fill up required field",
						  speed : "slow",
						  withShadow : true,
						  icon: failIcon
						}
						
						$("body").feedback(options);
			        },
			    'onValidationSuccess': function() {
			    	var oVesData = cr8v_platform.localStorage.get_local_storage({"name":"currentEditVessel"}),
					iVesselImage = oVesData.image;
					iVesselId = parseInt(oVesData.id);

					console.log(iVesselId);

			    	var oGetSetImage = cr8v_platform.localStorage.get_local_storage({name:"setCurrentImage"})

			        if(oGetSetImage === null)
					{
						cr8v_platform.localStorage.set_local_storage({
							name : "setCurrentImage",
							data : {value:iVesselImage}
						});
					}
					else
					{
						
					}
					_update(iVesselId);
			        
			        var options = {
					  form : "box",
					  autoShow : true,
					  type : "success",
					  title  : "Edit Category",
					  message : "Successfully Updated",
					  speed : "slow",
					  withShadow : true
					}
						
					$("body").feedback(options);		
			    }
			});

			var uiBtn = ui.find('[modal-target="edit-vessel"]');

			uiBtn.off('click.editVessel').on('click.editVessel', function(){
				var uiParent = uiBtn.closest(".vessel-container.position-rel"),
				iGetVesselId = uiParent.data("vesselId");
				
				
				$("body").css({overflow:'hidden'});
				var tm = $(this).attr("modal-target");

				$("div[modal-id~='"+tm+"']").addClass("showed");

				$("div[modal-id~='"+tm+"'] .close-me").on("click",function(){
					$("body").css({'overflow-y':'initial'});
					$("div[modal-id~='"+tm+"']").removeClass("showed");
				});

				var oGetAllVessel = cr8v_platform.localStorage.get_local_storage({name : "getAllVesselData"});

				
				for(var x in oGetAllVessel)
				{
					if(oGetAllVessel[x]["id"] == iGetVesselId)
					{
						var oCollectData = {};

							oCollectData = {
								id:iGetVesselId,
								name:oGetAllVessel[x]["name"],
								image : oGetAllVessel[x]["image"],
								alias :oGetAllVessel[x]["alias"],
								group :oGetAllVessel[x]["group"]
							}

							CVesselList.renderElement("updatevessel", oCollectData);

							cr8v_platform.localStorage.set_local_storage({
							"name": "currentEditVessel",
							"data" : oCollectData
							});

							//console.log(oCollectData);


					}
				}

			});
		}

		var uploadInstance = $("#uploadHolder").cr8vUpload({
			url : BASEURL + "vessel_list/add",
			accept : "jpg,png",
			autoUpload : false,
			onSelected : function(bool_valid){

			},
			success : function(oResult, textStatus, jqXHR){
				if(oResult.status){
					$("body").feedback({
						form : "box",
						autoShow : true,
						type : "success",
						title  : "Add Vessel",
						message : "Successfully Added",
						speed : "slow",
						withShadow : true
					});

					$('div[modal-id="add-new-vessel"] .close-me').trigger("click");
					$('#vesselName').val("");
					$('#vesselalias').val("");
					$('#vesselgroup').val("");
					get(_displayVessels);
				}else{
					var arrMessages = [];
					for(var i in oResult.data){
						arrMessages.push(oResult.data[i]);
					}
					$("body").feedback({
					  form : "box",
					  autoShow : true,
					  type : "danger",
					  title  : "Add Vessel",
					  message : arrMessages,
					  speed : "slow",
					  withShadow : true,
					  icon : failIcon
					});
				}
				
			},
			error : function(jqXHR, textStatus, errorThrown){
				
			}
		});

		$("#AddVesselForm").cr8vformvalidation({
			'preventDefault' : true,
		    'data_validation' : 'datavalid',
		    'input_label' : 'labelinput',
		    'onValidationError': function(arrMessages) {
		        // alert(JSON.stringify(arrMessages)); //shows the errors

		        var options = {
					  form : "box",
					  autoShow : true,
					  type : "danger",
					  title  : "Add Vessel",
					  message : "Please fill up required field",
					  speed : "slow",
					  withShadow : true,
					  icon : failIcon
					}
					
					$("body").feedback(options);
		        },
		    'onValidationSuccess': function() {
		    	var vesselName = $('#vesselName').val(),
					vesselAlias = $('#vesselalias').val();
					vesselGroup = $('#vesselgroup').val();


				uploadInstance.updateOptions({
					extraField : {
						"name": vesselName, "alias": vesselAlias, "group":vesselGroup
					}
				});

		    	uploadInstance.startUpload();
		    }
		});//end

		var uiAddVesselSubmit = $("#add-vessel");
		uiAddVesselSubmit.off("click.AddVessel").on("click.AddVessel", function(){
			$("#AddVesselForm").submit();
		})//end


		var uiUpdateVessel = $( "#update-vessel");				
		uiUpdateVessel.off("click.updateVesselBtn").on("click.updateVesselBtn", function(){
			$("#EditVesselForm").submit();
		});


		var uiDeleteVessel = $( "#delete-vessel");				
		uiDeleteVessel.off("click.deleteVesselBtn").on("click.deleteVesselBtn", function(){
			var oVesData = cr8v_platform.localStorage.get_local_storage({"name":"currentEditVessel"}),
				iVesselId = parseInt(oVesData.id);
					_delete(iVesselId);
					
		});
	}

	function renderElement(sAction, oData){
		//console.log(oData);
		var edituploadInstance = $('#edituploadHolder').cr8vUpload({
			url : BASEURL + "vessel_list/upload",
			backgroundPhotoUrl: oData.image,
			accept : "jpg,png",
			autoUpload : true,
			onSelected : function(bool_valid){

			},
			success : function(data, textStatus, jqXHR){
				var sImageUpload = data.data.location;
				cr8v_platform.localStorage.set_local_storage({
					name : "setCurrentImage",
					data : {value:sImageUpload}
				});
			},
			error : function(jqXHR, textStatus, errorThrown){
				
			}
		});
		$("#editVesselName").val(oData["name"]);
		$("#editVesselAlias").val(oData["alias"]);
		$("#vesselgroup").val(oData["group"]);
	}

	/* END PUBLIC METHODS */

	/* PRIVATE METHODS */
	

	function _delete(iVesselId){
		var oDataAjax = {
			type : "POST",
			url : BASEURL+"vessel_list/delete",
			data : {"id":iVesselId},
			success: function(response)
			{
				var options = {
					  form : "box",
					  autoShow : true,
					  type : "success",
					  title  : "Delete Vessel",
					  message : "Successfully Deleted",
					  speed : "slow",
					  withShadow : true
					}
					$("body").feedback(options);
					
						$('div[modal-id="edit-vessel"] .close-me').trigger("click");
					get();
					
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oDataAjax);
	}

	function _update(iVessel)
	{
		var vesselName = $('#editVesselName').val(),
			vesselAlias = $('#editVesselAlias').val(),
			group = $('#editVesselGroup').val(),
			
			oGetCurrentImage = cr8v_platform.localStorage.get_local_storage({name:"setCurrentImage"});



		var oAjaxConfig = {
			type: "POST",
			url : BASEURL+"vessel_list/update",
			dataType: "json",
			data:{ "name": vesselName, "alias": vesselAlias,"id":iVessel,"image":oGetCurrentImage.value,"group":group},
			success: function(response)
			{
				console.log(response);
				get(_displayVessels);
				
				var options = {
					  form : "box",
					  autoShow : true,
					  type : "success",
					  title  : "Updated Vessel",
					  message : "Successfully Updated",
					  speed : "slow",
					  withShadow : true
					}

				$("body").feedback(options);
				$('div[modal-id="edit-vessel"] .close-me').trigger("click");
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);
		cr8v_platform.localStorage.delete_local_storage({name:"currentEditVessel"});
	}


	function _getTemplate() {
		var sHtml = '<div class="vessel-container position-rel display-inline-mid width-50per bggray-white box-shadow-light margin-all-5 text-left default-cursor ">';
				sHtml += '<div class="vessel-show padding-all-10">';					
					sHtml += '<div class="vessel-image  display-inline-mid margin-right-20 width-20percent ">';
						sHtml += '<img src="" class="vessel-image height-60px width-80px margin-left-20" />';
					sHtml += '</div>';
					sHtml += '<div class="vessel-details width-48percent display-inline-mid width-50percent ">';
						sHtml += '<p class="font-16 font-400 vessel-name"> fa fadfas das dasd sa</p>';
						sHtml += '<p class="vessel-alias"></p>';
						sHtml += '<p class="vessel-group"></p>';
					sHtml += '</div>';
				sHtml += '</div>';
						sHtml += '<div class="vessel-hide">';
							sHtml +='<button id="editvessel" class="btn general-btn modal-trigger padding-left-30 padding-right-30 font-500 padding-left-30 padding-right-30 font-500" modal-target="edit-vessel">Edit</button>';
							sHtml += '</div>';
			sHtml += '</div>';
		return $(sHtml);	
	}

	function _displayVessels(oAllVessels){
		var uiVesselContainer = $("#vesselContainer");

		uiVesselContainer.html("");

		for(var i in oAllVessels){
			var singleVessel = oAllVessels[i],
				name = singleVessel.name,
				image = singleVessel.image,
				alias = singleVessel.alias,
				group = singleVessel.group,
				uiTemplate = _getTemplate();
				
				uiTemplate.data({
					"vesselId":singleVessel.id
				});

			uiTemplate.find(".vessel-image").attr("src", image);
			uiTemplate.find(".vessel-name").html(name);
			uiTemplate.find(".vessel-alias").html(alias);
			uiTemplate.find(".vessel-group").html(group);

			uiVesselContainer.append(uiTemplate);

			bindEvents("editVessel", uiTemplate);
		}
	}
	/* END PRIVATE METHODS */

	return {
		get : get,
		bindEvents : bindEvents,
		renderElement : renderElement,
		displayVessels : _displayVessels
	}

})();

$(document).ready(function() {
		
	if(window.location.href.indexOf("vessel_list") > -1){
		CVesselList.bindEvents();
		CVesselList.get(CVesselList.displayVessels);
	}else{
		CVesselList.get();
	}


});