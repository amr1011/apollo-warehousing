var CProducts = (function() {
    var oProducts = {},
        failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg",
        successIcon = BASEURL + "assets/js/plugins/feedback/images/success.svg",
        oProductsByCategory = {},
        oUnitOfMeasure = '';
    // Object.keys(oProducts).length; //to get the length of an json object


    function bindEvents(sAction) { //BINDS ALL EVENT FOR THE PRODUCT MANAGEMENT MODULE.
        if (sAction == "draggable_composite_product") {
            var container = $("#composite_product_list_UI"),
                uiItems = container.find('.single_composite_product'),
                dragList = $(".drag-list");

            uiItems.draggable({
                helper: "clone",
                iframeFix: true,
                zIndex: 999,
                start: function(event, ui) {
                    $(ui.helper).css({
                        "box-shadow": "0px 0px 5px #999"
                    });
                }
            });
            dragList.droppable({
                accept: ".single_composite_product",
                tolerance: "pointer",
                drop: function(event, ui) {
                    var uiItem = ui.helper.clone();
                    uiItem.removeAttr("style");
                    $(this).append(uiItem);
                    $(this).find('.drag-item').hide();

                    var iSelectedItem = uiItem.closest(".items").attr("data-id");

                   $.each(uiItems, function(){
                        var uiGetThis = $(this),
                            iData = uiGetThis.attr("data-id");

                           if(iSelectedItem == iData){
                                uiGetThis.remove();
                           }
                   });

                    uiItem.draggable({
                        helper: "clone",
                        iframeFix: true,
                        zIndex: 999
                    });

  //                  uiItem.draggable("disable");

                    uiItem.css("position", "relative");

                    var inputHtml = '<input type="text" placeholder="number" style="position:absolute; top: 25px; right: 55px; width: 100px;" value="1" />',
                        uiInput = $(inputHtml),
                        sTimeout = {};

                    uiInput.on("keyup", function(e) {
                        var uiThis = $(this);
                        var regex = new RegExp('^[0-9]+$', 'g'),
                            bTest = regex.test($(this).val());
                        if (!bTest) {
                            var val = uiThis.val(),
                                newVal = val.slice(0, -1);
                            uiThis.val(newVal);
                            clearTimeout(sTimeout);
                            return false;
                        }

                    });

                    uiItem.append(uiInput);

                    var closeHtml = '<div class="width-20px" style="position:absolute; top: 25px; right: 40px;">' +
                        '<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor delete-compo"></div>';
                    var uiClose = $(closeHtml);
                    uiClose.click(function() {

                        $.each(uiItems, function(){
                        var uiGetThis = $(this),
                            iData = uiGetThis.attr("data-id");

                           if(iSelectedItem == iData){
                                 uiGetThis.addClass('ui-draggable ui-draggable-handle');
                                container.prepend(uiGetThis);
                                uiItem.remove();

                                 uiGetThis.draggable({
                                        helper: "clone",
                                        iframeFix: true,
                                        zIndex: 999
                                    });
                           }
                        });
                    });

                    uiItem.append(uiClose);


                }
            });
        } else {
            var sCurrentPath = window.location.href;

            cr8v_platform.localStorage.set_local_storage({
                name: "add_product_image",
                data: {
                    value: "false"
                }
            });

            if (sCurrentPath.indexOf("add_product") > -1) {
                _build_compositeProductList($("#composite_product_list_UI")); // BUILD THE COMPOSITE PRODUCT LIST
                get_sku();

                $('#minThreshold').number(true, 0);
                $('#maxThreshold').number(true, 0);
                $('#itemPrice').number(true, 2);
                $('#itemGuarantee').number(true, 0);
                $('#itemShelfLife').number(true, 0);



				
				var BuildNewMeasurement = setInterval(function(){
					var sGetUnitOfMeasure = oUnitOfMeasure;

					if(sGetUnitOfMeasure !== null)
					{
						buildUnitOfMeasure(sGetUnitOfMeasure);
						clearInterval(BuildNewMeasurement);
					}
				}, 300);
				

                var iTimeoutGuarantee = {};
                $('#itemGuarantee').on('keyup.guarantee', function(){
                	var uiThis = $(this),
                		val = parseFloat(uiThis.val())

                	clearTimeout(iTimeoutGuarantee);
                	iTimeoutGuarantee = setTimeout(function() {
                		if(val > 100){
                			uiThis.val(100);
                		}
                	}, 300);
                	
                });

                var uploadInstance = $('#upload_photo').cr8vUpload({
                    url: BASEURL + "products/upload",
                    // backgroundPhotoUrl : "",
                    // titleText : "Change Photo",
                    accept: "jpg,png",
                    autoUpload: false,
                    onSelected: function(bValid) {
                        if (bValid) {
                            cr8v_platform.localStorage.set_local_storage({
                                name: "add_product_image",
                                data: {
                                    value: "true"
                                }
                            });
                        } else {
                            cr8v_platform.localStorage.set_local_storage({
                                name: "add_product_image",
                                data: {
                                    value: "false"
                                }
                            });
                        }
                    },
                    success: function(data, textStatus, jqXHR) {
                        _add(data.data.location);
                        // alert(data.data.path);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                    }
                });


                $(".single_composite_product").click(function() {
                    // alert($(this).data("id"));
                });

                $("#submit_add_product").click(function() {
                    $("#addProductForm").submit();
                });

                //VALIDATIONS
                $("#addProductForm").cr8vformvalidation({
                    'preventDefault': true,
                    'data_validation': 'datavalid',
                    'input_label': 'labelinput',
                    'onValidationError': function(arrMessages) {
                        // alert(JSON.stringify(arrMessages)); //shows the errors
                        $("body").feedback({
                            title: "Error!",
                            message: "Input is not valid.",
                            type: "danger",
                            icon: failIcon
                        });
                    },
                    'onValidationSuccess': function() {
                        // alert("Success"); //function if you have something to do upon submission
                        // $("body").feedback({message : "is valid."});

                        var oComposition = JSON.parse(_getSelectedCompositeProducts()),
                            error = 0;
                        for (var i in oComposition) {
                            if (oComposition[i]['quantity'].trim().length == 0) {
                                error++;
                            }
                        }

                        if (error == 0) {
                            var oHasImage = cr8v_platform.localStorage.get_local_storage({
                                name: "add_product_image"
                            });
                            if (oHasImage.value == "true") {
                                uploadInstance.startUpload();
                            } else {
                                _add();
                            }
                        } else {
                            $("body").feedback({
                                title: "Error!",
                                message: "Bill of material error",
                                type: "danger",
                                icon: failIcon
                            });
                        }
                    }
                });

                $("#cancel_close_composite_product").off("click").on("click", function() {
                    $(".composite-product-modal").removeAttr("style");
                    $(".composite-product-modal").removeClass("showed");
                });

                $("#confirm_close_composite_product").off("click").on("click", function() {
                    $(".composite-product-modal").removeAttr("style");
                    $(".composite-product-modal").removeClass("showed");
                });
            }

            if (sCurrentPath.indexOf("add_product_empty") > -1) {
                $("#submit_edit_product").click(function() {
                    // _update();

                    // console.log(test);
                    // var ui_dragList = $(".drag-list");
                    // var oComposite_products = {};
                    // var counter = 0;

                    // ui_dragList.find(".single_composite_product").each(function() {
                    // 	oComposite_products[ counter ] = $(this).data("id");
                    // 	++counter;
                    // });
                    // console.log(oComposite_products);
                    console.log(_getSelectedCompositeProducts());
                });

                $(".single_composite_product").click(function() {
                    alert($(this).data("id"));
                });
            }

            if (sCurrentPath.indexOf("view_product") > -1) {
                var current_product = _getCurrentProduct(); // GETS THE CURRENT PRODUCT FROM THE LOCAL STORAGE
                _getAllProductCategory();

                if (current_product !== null && current_product !== undefined && JSON.stringify(current_product) != "{}") {
                    _build_view_product_UI({
                        status: "display"
                    });
                }
                // _build_view_product_UI({status : "display"});
                $("#view_product_edit_button").click(function() {
                    // alert("test");
                });

                $("#switch1").off("change").on("change", function() {
                    var is_activeSwitch = $("#switch1");
                    _setProductIsActive(is_activeSwitch.prop("checked"));
                });
            }

            if (sCurrentPath.indexOf("edit_product") > -1) {
                _build_compositeProductList($("#composite_product_list_UI")); //BUILDS THE COMPOSITE PRODUCT LIST
                _UI_editProduct(); // BUILDS THE UI FOR THE EDIT PRODUCT PAGE

                /*$("#submit_edit_product").click(function() {
					_update();
				});*/
                // $("#submit_delete_product").click(function() {
                // 	// _delete();
                // });

                $(".ui-deletable").click(function() {
                    $(this).remove();
                });

                $(".drag-list").bind("DOMNodeInserted", function() {
                    $(".drag-list").children().each(function() {
                        $(this).addClass(".ui-deletable");
                        $(this).bind("click", function() {
                            // $(this).remove();
                        })
                    });
                });

                var oCompo = cr8v_platform.localStorage.get_local_storage({
                    name: "productList"
                });
                console.log(oCompo);

                $(".composite-activated label").off("click").on("click", function() {
                    if ($(".composite-activated .toggleSwitch input").is(":checked")) {
                        $('div[modal-id="compo-product"]').addClass("showed");
                        $(".toggleSwitch input").attr("checked", false);
                    } else {
                        $(".composite-add-product").slideDown(500);
                        $(".remove-composite").show();
                        $('div[modal-id="compo-product"]').removeClass("showed");
                    }
                });
            }

            /*if(sCurrentPath.indexOf("product_management") > -1) {	
				sProduct_build = _build_product_management_UI();
				_render({element : $("#product_management_div_UI"), data : sProduct_build});
				$(".view_product").click(function() {
					_build_view_product_UI({iProduct_id : $(this).data("id"), status : "get"});
				});
			}*/

        }

        $('#confirmDelete').off("click").on("click", function() {
            _delete();
        });

        /* FOR DELETE PRODUCT EVENTS */
        var uiTrashBtn = $('#deleteProduct');
        uiTrashBtn.off("click.delete").on("click.delete", function() {
            $("div.delete-modal").addClass("showed");

            $("div.delete-modal .close-me").off("click").on("click", function() {
                $("body").css({
                    'overflow-y': 'initial'
                });
                $("div.delete-modal").removeClass("showed");
            });
        });
        /* END FOR DELETE PRODUCT EVENTS */
    }

    function get(options) {
        var id = options.id,
            name = options.name,
            sku = options.sku,
            category = options.category,
            offset = options.offset,
            limit = options.limit,
            sorting = options.sorting,
            sort_field = options.sort_field;

        $.ajax({
            type: "POST",
            datatype: "html",
            data: {
                id: id,
                name: name,
                sku: sku,
                category: category,
                offset: offset,
                limit: limit,
                sorting: sorting,
                sort_field: sort_field,
            },
            returnType: "json",
            url: BASEURL + "products/get",
            success: function(oResult) {
                console.log(oResult);
            }
        });
    }

    function getAllProducts() {
        var oOptions = {
            type: "GET",
            data: {
                test: true
            },
            url: BASEURL + "products/get_all_products",
            success: function(oResult) {
                CProducts.setProductObject(oResult.data);
            }
        }
        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    function getUnitOfMeasurement() {
       var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "products/get_unit_of_measure",
			returnType : "json",
			success : function(oResult) {


				oUnitOfMeasure = oResult.data;
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

   function buildUnitOfMeasure(oData) {
       // Added Unit Measure
		var uiSelectMeasurement = $("#unit-of-measure");

		uiSelectMeasurement.html("");

		for(var x in oData)
		{
			if(oData[x]['name'] == "kg" || oData[x]['name'] == "mt" || oData[x]['name'] == "L" )
			{					
				var sHtmlMeasure = '<option value="'+oData[x]['id']+'">'+oData[x]['name']+'</option>';
				 uiSelectMeasurement.append(sHtmlMeasure);
			}

		}

		setTimeout(function(){
			$("#unit-of-measure").show().css({
			"padding" : "5px 6px",
			"width" :  "60px"
			});
			$("#unit-measure-drop-down").find(".frm-custom-dropdown").remove();
		}, 500);

		// End Added Unit Measure

    }


    function buildUnitOfMeasureUpdate(unit_id) {
       // Added Unit Measure
		var uiSelectMeasurement = $("#unit-of-measure");

			var setMeasurement = setInterval(function(){
				var oData = oUnitOfMeasure;
				
				if(oData !== null)
				{
					uiSelectMeasurement.html("");

					for(var x in oData)
					{
						if(oData[x]['id'] == unit_id)
						{				
							var sHtmlMeasure = '<option value="'+oData[x]['id']+'" selected>'+oData[x]['name']+'</option>';
							 uiSelectMeasurement.append(sHtmlMeasure);
						}else{
							var sHtmlMeasure = '<option value="'+oData[x]['id']+'">'+oData[x]['name']+'</option>';
							 uiSelectMeasurement.append(sHtmlMeasure);
						}

					}

					setTimeout(function(){

						$("#unit-of-measure").show().css({
						"padding" : "5px 6px",
						"width" :  "60px"
						});
						$("#unit-measure-drop-down").find(".frm-custom-dropdown").remove();
					}, 500);
					clearInterval(setMeasurement);
				}
			}, 300);

		

		

		// End Added Unit Measure

    }


    function selectUnitMeasure(unit_id)
    {
		var oData = oUnitOfMeasure,
			sSelected = '';
		

		for(var x in oData)
		{
			if(oData[x]['id'] == unit_id )
			{					
				sSelected = oData[x]['name'];
			}

		}

		return sSelected;

    }

    function setProductObject(oResults) {
        // if(oProducts === null
        // || oProducts === undefined
        // || oProducts === "") {
        var oProductsByCategory = oResults;

        for (var i in oProductsByCategory) {
            if (oProductsByCategory[i].hasOwnProperty("products")) {
                for (var x in oProductsByCategory[i]["products"]) {
                    oProducts[Object.keys(oProducts).length] = oProductsByCategory[i]["products"][x];
                }
            }
        }

        cr8v_platform.localStorage.delete_local_storage({
            name: "productList"
        });
        cr8v_platform.localStorage.set_local_storage({
            name: "productList",
            data: oProducts
        });
        // }

        if (window.location.href.indexOf("product_management") > -1) {
            _buildGridAndListView(oProductsByCategory);
        }

        /*var sProduct_build = _build_product_management_UI();

		_render({element : "product_management_div_UI", data : sProduct_build});	
		$(".view_product").click(function() {
			_build_view_product_UI({iProduct_id : $(this).data("id"), status : "get"});
		});
		return oProducts;*/
    }

    function test() {
        // var oElement = {
        // 	"alert_this" : function(oData) {
        // 		alert("this");
        // 	},
        // 	"alert_that" : function(oData) {
        // 		alert("that");
        // 	}
        // }

        // if (typeof (oElement[element.div]) == "function") {
        // 	oElement[element.div](element.div);
        // }
        var element = $("#composite_product_list_UI");
        _buildTemplate({
            name: element[0].id
        });
    }

    function get_sku() {
        var oOptions = {
            type: "GET",
            data: {
                data: ""
            },
            returnType: "json",
            url: BASEURL + "products/generate_sku",
            success: function(oResult) {
                // alert(JSON.stringify(oResult));
                _setNewSku(oResult.data);
            }
        }

        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    // ================================================== //
    // =================PRIVATE METHODS================== //
    // ================================================== //

    function _render(oParams) {
        var element = oParams.element,
            oData = oParams.data,
            uiElement = $("#" + element);

        if (element == "add_product_form") { //UI FOR ADD PRODUCT AFTER PRODUCT IS ADDED.
            var uiAddProductForm = $("#add_product_form :input"),
                uiItemCategory = $("#itemCategory");

            uiAddProductForm.val(oData);
            uiItemCategory.val(1);
        }

        if (element == "product_management_div_UI") { //THE UI FOR PRODUCT_MANAGEMENT PAGE.
            uiElement.html(oData);
        }

        if (element == "view_product") {
            var skuField = $("#view_product_sku"),
                nameField = $("#view_product_name"),
                categoryField = $("#view_product_category"),
                quantityThresholdField = $("#view_product_quantity_threshold"),
                shelfLifeField = $("#view_product_shelf_life"),
                minimumField = $("#view_product_minimum"),
                maximumField = $("#view_product_maximum"),
                priceField = $("#view_product_price"),
                descriptionField = $("#view_product_description"),
                breadcrumbField = $("#view_product_breadcrumb"),
                imageField = $("#image_view"),
                uiSwitch = $("#switch1"),
                uiGuarantee = $("#view_product_guarantee"),
                oProductCategories = cr8v_platform.localStorage.get_local_storage({
                    name: "allProductCategory"
                });

			//  console.log(JSON.stringify(oData));
            for (var a in oProductCategories) {
                if (oProductCategories[a].id == oData.product_category) {
                    oData.category_name = oProductCategories[a].name;
                }
            }
			
			var setMeasurement = setInterval(function(){
				var sGetUnitOfMeasure = selectUnitMeasure(oData.unit_of_measure_id);
				
				if(sGetUnitOfMeasure !== null)
				{
					minimumField.html($.number(oData.threshold_min, 0) +" "+sGetUnitOfMeasure.toUpperCase());
            		maximumField.html($.number(oData.threshold_maximum, 0) +" "+sGetUnitOfMeasure.toUpperCase());
					clearInterval(setMeasurement);
				}
			}, 300);


            skuField.html(oData.sku);
            nameField.html(oData.name);
            categoryField.html(oData.category_name);
            quantityThresholdField.html(" ");
            shelfLifeField.html($.number(oData.product_shelf_life, 0));
           
            priceField.html(oData.unit_price+" Php");
            descriptionField.html(oData.description);
            breadcrumbField.html(oData.name);
            uiGuarantee.html($.number(oData.guarantee, 2)+"%");
	

            imageField.html('<img class="img-responsive f-left width-30per margin-top-10" src="' + oData.image + '">') //THE UI FOR VIEW PRODUCT EMPTY (SINGLE PRODUCT VIEWING).

            if (Object.keys(oData.product_composition).length > 0) {
                $("#num_material").html(Object.keys(oData.product_composition).length);
                var uiContainer = $('.materials-container');
                for (var i in oData.product_composition) {
                    var item = oData.product_composition[i],
                        sHtml = '<div class="box-shadow-light margin-top-10 display-inline-mid width-50per padding-all-10 margin-right-5">' +
                        '<div class="f-right">' +
                        '<p class="font-bold black-color font-15">' + item.composition_quantity + ' Unit</p>' +
                        '</div>' +
                        '<div class="clear"></div>' +
                        '<div class="display-inline-mid image-content-product">' +
                        '<img class="" src="' + item.image + '">' +
                        '</div>' +
                        '<div class="display-inline-mid width-45per text-left margin-left-20">' +
                        '<p class="font-bold black-color font-15">' + item.name + '</p>' +
                        '<p class="italic font-bold margin-top-5">SKU #' + item.sku + '</p>' +
                        '</div>' +
                        '</div>';
                    var $elem = $(sHtml);
                    uiContainer.append($elem);
                }
            }

            if (oData.is_active == "1") {
                uiSwitch.prop("checked", true);
            }

            uiSwitch.attr("data-id", oData.id);

        }

        if (element == "composite_product_list_UI") {
            uiElement.html(oData);
            bindEvents("draggable_composite_product");
        }

        if (element == "edit_product") {
            var oProductComposition = oData.product_composition,
                uiItemId = $("#itemID"),
                uiSku = $("#itemSku"),
                uiItemName = $("#itemName"),
                uiItemCategory = $("#itemCategory"),
                uiMaxThreshold = $("#maxThreshold"),
                uiMinThreshold = $("#minThreshold"),
                uiItemShelfLife = $("#itemShelfLife"),
                uiItemPrice = $("#itemPrice"),
                uiItemGuarantee = $("#itemGuarantee"),
                uiItemDescription = $("#itemDescription");

                console.log(JSON.stringify(oData));

            uiItemId.val(oData.id);
            uiSku.val(oData.sku);
            uiItemName.val(oData.name);
            uiItemCategory.val(oData.product_category);
            uiMinThreshold.val(oData.threshold_min);
            uiMaxThreshold.val(oData.threshold_maximum);
            uiItemShelfLife.val(oData.product_shelf_life);
            uiItemPrice.val(oData.unit_price);
            uiItemDescription.val(oData.description);
            uiItemGuarantee.val(oData.guarantee);

            buildUnitOfMeasureUpdate(oData.unit_of_measure_id);

            uiItemGuarantee.number(true, 0);
            uiItemShelfLife.number(true, 0);
            uiMinThreshold.number(true, 0);
            uiMaxThreshold.number(true, 0);
            
            var iTimeoutGuarantee = {};
            uiItemGuarantee.on('keyup.guarantee', function(){
                var uiThis = uiItemGuarantee,
                    val = parseFloat(uiThis.val())

                clearTimeout(iTimeoutGuarantee);
                iTimeoutGuarantee = setTimeout(function() {
                    if(val > 100){
                        uiThis.val(100);
                    }
                }, 300);
                
            });

            if (Object.keys(oProductComposition).length > 0) {
                $('#switch1').trigger('click');
            }

            for (var a in oProductComposition) {
                var product_template = '<div class="items single_composite_product ui-draggable ui-draggable-handle ui-draggable-dragging ui-deletable" data-id="' + oProductComposition[a].id + '" data-id="' + oProductComposition[a].id + '"  style="position:relative;">' + '<div class="placeholder-img">' + "<div class='product-list-image left-prod-image' style='background-image:url(" + oProductComposition[a].image + ")'></div>" + '</div>' + '<div>' + '<p>' + oProductComposition[a].name + '</p>' + '<p>SKU #' + oProductComposition[a].sku + '</p>' + '</div>' + '<div class="width-20px" style="position:absolute; top: 25px; right: 40px;">' + '<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor delete-compo">' + '</div>' + '</div>';
                $(".drag-list").append(product_template);
            }

            var uiCloseBtn = $(".drag-list").find(".delete-compo");
            uiCloseBtn.click(function() {
                $(this).closest('.items.single_composite_product').remove();
            });

            var uploadInstance = $('#upload_photo').cr8vUpload({
                url: BASEURL + "products/update",
                backgroundPhotoUrl: oData.image,
                titleText: "Change Photo",
                accept: "jpg,png",
                autoUpload: false,
                onSelected: function(bValid) {},
                success: function(oResult, textStatus, jqXHR) {
                    if (oResult.data > 0) {
                        var options = {
                            title: "Success!",
                            message: oResult.message,
                            speed: "slow",
                            icon: successIcon
                        };

                        localStorage.removeItem("currentProduct");
                        localStorage.setItem("currentProduct", JSON.stringify(oResult.product_data));

                        $("body").feedback(options);

                        setTimeout(function() {
                            window.location.href = BASEURL + "products/view_product";
                        }, 1000);
                    } else {
                        var options = {
                            title: "Error!",
                            message: oResult.message,
                            speed: "slow",
                            icon: failIcon
                        };

                        $("body").feedback(options);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {

                }
            });

            $("#submit_edit_product").click(function() {
                _update(uploadInstance);
            });
        }
    }

    function _setProductIsActive(is_active) {
        var is_active_value = 0,
            oOptions = {},
            id = $("#switch1").data("id");

        (is_active) ? is_active_value = 1 : is_active_value = 0;
        oOptions = {
            type: "POST",
            data: {
                id: id,
                is_active: is_active_value
            },
            returnType: "json",
            url: BASEURL + "products/set_product_is_active",
            success: function(oResult) {
                var sStatus = "",
                    sMessage = "",
                    oFeedback = {},
                    sIcon = "";

                (oResult.status) ? sStatus = "Success!" : sStatus = "Fail!";
                (oResult.status) ? sIcon = successIcon : sIcon = failIcon;
                oFeedback = {
                    title: sStatus,
                    message: oResult.message,
                    icon: sIcon,
                    speed: "mid"
                };

                $("body").feedback(oFeedback);

            }
        }

        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    function _setNewSku(newSku) {
        $("#itemSku").val(newSku);
    }

    function _listContainerTemplate() {
        var sHtml = '<div class="panel-group margin-top-20">';
        sHtml += '<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">';
        sHtml += '<div class="panel-heading panel-heading"> ';
        sHtml += '<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#"><i class="fa fa-caret-down margin-right-5"></i><span class="cat-name"></span></a>';
        sHtml += '<p class="f-right font-15 font-500  num-products-in-cat"></p>';
        sHtml += '<div class="clear"></div>';
        sHtml += '</div>';
        sHtml += '<div class="panel-collapse collapse in">';
        sHtml += '<div class="panel-body bggray-white"> </div>';
        sHtml += '</div>';
        sHtml += '</div>';
        sHtml += '</div>';
        return sHtml;
    }

    function _gridTemplate() {
        var sHtml = '<div class="box-shadow-dark border-top border-blue display-inline-mid padding-all-10 width-33per margin-right-5 margin-top-10 " >';
        sHtml += '<div class="product-list-image"> </div>';
        sHtml += '<p class="font-bold margin-top-15 black-color font-20 prod-name"></p>';
        sHtml += '<div class="f-left text-left margin-top-20">';
        sHtml += '<p class="black-color font-bold">SKU #<span class="pro-sku"></span></p>';
        sHtml += '<p class="black-color font-bold"> <span class="prod-quantity">0</span> Items </p>';
        sHtml += '</div>';

        sHtml += '<div class="f-right margin-top-20 margin-right-10">';
        sHtml += '<a href="javascript:void(0)" class="red-color font-15 font-bold prod-status"> DEACTIVATED </a>';
        sHtml += '</div>';
        sHtml += '</div>';
        return sHtml;
    }

    function _tableTemplate() {
        var sHtml = '<table class="tbl-4c3h margin-top-20">';
        sHtml += '<thead>';
        sHtml += '<tr>';
        sHtml += '<th class="black-color">Item Name</th>';
        sHtml += '<th class="black-color">SKU Number</th>';
//         sHtml += '<th class="black-color">No. of Batches</th>';
//         sHtml += '<th class="black-color">Total Quantity</th>';
        sHtml += '<th class="black-color">Status</th>';
        sHtml += '</tr>';
        sHtml += '</thead>';
        sHtml += '</table>';
        return sHtml;
    }

    function _tableListTemplate() {
        var sHtml = '<div class="tbl-like ">';
        sHtml += '<div class="first-tbl no-padding-all mngt-tbl-content">';
        sHtml += '<div class="display-inline-mid " style="width: 40%; padding-left: 40px;"> <p class="font-400 f-none font-14 prod-name"> </p> </div>';
        sHtml += '<div class="display-inline-mid" style="width: 40%; padding-left: 40px;"> <p class="font-400 f-none font-14 prod-sku"></p> </div>';
//         sHtml += '<div class="display-inline-mid"> <p class="font-400 f-none font-14 prod-no-batches"> 0 </p> </div>';
//         sHtml += '<div class="display-inline-mid"> <p class="font-400 f-none font-14 prod-quantity">  </p> </div>';
        sHtml += '<div class="display-inline-mid" style="width: 20%;">  <a class="prod-status red-color font-400 f-none font-14" href="javascript:void(0)">DEACTIVATED</a> </div>';
        sHtml += '</div>';
        sHtml += '<div class="hover-tbl" style="height: 50px;">';
        sHtml += '<a href="javascript:void(0)">';
        sHtml += '<button class="btn general-btn view-product-details">View Item Details</button>';
        sHtml += '</a>';
        sHtml += '</div>';
        sHtml += '</div>';
        return sHtml;
    }

    function _buildGridAndListView(oProductsByCategory) {
        var gridContainer = $("#product_management_div_UI"),
            listDiplayContainer = $("#listDiplayContainer");
        for (var i in oProductsByCategory) {
            var listContainerTemplate = $(_listContainerTemplate()),
                uiListContainer = listContainerTemplate.find(".panel-body");

            listContainerTemplate.find("span.cat-name").html(oProductsByCategory[i]["category_name"]);

            if (oProductsByCategory[i].hasOwnProperty("products")) {
                listContainerTemplate.find(".num-products-in-cat").html(Object.keys(oProductsByCategory[i]["products"]).length + " Products in this Category");
                for (var x in oProductsByCategory[i]["products"]) {
                    var prod = oProductsByCategory[i]["products"][x],
                        uiList = $(_gridTemplate());

                    uiList.find(".prod-name").html(prod.name);
                    uiList.find(".product-list-image").css({
                        "background-image": "url(" + prod.image + ")"
                    });
                    uiList.find(".pro-sku").html(prod.sku);
                    uiList.data("id", prod.id);

                    if (prod.is_active == "1") {
                        uiList.find(".prod-status").html('ACTIVATED');
                        uiList.find('.prod-status').removeClass('red-color').addClass("green-color");
                    }

                    uiList.click(function() {
                        var sID = $(this).data("id");
                        _build_view_product_UI({
                            iProduct_id: sID,
                            status: "get"
                        });
                        window.location.href = BASEURL + "products/view_product";
                    });

                    uiList.hover(function() {
                        $("body").css("cursor", "pointer");
                    }, function() {
                        $("body").css("cursor", "default");
                    });

                    uiList.find(".prod-quantity").html(Object.keys(prod.product_composition).length);

                    uiListContainer.append(uiList);
                }
            }
            gridContainer.append(listContainerTemplate);
        }

        for (var i in oProductsByCategory) {
            var listContainerTemplate = $(_listContainerTemplate()),
                uiListContainer = listContainerTemplate.find(".panel-body");

            listContainerTemplate.find("span.cat-name").html(oProductsByCategory[i]["category_name"]);

            if (oProductsByCategory[i].hasOwnProperty("products")) {
                uiListContainer.append(_tableTemplate());
                listContainerTemplate.find(".num-products-in-cat").html(Object.keys(oProductsByCategory[i]["products"]).length + " Products in this Category");
                for (var x in oProductsByCategory[i]["products"]) {
                    var prod = oProductsByCategory[i]["products"][x],
                        uiList = $(_tableListTemplate());

                    uiList.find(".prod-name").html(prod.name);
                    uiList.find(".prod-sku").html(prod.sku);
                    if (prod.is_active == "1") {
                        uiList.find(".prod-status").html('ACTIVATED');
                        uiList.find('.prod-status').removeClass('red-color').addClass("green-color");
                    }
                    uiList.data("id", prod.id);

                    uiList.find(".view-product-details").click(function() {
                        var uiParent = $(this).closest('.tbl-like'),
                            sID = uiParent.data("id");
                        _build_view_product_UI({
                            iProduct_id: sID,
                            status: "get"
                        });
                        window.location.href = BASEURL + "products/view_product";
                    });



                    uiListContainer.append(uiList);
                }
            }
            listDiplayContainer.append(listContainerTemplate);
        }

        /* ACCORDION */
        $(".panel-group .panel-heading").each(function() {
            var ps = $(this).next(".panel-collapse");
            var ph = ps.find(".panel-body").outerHeight();

            if (ps.hasClass("in")) {
                $(this).find("h4").addClass("active")
            }

            $(this).find("a").off("click").on("click", function(e) {
                e.preventDefault();
                ps.css({
                    height: ph
                });

                if (ps.hasClass("in")) {
                    $(this).find("h4").removeClass("active");
                    $(this).find(".fa").removeClass("fa-caret-down").addClass("fa-caret-right");
                    ps.removeClass("in");
                } else {
                    $(this).find("h4").addClass("active");
                    $(this).find(".fa").removeClass("fa-caret-right").addClass("fa-caret-down");
                    ps.addClass("in");
                }

                setTimeout(function() {
                    ps.removeAttr("style")
                }, 500);
            });
        });
    }

    function _add(sImagePath) {
        var img = (sImagePath) ? sImagePath : "";
        var oOptions = {
            type: "POST",
            data: {
                image: img,
                product_name: $("#itemName").val(),
                item_category: $("#itemCategory").val(),
                min_threshold: $("#minThreshold").val(),
                max_threshold: $("#maxThreshold").val(),
                sku: $("#itemSku").val(),
                product_shelf_life: $("#itemShelfLife").val(),
                unit_price: $("#itemPrice").val(),
                description: $("#itemDescription").val(),
                product_composition: _getSelectedCompositeProducts(),
                guarantee : $("#itemGuarantee").val(),
                unit_measure : $("#unit-of-measure option:selected").val()
            },
            url: BASEURL + "products/add",
            success: function(oResult) {
                getAllProducts();
                _render({
                    element: "add_product_form",
                    oData: ""
                });
                var options = {
                    title: "Success!",
                    message: "Product added!",
                    speed: "slow",
                    withShadow: true
                };
                $("body").feedback(options);
                window.location.href = BASEURL+"products/product_management";
            }
        };

        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    function _update(uploadInstance) {
        var itemID = $("#itemID").val(),
            itemName = $("#itemName").val(),
            itemCategory = $("#itemCategory option:selected").val(),
            minThreshold = $("#minThreshold").val(),
            maxThreshold = $("#maxThreshold").val(),
            itemShelfLife = $("#itemShelfLife").val(),
            itemPrice = $("#itemPrice").val(),
            itemDescription = $("#itemDescription").val(),
            itemGuarantee = $("#itemGuarantee").val(),
            iUnit_measure = $("#unit-of-measure option:selected").val(),
            productComposition = _getSelectedCompositeProducts();

        var error = 0,
            errMessage = [];
        if(itemName.trim().length == 0){
            error++;
            errMessage.push('Item name required');
        }
        if(minThreshold.trim().length == 0){
            error++;
            errMessage.push('Min treshold required');
        }
        if(maxThreshold.trim().length == 0){
            error++;
            errMessage.push('Max treshold required');
        }
        if(itemPrice.trim().length == 0){
            error++;
            errMessage.push('Price is required');
        }
        if(itemDescription.trim().length == 0){
            error++;
            errMessage.push('Description is required');
        }
        if(itemGuarantee.trim().length == 0){
            error++;
            errMessage.push('Percent guarantee required');
        }else{
            if(itemGuarantee == 0){
                error++;
                errMessage.push('Percent guarantee must be greater than zero');
            }
        }

        if( parseFloat(minThreshold) > parseFloat(maxThreshold)){
            error++;
            errMessage.push('Max treshold must be greated than min treshold');
        }

        console.log(minThreshold);
        console.log(maxThreshold);

        if(error > 0){
            $("body").feedback({
                title: "Message",
                message: errMessage.join(", "),
                speed: "slow",
                icon: failIcon
            });
        }

        if (uploadInstance && error == 0) {
            uploadInstance.updateOptions({
                "extraField": {
                    itemID: itemID,
                    itemName: itemName,
                    itemCategory: itemCategory,
                    minThreshold: minThreshold,
                    maxThreshold: maxThreshold,
                    itemShelfLife: itemShelfLife,
                    itemPrice: itemPrice,
                    itemDescription: itemDescription,
                    productComposition: productComposition,
                    guarantee : itemGuarantee,
                    unit_measure : iUnit_measure
                }
            });

            uploadInstance.startUpload();
        }





        /*var post_data = {
			type : "POST",
			data : { 
				itemID : itemID, 
				itemName : itemName, 
				itemCategory : itemCategory, 
				minThreshold : minThreshold, 
				maxThreshold : maxThreshold, 
				itemShelfLife : itemShelfLife, 
				itemPrice : itemPrice, 
				itemDescription : itemDescription, 
				productComposition : productComposition
			},
			url : BASEURL + "products/update",
			success : function(oResult) {
				if(oResult.data > 0) {
					var options = { title : "Success!", message : oResult.message, speed : "slow", icon : successIcon };
					
					$("body").feedback(options);
					_updateCurrentProduct();
					window.location.href = BASEURL + "products/view_product";
				} else {
					var options = {title : "Error!", message : oResult.message, speed : "slow", icon : failIcon };
					
					$("body").feedback(options);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(post_data);*/
    }

    function _delete() {
        var oCurrentProduct = _getCurrentProduct({
                name: "currentProduct"
            }),
            iCurrentProductID = oCurrentProduct.id;

        productComposition = _getSelectedCompositeProducts();

        var post_data = {
            type: "POST",
            data: {
                id: iCurrentProductID
            },
            url: BASEURL + "products/delete",
            success: function(oResult) {
                if (oResult.status) {
                    var options = {
                        title: "Success!",
                        message: oResult.message,
                        speed: "slow",
                        icon: successIcon
                    };

                    $("body").feedback(options);
                    _updateCurrentProduct();
                    window.location.href = BASEURL + "products/product_management";
                } else {
                    var options = {
                        title: "Fail!",
                        message: oResult.message,
                        speed: "slow",
                        icon: failIcon
                    };

                    $("body").feedback(options);
                }
            }
        };

        cr8v_platform.CconnectionDetector.ajax(post_data);
    }

    function _update_multi() {
        //YUNG MAY QUALIFIER NA UPDATE METHOD (ACCORDING SA DDD)./
        //asdfgh
    }

    function _build_update_data() {
        var oUpdate_data = {},
            oQualifier = {},
            oData = {},
            oObject = {},
            iCounter = 0,
            sName_val = $("#itemName").val(),
            sCat_val = $("#itemCategory option:selected").val(),
            sMin_val = $("#minThreshold").val(),
            sMax_val = $("#maxThreshold").val(),
            iShelf_life_val = $("#itemShelfLife").val(),
            sPrice_val = $("#itemPrice").val(),
            sDesc_val = $("#itemDescription").val();


        if (sName_val !== "" && sName_val !== null && sName_val.length !== 0 && sName_val !== undefined) {
            oObject = {
                field: "itemName",
                operation: "=",
                value: sName_val
            };
            oQualifier[iCounter] = oObject;
            oData["itemName"] = sName_val;
            ++iCounter;
        }

        if (sCat_val !== "" && sCat_val !== null && sCat_val.length !== 0 && sCat_val !== undefined) {
            oObject = {
                field: "itemCategory",
                operation: "=",
                value: sCat_val
            };
            oQualifier[iCounter] = oObject;
            oData["itemCategory"] = sCat_val;
            ++iCounter;
        }

        if (sMin_val !== "" && sMin_val !== null && sMin_val.length !== 0 && sMin_val !== undefined) {
            oObject = {
                field: "minThreshold",
                operation: "=",
                value: sMin_val
            };
            oQualifier[iCounter] = oObject;
            oData["minThreshold"] = sMin_val;
            ++iCounter;
        }

        if (sMax_val !== "" && sMax_val !== null && sMax_val.length !== 0 && sMax_val !== undefined) {
            oObject = {
                field: "maxThreshold",
                operation: "=",
                value: sMax_val
            };
            oQualifier[iCounter] = oObject;
            oData["maxThreshold"] = sMax_val;
            ++iCounter;
        }

        if (iShelf_life_val !== "" && iShelf_life_val !== null && iShelf_life_val.length !== 0 && iShelf_life_val !== undefined) {
            oObject = {
                field: "itemShelfLife",
                operation: "=",
                value: iShelf_life_val
            };
            oQualifier[iCounter] = oObject;
            oData["itemShelfLife"] = iShelf_life_val;
            ++iCounter;
        }

        if (sPrice_val !== "" && sPrice_val !== null && sPrice_val.length !== 0 && sPrice_val !== undefined) {
            oObject = {
                field: "itemPrice",
                operation: "=",
                value: sPrice_val
            };
            oQualifier[iCounter] = oObject;
            oData["itemPrice"] = sPrice_val;
            ++iCounter;
        }

        if (sDesc_val !== "" && sDesc_val !== null && sDesc_val.length !== 0 && sDesc_val !== undefined) {
            oObject = {
                field: "itemDescription",
                operation: "=",
                value: sDesc_val
            };
            oQualifier[iCounter] = oObject;
            oData["itemDescription"] = sDesc_val;
            ++iCounter;
        }

        oUpdate_data["qualifier"] = oQualifier;
        oUpdate_data["data"] = oData;
        return oUpdate_data;
    }

    function _buildTemplate(oParams) {
        var template = {};

        var oElement = {
            "composite_product_list_UI": function(oData) { // PRODUCT COMPOSITION LIST DIV
                var template_title = '<div class="items single_composite_product" data-id="',
                    template_title_close = '">' + '<div class="placeholder-img">',
                    template_content = '</div>' + '<div>',
                    template_footer = '</div>' + '</div>';

                template["title"] = template_title;
                template["title_close"] = template_title_close;
                template["content"] = template_content;
                template["footer"] = template_footer;
            },
            "product_management_div_UI": function(oData) { // PRODUCT MANAGEMENT MAIN DIV
                var template_title = "",
                    template_content = "",
                    template_footer = "";

                template_title += '<div class="panel-group margin-top-20">' + '<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">' + '<!-- step 1 -->' + '<div class="panel-heading panel-heading">' + '<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">' + '<i class="fa fa-caret-down margin-right-5"></i>';
                //Grains
                template_content += '</a>' + '<p class="f-right font-15 font-500  ">6 Products in this Category</p>' + '<div class="clear"></div>' + '</div>' + '<div class="panel-collapse collapse in">' + '<div class="panel-body bggray-white">';

                template_footer += '</div>' + '</div>' + '</div>' + '</div>';

                template["title"] = template_title;
                template["content"] = template_content;
                template["footer"] = template_footer;
            }
        }

        if (typeof(oElement[oParams.name]) == "function") {
            oElement[oParams.name](oParams.name);
        }

        return template;
    }

    function _getSelectedCompositeProducts(iProduct_id) {
        var ui_dragList = $(".drag-list"),
            oComposite_products = {},
            counter = 0,
            composite_product = {};

        ui_dragList.find(".single_composite_product").each(function() {
            var len = Object.keys(composite_product).length,
                input = $(this).find('input');
            composite_product[len] = {
                base_product_id: $(this).data("id"),
                quantity: input.val()
            };
        });

        oComposite_products = JSON.stringify(composite_product);
        return oComposite_products;
    }

    function _setCurrentProduct(oProduct) {
        var iProd_cat = oProduct.product_category;
        oAllProductCategory = cr8v_platform.localStorage.get_local_storage({
            name: "allProductCategory"
        });
        for (var a in oAllProductCategory) {
            if (iProd_cat == oAllProductCategory.product_category) {
                oProduct["category_name"] = oAllProductCategory[a].name;
            }
        }
        oData = {
            name: "currentProduct",
            data: oProduct
        };

        cr8v_platform.localStorage.delete_local_storage({
            name: "currentProduct"
        });
        cr8v_platform.localStorage.set_local_storage(oData);
    }

    function _getCurrentProduct() {
        var current_product = {};

        current_product = cr8v_platform.localStorage.get_local_storage({
            name: "currentProduct"
        });
        return current_product;
    }

    function _updateCurrentProduct() {
        getAllProducts(); //UPDATES THE LOCAL STORAGE WITH THE NEW DB RECORDS.
        var current_product = _getCurrentProduct();

        for (var a in oProducts) {
            if (oProducts[a].id == current_product.id) {
                _setCurrentProduct(oProducts[a]);
            }
        }
    }

    function _getAllProductCategory() {
        var oOptions = {
            type: "GET",
            data: {
                test: ""
            },
            returnType: "json",
            url: BASEURL + "products/get_all_product_category",
            success: function(oResult) {
                _setAllProductCategory(oResult.data);
                // alert(JSON.stringify(oResult.data));
            }
        }
        cr8v_platform.CconnectionDetector.ajax(oOptions);
    }

    function _setAllProductCategory(oResult) {
        var oAllProductCategory = {
            name: "allProductCategory",
            data: oResult
        };

        cr8v_platform.localStorage.delete_local_storage({
            name: "allProductCategory"
        });
        cr8v_platform.localStorage.set_local_storage(oAllProductCategory);
    }

    // ================================================== //
    // ============END OF PRIVATE METHODS================ //
    // ================================================== //

    // ================================================== //
    // ===========PRODUCT MANAGEMENT DISPLAY============= //
    // ================================================== //

    function _build_product_management_UI() {
        var productCategory = {},
            counter = 0;

        if (oProducts !== null && oProducts !== undefined && oProducts !== "") {
            for (var a in oProducts) {
                if (JSON.stringify(productCategory) != "{}") {
                    for (var b in productCategory) {
                        if (productCategory[b] != oProducts[a].product_category) {
                            productCategory[counter] = oProducts[a].product_category;
                            ++counter;
                        }
                    }
                } else {
                    productCategory[counter] = oProducts[a].product_category;
                    ++counter;
                }
            }
        }

        if (JSON.stringify(productCategory) != "{}") {
            var template = _buildTemplate({
                    name: "product_management_div_UI"
                }),
                template_title = template.title,
                template_content = template.content,
                template_footer = template.footer,
                products = "",
                product = "",
                product_display = "";

            for (var c in oProducts) {
                product = "";
                product += '<a href="' + BASEURL + 'products/view_product" class="view_product" data-id="' + oProducts[c].id + '">' +
                		   '<div class="box-shadow-dark border-top border-blue display-inline-mid padding-all-10 width-33per margin-right-5 margin-top-10 ">' +
                		   '<div class="">' + '<div class="width-100per product-list-image" style="background-image:url(' + oProducts[c].image + ')"></div>' +
                		   '</div>' + '<p class="font-bold margin-top-15 black-color font-20">' + oProducts[c].name + '</p>' + '<div class="f-left text-left margin-top-20">' +
                		   '<div class="f-left text-left margin-top-20">' + '<p class="black-color font-bold">SKU #' + oProducts[c].sku + '</p>' +
                		   '<p class="black-color font-bold">200 Items (45 Batches)</p>' + '</div>' + '</div>' +
                		   '<div class="f-right margin-top-20 margin-right-10">' + '<a href="#" class="green-color font-15 font-bold ">ACTIVATED</a>' + '</div>' +
                		   '<div class="clear"></div>' + '</div>' + '</a>';
                products += product;
            }

            product_display += template_title;
            product_display += "3";
            product_display += template_content;
            product_display += products;
            product_display += template_footer;
            return product_display;
        }
    }

    function _build_view_product_UI(oParams) {
        if (oParams.status == "get") {
            var oProduct_details = {};
            for (var a in oProducts) {
                if (oParams.iProduct_id == oProducts[a].id) {
                    oProduct_details = oProducts[a];
                }
            }
            _setCurrentProduct(oProduct_details);
        }

        if (oParams.status == "display") {
            var oProduct_details = {};

            oProduct_details = cr8v_platform.localStorage.get_local_storage({
                name: "currentProduct"
            });
            _render({
                element: "view_product",
                data: oProduct_details
            });
        }
    }

    function _build_compositeProductList(element, oProducts) {
        element.html("");

        var uiElement = element[0].id,
            template = _buildTemplate({
                name: element[0].id
            }),
            oProducts = (!oProducts) ? cr8v_platform.localStorage.get_local_storage({
                name: "productList"
            }) : oProducts,
            composite_product_view = "";

        for (var a in oProducts) {
            composite_product_view += template.title;
            composite_product_view += oProducts[a].id;
            composite_product_view += template.title_close;
            composite_product_view += "<div class='product-list-image left-prod-image' style='background-image:url(" + oProducts[a].image + ")'></div>";
            composite_product_view += template.content;
            composite_product_view += "<p>" + oProducts[a].name + "</p>";
            composite_product_view += "<p>SKU #" + oProducts[a].sku + "</p>";
            composite_product_view += template.footer;
        }

        _render({
            element: uiElement,
            data: composite_product_view
        });

        _addSearchEventLeftList();
    }

    function _addSearchEventLeftList() {
        var uiSearch = $(".search-item > input");
        uiSearch.off("keyup").on("keyup", function(e) {
            var val = $(this).val();
            if (e.keyCode == 13) {
                var oOptions = {
                    type: "POST",
                    data: {
                        key: val,
                        limit: 30,
                        sorting: "asc",
                        sort_field: "name"
                    },
                    url: BASEURL + "products/search",
                    success: function(oResult) {
                        _build_compositeProductList($("#composite_product_list_UI"), oResult.data);
                    }
                }
                cr8v_platform.CconnectionDetector.ajax(oOptions);
            }

        });
    }

    function _UI_editProduct() {
        var oProduct = _getCurrentProduct();
        $(".product-name-link").html(oProduct.name);
        _render({
            element: "edit_product",
            data: oProduct
        });

    }

    // ================================================== //
    // =========END PRODUCT MANAGEMENT DISPLAY=========== //
    // ================================================== //

    function bindSelectCategory() {
        CCategories.get(function(oCategories) {
            var saveCatMethod = function() {
                $("body").css({
                    overflow: 'hidden'
                });

                $('div[modal-id="add-new-category"]').addClass("showed");

                $('div[modal-id="add-new-category"] .close-me').on("click", function() {
                    $("body").css({
                        'overflow-y': 'initial'
                    });
                    $('div[modal-id="add-new-category"]').removeClass("showed");
                });

                // get all types of Category
                var oAjaxConfig = {
                    type: "GET",
                    url: BASEURL + "categories/get_category_type",
                    data: {
                        params: 'adsds'
                    },
                    async: false,
                    success: function(oResponse) {
                        CCategories.arrCategoryType = oResponse.data;
                        CCategories.renderElement("categoryType", CCategories.arrCategoryType);

                    }
                }
                cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);

                CCategories.bindEvents("addproduct");
            }


            if (Object.keys(oCategories).length == 0) {
                saveCatMethod();
            } else {
                var uiSelect = $("#itemCategory");
                var uiSiblings = uiSelect.siblings();
                uiSiblings.remove();
                uiSelect.removeClass("frm-custom-dropdown-origin");
                uiSelect.html("");
                oCategories[Object.keys(oCategories).length] = {
                    "category_id": "add-cat",
                    "category_name": "Add new category"
                };
                for (var i in oCategories) {
                    var $elem = $('<option value="' + oCategories[i]["category_id"] + '">' + oCategories[i]["category_name"] + '</option>');
                    uiSelect.append($elem);
                }
                uiSelect.transformDD();
                CDropDownRetract.retractDropdown(uiSelect);


                uiSelect.on("change", function() {

                    if ($(this).val() == "add-cat") {
                        saveCatMethod();
                    }
                });
            }

        });
    }

    if (window.location.href.indexOf("products") > -1) {
        getAllProducts();
        getUnitOfMeasurement();
    }

    return {
        get: get,
        bindEvents: bindEvents,
        getAllProducts: getAllProducts,
        setProductObject: setProductObject,
        bindSelectCategory: bindSelectCategory,
        test: test,
        get_sku: get_sku,
        _getAllProductCategory: _getAllProductCategory
    }
})();


$(document).ready(function() {

    if (window.location.href.indexOf("products") > -1) {
        CProducts.bindEvents();
        CProducts.bindSelectCategory();

        $(".composite-product-modal .yes-btn").off("click.modal").on("click.modal", function() {
            $('.item-list.drag-list .items.single_composite_product').remove();
            $(".add-composite-switch .switch").attr("checked", false);
            $(".composite-add-product").slideUp(500);
            $(".remove-composite").hide();
            $('div[modal-id="compo-product"]').removeClass('showed');
            $('div[modal-id="compo-product"]').removeAttr("<style></style>");
        });
    }

});