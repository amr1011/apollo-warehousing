var WithdrawalConsignee = (function(){

	var oAllItems = {},
		oAddedItems = {},
		oUnitOfMeasures = {},
		oUploadedDocuments = {},
		oAllNotes = {},
		oAllVessels = {},
		oAllConsignees = {},
		oRemovedItems = {},
		oAllLoadingArea = {},
		failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg";

	var oSaveData = {
		"mode_of_delivery" : "truck",
		"cargo_swap" : false
	};


	//HELPER FUNCTION CAPITALIZE FIRST LETTER
	function capitalizeFirstLetter(string) {
	    return string.charAt(0).toUpperCase() + string.slice(1);
	}

	/* START OF PRIVATE FUNCTIONS */

	function _getConsignees(callBack){
		var oOptions = {
			url : BASEURL+"withdrawals/get_consignee_with_receiving",
			type : "POST",
			data : { "x" : "none" },
			success: function(oResponse)
			{
				if(typeof callBack == 'function'){
					callBack(oResponse.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getVessels(callBack){
		var oOptions = {
			url : BASEURL+"withdrawals/get_vessels",
			type : "POST",
			data : { "x" : "none" },
			success: function(oResponse)
			{
				if(typeof callBack == 'function'){
					callBack(oResponse.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getLoadingArea(callBack){
		var oOptions = {
			url : BASEURL+"withdrawals/get_loading_area",
			type : "POST",
			data : { "x" : "none" },
			success: function(oResponse)
			{
				if(typeof callBack == 'function'){
					callBack(oResponse.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getUomDetails(id){
		for(var i in oUnitOfMeasures){
			if(oUnitOfMeasures[i]["id"] == id){
				return oUnitOfMeasures[i];
			}
		}

		return false;
	}

	function _getUomDetailsByName(name){
		for(var i in oUnitOfMeasures){
			if(oUnitOfMeasures[i]["name"] == name){
				return oUnitOfMeasures[i];
			}
		}

		return false;
	}

	function _transformDD(oData, ui)
	{
		ui.removeClass('frm-custom-dropdown-origin');
		
		ui.siblings('.frm-custom-dropdown').remove();

		ui.html("").show();

		for(var i in oData){
			var sSelected = '';
			sSelected = (oData[i].hasOwnProperty("selected")) ? 'selected="selected"' : '';
			
			var option = "<option value='"+oData[i]["id"]+"' "+ sSelected +" >"+oData[i]["name"]+"</option>";
			ui.append(option);
		}

		ui.transformDD();
		CDropDownRetract.retractDropdown(ui);
	}

	function _selectConsigneesEvent()
	{
		var select1 = $("#selectWithdrawalConsignee1"),
			select2 = $("#selectWithdrawalConsignee2"),
			cargoSwap = $("#cargoSwap"),
			cargoSwapVal = false,
			selectedVal1 = "",
			selectedVal2 = "",
			removedSelect1 = [],
			removedSelect2 = [];

		select1.off('change').on('change', function(e){

			selectedVal1 = $(this).val();
			selectedVal2 = select2.val();

			if(selectedVal1 == selectedVal2){
				var oConsignees = _getFilteredSelectedConsignees(1);
				_transformDD(oConsignees, select2);
			}
		});

		select2.off('change').on('change', function(e){

			selectedVal1 = select1.val();
			selectedVal2 = $(this).val();

			if(selectedVal1 == selectedVal2){
				var oConsignees = _getFilteredSelectedConsignees(2);
				_transformDD(oConsignees, select1);
			}
		});
	}

	function _selectConsignees(oData, consigneeID1, consigneeID2){
		var select1 = $("#selectWithdrawalConsignee1"),
			select2 = $("#selectWithdrawalConsignee2");
		
		oAllConsignees = oData;

		var oData1 = oData;
		for(var i in oData1){
			if(consigneeID1){
				if(oData1[i]["id"] == consigneeID1){
					oData1[i]["selected"] = true;
				}
			}
		}
		_transformDD(oData1, select1);

		select2.closest('.select').addClass('disabled');

		if(consigneeID2){
			var oData2 = oData;
			for(var i in oData2){
				if(oData2[i]["id"] == consigneeID2){
					oData2[i]["selected"] = true;
				}
			}
			_transformDD(oData2, select2);
		}

		_selectConsigneesEvent();
	}

	function _showModeOfDeliverySection(sSelect){
		switch(sSelect){
			case 'truck' :
				$('.trucking').removeClass('display-none');
				$('.vessel').addClass('display-none');

				var inputs = $('.trucking').find('input:not(.dd-txt)');
				inputs.val("");
			break;
			case 'vessel' :
				$('.trucking').addClass('display-none');
				$('.vessel').removeClass('display-none');

				var inputs = $('.vessel').find('input:not(.dd-txt)');
				inputs.val("");
			break;
		}
	}

	function _selectModeOfDelivery(){
		var select = $("#selectModeDelivery");
		select.off("change.selectmode").on("change.selectmode", function(e){
			oSaveData["mode_of_delivery"] = $(this).val();
			_showModeOfDeliverySection($(this).val());
		});
	}

	function _getFilteredSelectedConsignees(intType)
	{
		var select = $("#selectWithdrawalConsignee"+intType),
			selectVal = select.val(),
			oResponse = {};

		for(var i in oAllConsignees){
			if(oAllConsignees[i]["id"] != selectVal){
				oResponse[Object.keys(oResponse).length] = oAllConsignees[i];
			}
		}

		return oResponse;
	}

	function _cargoSwapEvent(){
		var select1 = $("#selectWithdrawalConsignee1"),
			select2 = $("#selectWithdrawalConsignee2");
		$("#cargoSwap").off("click").on("click", function(e){
			var bVal = $(this).is(":checked");
			if(bVal){
				var select1Val = select1.val(),
					oConsignees = _getFilteredSelectedConsignees(1);

				_transformDD(oConsignees, select2);
				select2.closest('.select').removeClass('disabled');
			}else{
				_transformDD([], select2);
				select2.closest('.select').addClass('disabled');
			}
			oSaveData["cargo_swap"] = bVal;
		});
	}

	function _getItems(callBack){
		var oOptions = {
			url : BASEURL+"withdrawals/get_items_from_receiving",
			type : "POST",
			data : { "data" : "none" },
			success: function(oResponse)
			{
				if(typeof callBack == 'function'){
					callBack(oResponse.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _displaySelectItems(oData){
		var uiSelect = $("#selectItem");
		_transformDD(oData, uiSelect);
		oAllItems = oData;
		_addItemEvent();
	}

	function _addedItemDetails(ui, data, bViewFirst){
		var image = ui.find(".item-image"),
			skuName = ui.find(".item-sku-name"),
			description = ui.find(".item-description"),
			radios = ui.find('.bagbulk'),
			uomLabel = ui.find(".unit-of-measure-label"),
			qtyToWithdrawLabel = ui.find(".qty-to-withdraw-label"),
			qtyToWithdraw = ui.find(".qty-to-withdraw"),
			loadingMethodLabel = ui.find(".loading-method");

		image.attr("src", data.image);
		skuName.html("#"+data.sku+" "+data.name);
		description.html(data.description);

		if(!bViewFirst){
			radios.attr("name", "bagbulk_"+data.id);
		}else{
			skuName.html(" SKU #"+data.sku+" - "+data.name);

			if(data.loading_method == 'bag'){
				sLoadingMethod = "By Bag";
			}else{
				sLoadingMethod = "By Bulk";
			}

			loadingMethodLabel.html(sLoadingMethod);

			uomLabel.html(data.unit_of_measure.name);

			qtyToWithdraw.hide();
			qtyToWithdrawLabel.html($.number(data.qty_to_withdraw, 2));
		}
	}

	function _removeItemEvent(e){
		var uiParent = $(this).closest('.item-template'),
			id = uiParent.attr("prod-id"),
			uiSelect = $("#selectItem");
		
		for(var i in oAddedItems){
			if(oAddedItems[i]["id"] == id){
				delete oAddedItems[i];
			}
		}

		uiParent.remove();

		var items = _getItemsNotAdded();
		_transformDD(items, uiSelect);
	}

	function _addedItemEvents(ui, bShowEditButton){
		if(!bShowEditButton){
			var btnEdit = ui.find(".stock-edit");
			btnEdit.remove();

			//EVENT FOR CREATE
			var uiSelect = $("#selectItem"),
				remove = ui.find(".item-remove"),
				selectUnit = ui.find(".item-unit-of-measure"),
				qtyToWithdraw = ui.find('.qty-to-withdraw'),
				radioBulk = ui.find('.bagbulk[value="bulk"]'),
				radioBag= ui.find('.bagbulk[value="bag"]');


			var insertDataToAddedItems = function(id, data){
				for(var i in oAddedItems){
					if(oAddedItems[i]["id"] == id){
						oAddedItems[i][data.key] = data.val;
					}
				}
			}

			var oUOMfiltered = {};
			for(var i in oUnitOfMeasures){
				if(oUnitOfMeasures[i]["name"] == 'kg' || oUnitOfMeasures[i]["name"] == 'mt' || oUnitOfMeasures[i]["name"] == 'L'){
					oUOMfiltered[Object.keys(oUOMfiltered).length] = oUnitOfMeasures[i];
				}
			}

			_transformDD(oUOMfiltered, selectUnit);

			qtyToWithdraw.number(true, 2);

			qtyToWithdraw.off('keyup.qtytowithdraw').on('keyup.qtytowithdraw', function(){
				var uiThis = $(this),
					uiParent = uiThis.closest('[prod-id]'),
					selectUOM = uiParent.find(".item-unit-of-measure")
					id = uiParent.attr("prod-id"),
					num = 0;

				if(uiThis.val().trim().length > 0){
					num = parseFloat(uiThis.val());
				}

				insertDataToAddedItems(id, { key: "qty_to_withdraw", val :  num });
				insertDataToAddedItems(id, { key: "qty_to_withdraw_uom", val :  selectUOM.val() });

			});

			remove.click(_removeItemEvent);

			radioBulk.off('click').on('click', function(){
				if($(this).is(":checked")){
					var parent = $(this).closest('.item-template'),
						id = parent.attr("prod-id"),
						selectUnitMeasure = parent.find(".item-unit-of-measure"),
						select = parent.find(".select");
					select.removeClass('disabled');
					insertDataToAddedItems(id, { key: "loading_method", val : "bulk" });
					insertDataToAddedItems(id, { key: "qty_to_withdraw_uom", val :  selectUnitMeasure.val() });

				}
			});

			radioBulk.prop("checked", true);
			radioBulk.trigger("click");

			radioBag.off('click').on('click', function(){
				if($(this).is(":checked")){
					var parent = $(this).closest('.item-template'),
						id = parent.attr("prod-id"),
						select = parent.find(".select");
					select.addClass('disabled');

					var uomBag = _getUomDetailsByName("bag");
					insertDataToAddedItems(id, { key: "qty_to_withdraw_uom", val :  uomBag.id });
					insertDataToAddedItems(id, { key: "loading_method", val : "bag" });
				}
			});
			//EVENT FOR CREATE
		}
		if(bShowEditButton){
			//EVENT FOR EDIT
			var btnEdit = ui.find(".stock-edit"),
				btnCancel = ui.find(".stock-cancel"),
				btnSaveChanges = ui.find(".stock-edit-save-changes");

			btnEdit.off("click").on("click", function(){
				var uiThis = $(this),
					parent = uiThis.closest(".item-template"),
					parentData = parent.data(),
					btnCancelThis = parent.find(".stock-cancel"),
					btnSaveChangesThis = parent.find(".stock-edit-save-changes");
					prodID = parent.attr("prod-id"),
					uiLoadingMethod = parent.find(".loading-method"),
					uiUOM = parent.find(".unit-of-measure-label"),
					uiQtyLabel = parent.find(".qty-to-withdraw-label"),
					uiRadioContainer = parent.find(".loading-method-radios"),
					selectUomContainer = parent.find(".select-uom"),
					selectUnit = parent.find(".unit-of-measure"),
					qtyWithdrawContainer = parent.find(".qty-to-withdraw-input-container"),
					qtyWithdrawInput = parent.find(".qty-to-withdraw-input"),
					radioBulk = ui.find('.bagbulk[value="bulk"]'),
					radioBag = ui.find('.bagbulk[value="bag"]'),
					addBatch = parent.find('[modal-target="add-batch"]');
				
				addBatch.hide();
				radioBulk.attr("name", "bagbulk_"+prodID);
				radioBag.attr("name", "bagbulk_"+prodID);

				uiThis.hide();
				btnCancelThis.show();
				btnSaveChangesThis.show();

				uiLoadingMethod.hide();
				uiRadioContainer.show();

				selectUomContainer.show();
				uiUOM.hide();

				uiQtyLabel.hide();
				qtyWithdrawContainer.show();
				qtyWithdrawInput.val(parentData.qty_to_withdraw);
				qtyWithdrawInput.number(true, 2);

				var oUOMfiltered = {};
				for(var i in oUnitOfMeasures){
					if(oUnitOfMeasures[i]["name"] == 'kg' || oUnitOfMeasures[i]["name"] == 'mt' || oUnitOfMeasures[i]["name"] == 'L'){
						if(parentData.unit_of_measure_id == oUnitOfMeasures[i]["id"]){
							oUnitOfMeasures[i]["selected"] = true;
						}
						oUOMfiltered[Object.keys(oUOMfiltered).length] = oUnitOfMeasures[i];
					}
				}

				_transformDD(oUOMfiltered, selectUnit);

				if(parentData.loading_method == 'bulk'){
					radioBulk.prop("checked", true);
				}
				if(parentData.loading_method == 'bag'){
					radioBag.prop("checked", true);
				}

				radioBag.off('click').on('click', function(){
					if($(this).is(":checked")){
						var parent = $(this).closest('.item-template'),
							id = parent.attr("prod-id"),
							select = parent.find(".select");
						select.addClass('disabled');
					}
				});

				radioBulk.off('click').on('click', function(){
					if($(this).is(":checked")){
						var parent = $(this).closest('.item-template'),
							id = parent.attr("prod-id"),
							selectUnitMeasure = parent.find(".item-unit-of-measure"),
							select = parent.find(".select");
						select.removeClass('disabled');
					}
				});
			});

			btnCancel.off("click").on("click", function(){
				var uiThis = $(this),
					parent = uiThis.closest(".item-template"),
					parentData = parent.data(),
					btnEditThis = parent.find(".stock-edit"),
					btnCancelThis = parent.find(".stock-cancel"),
					btnSaveChangesThis = parent.find(".stock-edit-save-changes");
					prodID = parent.attr("prod-id"),
					uiLoadingMethod = parent.find(".loading-method"),
					uiUOM = parent.find(".unit-of-measure-label"),
					uiQtyLabel = parent.find(".qty-to-withdraw-label"),
					uiRadioContainer = parent.find(".loading-method-radios"),
					selectUomContainer = parent.find(".select-uom"),
					selectUnit = parent.find(".unit-of-measure"),
					qtyWithdrawContainer = parent.find(".qty-to-withdraw-input-container"),
					qtyWithdrawInput = parent.find(".qty-to-withdraw-input"),
					addBatch = parent.find('[modal-target="add-batch"]');
				
				addBatch.show();
				
				uiThis.hide();
				btnSaveChangesThis.hide();
				btnEditThis.show();

				uiLoadingMethod.show();
				uiRadioContainer.hide();

				selectUomContainer.hide();
				uiUOM.show();

				uiQtyLabel.show();
				qtyWithdrawContainer.hide();
			
			});

			btnSaveChanges.off("click").on("click", function(){
				var uiThis = $(this),
					parent = uiThis.closest(".item-template"),
					parentData = parent.data(),
					btnCancelThis = parent.find(".stock-cancel"),
					btnSaveChangesThis = parent.find(".stock-edit-save-changes");
					prodID = parent.attr("prod-id"),
					uiLoadingMethod = parent.find(".loading-method"),
					uiUOM = parent.find(".unit-of-measure-label"),
					uiQtyLabel = parent.find(".qty-to-withdraw-label"),
					uiRadioContainer = parent.find(".loading-method-radios"),
					selectUomContainer = parent.find(".select-uom"),
					selectUnit = parent.find(".unit-of-measure"),
					qtyWithdrawContainer = parent.find(".qty-to-withdraw-input-container"),
					qtyWithdrawInput = parent.find(".qty-to-withdraw-input"),
					radioBulk = parent.find('.bagbulk[value="bulk"]'),
					radioBag = parent.find('.bagbulk[value="bag"]'),
					radioBulkBag = parent.find(".bagbulk:checked"),
					recordItemId = parentData.record_item_id,
					addBatch = parent.find('[modal-target="add-batch"]');
				
				addBatch.show();

				if(qtyWithdrawInput.val().trim().length == 0){
					$("body").feedback({title : "Message", message : "Quantity is empty", type : "danger", icon : failIcon});
				}else{
					var recordID = localStorage.getItem("withdrawalLogID");
					var oBag = _getUomDetailsByName("bag");
					var oData = {
						"withdrawal_log_id" : recordID,
						"record_item_id" : recordItemId,
						"unit_of_measure_id" : selectUnit.val(),
						"loading_method" : radioBulkBag.val(),
						"quantity" : qtyWithdrawInput.val()
					};

					if(oData["loading_method"]  == "bag"){
						oData["unit_of_measure_id"] = oBag.id;
					}

					var oOptions = {
						url : BASEURL+"withdrawals/update_item",
						type : "POST",
						data : oData,
						success: function(oResponse)
						{
							if(oResponse.status){
								$("body").feedback({title : "Message", message : "Success updating", type : "success" });
								parent.data({
									"unit_of_measure_id" : oData["unit_of_measure_id"],
									"qty_to_withdraw" : oData["quantity"],
									"qty_to_withdraw_computable" : oData["quantity"],
									"loading_method" : oData["loading_method"],
									"unit_of_measure" : _getUomDetails(oData["unit_of_measure_id"])
								});

								var sLoadingMethod = "By Bulk";
								if(oData["unit_of_measure_id"] == oBag.id){
									sLoadingMethod = "By Bag";
								}
								uiLoadingMethod.html(sLoadingMethod);

								var oSelectedUOM = _getUomDetails(oData["unit_of_measure_id"]);
								uiUOM.html(oSelectedUOM.name);

								uiQtyLabel.html($.number(oData.quantity, 2));

								btnCancelThis.click();
							}else{
								$("body").feedback({title : "Message", message : "Error updating", type : "danger", icon : failIcon});
							}
						}
					}
					cr8v_platform.CconnectionDetector.ajax(oOptions);
				}

			});
		}
	}

	function _getBatchFromProduct(prodID, callBack){
		var oOptions = {
			type : "POST",
			data : {  "product_id" : prodID },
			url : BASEURL + "withdrawals/get_batches_from_product",
			returnType : "json",
			beforeSend: function(){},
			success : function(oResponse){
				if(oResponse.status){
					if(typeof callBack == 'function'){
						callBack(oResponse.data);
					}
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _withdrawalAddBatchLoader(uiModal){
		var modalContent = uiModal.find(".modal-content");
		var loader = $("<div class='loader'></div>");
		var title = $("<h2>Fetching data...</h2>");
		loader.css({
			"position" : "absolute",
			"top" : 0,
			"left" : 0,
			"background-color" : "rgb(243, 243, 243)",
			"opacity" : 1,
			"width" : "100%",
			"height" : "100%"
		});
		title.css({
			"position" : "absolute",
			"top" : "45%",
			"left" : "45%"
		});
		loader.append(title);
		modalContent.append(loader);
		modalContent.css("position", "relative");
	}

	function _displayLocations(oBatch, parentData, inputQty, uiParentBatch){
		var uiModal = $('[modal-id="add-batch"]'),
			uiLocation = uiModal.find(".location-container"),
			uiQtyContainer = uiModal.find(".qty-container"),
			inputVal = uiModal.find(".withdraw-qty"),
			uiAmountToWithdraw = uiModal.find(".amount-to-withdraw"),
			uiWithdrawalBal = uiModal.find(".withdrawal-balance"),
			p_start = '<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">',
			p_end = '</p>',
			i = '<i class="display-inline-mid font-14 padding-right-10">&gt;</i>',
			keyupTimeout = {};

		uiModal.data("batch", oBatch);

		var oLocation = oBatch.location_hierarchy,
			iQty = parseFloat(oBatch.quantity),
			uom_id = oBatch.unit_of_measure_id,
			oUOMdata = _getUomDetails(uom_id),
			oUOMdataParent = _getUomDetails(parentData.unit_of_measure.id),
			sLocationOutput = '';
		for(var x in oLocation){
			sLocationOutput += p_start + " "+oLocation[x]["name"]+" " + p_end;
			if(Object.keys(oLocation).length > parseInt(x) + 1){
				sLocationOutput += i;
			}
			
		}

		var sUnitFrom = oUOMdata["name"],
			sUnitTo = oUOMdataParent["name"],
			iTotalConvert = UnitConverter.convert(sUnitFrom, sUnitTo, iQty);

		uiQtyContainer.html($.number(iTotalConvert, 2) + " "+sUnitTo + " <div class='margin-top-5'></div> ("+ $.number(iQty, 2) +" "+ oUOMdata.name+")");

		uiAmountToWithdraw.html( $.number(parentData.qty_to_withdraw, 2) + " "+ sUnitTo);

		uiWithdrawalBal.html($.number(0, 2) +" "+sUnitTo);

		
		inputVal.off("keyup.batch").on("keyup.batch", function(){
			var iThisVal = $(this).val(),
				iThis = parseFloat(iThisVal);
			if(iTotalConvert < iThis){
				$(this).val(iTotalConvert);
			}
			

			clearTimeout(keyupTimeout);

			keyupTimeout = setTimeout(function(){
				var input = inputVal.val(),
					inpVal = parseFloat(input),
					qtyToWithdraw = parseFloat(parentData.qty_to_withdraw_computable),
					total = qtyToWithdraw - inpVal;
				
				var uiBath = $('[batch-id="'+oBatch.id+'"]');
				if(uiBath.length > 0){
					$('[modal-id="add-batch"]').find(".confirm").attr("disabled", true);

					var iTotalQty = 0;
					$.each(uiBath, function(){
						iTotalQty += parseFloat($(this).data("qty"));
					});

					var iTotalLocationQty = iTotalConvert - iTotalQty;

					if(inputQty){
						iTotalLocationQty += inputQty;
					}

					uiQtyContainer.html($.number( iTotalLocationQty , 2) + " "+sUnitTo + " <div class='margin-top-5'></div> ("+ $.number(iQty, 2) +" "+ oUOMdata.name+")");
					total = ( iTotalConvert - iTotalQty   );
					
					var diff = ( iTotalConvert - iTotalQty   ),
						diffInput = diff - inpVal,
						totalWithdraw = qtyToWithdraw - diff;
					
					if(iTotalLocationQty == 0){
						inputVal.val(0);
						uiWithdrawalBal.html($.number((totalWithdraw - inpVal), 2) +" "+sUnitTo);
					}else{
						 
						if(total <= 0 && !inputQty){
							inputVal.val(diff);
							uiWithdrawalBal.html($.number(totalWithdraw, 2) +" "+sUnitTo);
						}else{
							if( parseFloat(inputVal.val()) > iTotalLocationQty){
								inputVal.val(0);
							}else{
								totalWithdraw = qtyToWithdraw - inpVal;
								uiWithdrawalBal.html($.number(totalWithdraw, 2) +" "+sUnitTo);
							}	
						}
					}
					

				}else{
					if(total < 0){
						inputVal.val(qtyToWithdraw);
						uiWithdrawalBal.html($.number(0, 2) +" "+sUnitTo);
					}else{
						if(total == 0){
							inputVal.val(0);	
						}
						uiWithdrawalBal.html($.number(total, 2) +" "+sUnitTo);	
					}
				}

				$('[modal-id="add-batch"]').find(".confirm").attr("disabled", false);
			}, 300);
		});

		inputVal.number(true, 2);

		inputVal.trigger("keyup.batch");

		if(inputQty){
			inputVal.val(inputQty);
		}else{
			inputVal.val(0);
		}

		uiLocation.data("location", oLocation);
		uiLocation.html(sLocationOutput);
	}

	function _editProductBatch(qty, prodID, oLocation, uiParentBatch){
		var uiModal = $('[modal-id="add-batch"]'),
			batchData = uiModal.data("batch"),
			p_start = '<p class="font-14 font-400 display-inline-mid padding-right-10">',
			p_end = '</p>',
			i_arrow = '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>',
			uiItem = $('[prod-id="'+prodID+'"]'),
			itemData = uiItem.data(),
			batchLocation = uiItem.find(".transfer-prod-location"),
			uiTemplate = batchLocation.find(".product-batch-item.template"),
			uiNotYetAssigned = batchLocation.find(".not-yet-assigned"),
			sName = batchData.name,
			sLocationOutput = "";
		
		uiParentBatch.remove();
		
		if(uiNotYetAssigned.is(":visible")){
			uiNotYetAssigned.hide();
		}

		for(var i in oLocation){

			sLocationOutput += p_start;
			sLocationOutput += oLocation[i]["name"];
			sLocationOutput += p_end;

			if(Object.keys(oLocation).length == (parseInt(i) + 1)){
				
			}else{
				sLocationOutput += i_arrow;
			}
		}

		var clone = uiTemplate.clone(),
			removeBatch = clone.find('[modal-target="remove-batch"]'),
			editBatch = clone.find('[modal-target="edit-batch"]'),
			batchName = clone.find(".batch-name"),
			location = clone.find(".location"),
			quantity = clone.find(".quantity");
		clone.removeClass('template');
		
		clone.data({
			"location" : oLocation,
			"batch" : batchData,
			"qty" : qty
		});
		
		clone.attr("batch-id", batchData.id);
		clone.off("mouseenter");
		clone.off("mouseover");
		clone.hover(function(){
			var hoverContainer = $(this).find(".btn-hover-storage");
			clone.clearQueue();
			hoverContainer.removeAttr("style");
			hoverContainer.show();
		}, function(){
			var hoverContainer = $(this).find(".btn-hover-storage");
			clone.clearQueue();
			hoverContainer.removeAttr("style");
			hoverContainer.hide();
		});

		batchName.html(sName);
		location.html(sLocationOutput);
		quantity.html($.number(qty, 2) + " " +itemData.unit_of_measure.name);
		quantity.data("qty", qty);
		batchLocation.append(clone);
		
		uiItem.data("qty_to_withdraw_computable", ( parseFloat(itemData.qty_to_withdraw_computable) - qty) );

		var prevSiblings = clone.prev(".product-batch-item:not(.template)");
		if(prevSiblings.length > 0){
			var uiWhite = prevSiblings.find(".show-ongoing-content.bggray-white"),
				uiGray = prevSiblings.find(".show-ongoing-content.bg-light-gray");
			if(uiWhite.length > 0){
				var cloneBG = clone.find(".show-ongoing-content");
				cloneBG.removeClass('bggray-white');
				cloneBG.addClass('bg-light-gray');
			}
			if(uiGray.length > 0){
				var cloneBG = clone.find(".show-ongoing-content");
				cloneBG.removeClass('bg-light-gray');
				cloneBG.addClass('bbggray-white');
			}
		}
		
		removeBatch.data("return-qty", qty);
		removeBatch.off("click").on("click", function(){
			var uiParentBatch = $(this).closest('.product-batch-item'),
				uiRemoveModal = $('[modal-id="remove-batch"]'),
				thisReturnQty = $(this).data("return-qty"),
				confirmRemove = uiRemoveModal.find(".confirm"),
				message = uiRemoveModal.find(".message"),
				qtyComputable = parseFloat(itemData.qty_to_withdraw_computable),
				removeCloseMe = uiRemoveModal.find(".close-me");
	
			message.html("Are you sure you want to remove Batch <i style='color:#D33737;'>'"+sName+"'</i>");

			uiRemoveModal.addClass("showed");

			removeCloseMe.off("click").on("click", function(){
				uiRemoveModal.removeClass('showed');
			});

			confirmRemove.off("click").on("click", function(){
				uiItem.data("qty_to_withdraw_computable", (qtyComputable + thisReturnQty) );
				uiParentBatch.remove();
				uiRemoveModal.removeClass('showed');
			});
		});

		editBatch.data("qty", qty);
		editBatch.off("click").on("click", function(e){
			var qty = parseFloat($(this).data("qty")),
				uiBatchList = $(this).closest(".product-batch-item"),
				oBatch = uiBatchList.data("batch"),
				parenQty = parseFloat(uiItem.data("qty_to_withdraw_computable"));
			uiItem.data("qty_to_withdraw_computable", (parenQty + qty) );
			_withdrawalEditBatchEvent(e, qty, oBatch);
		});
	}

	function _addProductBatch(qty, prodID, oLocation, oBatchData, oItemData, editable){
		var uiModal = $('[modal-id="add-batch"]');
		
		var batchData = (oBatchData) ? oBatchData : uiModal.data("batch");

		var	p_start = '<p class="font-14 font-400 display-inline-mid padding-right-10">',
			p_end = '</p>',
			i_arrow = '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>',
			uiItem = $('[prod-id="'+prodID+'"]'),
			itemInterval = {};

		itemInterval = setInterval(function(){
			if(uiItem.length > 0){
				
				var itemData = (oItemData) ? oItemData : uiItem.data();

				var	batchLocation = uiItem.find(".transfer-prod-location"),
					uiTemplate = batchLocation.find(".product-batch-item.template"),
					uiNotYetAssigned = batchLocation.find(".not-yet-assigned"),
					sName = batchData.name,
					sLocationOutput = "";

				if(uiNotYetAssigned.is(":visible")){
					uiNotYetAssigned.hide();
				}

				for(var i in oLocation){

					sLocationOutput += p_start;
					sLocationOutput += oLocation[i]["name"];
					sLocationOutput += p_end;

					if(Object.keys(oLocation).length == (parseInt(i) + 1)){

					}else{
						sLocationOutput += i_arrow;
					}
				}

				var clone = uiTemplate.clone(),
					removeBatch = clone.find('[modal-target="remove-batch"]'),
					editBatch = clone.find('[modal-target="edit-batch"]'),
					batchName = clone.find(".batch-name"),
					location = clone.find(".location"),
					quantity = clone.find(".quantity");
				clone.removeClass('template');

				clone.data({
					"location" : oLocation,
					"batch" : batchData,
					"qty" : qty
				});

				clone.attr("batch-id", batchData.id);

				clone.off("mouseenter");
				clone.off("mouseover");
				clone.hover(function(){
					var hoverContainer = $(this).find(".btn-hover-storage");
					clone.clearQueue();
					hoverContainer.removeAttr("style");
					hoverContainer.show();
				}, function(){
					var hoverContainer = $(this).find(".btn-hover-storage");
					clone.clearQueue();
					hoverContainer.removeAttr("style");
					hoverContainer.hide();
				});

				batchName.html(sName);
				location.html(sLocationOutput);
				quantity.html($.number(qty, 2) + " " +itemData.unit_of_measure.name);
				quantity.data("qty", qty);
				batchLocation.append(clone);

				uiItem.data("qty_to_withdraw_computable", ( parseFloat(itemData.qty_to_withdraw_computable) - qty) );

				var prevSiblings = clone.prev(".product-batch-item:not(.template)");
				if(prevSiblings.length > 0){
					var uiWhite = prevSiblings.find(".show-ongoing-content.bggray-white"),
						uiGray = prevSiblings.find(".show-ongoing-content.bg-light-gray");
					if(uiWhite.length > 0){
						var cloneBG = clone.find(".show-ongoing-content");
						cloneBG.removeClass('bggray-white');
						cloneBG.addClass('bg-light-gray');
					}
					if(uiGray.length > 0){
						var cloneBG = clone.find(".show-ongoing-content");
						cloneBG.removeClass('bg-light-gray');
						cloneBG.addClass('bbggray-white');
					}
				}

				if(editable){
					removeBatch.data("return-qty", qty);
					removeBatch.off("click").on("click", function(){
						var uiParentBatch = $(this).closest('.product-batch-item'),
							uiRemoveModal = $('[modal-id="remove-batch"]'),
							thisReturnQty = $(this).data("return-qty"),
							confirmRemove = uiRemoveModal.find(".confirm"),
							message = uiRemoveModal.find(".message"),
							qtyComputable = parseFloat(itemData.qty_to_withdraw_computable),
							removeCloseMe = uiRemoveModal.find(".close-me");

						message.html("Are you sure you want to remove Batch <i style='color:#D33737;'>'"+sName+"'</i>");

						uiRemoveModal.addClass("showed");

						removeCloseMe.off("click").on("click", function(){
							uiRemoveModal.removeClass('showed');
						});

						confirmRemove.off("click").on("click", function(){
							uiItem.data("qty_to_withdraw_computable", (qtyComputable + thisReturnQty) );
							uiParentBatch.remove();
							uiRemoveModal.removeClass('showed');
						});
					});

					editBatch.data("qty", qty);
					editBatch.off("click").on("click", function(e){
						var qty = parseFloat($(this).data("qty")),
							uiBatchList = $(this).closest(".product-batch-item"),
							oBatch = uiBatchList.data("batch"),
							parenQty = parseFloat(uiItem.data("qty_to_withdraw_computable"));
						uiItem.data("qty_to_withdraw_computable", (parenQty + qty) );
						_withdrawalEditBatchEvent(e, qty, oBatch);
					});	
				}else{
					clone.find(".btn-hover-storage").remove();
				}

				clearInterval(itemInterval);
			}else{
				uiItem = $('[prod-id="'+prodID+'"]');
			}
		}, 300);

		

	}

	function _confirmAddBatch(e){
		var uiModal = $('[modal-id="add-batch"]'),
			nameSku = uiModal.find(".sku-name"),
			data = nameSku.data(),
			prodID = data.id,
			uiLocation = uiModal.find(".location-container"),
			oLocation = uiLocation.data("location"),
			withdrawalQty = uiModal.find(".withdraw-qty"),
			floatQty = parseFloat(withdrawalQty.val());
		
		if(floatQty > 0){
			_addProductBatch(floatQty, prodID, oLocation, undefined, undefined, true);
			uiModal.removeClass("showed");
		}else{
			$("body").feedback({title : "Message", message : "Please add quantity", type : "danger", icon : failIcon});
		}
		
	}

	function _confirmEditBatch(e, uiParentBatch){
		var uiModal = $('[modal-id="add-batch"]'),
			nameSku = uiModal.find(".sku-name"),
			data = nameSku.data(),
			prodID = data.id,
			uiLocation = uiModal.find(".location-container"),
			oLocation = uiLocation.data("location"),
			withdrawalQty = uiModal.find(".withdraw-qty"),
			floatQty = parseFloat(withdrawalQty.val());
		
		if(floatQty > 0){
			_editProductBatch(floatQty, prodID, oLocation, uiParentBatch);
			uiModal.removeClass("showed");
		}else{
			$("body").feedback({title : "Message", message : "Please add quantity", type : "danger", icon : failIcon});
		}
	}

	function _countBatchTotalQty(productData){
		var id = productData.id,
			iResponse = 0,
			uiItem = $('[prod-id="'+id+'"]'),
			batches = uiItem.find(".product-batch-item:not(.template)");
		
		$.each(batches, function(){
			var uiQty = $(this).find(".quantity"),
				qty = uiQty.data("qty"),
				iQty = parseFloat(qty);
			
			iResponse += iQty;
		});

		return iResponse;
	}

	function _withdrawalEditBatchEvent(e, inputVal, oBatch){
		var uiModal = $('[modal-id="add-batch"]'),
			uiParentBatch = $(e.target).closest('.product-batch-item'),
			modalContent = uiModal.find(".modal-content"),
			closeBtn = uiModal.find(".close-me"),
			confirmBtn = uiModal.find(".confirm"),
			parent = $(e.target).closest(".item-template"),
			nameSku = uiModal.find(".sku-name"),
			parentData = parent.data(),
			qty_to_withdraw_computable = parseFloat(parent.data("qty_to_withdraw_computable")),
			oFetchedBatches = {};

		nameSku.html("SKU No."+parentData.sku+" - "+ capitalizeFirstLetter(parentData.name));
		nameSku.data(parentData);
		
		/*
		var iTotalBatch = _countBatchTotalQty(parentData),
			qty_to_withdraw_computable = parseFloat(parent.data("qty_to_withdraw_computable")),
			batchTotal = qty_to_withdraw_computable - iTotalBatch;
		
		if(batchTotal <= 0){
			batchTotal = 0;
		}

		parent.data("qty_to_withdraw_computable", batchTotal);
		*/
		_withdrawalAddBatchLoader(uiModal);

		uiModal.addClass('showed');

		_getBatchFromProduct(parentData.id, function(oBatches){
			oFetchedBatches = oBatches;

			_transformDD(oBatches, $("#selectBatch"));

			$("#selectBatch").off("change").on("change", function(){
				var val = $(this).val();
				for(var i in oBatches){
					if(oBatches[i]["id"] == val){
						var storageData = oBatches[i]["storage_data"];
						_transformDD([storageData], $("#selectLocation"));
						_displayLocations(oBatches[i], parentData, inputVal, uiParentBatch);
					}
				}
			});

			setTimeout(function(){
				$('[data-value="'+oBatch.id+'"]').click();
				modalContent.find(".loader").remove();
			},500);
		});
		

		confirmBtn.off("click").on("click", function(e){
			_confirmEditBatch(e, uiParentBatch);
		});

		closeBtn.off("click").on("click", function(){
			parent.data("qty_to_withdraw_computable", (qty_to_withdraw_computable - inputVal));
			uiModal.removeClass('showed');
		});
	}

	function _withdrawalAddBatchEvent(e){
		var uiModal = $('[modal-id="add-batch"]'),
			modalContent = uiModal.find(".modal-content"),
			closeBtn = uiModal.find(".close-me"),
			confirmBtn = uiModal.find(".confirm"),
			parent = $(e.target).closest(".item-template"),
			nameSku = uiModal.find(".sku-name"),
			parentData = parent.data(),
			qty_to_withdraw_computable = parseFloat(parent.data("qty_to_withdraw_computable")),
			oFetchedBatches = {};

		nameSku.html("SKU No."+parentData.sku+" - "+ capitalizeFirstLetter(parentData.name));
		nameSku.data(parentData);
		
		/*
		var iTotalBatch = _countBatchTotalQty(parentData),
			qty_to_withdraw_computable = parseFloat(parent.data("qty_to_withdraw_computable")),
			batchTotal = qty_to_withdraw_computable - iTotalBatch;
		
		if(batchTotal <= 0){
			batchTotal = 0;
		}

		parent.data("qty_to_withdraw_computable", batchTotal);
		*/
		_withdrawalAddBatchLoader(uiModal);

		uiModal.addClass('showed');

		_getBatchFromProduct(parentData.id, function(oBatches){
			oFetchedBatches = oBatches;

			_transformDD(oBatches, $("#selectBatch"));

			$("#selectBatch").off("change").on("change", function(){
				var val = $(this).val();
				for(var i in oBatches){
					if(oBatches[i]["id"] == val){
						var storageData = oBatches[i]["storage_data"];
						_transformDD([storageData], $("#selectLocation"));
						_displayLocations(oBatches[i], parentData);
					}
				}
			});

			setTimeout(function(){
				$("#selectBatch").trigger("change");
				modalContent.find(".loader").remove();
			},500);
		});
		

		confirmBtn.off("click").on("click", _confirmAddBatch);

		closeBtn.off("click").on("click", function(){
			parent.data("qty_to_withdraw_computable", qty_to_withdraw_computable);
			uiModal.removeClass('showed');
		});
	}

	function _displayAddedItems(bViewFirst, bShowEditButton, oStatus){
		var itemContainer = $("#itemContainer"),
			uiTemplate = itemContainer.find('.item-template.template');

		for(var i in oAddedItems){
			var clone = uiTemplate.clone(),
				uiSameItem = $('.item-template[prod-id="'+oAddedItems[i]["id"]+'"]');

			if(uiSameItem.length == 0){
				clone.removeClass('template');
				clone.attr("prod-id", oAddedItems[i]["id"]);

				oAddedItems[i]["qty_to_withdraw_computable"] = oAddedItems[i]["qty_to_withdraw"];

				clone.data(oAddedItems[i]);

				_addedItemDetails(clone, oAddedItems[i], bViewFirst);
				_addedItemEvents(clone, bShowEditButton);
			
				if(oStatus !== undefined && oStatus.description == 'save loading details'){
					var withdrawBatchContainer = clone.find(".withdraw-batch"),
						btnWithdrawBatch = withdrawBatchContainer.find('[modal-target="add-batch"]');
					withdrawBatchContainer.show();
					btnWithdrawBatch.off("click").on("click", _withdrawalAddBatchEvent);
				}

				if(oStatus !== undefined && (oStatus.description != "save loading details" && oStatus.description != "proceed to loading"))
				{
					clone.find(".stock-edit").remove();
					$(".edit-consignee-data").remove();
				}

				if(oStatus !== undefined && oStatus.description == "save loading details"){
					$(".edit-consignee-data").remove();
				}
				
				for(var ib in oAddedItems[i]["batches"]){
					var batch = oAddedItems[i]["batches"][ib];
					_addProductBatch(batch.qty, oAddedItems[i]["id"], batch.location_hierarchy, batch.batch_data, oAddedItems[i], false);	
				}


				itemContainer.append(clone);

				_fixAccordions();
			}
		}
	}

	function _removeExistingItemEvent(e){
		var uiThis = $(this),
			uiParent = uiThis.closest('.item-template'),
			parentData = uiParent.data(),
			uiModal = $('[modal-id="remove-existing-item"]'),
			closeMe = uiModal.find(".close-me"),
			confirm = uiModal.find(".confirm");

		closeMe.off("click").on("click", function(){
			uiModal.removeClass("showed");
		});

		uiModal.addClass("showed");

		confirm.off("click").on("click", function(){
			oRemovedItems[Object.keys(oRemovedItems).length] = parentData;
			uiParent.remove();

			var oSelectItems = _getItemsNotAdded();

			_transformDD(oSelectItems, $("#selectItem"));

			uiModal.removeClass("showed");

			console.log(oRemovedItems);
			
		});
	}

	function _displayAddedEditExistingItems(oItems){
		var itemContainer = $("#existingItemContainer"),
			uiTemplate = itemContainer.find('.item-template.template')

		for(var i in oItems){
			var clone = uiTemplate.clone(),
				image = clone.find(".item-image"),
				skuName = clone.find(".item-sku-name"),
				remove = clone.find(".remove-item"),
				uiSameItem = $('.item-template[exist-prod-id="'+oItems[i]["id"]+'"]');

			if(uiSameItem.length == 0){
				clone.removeClass('template');
				clone.attr("exist-prod-id", oItems[i]["id"]);

				image.attr("src", oItems[i]["image"]);
				skuName.html("SKU # "+oItems[i]["sku"]+" - "+oItems[i]["name"]);

				clone.data(oItems[i]);

				remove.off("click").on("click", _removeExistingItemEvent);

				itemContainer.append(clone);

				_fixAccordions();
			}
		}
	}

	function _getItemDetails(id){
		for(var i in oAllItems){
			if(oAllItems[i]["id"] == id){
				return oAllItems[i];
			}
		}

		return false;
	}

	function _getItemsNotAdded(){
		var response = {},
			allAddedID = [],
			allAddedItems = $('.item-template[prod-id]'),
			allExistAddedItems = $('.item-template[exist-prod-id]');
		$.each(allAddedItems, function(){
			var id = $(this).attr("prod-id");
			allAddedID.push(id);
		});

		$.each(allExistAddedItems, function(){
			var id = $(this).attr("exist-prod-id");
			allAddedID.push(id);
		});

		for(var i in oAllItems){
			var id = oAllItems[i]["id"];
			if(allAddedID.indexOf(id) == -1){
				response[Object.keys(response).length] = oAllItems[i];
			}
		}

		return response;
	}

	function _addItemEvent()
	{
		var btn = $("#btnAddItem"),
			uiSelect = $("#selectItem");
		btn.off("click").on("click", function(){
			var val = uiSelect.val(),
				customDropDown = uiSelect.siblings('.frm-custom-dropdown'),
				optionDropDown = customDropDown.find('.frm-custom-dropdown-option'),
				optionSelected = optionDropDown.find(".option[data-value='"+val+"']"),
				optionSelectedFromSelect = uiSelect.find("option:selected"),
				inputField = customDropDown.find(".dd-txt"),
				lenAddedItems = Object.keys(oAddedItems).length;

			if(val !== null && val.trim().length > 0){
				oAddedItems[lenAddedItems] = _getItemDetails(val);

				_displayAddedItems();

				var items = _getItemsNotAdded();
				_transformDD(items, uiSelect);
			}

		});
	}

	function _displayDocuments(oDocs, oRecord)
	{
		var bChangeBg = false,
			status = (oRecord) ? oRecord.status : '';
		var uiContainer = $("#displayDocuments"),
			sTemplate = '<div class="table-content position-rel tbl-dark-color">'+
							'<div class="content-show padding-all-10">'+
								'<div class="width-85per display-inline-mid padding-left-10">'+
									'<i class="fa font-30 display-inline-mid width-50px icon"></i>'+
									'<p class=" display-inline-mid doc-name">Document Name</p>'+
								'</div>'+
								'<p class=" display-inline-mid date-time"></p>'+
							'</div>'+
							'<div class="content-hide" style="height: 50px;">'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30 view-document">View</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid download-document">'+
									'<button class="btn general-btn">Download</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30">Print</button>'+
								'</a>'+
							'</div>'+
						'</div>';

		uiContainer.html("");
		for(var i in oDocs){
			var uiList = $(sTemplate),
				ext = (oDocs[i]["document_path"].substr(oDocs[i]["document_path"].lastIndexOf('.') + 1)).toLowerCase();

			if(ext == 'csv'){
				uiList.find(".icon").addClass('fa-file-excel-o');
			}else if(ext == 'pdf'){
				uiList.find(".icon").addClass('fa-file-pdf-o');
			}

			uiList.data(oDocs[i]);
			uiList.find(".doc-name").html(oDocs[i]["document_name"]);
			uiList.find(".date-time").html(oDocs[i]["date_added_formatted"]);
			if(bChangeBg){
				uiList.removeClass('tbl-dark-color');
				bChangeBg = false;
			}else{
				bChangeBg = true;
			}

			uiContainer.append(uiList);
		}

		_downloadDocument(oDocs);
	}

	function _downloadDocument(oDocs)
	{
		
		var	btnDownloadDocument = $(".download-document"),
			btnViewDocument = $(".view-document");


		btnDownloadDocument.off("mouseenter.download").on("mouseenter.download", function(e){
			e.preventDefault();
			var uiDownloadThis = $(this),
				uiParentContainer = uiDownloadThis.closest('.table-content.position-rel'),
				uiGetDataPath = uiParentContainer.data("document_path"),
				uiGetDataName = uiParentContainer.data("document_name");

				uiDownloadThis.attr("href", uiGetDataPath);
				uiDownloadThis.attr("download", uiGetDataName);

		});


		btnViewDocument.off("click.viewDocument").on("click.viewDocument", function(e){
			e.preventDefault();
			var uiParentContainer = $(this).closest('.table-content.position-rel'),
				uiGetDataPath = uiParentContainer.data("document_path");

				window.open(uiGetDataPath, '_blank');
		});
	}

	function _documentEvents(bTemporary)
	{
		var uiBtn = $("#btnUploadDocs");

		var extraData = {  };

		if(bTemporary === true){
			extraData["temporary"] = true;
		}else{
			var recordID = localStorage.getItem("withdrawalLogID");
			extraData = {
				"withdrawal_id" : recordID
			};
		}

		uiBtn.cr8vFileUpload({
			url : BASEURL + "withdrawals/upload_document",
			accept : "pdf,csv",
			filename : "attachment",
			extraField : extraData,
			success : function(oResponse){
				if(oResponse.status){
					$('[cr8v-file-upload="modal"] .close-me').click();
					var oDocs = oResponse.data[0]["documents"];
					if(bTemporary){
						oUploadedDocuments[Object.keys(oUploadedDocuments).length] = oResponse.data[0]["documents"][0];
						_displayDocuments(oUploadedDocuments);
					}else{
						var oDocsOld = _getDocuments();
						
						oDocsOld[Object.keys(oDocsOld).length] = oDocs[0];

						_displayDocuments(oDocsOld);
					}
				}
				$("body").removeAttr("style");
			},
			error : function(){
				$("body").removeAttr("style");
				$("body").feedback({title : "Message", message : "Was not able to upload, file maybe too large", type : "danger", icon : failIcon});
			}
		});
	}

	function _getDocuments(){
		var uiContainer = $("#displayDocuments"),
			uiList = uiContainer.find(".table-content"),
			oResponse = {};

		$.each(uiList, function(){
			var data = $(this).data();
			oResponse[Object.keys(oResponse).length] = data;
		});

		return oResponse;
	}

	function _getNotes(){
		var uiContainer = $("#noteContainer"),
			uiList = uiContainer.find(".note-list"),
			oResponse = {};

		$.each(uiList, function(){
			var uiThis = $(this),
				uiSubject = uiThis.find(".subject"),
				uiDateTime = uiThis.find(".date-time"),
				uiMessage = uiThis.find(".message");

			oResponse[Object.keys(oResponse).length] = {
				"subject" : uiSubject.data("subject"),
				"datetime" : uiDateTime.data("datetime"),
				"message" : uiMessage.data("message")
			};

		});

		return oResponse;
	}

	function _displayNotes(oData){
		var sHtml = '<div class="border-full padding-all-10 margin-left-18 margin-top-10 note-list">'+
				'		<div class="border-bottom-small border-gray padding-bottom-10">'+
				'			<p class="f-left font-14 font-400 subject"></p>'+
				'			<p class="f-right font-14 font-400 date-time"></p>'+
				'			<div class="clear"></div>'+
				'		</div>'+
				'		<p class="font-14 font-400 no-padding-left padding-all-10 message"> </p>'+
				'		<a href="" class="f-right padding-all-10  font-400"> </a>'+
				'		<div class="clear"></div>'+
				'	</div>';

		var uiContainer = $("#noteContainer")
		uiContainer.html("");
		for(var i in oData){
			var uiTemplate = $(sHtml),
				subj = oData[i].subject,
				message = oData[i].message;

			uiTemplate.find(".subject").html("Subject : "+ subj);
			uiTemplate.find(".subject").data("subject", subj);
			uiTemplate.find(".message").html(message);
			uiTemplate.find(".message").data("message", message);
			uiTemplate.find(".date-time").html("Date/Time : "+oData[i]["data"]["date_formatted"]);
			uiTemplate.find(".date-time").data("datetime", oData[i]["data"]["date"]);

			uiContainer.append(uiTemplate);
			uiTemplate.addClass('new-note');
		}

		
	}

	function _addNotesEvent(autoSaveRecord){
		var btnAddNotes = $("#btnAddNotes"),
			uiBtnAddNote = $("#confirmNote"),
			uiNoteSubject = $("#noteSubject"),
			uiNoteMessage = $("#noteMessage");

		btnAddNotes.off("click").on("click", function(){
			var modalNotes = $('[modal-id="add-notes"]'),
				closeMe = modalNotes.find(".close-me");

			modalNotes.addClass('showed');

			closeMe.off('click').on('click', function(){
				modalNotes.removeClass("showed");
			});
		});


		uiBtnAddNote.click(function(){

			var uiContainer = $("#noteContainer"),
				subj = uiNoteSubject.val(),
				message = uiNoteMessage.val();

			if(subj.trim().length > 0 && message.trim().length && (!autoSaveRecord || autoSaveRecord == false)){
				var oOptions = {
					type : "POST",
					data : { data : "none" },
					url : BASEURL + "receiving/get_current_date_time",
					returnType : "json",
					success : function(oResult) {
						$("body").removeAttr("style");

						oAllNotes[Object.keys(oAllNotes).length] = {
							"subj" : subj, "message" : message, "data" : {
								"date_formatted" : oResult.data.date_formatted,
								"date" : oResult.data.date
							}
						}

						_displayNotes(oAllNotes);

						$('.modal-close.close-me:visible').click();
					},
					error : function(){
						$("body").removeAttr("style");
					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);
			}else if(subj.trim().length > 0 && message.trim().length && autoSaveRecord == true ){

					var recordID = localStorage.getItem("withdrawalLogID");

				var oOptions = {
					type : "POST",
					data : { 
						withdrawal_id : recordID,
						subject : subj,
						note : message
					},
					url : BASEURL + "withdrawals/save_new_notes",
					returnType : "json",
					success : function(oResult) {
						$("body").removeAttr("style");

						oAllNotes[Object.keys(oAllNotes).length] = {
							"subject" : subj, "message" : message, "data" : {
								"date_formatted" : oResult.data.date_formats["1"],
								"date" : oResult.data.date_created
							}
						}

						_displayNotes(oAllNotes);

						$('.modal-close.close-me:visible').click();
					},
					error : function(){
						$("body").removeAttr("style");
					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);
			}

		});
	}

	function _fixAccordions(){
		$(".panel-group .panel-heading").each(function(){
			var pt = $(this).find(".panel-title");
			var ps = $(this).next(".panel-collapse");
			var ph = ps.find(".panel-body").outerHeight();

			if(ps.hasClass("in")){
				$(this).find("h4").addClass("active")
			}

			pt.off("click");

			$(this).find("a").off("click").on("click",function(e){
				$("body").removeAttr("style");
				e.preventDefault();
				ps.css({height:ph});

				if(ps.hasClass("in")){
					$(this).find("h4").removeClass("active");
					$(this).find(".fa").removeClass("fa-caret-down").addClass("fa-caret-right");
					ps.removeClass("in");
				}else{
					$(this).find("h4").addClass("active");
					$(this).find(".fa").removeClass("fa-caret-right").addClass("fa-caret-down");
					ps.addClass("in");
				}

				setTimeout(function(){
					ps.removeAttr("style")
				},500);
			});
		});
	}

	function _validateCreateAtl(editMode)
	{
		var atlNum = $("#inputWithdrawalATLno"),
			select1 = $("#selectWithdrawalConsignee1"),
			select2 = $("#selectWithdrawalConsignee2"),
			cargoSwap = oSaveData["cargo_swap"],
			modeOfDelivery = oSaveData["mode_of_delivery"],
			error = 0,
			errorMessage = [];

		if(atlNum.val().trim().length == 0){
			error++;
			errorMessage.push("ATL number is required");
		}

		if(modeOfDelivery == 'truck'){
			var inputTrucking = $("#inputTrucking"),
				inputTruckingPlateNo = $("#inputTruckingPlateNo"),
				inputTruckingDriverName = $("#inputTruckingDriverName"),
				inputTruckingLicenseNo = $("#inputTruckingLicenseNo");

			if(inputTrucking.val().trim().length == 0){
				error++;
				errorMessage.push("Trucking is required");
			}
			if(inputTruckingPlateNo.val().trim().length == 0){
				error++;
				errorMessage.push("Trucking plate no is required");
			}
			if(inputTruckingDriverName.val().trim().length == 0){
				error++;
				errorMessage.push("Trucking driver name is required");
			}
			if(inputTruckingLicenseNo.val().trim().length == 0){
				error++;
				errorMessage.push("Trucking license is required");
			}

		}else{
			var inputVesselDestination = $("#inputVesselDestination"),
				inputVesselCaptain = $("#inputVesselCaptain");

			if(inputVesselDestination.val().trim().length == 0){
				error++;
				errorMessage.push("Vessel destination is required");
			}

			if(inputVesselCaptain.val().trim().length == 0){
				error++;
				errorMessage.push("Vessel captain is required");
			}
		}
		
		if(!editMode){
			var uiAddedProducts = $("[prod-id]");

			if(uiAddedProducts.length == 0){
				error++;
				errorMessage.push("Add product");
			}else{
				$.each(uiAddedProducts, function(){
					var uiThis = $(this),
						data = uiThis.data(),
						qtyToWithdraw = uiThis.find(".qty-to-withdraw");

					if(qtyToWithdraw.val().trim().length == 0){
						error++;
						errorMessage.push("Add quantity to withdraw in "+data.name);
					}
				});
			}
		}else{
			var uiExistingProducts = $("[exist-prod-id]");
			var uiAddedProducts = $("[prod-id]");
			if(uiExistingProducts.length == 0){
				error++;
				errorMessage.push("Add product");
			}else{
				$.each(uiAddedProducts, function(){
					var uiThis = $(this),
						data = uiThis.data(),
						qtyToWithdraw = uiThis.find(".qty-to-withdraw");

					if(qtyToWithdraw.val().trim().length == 0){
						error++;
						errorMessage.push("Add quantity to withdraw in "+data.name);
					}
				});
			}
		}
		

		if(error == 0){
			return {
				status : true,
				message : errorMessage
			}
		}else{
			return {
				status : false,
				message : errorMessage
			}
		}

	}

	function _getVesselDetails(id){
		for(var i in oAllVessels){
			if(oAllVessels[i]["id"] == id){
				return oAllVessels[i];
			}
		}
		return false;
	}

	function _getConsigneeDetails(id){
		for(var i in oAllConsignees){
			if(oAllConsignees[i]["id"] == id){
				return oAllConsignees[i];
			}
		}
		return false;
	}

	function _getAtlDetails (callBack)
	{
		var oResponse = {},
			atlNum = $("#inputWithdrawalATLno"),
			select1 = $("#selectWithdrawalConsignee1"),
			select2 = $("#selectWithdrawalConsignee2"),
			originVessel = $("#selectWithdrawalVessel"),
			cargoSwap = oSaveData["cargo_swap"],
			modeOfDelivery = oSaveData["mode_of_delivery"];

		oResponse["atl_number"] = atlNum.val();
		oSaveData["atl_number"] = atlNum.val();

		var consignee =  _getConsigneeDetails(select1.val()),
			origin_vessel = _getVesselDetails(originVessel.val());

		oResponse["consignee"] = consignee;
		oSaveData["consignee"] = consignee;

		oResponse["origin_vessel"] = origin_vessel;
		oSaveData["origin_vessel"] = origin_vessel;

		oResponse["items"] = oAddedItems;

		var sPath = window.location.href;
		if(sPath.indexOf("edit_consignee_record") > -1){
			var uiItems = $('[prod-id]'),
				uiExistingItems = $('[exist-prod-id]'),
				oNewItems = {};

			$.each(uiItems, function(){
				var data = $(this).data();

				data["qty_to_withdraw"] = parseFloat($(this).find(".qty-to-withdraw").val());
				data["qty_to_withdraw_uom"] = $(this).find(".item-unit-of-measure").val();
				oNewItems[Object.keys(oNewItems).length] = data;
			});

			$.each(uiExistingItems, function(){
				var data = $(this).data(),
					record = data.record;

				data["qty_to_withdraw"] = record.qty
				data["qty_to_withdraw_uom"] = record.unit_of_measure_id;
				oNewItems[Object.keys(oNewItems).length] = data;
			});

			oResponse["items"] = oNewItems;
		}

		if(cargoSwap){
			oSaveData["consignee_from"] = consignee;
			oSaveData["consignee_to"] = _getConsigneeDetails(select2.val());
			oResponse["consignee"] = oSaveData["consignee_to"];
		}

		var sTruckingBarging = "",
			sTruckPlateNoBargeName = "";

		if(modeOfDelivery == "truck"){
			sTruckingBarging = $("#inputTrucking").val();
			sTruckPlateNoBargeName = $("#inputTruckingPlateNo").val();
			oResponse["truck_barge_patron"] = $("#inputTruckingDriverName").val();
			oResponse["license"] = $("#inputTruckingLicenseNo").val();

			oSaveData["mode_of_delivery_details"] = {
				"trucking" : sTruckingBarging,
				"plate_no" : sTruckPlateNoBargeName,
				"driver_name" : $("#inputTruckingDriverName").val(),
				"license" : $("#inputTruckingLicenseNo").val()
			}

		}else{
			var vessel = _getVesselDetails($("#selectVesselName").val());
			sTruckingBarging = vessel.name;
			sTruckPlateNoBargeName = vessel.name;
			oResponse["truck_barge_patron"] = $("#inputVesselCaptain").val();

			oSaveData["mode_of_delivery_details"] = {
				"vessel_id" : vessel.id,
				"destination" : $("#inputVesselDestination").val(),
				"captain" : $("#inputVesselCaptain").val()
			}
		}

		oResponse["trucking_barging"] = sTruckingBarging;
		oResponse["truck_plate_no_barge_name"] = sTruckPlateNoBargeName;

		oSaveData["documents"] = _getDocuments();
		oSaveData["notes"] = _getNotes();

		if(typeof callBack == 'function'){
			callBack(oResponse);
		}else{
			return oResponse;
		}
	}

	function _setAtlDetails (oData, bShowModal, callBack)
	{
		var setAtlCallback = function(oData, dateFormatted){
			var uiModal =  $('[modal-id="create-ATL"]'),
				atlNoLabel = uiModal.find(".atl-number"),
				date = uiModal.find(".date"),
				veselName = uiModal.find(".vessel-name"),
				commodity = uiModal.find(".commodity"),
				noBagBulk = uiModal.find(".no-bags-bulk"),
				license = uiModal.find(".license"),
				trackingBarging = uiModal.find(".trucking-barging"),
				truckPlateNoBargeName = uiModal.find(".truck-plate-no-barge-name"),
				consigneeName = uiModal.find(".consignee-name"),
				loadingType = uiModal.find(".loading-type"),
				estDateTime = uiModal.find(".est-loading-date-time"),
				loadingStart = uiModal.find(".loading-start"),
				loadingEnd = uiModal.find(".loading-end"),
				truckBargePatron = uiModal.find(".trucking-barging-patron"),
				equipmentUsed = uiModal.find(".equipment-used");

			atlNoLabel.html("ATL: "+oData.atl_number);
			consigneeName.html(oData.consignee.name);
			date.html(dateFormatted);
			veselName.html(oData.origin_vessel.name);

			var commodityLabel = "<ol style='list-style-position: inside;'>",
				bagsBulkLabel = "<ol style='list-style-position: inside;'>";
			for(var i in oData.items){
				var itm = oData.items[i],
					oUom = _getUomDetails(itm.qty_to_withdraw_uom);

				commodityLabel += "<li>"+itm.name+"</li>";
				bagsBulkLabel += "<li>"+ $.number(itm.qty_to_withdraw, 2) + " "+ oUom.name +"</li>";
			}
			commodityLabel += "</ol>";
			bagsBulkLabel += "</ol>";
			commodity.html(commodityLabel);
			noBagBulk.html(bagsBulkLabel);


			trackingBarging.html(oData.trucking_barging);
			truckPlateNoBargeName.html(oData.truck_plate_no_barge_name);

			if(oData.hasOwnProperty("est_date_time")){
				estDateTime.html(oData.est_date_time);
			}

			if(oData.hasOwnProperty("loading_start")){
				loadingStart.html(oData.loading_start);
			}

			if(oData.hasOwnProperty("loading_end")){
				loadingEnd.html(oData.loading_end);
			}

			if(oData.hasOwnProperty("equipments")){
				if(Object.keys(oData.equipments).length > 0){
					var equiopLabel = "<ol style='list-style-position: inside;'>";
					for(var i in oData.equipments){
						equiopLabel += "<li>"+oData.equipments[i]["name"]+"</li>";
					}
					equiopLabel += "</ol>";
					equipmentUsed.html(equiopLabel);
				}
			}

			if(oData.hasOwnProperty("license")){
				license.html(oData.license);
			}

			if(oData.hasOwnProperty("truck_barge_patron")){
				truckBargePatron.html(oData.truck_barge_patron);
			}

			$('[modal-target="create-ATL"]').html("Create ATL");
			if(bShowModal){
				uiModal.addClass('showed');
			}

			if(typeof callBack == 'function'){
				callBack();
			}
		}
		

		var oOptions = {
			type : "POST",
			data : { x : "x" },
			url : BASEURL + "receiving/get_current_date_time",
			returnType : "json",
			success : function(oResult) {
				var dateFormatted = oResult.data.date_formatted_2;
				setAtlCallback(oData, dateFormatted);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
		
	}

	function _createAtlEvent()
	{
		var btn = $('[modal-target="create-ATL"]');

		btn.off('click').on('click', function(){
			var uiModal =  $('[modal-id="create-ATL"]'),
				closeMe = uiModal.find(".close-me"),
				confirmAtl = uiModal.find(".confirm-atl"),
				oValidResponse = _validateCreateAtl();
			if(oValidResponse.status){

				btn.html("Generating...");

				//GET AND SET ATL DETAILS
				_getAtlDetails(function(data){
					_setAtlDetails(data, true);
				}),

				closeMe.off('click').on('click', function(){
					uiModal.removeClass('showed');
				});

				confirmAtl.off('click').on('click', function(){
					_confirmATLEvent();
				});

			}else{
				var sMessage = oValidResponse.message.join(",");
				$("body").feedback({title : "Error!", message : sMessage, type : "danger", speed: "slow", icon : failIcon });
			}
		});
	}

	function _saveData(sType, beforeSendCallBack, successCallBack){

		var url = "",
			sPath = window.location.href;

		if(sType == 'new'){
			url = "withdrawals/save_consignee_record";
		}else if(sType == 'update'){
			url = "withdrawals/update_consignee_record";
		}

		if(typeof beforeSendCallBack != 'function'){
			beforeSendCallBack = function(){}
		}

		if(typeof successCallBack != 'function'){
			successCallBack = function(){}
		}

		if(sPath.indexOf("create_consignee_record") > -1){
			oSaveData["items"] = oAddedItems;
		}

		if(sPath.indexOf("edit_consignee_record") > -1){
			var uiItems = $('[prod-id]'),
				recordID = localStorage.getItem("withdrawalLogID"),
				oItemData = {};
			$.each(uiItems, function(){
				var uiThis = $(this),
					data = uiThis.data(),
					qty = uiThis.find(".qty-to-withdraw"),
					radio = uiThis.find(".bagbulk:checked"),
					select = uiThis.find(".item-unit-of-measure");

				data["loading_method"] = radio.val();
				data["qty_to_withdraw"] = qty.val();
				data["qty_to_withdraw_uom"] = select.val();
				oItemData[Object.keys(oItemData).length] = data;
			});
			oSaveData["items"] = oItemData;
			oSaveData["removed_items"] = oRemovedItems;

			oSaveData["withdrawal_log_id"] = recordID;
		}


		var oOptions = {
			type : "POST",
			data : { "save_data" : JSON.stringify(oSaveData) },
			url : BASEURL + url,
			returnType : "json",
			beforeSend: beforeSendCallBack,
			success : successCallBack
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	function _saveAtlPrintable(logID){
		var uiModal =  $('[modal-id="create-ATL"]'),
			uiPrintable = uiModal.find(".printable");
		var sHTML = uiPrintable.html();

		var oOptions = {
			type : "POST",
			data : { "html" : sHTML, "withdrawal_id" : logID },
			url : BASEURL + "withdrawals/save_atl_to_pdf",
			returnType : "json",
			success : function(){
				
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _confirmATLEvent(){
		var uiModal =  $('[modal-id="create-ATL"]'),
			uiModalConfirmATL = $('[modal-id="confirm-ATL"]'),
			printAtl = uiModalConfirmATL.find(".print-atl"),
			showRecord = uiModalConfirmATL.find(".show-record"),
			closeMe = uiModalConfirmATL.find(".close-me"),
			createdWithdrawalLogID = "";

		var uiClose = uiModal.find(".modal-close.close-me"),
			uiCancel = uiModal.find("button.close-me"),
			uiConfirmATL = uiModal.find("button.confirm-atl");


		printAtl.off("click").on("click", function(){
			var uiHide = uiModal.find(".to-hide-print"),
				uiPrintable = uiModal.find(".printable");

			uiHide.hide();
			uiPrintable.printArea();
		});

		showRecord.off("click").on("click", function(){
			localStorage.setItem("withdrawalLogID", createdWithdrawalLogID);
			window.location.href = BASEURL + "withdrawals/view_consignee_record"
		});

		var beforeSendCallBack = function(){
			uiClose.css({
				"pointer-events" : "none",
				"background-color" : "#CECECE"
			});

			uiCancel.css({
				"pointer-events" : "none",
				"background-color" : "#CECECE"
			});

			uiConfirmATL.html("Saving...");
		}

		var successCallBack = function(oResponse){
			if(oResponse.status){

				createdWithdrawalLogID = oResponse.data.withdrawal_log_id;

				uiModal.removeClass('showed');
				uiModalConfirmATL.addClass('showed');

				var sMessage = "Authority to Load document no. "+oSaveData.atl_number+" has been created";

				uiModalConfirmATL.find(".message").html(sMessage);

				uiClose.removeAttr("style");
				uiCancel.removeAttr("style");
				uiConfirmATL.html("Confirm");

				_saveAtlPrintable(createdWithdrawalLogID);

			}else{
				var sMessage = oResponse.message;
				$("body").feedback({title : "Error!", message : sMessage, type : "danger", speed: "slow", icon : failIcon });

				uiClose.removeAttr("style");
				uiCancel.removeAttr("style");
				uiConfirmATL.html("Confirm");
			}
		}

		_saveData("new", beforeSendCallBack, successCallBack);

		closeMe.off('click').on('click', function(){
			window.location.reload();
		});

	}

	function _printATLevent(){
		var btn = $(".print-atl-btn");

		btn.off("click").on("click", function(){
			var oAtlDetails = {

			};

		});
	}

	function _getSingleRecord(recordID, callBack){
		var oOptions = {
			url : BASEURL+"withdrawals/get_withdrawal_consignee_record",
			type : "POST",
			data : { "record_id" : recordID },
			beforeSend : function(){

			},
			success: function(oResponse)
			{
				if(typeof callBack == 'function'){
					callBack(oResponse.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllRecords(callBack){
		var oOptions = {
			url : BASEURL+"withdrawals/get_withdrawal_consignee_record",
			type : "POST",
			data : { "x" : "none" },
			success: function(oResponse)
			{
				if(typeof callBack == 'function'){
					callBack(oResponse.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _listAllRecords(oData){
		var uiContainer = $("#recordContainer"),
			uiTemplate = uiContainer.find(".template");

		for(var i in oData){
			var data = oData[i],
				clone = uiTemplate.clone();

			clone.removeClass("template");
			clone.addClass("record");
			clone.data(data);

			var uiHover = clone.find(".hover-tbl");
			uiHover.css("height", "100%");

			_setDetailsRecordList(clone, data);
			_setEventRecordList(clone, data);
			_setButtonsRecordList(clone, data);

			uiContainer.append(clone);
		}
	}

	function _getStatus(oRecord){
		if(oRecord.status == 1){
			return {
				"stat" : "complete"
			}
		}else{
			if(oRecord.status == 0){
				return {
					"stat" : "on going",
					"description" : "proceed to loading"
				}
			}else if(oRecord.status == 2){
				return {
					"stat" : "on going",
					"description" : "save loading details"
				}
			}else if(oRecord.status == 3){
				return {
					"stat" : "on going",
					"description" : "complete loading"
				}
			}else if(oRecord.status == 4){
				return {
					"stat" : "on going",
					"description" : "generate cdr"
				}
			}
			
		}
	}

	function _setDetailsRecordList(ui, data){
		var uiwithdrawalNumber = ui.find(".withdrawal-number"),
			uiatlNumber = ui.find(".atl-number"),
			uicdrNumber = ui.find(".cdr-number"),
			uiwithdrawalDate = ui.find(".withdrawal-date"),
			uiwithdrawalOrigin = ui.find(".withdrawal-origin"),
			uiitems = ui.find(".items"),
			uiownerImage = ui.find(".owner-image"),
			uiownerName = ui.find(".owner-name"),
			uistatus = ui.find(".status");

		uiwithdrawalNumber.html(data.withdrawal_number);
		uiatlNumber.html(data.consignee_details.atl_number);

		if(Object.keys(data.cdr_details).length > 0){
			uicdrNumber.html(data.cdr_details.cdr_no);
		}

		uiwithdrawalDate.html(data.date_created_formats["2"]);
		uiwithdrawalOrigin.html(data.consignee_details.origin_vessel.name);

		for(var i in data.products){
			if(i < 2){
				var item = data.products[i];
				uiitems.append("<li>"+item.details.name+"</li>");
			}
		}
		if(Object.keys(data.products).length > 2){
			var len = Object.keys(data.products).length,
				total = len - 2;
			uiitems.append("<li>"+total+" more items </li>");
		}

		uiownerImage.html(data.owner.image);
		uiownerName.html(data.owner.first_name+" "+data.owner.last_name);
		
		var oStat = _getStatus(data);
		uistatus.html(oStat.stat);

	}

	function _setButtonsRecordList(ui, data){
		var btnView = ui.find(".view-record"),
			btnProceedToLoading = ui.find(".proceed-to-loading"),
			btnGenerateCDR = ui.find(".generate-cdr"),
			btnCompleteLoading = ui.find(".complete-loading"),
			btnSaveLoading = ui.find(".save-loading-details"),
			oStat = _getStatus(data);

		if(oStat.stat == "complete"){
			btnProceedToLoading.remove();
			btnGenerateCDR.remove();
			btnCompleteLoading.remove();
			btnSaveLoading.remove();
		}else{
			if(oStat.description == "save loading details"){
				btnProceedToLoading.remove();
				btnGenerateCDR.remove();
				btnCompleteLoading.remove();
			}
			if(oStat.description == "proceed to loading"){
				btnGenerateCDR.remove();
				btnCompleteLoading.remove();
				btnSaveLoading.remove();
			}
			if(oStat.description == "complete loading"){
				btnProceedToLoading.remove();
				btnGenerateCDR.remove();
				btnSaveLoading.remove();
			}
			if(oStat.description == "generate cdr"){
				btnProceedToLoading.remove();
				btnCompleteLoading.remove();
				btnSaveLoading.remove();
			}
		}
	}

	function _setEventRecordList(ui, data){
		var btnView = ui.find(".view-record"),
			btnProceedToLoading = ui.find(".proceed-to-loading"),
			btnGenerateCDR = ui.find(".generate-cdr"),
			btnCompleteLoading = ui.find(".complete-loading"),
			btnSaveLoadingDetails = ui.find(".save-loading-details"),
			oStat = _getStatus(data);

		var methods = {
			"redirect" : function(){
				window.location.href = BASEURL + "withdrawals/view_consignee_record";
			},
			"setid" : function(id){
				localStorage.removeItem("withdrawalLogID");
				localStorage.setItem("withdrawalLogID", id);
			}
		}

		btnView.off("click").on("click", function(){
			var uiParent = $(this).closest(".record"), 
				oRecord = uiParent.data();

			methods.setid(oRecord.id);
			methods.redirect();
		});

		btnProceedToLoading.off("click").on("click", function(){
			var uiParent = $(this).closest(".record"), 
				oRecord = uiParent.data();

			methods.setid(oRecord.id);
			methods.redirect();
			viewRedirect();
		});

		btnGenerateCDR.off("click").on("click", function(){
			var uiParent = $(this).closest(".record"), 
				oRecord = uiParent.data();

			methods.setid(oRecord.id);
			methods.redirect();
			viewRedirect();
		});

		btnCompleteLoading.off("click").on("click", function(){
			var uiParent = $(this).closest(".record"), 
				oRecord = uiParent.data();

			methods.setid(oRecord.id);
			methods.redirect();
		});

		btnSaveLoadingDetails.off("click").on("click", function(){
			var uiParent = $(this).closest(".record"), 
				oRecord = uiParent.data();

			methods.setid(oRecord.id);
			methods.redirect();
		});

	}

	function _setRecordDetails(oRecord){
		var truckVesselContainer = $(".trucking-vessel-details-container"),
			truckVesselTemplate = truckVesselContainer.find(".template"),
			truckVesselTitle = $(".trucking-vessel-title"),
			atlNo = $(".atl-no"),
			uiConsignee = $(".consignee"),
			uiOrigin = $(".origin"),
			modeOfDelivery = $(".mode-of-delivery"),
			dateTime = $(".date-time"),
			uiStatus = $(".record-status"),
			oStatus = _getStatus(oRecord),
			sConsignee = oRecord.consignee_details.consignee.name;

		var mod = oRecord.consignee_details.mode_of_delivery;

		atlNo.html(oRecord.consignee_details.atl_number);
		uiStatus.html(oStatus.stat);
		dateTime.html(oRecord.date_created_formats["1"]);
		modeOfDelivery.html(mod);
		uiOrigin.html(oRecord.consignee_details.origin_vessel.name);
		uiConsignee.html(capitalizeFirstLetter(sConsignee));

		if(mod == "truck"){
			truckVesselTitle.html("Trucking details");

			var details = oRecord.consignee_details.truck_details,
				zebra = "dark";
			for(var i in details){
				if(i != 'id' && i != 'is_deleted' && i != 'withdrawal_log_id'){
					var clone = truckVesselTemplate.clone(),
						key = clone.find(".key"),
						value = clone.find(".value");
					clone.removeClass('template');

					var keyString = i.replace("_", " "),
						valString = details[i].replace("_", " ");

					key.html(capitalizeFirstLetter(keyString) + ":");
					value.html(capitalizeFirstLetter(valString));

					clone.removeClass('.bggray-white');
					clone.removeClass('.bg-light-gray');
					if(zebra == 'dark'){
						clone.addClass('bg-light-gray');
						zebra = 'white';	
					}else{
						clone.addClass('bggray-white');
						zebra = 'dark';
					}

					truckVesselContainer.append(clone);
				}
			}
		}else{
			truckVesselTitle.html("Vessel details");

			var details = oRecord.consignee_details.vessel_details,
				zebra = "dark";
			for(var i in details){
				if( (i != 'id' && i != 'is_deleted' && i != 'withdrawal_log_id' && i != 'vessel_id') || i == 'vessel_data'){
					var clone = truckVesselTemplate.clone(),
						key = clone.find(".key"),
						value = clone.find(".value");
					clone.removeClass('template');

					var keyString = i.replace("_", " "),
						detail = (typeof details[i] == 'string')? details[i] : '',
						valString = detail.replace("_", " ");

					if(i == 'vessel_data'){
						keyString = 'Vessel Name';
						valString = details[i]['name'];
					}

					key.html(capitalizeFirstLetter(keyString) + ":");
					value.html(capitalizeFirstLetter(valString));

					clone.removeClass('.bggray-white');
					clone.removeClass('.bg-light-gray');
					if(zebra == 'dark'){
						clone.addClass('bg-light-gray');
						zebra = 'white';	
					}else{
						clone.addClass('bggray-white');
						zebra = 'dark';
					}

					truckVesselContainer.append(clone);
				}
			}
		}

	}
	function _setCdrInformation(oRecord, oStatus){
		if(oStatus.stat == 'complete'){
			 var uiContainer = $('.consignee-details-main'),
			 	cdrNumber = uiContainer.find(".cdr-no"),
			 	tareIn = uiContainer.find(".tare-in"),
			 	tareOut = uiContainer.find(".tare-out"),
			 	gatePassCtrlNum = uiContainer.find(".gate-pass-ctrl-no"),
			 	scaleTicketNum = uiContainer.find(".scale-ticket-no"),
			 	sealNumberContainer = uiContainer.find(".seal-no"),
			 	grayColor = uiContainer.find(".gray-color"),
			 	cdr_details = oRecord.cdr_details;
			 
			 grayColor.removeClass("gray-color");
			 cdrNumber.html(cdr_details.cdr_no);
			 tareIn.html($.number(cdr_details.weight_in, 2));
			 tareOut.html($.number(cdr_details.weight_out, 2));
			 scaleTicketNum.html("");
			 gatePassCtrlNum.html(cdr_details.gate_pass_ctrl_no);
			
			var sSeals = "";
			for(var x in cdr_details.seal_numbers){
				if( (parseInt(x) + 1) == Object.keys(cdr_details.seal_numbers).length ){
					sSeals += cdr_details.seal_numbers[x]["number"];
			 	}else{
					sSeals += cdr_details.seal_numbers[x]["number"] + ", ";
			 	}
			 }
			 sealNumberContainer.html(sSeals);


		}
	}

	function _generateSingleView(oRecord){
		var oStatus = _getStatus(oRecord);

		_setRecordDetails(oRecord);

		_setCdrInformation(oRecord, oStatus);

		_displayDocuments(oRecord["documents"], oRecord);

		UnitConverter.getUnitOfMeasures(function(data){
			oUnitOfMeasures = data;
			
			var prodLen = Object.keys(oRecord.products).length;
			for(var i in  oRecord.products){
				var lenAddedItems = Object.keys(oAddedItems).length;
				oAddedItems[lenAddedItems] = oRecord.products[i]["details"];
				oAddedItems[lenAddedItems]["loading_method"] = oRecord.products[i]["loading_method"];
				oAddedItems[lenAddedItems]["unit_of_measure_id"] = oRecord.products[i]["unit_of_measure_id"];
				oAddedItems[lenAddedItems]["qty_to_withdraw"] = oRecord.products[i]["qty"];
				oAddedItems[lenAddedItems]["record_item_id"] = oRecord.products[i]["id"];
				oAddedItems[lenAddedItems]["batches"] = oRecord.products[i]["batches"];


				if( (prodLen - 1) == i){
					var bShowLabels = true,
						bEditShow = true;
					if(oStatus.description == 'save loading details'){
						bEditShow = false;
					}
					_displayAddedItems(bShowLabels, bEditShow, oStatus);
				}
			}

			_getItems(function(oItems){
				_displaySelectItems(oItems);
				setTimeout(function(){
					var items = _getItemsNotAdded();
					_transformDD(items, $("#selectItem"));


				},1000);
			});
			
			
			setTimeout(function(){
				$('[modal-id="generate-view"]').removeClass('showed');
			},1000);
		});

		var toDisplayNotes = {};
		for(var i in oRecord.notes){
			toDisplayNotes[Object.keys(toDisplayNotes).length] = {
				"subject" : oRecord.notes[i]["subject"],
				"message" : oRecord.notes[i]["note"],
				"data" : {
					"date_formatted" : oRecord.notes[i]["date_formats"][1],
					"date" : oRecord.notes[i]["date_created"]
				}
			}
		}
		_displayNotes(toDisplayNotes);
		oAllNotes = toDisplayNotes;

		_documentEvents(); 

		_addNotesEvent(true);

		_setLoadingInformation(oRecord, oStatus);
	}

	function _setLoadingInformation(oRecord, oStatus){
		var selectLoadingArea = $("#selectLoadingArea"),
			loadingInfoDisplay = $(".loading-information-display"),
			loadingInfoEdit = $(".loading-information-edit"),
			loadingInterval = {};
		
		if(oStatus.description == 'proceed to loading'){
			loadingInfoDisplay.show();
			loadingInfoEdit.hide();
		}
		if(oStatus.description == 'save loading details'){
			loadingInfoDisplay.hide();
			loadingInfoEdit.show();
		}
		if(oStatus.description == 'complete loading'){
			loadingInfoDisplay.show();
			loadingInfoEdit.hide();
			var editBtn = loadingInfoDisplay.find(".vessel-edit-loading-info"),
				saveBtn = loadingInfoDisplay.find(".loading-info-save"),
				cancelBtn = loadingInfoDisplay.find(".vessel-cancel-loading-info"),
				loadingDateStart = loadingInfoDisplay.find(".loading-date-time-start"),
				loadingDateEnd = loadingInfoDisplay.find(".loading-date-time-end"),
				loadingWorkingList = loadingInfoDisplay.find(".worker-list-display"),
				loadingEquipment = loadingInfoDisplay.find(".equipment-display"),
				loadingStartDate = $("#loadingStartDate"),
				loadingStartTime = $("#loadingStartTime"),
				loadingHandlingMethod = $("#loadingHandlingMethod"),
				loadingHandlingMethodDisplay = loadingInfoDisplay.find(".handling-method-display"),
				loadingAreaDisplay = loadingInfoDisplay.find(".loading-area-display"),
				recordLoadingInfo = oRecord.loading_information,
				loadingInfoLoadingArea = $("#loadingInfoLoadingArea"),
				loadingAreaInterval = {};
			
			if(recordLoadingInfo.datetime_start !== null){
				
			}else{
				var prev = loadingDateStart.prev();
				prev.html("Est Loading Date/Time Start");
				loadingStartDate.val(recordLoadingInfo.est_date_time_formats[6]);
				loadingStartTime.val(recordLoadingInfo.est_date_time_formats[5]);
				loadingDateStart.html(recordLoadingInfo.est_date_time_formats[3]);
			}

			loadingHandlingMethodDisplay.html(recordLoadingInfo.handling_method);

			loadingAreaDisplay.html(recordLoadingInfo.loading_area.name);

			loadingAreaInterval = setInterval(function(){
				if(Object.keys(oAllLoadingArea).length > 0){
					
					var toLoadingArea = {};
					for(var i in oAllLoadingArea){
						if( oAllLoadingArea[i]["id"] == oRecord.loading_information.loading_area_id ){
							oAllLoadingArea[i]["selected"] = true;
							toLoadingArea[Object.keys(toLoadingArea).length] = oAllLoadingArea[i];
						}else{
							toLoadingArea[Object.keys(toLoadingArea).length] = oAllLoadingArea[i];
						}
					}
					_transformDD(toLoadingArea, loadingInfoLoadingArea);
					clearInterval(loadingAreaInterval);
				}
			},500);

			editBtn.show();
			
			var loadingLabel = loadingInfoDisplay.find('.loading-label'),
				loadingArea = loadingInfoDisplay.find('.loading-area'),
				loadingWithWidth = loadingInfoDisplay.find('.with-width'),
				loadingNotYetAssigned = loadingInfoDisplay.find('.not-yet-assigned'),
				loadingAssignedValueWithWith = loadingInfoDisplay.find('.assigned-value.with-width'),
				loadingAssignedValue = loadingInfoDisplay.find('.assigned-value'),
				loadingHandling = loadingInfoDisplay.find('.handling'),
				loadingCancel = loadingInfoDisplay.find('.vessel-cancel-loading-info'),
				loadingVesselEdit = loadingInfoDisplay.find('.vessel-edit'),
				loadingVesselEditLink = loadingInfoDisplay.find('.vessel-edit-link'),
				btnCompleteLoading = $('.complete-loading'),
				loadingPadding = loadingInfoDisplay.find('.padding'),
				loadingInfoGray = loadingInfoDisplay.find('.loading-information-gray');


			editBtn.off("click").on("click", function(){

				loadingLabel.removeClass('width-25percent');
				loadingLabel.css({'width':'18%'});
				loadingArea.removeClass('width-23percent');
				loadingArea.css({'width':'19%'});
				loadingWithWidth.removeClass('width-27percent');
				loadingWithWidth.css({'width':'30%'});
				loadingNotYetAssigned.hide();
				loadingAssignedValueWithWith.fadeIn();
				loadingAssignedValue.fadeIn();
				loadingHandling.addClass('padding-left-40');
				loadingCancel.show();
				loadingWithWidth.css({'display':'inline-block'});
				loadingWithWidth.removeClass('position-abs');
				loadingVesselEdit.attr('disabled',true);
				loadingVesselEditLink.attr('disabled',true);
				btnCompleteLoading.attr('disabled',true);
				loadingPadding.removeClass('padding-all-10');
				loadingPadding.css({'padding':'5px 5px 5px 10px'});
				loadingVesselEdit.attr('disabled',true);

				loadingInfoGray.removeClass('gray-color');
				loadingInfoGray.addClass('black-color');

				saveBtn.show();
				editBtn.hide();
			});

			cancelBtn.off("click").on("click", function(){
				loadingLabel.removeClass('width-18percent');
				loadingLabel.css({'width':'25%'});
				loadingWithWidth.removeClass('width-30percent');
				loadingWithWidth.css({'width':'27%'});
				loadingArea.removeClass('width-19percent');
				loadingArea.css({'width':'23%'});
				loadingNotYetAssigned.fadeIn();
				loadingAssignedValueWithWith.hide();
				loadingAssignedValue.hide();
				loadingHandling.removeClass('padding-left-40');
				loadingVesselEditLink.attr('disabled',false);
				btnCompleteLoading.attr('disabled',false);
				loadingPadding.css({'padding':'10px'});

				loadingInfoGray.removeClass('black-color');
				loadingInfoGray.addClass('gray-color');

				cancelBtn.hide();
				editBtn.attr('disabled',false);
				saveBtn.hide();
				editBtn.show();
			});

			saveBtn.off("click").on("click", function(){
				var recordID = localStorage.getItem("withdrawalLogID"),
					estDatePicker = loadingStartDate.data("DateTimePicker"),
					estTimePicker = loadingStartTime.data("DateTimePicker"),
					estDateGet = estDatePicker.getDate(loadingStartDate.data("date")),
					estTimeGet = estTimePicker.getDate(),
					estDateVal = estDateGet.format("YYYY-MM-DD"),
					estTimeVal = estTimeGet.format("HH:mm:ss");
				var oOptions = {
					url : BASEURL+"withdrawals/update_loading_information",
					type : "POST",
					data : {
						"withdrawal_id" : recordID,
						"est_date" : estDateVal,
						"est_time" : estTimeVal,
						"handling_method" : loadingHandlingMethod.val(),
						"loading_area_id" : loadingInfoLoadingArea.val()
					},
					beforeSend : function(){
						saveBtn.html("Updating...");
					},
					success: function(oResponse)
					{
						if(oResponse.status){
							saveBtn.html("Success");
							window.location.reload();
						}else{
							saveBtn.html("Save");
							$("body").feedback({title : "Message", message : "Unable to update", type : "danger", icon : failIcon});
						}
					}
				}
				cr8v_platform.CconnectionDetector.ajax(oOptions);
			});
		}

		if(oStatus.description == 'generate cdr' || (oStatus.stat == 'complete')){
			loadingInfoDisplay.show();
			loadingInfoEdit.hide();
			var editBtn = loadingInfoDisplay.find(".vessel-edit-loading-info"),
				saveBtn = loadingInfoDisplay.find(".loading-info-save"),
				cancelBtn = loadingInfoDisplay.find(".vessel-cancel-loading-info"),
				loadingDateStart = loadingInfoDisplay.find(".loading-date-time-start"),
				loadingDateEnd = loadingInfoDisplay.find(".loading-date-time-end"),
				loadingWorkingList = loadingInfoDisplay.find(".worker-list-display"),
				loadingEquipment = loadingInfoDisplay.find(".equipment-display"),
				loadingStartDate = $("#loadingStartDate"),
				loadingStartTime = $("#loadingStartTime"),
				loadingHandlingMethod = $("#loadingHandlingMethod"),
				loadingHandlingMethodDisplay = loadingInfoDisplay.find(".handling-method-display"),
				loadingAreaDisplay = loadingInfoDisplay.find(".loading-area-display"),
				loadingDuration = loadingInfoDisplay.find(".duration-display"),
				recordLoadingInfo = oRecord.loading_information,
				loadingInfoLoadingArea = $("#loadingInfoLoadingArea"),
				loadingInfoGray = loadingInfoDisplay.find(".gray-color"),
				loadingAreaInterval = {};
			
			loadingInfoGray.removeClass("gray-color");

			if(recordLoadingInfo.datetime_start !== null){
				var momentEnd = moment(recordLoadingInfo.datetime_end_formats[3]),
					momentStart = moment(recordLoadingInfo.datetime_start_formats[3]);
				var arrEnd = {
					"year" : momentEnd.format("YYYY"),
					"month" : momentEnd.format("MM"),
					"day" : momentEnd.format("DD"),
					"hour" : momentEnd.format("HH"),
					"minute" : momentEnd.format("mm"),
					"second" : momentEnd.format("ss")
				};

				var arrStart = {
					"year" : momentStart.format("YYYY"),
					"month" : momentStart.format("MM"),
					"day" : momentStart.format("DD"),
					"hour" : momentStart.format("HH"),
					"minute" : momentStart.format("mm"),
					"second" : momentStart.format("ss")
				};

				var startMoment = moment([arrStart["year"], arrStart["month"], arrStart["day"], arrStart["hour"], arrStart["minute"], arrStart["second"] ]),
					endMoment = moment([arrEnd["year"], arrEnd["month"], arrEnd["day"], arrEnd["hour"], arrEnd["minute"], arrEnd["second"] ]);
				
				var diffDays = endMoment.diff(startMoment, "days"),
					sDuration = "";
				
				if(diffDays <= 30 && diffDays >= 1){
					if(diffDays == 1){
						sDuration = diffDays + " day";
					}else{
						sDuration = diffDays + " days";
					}
				}else if(diffDays > 30){
					var diffMonth = endMoment.diff(startMoment, "month");
					if(diffMonth <= 12){
						if(diffMonth == 1){
							sDuration = diffMonth + " month";
						}else{
							sDuration = diffMonth + " months";
						}
					}
				}

				loadingDuration.html( sDuration );
				loadingDateStart.html(recordLoadingInfo.datetime_start_formats[3]);
				loadingDateEnd.html(recordLoadingInfo.datetime_end_formats[3]);
			}else{
				var prev = loadingDateStart.prev();
				prev.html("Est Loading Date/Time Start");
				loadingStartDate.val(recordLoadingInfo.est_date_time_formats[6]);
				loadingStartTime.val(recordLoadingInfo.est_date_time_formats[5]);
				loadingDateStart.html(recordLoadingInfo.est_date_time_formats[3]);
			}

			loadingHandlingMethodDisplay.html(recordLoadingInfo.handling_method);

			loadingAreaDisplay.html(recordLoadingInfo.loading_area.name);

			if(Object.keys(oRecord.equipments).length > 0){
				loadingWorkingList.find(".no-workers-logged").remove();
			}
			
			var bgStrip = 'white';
			for(var x in oRecord.workers){
				var worker = oRecord.workers[x];
					cloneWorker = loadingWorkingList.find(".template").clone();
				cloneWorker.removeClass("template");
				cloneWorker.find(".name").html(capitalizeFirstLetter(worker.name));
				cloneWorker.find(".designation").html(capitalizeFirstLetter(worker.worker_type_name));
				if(bgStrip == 'white'){
					cloneWorker.addClass('bg-light-gray');
					bgStrip = 'gray';
				}else{
					bgStrip = 'white';
				}
				cloneWorker.appendTo(loadingWorkingList);
			}

			if(Object.keys(oRecord.equipments).length > 0){
				loadingEquipment.find(".no-equipment-used").remove();
			}
			
			bgStrip = 'white';
			for(var x in oRecord.equipments){
				var equipment = oRecord.equipments[x];
					cloneEquip = loadingEquipment.find(".template").clone();
				cloneEquip.removeClass("template");
				cloneEquip.find(".name").html(capitalizeFirstLetter(equipment.name));
				if(bgStrip == 'white'){
					cloneEquip.addClass('bg-light-gray');
					bgStrip = 'gray';
				}else{
					bgStrip = 'white';
				}
				cloneEquip.appendTo(loadingEquipment);
			}


			editBtn.hide();
		}

		loadingInterval = setInterval(function(){
			if(Object.keys(oAllLoadingArea).length > 0){
				_transformDD(oAllLoadingArea, selectLoadingArea);
				clearInterval(loadingInterval);
			}
		},500);
		
	}

	function _generateSingleViewForEdit(oRecord){
		var oStatus = _getStatus(oRecord),
			consigneeDetails = oRecord.consignee_details;

		var inputWithdrawalATLno = $("#inputWithdrawalATLno"),
			selectWithdrawalConsignee1 = $("#selectWithdrawalConsignee1"),
			selectWithdrawalConsignee2 = $("#selectWithdrawalConsignee2"),
			selectWithdrawalVessel = $("#selectWithdrawalVessel"),
			selectModeDelivery = $("#selectModeDelivery"),
			selectVesselName = $("#selectVesselName"),
			inputVesselDestination = $("#inputVesselDestination"),
			inputVesselCaptain = $("#inputVesselCaptain"),
			inputTrucking = $("#inputTrucking"),
			inputTruckingPlateNo = $("#inputTruckingPlateNo"),
			inputTruckingDriverName = $("#inputTruckingDriverName"),
			inputTruckingLicenseNo = $("#inputTruckingLicenseNo");



		inputWithdrawalATLno.val(consigneeDetails.atl_number);
		if(consigneeDetails.mode_of_delivery == 'truck'){
			var truck_details = consigneeDetails.truck_details;
			inputTrucking.val(truck_details.trucking);
			inputTruckingPlateNo.val(truck_details.plate_no);
			inputTruckingDriverName.val(truck_details.driver_name);
			inputTruckingLicenseNo.val(truck_details.license_no);
		}else{
			var vessel_details = consigneeDetails.vessel_details;
			inputVesselDestination.val(vessel_details.destination);
			inputVesselCaptain.val(vessel_details.captain);
		}

		UnitConverter.getUnitOfMeasures(function(data){
			oUnitOfMeasures = data;
			
			var prodLen = Object.keys(oRecord.products).length,
				oItems = {};
			for(var i in  oRecord.products){
				var iLen = Object.keys(oItems).length,
					record = oRecord.products[i];

				oItems[iLen] = oRecord.products[i]["details"];
				delete record["details"];
				oItems[iLen]["record"] = record;
			}

			_displayAddedEditExistingItems(oItems);

			_getItems(function(oItems){
				_displaySelectItems(oItems);
				setTimeout(function(){
					var items = _getItemsNotAdded();
					_transformDD(items, $("#selectItem"));
				},1000);
			});
			
			
			setTimeout(function(){
				$('[modal-id="generate-view"]').removeClass('showed');
			},1000);

			_getVessels(function(oData){
				oAllVessels = oData;

				var oDataVessel = oData,
					originVesselID = consigneeDetails.origin_vessel_id;

				for(var i in oDataVessel){
					if(oDataVessel[i]["id"] == originVesselID){
						oDataVessel[i]["selected"] = true;
					}
				}

				_transformDD(oDataVessel, $("#selectWithdrawalVessel"));

				var oDataVesselName = oData;

				if(consigneeDetails.mode_of_delivery == 'vessel'){
					var vesselDetails = consigneeDetails.vessel_details,
						vessel_id = vesselDetails.vessel_id;

					for(var x in oDataVesselName){
						if(oDataVesselName[x]["id"] == vessel_id){
							oDataVesselName[x]["selected"] = true;
						}
					}
				}

				_transformDD(oDataVesselName, $("#selectVesselName"));


			});
		});
	}

	function _completeLoadingEvent(oRecord){
		var oAtlData = {
			"atl_number" : oRecord.consignee_details.atl_number,
			"consignee" : {
				"name" : oRecord.consignee_details.consignee.name
			},
			"origin_vessel" : {
				"name" : oRecord.consignee_details.origin_vessel.name
			},
			"items" : {}
		};

		if(oRecord.consignee_details.hasOwnProperty("vessel_details")){
			oAtlData["trucking_barging"] = oRecord.consignee_details.vessel_details.vessel_data.name;
			oAtlData["truck_plate_no_barge_name"] = oRecord.consignee_details.vessel_details.vessel_data.name;
		}

		if(oRecord.consignee_details.hasOwnProperty("truck_details")){
			oAtlData["trucking_barging"] = oRecord.consignee_details.truck_details.name;
			oAtlData["truck_plate_no_barge_name"] = oRecord.consignee_details.truck_details.plate_no;
		}

		if( Object.keys(oRecord.loading_information).length > 0 ){
			oAtlData["est_date_time"] = oRecord.loading_information.est_date_time_formats[3];
		}

		for(var i in oRecord.products){
			var record = oRecord.products[i],
				details = record.details,
				itm = {};
			
			itm["name"] = details.name;
			itm["qty_to_withdraw_uom"] = record.unit_of_measure_id;
			itm["qty_to_withdraw"] = record.qty;
			oAtlData["items"][Object.keys(oAtlData["items"]).length] = itm;
		}

		var uiModal = $('[modal-id="complete-loading"]'),
			uiModalComplete = $('[modal-id="complete-loading-confirmation"]'),
			workerList = uiModal.find(".worker-list"),
			equipmentList = uiModal.find(".equipment-list"),
			addWorkerModal = $("#addWorkerModal"),
			addEquipmentModal = $("#addEquipmentModal"),
			showMeRecord = uiModalComplete.find(".show-record"),
			closeMeComplete = uiModalComplete.find(".close-me"),
			closeMe = uiModal.find(".close-me"),
			confirmComplete = uiModal.find(".complete-loading-confirmation"),
			transferDateStart = $("#transferDateStart"),
			transferTimeStart = $("#transferTimeStart"),
			transferTimeEnd = $("#transferTimeEnd"),
			transferDateEnd = $("#transferDateEnd");

		uiModal.addClass('showed');
		
		showMeRecord.off("click").on("click", function(){
			window.location.reload();
		});

		closeMe.off("click").on("click", function(){
			uiModal.removeClass('showed');
		});

		closeMeComplete.off("click").on("click", function(){
			uiModal.removeClass('showed');
		});

		var removeAction = function(e){
			var uiThis = $(e.target),
				uiListParent = uiThis.closest(".list-item");
			uiListParent.remove();
		}

		addWorkerModal.off("click").on("click", function(){
			var template = workerList.find(".template"),
				clone = template.clone(),
				removeBtn = clone.find(".remove");
			
			removeBtn.off("click").on("click", removeAction);
			clone.removeClass("template");
			workerList.append(clone);
		});

		addEquipmentModal.off("click").on("click", function(){
			var template = equipmentList.find(".template"),
				clone = template.clone(),
				removeBtn = clone.find(".remove");
			
			removeBtn.off("click").on("click", removeAction);
			clone.removeClass("template");
			equipmentList.append(clone);
		});

		confirmComplete.off("click").on("click", function(){
			var addedWorkersUI = workerList.find(".list-item:not(.template)"),
				addedEquipmentUI = equipmentList.find(".list-item:not(.template)"),
				error = 0,
				errorMessage = [],
				oWorkers = {},
				oEquipments = {};
			
			if(addedWorkersUI.length == 0){
				error++;
				errorMessage.push("Add worker");
			}else{
				$.each(addedWorkersUI, function(i, ui){
					var name = $(ui).find(".name"),
						designation = $(ui).find(".designation"),
						item = {};
					if(name.val().trim().length == 0){
						error++;
						errorMessage.push("Add worker name on item "+ (parseInt(i) + 1));
					}else{
						item["name"] = name.val();
					}
					if(designation.val().trim().length == 0){
						error++;
						errorMessage.push("Add worker designation on item "+ (parseInt(i) + 1));
					}else{
						item["designation"] = designation.val();
					}

					oWorkers[Object.keys(oWorkers).length] = item;
				});
			}
			if(addedEquipmentUI.length == 0){
				error++;
				errorMessage.push("Add equipment");
			}else{
				$.each(addedEquipmentUI, function(i, ui){
					var name = $(ui).find(".equipment-name"),
						item = {};
					if(name.val().trim().length == 0){
						error++;
						errorMessage.push("Add equipment on item "+ (parseInt(i) + 1));
						
					}else{
						item["equipment"] = name.val();
					}

					oEquipments[Object.keys(oEquipments).length] = item;
				});
			}

			if(transferDateStart.val().trim().length == 0){
				error++;
				errorMessage.push("Add transfer start date");
			}
			if(transferTimeStart.val().trim().length == 0){
				error++;
				errorMessage.push("Add transfer start time");
			}

			if(transferDateEnd.val().trim().length == 0){
				error++;
				errorMessage.push("Add transfer end date");
			}
			if(transferTimeEnd.val().trim().length == 0){
				error++;
				errorMessage.push("Add transfer end time");
			}

			var dateTimePickerStartDate = transferDateStart.data("DateTimePicker"),
				dateTimePickerStartTime = transferTimeStart.data("DateTimePicker"),
				dateStart = dateTimePickerStartDate.getDate(transferDateStart.data("date")),
				timeStart = dateTimePickerStartTime.getDate(),
				dateStartVal = dateStart.format("YYYY-MM-DD"),
				timeStartVal = timeStart.format("HH:mm:SS"),

				dateTimePickerEndDate = transferDateEnd.data("DateTimePicker"),
				dateTimePickerEndTime = transferTimeEnd.data("DateTimePicker"),
				dateEnd = dateTimePickerEndDate.getDate(transferDateEnd.data("date")),
				timeEnd = dateTimePickerEndTime.getDate(),
				dateEndVal = dateEnd.format("YYYY-MM-DD"),
				timeEndVal = timeEnd.format("HH:mm:SS"),

				startDateTime = dateStartVal+" "+timeStartVal,
				endDateTime = dateEndVal+" "+timeEndVal,
				startUnix = moment(startDateTime).unix(),
				endUnix = moment(endDateTime).unix(),
				durationTimeStamp = 0,
				timeStampTotal = 0;

			timeStampTotal = endUnix - startUnix;
			if(timeStampTotal < 0){
				error++;
				errorMessage.push("Start date should be less than end date");
			}else{
				durationTimeStamp = timeStampTotal;
			}

			if(error > 0){
				$("body").feedback({title : "Message", message : errorMessage.join(", "), speed: "slow", type : "danger", icon : failIcon});
			}else{
				var recordID = localStorage.getItem("withdrawalLogID");
				
				oAtlData["loading_start"] = (dateStart.format("MMM DD, YYYY")) + " " +timeStart.format("hh:mm A");
				oAtlData["loading_end"] = (dateEnd.format("MMM DD, YYYY")) + " " +timeEnd.format("hh:mm A");
				
				oAtlData["equipments"] = {};
				for(var iq in oEquipments){
					oAtlData["equipments"][Object.keys(oAtlData["equipments"]).length] = {
						"name" :  oEquipments[iq]["equipment"]
					};
				}

				_setAtlDetails(oAtlData, false, function(){
					var uiModalGettingData = $('[modal-id="getting-data-record"]'),
						uiMessageGetting = uiModalGettingData.find(".message"),
						uiModalCreateATL =  $('[modal-id="create-ATL"]'),
						uiPrintable = uiModalCreateATL.find(".printable");

					var sHTML = uiPrintable.html();

					var oOptions1 = {
						type : "POST",
						data : { "html" : sHTML, "withdrawal_id" : recordID },
						url : BASEURL + "withdrawals/update_atl_pdf",
						returnType : "json",
						beforeSend: function(){
							uiModal.removeClass("showed");
							uiModalGettingData.addClass('showed');
						},
						success : function(oResponse){
							if(oResponse.status){
								var oOptions2 = {
									url : BASEURL+"withdrawals/complete_loading",
									type : "POST",
									data : {
										"withdrawal_id" : recordID,
										"workers" : JSON.stringify(oWorkers),
										"equipments" : JSON.stringify(oEquipments),
										"date_start" : dateStartVal,
										"time_start" : timeStartVal,
										"date_end" : dateEndVal,
										"time_end" : timeEndVal,
										"duration" : durationTimeStamp
									},
									beforeSend : function(){
										uiMessageGetting.html("Updating..");
									},
									success: function(oResponse)
									{
										if(oResponse.status){
											uiMessageGetting.html("Success, reloading...");
											window.location.reload();
										}else{
											uiMessageGetting.removeClass("showed");
											$("body").feedback({title : "Message", message : "Unable to update", type : "danger", icon : failIcon});
										}
									}
								}
								cr8v_platform.CconnectionDetector.ajax(oOptions2);
							}else{
								uiMessageGetting.removeClass("showed");
								$("body").feedback({title : "Message", message : "Unable to update", type : "danger", icon : failIcon});
							}
						}
					}

					cr8v_platform.CconnectionDetector.ajax(oOptions1);
					
				});
				
				
			}
			
		});
	}

	function _confirmCdrEvent(oSaveCdrData){
		var uiCdrModal = $('[modal-id="generate-cdr"]'),
			uiGetting = $('[modal-id="getting-data-record"]'),
			uiModalGenerated = $('[modal-id="confirm-generatecdr"]'),
			closeGenerated = uiModalGenerated.find(".close-me"),
			showRecordGenerated = uiModalGenerated.find(".show-record"),
			printGenerated = uiModalGenerated.find(".print-cdr"),
			uiPrintable = uiCdrModal.find(".printable"),
			sHtml = uiPrintable.html(),
			recordID = localStorage.getItem("withdrawalLogID");
		
		showRecordGenerated.off("click").on("click", function(){
			window.location.reload();
		});

		printGenerated.off("click").on("click", function(){
			uiPrintable.printArea();
		});

		var oOptions1 = {
			type : "POST",
			data : { "html" : sHtml, "withdrawal_id" : recordID },
			url : BASEURL + "withdrawals/create_cdr_pdf",
			returnType : "json",
			beforeSend: function(){
				uiCdrModal.removeClass("showed");
				uiGetting.addClass('showed');
			},
			success : function(oResponse){
				if(oResponse.status){
					var oOptions2 = {
						url : BASEURL+"withdrawals/complete_cdr",
						type : "POST",
						data : {
							"withdrawal_id" : recordID,
							"save_data" : JSON.stringify(oSaveCdrData)
						},
						beforeSend : function(){
							uiGetting.find(".message").html("Updating..");
						},
						success: function(oResponse)
						{
							if(oResponse.status){
								uiGetting.removeClass("showed");
								uiModalGenerated.addClass("showed");
							}else{
								uiGetting.removeClass("showed");
								$("body").feedback({title : "Message", message : "Unable to update", type : "danger", icon : failIcon});
							}
						}
					}
					cr8v_platform.CconnectionDetector.ajax(oOptions2);
				}else{
					uiGetting.removeClass("showed");
					$("body").feedback({title : "Message", message : "Unable to update", type : "danger", icon : failIcon});
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions1);
	}

	function _generateCDRevent(oRecord){
		var buttonHolder = $("#buttonHolder"),
			btnGenerateCDRmain = buttonHolder.find('.generate-cdr-main'),
			btnGenerateCDRconfirm = buttonHolder.find('.generate-cdr-confirm'),
			btnCancel = buttonHolder.find('.cdr-cancel');

		var uiConsigneeDetailsMain = $('.consignee-details-main'),
			uiConsigneeDetailsClone = $('.consignee-details-clone'),
			uiCdrContainer = $('.cdr-container'),
			cdrNumber = $("#cdrNumber"),
			cdrCargoSwap = $("#cdrCargoSwap"),
			cdrSelectConsignee = $("#cdrSelectConsignee"),
			cdrGatePassCrtlno = $("#cdrGatePassCrtlno"),
			cdrWeightIn = $("#cdrWeightIn"),
			cdrWeightOut = $("#cdrWeightOut"),
			sealContainer = $("#sealContainer"),
			cdrAddSeal = $("#cdrAddSeal"),
			uiLoadinInfoContainer = $('.loading-information-display'),
			itemContainer = $("#itemContainer"),
			uiModalGenerating = $('[modal-id="generate-view"]');
		
		cdrWeightIn.number(true, 2);
		cdrWeightOut.number(true, 2);
		
		cdrCargoSwap.off("click").on("click", function(){
			var bVal = $(this).is(":checked"),
				selectContainer = cdrSelectConsignee.closest('.select');
			if(bVal){
				selectContainer.removeClass("disabled");
			}else{
				selectContainer.addClass("disabled");
			}
		});

		var modalShowHide = function(){
			uiModalGenerating.addClass('showed');
			setTimeout(function(){
				uiModalGenerating.removeClass('showed');
			},1000);
		}

		modalShowHide();

		uiConsigneeDetailsMain.hide();
		uiLoadinInfoContainer.hide();
		itemContainer.hide();
		uiConsigneeDetailsClone.show();
		uiCdrContainer.show();

		btnGenerateCDRmain.hide();
		btnGenerateCDRconfirm.show();
		btnCancel.show();
		
		cdrAddSeal.off('click').on('click', function(){
			var clone = sealContainer.find(".seal-template").clone();
			clone.removeClass("seal-template");
			clone.find('p').html("");
			sealContainer.append(clone);
		});

		btnGenerateCDRconfirm.off('click').on('click', function(){
			var error = 0,
				errorMessage = [],
				uiSeals = sealContainer.find(".seal");
			
			if(cdrNumber.val().trim().length == 0){
				error++;
				errorMessage.push("Add CDR Number");
			}
			if(cdrGatePassCrtlno.val().trim().length == 0){
				error++;
				errorMessage.push("Add Gate Pass Control Number");
			}
			if(cdrWeightIn.val().trim().length == 0){
				error++;
				errorMessage.push("Add Weight In");
			}
			if(cdrWeightOut.val().trim().length == 0){
				error++;
				errorMessage.push("Add Weight Out");
			}
			
			var sealsLength = uiSeals.length,
				totalSealError = 0,
				sealErrorMessage = [],
				arrSealNumbers = [];

			$.each(uiSeals, function(i, ui){
				var thisInput = $(ui).find("input[type='text']"),
					thisVal = thisInput.val();
				if(thisVal.trim().length == 0){
					totalSealError++;
					error++;
					sealErrorMessage.push("Add seal number on item "+(parseInt(i) + 1));
				}else{
					arrSealNumbers.push(thisVal);
				}
			});

			if(totalSealError > 0){
				errorMessage.push("Add seal number at least 1");
			}

			if(error > 0){
				$("body").feedback({title : "Message", message : errorMessage.join(", "), type : "danger", icon : failIcon});
			}else{
				var recordID = localStorage.getItem("withdrawalLogID"),
					bCargoSwap = cdrCargoSwap.is(":checked"),
					
					oSaveCdrData = {
						"cdr_no" : cdrNumber.val(),
						"gate_pass_ctrl_no" : cdrGatePassCrtlno.val(),
						"cargo_swap" : bCargoSwap,
						"consignee_id" : cdrSelectConsignee.val(),
						"weight_in" : cdrWeightIn.val(),
						"weight_out" : cdrWeightOut.val(),
						"seals" : arrSealNumbers
					},

					oOptions = {
						type : "POST",
						data : { data : "none" },
						url : BASEURL + "receiving/get_current_date_time",
						returnType : "json",
						success : function(oResult) {
							var dateFormatted = oResult.data.date_formatted;
							var oDataForCDR = {
								"withdrawal_id" : recordID,
								"cdr_number" : cdrNumber.val(),
								"consignee_name" : oRecord.consignee_details.consignee.name,
								"date" : dateFormatted,
								"time_out" : " ",
								"vessel" : oRecord.consignee_details.origin_vessel.name,
								"storage_area" : " ",
								"loading_type" : oRecord.loading_information.loading_area.name,
								"loading_date" : oRecord.loading_information.datetime_start_formats[3],
								"gate_pass_ctrl_no" : cdrGatePassCrtlno.val(),
								"cargo_swap" : bCargoSwap,
								"consignee_id" : cdrSelectConsignee.val(),
								"vessel_name" : (oRecord.consignee_details.hasOwnProperty("vessel_details")) ? oRecord.consignee_details.vessel_details.vessel_data.name : "",
								"captain" : (oRecord.consignee_details.hasOwnProperty("vessel_details")) ? oRecord.consignee_details.vessel_details.captain : "",
								"issued_by" : " "
							};

							var iCdrWeightIn = parseFloat(cdrWeightIn.val()),
								iCdrWeightOut = parseFloat(cdrWeightOut.val()),
								uiCdrModal = $('[modal-id="generate-cdr"]'),
								uiCloseCdr = uiCdrModal.find(".close-me"),
								uiConfirmCdr = uiCdrModal.find(".confirm");

							var gross = iCdrWeightIn + iCdrWeightOut;
							var net = iCdrWeightIn - iCdrWeightOut;
							oDataForCDR["tare_in"] = iCdrWeightIn;
							oDataForCDR["gross"] = gross;
							oDataForCDR["net"] = net;

							var oCommodities = {};
							for(var i in oRecord.products){
								var item = {
									"name" : capitalizeFirstLetter(oRecord.products[i]["details"]["name"])
								};
								oCommodities[Object.keys(oCommodities).length] = item;
							}
							oDataForCDR["commodity"] = oCommodities;

							_setCdrDetails(oDataForCDR);

							uiCdrModal.addClass("showed");

							uiCloseCdr.off("click").on("click", function(){
								uiCdrModal.removeClass("showed");
							});

							uiConfirmCdr.off("click").on("click", function(){
								_confirmCdrEvent(oSaveCdrData);
							});
						}
					};

				cr8v_platform.CconnectionDetector.ajax(oOptions);
			}
		});

		btnCancel.off('click').on('click', function(){
			modalShowHide();

			btnGenerateCDRmain.show();
			btnGenerateCDRconfirm.hide();
			btnCancel.hide();

			uiConsigneeDetailsMain.show();
			uiLoadinInfoContainer.show();
			itemContainer.show();
			uiConsigneeDetailsClone.hide();
			uiCdrContainer.hide();
		});

		
	}

	function _setCdrDetails(oData){
		var uiModal = $('[modal-id="generate-cdr"]'),
			cdrNumber = uiModal.find('.cdr-number'),
			consigneeName = uiModal.find(".consignee-name"),
			date = uiModal.find(".date"),
			timeOut = uiModal.find(".time-out"),
			vessel = uiModal.find(".vessel"),
			commodity = uiModal.find(".commodity"),
			storageArea = uiModal.find(".storage-area"),
			loadingType = uiModal.find(".loading-type"),
			loadingDate = uiModal.find(".loading-date"),
			tareIn = uiModal.find(".tare-in"),
			gross = uiModal.find(".gross"),
			net = uiModal.find(".net"),
			vesselName = uiModal.find(".vessel-name"),
			captain = uiModal.find(".captain"),
			issuedBy = uiModal.find(".issued-by");

		if(oData.hasOwnProperty("commodity")){
			var uiOL = $("<ol style='list-style-position: inside;'></ol>");
			commodity.html(uiOL);
			for(var i in oData.commodity){
				uiOL.append("<li>"+oData.commodity[i]["name"]+"</li>");
			}
		}

		if(oData.hasOwnProperty("cdr_number")){
			cdrNumber.html("CDR: "+oData.cdr_number);
		}

		if(oData.hasOwnProperty("consignee_name")){
			consigneeName.html(oData.consignee_name);
		}

		if(oData.hasOwnProperty("date")){
			date.html(oData.date);
		}

		if(oData.hasOwnProperty("time_out")){
			timeOut.html(oData.time_out);
		}

		if(oData.hasOwnProperty("vessel")){
			vessel.html(oData.vessel);
		}

		if(oData.hasOwnProperty("storage_area")){
			storageArea.html(oData.storage_area);
		}

		if(oData.hasOwnProperty("loading_type")){
			loadingType.html(oData.loading_type);
		}

		if(oData.hasOwnProperty("loading_date")){
			loadingDate.html(oData.loading_date);
		}

		if(oData.hasOwnProperty("tare_in")){
			tareIn.html($.number(oData.tare_in, 2));
		}

		if(oData.hasOwnProperty("gross")){
			gross.html($.number(oData.gross, 2));
		}

		if(oData.hasOwnProperty("net")){
			net.html($.number(oData.net, 2));
		}

		if(oData.hasOwnProperty("vessel_name")){
			vesselName.html(oData.vessel_name);
		}

		if(oData.hasOwnProperty("captain")){
			captain.html(oData.captain);
		}

		if(oData.hasOwnProperty("issued_by")){
			issuedBy.html(oData.issued_by);
		}
		


	}

	function _setButtonsForRecordStatus(oRecord){
		var buttonHolder = $("#buttonHolder");
		var oStatusData = _getStatus(oRecord);
		var button = "";

		if(oStatusData.description == 'proceed to loading'){
			button = $('<button class="btn general-btn display-inline-mid modal-trigger"> Proceed to loading</button>');
			button.off("click").on("click", function(){
				var recordID = localStorage.getItem("withdrawalLogID");
				var oOptions = {
					url : BASEURL+"withdrawals/proceed_to_loading",
					type : "POST",
					data : {
						"withdrawal_id" : recordID
					},
					success: function(oResponse)
					{
						if(oResponse.status){
							$("body").feedback({title : "Message", message : "Success updating", type : "success" });
							window.location.reload();
						}else{
							$("body").feedback({title : "Message", message : "Unable to update", type : "danger", icon : failIcon});
						}
					}
				}
				cr8v_platform.CconnectionDetector.ajax(oOptions);
			});
		}

		if(oStatusData.description == 'save loading details'){
			var cancel = $('<button type="button" class="font-12 btn btn-default font-12 display-inline-mid red-color">Cancel</button>'),
				save = $('<button class="btn general-btn display-inline-mid modal-trigger" >Save Loading Details</button>');


			cancel.off("click").on("click", function(){
				var recordID = localStorage.getItem("withdrawalLogID");
				var oOptions = {
					url : BASEURL+"withdrawals/cancel_proceed_to_loading",
					type : "POST",
					data : {
						"withdrawal_id" : recordID
					},
					success: function(oResponse)
					{
						if(oResponse.status){
							window.location.reload();
						}else{
							$("body").feedback({title : "Message", message : "Unable to update", type : "danger", icon : failIcon});
						}
					}
				}
				cr8v_platform.CconnectionDetector.ajax(oOptions);
			});
			save.off("click").on("click", function(){
				var uiItems = $('[prod-id]'),
					loadingInfoEdit = $(".loading-information-edit"),
					estDate = $("#estDate"),
					estTime = $("#estTime"),
					selectLoadingArea = $("#selectLoadingArea"),
					handlingMethod = $("#handlingMethod"),
					error = 0,
					errorMessage = [],
					oItemRecord = {};
				$.each(uiItems, function() {
					var uiThis =  $(this),
						itemData = uiThis.data(),
						uom_id = itemData.unit_of_measure_id,
						uiBatchContainer = uiThis.find(".transfer-prod-location"),
						batches = uiBatchContainer.find(".product-batch-item:not(.template)");
					if(batches.length == 0){
						error++;
						errorMessage.push("Add batch to "+capitalizeFirstLetter(itemData.name));
					}else{
						var item = {
							"record_id" : itemData.record_item_id,
							"batches" : {}
						};
						$.each(batches, function(){
							var batchData = $(this).data(),
								location = batchData.location,
								batch = batchData.batch,
								qty = batchData.qty,
								locationLen = Object.keys(location).length,
								lastIndex = locationLen - 1,
								lastLocation = location[lastIndex];

							delete batch["location_hierarchy"];
							delete batch["storage_data"];
							item["batches"][Object.keys(item["batches"]).length] = {
								"location" : lastLocation,
								"batch" : batch,
								"uom_id" : uom_id,
								"qty" : qty
							};
						});

						oItemRecord[Object.keys(oItemRecord).length] = item;
					}
				});
				
				if(estDate.val().trim().length == 0){
					error++;
					errorMessage.push("Add est date");
				}
				if(estTime.val().trim().length == 0){
					error++;
					errorMessage.push("Add est time");
				}

				if(error == 0){
					var recordID = localStorage.getItem("withdrawalLogID"),
						estDatePicker = estDate.data("DateTimePicker"),
						estTimePicker = estTime.data("DateTimePicker"),
						estDateGet = estDatePicker.getDate(estDate.data("date")),
						estTimeGet = estTimePicker.getDate(),
						estDateVal = estDateGet.format("YYYY-MM-DD"),
						estTimeVal = estTimeGet.format("HH:mm:SS");

					var oOptions = {
						url : BASEURL+"withdrawals/save_loading_details",
						type : "POST",
						data : {
							"withdrawal_id" : recordID,
							"items" : JSON.stringify(oItemRecord),
							"est_date" : estDateVal,
							"est_time" : estTimeVal,
							"loading_area" : selectLoadingArea.val(),
							"handling_method" : handlingMethod.val()
						},
						success: function(oResponse)
						{
							if(oResponse.status){
								window.location.reload();
							}else{
								$("body").feedback({title : "Message", message : "Unable to update", type : "danger", icon : failIcon});
							}
						}
					}
					cr8v_platform.CconnectionDetector.ajax(oOptions);
				}else{
					$("body").feedback({title : "Message", message : errorMessage.join(", "), type : "danger", icon : failIcon});
				}

			});

			button = $('<div></div>');
			button.append(cancel);
			button.append(save);
		}

		if(oStatusData.description == 'complete loading'){
			var btnComplete = $('<button class="btn general-btn display-inline-mid modal-trigger complete-loading">Complete Loading</button>');
			
			btnComplete.off("click").on("click", function(){
				_completeLoadingEvent(oRecord);
			});

			button = $('<div></div>');
			button.append(btnComplete);
		}

		if(oStatusData.description == 'generate cdr'){
			var btnGenerateCDR = $('<button class="btn general-btn display-inline-mid modal-trigger generate-cdr-main">Generate CDR</button>'),
				btnGenerateCDRconfirm = $('<button class="btn general-btn display-inline-mid modal-trigger display-none generate-cdr-confirm">Generate CDR</button>'),
				btnCancelCDR = $('<button type="button" class="font-12 btn btn-default font-12 display-inline-mid display-none red-color cdr-cancel">Cancel</button>');
			
			btnGenerateCDR.off("click").on("click", function(){
				_generateCDRevent(oRecord);	
			});

			button = $('<div></div>');
			button.append(btnCancelCDR);
			button.append(btnGenerateCDRconfirm);
			button.append(btnGenerateCDR);

			_getConsignees(function(oConsignees){
				_transformDD(oConsignees, $("#cdrSelectConsignee"));
				$("#cdrSelectConsignee").closest('.select').addClass('disabled');
			});
		}

		buttonHolder.html(button);
	}

	function _saveChangesEditEvent(recordID){
		var uiBtn = $('[modal-target="save-changes"]'),
			uiModalGettingData = $('[modal-id="getting-data-record"]'),
			beforeSendCallBack = function(){
				var uiModal = $('[modal-id="updating-record"]');
				uiModal.addClass('showed');
			},
			successCallBack = function(){
				window.location.href = BASEURL + "withdrawals/view_consignee_record"
			};


		uiBtn.off("click").on("click", function(){
			var bEditMode = true,
				oValidResponse = _validateCreateAtl(bEditMode);
			if(oValidResponse.status){
				//GET AND SET ATL DETAILS
				_getAtlDetails(function(data){
					_setAtlDetails(data, false, function(){
						var uiModalCreateATL =  $('[modal-id="create-ATL"]'),
							uiPrintable = uiModalCreateATL.find(".printable");

						var sHTML = uiPrintable.html();

						var oOptions = {
							type : "POST",
							data : { "html" : sHTML, "withdrawal_id" : recordID },
							url : BASEURL + "withdrawals/update_atl_pdf",
							returnType : "json",
							beforeSend: function(){
								uiModalGettingData.addClass('showed');
							},
							success : function(oResponse){
								if(oResponse.status){
									uiModalGettingData.removeClass('showed');
									_saveData("update", beforeSendCallBack, successCallBack);
								}
							}
						}

						cr8v_platform.CconnectionDetector.ajax(oOptions);
					});
				});

			}else{
				var sMessage = oValidResponse.message.join(",");
				$("body").feedback({title : "Error!", message : sMessage, type : "danger", speed: "slow", icon : failIcon });
			}
			
		});
	}

	/* END OF PRIVATE FUNCTIONS */


	/* START OF PUBLIC FUNCTIONS */

	function createRecordEvents(){
		_getConsignees(_selectConsignees);

		_getVessels(function(oData){
			oAllVessels = oData;
			_transformDD(oData, $("#selectWithdrawalVessel"));
			_transformDD(oData, $("#selectVesselName"));
		});

		UnitConverter.getUnitOfMeasures(function(data){
			oUnitOfMeasures = data;
		});

		_selectModeOfDelivery();

		_cargoSwapEvent();

		_getItems(_displaySelectItems);

		_documentEvents(true);

		_addNotesEvent();

		_fixAccordions();

		_createAtlEvent();

		
		oSaveData["withdrawal_number"] = $(".withdrawal-number").data("withdrawalNumber");

	}

	function viewRecordEvents(){

		var recordID = localStorage.getItem("withdrawalLogID");

		_getSingleRecord(recordID, function(oData){
			
			var oRecord = oData[0];
			_generateSingleView(oRecord);

			$(".withdrawal-no").html("Withdrawal No. " + oRecord.withdrawal_number);

			_setButtonsForRecordStatus(oRecord);

		});

		_getLoadingArea(function(loadingArea){
			oAllLoadingArea = loadingArea;
		});

		$(".edit-consignee-data").off("click").on("click", function(){
			window.location.href = BASEURL + "withdrawals/edit_consignee_record"
		})

		_printATLevent();
	}

	function editRecordEvents(){
		var recordID = localStorage.getItem("withdrawalLogID"),
			oRecord = {};

		_cargoSwapEvent();

		_getSingleRecord(recordID, function(oData){
			

			oRecord = oData[0];
			var consigneeDetails = oRecord.consignee_details;

			if(consigneeDetails.cargo_swap == "1"){
				_getConsignees(function(oConsigness){
					if(Object.keys(oRecord.consignee_swap_data).length > 0){
						var consignee_id_to = oRecord.consignee_swap_data.consignee_id_to,
							consignee_id_from = oRecord.consignee_swap_data.consignee_id_from;
						_selectConsignees(oConsigness, consignee_id_from, consignee_id_to);

						$("#cargoSwap").click();

						oSaveData["cargo_swap"] = true;
					}else{
						oSaveData["cargo_swap"] = false;
					}
				});
			}else{
				oSaveData["cargo_swap"] = false;
				_getConsignees(function(oConsigness){
					var consigneeID1 = consigneeDetails.consignee_id;
					_selectConsignees(oConsigness, consigneeID1);
				});
			}

			

			//SELECT VESSEL OR TRUCKING
			$('[data-value="'+consigneeDetails.mode_of_delivery+'"]').click();

			$(".withdrawal-number").html("Withdrawal No. " + oRecord.withdrawal_number);
			$(".anchor-withdrawal-no").html("Withdrawal No. " + oRecord.withdrawal_number);
			$(".anchor-withdrawal-no").off("click").on("click", function(){
				window.location.href = BASEURL + "withdrawals/view_consignee_record";
			});

			_generateSingleViewForEdit(oRecord);

			_saveChangesEditEvent(recordID);
			
		});


		UnitConverter.getUnitOfMeasures(function(data){
			oUnitOfMeasures = data;
		});

		_selectModeOfDelivery();

	}

	function listView() {
		
		_getAllRecords(_listAllRecords);

	}


	/* END OF PUBLIC FUNCTIONS */


	/*
		Add public functions here
	*/
	return {
		createRecordEvents : createRecordEvents,
		viewRecordEvents : viewRecordEvents,
		editRecordEvents : editRecordEvents,
		listView : listView
	}
})();

$(document).ready(function($) {
	
	var sPath = window.location.href;

	if(sPath.indexOf("consignee_record_list") > -1){
		WithdrawalConsignee.listView();
	}


	if(sPath.indexOf("create_consignee_record") > -1){
		WithdrawalConsignee.createRecordEvents();

		var uiModal = $('[modal-id="create-ATL"]').detach();
		$("body").append(uiModal);

		var uiModalConfirmATL = $('[modal-id="confirm-ATL"]').detach();
		$("body").append(uiModalConfirmATL);
	}

	if(sPath.indexOf("view_consignee_record") > -1){
		$('[modal-id="generate-view"]').addClass('showed');


		WithdrawalConsignee.viewRecordEvents();

		var body = $("body"),
			uiModal = $('[modal-id="create-ATL"]').detach(),
			uiModalCDR = $('[modal-id="generate-cdr"]').detach(),
			uiModalConfirmCDR = $('[modal-id="confirm-generatecdr"]').detach();
		
		body.append(uiModal);
		body.append(uiModalCDR);
		body.append(uiModalConfirmCDR);
	}

	if(sPath.indexOf("edit_consignee_record") > -1){
		$('[modal-id="generate-view"]').addClass('showed');


		WithdrawalConsignee.editRecordEvents();

	}

});