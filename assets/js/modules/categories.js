
var CCategories = (function(){

	var arrCategoryType = {},
		arrNotif = {};

	function add(oCategoryData)
	{	
		
		var oAjaxConfig = {
			type: "POST",
			url : BASEURL+"categories/add",
			dataType: "json",
			data: {"company_id":oCategoryData.company_id, "category_name":oCategoryData.category_name, "category_type":JSON.stringify(oCategoryData.category_type)},
			success: function(response)
			{
				// 	console.log(response);
				if(window.location.href.indexOf("add_product") > -1){
					CProducts.bindSelectCategory();
					$('div[modal-id="add-new-category"] .close-me').trigger("click");
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);
	}


	function get(callBack)
	{

		oAjaxConfig = {
			type: "POST",
			url : BASEURL+"categories/get",
			data:{"company_id": iConstCompanyId},
			success: function(oResponse)
			{
				if(oResponse.status){
					arrCategoryType = oResponse.data;
					CCategories.renderElement("getCategory", arrCategoryType);
					if(typeof callBack == 'function'){
						callBack(oResponse.data);
					}
				}
				cr8v_platform.localStorage.set_local_storage({
					"name":"allCategories",
					"data":arrCategoryType
				});


			} 
		}

		cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);


	}

	function update(oDatas, oQualifiers)
	{
		var oDataAjax = {
			type: "POST",
			url : BASEURL+"categories/edit",
			dataType: "json",
			data: {"data":JSON.stringify(oDatas), "qualifiers":JSON.stringify(oQualifiers)},
			success: function(response)
			{
				console.log(response);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oDataAjax);

		
	}

	function _delete(iCategoryId)
	{
		var oDataAjax = {
			type : "POST",
			url : BASEURL+"categories/archive",
			data : {"category_id":iCategoryId},
			success: function(response)
			{
				var options = {
					  form : "box",
					  autoShow : true,
					  type : "success",
					  title  : "Delete Category",
					  message : "Successfully Deleted",
					  speed : "slow",
					  withShadow : true
					}
					
					$("body").feedback(options);
					$("#displayAllCategories").load(location.href + " #displayAllCategories", function(){

						CCategories.get();
						$(".close-me").trigger("click");
					});
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oDataAjax);
	}

	function bindEvents(sAction, ui)
	{
		if(sAction === 'editCategory')
		{
			var uiBtn = ui.find('[modal-target="edit-product-category"]');

			uiBtn.off('click.editCategory').on('click.editCategory', function(){
				// console.log(categoryId);
				$("body").css({overflow:'hidden'});
				var tm = $(this).attr("modal-target");

				$("div[modal-id~='"+tm+"']").addClass("showed");

				$("div[modal-id~='"+tm+"'] .close-me").on("click",function(){
					$("body").css({'overflow-y':'initial'});
					$("div[modal-id~='"+tm+"']").removeClass("showed");
				});

				var uiParent = $(this).closest('.table-content.position-rel');

				var categoryId = uiParent.data("categoryId"),
					categoryTypes = uiParent.data("categoryTypes"),
					categoryName = uiParent.data("categoryName");



				var oCatData = {
					"categoryId" : categoryId,
					"categoryTypes" : categoryTypes,
					"categoryName" : categoryName
				};

				cr8v_platform.localStorage.set_local_storage({
					"name": "currentEditCategory",
					"data" : oCatData
				});


				CCategories.renderElement("updatecategory",  oCatData);
				
			});

		}
		else if(sAction === 'addproduct')
		{
			// add ui's
			var iCompanyId = iConstCompanyId,
				uiCategoryName = $("#categoryName"),
				uiSolid = $("#solid"),
				uiLiquired = $("#liquid"),
				uiGas = $("#gas"),
				uiSaveCategory = $("#saveCategory"),
				arrCategoryType = {},
				oCategoryData = {
					"company_id" : iCompanyId,
					"category_name" : "",
					"category_type" : {}
				};

				$("#formCategory").cr8vformvalidation({
					'preventDefault' : true,
				    'data_validation' : 'datavalid',
				    'input_label' : 'labelinput',
				    'onValidationError': function(arrMessages) {
				        // alert(JSON.stringify(arrMessages)); //shows the errors

				       	 	var options = {
							  form : "box",
							  autoShow : true,
							  type : "danger",
							  title  : "Add Category",
							  message : JSON.stringify(arrMessages),
							  speed : "slow",
							  withShadow : true
							}
							
							$("body").feedback(options);

				        },
				    'onValidationSuccess': function() {
				      //  alert("Success"); //function if you have something to do upon submission

					       	var options = {
							  form : "box",
							  autoShow : true,
							  type : "success",
							  title  : "Add Category",
							  message : "Successfully Added",
							  speed : "slow",
							  withShadow : true
							}
							
							$("body").feedback(options);
				
							$("#displayAllCategories").load(location.href + " #displayAllCategories", function(){

								CCategories.get();
								uiCategoryName.val("");
								$("input[name='category_type[]']").prop("checked", false);
								$(".close-me").trigger("click");

							});

							
				        }
				});


				uiSaveCategory.off("click.savecategory").on("click.savecategory", function(){

					var uiCatTypeContainer = $("#modalAddNewCategoryTypeContainer"),
						uiCatTypeCheckBox = $("input[name='category_type[]']"),
						icount = 0;

					$.each(uiCatTypeCheckBox, function(){
						var uiParentDiv = $(this).closest('.input-container'),
							sCategoryTypeName = uiParentDiv.data('categoryTypeName'),
							sCategoryID = uiParentDiv.data('categoryId');
							oCategoryData.category_type[sCategoryTypeName] = { 'categoryId' : sCategoryID, 'value' : false };

						if($(this).prop("checked")){
							icount++;
							oCategoryData.category_type[sCategoryTypeName]['value'] = true;
						}
					});

					$("#formCategory").trigger("submit");

					if(icount != 0 && uiCategoryName.val().trim() != "")
					{
						oCategoryData["category_name"] = uiCategoryName.val();
						
						add(oCategoryData);
					}		

				});

		}
		else if(sAction === 'saveUpdateCategory')
		{

			//edit ui's
			var uieditCategoryNameInput = $("#editCategoryNameInput"),
				uieditCategory = $("#editCategory"),
				oCatTypes = CCategories.arrCategoryType,
				oCatData = cr8v_platform.localStorage.get_local_storage({
					"name":"currentEditCategory"
				}),
				oDatas = [{
					"company_id": iConstCompanyId,
					"category_name": "",
					"category_id": parseInt(oCatData.categoryId),
					"category_type" : {}
				}],
				oQualifiers = [{
					"field" : "id",
					"operation" : "=",
					"value" : parseInt(oCatData.categoryId)
				}];

				// console.log(oCatData.categoryId);


				$("#updateformCategory").cr8vformvalidation({
					'preventDefault' : true,
				    'data_validation' : 'datavalid',
				    'input_label' : 'labelinput',
				    'onValidationError': function(arrMessages) {
				        // alert(JSON.stringify(arrMessages)); //shows the errors

				        var options = {
							  form : "box",
							  autoShow : true,
							  type : "danger",
							  title  : "Add Category",
							  message : JSON.stringify(arrMessages),
							  speed : "slow",
							  withShadow : true
							}
							
							$("body").feedback(options);
				        },
				    'onValidationSuccess': function() {
				        //alert("Success"); //function if you have something to do upon submission

				        var options = {
							  form : "box",
							  autoShow : true,
							  type : "success",
							  title  : "Edit Category",
							  message : "Successfully Updated",
							  speed : "slow",
							  withShadow : true
							}
							
							$("body").feedback(options);
							$("#displayAllCategories").load(location.href + " #displayAllCategories", function(){

								CCategories.get();

								$(".close-me").trigger("click");

							});

				    }
				});

			uieditCategory.off("click.saveUpdate").on("click.saveUpdate", function(){
				var uiCatTypeContainer = $("#modalUpdateNewCategoryTypeContainer"),
					uiCatTypeCheckBox = $("input[name='update_category_type[]']"),
					icount = 0;


					$.each(uiCatTypeCheckBox, function(){
						var uiParentDiv = $(this).closest('.input-container'),
							scategoryTypeName = uiParentDiv.data("categoryTypeName"),
							scategoryId = uiParentDiv.data("categoryId");
							oDatas[0]["category_type"][scategoryTypeName] = { 'categoryId' : scategoryId, 'value' : false };

							

							if($(this).prop("checked")){
								icount++;
								oDatas[0]["category_type"][scategoryTypeName]['value'] = true;
							}



					});

					$("#updateformCategory").trigger("submit");

					if(icount != 0 && uieditCategoryNameInput.val().trim() != "")
					{
						oDatas[0]["category_name"] = uieditCategoryNameInput.val().trim();
						// console.log(JSON.stringify(oDatas));
						update(oDatas, oQualifiers);
					}		

			});

			
		}
		else if(sAction === 'deleteCategory')
		{
			var uiConfirmDelete = $("#confirmDelete"),
				oCatData = cr8v_platform.localStorage.get_local_storage({"name":"currentEditCategory"}),
				iCategoryId = parseInt(oCatData.categoryId);

			uiConfirmDelete.off("click.confirmIt").on("click.confirmIt", function(){
				$(".modal-close.close-me").trigger("click");

				var options = {
				  form : "box",
				  autoShow : true,
				  type : "success",
				  title  : "Delete Product Category",
				  message : "Successfully Deleted",
				  speed : "slow",
				  withShadow : true
				}
				
				$("body").feedback(options);
				_delete(iCategoryId);
			});


		
		}
		
	}

	function renderElement(sType, oData){
		// console.log(JSON.stringify(oData));


		if(sType === 'categoryType' && Object.keys(oData).length > 0){
			var uiContainer = $("#modalAddNewCategoryTypeContainer");
			uiContainer.html("");

			for(var i in oData){
				var sHtml = '<div class="padding-bottom-10 input-container">'+
						'<input  type="checkbox" datavalid="checkbox-atleast-one" labelinput="Category Type" name="category_type[]" value="'+oData[i]['id']+'"  id="'+oData[i]['id']+'" class="default-cursor">'+
						'<label for ="'+oData[i]['id']+'" class="default-cursor font-300 font-14" >'+oData[i]['name']+'</label>'+
					'</div>';
				var uiType = $(sHtml);
				uiType.data({
					"categoryTypeName" : oData[i]['name'],
					"categoryId" : oData[i]['id']
				});
				uiContainer.append(uiType);
			}
		}

		if(sType === 'getCategory' && Object.keys(oData).length > 0)
		{
			// console.log(JSON.stringify(oData));
			var uigetContainer = $("#displayAllCategories"),
				uiOddEven = '',
				icount = 0;
				uigetContainer.html("");

				for(x in oData)
				{

					if(icount % 2 == 0)
					{
						uiOddEven = '<div class="content-show height-auto width-100per padding-top-20 padding-bottom-20 tbl-dark-color">';
					}
					else
					{
						uiOddEven = '<div class="content-show height-auto width-100per padding-top-20 padding-bottom-20">';
					}
				
							var sHtml = '<div class="table-content position-rel">'+
											uiOddEven+
												'<div class="text-center width-50per display-inline-mid">'+
													'<p>'+oData[x]['category_name']+'</p>'+
												'</div>'+
												'<div class="text-center width-50per display-inline-mid">'+
													'<p>';

												var lenCatType = Object.keys(oData[x]['category_types']).length;

												if(lenCatType == 0){
													sHtml += 'none';
												}else{
													for(var i in oData[x]['category_types']){
														var type = oData[x]['category_types'][i];
														if(lenCatType == (parseInt(i) + 1)){
															sHtml += type.name;
														}else{
															sHtml += type.name + ", ";
														}
														
													}
												}
												sHtml += '</p>'+
												'</div>'+
											'</div>'+
											'<div class="content-hide" style="height: 60px;">'+
												'<button class="btn general-btn modal-trigger " modal-target="edit-product-category" style="width: 100px;">Edit</button>'+
											'</div>'+
										'</div>';
							var uiGet = $(sHtml);
							uiGet.data({
								"categoryId" : oData[x]["category_id"],
								"categoryTypes" : oData[x]['category_types'],
								"categoryName" : oData[x]["category_name"]
							});

							uigetContainer.append(uiGet);
							bindEvents("editCategory", uiGet);

						icount++;

				}
		}

		if(sType === 'updatecategory' && Object.keys(oData).length > 0)
		{
			var uiUpdateContainer = $("#modalUpdateNewCategoryTypeContainer"),
				uieditCategoryNameInput = $("#editCategoryNameInput");
				uiUpdateContainer.html(""),
				oCatTypes = CCategories.arrCategoryType;

			for(var x in oCatTypes){
				var sHtml = '<div class="padding-bottom-10 input-container" data-cat-id="'+oCatTypes[x]['id']+'">'+
									'<input type="checkbox" datavalid="checkbox-atleast-one" labelinput="Category Type"  id="edit-'+oCatTypes[x]['name']+'" value="'+oCatTypes[x]['id']+'" name="update_category_type[]"  class="default-cursor">'+
									'<label for ="edit-'+oCatTypes[x]['name']+'" class="default-cursor font-400 font-14"  >'+oCatTypes[x]['name']+'</label>'+
								'</div>';
				var uiUpdateType = $(sHtml);
				uiUpdateType.data({
					"categoryTypeName" : oCatTypes[x]['name'],
					"categoryId" : oCatTypes[x]['id']
				});

				uiUpdateContainer.append(uiUpdateType);
			}

			uieditCategoryNameInput.val(oData.categoryName);


			for(var n in oData.categoryTypes){

				if($("[data-cat-id='"+oData.categoryTypes[n]["id"]+"']").length > 0){
					$("[data-cat-id='"+oData.categoryTypes[n]["id"]+"'] input[type='checkbox']").prop("checked", true);
				}
			}

			bindEvents("saveUpdateCategory", uiUpdateType);

		}


	}




	return {
		add : add,
		get : get,
		update : update,
		bindEvents : bindEvents,
		renderElement : renderElement,
		arrCategoryType : arrCategoryType
	}


})();

$(document).ready(function(){

	$('#disablehierarchy').click(function(){
		if ($('#disablehierarchy').is(':checked')) {
			$('#hierarchyDisplay').find('#level').val("")

			$('#hidehierarchy').hide();
		}
		else{
			$('#hidehierarchy').show();
		}
	})

	if(window.location.href.indexOf("system_settings") > -1){
		var	uiAddProduct = $("#addProductlink"),
			iuDeleteCategory = $("#deleteCategory"); 

		uiAddProduct.off("click.addproduct").on("click.addproduct", function(){
			$("body").css({overflow:'hidden'});
			var tm = $(this).attr("modal-target");

			$('div[modal-id="add-new-category"]').addClass("showed");

			$("div[modal-id='add-new-category'] .close-me").on("click",function(){
				$("body").css({'overflow-y':'initial'});
				$("div[modal-id='add-new-category']").removeClass("showed");
			});
			CCategories.bindEvents("addproduct");
		});

		// this is to delete Category
		iuDeleteCategory.off("click.deletecategory").on("click.deletecategory", function(){


			$("body").css({overflow:'hidden'});
						
			$("#trashModal").addClass("showed");

			$("#trashModal .close-me").on("click",function(){
				$("body").css({'overflow-y':'initial'});
				$("#trashModal").removeClass("showed");
			});

			CCategories.bindEvents("deleteCategory");


		});

		// get all types of Category
		var oAjaxConfig = {
			type: "GET",
			url: BASEURL+"categories/get_category_type",
			data: {params : 'adsds'},
			async: false,
			success: function(oResponse)
			{
				CCategories.arrCategoryType = oResponse.data;
				CCategories.renderElement("categoryType", CCategories.arrCategoryType);
				
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oAjaxConfig);

		CCategories.get();
	}



});

