var CTransferCompanyGoods = (function() {
	
	var oTransfers = {},
	oTransferProducts = {},
	oCurrentProduct = {},
	oWorkerTypes = {},
	oBatches = {},
	oNotes = {},
	oDocuments = {},
	oProducts = {},
	oUnitOfMeasure = {},
	oModeOfTransfer = {},
	oLocation = {},
	oStorages = {},
	oDateTime = {},
	oLocationBreadCrumb = {},
	
	sTemplate = '',
	sLocation = '',
	sCurrentPath = '',
	sTranferNumber = '',
	sUnitOfMeasure = '',
	sNewTransferNumber = '',
	sLocationBreadCrumb = '',
	sCurrentTransferNumber = '',
	sGeneratedTransferNumber = '',

	btnRadioBulk = {},
	btnRadioPiece = {},
	btnSubmitBatch = {},
	btnAddBatch = {},
	btnEditBatch = {},
	btnRemoveBatch = {},
	btnCompleteTransfer = {},
	btnShowTransferRecord = {},
	btnSubmitCompleteRecord = {},
	btnAddWorker = {},
	btnAddEquipment = {},
	btnCreateRecord = {},
	btnConfirmCreateRecord = {},
	btnAddItem = {},
	btnAddNote = {},
	btnSubmitAddNote = {},
	btnCloseModal = {},
	btnEditProduct = {},
	btnRemoveProduct = {},

	ddUnitOfMeasure = {},
	ddProducts = {},
	ddWorkerDesignation = {},
	ddBatches = {},
	ddModeOfTransfer = {},

	inputWorkerName = {},
	inputEquipmentName = {},
	inputRequestedBy = {},
	inputReasonForTransfer = {},
	inputNoteSubject = {},
	inputNoteContent = {},

	iToLimit = null,
	iCounter = null,
	iTransferLogId = null,

	uiParent = {},
	uiGenerateViewModal = $('.modal-generate-view'),
	uiBatchModal = $('.modal-batch'),
	uiCompleteTransferModal = $('.modal-complete-transfer'),
	uiNoteModal = $('.modal-note'),

	sFailIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg",
	sSuccessIcon = BASEURL + "assets/js/plugins/feedback/images/success.svg";

	function bindEvents() {
		var oData = {};
		sCurrentPath = window.location.href;

		if(sCurrentPath.indexOf('transfers/') > - 1) {
			
			if(sCurrentPath.indexOf("company_transfer") > -1) {
				
				_getAllWorkerType(function(oResult) {
					
					_getAllTransfers(function(oResult) {

						_getAllProducts(function(oResult) {
							
							oData['page'] = 'company_transfer';
							_render(oData);

						});
						
					});

				});
			}

			if(sCurrentPath.indexOf("company_complete") > -1) {
				
			}

			if(sCurrentPath.indexOf("company_create") > -1) {
				
			}

			if(sCurrentPath.indexOf("company_ongoing_edit") > -1) {	

			}

			if(sCurrentPath.indexOf("company_ongoing") > -1 && sCurrentPath.indexOf('company_ongoing_edit') == -1) {
				
			}
		}
	}

	function _render(oParams) {
		var oElement = {

			"company_transfer" : function(oData) {
				uiGenerateViewModal.addClass('showed');

				var uiTransferList = $('#company_transfer_list'),
				uiOngoingTemplate = $('.ongoing-template'),
				uiCompletedTemplate = $('.completed-template'),
				uiCloned = {},
				oTransferData = {},
				btnViewTransfer = {},
				iTransferLength = null;

				if(typeof(oTransfers) != 'string') {	

					iTransferLength = parseInt(Object.keys(oTransfers).length) - parseInt(1);

					for(var a in oTransfers) {
						oTransferProducts = oTransfers[a].product_list;

						if(oTransfers[a]['status'] == 0) {
							uiCloned = uiOngoingTemplate.clone();
							uiCloned.removeClass('ongoing-template');
						} else {
							uiCloned = uiCompletedTemplate.clone();
							uiCloned.removeClass('completed-template')
						}
						
						uiCloned.addClass('cloned-template');
						uiCloned.find('.transfer-number').html(oTransfers[a]['to_number']);
						uiCloned.find('.transfer-date').html(oTransfers[a]['transfer_date']);

						for(var b in oTransferProducts) {

							for(var c in oProducts) {
								if(oTransferProducts[b]['product_id'] == oProducts[c]['id']) {
									oCurrentProduct = oProducts[c];
									uiCloned.find('.transfer-product-list').append('<li>' + oCurrentProduct['name'] + '</li>');
								}
							}
						}

						if(iTransferLength == a) {
							setTimeout(function() {
								uiGenerateViewModal.removeClass('showed');
							},500);
						}

						uiCloned.data('transfer_details', oTransfers[a]);
						uiCloned.show();
						uiTransferList.append(uiCloned);	
					}

					btnViewTransfer = $('.btn-view-record');
					btnCompleteTransfer = $('.btn-complete-record');

					btnViewTransfer.off('click').on('click', function() {
						uiParent = $(this).closest('.cloned-template');
						oTransferData = uiParent.data('transfer_details');
						_setCurrentTransfer(oTransferData['id']);
						setTimeout(function() {
							window.location.href = BASEURL + 'transfers/company_ongoing';
						}, 300);
					});

					btnCompleteTransfer.off('click').on('click', function() {
						uiParent = $(this).closest('.cloned-template');
						oTransferData = uiParent.data('transfer_details');
						console.log(oTransferData);
						// _setCurrentTransfer(oTransferData['id']);
					});

					
					// var uiDiv = $("#company_transfer_list"),
					// uiCurrentDiv = null;
					// uiDiv.append(oElementTemplate);
					// uiCurrentDiv = uiDiv.find("[data-id='" + oElementData.id + "']");
					// uiCurrentDiv.find('.transfer_number').html(oElementData.to_number);
					// uiCurrentDiv.find('.transfer_date').html(oElementData.transfer_date);
					// uiCurrentDiv.find('.transfer_owner').html("Abdul O'neil");

					// var arrProducts = oElementData.product_list,
					// sTemplate = "",
					// uiProductList = uiCurrentDiv.find('.transfer_products');

					// for(var a in arrProducts) {
					// 	sTemplate += '<li>#' + arrProducts[a].details.sku + ' - ' + arrProducts[a].details.name + '</li>';
					// }

					// uiProductList.html(sTemplate);
					// var btnCompleteTransfer = $('.transfer_complete_transfer'),
					// btnAddWorker = $("#btn_add_worker"),
					// uiAddWorkerList = $("#add_worker_list"),
					// btnAddEquipment = $("#btn_add_equipment"),
					// uiAddEquipmentList = $("#add_equipment_list");

					// btnCompleteTransfer.off("click").on("click", function() {
					// 	var uiParentDiv = $(this).closest(".transfer-hoverable"),
					// 	modalCompleteTransfer = $('.modal-complete-transfer'),
					// 	btnShowTransferRecord = $('.show-transfer-record');

					// 	modalCompleteTransfer.addClass('showed');
					// 	modalCompleteTransfer.attr("data-id", uiParentDiv.data("id"));
					// 	btnShowTransferRecord.data("id", uiParentDiv.data("id"));
					// 	btnCompleteTransfer.data("id", uiParentDiv.data("id"));
					// });

					// btnAddWorker.off('click').on('click', function() {
					// 	var sTemplate = _buildTemplate({name : "add_worker"});

					// 	uiAddWorkerList.append(sTemplate);

					// 	var btnRemoveWorker = $(".btn-remove-worker");

					// 	btnRemoveWorker.off("click").on("click", function() {
					// 		var btnParent = $(this).closest('.add-worker-div');
					// 		btnParent.remove();
					// 	});
					// });

					// btnAddEquipment.off('click').on('click', function() {
					// 	var sTemplate = _buildTemplate({name : "add_equipment"});

					// 	uiAddEquipmentList.append(sTemplate);

					// 	var btnRemoveEquipment = $(".btn-remove-equipment");

					// 	btnRemoveEquipment.off("click").on("click", function() {
					// 		var btnParent = $(this).closest('.equipment-div');
					// 		btnParent.remove();
					// 	});
					// });

					// setTimeout(function(){
					// 	// HIDES THE GENERATING VIEW MODAL.
					// 	uiGenerateViewModal.removeClass('showed');
					// }, 1000);

				} else {

					uiGenerateViewModal.find('p').text(oTransfers);
					setTimeout(function() {
						uiGenerateViewModal.removeClass('showed');
					}, 1000);

				}
			},

			"company_create_record" : function(oData) {

			},

			"company_edit_record" : function(oData) {

			},

			"company_view_ongoing_record" : function(oData) {

			},

			"company_view_completed_record" : function(oData) {

			}
		};

		if (typeof (oElement[oParams.page]) == "function") 
		{
			oElement[oParams.page](oParams.page);
		}
	}

	// ================================================== //
	// =======GETTING OF GLOBAL VARIABLE VALUES========== //
	// ================================================== //

	function _getAllTransfers(callBack) {
		oOptions = {
			type : 'GET',
			data : {get : true},
			returnType : 'json',
			url : BASEURL + 'transfers/get_all_company_transfers',
			success : function(oResult) {
				oTransfers = Object.keys(oResult.data).length > 0 ? oResult.data : 'No records found.';
				callBack(oResult.data);
			}
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getTransferRecord(iTransferLogId, callBack) {
		oOptions = {
			type : "GET",
			data : {transfer_log_id : iTransferLogId},
			returnType : "json",
			url : BASEURL + "transfers/get_transfer_record",
			success : function(oResult) {
				oTransfer = Object.keys(oResult.data).length > 0 ? oResult.data : 'No record found.';
				if(typeof callBack == 'function') {
					callBack(oResult.data);	
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}
	
	function _getTransferNumber(callBack) {
		oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "transfers/generate_transfer_number",
			success : function(oResult) {
				sTranferNumber = oResult.data.toString();
				sNewTransferNumber = sTranferNumber.toString();
				sCurrentTransferNumber = null;
				iToLimit = 9;

				while(sNewTransferNumber.length < iToLimit) {
					sNewTransferNumber = "0" + sNewTransferNumber.toString();
				}

				sGeneratedTransferNumber = sNewTransferNumber;
				callBack(sGeneratedTransferNumber);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllProducts(callBack) {
		oOptions = {
			type : 'GET',
			data : {get : true},
			returnType : 'json',
			url : BASEURL + 'transfers/get_product_list',
			success : function(oResult) {
				oProducts = Object.keys(oResult.data).length > 0 ? oResult.data : 'No products found.';
				callBack(oResult.data);
			}
		};

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getAllWorkerType(callBack) {
 		var oOptions = {
 			type : "GET",
 			returnType : "json",
 			data : {test : true},
 			url : BASEURL + "transfers/get_all_worker_type",
 			success : function(oResult) {
 				oWorkerTypes = oResult.data;
 				callBack(oWorkerTypes);
 			}
 		}

 		cr8v_platform.CconnectionDetector.ajax(oOptions);
 	}

	// ================================================== //
	// =======================END======================== //
	// ================================================== //

	function _setCurrentTransfer(iTransferLogId) {
		var oData = {
			name : 'current_company_transfer',
			data : { transfer_log_id : iTransferLogId }
		};

		cr8v_platform.localStorage.delete_local_storage({name : 'current_company_transfer'});
		cr8v_platform.localStorage.set_local_storage(oData);
	}

	return {
		bindEvents : bindEvents
	};
	
})();

$(document).ready(function() {
	CTransferCompanyGoods.bindEvents();
});