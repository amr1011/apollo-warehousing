var CDashboard = (function(){

	function getProductsShelfLife()
	{

		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "dashboard/get_products_shelf_life",
			returnType : "json",
			success : function(oResponse) {
// 				console.log(JSON.stringify(oResponse.data));
				renderElements('renderRemainingShelfLife', oResponse.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getStorageInformation()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "dashboard/get_storage_information",
			returnType : "json",
			success : function(oResponse) {
				// console.log(JSON.stringify(oResponse.data));
				renderElements('renderStorageInformation', oResponse.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getWarehouseMovement()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "dashboard/get_warehouse_movement",
			returnType : "json",
			success : function(oResponse) {
				// console.log(JSON.stringify(oResponse.data));
				renderElements('renderWarehouseMovement', oResponse.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getReceivingGraph()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "dashboard/get_receiving_graph",
			returnType : "json",
			success : function(oResponse) {
				console.log(JSON.stringify(oResponse.data));
				// renderElements('renderWarehouseMovement', oResponse.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getAllDateGraph()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "dashboard/get_all_date_graph",
			returnType : "json",
			success : function(oResponse) {
				// console.log(JSON.stringify(oResponse.data));


				if(typeof oResponse.data == 'undefined' || oResponse.data === null)
				{
					$("#display-graphs-main").hide();

				}else{
					renderElements('renderGraphProducts', oResponse.data);
				}
				
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getSelecetedReceivingGraph(sValue)
	{
		var oOptions = {
			type : "POST",
			data : { "date_start" : sValue },
			url : BASEURL + "dashboard/get_seleceted_receiving_graph",
			returnType : "json",
			beforeSend: function(){
				$('#generate-data-view').addClass('showed');
			},
			success : function(oResponse) {
				// 	console.log(JSON.stringify(oResponse.data));

				_displayReceivingGraphProducts(oResponse.data)
			},
			complete: function(){
				$('#generate-data-view').removeClass('showed');
				$('#generate-data-view').find("text-left").html("Gathering Information");
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getSelecetedWithdrawalGraph(sValue)
	{
		var oOptions = {
			type : "POST",
			data : { "date_start" : sValue },
			url : BASEURL + "dashboard/get_seleceted_withdrawal_graph",
			returnType : "json",
			beforeSend: function(){
				$('#generate-data-view').addClass('showed');
			},
			success : function(oResponse) {
// 				console.log(JSON.stringify(oResponse.data));

				_displayWithdrawalgGraphProducts(oResponse.data)
			},
			complete: function(){
				$('#generate-data-view').removeClass('showed');
				$('#generate-data-view').find("text-left").html("Gathering Information");
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _displayRemainingShelfLifeRecord(oData)
	{
		var uidDisplayMainProductRecord = $("#display-main-product-record"),
			uiTemplate = $(".display-main-product-item"),
			iCount = 1,
			iCountBreak = 0;

			uidDisplayMainProductRecord.html("");

			console.log(JSON.stringify(oData));

			for(var x in oData)
			{

				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-main-product-item").show();
					uiCloneTemplate.find(".display-product-sku-name").html("SKU # "+item.sku+" - "+item.product_name);
					uiCloneTemplate.find(".display-product-batch-name").html(item.batch_name);
					uiCloneTemplate.find(".display-product-remaining-days").html(item.product_shelf_life);

					if(iCount % 2 == 0)
					{
						uiCloneTemplate.addClass("f-right");				

					}else{
						uiCloneTemplate.addClass("f-left");
					}

					uidDisplayMainProductRecord.append(uiCloneTemplate);

					if(iCountBreak % 2 == 0)
					{
						// uiCloneTemplate.addClass("f-left");
						$( '<div class="clear"></div>' ).insertAfter(".f-right");
					}



					iCount++;


			}
	}

	function _displayStorageInformation(oData)
	{

		var uiDisplayRoundedStoragesRecord = $("#display-rounded-storages-record"),
			uiTemplate = $(".display-rounded-storages-item");

			uiDisplayRoundedStoragesRecord.html("");

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();
					// console.log(JSON.stringify(item));


					uiCloneTemplate.removeClass("display-rounded-storages-item").show();
					var uiCircleGrapth = uiCloneTemplate.find(".round-graph"),
						sColor = _randomColors();

					uiDisplayRoundedStoragesRecord.append(uiCloneTemplate);

					var qty = item.storage_quantity,
						cap = parseFloat(item.storage_capacity),
						percent = (item.storage_quantity / cap) * 100;

					uiCircleGrapth.circliful({
						showPercent : 0,
						percent: item.storage_quantity,
						backgroundColor: '#aaa9aa',
						foregroundColor: sColor,         
						customTopText: item.storage_capacity,
						customHtml : item.storage_measurement_name.toUpperCase(),
						textStyle: 'font-size: 15px; font-weight: 700;',
						textColor: '#666',
						text: item.storage_name   
					});

					uiCloneTemplate.css({"display":"inline-block"});

			}

	}

	function _displayReceivingWarehouseMovement(oData)
	{

		var uiSliderContainer = $(".slider-container");

		for(var x in oData)
		{
			var item = oData[x];
		

			var sHtml = '<div class="slide receiving" style="float: left; list-style: none; position: relative; width: 450px; margin-right: 10px;">'+
						'	<div>'+
						'		<div class="title-slide">'+
						'			<p class="f-left">Receiving</p>'+
						'			<p class="f-right receiving-sliding-time">'+item.date_received_time+'</p>'+
						'			<div class="clear"></div>'+
						'		</div>'+
						'		<div>'+
						'			<table class="table-slide">'+
						'				<tbody>'+
						'					<tr>'+
						'						<td style="width: 20%;">Vessel/Truck:</td>'+
						'						<td class="receiving-sliding-vessel-name" style="width: 75%;"><p style="margin-left: 15px; float: left;">'+item.vessel_name+'</p></td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>Consignee(s):</td>'+
						'						<td class="receiving-sliding-consignee-name" ><p style="margin-left: 15px; float: left;">'+item.consignee_info+'</p></td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>Product(s):</td>'+
						'						<td class="receiving-sliding-products"><p style="margin-left: 15px; float: left;">'+item.product_info+'</p></td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>Quantity:</td>'+
						'						<td><p class="quantity display-inline-mid receiving-sliding-quantity" style="margin-left: 15px; float: left;">'+item.total_quantity+' </p><p class="unit display-inline-mid ">MT</p></td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>Status:</td>'+
						'						<td class="status green receiving-sliding-status"><p style="margin-left: 15px; float: left;">'+item.status_name+'</p></td>'+
						'					</tr>'+
						'				</tbody>'+
						'			</table>'+
						'		</div>'+
						'	</div>'+
						'</div>';

						// uiSliderContainer.append(sHtml);
						$(sHtml).appendTo(".slider-container");

		}

		

	}

	function _displayWithdrawalWarehouseMovement(oData)
	{
		var uiSliderContainer = $(".slider-container");

		for(var x in oData)
		{
			var item = oData[x];

			var sHtml = '<div class="slide withdrawal">'+
						'	<div>'+
						'		<div class="title-slide">'+
						'			<p class="f-left">Withdrawal</p>'+
						'			<p class="f-right">'+item.date_withdraw_time+'</p>'+
						'			<div class="clear"></div>'+
						'		</div>'+
						'		<div>'+
						'			<table class="table-slide">'+
						'				<tbody>'+
						'					<tr>'+
						'						<td style="width: 30%;">ATL No.:</td>'+
						'						<td style="width: 65%;">'+item.atl_number+'</td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>CDR No.:</td>'+
						'						<td>'+item.cdr_number+'</td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>Consignee:</td>'+
						'						<td>'+item.consignee_name+'</td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>Product:</td>'+
						'						<td>'+item.withdaw_product_names+'</td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>Quantity:</td>'+
						'						<td><p class="quantity display-inline-mid">'+item.withdaw_total_qty+' </p><p class="unit display-inline-mid ">MT</p></td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>Status:</td>'+
						'						<td class="status green">'+item.status_name+'</td>'+
						'					</tr>'+
						'				</tbody>'+
						'			</table>'+
						'		</div>'+
						'	</div>'+
						'</div>';
			$(sHtml).appendTo(".slider-container");

		}
	}

	function _displayTransferWarehouseMovement(oData)
	{
		for(var x in oData)
		{
			var item = oData[x];

			var sHtml = '<div class="slide transfer" style="border-left: 10px solid #e74c3c;">'+
						'	<div>'+
						'		<div class="title-slide">'+
						'			<p class="f-left">Transfer</p>'+
						'			<p class="f-right">'+item.date_transfer_time+'</p>'+
						'			<div class="clear"></div>'+
						'		</div>'+
						'		<div>'+
						'			<table class="table-slide">'+
						'				<tbody>'+
						'					<tr>'+
						'						<td style="width: 30%;">Requested By:</td>'+
						'						<td style="width: 65%;">'+item.requestor+'</td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>Mode of Transfer:</td>'+
						'						<td>'+item.mode_of_transfer_name+'</td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>Products:</td>'+
						'						<td>'+item.product_names+'</td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>Quantity:</td>'+
						'						<td><p class="quantity display-inline-mid">'+item.total_quantity+' </p><p class="unit display-inline-mid ">MT</p></td>'+
						'					</tr>'+
						'					<tr>'+
						'						<td>Status:</td>'+
						'						<td class="status green">'+item.status_name+'</td>'+
						'					</tr>'+
						'				</tbody>'+
						'			</table>'+
						'		</div>'+
						'	</div>'+
						'</div>';
			$(sHtml).appendTo(".slider-container");
		}
	}

	function _displayReceivingGraphProducts(oData)
	{
		var uiDisplayGraphReceiving = $("#display-graph-receiving"),
			icount = 0;
			
			// console.log(JSON.stringify(oData));

			for(var x in oData)
			{
				if(oData[x]["y"] != 0)
				{
					icount++;
				}
			}

			if(icount == 0)
			{
				
			}

			uiDisplayGraphReceiving.highcharts({
		        chart: {
		            type: 'column',
		            height: 400,
		            width: 480,
		            marginTop: 80
		        },
		        title: {
		            text: 'Items Received'
		        },
		        xAxis: {	        
		            type: 'category'
		        },
		        yAxis: {
		            title: {
		                text: ''
		            }
		        },
		        legend: {
		            enabled: false
		        },

		        tooltip: {
		            headerFormat: '<span style="font-size:14px; font-weight: 700;"> {series.name} </span>',
		            pointFormat: '<span style="color: #4c8e94; font-size: 14px; font-weight: 700;">{point.y}</span> <b> MT</b>'
		        },

		        series: [{
		            name: 'Items Receivied:',
		            colorByPoint: true,
		            data: [{
		                name: 'M',
		                y: oData[0]["y"],
		                drilldown: 'Monday'
		            }, {
		                name: 'T',
		                y: oData[1]["y"],
		                drilldown: 'Tuesday'
		            }, {
		                name: 'W',
		                y: oData[2]["y"],
		                drilldown: 'Wednesday'
		            }, {
		                name: 'TH',
		                y: oData[3]["y"],
		                drilldown: 'Thursday'
		            }, {
		                name: 'F',
		                y: oData[4]["y"],
		                drilldown: 'Friday'
		            }, {
		                name: 'S',
		                y: oData[5]["y"],
		                drilldown: 'Saturday'
		            }, {
		                name: 'S',
		                y: oData[6]["y"],
		                drilldown: 'Sunday'
		            
		            }]
		        }],
		       
		    });


		 _SelectReceivingGraphRecords();
			
	}

	function _displayWithdrawalgGraphProducts(oData)
	{
		var uiDisplayGraphWithdrawal = $("#display-graph-withdrawal"),
			icount = 0;
			
			// console.log(JSON.stringify(oData));

			for(var x in oData)
			{
				if(oData[x]["y"] != 0)
				{
					icount++;
				}
			}

			if(icount == 0)
			{
				
			}

			uiDisplayGraphWithdrawal.highcharts({
		        chart: {
		            type: 'column',
		            height: 400,
		            width: 480,
		            marginTop: 80
		        },
		        title: {
		            text: 'Items Withdrawn'
		        },
		        xAxis: {	        
		            type: 'category'
		        },
		        yAxis: {
		            title: {
		                text: ''
		            }
		        },
		        legend: {
		            enabled: false
		        },

		        tooltip: {
		            headerFormat: '<span style="font-size:14px; font-weight: 700;"> {series.name} </span>',
		            pointFormat: '<span style="color: #4c8e94; font-size: 14px; font-weight: 700;">{point.y}</span> <b> MT</b>'
		        },

		        series: [{
		            name: 'Items Receivied:',
		            colorByPoint: true,
		            data: [{
		                name: 'M',
		                y: oData[0]["y"],
		                drilldown: 'Monday'
		            }, {
		                name: 'T',
		                y: oData[1]["y"],
		                drilldown: 'Tuesday'
		            }, {
		                name: 'W',
		                y: oData[2]["y"],
		                drilldown: 'Wednesday'
		            }, {
		                name: 'TH',
		                y: oData[3]["y"],
		                drilldown: 'Thursday'
		            }, {
		                name: 'F',
		                y: oData[4]["y"],
		                drilldown: 'Friday'
		            }, {
		                name: 'S',
		                y: oData[5]["y"],
		                drilldown: 'Saturday'
		            }, {
		                name: 'S',
		                y: oData[6]["y"],
		                drilldown: 'Sunday'
		            
		            }]
		        }],
		       
		    });


		 _SelectWithdrawnGraphRecords();
	}
	

	function _displayGraphDropdown(oData)
	{
	    var uiWeeksFromAndNow = $(".weeks-from-and-now"),
	    	uiParent = uiWeeksFromAndNow.parent(),
	   	 	uiSibling = uiParent.find('.frm-custom-dropdown');

			uiSibling.remove();
			uiWeeksFromAndNow.removeClass("frm-custom-dropdown-origin");

			for(var x in oData)
			{
				sHtml = '<option value="'+x+'" data-timestamp="'+oData[x]["this_start"]+'">'+oData[x]["weeks"]+'</option>';
				uiWeeksFromAndNow.append(sHtml);
			}

			uiWeeksFromAndNow.transformDD();

			CDropDownRetract.retractDropdown(uiWeeksFromAndNow);

	   

	}

	function _SelectReceivingGraphRecords()
	{
		var uiSelectReceivingGraph = $("#select-receiving-graph"),
			uiWeeksFromAndNow = $(".weeks-from-and-now");
		
		var btnDropdown = $(".receiving-drop-graph").find(".frm-custom-dropdown").find(".frm-custom-dropdown-option").find(".option");
		
		uiSelectReceivingGraph.off('change.productreceivedtimestamp').on('change.productreceivedtimestamp', function(){
			var uiThis = $(this),
				val = uiThis.val(),
				option = uiThis.find('option[value="'+val+'"]'),
				timestamp = option.attr('data-timestamp');

			_getSelecetedReceivingGraph(timestamp);
		});

	}

	function _SelectWithdrawnGraphRecords()
	{
		var uiSelectWithdrawalGraph = $("#select-withdrawal-graph"),
			uiWeeksFromAndNow = $(".weeks-from-and-now");
		
		var btnDropdown = $(".withdrawal-drop-graph").find(".frm-custom-dropdown").find(".frm-custom-dropdown-option").find(".option");
		
		uiSelectWithdrawalGraph.off('change.productwithdrawntimestamp').on('change.productwithdrawntimestamp', function(){
			var uiThis = $(this),
				val = uiThis.val(),
				option = uiThis.find('option[value="'+val+'"]'),
				timestamp = option.attr('data-timestamp');

			_getSelecetedWithdrawalGraph(timestamp);
		});
	}


	function _randomColors()
	{
		var oColors = ["#1abc9c", "#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#8e44ad", "#2c3e50", "#f1c40f", "#e67e22", "#e74c3c", "#95a5a6", "#f39c12", "#d35400", "#c0392b", "#7f8c8d"];
	
		var oColor = oColors[Math.floor(Math.random()*oColors.length)];

		return oColor;
	}

	function bindEvents(sAction, ui)
	{

	}

	function renderElements(sType, oData)
	{
		if(sCurrentPath.indexOf("dashboard") > -1)
		{
			if(sType === 'renderRemainingShelfLife')
			{
				if(Object.keys(oData).length > 0)
				{
					_displayRemainingShelfLifeRecord(oData);
				}else{
					// $("#display-main-transfer-record").hide();
					$("#display-main-product-record").hide();
				}
			}
			else if(sType === 'renderStorageInformation')
			{
				if(Object.keys(oData).length > 0)
				{
					_displayStorageInformation(oData);
				}
			}
			else if(sType === 'renderWarehouseMovement')
			{	
				if(Object.keys(oData).length > 0)
				{
					$("#sliding-this-day").html(oData["this_day"]);
				}

				if(Object.keys(oData["receiving_info"]).length > 0)
				{
					_displayReceivingWarehouseMovement(oData["receiving_info"]);
				}

				if(Object.keys(oData["withdrawal_info"]).length > 0)
				{
					_displayWithdrawalWarehouseMovement(oData["withdrawal_info"]);
				}

				if(Object.keys(oData["transfer_info"]).length > 0)
				{
					_displayTransferWarehouseMovement(oData["transfer_info"]);
				}



				// for div slider 
		        $('.slider-container').bxSlider({
				    slideWidth: 450,
				    minSlides: 2,
				    maxSlides: 2,
					slideMargin: 10,
					pager: false,
					infiniteLoop: true
				});
			}
			else if(sType === 'renderGraphProducts')
			{
				if(Object.keys(oData).length > 0)
				{
					$("#display-graphs-main").show();
					_displayGraphDropdown(oData[0]["weeks_graph_info"]);
					_displayReceivingGraphProducts(oData[0]["current_receiving_graph_now"]);
					_displayWithdrawalgGraphProducts(oData[0]["current_withdrawal_graph_now"]);

// 					console.log(JSON.stringify(oData));

				}else{
					$("#display-graphs-main").hide();
				}
			}

		}
	}

	return {
		bindEvents : bindEvents,
		renderElements : renderElements,
		getProductsShelfLife : getProductsShelfLife,
		getStorageInformation : getStorageInformation,
		getWarehouseMovement : getWarehouseMovement,
		getAllDateGraph : getAllDateGraph,
		getReceivingGraph : getReceivingGraph

	}

})();



$(document).ready(function(){

	if(sCurrentPath.indexOf("dashboard") > -1)
	{
		CDashboard.getProductsShelfLife();
		CDashboard.getStorageInformation();
		CDashboard.getWarehouseMovement();
		CDashboard.getAllDateGraph();
		// CDashboard.getReceivingGraph();
	}

});