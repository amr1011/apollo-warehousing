var CCReceivingConsigneeGoods = (function(){

	var failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg",
		receiving_view_all = '',
		oProducts = '',
		oConsigneeName = '',
		oTempConsigneeName = '',
		oUploadedDocuments = {},
		oCreateNotes = '',
		oModeOfDelivery = '',
		oAddConsigneeReceivingItem = '',
		oStorages = '',
		oSelectedData = '',
		oItemReceivingBalance = {},
		oBatchPerItem = {},
		bBatchEditMode = false,
		sBatchEditKey = "";

	function _add(oData)
	{

		var oOptions = {
			url : BASEURL+"receiving/add_receiving_consignee_goods",
			type : "POST",
			data : {
					"receiving_number":oData["receiving_number"],
					"purchase_order" : oData["purchase_order"],
					"mode_of_delivery":oData["mode_of_delivery"],
					"product_data":JSON.stringify(oData["product_data"]),
					"note_data":JSON.stringify(oData["note_data"]),
					"delivery_mode_data":JSON.stringify(oData["delivery_mode_data"]),
					"consignee_items":JSON.stringify(oData["consignee_items"]),
					"uploaded_documents":JSON.stringify(oData["uploaded_documents"])
				 },
			async : false,
			beforeSend : function(){

				$('#generate-saving-data').addClass('showed');

			},
			success : function(oResponse)
			{
				
				var oDataResponse = JSON.parse(oResponse);

				console.log(oDataResponse);

				cr8v_platform.localStorage.set_local_storage({
			 		name : "setCurrentDisplayReceivedId",
			 		data : {value:oDataResponse["data"], origin:oData["mode_of_delivery"]}
			 	});


				$('#generate-saving-data').removeClass('showed');

				var options = {title : "Successfully Added", message : "ConsigneeGoods added!", speed : "slow", withShadow : true, type: "success"};
				$("body").feedback(options);


			    $("body").css({overflow:'hidden'});
				
				$("#SaveconfirmCreateRecord").addClass("showed");

				$("#SaveconfirmCreateRecord .close-me").on("click",function(){
					$("body").css({'overflow-y':'initial'});
					$("#SaveconfirmCreateRecord").removeClass("showed");
				});


				
			}

		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);

		cr8v_platform.localStorage.delete_local_storage({name:"dataUploadedDocument"});

	}


	/**
     * _getProducts
     * @description This function is for getting all products
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _getProducts(callBack)
	{
		var oOptions = {
			url : BASEURL+"/receiving/get_all_products",
			type : "GET",
			data : {"oparams": "no"},
			async : false,
			beforeSend : function(){
				$('#generate-data-view').addClass('showed');
			},
			success: function(oResponse)
			{
				// console.log(JSON.stringify(oResponse.data));
				// arrGetAllProducts = oResponse.data;

				if(oResponse.data === null){

				}else{
					if(typeof callBack == 'function')
					{
						callBack(oResponse.data)
					}
				}

				
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	/**
     * getVesselName
     * @description This function is for getting all Vessel
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _getVesselName(callBack)
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "receiving/get_vessel_name",
			returnType : "json",
			success : function(oResult) {

				if(oResult.data === null){

				}else{

					if(typeof callBack == 'function')
					{
						callBack(oResult.data)
					}

					// cr8v_platform.localStorage.set_local_storage({
					// 	name : "getAllVesselName",
					// 	data : oResult.data
					// });
					// CCReceivingConsigneeGoods.renderElement("displayItemsForVesselList", oResult.data);
					// CCReceivingConsigneeGoods.renderElement("displayItemsForVesselListForTruck", oResult.data);
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	/**
     * _getAllStorage
     * @description This function is for getting all Storages
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _getAllStorage(callBack)
	{
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "storages/get_all_storages",
			success : function(oResult) {
				if(typeof callBack == 'function')
				{
					callBack(oResult);
				}
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	/**
     * _displaySelectedReceivingConsignee
     * @description This function is for getting selected Record
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _getSelectedReceivingConsignee(callBack)
	{
		var getCurrentSelectedReceivedConsignee = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"}),
			sWhatOrigin = getCurrentSelectedReceivedConsignee.origin,
			sOrigin = '';
		// console.log(getCurrentSelectedReceivedConsignee.value);

			if(sWhatOrigin == 'Truck')
			{
				sOrigin = BASEURL+"receiving/get_current_selected_receiving_consignee_truck";
			}else{
				sOrigin = BASEURL+"receiving/get_current_selected_receiving_consignee_vessel";
			}


		var oOptions = {
			url : sOrigin,
			type : "POST",
			data: {"receiving_id":getCurrentSelectedReceivedConsignee.value},
			returnType: "json",
			beforeSend : function(){
				$('#generated-data-view').addClass('showed');
			},
			success: function(oResult){
				if(oResult.data === null)
				{
					console.log("Not Found");
				}else{

					if(typeof callBack == 'function')
					{
						callBack(oResult.data);
					}
					// cr8v_platform.localStorage.set_local_storage({
					// 	name : "setSelectedReceivingConsigneeItem",
					// 	data : oResult.data
					// });
					// console.log(JSON.stringify(oResult.data));
				}


			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	function _getUnitOfMeasure(callBack)
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "products/get_unit_of_measure",
			returnType : "json",
			success : function(oResult) {
				// console.log(JSON.stringify(oResult.data));
				// if(oResult.data === null)
				// {
				// 	cr8v_platform.localStorage.set_local_storage({
				// 		"name" : "unit_of_measures",
				// 		"data" : ""
				// 	});
				// }else{
				// 	cr8v_platform.localStorage.set_local_storage({
				// 		"name" : "unit_of_measures",
				// 		"data" : oResult.data
				// 	});
				// }

				if(typeof callBack == 'function'){
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	/**
     * _getAllReceivingConsignee
     * @description This function is for get all receiving consignee record
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _getAllReceivingConsignee(callBack)
	{

		var oOptions = {
			url : BASEURL+"receiving/display_all_receiving_consignee_goods",
			type : "GET",
			data : {"data":"none"},
			returnType : "json",
			async : false,
			beforeSend: function(){
				$('#generated-data-view').addClass('showed');
			},
			success : function(oResult){

				$('#generated-data-view').removeClass('showed');

				if(oResult.data === null)
				{

				}else{

					if(typeof callBack == 'function')
					{
						callBack(oResult.data)
					}
				}
			}

		} 
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}


	/**
     * _getConsignee
     * @description This function is for get all consignee Names
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _getConsignee(callBack)
	{
		var oOptions = {
			url : BASEURL+"consignee/get_consignee_names",
			type : "GET",
			data : {"oparams": "no"},
			async : false,
			success: function(oResponse)
			{
				// console.log(JSON.stringify(oResponse.data));
				// arrGetAllProducts = oResponse.data;

				// if(oResponse.data === null){
				// 	cr8v_platform.localStorage.set_local_storage({
				// 		name : "getAllConsigneeReceiveConsignee",
				// 		data : ""
				// 	});
				// }else{
				// 	cr8v_platform.localStorage.set_local_storage({
				// 		name : "getAllConsigneeReceiveConsignee",
				// 		data : oResponse.data
				// 	});
				// }

				if(typeof callBack == 'function')
				{
					callBack(oResponse.data);
				}

				
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}


	/**
     * _saveConsigneeOngoingTruck
     * @description This function is for saving receiving consignee truck
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _saveConsigneeOngoingTruck(oData)
	{
		var oReceivingId = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"}),
			iReceivedId = oReceivingId.value;

		var oOptions = {
			url : BASEURL+"receiving/save_consignee_ongoing_truck_tare",
			type : "POST",
			data : {"receiving_id":iReceivedId, "data":JSON.stringify(oData)},
			success : function(oResponse){
				
				window.location.href = BASEURL+"receiving/consignee_complete_truck";
			}

		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);

		cr8v_platform.localStorage.get_local_storage({name:"currentReceivingNumber"});
		cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"});

	}

	/**
     * _saveCompleteReceivingRecordFormVessel
     * @description This function is for saving receiving consignee vessel
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _saveCompleteReceivingRecordFormVessel(oData)
	{
		oAjax = {
			url : BASEURL+"receiving/save_complete_receiving_record_vessel",
			type : "POST",
			data : {"data":JSON.stringify(oData)},
			success : function(oResponse){

				window.location.href = BASEURL+"receiving/consignee_complete_vessel";
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oAjax);

		cr8v_platform.localStorage.delete_local_storage({name:"setSelectedReceivingConsigneeItem"});
		cr8v_platform.localStorage.delete_local_storage({name:"SetSelectedProductForReceivingConsignee"});
		cr8v_platform.localStorage.delete_local_storage({name:"dataUploadedDocument"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllConsigneeReceiveConsignee"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllProductsReceiveConsignee"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllVesselData"});
		
	}


	/**
     * _getCurrentDateTime
     * @description This function is for getting the current Date and time
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _getCurrentDateTime()
	{
		var oOptions = {
			type : "POST",
			data : { data : "none" },
			url : BASEURL + "receiving/get_current_date_time",
			returnType : "json",
			success : function(oResult) {

				cr8v_platform.localStorage.set_local_storage({
					name : "setCurrentDateTime",
					data : {"date":oResult.data.date, "dateFormat":oResult.data.date_formatted}
				})
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	/**
     * _itemsEvent
     * @description This function is for searching item
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _itemsEvent()
	{
		var methods = {
			displayToSelect : function(oData, bShowResult){
				var oTimeOut = {},
					uiSelect = $('#selectProductListSelect'),
					uiParent = uiSelect.parent(),
					uiSibling = uiParent.find('.frm-custom-dropdown');
				// uiSibling.remove();

				// uiSelect.removeClass("frm-custom-dropdown-origin");

				// uiSelect.html("");

				for(var i in oData){
					var sHtml = '<option value="'+oData[i]['id']+'">'+oData[i]['name']+'</option>';
					uiSelect.append(sHtml);
				}

				uiSelect.transformDD();

				CDropDownRetract.retractDropdown(uiSelect);


				var uiInput = uiParent.find('input.dd-txt');


				if(bShowResult){
					uiParent.find('.frm-custom-icon').click();
					uiInput.focus();
				}

				

				uiInput.off("keyup").on("keyup", function(e){
					var val = $(this).val();
					uiInput.attr("value",val);
					
					clearTimeout(oTimeOut);
					if(val.length > 0 && e.keyCode !=8 && e.keyCode !=48){
						oTimeOut = setTimeout(function(){
							methods.search(val, true);
						}, 3000);	
					}
					

				});
			},
			getItem : function(id){
				for(var i in oItems){
					if(oItems[i]["id"] == id){
						return oItems[i];
					}
				}
			},
			search : function(sKey, bShowResult){
				var oOptions = {
					type : "POST",
					data : {
						key : sKey,
						limit : 30,
						sorting : "asc",
						sort_field : "name"
					},
					url : BASEURL + "products/search",
					returnType : "json",
					success : function(oResult) {
						oItems = oResult.data;
						methods.displayToSelect(oItems, bShowResult);
					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);
			}
		};

		methods.search("");
	}

	/**
     * _buildProductListDropDown
     * @description This function is for building product dropdowns
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _buildProductListDropDown(oData)
	{
		var oSetDataPoductListReceivingConsignee = oData,
			uiSetProductListSelect = $("#selectProductListSelect"),
			uiParent = uiSetProductListSelect.parent(),
			uiSibling = uiParent.find('.frm-custom-dropdown');

			uiSibling.remove();
			uiSetProductListSelect.removeClass("frm-custom-dropdown-origin");

			for(var x in oSetDataPoductListReceivingConsignee)
			{
				var sHtml = '<option value="'+oSetDataPoductListReceivingConsignee[x]['id']+'">'+oSetDataPoductListReceivingConsignee[x]['name']+'</option>';
				uiSetProductListSelect.append(sHtml);
			}

			uiSetProductListSelect.transformDD();

			CDropDownRetract.retractDropdown(uiSetProductListSelect);


	}

	/**
     * _buildVesselNameDropDown
     * @description This function is for building Vessel dropdowns
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _buildVesselNameDropDown(oData)
	{
		var oSetDataVesselListReceivingConsignee = oData,
			uiSetVesselListSelect = $("#addInputVesselName"),
			uiParent = uiSetVesselListSelect.parent(),
			uiSibling = uiParent.find('.frm-custom-dropdown');

			uiSibling.remove();
			uiSetVesselListSelect.removeClass("frm-custom-dropdown-origin");

			if(oSetDataVesselListReceivingConsignee === 0){
				var sHtml ='<option value="">No Vessels Found</option>';
				uiSetVesselListSelect.append(sHtml);
			}else{
				for(var x in oSetDataVesselListReceivingConsignee)
				{
					sHtml = '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'">'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
					uiSetVesselListSelect.append(sHtml);
				}

			}

			
			uiSetVesselListSelect.transformDD();
			CDropDownRetract.retractDropdown(uiSetVesselListSelect);
	}

	/**
     * _buildVesselNameForTruckDropDown
     * @description This function is for building Vessel dropdowns for Truck
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _buildVesselNameForTruckDropDown(oData)
	{
		var oSetDataVesselListReceivingConsignee = oData,
			uiSetVesselListSelect = $("#addInputTruckOrigin"),
			uiParent = uiSetVesselListSelect.parent(),
			uiSibling = uiParent.find('.frm-custom-dropdown');

			uiSibling.remove();
			uiSetVesselListSelect.removeClass("frm-custom-dropdown-origin");

			if(oSetDataVesselListReceivingConsignee === 0){
				var sHtml ='<option value="">No Vessels Found</option>';
				uiSetVesselListSelect.append(sHtml);
			}else{
				for(var x in oSetDataVesselListReceivingConsignee)
				{
					sHtml = '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'">'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
					uiSetVesselListSelect.append(sHtml);
				}

			}

			
			uiSetVesselListSelect.transformDD();
			CDropDownRetract.retractDropdown(uiSetVesselListSelect);
	}

	/**
     * _buildUnitMeasureDropDown
     * @description This function is for building Unit of Measure dropdowns
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */
	function _buildUnitMeasureDropDown(ui)
	{

		var uiSelectMeasurement = ui.find(".unit-of-measure"),
			oGetData = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
			oData = oGetData;
		
		uiSelectMeasurement.html("");
		for(var x in oData)
		{
			if(oData[x]['name'] == "kg" || oData[x]['name'] == "mt" || oData[x]['name'] == "L" )
			{					
				var sHtmlMeasure = '<option value="'+oData[x]['id']+'">'+oData[x]['name']+'</option>';
				 uiSelectMeasurement.append(sHtmlMeasure);
			}

		}

		ui.find(".unit-of-measure").show().css({
			"padding" : "3px 4px",
			"width" :  "50px"
		});
		ui.find(".remove-measure-drop").find(".frm-custom-dropdown").remove();
		

	}

	/**
     * displayAllReceivingConsignee
     * @description This function is for displaying all receiving consignee goods
     * @dependencies 
     * @param N/A
     * @response N/A
     * @criticality N/A
     * @software_architect N/A
     * @developer Nelson Estuesta Jr
     * @method_id N/A
     */

	function _displayAllReceivingConsignee(oData)
	{
		var uiReceivingConsigneeContainer = $("#displayAllReceivingConsigneeList"),
			uiTemplate = uiReceivingConsigneeContainer.find(".display-receiving-records"),
			uiDisplayTotalRow = $("#total-row-consignee"),
			iTotal = Object.keys(oData).length;


			uiDisplayTotalRow.html("Total Records: "+iTotal);

			uiReceivingConsigneeContainer.html("");

			// console.log(JSON.stringify(oData));
			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone();

					uiCloneTemplate.removeClass("display-receiving-records").show();
					uiCloneTemplate.find(".diplay-receiving-consignee-receiving-num").html(item.receiving_number);
					uiCloneTemplate.find(".diplay-receiving-consignee-po").html(item.purchase_order);
					uiCloneTemplate.find(".diplay-receiving-consignee-issued-date").html(item.issued_date);
					uiCloneTemplate.find(".diplay-receiving-consignee-vessel-name").html(item.origin);
					uiCloneTemplate.find(".diplay-receiving-consignee-user-name").html(item.owner);
					uiCloneTemplate.find(".diplay-receiving-consignee-user-pic").attr("src", CGetProfileImage.getDefault(item.owner));
					uiCloneTemplate.find(".diplay-receiving-consignee-status").html(item.status);

					for(var i in item.prducts)
					{
						var prods = item.prducts[i],
							SecondClone = uiCloneTemplate.find(".list-records").clone();

							SecondClone.removeClass('list-records').show();
							SecondClone.find(".display-items").html('#'+prods.sku+" - "+prods.name);

							uiCloneTemplate.find(".list-of-records").append(SecondClone);

					}

					uiCloneTemplate.find(".list-of-records").find(".list-records").remove();

					if(item.status == "Complete" && item.status_id == 1)
					{
						var uiLink = '<button class="btn general-btn margin-right-10 setConsigneeReceiveId">View Record</button>',
							uiSaveLink = $(uiLink);

							uiSaveLink.data({
								"receiving_id":item.receiving_id,
								"status": item.status
							});

						uiCloneTemplate.find(".display-receiving-link").append(uiSaveLink);


					}else if(item.status == "Ongoing" && item.status_id == 2)
					{
						var uiLink = '<button class="btn general-btn margin-right-10 setConsigneeReceiveId">View Record</button>',
							uiSaveLink = $(uiLink);

							uiSaveLink.data({
								"receiving_id":item.receiving_id,
								"status": item.status,
								"origin":item.origin
							});

							uiCloneTemplate.find(".display-receiving-link").append(uiSaveLink);

							var uiMustComp = '<button class="btn general-btn modal-trigger show-modal-complete" data-origin="'+item.origin+'">Complete Receiving Record</button>',
								saveMustComp = $(uiMustComp);

								saveMustComp.data({
									"receiving_id":item.receiving_id,
									"status": item.status,
									"origin":item.origin,
									"departure_date":item.departure_date,
									"departure_time":item.departure_time

								});

							uiCloneTemplate.find(".display-receiving-link").append(saveMustComp);

					}else if(item.status == "Ongoing" && item.status_id == 0)
					{
						var uiLink = '<button class="btn general-btn margin-right-10 setConsigneeReceiveId">View Record</button>',
							uiSaveLink = $(uiLink);

							uiSaveLink.data({
								"receiving_id":item.receiving_id,
								"status": item.status,
								"origin":item.origin
							});

							uiCloneTemplate.find(".display-receiving-link").append(uiSaveLink);

							var uiMustComp = '<button class="btn general-btn modal-trigger setConsigneeReceiveId">Enter Product Details</button>',
								saveMustComp = $(uiMustComp);

								saveMustComp.data({
									"receiving_id":item.receiving_id,
									"status": item.status,
									"origin":item.origin
								});

							uiCloneTemplate.find(".display-receiving-link").append(saveMustComp);
					}




				uiReceivingConsigneeContainer.append(uiCloneTemplate);

				_setReceivingConsigneeID(uiCloneTemplate);

				_receivingListAddCompleteRecord()

				
			}

	}

	/**
    * _setReceivingConsigneeID
    * @description This function is for setting receiving id receiving consignee goods
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _setReceivingConsigneeID(uiCloneTemplate)
	{
		var uiSetConsigneeReceiveIdBtn = $(".setConsigneeReceiveId"),
			btnShowModalComplete = $(".show-modal-complete");

		 uiSetConsigneeReceiveIdBtn.off("click.consigneeBtn").on("click.consigneeBtn", function(){
		 	var iReceivedId = $(this).data("receiving_id"),
		 		sStatus = $(this).data("status"),
		 		sOrigin = $(this).data("origin");

		 		// console.log(iReceivedId);

		 	cr8v_platform.localStorage.set_local_storage({
		 		name : "setCurrentDisplayReceivedId",
		 		data : {value:iReceivedId, origin:sOrigin}
		 	});

// 		 	if(sStatus == "Ongoing" && sOrigin == "Vessel")
// 		 	{
// 		 		window.location.href = BASEURL+'receiving/consignee_ongoing_vessel';
// 		 	}
// 		 	else if(sStatus == "Ongoing" && sOrigin == "Truck")
// 		 	{
// 		 		window.location.href = BASEURL+'receiving/consignee_ongoing_truck';
// 		 	}
// 		 	else if(sStatus == "Complete" && sOrigin == "Vessel")
// 		 	{
// 		 		window.location.href = BASEURL+'receiving/consignee_complete_vessel';
// 		 	}
// 		 	else if(sStatus == "Complete" && sOrigin == "Truck")
// 		 	{
// 		 		window.location.href = BASEURL+'receiving/consignee_complete_truck';
// 		 	}

		 	if(sStatus == "Ongoing")
		 	{
		 		window.location.href = BASEURL+'receiving/consignee_ongoing';
		 	}
		 	else if(sStatus == "Complete")
		 	{
		 		window.location.href = BASEURL+'receiving/consignee_complete';
		 	}
		 	

		 });

		 btnShowModalComplete.off("click.consigneeBtnID").on("click.consigneeBtnID", function(){
		 	iReceivedId = $(this).data("receiving_id");

		 	cr8v_platform.localStorage.set_local_storage({
		 		name : "setCurrentDisplayReceivedId",
		 		data : {value:iReceivedId, origin:sOrigin}
		 	});

		 });
	}

	/**
    * _receivingListAddCompleteRecord
    * @description This function is for Displaying Modal for Receiving List
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _receivingListAddCompleteRecord()
	{
		var uiShowModalComplete = $(".show-modal-complete");


			uiShowModalComplete.off("click.checkIfWhat").on("click.checkIfWhat", function(){
				var uiThisClick = $(this),
					uiOrigin = uiThisClick.attr("data-origin"),
					oItemData = uiThisClick.data();
					

					if(uiOrigin == "Vessel")
					{
						_receivingListAddVessel(oItemData);

						// console.log(JSON.stringify(oItemData));

						$("body").css({overflow:'hidden'});
					
						$(".show-complete-record-vessel-modal").addClass("showed");

						$(".show-complete-record-vessel-modal .close-me").on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$(".show-complete-record-vessel-modal").removeClass("showed");
						});

					}
					else if(uiOrigin == "Truck")
					{
						
						$("body").css({overflow:'hidden'});
					
						$(".show-complete-record-truck-modal").addClass("showed");

						$(".show-complete-record-truck-modal .close-me").on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$(".show-complete-record-truck-modal").removeClass("showed");
						});

						_receivingListAddTruck(oItemData);
					}

			});
	}


	/**
    * _receivingListAddTruck
    * @description This function is for Calculation for completing truck list
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _receivingListAddTruck(oItemData)
	{
		var uiSetTareIn = $("#set-tare-in"),
			uiSetTareOut = $("#set-tare-out"),
			uiSetNetWeight = $("#set-net-weight"),
			uiTriggerComputeNetWeight = $(".trigger-compute-net-weight"),
			btnCompleteReceivingRecordTruck = $("#complete-receiving-record-truck"),
			iTotal = 0;

			uiSetTareIn.number(true, 2);
			uiSetTareOut.number(true, 2);


			uiTriggerComputeNetWeight.off("keyup.compute").on("keyup.compute", function(){
				var iGetTareIn = parseFloat(uiSetTareIn.val()),
					iGetTareOut = parseFloat(uiSetTareOut.val());

					if(uiSetTareIn.val().trim() != "" && uiSetTareOut.val().trim() != "")
					{
						uiSetNetWeight.html("");

						iTotal = iGetTareIn - iGetTareOut;
						uiSetNetWeight.html($.number(iTotal, 2)+" MT");
					}	
			});

			btnCompleteReceivingRecordTruck.off("click.confirmed").on("click.confirmed", function(e){
				e.preventDefault();
				if(uiSetTareIn.val().trim() != "" && uiSetTareOut.val().trim() != "" && iTotal > 0)
				{
					$("body").feedback({title : "Complete Receiving Record", message : "Successfully Complete", type : "success"});

					var oData = {
						"tare_in" : parseFloat(uiSetTareIn.val().trim()),
						"tare_out" : parseFloat(uiSetTareOut.val().trim()),
						"net_weight" : $.number(iTotal, 2)
					}

					_saveConsigneeOngoingTruck(oData);


				}else{
					$("body").feedback({title : "Complete Receiving Record", message : "Please fill up Truck Tare In and Tare Out Correctly", type : "danger", icon : failIcon});
				}

			});

	}

	/**
    * _receivingListAddVessel
    * @description This function is for Calculation for completing vessel list
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _receivingListAddVessel(oItemData)
	{
		var uiBerthingStartDate = $("#berthing-start-date"),
			uiBerthingStartTime = $("#berthing-start-time"),
			uiUnloadingStartDate = $("#unloading-start-date"),
			uiUnloadingStartTime = $("#unloading-start-time"),
			uiUnloadingEndDate = $("#unloading-end-date"),
			uiUnloadingEndTime  =$("#unloading-end-time"),
			uiDurationDateTime = $("#duration-date-time"),
			uiDurationDate = $("#departure-date"),
			uiDurationTime = $("#departure-time"),
			btnConfirmCompleteReceivingRecord = $("#confirm-complete-receiving-record"),
			uiTriggerComputeDuration = $(".trigger-compute-duration"),
			iDurationDateTime = 0,
			iResultDates = 0,
			arrErrorCount = [];

			uiBerthingStartDate.val(oItemData.departure_date);
			uiBerthingStartTime.val(oItemData.departure_time);


			uiTriggerComputeDuration.off("click.duration change.durations").on("click.duration change.durations", function(){

				if(uiUnloadingStartDate.val().trim() != "" 
					&& uiUnloadingStartTime.val().trim() != "" 
					&& uiUnloadingEndDate.val().trim() != ""
					&& uiUnloadingEndTime.val().trim() != "")
				{
					var sUnloadingStartDate = uiUnloadingStartDate.val().split('/').join("-"),
						sUnloadingEndDate = uiUnloadingEndDate.val().split('/').join("-"),
						sStartTime = _convertHours(uiUnloadingStartTime.val()),
						sEndTime = _convertHours(uiUnloadingEndTime.val()),
						sStartDateTime = sUnloadingStartDate+" "+sStartTime,
						sEndDateTiem = sUnloadingEndDate+" "+sEndTime;

						var sGetTimesTampStart = _convertDateAndHoursToTimeStamp(sStartDateTime),
							sGetTimesTampEnd = _convertDateAndHoursToTimeStamp(sEndDateTiem),
							sGetResultTimeStamp = sGetTimesTampEnd - sGetTimesTampStart,
							sConvertToml = (sGetResultTimeStamp) * 1000;

							iResultDates = sGetResultTimeStamp;

							console.log(sGetResultTimeStamp);

					// uiDurationDateTime.html("");
					var date = new Date(sConvertToml);
						var str = date.getUTCDate()-1 + " days, "+date.getUTCHours() + " hours, "+date.getUTCMinutes() + " minutes, "+date.getUTCSeconds() + " seconds. ";

						uiDurationDateTime.text(str);

						// console.log("ok");

						iDurationDateTime = sConvertToml;
						// 	console.log(iDurationDateTime);
				}
			});

			btnConfirmCompleteReceivingRecord.off("click.confirmed").on("click.confirmed", function(){

				(uiBerthingStartDate.val().trim() == "") ? arrErrorCount.push("Berthing Date Start Required") : "";
				(uiBerthingStartTime.val().trim() == "") ? arrErrorCount.push("Berthing Time Start Required") : "";
				(uiUnloadingStartDate.val() == "") ? arrErrorCount.push("Unloading Date Start Required") : "";
				(uiUnloadingStartTime.val() == "") ? arrErrorCount.push("Unloading Time Start Required") : "";
				(uiUnloadingEndDate.val() == "") ? arrErrorCount.push("Unloading Date End Required") : "";
				(uiUnloadingEndTime.val() == "") ? arrErrorCount.push("Unloading Time End Required") : "";
				(uiDurationDate.val() == "") ? arrErrorCount.push("Duration Date Required") : "";
				(uiDurationTime.val() == "") ? arrErrorCount.push("Duration Time Required") : "";

				(iResultDates < 0) ? arrErrorCount.push("Unloading Date/Time End must greater than Unloading Date/Time Start") : "";

				if(arrErrorCount.length > 0)
				{
					$("body").feedback({title : "Completing Receiving Record", message : arrErrorCount.join("<br />"), type : "danger", icon : failIcon});
				}else{

					var oData = {
						receiving_id : oItemData.receiving_id,
						berthing_date : uiBerthingStartDate.val().trim(),
						berthing_time : uiBerthingStartTime.val().trim(),
						start_unloading_date : uiUnloadingStartDate.val(),
						start_unloading_time : uiUnloadingStartTime.val(),
						end_unloading_date : uiUnloadingEndDate.val(),
						end_unloading_time : uiUnloadingEndTime.val(),
						duration_date : uiDurationDate.val(),
						duration_time : uiDurationTime.val(),
						unloading_duration : iDurationDateTime
					}

					_saveCompleteReceivingRecordFormVessel(oData);

					var options = {title : "Successfully Completed", message : "Consignee Goods Completed!", speed : "slow", withShadow : true, type: "success"};
					$("body").feedback(options);

				}

				arrErrorCount = [];
			});
	}

	/**
    * _convertHours
    * @description This function is for conveting hours example = 10:15 PM to 22:15
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _convertHours(time)
	{
		// Convert 10:15 PM to 22:15

		// var time = uiUnloadingEndTime.val();
		var hours = Number(time.match(/^(\d+)/)[1]);
		var minutes = Number(time.match(/:(\d+)/)[1]);
		var AMPM = time.match(/\s(.*)$/)[1];
		if(AMPM == "PM" && hours<12) hours = hours+12;
		if(AMPM == "AM" && hours==12) hours = hours-12;
		var sHours = hours.toString();
		var sMinutes = minutes.toString();
		if(hours<10) sHours = "0" + sHours;
		if(minutes<10) sMinutes = "0" + sMinutes;
		// console.log(sHours + ":" + sMinutes);
		var sGetHoursAndMins =  sHours + ":" + sMinutes;

		return sGetHoursAndMins;
	}

	/**
    * _convertDateAndHoursToTimeStamp
    * @description This function is for conveting DateTime 
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _convertDateAndHoursToTimeStamp(sStartDateTime)
	{

		var datum = Date.parse(sStartDateTime);
 			return datum/1000;

	}

	/**
    * _triggerModeOfDelivery
    * @description This function is for trigger of mode of delivery
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _triggerModeOfDelivery()
	{
		$(".mode-of-deliver.select div.option").click(function() {
				var selectedDelivery = $(this).text();
				switch (selectedDelivery) {
					case 'Vessel' :
						$(".vessel-delivery").slideDown();
						$(".truck-delivery").slideUp();
						break;
					case 'Truck' :
						$(".vessel-delivery").slideUp();
						$(".truck-delivery").slideDown();
						break;
					default : 
						break;
				}
			});

			$(".mode-of-deliver.select div.option").trigger("click")
	}

	/**
    * _triggerAddProductItem
    * @description This function is for trigger of adding product item
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _triggerAddProductItem()
	{
		var uibtn = $("#addItemProductConsigneeBtn");

		uibtn.off("click.addItem").on("click.addItem", function(e){
			e.preventDefault();
			var uiProductDropDown = $("#selectProductListSelect option:selected");

			// console.log(uiProductDropDown.val());

			_displayProductSelected(uiProductDropDown.val());


		});
	}

	/**
    * _displayProductSelected
    * @description This function is for diplaying selected product item
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _displayProductSelected(prod_id)
	{
		var uiContainer = $("#displayReceivingConsigneeProductSelects"),
			uiTemplate = uiContainer.find(".product_selected_receive_consign"),
			uiAddInputVesselName = $(".mode-of-deliver").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt"),
			sWord = _shuffelWord("abcdefghijklmnop");


// 			console.log(JSON.stringify(oProducts));

		for(var x in oProducts)
		{
			var item = oProducts[x];

			if(item.id == prod_id)
			{
				var uiCloneTemplate = uiTemplate.clone();

				uiCloneTemplate.removeClass("product_selected_receive_consign").show();

				
				uiCloneTemplate.css({"display":"block"});

				uiCloneTemplate.data(item);

				uiCloneTemplate.find(".display-product-sku-name").html("SKU# "+item.sku+" - "+item.name);
				uiCloneTemplate.find(".display-product-aging-days").html(item.product_shelf_life);
				uiCloneTemplate.find(".display-product-image").attr("src", item.image);

				

				if(uiAddInputVesselName.val().trim() == "Vessel")
				{
					uiCloneTemplate.find(".for_bag_set").hide();
					uiCloneTemplate.find(".bulk_set").attr("id", "bulk"+sWord);
					uiCloneTemplate.find(".bulk_set_for").attr("for", "bulk"+sWord);

					uiCloneTemplate.find(".bulk_set").attr("name", "bag-or-bulk"+sWord);

					uiCloneTemplate.find(".bulk_set").attr("checked", true);
					uiCloneTemplate.find(".input-bag-qty").attr("disabled", true);
					uiCloneTemplate.find(".consignee-dist-bag-qty").attr("disabled", true);
				}else{
					uiCloneTemplate.find(".bulk_set").attr("name", "bag-or-bulk"+sWord);
					uiCloneTemplate.find(".bulk_set").attr("id", "bulk"+sWord);
					uiCloneTemplate.find(".bulk_set_for").attr("for", "bulk"+sWord);
				

					uiCloneTemplate.find(".bag_set").attr("id", "bag"+sWord);
					uiCloneTemplate.find(".bag_set_for").attr("for", "bag"+sWord);
					uiCloneTemplate.find(".bag_set").attr("name", "bag-or-bulk"+sWord);

					uiCloneTemplate.find(".bag_set").attr("checked", true);

				}

				var oConsigneeNameRecords = oConsigneeName,
					uiSecondContainer = uiCloneTemplate.find(".display-more-consign-records"),
					uiSecondTemplate = uiSecondContainer.find(".display-more-consign-items");

					for(var x in oConsigneeNameRecords)
					{
						var consign = oConsigneeNameRecords[x],
							uiSecondCloned = uiSecondTemplate.clone();
							
							uiSecondCloned.css({"display":"block"});

							uiSecondCloned.removeClass("display-more-consign-items").show();

							var uiSelectConsignName = uiSecondCloned.find(".select-consignee-name"),
								uiParent = uiSelectConsignName.parent(),
								uiSibling = uiParent.find('.frm-custom-dropdown');

								uiSibling.remove();
								uiSelectConsignName.removeClass("frm-custom-dropdown-origin");


							if(oConsigneeName === 0){
								var sHtml ='<option value="">Consignee Found</option>';
								uiSelectConsignName.append(sHtml);
							}else{
								uiSelectConsignName.html("");
								for(var x in oConsigneeName)
								{
									sHtml = '<option value="'+oConsigneeName[x]['id']+'">'+oConsigneeName[x]['name']+'</option>';
									uiSelectConsignName.append(sHtml);
								}

							}

							uiSelectConsignName.transformDD();

							CDropDownRetract.retractDropdown(uiSelectConsignName);
							
							



					}

					uiSecondContainer.append(uiSecondCloned);

					



				uiContainer.append(uiCloneTemplate);

				_buildUnitMeasureDropDown(uiCloneTemplate);

				_buildConsigneeDropDown(uiCloneTemplate);

				_buildMoreConsigneeDistribution(uiCloneTemplate);

				_removeSelectedProduct(uiCloneTemplate);

				_removeSelectedConsignee(uiCloneTemplate);

				_convertQtyToReceived(uiCloneTemplate);

				_numberOnlyQuantity();

				_triggerUnitMeasure(uiCloneTemplate);


			}
		}
	}

	/**
    * _numberOnlyQuantity
    * @description This function is for inputting numbers only
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _numberOnlyQuantity()
	{
		var uiNumbers = $(".number-only");

		uiNumbers.number(true, 0);

		// uiNumbers.off("keypress.numberOnly").on("keypress.numberOnly", function(){
			
		// });
	}


	/**
    * _triggerUnitMeasure
    * @description This function is for diplaying current unit of measure
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _triggerUnitMeasure(ui)
	{
		var uiContainer = ui.closest(".product-selected"),
			uiMeasure = uiContainer.find(".unit-of-measure"),
			sSelected = uiMeasure.find("option:selected").text(),
			uiLabel = uiContainer.find(".display-unit-measure-label"),
			sMeasurement = '';
			
			uiLabel.html(sSelected.toUpperCase());

			uiMeasure.off("change.measureGet").on("change.measureGet", function(){
				var uiThis = $(this),
					sMeasured = uiThis.find("option:selected").text();
					
					uiLabel.html(sMeasured.toUpperCase());
					
			});

	}



	/**
    * _convertQtyToReceived
    * @description This function is for converting quantity
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _convertQtyToReceived(ui)
	{
		var uiParent = ui.closest(".product-selected"),
			uiQty = uiParent.find(".input-bulk-qty"),
			uiUnitMeasure = uiParent.find(".unit-of-measure"),
			sOldQty = uiUnitMeasure.find("option:selected").text(),
			sGetOldQty = '';

			sGetOldQty = sOldQty;

			uiUnitMeasure.off("change.convertQty").on("change.convertQty", function(){
				var uiThis = $(this),
					sUnit = uiThis.find("option:selected").text(),
					iNewQty = _unitConverter(sOldQty, sUnit, uiQty.val());

					sGetOldQty = sUnit;

					uiQty.val(iNewQty);

			});


	}


	/**
    * _removeSelectedConsignee
    * @description This function is for removing selected consignee
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _removeSelectedConsignee(ui)
	{
		var uiDelete = ui.find(".deleteProductConsignee");
			
			uiDelete.off("click.removeConsign").on("click.removeConsign", function(){
				var uiThis = $(this),
					uiParent = uiThis.closest(".consignee-items"),
					uiContainer = uiThis.closest(".display-more-consign-records"),
					icount = 1;

					uiParent.remove();

					var uiTemplate = uiContainer.find(".consignee-items:not(.display-more-consign-items)");

						$.each(uiTemplate, function(){
							var tempThis = $(this),
								uiCountTemp = tempThis.find(".display-count-records").html(icount);
								icount++;
						});

						_buildMoreConsigneeDistribution(ui);


			});
			
	}
	

	/**
    * _removeSelectedProduct
    * @description This function is for removing selected product
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _removeSelectedProduct(ui) {

		var btnRemove = ui.find(".deleteProd");
			
			btnRemove.off("click.deleteProd").on("click.deleteProd", function(){
				var uiThis = $(this),
					uiParent = uiThis.closest(".product-selected"),
					iProdID = uiParent.data("id");
					
					$("body").css({overflow:'hidden'});

					$("#trashModal").addClass("showed");

					$("#trashModal .close-me").on("click",function(){
						$("body").css({'overflow-y':'initial'});
						$("#trashModal").removeClass("showed");
					});

					var btnConfirm = $("#confirmDelete");

					btnConfirm.off("click.confirmProd").on("click.confirmProd", function(){

						var oItems = [];

						for(var i in  oProducts)
						{
							oItems.push(oProducts[i]);
						}

						uiParent.remove();

						$.each(oItems, function(i, el){
							if (this.id == iProdID){
								oItems.splice(i, 1);
							}
						});

						$(".modal-close.close-me").trigger("click");

						var options = {
						  form : "box",
						  autoShow : true,
						  type : "success",
						  title  : "Delete Product",
						  message : "Successfully Deleted",
						  speed : "slow",
						  withShadow : true
						}

						$("body").feedback(options);

						
							
					});

			});
	 
	}



	/**
    * _shuffelWord
    * @description This function is for shuffle words
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _shuffelWord(word) {
	  var shuffledWord = '';
	  var charIndex = 0;
	  word = word.split('');
	  while (word.length > 0) {
	    charIndex = word.length * Math.random() << 0;
	    shuffledWord += word[charIndex];
	    word.splice(charIndex, 1);
	  }
	  return shuffledWord;
	}


		/**
    * _buildMoreConsigneeDropDown
    * @description This function is for display dropdowns for Consignee names
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _buildConsigneeDropDown(ui)
	{
		// console.log(JSON.stringify(oConsigneeName));

			var uiSelectConsignName = ui.find(".select-consignee-name"),
				uiParent = uiSelectConsignName.parent(),
				uiSibling = uiParent.find('.frm-custom-dropdown');

				uiSibling.remove();
				uiSelectConsignName.removeClass("frm-custom-dropdown-origin");


			if(oConsigneeName === 0){
				var sHtml ='<option value="">Consignee Found</option>';
				uiSelectConsignName.append(sHtml);
			}else{
				for(var x in oConsigneeName)
				{
					sHtml = '<option value="'+oConsigneeName[x]['id']+'">'+oConsigneeName[x]['name']+'</option>';
					uiSelectConsignName.append(sHtml);
				}

			}

			uiSelectConsignName.transformDD();

			CDropDownRetract.retractDropdown(uiSelectConsignName);



	}


	/**
    * _buildMoreConsigneeDropDown
    * @description This function is for display dropdowns for More Consignee names
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _buildMoreConsigneeDistribution(ui)
	{
		// console.log(JSON.stringify(oConsigneeName));

		
		
	
		var uiAddInputVesselName = $(".mode-of-deliver").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt"),
			uiContainer = ui.find(".display-more-consign-records"),
			uiTemplate = uiContainer.find(".display-more-consign-items"),
			uiFirstConsign = ui.find(".first-consign-name option:selected"),
			btnAdd = ui.find(".add-more-consignee-names");
			
			btnAdd.off("click.addConsign").on("click.addConsign", function(e){
				e.preventDefault();
				var btnThis = $(this),
					oConsigneeNameRecords = [];


					for(var i in oConsigneeName)
					{
						oConsigneeNameRecords.push(oConsigneeName[i]);
					}				

				if(Object.keys(oConsigneeNameRecords).length > 0){
				
					var uiConsignContent = uiContainer.find(".consignee-items:not(.display-more-consign-items)").find(".select-consignee-name option:selected"),
						iCount = uiContainer.find(".consignee-items:not(.display-more-consign-items)").length,
						iRealCount = 1 + iCount;




						if(iCount == 0)
						{
							$.each(oConsigneeNameRecords, function(i, el){
								if (this.id == uiFirstConsign.val()){
								   oConsigneeNameRecords.splice(i, 1);

								   // console.log(JSON.stringify(oConsigneeNameRecords));

								}
							});
						}else{

							$.each(uiConsignContent, function(){
								var uiThis = $(this).val();

								$.each(oConsigneeNameRecords, function(i, el){
									if (this.id == uiThis){
									   oConsigneeNameRecords.splice(i, 1);

										// console.log(JSON.stringify(oConsigneeNameRecords));

									}
								});

							});

						}

					var uiCloneTemplate = uiTemplate.clone();

						uiCloneTemplate.removeClass("display-more-consign-items").show();
						uiCloneTemplate.css({"diplay":"block"});



					var uiSelectConsignName = uiCloneTemplate.find(".select-consignee-name"),
						uiParent = uiSelectConsignName.parent(),
						uiSibling = uiParent.find('.frm-custom-dropdown');

						uiSibling.remove();
						uiSelectConsignName.removeClass("frm-custom-dropdown-origin");


						uiCloneTemplate.find(".display-count-records").html(iRealCount+".");

						if(uiAddInputVesselName.val() == 'Vessel')
						{
							uiCloneTemplate.find(".consignee-dist-bag-qty").attr("disabled", true);
						}



					if(oConsigneeNameRecords === 0){
						var sHtml ='<option value="">Consignee Found</option>';
						uiSelectConsignName.append(sHtml);
					}else{
						uiSelectConsignName.html("");
						for(var x in oConsigneeNameRecords)
						{
							var sHtml = '<option value="'+oConsigneeNameRecords[x]['id']+'">'+oConsigneeNameRecords[x]['name']+'</option>';
							uiSelectConsignName.append(sHtml);
						}

					}

					uiSelectConsignName.transformDD();

					uiContainer.append(uiCloneTemplate);

					_removeSelectedConsignee(uiCloneTemplate);

					CDropDownRetract.retractDropdown(uiSelectConsignName);

					_numberOnlyQuantity();

					_triggerUnitMeasure(uiCloneTemplate);

				}else{
					btnThis.hide();
				}

			});
			
		
	}

	/**
    * _buildMoreConsigneeDropDown
    * @description This function is for display dropdowns for More Consignee names
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _createCancelConfirmation()
	{
		var cancelBtn = $(".cancel-creating-record");

		cancelBtn.off('click.cancelCreating').on('click.cancelCreating', function(){

			$("body").css({overflow:'hidden'});
				
			$("#cancelModal").addClass("showed");

			$("#cancelModal .close-me").on("click",function(){
				$("body").css({'overflow-y':'initial'});
				$("#cancelModal").removeClass("showed");
			});

			var uiConfirmCancelBtn = $("#confirmCancel");

			 uiConfirmCancelBtn.off('click.confirmedcancel').on('click.confirmedcancel', function(){
			 	window.location.href = BASEURL+"receiving/consignee_receiving";
			 });

		});
	}


	/**
    * _documentEvents
    * @description This function is for Document Events
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _documentEvents(bTemporary)
	{
		var btnUploadDocs =  $('[modal-target="upload-documents"]'),
			oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "setCurrentDisplayReceivedId"}),
			receivingId = 0;

			if(oRecord !== null){

				receivingId = oRecord.value
			}else{
				receivingId = 0;
			}

		btnUploadDocs.off('click');


		var extraData = { "receiving_id" : receivingId };

		if(bTemporary === true){
			extraData["temporary"] = true;
		}

		btnUploadDocs.cr8vFileUpload({
			url : BASEURL + "receiving/upload_document_consignee",
			accept : "pdf,csv",
			filename : "attachment",
			extraField : extraData,
			onSelected : function(bIsValid){
				if(bIsValid){
					//selected valid
				}
			},
			success : function(oResponse){
				if(oResponse.status){
					$('[cr8v-file-upload="modal"] .close-me').click();
					var oDocs = oResponse.data[0]["documents"];
					if(bTemporary){
						oUploadedDocuments[Object.keys(oUploadedDocuments).length] = oResponse.data[0]["documents"][0];
						_displayDocuments(oUploadedDocuments);
					}else{
						_displayDocuments(oDocs);
					}

					// 	console.log(JSON.stringify(oResponse.status));
				}
			},
			error : function(error){
				console.log(error);
				$("body").feedback({title : "Message", message : "Was not able to upload, file maybe too large", type : "danger", icon : failIcon});
			}
		});

		// console.log("documents in");
	}

	/**
    * _displayDocuments
    * @description This function is for Displaying Documents
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _displayDocuments(oDocs)
	{
		// console.log(JSON.stringify(oDocs));

		var bChangeBg = false,
			iCountDocument = 0;

		var uiContainer = $("#displayDocuments"),
			sTemplate = '<div class="table-content position-rel tbl-dark-color">'+
							'<div class="content-show padding-all-10">'+
								'<div class="width-85per display-inline-mid padding-left-10">'+
									'<i class="fa font-30 display-inline-mid width-50px icon"></i>'+
									'<p class=" display-inline-mid doc-name">Document Name</p>'+
								'</div>'+
								'<p class=" display-inline-mid date-time"></p>'+
							'</div>'+
							'<div class="content-hide" style="height: 50px;">'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30 view-document">View</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid download-document">'+
									'<button class="btn general-btn">Download</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30">Print</button>'+
								'</a>'+
							'</div>'+
						'</div>';

		uiContainer.html("");
		for(var i in oDocs){
			var uiList = $(sTemplate),
				ext = (oDocs[i]["document_path"].substr(oDocs[i]["document_path"].lastIndexOf('.') + 1)).toLowerCase();

			if(ext == 'csv'){
				uiList.find(".icon").addClass('fa-file-excel-o');
			}else if(ext == 'pdf'){
				uiList.find(".icon").addClass('fa-file-pdf-o');
			}

			uiList.data(oDocs[i]);
			uiList.find(".doc-name").html(oDocs[i]["document_name"]);
			uiList.find(".date-time").html(oDocs[i]["date_added_formatted"]);
			if(bChangeBg){
				uiList.removeClass('tbl-dark-color');
				bChangeBg = false;
			}else{
				bChangeBg = true;
			}


			uiContainer.append(uiList);
			
		}



		cr8v_platform.localStorage.set_local_storage({
			name : "dataUploadedDocument",
			data : oDocs
		});

		_downloadDocument(oDocs);

	}


	/**
    * _downloadDocument
    * @description This function is for Downloading Documents
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _downloadDocument(oDocs)
	{
		
		var	btnDownloadDocument = $(".download-document"),
			btnViewDocument = $(".view-document");


		btnDownloadDocument.off("mouseenter.download").on("mouseenter.download", function(e){
			e.preventDefault();
			var uiDownloadThis = $(this),
				uiParentContainer = uiDownloadThis.closest('.table-content.position-rel'),
				uiGetDataPath = uiParentContainer.data("document_path"),
				uiGetDataName = uiParentContainer.data("document_name");

				uiDownloadThis.attr("href", uiGetDataPath);
				uiDownloadThis.attr("download", uiGetDataName);

		});


		btnViewDocument.off("click.viewDocument").on("click.viewDocument", function(e){
			e.preventDefault();
			var uiParentContainer = $(this).closest('.table-content.position-rel'),
				uiGetDataPath = uiParentContainer.data("document_path");

				window.open(uiGetDataPath, '_blank');
		});
	}
	

	/**
    * _notesEvent
    * @description This function is for Notes Creating
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _notesEvent()
	{
		var uiBtn = $("#AddReceivingConsigneeNote");

			uiBtn.off("click");

			uiBtn.off("click.addNotes").on("click.addNotes", function(){

				$("body").css({overflow:'hidden'});
					
				$('[modal-id="add-notes"]').addClass("showed");

				$('[modal-id="add-notes"] .close-me').on("click",function(){
					$("body").css({'overflow-y':'initial'});
					$('[modal-id="add-notes"]').removeClass("showed");
				});

				var uiSubject = $("#AddReceivingConsigneeNoteSubject"),
					uiMessage = $("#AddReceivingConsigneeNoteMessage"),
					btnSave = $("#addRecievingConsigneeNotesSave"),
					oError = [],
					sDateGet = "",
					sDateFormatGet = "";

					var setCurrentInterval = setInterval(function(){
						var oGetDateTime = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDateTime"});
						if(oGetDateTime !== null)
						{
							sDateGet = oGetDateTime.date;
							sDateFormatGet = oGetDateTime.dateFormat;
							clearInterval(setCurrentInterval);
						}
					}, 300);

					uiSubject.val("");
					uiMessage.val("");

					btnSave.off("click.saveNotes").on("click.saveNotes", function(){
						var oSetOdata = {},
							oGatheredData = [],
							iCount = 1;

						(uiSubject.val().trim() == "") ? oError.push("Subject is Required.") : "";
						(uiMessage.val().trim() == "") ? oError.push("Message is Required.") : "";

						if(oError.length > 0)
						{
							$("body").feedback({title : "Add Batch", message : oError.join("<br />"), type : "danger", icon : failIcon});
						}else{
							
							if(oCreateNotes == '')
					    	{
					    		 oSetOdata = {
					    			id : iCount,
					    			userId: iConstUserId,
					    			subject : uiSubject.val().trim(),
						    		message : uiMessage.val().trim(),
						    		datetime : sDateGet,
						    		displayDate : sDateFormatGet
						    	}

						    	oGatheredData.push(oSetOdata);
						    	
					    	}
					    	else
					    	{
					    		for(var x in oCreateNotes)
					    		{
					    			oSetOdata = {
					    				id : iCount,
					    				userId: iConstUserId,
							    		subject : oCreateNotes[x]["subject"],
							    		message : oCreateNotes[x]["message"],
							    		datetime : oCreateNotes[x]["datetime"],
							    		displayDate :oCreateNotes[x]["displayDate"]
							    	}
							    	oGatheredData.push(oSetOdata);
							    	iCount++;
					    		}

					    		oSetOdata = {
					    			id : iCount,
					    			userId: iConstUserId,
					    			subject : uiSubject.val().trim(),
						    		message : uiMessage.val().trim(),
						    		datetime : sDateGet,
						    		displayDate : sDateFormatGet
						    	}

						    	oGatheredData.push(oSetOdata);

					    	}

					    	oCreateNotes = oGatheredData;

					    	$(".modal-close.close-me").trigger("click");


					    	_displayReceivingConsigneeNotes(oCreateNotes);
						}

					});
				

			});

	}

	/**
    * _displayReceivingConsigneeNotes
    * @description This function is for Displaying Notes 
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _displayReceivingConsigneeNotes(oData)
	{
		var uiAddReceivingConsigneeNotesDisplay = $("#AddReceivingConsigneeNotesDisplay");

			uiAddReceivingConsigneeNotesDisplay.html("");

			for(var x in oData)
			{

				var sHtml = '<div class="border-full padding-all-10 margin-left-18 margin-top-10">'+
								'<div class="border-bottom-small border-gray padding-bottom-10">'+
									'<p class="f-left font-14 font-400">Subject: '+oData[x]["subject"]+'</p>'+
									'<p class="f-right font-14 font-400">Date/Time: '+oData[x]["displayDate"]+'</p>'+
									'<div class="clear"></div>'+
								'</div>'+
								'<p class="font-14 font-400 no-padding-left padding-all-10">'+oData[x]["message"]+'</p>'+
								// '<a href="" class="f-right padding-all-10 font-400">Show More</a>'+
								'<div class="clear"></div>'+
							'</div>';
					var oSetHtmlData = $(sHtml);
						oSetHtmlData.data({
							"notesId":oData[x]["id"]
						});
						uiAddReceivingConsigneeNotesDisplay.append(oSetHtmlData);
			}
	}
	
	/**
    * _unitConverter
    * @description This function is for conveting units
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _unitConverter(iFrom, iTo, iQuantity)
	{
		iresult = UnitConverter.convert(iFrom, iTo, iQuantity);
		return iresult;
	}

	/**
    * _checkCreatingReceivingConsignee
    * @description This function is for conveting units
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _checkCreatingReceivingConsignee()
	{
		var btnConfirm = $(".confirmSaveNewConsigneeGoods");


			btnConfirm.off("click.check").on("click.check", function(){
				

				 $("body").css({overflow:'hidden'});
					
				$("#confirmCreateRecord").addClass("showed");

				$("#confirmCreateRecord .close-me").on("click",function(){
					$("body").css({'overflow-y':'initial'});
					$("#confirmCreateRecord").removeClass("showed");
				});

				var btnSave = $("#confirmCreateRecord").find(".addSaveNewConsigneeGoods");

					btnSave.off("click.saveIt").on("click.saveIt", function(){
						 var oError = [],
				 	 		iProdList = $("#displayReceivingConsigneeProductSelects").find(".product-selected:not(.product_selected_receive_consign)").length,
				 	 		uiProductContainer = $("#displayReceivingConsigneeProductSelects").find(".product-selected:not(.product_selected_receive_consign)"),
				 	 		uiVesselName = $(".mode-of-deliver").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt"),
				 	 		sAddInputPoNumber = $("#addInputPoNumber"),
							sAddInputVesselName = $("#addInputVesselName"),
							sAddInputVesselHatches = $("#addInputVesselHatches"),
							sAddInputVesselType = $("#addInputVesselType"),
							sAddInputVesselCaptain = $("#addInputVesselCaptain"),
							sAddInputVesselDischargeType = $("#addInputVesselDischargeType"),
							sAddInputVesselLandingVolume = $("#addInputVesselLandingVolume"),
							sAddInputVesselBerthingDate = $("#addInputVesselBerthingDate"),
							sAddInputVesselTransferTime = $("#addInputVesselTransferTime"),
							sAddInputTruckOrigin = $("#addInputTruckOrigin"),
							saddInputTruckings = $("#addInputTruckings"),
							sAddInputTruckPlateNumber = $("#addInputTruckPlateNumber"),
							sAddInputTruckDriverName = $("#addInputTruckDriverName"),
							saddInputTruckLicenseNum= $("#addInputTruckLicenseNum"),
							iBillLadingVolume = $("#bill-lading-volume-measure"),
							oCollectModeOfDelivery = {},
							iSetReceivingNumber = $("#setReceivingNumber").val();
				 	 		iCountVendors = 0,
				 	 		iCountQty = 0,
				 	 		iCountConsigneeQty = 0,
				 	 		iCountBag = 0,
				 	 		iCountConsigneeBag = 0,
				 	 		oStoreProducts = [],
				 	 		oConsigneeData = [];

				 	 		// console.log(iBillLadingVolume.val());

				 	 		// For Product Check
				 	 		$.each(uiProductContainer, function(){
				 	 			var uiThis = $(this),
				 	 				oItemData = uiThis.data(),
				 	 				uiLoadMethod = uiThis.find(".set-bulk-bag-radio:checked").val(),
				 	 				uiUnitMeasure = uiThis.find(".unit-of-measure option:selected").val(),
				 	 				uiItemName = uiThis.find(".display-product-sku-name").text(),
				 	 				uiVendor = uiThis.find(".receive-consignee-vendor-item").val().trim(),
				 	 				uiQty = uiThis.find(".input-bulk-qty").val().trim(),
				 	 				uiBag = uiThis.find(".input-bag-qty").val().trim(),
				 	 				uiConsigneeContainer = uiThis.find(".display-more-consign-records"),
				 	 				uiContains = uiConsigneeContainer.find(".consignee-items:not(.display-more-consign-items)"),
				 	 				icountAllConsignQty = 0,
				 	 				icountAllConsignBag = 0;

				 	 				// console.log(uiUnitMeasure);

				 	 				// For consignee Check
				 	 				$.each(uiContains, function(){
				 	 					var uiConsigneeThis = $(this),
				 	 						uiConsigneeName = uiThis.find(".unit-of-measure option:selected").val(),
				 	 						uiConsigneeQty = uiConsigneeThis.find(".set-consignee-dist-qty").val().trim(),
				 	 						uiConsigneeBag = uiConsigneeThis.find(".consignee-dist-bag-qty").val().trim();



				 	 						(uiConsigneeQty == "") ? iCountConsigneeQty++ : "";

				 	 						if(uiVesselName.val().trim() == "Truck")
											{
												(uiConsigneeBag == "") ? iCountConsigneeBag++ : "";

												icountAllConsignBag += parseFloat(uiConsigneeBag);

											}

											//console.log(uiConsigneeBag);

											icountAllConsignQty += parseFloat(uiConsigneeQty);


											if(uiVesselName.val().trim() == "Vessel")
											{
												var oGatheredDataConsignee = {
														product_id : oItemData.id,
														consignee_id : uiConsigneeName,
														distribution_qty : uiConsigneeQty,
														unit_of_measure_id : uiUnitMeasure,
														bag : "",
														loading_method : uiLoadMethod
												}
												oConsigneeData.push(oGatheredDataConsignee);

											}else{
												var oGatheredDataConsignee = {
														product_id : oItemData.id,
														consignee_id : uiConsigneeName,
														distribution_qty : uiConsigneeQty,
														unit_of_measure_id : uiUnitMeasure,
														bag : uiConsigneeBag,
														loading_method : uiLoadMethod
												}
												oConsigneeData.push(oGatheredDataConsignee);

											}


											

				 	 				});





				 	 				(uiVendor == "") ? iCountVendors++ : "";

				 	 				(uiQty == "") ? iCountQty++ : "";

				 	 				if(uiVesselName.val().trim() == "Truck")
									{
										(uiBag == "") ? iCountBag++ : "";
										// iCountMainConsignBag = icountAllConsignBag;

										if(parseFloat(uiBag) != icountAllConsignBag)
										{
											oError.push("Consignee Distribution Bags is not equal to item Bags in "+uiItemName) ;
										}

										//console.log(parseFloat(uiBag));
										//console.log(parseFloat(icountAllConsignBag));
									}


									if(parseFloat(uiQty) != icountAllConsignQty)
									{
										oError.push("Consignee Distribution quantity is not equal to item quantity in "+uiItemName) ;
									}

									if(uiVesselName.val().trim() == "Vessel")
									{
										var oGatheredData = {
												product_id : oItemData.id,
												vendor : uiVendor,
												loading_method : uiLoadMethod,
												unit_of_measure : uiUnitMeasure,
												qty_to_receive : uiQty,
												bag : "",
												unit_bill_volume : iBillLadingVolume.val()

										}
										oStoreProducts.push(oGatheredData);

									}else{
										var oGatheredData = {
												product_id : oItemData.id,
												vendor : uiVendor,
												loading_method : uiLoadMethod,
												unit_of_measure : uiUnitMeasure,
												qty_to_receive : uiQty,
												bag : uiBag,
												unit_bill_volume : ""
										}
										oStoreProducts.push(oGatheredData);

									}



									

									

				 	 		});

				 	 		
				 	 		(iProdList == 0) ? oError.push("Add Product Item First") : "";
				 	 		(iCountVendors != 0) ? oError.push("Product Vendor is Required") : "";
				 	 		(iCountQty != 0) ? oError.push("Quantity to Receive is Required") : "";
				 	 		(iCountConsigneeQty != 0) ? oError.push("Consignee Quantity to Receive is Required") : "";

				 	 		(iCountBag != 0) ? oError.push("Bag Quantity to Receive is Required") : "";
				 	 		(iCountConsigneeBag != 0) ? oError.push("Consignee Bag Quantity to Receive is Required") : "";


				 	 		if(uiVesselName.val().trim() == "Vessel")
							{
								sAddInputVesselName.attr("datavalid", "required");
								sAddInputVesselHatches.attr("datavalid", "required");
								sAddInputVesselType.attr("datavalid", "required");
								sAddInputVesselCaptain.attr("datavalid", "required");
								sAddInputVesselDischargeType.attr("datavalid", "required");
								sAddInputVesselLandingVolume.attr("datavalid", "required");
								// sAddInputVesselBerthingDate.attr("datavalid", "required");
								// sAddInputVesselTransferTime.attr("datavalid", "required");

								sAddInputTruckOrigin.attr("datavalid", false);
								saddInputTruckings.attr("datavalid", false);
								sAddInputTruckPlateNumber.attr("datavalid", false);
								sAddInputTruckDriverName.attr("datavalid", false);
								saddInputTruckLicenseNum.attr("datavalid", false);

								oCollectModeOfDelivery = {};

								oCollectModeOfDelivery = {
									modeType : uiVesselName.val().trim(),
									vesselName : sAddInputVesselName.val().trim(),
									hatches : sAddInputVesselHatches.val().trim(),
									vesselType : sAddInputVesselType.val().trim(),
									vesselCaptain : sAddInputVesselCaptain.val().trim(),
									dischargeType : sAddInputVesselDischargeType.val().trim(),
									loadingVolume : sAddInputVesselLandingVolume.val().trim(),
									berthingDate : sAddInputVesselBerthingDate.val().trim(),
									transferTime : sAddInputVesselTransferTime.val().trim()
								}

								oModeOfDelivery = oCollectModeOfDelivery;

								// cr8v_platform.localStorage.set_local_storage({
								// 	name: "modeOfDelivery",
								// 	data : oCollectModeOfDelivery
								// });


								
								(sAddInputVesselName.val().trim() == "") ?  oError.push("Vessel Name is Required") : "";
								(sAddInputVesselHatches.val().trim() == "") ?  oError.push("Vessel Hatches is Required") : "";
								(sAddInputVesselType.val().trim() == "") ?  oError.push("Vessel Type is Required") : "";
								(sAddInputVesselCaptain.val().trim() == "") ?  oError.push("Vessel Captain is Required") : "";
								(sAddInputVesselDischargeType.val().trim() == "") ?  oError.push("Vessel Discharge Type is Required") : "";
								(sAddInputVesselLandingVolume.val().trim() == "") ?  oError.push("Vessel Bill of Landing Volume is Required") : "";
								// (sAddInputVesselBerthingDate.val().trim() == "") ?  oError.push("Vessel Berthing Date is Required") : "";
								// (sAddInputVesselTransferTime.val().trim() == "") ?  oError.push("Vessel Berthing Time is Required") : "";

							}else{

								sAddInputTruckOrigin.attr("datavalid", "required");
								saddInputTruckings.attr("datavalid", "required");
								sAddInputTruckPlateNumber.attr("datavalid", "required");
								sAddInputTruckDriverName.attr("datavalid", "required");
								saddInputTruckLicenseNum.attr("datavalid", "required");

								sAddInputVesselName.attr("datavalid", false);
								sAddInputVesselHatches.attr("datavalid", false);
								sAddInputVesselType.attr("datavalid", false);
								sAddInputVesselCaptain.attr("datavalid", false);
								sAddInputVesselDischargeType.attr("datavalid", false);
								sAddInputVesselLandingVolume.attr("datavalid", false);
								// sAddInputVesselBerthingDate.attr("datavalid", false);
								// sAddInputVesselTransferTime.attr("datavalid", false);

								oCollectModeOfDelivery = {};

								oCollectModeOfDelivery = {
									modeType : uiVesselName.val().trim(),
									vesselOrigin : sAddInputTruckOrigin.val().trim(),
									trucking : saddInputTruckings.val().trim(),
									plateNumber : sAddInputTruckPlateNumber.val().trim(),
									driverName : sAddInputTruckDriverName.val().trim(),
									licenseNum : saddInputTruckLicenseNum.val().trim()
								}

								oModeOfDelivery = oCollectModeOfDelivery;

								// cr8v_platform.localStorage.set_local_storage({
								// 	name: "modeOfDelivery",
								// 	data : oCollectModeOfDelivery
								// });

								
								(sAddInputTruckOrigin.val().trim() == "") ?  oError.push("Vessel Origin is Required") : "";
								(saddInputTruckings.val().trim() == "") ? oError.push("Trucking is Required") : "";
								(sAddInputTruckPlateNumber.val().trim() == "") ?  oError.push("Truck Plate Number is Required") : "";
								(sAddInputTruckDriverName.val().trim() == "") ?  oError.push("Truck Driver Name is Required") : "";
								(saddInputTruckLicenseNum.val().trim() == "") ?  oError.push("License Number is Required") : "";


							}

							var oDocument = cr8v_platform.localStorage.get_local_storage({name:"dataUploadedDocument"})


							


				 	 		if(oError.length > 0)
							{
								$("body").feedback({title : "Receiving Consignee", message : oError.join("<br />"), type : "danger", icon : failIcon});
							}else{
								//alert("OK");

								var uiDisplayReceiveConsigneeGoodsId = $("#displayReceiveConsigneeGoodsId"),
									uiCreateLinkToGo = $("#create-link-to-go");

									
									uiDisplayReceiveConsigneeGoodsId.text("Receiving No. "+iSetReceivingNumber+" has been created.");

								
							uiCreateLinkToGo.attr("href", BASEURL+"receiving/consignee_ongoing");
// 							if(uiVesselName.val().trim() == "Vessel")
// 							{
// 								uiCreateLinkToGo.attr("href", BASEURL+"receiving/consignee_ongoing_vessel");
// 							}else if(uiVesselName.val().trim() == "Truck")
// 							{
// 								uiCreateLinkToGo.attr("href", BASEURL+"receiving/consignee_ongoing_truck");
// 							}


								var oData = {
									receiving_number : iSetReceivingNumber,
									purchase_order : sAddInputPoNumber.val().trim(),
									mode_of_delivery : uiVesselName.val().trim(),
									product_data : oStoreProducts,
									note_data : oCreateNotes,
									delivery_mode_data : oModeOfDelivery,
									consignee_items : oConsigneeData,
									uploaded_documents : oDocument

								}

								_add(oData);
							}

					});

			});

	}

	/**
    * _displaySelectedReceivingConsignee
    * @description This function is for displaying selected record
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _displaySelectedReceivingConsignee(oData)
	{

		// console.log(JSON.stringify(oData));
		var uiDisplaySelectedReceiveNumber = $("#displaySelectedReceiveNumber"),
			uiDisplaySelectedPONum = $("#displaySelectedPONum"),
			uiDisplaySelectedDeliveryOrigin = $("#displaySelectedOrigin"),
			uiDisplaySelectedDateIssued = $("#displaySelectedDateIssued"),
			uiDisplaySelectedVesselName = $("#display-selected-vessel-name"),
			uiDisplaySelectedVesselType = $("#display-selected-vessel-type"),
			uiDisplaySelectedVesselCaptain = $("#display-selected-vessel-captain"),
			uiDisplaySelectedVesselHatches = $("#display-selected-vessel-hatches"),
			uiDisplaySelectedVesselDischargeType = $("#display-selected-vessel-discharge-type"),
			uiDisplaySelectedVesselBillLoadingVol = $("#display-selected-vessel-bill-loading-volume"),
			uiDisplaySelectedVesselBertingTimeStart = $("#display-selected-vessel-berthing-time-start"),
			uiDisplaySelectedVesselBertingDateStart = $("#display-selected-vessel-berthing-date-start"),
			uiDisplaySelectedVesselBertingTime = $("#display-selected-vessel-berthing-time"),
			uiDisplaySelectedVesselBertingTitle = $("#display-selected-vessel-berthing-title"),
			uiModeDelivery = $("#displaySelectedOrigin");

			

		var uiDisplaySelectedVesselOrigin = $("#display-selected-vessel-name-truck"),
			uiDisplaySelectedVesselTrucking = $("#display-selected-trucking"),
			uiDisplaySelectedVesselDriversName= $("#display-selected-driver-name"),
			uiDisplaySelectedVesselLicense = $("#display-selected-license-name"),
			uiDisplaySelectedVesselPlateNumber = $("#display-selected-plate-number"),
			uiIfVessel = $("#delivery-for-vessel"),
			uiIfTruck = $("#delivery-for-truck");


			uiDisplaySelectedReceiveNumber.html("Receiving No. "+oData["receiving_number"]);
			uiDisplaySelectedPONum.html(oData["purchase_order"]);
			uiDisplaySelectedDateIssued.html(oData["date_issued"]);
			uiModeDelivery.html(oData["origin"]);

			if(oData["origin"] == 'Vessel')
			{
				uiIfVessel.css({"display":"block"});

				
				uiDisplaySelectedVesselName.html(oData["vessel_name"]);
				uiDisplaySelectedVesselType.html(oData["vessel_type"]);
				uiDisplaySelectedVesselCaptain.html(oData["vessel_captain"]);
				uiDisplaySelectedVesselHatches.html(oData["hatches"]);
				uiDisplaySelectedVesselDischargeType.html(oData["discharge_type"]);
				uiDisplaySelectedVesselBillLoadingVol.html(oData["bill_of_landing_volume"]+" "+oData["bill_measure"].toUpperCase());
				uiDisplaySelectedVesselBertingTime.html(oData["berthing_time"]);
				uiDisplaySelectedVesselBertingTimeStart.val(oData["berthing_time_set"]);
				uiDisplaySelectedVesselBertingDateStart.val(oData["berthing_time_date"]);

				if(oData["bill_of_landing_volume"] != "")
				{
					uiDisplaySelectedVesselBertingTime.removeClass("gray-color");
					uiDisplaySelectedVesselBertingTitle.removeClass("gray-color");
				}


			}else if(oData["origin"] == 'Truck'){ 
				uiIfTruck.css({"display":"block"});

				uiDisplaySelectedVesselOrigin.html(oData["vessel_name"]);
				uiDisplaySelectedVesselTrucking.html(oData["trucking"]);
				uiDisplaySelectedVesselDriversName.html(oData["driver_name"]);
				uiDisplaySelectedVesselLicense.html(oData["license_name"]);
				uiDisplaySelectedVesselPlateNumber.html(oData["plate_number"]);
			}


			_displayProductItems(oData["product_info"]);

			_dispalyReceivingNotes(oData["receiving_notes"]);

			_displayDocuments(oData["receiving_documents"]);

	}


	/**
    * _displayProductItems
    * @description This function is for displaying Products ongoing
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _displayProductItems(oItems)
	{

		// console.log(JSON.stringify(oItems));
		var uiContainer = $("#displayProductItemList"),
			uiTemplate = $('.item-template-product'),
			iCountComplete = 0,
			iCountCheck = 0;

			uiContainer.html("");

		for(var i in oItems){
			var item = oItems[i];
			
			var uiCloneTemplate = uiTemplate.clone();
			uiCloneTemplate.removeClass("item-template-product").show();
			uiCloneTemplate.data(item);
			uiCloneTemplate.find(".display-consignee-sku-name").html(" SKU #"+item.sku+" - "+item.product_name);
			uiCloneTemplate.find(".display-consignee-prod-img").css('background-image', 'url('+item.product_image+')');
			uiCloneTemplate.find(".display-consignee-vendor-name").html(item.vendor_name);
			uiCloneTemplate.find(".display-consignee-loading-method").html(item.loading_method);
			uiCloneTemplate.find(".display-consignee-qtr-to-receive").html(item.qty+" "+item.measures_name.toUpperCase()+" ("+item.bag+" Bags)").data({
				"quantity":item.qty,
				"unitOfMesurement":item.measures_name.toUpperCase()
			});

			uiCloneTemplate.attr("prod-id", item.product_inventory_id);


			_displayConsigneeDistribution(item.consignee_info, uiCloneTemplate);
			_displayEditConsigneeDistribution(item.consignee_info, uiCloneTemplate);

			
			uiContainer.append(uiCloneTemplate);
			

			_createCollapseProduct(uiCloneTemplate);
			_createEditProduct(oItems);
			_createCancelProduct();
			_createEnterProductDetails();
			_createEnterProductDetailsCancel();
			_displayProductInfoSelected(uiCloneTemplate);

			_addBatchForProduct();


			oItemReceivingBalance[item.product_inventory_id] = parseFloat(item.qty);
			 oBatchPerItem[item.product_inventory_id] = {};

			 if(item.hasOwnProperty("storage_assignment")){
				if(Object.keys(item.storage_assignment).length > 0){
					for(var x in item.storage_assignment){
						var storageAssignment = item.storage_assignment[x],
							batch = storageAssignment.batch;

						for(var n in batch){
							var recordBatch = oBatchPerItem[item.product_inventory_id],
							len = Object.keys(recordBatch).length,
							oArrData = batch[n]["location"];

							var lastArr = oArrData[oArrData.length - 1];
							// console.log(JSON.stringify(lastArr));
							
							oBatchPerItem[item.product_inventory_id][len] = {
								"location" : lastArr,
								"item_id" : item.product_id,
								"batch_name" : batch[n]["name"],
								"batch_amount" : batch[n]["quantity"],
								"uom" : item.measures_name,
								"uom_id" : item.unit_of_measure_id,
								"hierarchy" : batch[n]["location"]
							};
							
						}
					}

					_displayBatches(item.product_inventory_id);

					iCountComplete++;

				}
			}

			// console.log(item);
			if(Object.keys(oBatchPerItem[item.product_inventory_id]).length > 0){
				uiCloneTemplate.find(".gray-color").removeClass("gray-color");
			}



			if(item.hasOwnProperty("discrepancy")){
				if(Object.keys(item.discrepancy).length > 0){

					var oGatherDiscrepancyData = {
						received_quantity : item.discrepancy.received_quantity,
						discrepancy_type : item.discrepancy.type,
						discrepancy_value : item.discrepancy.discrepancy_quantity,
						discrepancy_reason : item.discrepancy.remarks,
						discrepancy_cause : item.discrepancy.cause,
						unit_of_measure : item.measures_name.toUpperCase()
					}




					_displayQuantityCheckSuccess(uiCloneTemplate, oGatherDiscrepancyData);

				}
			}

			iCountCheck++;


		}


		if(iCountComplete == iCountCheck)
		{
			$('.complete-receiving-record').attr('disabled',false);

			_addCompleteReceivingRecord();
			
		}else{
			$('.complete-receiving-record').attr('disabled',true);
		}


		_saveUpdateCosigneeProductDetails();
		_CheckOngoingReceivingConsignee()

	}

	/**
    * _displayConsigneeDistribution
    * @description This function is for displaying Consignee Distribution in ongoing
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _displayConsigneeDistribution(oConsigneeItems, uiCloneTemplate)
	{
			var uiConsigneeContainer = uiCloneTemplate.find(".display-consignee-distribution"),
				uiConsigneeTemplate = uiConsigneeContainer.find(".display-consignee-items");
			
			uiConsigneeContainer.html("");
			for(var x in oConsigneeItems)
			{
				
				oConsignee = oConsigneeItems[x];

				var uiCloneConsigneeTemplate = uiConsigneeTemplate.clone();
				uiCloneConsigneeTemplate.removeClass("display-consignee-items").show();
				uiCloneConsigneeTemplate.data(oConsignee);
				uiCloneConsigneeTemplate.find(".display-consignee-distribution-name").html(oConsignee.name);
				uiCloneConsigneeTemplate.find(".display-consignee-distribution-quantity").html(oConsignee.distribution_qty+" "+oConsignee.measures_name.toUpperCase()+" ("+oConsignee.bag+" Bags)");

				uiConsigneeContainer.append(uiCloneConsigneeTemplate);
			}
	}
	
	/**
    * _displayEditConsigneeDistribution
    * @description This function is for displaying Consignee Distribution in ongoing in edit
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _displayEditConsigneeDistribution(oConsigneeItems, uiCloneTemplate)
	{
			var uiConsigneeContainer = uiCloneTemplate.find(".display-edit-consignee-distibution"),
				uiParentThis = uiConsigneeContainer.parent(),
				uiConsigneeTemplate = uiParentThis.find(".display-edit-consignee-items");
			
			uiConsigneeContainer.html("");
			for(var x in oConsigneeItems)
			{
				
				oConsignee = oConsigneeItems[x];


				var uiCloneConsigneeTemplate = uiConsigneeTemplate.clone();
				uiCloneConsigneeTemplate.removeClass("display-edit-consignee-items").show();
				uiCloneConsigneeTemplate.data(oConsignee);
				uiCloneConsigneeTemplate.find(".display-consignee-distribution-name").html(oConsignee.name);
				uiCloneConsigneeTemplate.find(".display-consignee-distribution-quantity").html(oConsignee.distribution_qty+" "+oConsignee.measures_name.toUpperCase()+" ("+oConsignee.bag+" Bags)");

				uiConsigneeContainer.append(uiCloneConsigneeTemplate);
			}
	}

	/**
    * _createCollapseProduct
    * @description This function is for item collapse
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _createCollapseProduct(uiTemplate)
	{
		var uidDisplayConsigneeSkuNameBtn = $(".display-consignee-sku-name");

		uidDisplayConsigneeSkuNameBtn.off("click.collapeIt").on("click.collapeIt", function(e){
			e.preventDefault();
			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark");
				// iProductId = uiParent.data("product_id");

				// accordion
				$(".panel-group .panel-heading").each(function(){
					var ps = $(this).next(".panel-collapse");
					var ph = ps.find(".panel-body").outerHeight();

					if(ps.hasClass("in")){
						$(this).find("h4").addClass("active")
					}

					$(this).find("a").off("click").on("click",function(e){
						e.preventDefault();
						ps.css({height:ph});

						if(ps.hasClass("in")){
							$(this).find("h4").removeClass("active");
							$(this).find(".fa").removeClass("fa-caret-down").addClass("fa-caret-right");
							ps.removeClass("in");
						}else{
							$(this).find("h4").addClass("active");
							$(this).find(".fa").removeClass("fa-caret-right").addClass("fa-caret-down");
							ps.addClass("in");
						}

						setTimeout(function(){
							ps.removeAttr("style")
						},500);
					});
				});
					// accordion for product-inventory 
					// $(".panel-title").off("click").on("click", function() {		
					// 	$(".panel-title i").removeClass("fa-caret-right").addClass("fa-caret-down");
					// });
					// $(".panel-title .caret-click").off("click").on("click", function() {
					// 	$(this).removeClass(".fa-caret-right").addClass(".fa-caret-down");
					// });


				// console.log(iProductId);
		});

	}

	/**
    * _createCancelProduct
    * @description This function is for canceling item
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _createCancelProduct()
	{

		var btnEditingCancel = $(".editing-cancel");

		btnEditingCancel.off("click.cancel").on("click.cancel", function(e){
			e.preventDefault();

			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
				uiEditingButton = uiParent.find(".editing-button"),
				uiMarker1Btn = uiParent.find(".marker1"),
				uiHideLabel = uiParent.find(".hide-label"),
				uiDisplayLabel = uiParent.find(".display-label"),
				uidisplayDistribution = uiParent.find(".display-distribution"),
				uiHideDistribution = uiParent.find(".hide-distribution"),
				uiHideLink = uiParent.find(".hide-link"),
				uiAddingBatch = uiParent.find(".adding-batch"),
				btnHoverStorage = uiParent.find(".btn-hover-storage"),
				uiQuantityCheckHide = uiParent.find(".quantity-check-hide"),
				uiQuantityCheckBtn = uiParent.find(".quantity-check-btn"),
				uiStConsigneeItems = uiParent.find(".set-consignee-items"),
				uiDisplayEditConsigneeDistibution = uiParent.find(".display-edit-consignee-distibution");



				uiThis.hide();
				$('.to-editing-page').show();
				uiEditingButton.text('Edit');

				uiMarker1Btn.show();

				uiHideLabel.hide();
				uiDisplayLabel.show();
				uidisplayDistribution.show();
				uiHideDistribution.hide();
				uiHideLink.hide();
				uiAddingBatch.hide();
				btnHoverStorage.hide();
				uiQuantityCheckHide.show();
				uiQuantityCheckBtn.hide();
				// uiQuantityCheckBtnWithValue.show();
				uiDisplayEditConsigneeDistibution.show();
				uiStConsigneeItems.hide();


		})

	}

	/**
    * _createEditProduct
    * @description This function is for editng Item
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _createEditProduct(oItems)
	{
		var btnTriggerEditingButton = $(".trigger-editing-button");

		btnTriggerEditingButton.off("click.editprod").on("click.editprod", function(e){
			e.preventDefault();
			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
				iGetProductInventoryID = uiParent.data("product_inventory_id");
				uiMarker1Btn = uiParent.find(".marker1"),
				uiMarker1Cancel = uiParent.find(".marker1-cancel"),
				uiEditingCancel = uiParent.find(".editing-cancel"),
				uiDisplayLabel = uiParent.find(".display-label"),
				uiHideLabel = uiParent.find(".hide-label"),
				uidisplayDistribution = uiParent.find(".display-distribution"),
				uiHideDistribution = uiParent.find(".hide-distribution"),
				uiHideLink = uiParent.find(".hide-link"),
				btnHoverStorage = uiParent.find(".btn-hover-storage"),
				uiAddingBatch = uiParent.find(".adding-batch"),
				uiStorageCheckBtn = uiParent.find(".storage-check-btn"),
				uiLoadingMethodContainer = uiParent.find(".loading-method-container");
				uiStConsigneeItems = uiParent.find(".set-consignee-items"),
				uiDisplayEditConsigneeDistibution = uiParent.find(".display-edit-consignee-distibution");

				
					var getText = uiThis.text();
					if (getText == 'Edit'){

						uiThis.html('Save Changes');

						uiMarker1Btn.hide();
						uiMarker1Cancel.hide();

						$('.to-editing-page').hide();
						uiEditingCancel.show();


						uiDisplayLabel.hide();
						uiHideLabel.css({'display':'inline-block'});


						uidisplayDistribution.hide();
						uiHideDistribution.show();

						uiHideLink.show();
						btnHoverStorage.show();



						uiStConsigneeItems.show();
						uiDisplayEditConsigneeDistibution.hide();


					} else if (getText == 'Save Changes'){

						uiThis.html('Edit');

						

					}

					uiLoadingMethodContainer.show();
					uiStorageCheckBtn.show();
					

		});
	}

	/**
    * _createEnterProductDetails
    * @description This function is for entering Item Detials
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _createEnterProductDetails()
	{

		var btnEditingCancel = $(".marker1");

		btnEditingCancel.off("click.cancel").on("click.cancel", function(e){
			e.preventDefault();

			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
				uiEditingButton = uiParent.find(".editing-button"),
				uiMarker1Cancel = uiParent.find(".marker1-cancel"),
				uiEditingCancel = uiParent.find(".editing-cancel"),
				uiEditingButton = uiParent.find(".editing-button"),
				uiAddingBatch = uiParent.find(".adding-batch"),
				uiListContainer = uiParent.find(".storage-assign-panel.position-rel").find(".btn-hover-storage"),
				uiQuantityCheckHide = uiParent.find(".quantity-check-hide"),
				uiQuantityCheckBtn = uiParent.find(".quantity-check-btn"),
				uiExclamationMark = uiParent.find(".exclamation-mark"),
				uiCheckMark = uiParent.find(".check-mark"),
				uiStorageCheckBtn = uiParent.find(".storage-check-btn"),
				uiStConsigneeItems = uiParent.find(".set-consignee-items"),
				uiDisplayEditConsigneeDistibution = uiParent.find(".display-edit-consignee-distibution");


				var text1 = uiThis.text();

				if(text1 == 'Save Details'){

					uiThis.html('Enter Product Details');

					uiListContainer.find(".btn-hover-storage").css({'opacity' :'0'});

					_displayBatchHover(uiParent, "false");

					
				}else if (text1 == 'Enter Product Details'){
					$('.complete-receiving-record').attr('disabled',true);
					uiThis.html('Save Details');
					$(".to-editing-page").hide();
					uiEditingButton.hide();
					uiMarker1Cancel.show();

					_displayBatchHover(uiParent, "true");


					uiQuantityCheckHide.hide();
					uiQuantityCheckBtn.show();
					uiAddingBatch.show();

					uiStorageCheckBtn.show();
					
					
				}else if(text1 == 'Update Product Details')
				{
					uiThis.html('Save Details');
					$(".to-editing-page").hide();
					uiEditingButton.hide();
					uiMarker1Cancel.show();

					_displayBatchHover(uiParent, "true");


					uiQuantityCheckHide.hide();
					uiQuantityCheckBtn.show();
					uiAddingBatch.show();

					uiStorageCheckBtn.show();
				}

				uiStorageCheckBtn.show();

		});

	}


	/**
    * _createEnterProductDetailsCancel
    * @description This function is for entering Item Detials Cancel
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _createEnterProductDetailsCancel()
	{
		var uiMarker1Cancel = $(".marker1-cancel");

		uiMarker1Cancel.off("click.cancel").on("click.cancel", function(e){
			e.preventDefault();

			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
				oItemData = uiParent.data(),
				uiEditingCancel = uiParent.find(".editing-cancel"),
				uiEditingButton = uiParent.find(".editing-button"),
				uiMarker1Btn = uiParent.find(".marker1"),
				uidisplayDistribution = uiParent.find(".display-distribution"),
				uiHideDistribution = uiParent.find(".hide-distribution"),
				uiAddingBatch = uiParent.find(".adding-batch"),
				uiListContainer = uiParent.find(".storage-assign-panel.position-rel").find(".btn-hover-storage"),
				uiStorageCheckBtn = uiParent.find(".storage-check-btn"),
				uiQuantityCheckBtnWithValue = uiParent.find(".quantity-check-btn-with-value"),
				uiQuantityCheckBtn = uiParent.find(".quantity-check-btn"),
				uiQuantityCheckHide = uiParent.find(".quantity-check-hide"),
				uiStConsigneeItems = uiParent.find(".set-consignee-items"),
				uiDisplayEditConsigneeDistibution = uiParent.find(".display-edit-consignee-distibution");

				uiListContainer.off("mouseenter").on("mouseenter", function(){
					$(this).css({"opacity":"0"});
				});

				var text1 = uiThis.text();


				 if(oItemData.hasOwnProperty("batch") && oItemData.hasOwnProperty("discrepancy")){

					if((Object.keys(oItemData.batch).length > 0) && (Object.keys(oItemData.discrepancy).length > 0) ){
						uiMarker1Btn.html('Update Product Details');
					}else{
						uiMarker1Btn.html('Enter Product Details');
					}
				 }else{
				 	uiMarker1Btn.html('Enter Product Details');
				 }




				uiThis.hide();
				uiEditingCancel.hide();
				$(".to-editing-page").show();

				uiMarker1Btn.show();
				uiEditingButton.show();
				

				uiHideDistribution.hide();
				uidisplayDistribution.show();

				uiStorageCheckBtn.show();

				uiAddingBatch.hide();
				
				uiQuantityCheckBtn.hide();
				uiQuantityCheckHide.show();


		});
	}
	
	/**
    * _displayProductInfoSelected
    * @description This function is for displaying Selected Item Info 
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _displayProductInfoSelected(uiCloneTemplate)
	{

		var uiThis = uiCloneTemplate,
			uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
			uiGetUnitMeasurement = uiParent.data("unit_of_measure_id"),
			oItemDaTa = uiParent.data(),
			oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
			oGetAllConsignee = oConsigneeName,
			uiLoadingMethodContainer = uiParent.find(".loading-method-container"),
			uiSetQtyToBulk = uiParent.find(".set-qty-to-bulk"),
			uiSetQtyToBag = uiParent.find(".set-qty-to-bag"),
			uiUnitOfMeasure = uiParent.find(".unit-of-measure"),
			uiRemoveDropdown = uiParent.find(".get-dropdown-here").find(".frm-custom-dropdown");

			uiRemoveDropdown.remove();



			// console.log(uiGetProdInventoryID);

			var sHtml = '<div class="f-left width-35percent hide-label">'+
							'	<div class="f-left trigger-bulk-radio">'+
							'		<input type="radio" value="Bulk" class="display-inline-mid width-20px default-cursor set-method-by-bulk'+oItemDaTa.product_inventory_id+'" name="bag-or-bulk'+oItemDaTa.product_inventory_id+'" id="bulk'+oItemDaTa.product_inventory_id+'">'+
							'		<label for="bulk'+oItemDaTa.product_inventory_id+'" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>'+
							'	</div>'+
							'	<div class="f-left margin-left-20 trigger-bag-radio">'+																	
							'		<input type="radio" value="Bags" class="display-inline-mid width-20px default-cursor set-method-by-bags'+oItemDaTa.product_inventory_id+'" name="bag-or-bulk'+oItemDaTa.product_inventory_id+'" id="bags'+oItemDaTa.product_inventory_id+'">'+
							'		<label for="bags'+oItemDaTa.product_inventory_id+'" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bags</label>'+
							'	</div>'+								
							'	<div class="clear"></div>'+
							'</div>';

			var uiGetContainer = $(sHtml);
			uiLoadingMethodContainer.append(sHtml);

			var uiSetMethodByBulk = uiParent.find(".set-method-by-bulk"+oItemDaTa.product_inventory_id),
				uiSetMethodByBags = uiParent.find(".set-method-by-bags"+oItemDaTa.product_inventory_id);



				// console.log(JSON.stringify(oItemDaTa));

			if(oItemDaTa.loading_method == "Bulk")
			{
				uiSetMethodByBulk.attr("checked", true);
				uiSetQtyToBag.attr("disabled", true).val("");

			}else{
				uiSetMethodByBags.attr("checked", true);
			}

			uiSetQtyToBulk.val(oItemDaTa.qty);
			uiSetQtyToBulk.number(true, 2);

			if(oItemDaTa.bag != "0")
			{
				uiSetQtyToBag.val(oItemDaTa.bag);
			}

			for(var x in oUnitOfMeasures)
			{

				if(uiGetUnitMeasurement == oUnitOfMeasures[x]['id'])
				{
					sHtmlMeasure = '<option value="'+oUnitOfMeasures[x]['id']+'" selected>'+oUnitOfMeasures[x]['name']+'</option>';
					uiUnitOfMeasure.append(sHtmlMeasure);
				}else{
					if(oUnitOfMeasures[x]['name'] == "kg" || oUnitOfMeasures[x]['name'] == "mt" || oUnitOfMeasures[x]['name'] == "L" )
					{					
						sHtmlMeasure = '<option value="'+oUnitOfMeasures[x]['id']+'">'+oUnitOfMeasures[x]['name']+'</option>';
						uiUnitOfMeasure.append(sHtmlMeasure);
					}
					
				}


			}
			uiUnitOfMeasure.show().css({
				"padding" : "3px 4px",
				"width" :  "50px"
			});




			var uiSetConsigneeItemsContainer = uiParent.find(".set-consignee-items"),
				iCountConsign = 0;

				uiSetConsigneeItemsContainer.html("");

					var sHtmls = '<div class="transfer-prod-location font-0 bggray-white position-rel display-consignee-distribution">';

			for(var i in oItemDaTa.consignee_info){
				var consignee = oItemDaTa.consignee_info[i];
				// console.log(JSON.stringify(consignee));
				var ibagsQty = "";

				(consignee.bag != 0) ? ibagsQty = consignee.bag : ibagsQty = "";

				consignee.distribution_qty

				var sHtml = '<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change display-consignee-items set-cosignee-per-item" count-id="'+iCountConsign+'">'+
							'	<div class="width-50percent display-inline-mid text-center">'+
							'		<div class="select large hide-distribution">'+
							'			<select class="transform-dd select-option-consignee selectConsigneeListSelect">'+
							'				<option value="'+consignee.consignee_id+'">'+consignee.name+'</option>'+
							'			</select>'+
							'		</div>'+
							'	</div>'+
							'	<div class="width-45percent display-inline-mid text-center">'+
							'		<div class="hide-distribution">'+
							'			<input type="text" class="t-small display-inline-mid width-70px margin-left-20 display-consignee-qty" value="'+consignee.distribution_qty+'">'+
							'			<p class="display-inline-mid font-300 margin-left-10 margin-right-15 set-measurement-name">'+consignee.measures_name+'</p>'+
							'			<input type="text" class="t-small display-inline-mid width-70px margin-left-10 display-consignee-bag" value="'+ibagsQty+'">'+
							'			<p class="display-inline-mid margin-left-10 font-300">Bags</p>'+
							'		</div>'+
							'	</div>'+
							'</div>';
					var oPassData = $(sHtml);
						oPassData.data(consignee);

				uiSetConsigneeItemsContainer.append(oPassData);
				iCountConsign++;

			}

				sHtmls = '</div>';
				uiSetConsigneeItemsContainer.append(sHtmls);

				_triggerAddConsigneeItem(uiThis, iCountConsign);


			var uiDisplayConsigneeQty = uiParent.find(".display-consignee-qty"),
				uiDisplayConsigneeBag = uiParent.find(".display-consignee-bag");
				uiDisplayConsigneeQty.number(true, 2);


			if(oItemDaTa.loading_method == "Bulk")
			{

				uiDisplayConsigneeBag.attr("disabled", true).val("");

			}else{
				uiDisplayConsigneeBag.attr("disabled", false);
			}


			// FOR CONSIGNEE DROPDOWN
			var uiSelectConsigneeListSelect = $(".selectConsigneeListSelect"),
			uiParentDropdown = uiSelectConsigneeListSelect.parent(),
			uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');

			uiSiblingDropdown.remove();
			uiSelectConsigneeListSelect.removeClass("frm-custom-dropdown-origin");

			for(var x in oGetAllConsignee)
			{
				var sHtml = '<option value="'+oGetAllConsignee[x]['id']+'">'+oGetAllConsignee[x]['name']+'</option>';
					uiSelectConsigneeListSelect.append(sHtml);
			}

			uiSelectConsigneeListSelect.transformDD();


			_saveUpdateCosigneeProduct();

			CCReceivingConsigneeGoods.bindEvents("triggerLoadingMethod");
			CCReceivingConsigneeGoods.bindEvents("triggerUnitOfMeasure");
	}

	/**
    * _addBatchForProduct
    * @description This function is for adding Batch for Item
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _addBatchForProduct()
	{
		var uiTemplateContainer =  $(".record-item"),
			batchModal = $('[modal-id="add-batch"]'),
			batchModalClose = batchModal.find(".close-me"),
			confirmAddBatch = $("#confirmAddBatch"),
			oGatherDiscrepancy = {},
			oStoredDiscrepancy = [];



			// console.log();

			$.each(uiTemplateContainer, function(){
				var uiThisParent = $(this),
					oRecord = uiThisParent.data(),
					uiShowBatchModalBtn = uiThisParent.find('[modal-target="add-batch"]');

					uiShowBatchModalBtn.off("click.addBatch").on("click.addBatch", function(e){

						e.preventDefault();


						batchModal.addClass('showed');

						var	uiGetQuantityData = uiThisParent.find(".display-consignee-qtr-to-receive"),
							sGetProdUnitMeasurement = uiGetQuantityData.data("unitOfMesurement"),
							uigetDiscrepancy = uiThisParent.find(".discrepancy"),
							oDiscrepancy= uigetDiscrepancy.data(),
							itemReceivingBalance = 0,
							iQtyToReceive = uiGetQuantityData.data("quantity");

							if(typeof oDiscrepancy.discrepancy == 'undefined')
							{
								itemReceivingBalance = oItemReceivingBalance[oRecord.product_inventory_id];
							}
							else
							{

								var iTotalBalance = parseFloat(iQtyToReceive) - parseFloat(oDiscrepancy.discrepancy.total);
								itemReceivingBalance = iTotalBalance;
							}


						if(bBatchEditMode){
							var batchAmount = parseFloat(oBatchPerItem[oRecord.product_inventory_id][sBatchEditKey]["batch_amount"]);
							itemReceivingBalance = itemReceivingBalance + batchAmount;
						}


						var uiNameSku = batchModal.find(".product-name-sku"),
							uiVesselName = batchModal.find(".product-vessel-name"),
							uiInputName = batchModal.find(".input-batch-name"),
							uiToReceive = batchModal.find(".to-receive"),
							uiInputAmount = batchModal.find(".input-batch-amount"),
							uiAmountUom = batchModal.find(".batch-amount-uom"),
							uiReceiveBal = batchModal.find(".receiving-balance"),
							iReceivingTotalBal = parseFloat(itemReceivingBalance),
							uiGetVesselName = $("#display-selected-vessel-name-temp"),
							uiInputBagsAmount = batchModal.find(".input-bags-amount"),
							uiBagsInputDivs = batchModal.find(".bags-input-divs"),
							sVesselName = uiGetVesselName.text().trim(),
							timeoutType = {};


							if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1){
								if(oRecord.loading_method == "Bags")
								{
									uiBagsInputDivs.show();
									uiInputBagsAmount.val("");

								}else{
									uiBagsInputDivs.hide();
								}
							}




							if(iReceivingTotalBal < 0){
								uiReceiveBal.html($.number(iReceivingTotalBal, 2)+" "+oRecord.measures_name).css({"color":"red"});
							}else{
								uiReceiveBal.html($.number(iReceivingTotalBal, 2)+" "+oRecord.measures_name);
							}

							uiNameSku.html("SKU No. "+oRecord.sku+" - "+oRecord.product_name);
							uiVesselName.html(sVesselName);
							uiToReceive.html($.number(oRecord.qty, 2)+" "+oRecord.measures_name);
							
							uiAmountUom.html(oRecord.measures_name);
							uiInputAmount.number(true, 2);

							uiInputName.val("");
							uiInputAmount.val("");

							uiInputAmount.off('keyup.changeamount').on('keyup.changeamount', function(){

								var uiThis = $(this),
									sThisVal = uiThis.val(),
									iThisVal = parseFloat(sThisVal),
									iBalance = parseFloat(itemReceivingBalance),
									uom = oRecord.measures_name;
								
								clearTimeout(timeoutType);
				
								timeoutType = setTimeout(function(){
									if(sThisVal.trim().length > 0){
										var iTotal = iBalance - iThisVal;
										if(iTotal > iBalance){
											iTotal = iBalance;
											uiReceiveBal.html('<i class="fa fa-minus" aria-hidden="true"></i>'+$.number(iTotal, 2)+" "+uom).css({"color":"red"});
											//iReceivingTotalBal = iTotal;
										}else if(iTotal < 0){

											uiReceiveBal.html('<i class="fa fa-minus" aria-hidden="true"></i>'+$.number(Math.abs(iTotal), 2)+" "+uom).css({"color":"red"});
											//uiThis.val(iBalance);
											//iReceivingTotalBal = 0;

										}else{
											if(iTotal < 0)
											{
												uiReceiveBal.html($.number(iTotal, 2)+" "+uom).css({"color":"red"});
											}else{

												uiReceiveBal.html($.number(iTotal, 2)+" "+uom).css({"color":"black"});
											}
											//iReceivingTotalBal = iTotal;
										}
									}else{
										if(iBalance < 0)
											{
												uiReceiveBal.html($.number(iBalance, 2)+" "+uom).css({"color":"red"});
											}else{

												uiReceiveBal.html($.number(iBalance, 2)+" "+uom).css({"color":"black"});
											}
										//iReceivingTotalBal = iBalance;
									}
									clearTimeout(timeoutType);
								},500);

							});

							_displayBatchStorages(oStorages);

							batchModalClose.off('click').on('click', function(){
								batchModal.removeClass("showed");
								bBatchEditMode = false;
							});

							confirmAddBatch.off('click').on('click', function(){
								var uiSelectedLocation = $(".item-bread.temporary"),
									oLocation = uiSelectedLocation.data(),
									error = 0,
									arrErrorMsg = [];
								
								if(oItemReceivingBalance[oRecord.product_inventory_id] == 0 && !bBatchEditMode){
									error++;
									arrErrorMsg.push("Receiving Balance Already Been Used");
								}else{
									if(!oLocation){
										error++;
										arrErrorMsg.push("Select Location");
									}

									if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1){
											if(oRecord.loading_method == "Bags")
											{
												if(uiInputBagsAmount.val().trim() == "" || uiInputBagsAmount.val().trim() == 0 || uiInputBagsAmount.val().trim() == "0")
												{
													error++;
													arrErrorMsg.push("Enter Bags Amount");
												}

											}
										}
									


									if(uiInputName.val().trim().length == 0){
										error++;
										arrErrorMsg.push("Enter Batch Name");
									}

									if(uiInputName.val().trim().length == 0){
										error++;
										arrErrorMsg.push("Enter Batch Name");
									}
									
								}

								if(error > 0){
									$("body").feedback({title : "Message", message : arrErrorMsg.join(","), type : "danger", icon : failIcon});
								}else{
									// Add batch
									var recordBatch = oBatchPerItem[oRecord.product_inventory_id],
										len = Object.keys(recordBatch).length,
										iBagsAmountTruck = 0;

										if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1){
											if(oRecord.loading_method == "Bags")
											{
												iBagsAmountTruck = uiInputBagsAmount.val().trim();
											}
										}

										

									// 	console.log(JSON.stringify(oRecord));

										if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1)
										{
											// Add batch
											if(!bBatchEditMode){
												oBatchPerItem[oRecord.product_inventory_id][len] = {
													"location" : oLocation,
													"item_id" : oRecord.product_id,
													"batch_name" : uiInputName.val(),
													"batch_amount" : uiInputAmount.val(),
													"bag_amount" : iBagsAmountTruck,
													"uom" : oRecord.measures_name,
													"uom_id" : oRecord.unit_of_measure_id,
													"hierarchy" : _getBreadCrumbsHierarchy()
												};
											}else{
												oBatchPerItem[oRecord.product_inventory_id][sBatchEditKey] = {
													"location" : oLocation,
													"item_id" : oRecord.product_id,
													"batch_name" : uiInputName.val(),
													"batch_amount" : uiInputAmount.val(),
													"bag_amount" : iBagsAmountTruck,
													"uom" : oRecord.product_unit_of_measure,
													"uom_id" : oRecord.unit_of_measure_id,
													"hierarchy" : _getBreadCrumbsHierarchy()
												}
												bBatchEditMode = false;
											}
										}else{

											// Add batch
											if(!bBatchEditMode){
												oBatchPerItem[oRecord.product_inventory_id][len] = {
													"location" : oLocation,
													"item_id" : oRecord.product_id,
													"batch_name" : uiInputName.val(),
													"batch_amount" : uiInputAmount.val(),
													"uom" : oRecord.measures_name,
													"uom_id" : oRecord.unit_of_measure_id,
													"hierarchy" : _getBreadCrumbsHierarchy()
												};
											}else{
												oBatchPerItem[oRecord.product_inventory_id][sBatchEditKey] = {
													"location" : oLocation,
													"item_id" : oRecord.product_id,
													"batch_name" : uiInputName.val(),
													"batch_amount" : uiInputAmount.val(),
													"uom" : oRecord.product_unit_of_measure,
													"uom_id" : oRecord.unit_of_measure_id,
													"hierarchy" : _getBreadCrumbsHierarchy()
												}
												bBatchEditMode = false;
											}

										}

										


									oItemReceivingBalance[oRecord.product_inventory_id] = iReceivingTotalBal;
									
									$('[modal-target="add-batch"]').show();
									$("[modal-id='add-batch'] .close-me:visible").click();


									_displayBatches(oRecord.product_inventory_id);

									_displayBatchHover(uiThisParent, "true");


								}

							});

							setTimeout(function(){
								if(typeof callBack == 'function'){
									callBack();
								}
							}, 100);

							// _displayBatchStorages(oStorages);


					});

			});

	}

	/**
    * _displayBatches
    * @description This function is for displaying Batches
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _displayBatches(prodID)
	{
		var oBatch = oBatchPerItem[prodID],
			productContainer = $(".record-item[prod-id='"+prodID+"']"),
			uiAssignmentDetails = productContainer.find(".assignment-display"),
			uiTable = uiAssignmentDetails.find(".tbl-4c3h"),
			uiTableSibling = uiTable.nextAll(),
			uiMarker1 =  productContainer.find(".marker1"),
			uiHibeAddBatchBtn = uiAssignmentDetails.find(".hide-method"),
			uiSpanReceivingTotal = productContainer.find(".received-quantity"),
			uiSpanDiscrepancy = productContainer.find(".discrepancy"),
			receivingData = productContainer.find(".display-consignee-qtr-to-receive").data(),
			qtyToReceive = parseFloat(receivingData.quantity),
			uiListContainer = productContainer.find(".storage-assign-panel.position-rel").find(".btn-hover-storage"),
			uom = receivingData["unitOfMesurement"];

			// console.log(qtyToReceive);

		
		uiTableSibling.remove();
		uiHibeAddBatchBtn.show();

		var template = {
			container : "<div class='transfer-prod-location font-0 bggray-white'></div>",
			total : '<div class="total-amount-storage margin-top-10"> <p class="first-text">Total</p><p class="second-text"> </p> <div class="clear"></div></div>',
			hier_text : '<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>',
			hier_arrow : '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>',
			list : '<div class="storage-assign-panel position-rel">'+
						'<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change">'+
							'<div class="width-25percent display-inline-mid text-center">'+
								'<p class="font-14 font-400 batch-name"> </p>'+
							'</div>'+
							'<div class="width-50percent text-center display-inline-mid height-auto line-height-25 hierarchy">'+											
							'</div>'+
							'<div class="width-25percent display-inline-mid text-center">'+
								'<p class="font-14 font-400 quantity"></p>'+
							'</div>'+
						'</div>'+
						'<div class="btn-hover-storage" style="height: 55px; display: block; opacity: 0;">'+
							'<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>'+
							'<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>'+
						'</div>'+
					'</div>',
		not_yet_assigned : '<div class="transfer-prod-location font-0 bggray-white position-rel">'+
                    '<div class="width-100percent padding-top-10 padding-bottom-10 show-ongoing-content margin-bottom-20 background-change">'+
                        '<div class="width-100percent display-inline-mid text-center">'+
                            '<p class="font-14 font-400 italic">Not Yet Assigned</p>'+
                        '</div>'+
                    '</div>'+
                '</div>'
		};



		
		var uiContainer = $(template.container),
			stripChange = false,
			iTotal = 0,
			unitOfMeasure = "";
		
		uiContainer.insertAfter(uiTable);

		for(var i in oBatch){
			var uiListContainer = $(template.list),
				oHierarchy = oBatch[i]["hierarchy"],
				uom = oBatch[i]["uom"],
				batch_amount = oBatch[i]["batch_amount"],
				uiRemoveBatch = uiListContainer.find('[modal-target="remove-batch"]'),
				uiEditBatch = uiListContainer.find("[modal-target='edit-batch']");

			// Remove Batch
			uiRemoveBatch.attr("batch-key", i);
			uiEditBatch.attr("batch-key", i);


			uiEditBatch.off('click').on('click', function(e){
					var uiThis = $(this),
						uiList = uiThis.closest(".storage-assign-panel"),
						uiThisParent = uiThis.closest(".transfer-prod-location"),
						uiParent = uiThis.closest(".record-item"),
						uiParentData = uiParent.data(),
						batchData = uiParentData.batch,
						batchKey = uiThis.attr("batch-key"),
						uiTotalContainer = uiParent.find(".total-amount-storage"),
						oBatchData = {};
					
					sBatchEditKey = batchKey;
					bBatchEditMode = true;

					for(var i in batchData){
						if(i == batchKey){
							oBatchData = batchData[i];	
							$(".input-batch-amount").data("value", oBatchData.batch_amount);
						}
					}


					_addBatchEvent(e, function(){

						var breadCrumbsHierarchy = $("#breadCrumbsHierarchy"),
							uiBreadParent = breadCrumbsHierarchy.parent(),
							batchDisplay = $("#batchDisplay"),
							oHierarchy = oBatchData.hierarchy,
							oQueue = {};


						
						$(".input-batch-name").val(oBatchData.batch_name);
						$(".input-batch-amount").val(oBatchData.batch_amount);



						if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1){
							if(uiParentData.loading_method == "Bags")
							{
								$(".input-bags-amount").val(oBatchData.bag_amount);
								$(".bags-input-divs").show();

								console.log(oBatchData.bag_amount);
							}else{
								uiBagsInputDivs.hide();
							}
						}


						batchDisplay.css("opacity", 0);
						var uiLoader = $("<div loader='true'></div>");
						uiLoader.css({
							"position" : "absolute",
							"top" : "0px",
							"left" : "0px",
							"width" : "100%",
							"height" : "100%",
							"background-color" : "#444",
							"color" : "#fff",
							"text-align" : "center"
						});
						var uiSpinner = $('<div><i class="fa fa-cog fa-spin fa-3x fa-fw margin-bottom"></i>Loading..</div>');
						uiSpinner.css({
							"position" : "absolute",
							"width" : "30px",
							"height" : "30px",
							"top" : "40%",
							"left" : "50%",
							"margin-left" : "-15px",
							"margin-right" : "-15px"
						});
						uiLoader.html(uiSpinner);
						uiBreadParent.append(uiLoader);
						uiBreadParent.css("position", "relative");

						for(var i in oHierarchy){
							oQueue[i] = {};
							if(i == "0"){
								oQueue[i]["selector"] = "[storage-id='"+oHierarchy[i]["id"]+"']";
							}else{
								oQueue[i]["selector"] = "[location-id='"+oHierarchy[i]["id"]+"']";
							}

							oQueue[i]["callback"] = function(){
								var oThis = this, 
									selector = oThis.selector,
									uiLocation = $(selector);

								uiLocation.click();
								setTimeout(function(){
									if(typeof oThis.nextcall == 'function'){
										uiLocation.click();
										setTimeout(function(){
											var oNext = oThis.nextobj;
											oThis.nextcall.apply(oNext);

											if(oNext.hasOwnProperty("last")){
												if(oNext.last){
													setTimeout(function(){
														batchDisplay.css("opacity", 1);
														$("[loader='true']").remove();
													},1300);
												}
											}
										},500);
									}
								}, 30);
							}
						}
						
						var timeoutCall = function(obj){
							setTimeout(function(){
								var que = obj,
									callback = que.callback;
								callback.apply(que);
							}, 500);
						}
						
						for(var i in oQueue){
							var queNext = oQueue[parseInt(i) + 1];
							if(queNext){
								oQueue[i]['callback'].bind(queNext);
								oQueue[i]['nextobj'] = queNext;
								oQueue[i]['nextcall'] = oQueue[parseInt(i) + 1]['callback'];
								oQueue[i]['nextcall_from'] = oQueue[i]["selector"];
								oQueue[i]['nextcall_to'] = oQueue[parseInt(i) + 1]["selector"];
							}else{
								oQueue[i]['last'] = true;
							}
							timeoutCall(oQueue[i]);
						}
						 

					});
				});

			uiRemoveBatch.off('click').on('click', function(){
				var uiThis = $(this),
					uiList = uiThis.closest(".storage-assign-panel"),
					uiThisParent = uiThis.closest(".transfer-prod-location"),
					uiParent = uiThis.closest(".record-item"),
					uiParentData = uiParent.data(),
					uom = uiParentData.unit_of_measure_id,
					batchData = uiParentData.batch,
					batchKey = uiThis.attr("batch-key"),
					uiTotalContainer = uiParent.find(".total-amount-storage"),
					uiDiscrepancy = uiParent.find(".discrepancy"),
					uiReceivingTotal = uiParent.find(".received-quantity"),
					iBatchAmount = 0;


					
						
				
				console.log(batchData);
				for(var i in batchData){
					if(i == batchKey){
						iBatchAmount = parseFloat(batchData[i]["batch_amount"]);
						delete batchData[i];	
					}
				}
				console.log(batchData);

				var counter = 0,
					newBatchData = {};
				for(var i in batchData){
					newBatchData[counter] = batchData[i];
					counter++;
				}

				uiParent.data("batch", newBatchData);
				uiList.remove();

				if(Object.keys(newBatchData).length == 0){
					var uiNotYetAssigned = $(template.not_yet_assigned);

					batchData = {};
					uiTotalContainer.remove();

					uiNotYetAssigned.insertAfter(uiTable);

					oItemReceivingBalance[uiParentData.product_inventory_id] = parseFloat(uiParentData.quantity);

					uiDiscrepancySpan = uiDiscrepancy.find("span");
					uiDiscrepancySpan.html($.number(0, 2));
					
					uiReceivingTotal.html($.number(0, 2) + " "+uom);
					iTotal = 0;
					_setQuantityCheckProduct(uiContainer, iTotal);

				}else{
					var iBal = parseFloat(oItemReceivingBalance[uiParentData.product_inventory_id]),
						iTotal = 0;

					iTotal = iBal + iBatchAmount;

					oItemReceivingBalance[uiParentData.product_inventory_id] = iTotal;
					
					oBatchPerItem[uiParentData.product_inventory_id] = newBatchData;
					
					_displayBatches(uiParentData.product_inventory_id);

					_displayBatchHover(uiParent, "true");
				}



			});
			// End Remove Batch

			unitOfMeasure = uom;
			
			if(stripChange){
				uiListContainer.find(".background-change").removeClass("background-change");
			}

			uiListContainer.find(".batch-name").html(oBatch[i]["batch_name"]);
			
			for(var x in oHierarchy){
				var uiHierContainer = uiListContainer.find(".hierarchy"),
					uiHierArrow = $(template.hier_arrow),
					uiHierText = $(template.hier_text);
				
				uiHierText.html(oHierarchy[x]["name"]);
				uiHierContainer.append(uiHierText);
				if((Object.keys(oHierarchy).length - 1) != x){
					uiHierContainer.append(uiHierArrow);	
				}
			}

			uiListContainer.find(".quantity").html($.number(batch_amount, 2)+" "+uom);

			uiContainer.append(uiListContainer);

			if(stripChange){
				stripChange = false;
			}else{
				stripChange = true;
			}

			iTotal = iTotal + parseFloat(batch_amount);

		}


		var uiTotal = $(template.total);
		
		uiTotal.find(".second-text").html($.number(iTotal, 2)+" "+unitOfMeasure).data({"total_batch_quantity":iTotal});
		uiTotal.insertAfter(uiContainer);




		_setQuantityCheckProduct(uiContainer, iTotal);


		productContainer.data("batch", oBatch);


	}

	/**
    * _displayQuantityCheckSuccess
    * @description This function is for displaying Quantity Check Success
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _displayQuantityCheckSuccess(ui, oData)
	{	

		var uiParent = ui.closest(".border-top.border-blue.box-shadow-dark"),
			uiDisplaySuccessReceiveQty = uiParent.find(".display-success-receive-qty"),
			uiDisplaySuccessReasonDiscrepancy = uiParent.find(".display-success-reason-discrepancy"),
			uiDisplaySuccessDiscrepancy = uiParent.find(".display-success-discrepancy"),
			uiDisplaySuccessDiscrepancyRemarks = uiParent.find(".display-success-discrepancy-remarks"),
			uiDisplaySuccessIcon = uiParent.find(".caution.display-inline-mid");
			uiMarker1 = uiParent.find(".marker1");
			sDisplayType = "";

			uiMarker1.text("Update Product Details");

			uiDisplaySuccessIcon.find(".check-mark").show();
			uiDisplaySuccessIcon.find(".exclamation-mark").hide();
			

			uiParent.find(".gray-color").removeClass("gray-color");

				


				if(oData["discrepancy_type"] == 1)
				{
					sDisplayType = '<i class="fa fa-minus" aria-hidden="true"></i>';
				}


				uiDisplaySuccessReceiveQty.html($.number(oData["received_quantity"], 2)+" "+oData["unit_of_measure"]);
				uiDisplaySuccessReasonDiscrepancy.html(oData["discrepancy_cause"]);
				uiDisplaySuccessDiscrepancy.html(sDisplayType+" "+$.number(oData["discrepancy_value"], 2)+" "+oData["unit_of_measure"]);
				uiDisplaySuccessDiscrepancyRemarks.html(oData["discrepancy_reason"]);

	}


	/**
    * _addCompleteReceivingRecord
    * @description This function is for adding Complete Receiving Record
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _addCompleteReceivingRecord()
	{		
		var oGetAllItem = {},
			uiBerthingStartDate = $("#berthing-start-date"),
			uiBerthingStartTime = $("#berthing-start-time"),
			uiUnloadingStartDate = $("#unloading-start-date"),
			uiUnloadingStartTime = $("#unloading-start-time"),
			uiUnloadingEndDate = $("#unloading-end-date"),
			uiUnloadingEndTime  =$("#unloading-end-time"),
			uiDurationDateTime = $("#duration-date-time"),
			uiDurationDate = $("#departure-date"),
			uiDurationTime = $("#departure-time"),
			btnConfirmCompleteReceivingRecord = $("#confirm-complete-receiving-record"),
			uiTriggerComputeDuration = $(".trigger-compute-duration"),
			iDurationDateTime = 0,
			arrErrorCount = [],
			oGetFromData = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"});

			if(oGetFromData !== null)
			{
				oGetAllItem = oGetFromData;
			}else{
				

				var getCompleteInterval = setInterval(function(){
					var oGetFromComplete = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItemComplete"});
						
					if(oGetFromComplete !== null)
					{
						oGetAllItem = oGetFromComplete;
						clearInterval(getCompleteInterval);
					}

				}, 300);


			}



			
				uiBerthingStartDate.val(oGetAllItem["berthing_time_date"]);
				uiBerthingStartTime.val(oGetAllItem["berthing_time_set"]);


			


			uiTriggerComputeDuration.off("click.duration change.durations").on("click.duration change.durations", function(){

					if(uiUnloadingStartDate.val().trim() != "" 
						&& uiUnloadingStartTime.val().trim() != "" 
						&& uiUnloadingEndDate.val().trim() != ""
						&& uiUnloadingEndTime.val().trim() != "")
					{
						var sUnloadingStartDate = uiUnloadingStartDate.val().split('/').join("-"),
							sUnloadingEndDate = uiUnloadingEndDate.val().split('/').join("-"),
							sStartTime = _convertHours(uiUnloadingStartTime.val()),
							sEndTime = _convertHours(uiUnloadingEndTime.val()),
							sStartDateTime = sUnloadingStartDate+" "+sStartTime,
							sEndDateTiem = sUnloadingEndDate+" "+sEndTime;

							var sGetTimesTampStart = _convertDateAndHoursToTimeStamp(sStartDateTime),
								sGetTimesTampEnd = _convertDateAndHoursToTimeStamp(sEndDateTiem),
								sGetResultTimeStamp = sGetTimesTampEnd - sGetTimesTampStart,
								sConvertToml = (sGetResultTimeStamp) * 1000;

						// uiDurationDateTime.html("");
						var date = new Date(sConvertToml);
							var str = date.getUTCDate()-1 + " days, "+date.getUTCHours() + " hours, "+date.getUTCMinutes() + " minutes, "+date.getUTCSeconds() + " seconds. ";

							uiDurationDateTime.text(str);

							// console.log("ok");

							iDurationDateTime = sConvertToml;
							// console.log(iDurationDateTime);
					}
				});




			btnConfirmCompleteReceivingRecord.off("click.confirmed").on("click.confirmed", function(){

				(uiBerthingStartDate.val().trim() == "") ? arrErrorCount.push("Berthing Date Start Required") : "";
				(uiBerthingStartTime.val().trim() == "") ? arrErrorCount.push("Berthing Time Start Required") : "";
				(uiUnloadingStartDate.val() == "") ? arrErrorCount.push("Unloading Date Start Required") : "";
				(uiUnloadingStartTime.val() == "") ? arrErrorCount.push("Unloading Time Start Required") : "";
				(uiUnloadingEndDate.val() == "") ? arrErrorCount.push("Unloading Date End Required") : "";
				(uiUnloadingEndTime.val() == "") ? arrErrorCount.push("Unloading Time End Required") : "";
				(uiDurationDate.val() == "") ? arrErrorCount.push("Duration Date Required") : "";
				(uiDurationTime.val() == "") ? arrErrorCount.push("Duration Time Required") : "";

				if(arrErrorCount.length > 0)
				{
					$("body").feedback({title : "Completing Receiving Record", message : arrErrorCount.join("<br />"), type : "danger", icon : failIcon});
				}else{

					


					var oData = {
						receiving_id : oGetAllItem["receiving_id"],
						berthing_date : uiBerthingStartDate.val().trim(),
						berthing_time : uiBerthingStartTime.val().trim(),
						start_unloading_date : uiUnloadingStartDate.val(),
						start_unloading_time : uiUnloadingStartTime.val(),
						end_unloading_date : uiUnloadingEndDate.val(),
						end_unloading_time : uiUnloadingEndTime.val(),
						duration_date : uiDurationDate.val(),
						duration_time : uiDurationTime.val(),
						unloading_duration : iDurationDateTime
					}


					_saveCompleteReceivingRecordFormVessel(oData);

					var options = {title : "Successfully Completed", message : "Consignee Goods Completed!", speed : "slow", withShadow : true, type: "success"};
					$("body").feedback(options);

				}

				arrErrorCount = [];
			});
	}

	/**
    * _saveUpdateCosigneeProductDetails
    * @description This function is for saving Update Consignee Item Details
    * @dependencies 
    * @param N/A
    * @response N/A
    * @criticality N/A
    * @software_architect N/A
    * @developer Nelson Estuesta Jr
    * @method_id N/A
    */
	function _saveUpdateCosigneeProductDetails()
	{
		var btnTriggerSaveProdButton = $(".marker1"),
			oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"});

		btnTriggerSaveProdButton.off("click.saveUpdate").on("click.saveUpdate", function(e){
			e.preventDefault();
			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
				oItemData = uiParent.data(),
				uiMarker1Cancel = uiParent.find(".marker1-cancel"),
				uiEditingCancel = uiParent.find(".editing-cancel"),
				uiEditingButton = uiParent.find(".editing-button"),
				uiAddingBatch = uiParent.find(".adding-batch"),
				uiQuantityCheckHide = uiParent.find(".quantity-check-hide"),
				uiQuantityCheckBtn = uiParent.find(".quantity-check-btn"),
				uiExclamationMark = uiParent.find(".exclamation-mark"),
				uiCheckMark = uiParent.find(".check-mark"),
				uiGetRemarks = uiParent.find('.set-discrepancy-remarks'),
				uiGetCause = uiParent.find('.set-discrepancy-cause'),
				uiGetTotalBatchQty = uiParent.find(".second-text"),
				oGetTotalBatchQty = uiGetTotalBatchQty.data("total_batch_quantity"),
				iTotalBatchQty = parseFloat(oGetTotalBatchQty),
				uiDiscrepancy = uiParent.find(".discrepancy"),
				oDiscrepancy = uiDiscrepancy.data();
				getText = uiThis.text(),
				oStoreProductDetailsPerItem = [],
				oStoredError = [],
				iDiscrepancy = 0,
				iDiscrepancyType = 0,
				iTotalQtyToReceived = 0;

			if(getText == "Enter Product Details"){

				if(typeof oGetTotalBatchQty == 'undefined'){
					oStoredError.push("Storage Assignment must not be empty");
				}

				if(typeof oDiscrepancy.discrepancy == 'undefined')
				{
					iDiscrepancy = 0;
					iDiscrepancyType = 0;

				}
				else
				{
					if(oDiscrepancy.discrepancy.type == "0")
					{

					}else{
						iDiscrepancy = oDiscrepancy.discrepancy.value;
						iTotalQtyToReceived = oDiscrepancy.discrepancy.total;
						if(oDiscrepancy.discrepancy.type == "up")
						{
							iDiscrepancyType = 1;
						}else if(oDiscrepancy.discrepancy.type == "down"){
							iDiscrepancyType = 2;
						}else{
							iDiscrepancyType = 0;
						}


						if(uiGetRemarks.val().trim() == "")
						{
							oStoredError.push("Quantity Check Remarks is Required");
						}

						if(uiGetCause.val() == "")
						{
							oStoredError.push("Cause of Discrepancy is Required");
						}


					}

					
				}


				if(oStoredError.length === 0)
				{


					var oGatherProductDetailsPerItem = {
						receiving_item_record_id : oItemData.product_inventory_id,
						product_id : oItemData.product_id,
						batch : oItemData.batch,
						received_quantity : iTotalQtyToReceived,
						discrepancy_type : iDiscrepancyType,
						discrepancy_value : iDiscrepancy,
						discrepancy_reason : uiGetRemarks.val().trim(),
						discrepancy_cause : uiGetCause.val()

					}
					oStoreProductDetailsPerItem.push(oGatherProductDetailsPerItem);
					

					_completeProductDetails(oStoreProductDetailsPerItem);


					var oSetDiscrepancy = {
						received_quantity : iTotalQtyToReceived,
						discrepancy_type : iDiscrepancyType,
						discrepancy_value : iDiscrepancy,
						discrepancy_reason : uiGetRemarks.val().trim(),
						discrepancy_cause : uiGetCause.val(),
						unit_of_measure : oItemData.measures_name.toUpperCase()
					}

					uiParent.data("discrepancy", oSetDiscrepancy);


					uiThis.html('Enter Product Details');
					uiMarker1Cancel.hide();
					uiEditingCancel.hide();
					$(".to-editing-page").show();
					uiThis.show();
					uiEditingButton.show();



					uiAddingBatch.hide();

					
					uiQuantityCheckHide.show();
					uiQuantityCheckBtn.hide();

					uiExclamationMark.hide();
					uiCheckMark.show();

					$("body").feedback({title : "Update Product Details", message : "Successfully Updated", type : "success"});


					_displayQuantityCheckSuccess(uiParent, oSetDiscrepancy);

				}else{


					$("body").feedback({title : "Updating Product Details", message : oStoredError.join("<br />"), type : "danger", icon : failIcon});

					uiThis.html('Save Details');
				}
				
			}

			oStoredError = [];

			

		});
	}

	function _CheckOngoingReceivingConsignee()
	{


		// CHECK IF READY TO  COMPLETE
		var BtnMarker1 = $(".marker1");

		BtnMarker1.off("click.checkit").on("click.checkit", function(){
			var uiThis = $(this),
				text1 = uiThis.text();

			if(text1 == 'Update Product Details' || text1 == 'Enter Product Details'){

				var uiGetMainContainer = $("#displayProductItemList").find(".border-top.border-blue.box-shadow-dark"),
					iCountComplete = 0,
					iCountCheck = 0;

				$.each(uiGetMainContainer, function(){
					var uiThisContainer = $(this),
						oItemData = uiThisContainer.data();

						if( (oItemData.hasOwnProperty("batch")) && (oItemData.hasOwnProperty("discrepancy"))){
							if( (Object.keys(oItemData.batch).length > 0) && (Object.keys(oItemData.discrepancy).length > 0))
							{
								iCountComplete++;
							}
						}
						iCountCheck++;


				});
				// console.log(iCountComplete);
				// console.log(iCountCheck);

				if(iCountComplete == iCountCheck)
				{
					$('.complete-receiving-record').attr('disabled',false);
					_updateReceivingRecordComplete();
					_addCompleteReceivingRecord();
				}else{
					$('.complete-receiving-record').attr('disabled',true);

				}
			}

		});

	}


	function _updateReceivingRecordComplete()
	{
		var receivingId = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"});
		var oOptions = {
			url : BASEURL+"receiving/update_to_compelete_record",
			type : "POST",
			data : {"receiving_id":receivingId.value},
			success : function(oResponse){
				console.log(oResponse);
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}


	function _triggerAddConsigneeItem(uiCloneTemplate, iCountConsign)
	{
		var uiThis = uiCloneTemplate,
			uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
			oItemDaTa = uiParent.data(),
			uiTriggerAddConsignee = uiParent.find(".trigger-add-consignee"),
			uiSetConsigneeItemsContainer = uiParent.find(".set-consignee-items"),
			uiSetMethodByBulk =uiParent.find(".set-method-by-bulk"+oItemDaTa.product_inventory_id),
			oGetAllConsignee = "",
			iCount = iCountConsign + 1;

			var getConsigneeInterval = setInterval(function(){
				oGetAllConsignee = oConsigneeName;

				if(oGetAllConsignee !== null)
				{
					clearInterval(getConsigneeInterval);

					uiTriggerAddConsignee.off("click.addConsign").on("click.addConsign", function(e){
						e.preventDefault();



						var sHtml = '<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change display-consignee-items set-cosignee-per-item" count-id="'+iCount+' ">'+
										'	<div class="width-50percent display-inline-mid text-center">'+
										'		<div class="select large ">'+
										'			<select class="transform-dd select-option-consignee selectConsigneeListSelect">'+
										'			</select>'+
										'		</div>'+
										'	</div>'+
										'	<div class="width-45percent display-inline-mid text-center">'+
										'		<div class="hide-distribution">'+
										'			<input type="text" class="t-small display-inline-mid width-70px margin-left-20 display-consignee-qty" value="">'+
										'			<p class="display-inline-mid font-300 margin-left-10 margin-right-15 set-measurement-name">'+oItemDaTa.measures_name+'</p>'+
										'			<input type="text" class="t-small display-inline-mid width-70px margin-left-10 display-consignee-bag" value="">'+
										'			<p class="display-inline-mid margin-left-10 font-300">Bags</p>'+
										'		</div>'+
										'	</div>'+
										'</div>';
							var oPassData = $(sHtml);
							uiSetConsigneeItemsContainer.append(oPassData);

							iCount++;

							// FOR CONSIGNEE DROPDOWN
							var uiSelectConsigneeListSelect = $(".selectConsigneeListSelect"),
							uiParentDropdown = uiSelectConsigneeListSelect.parent(),
							uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');

							uiSiblingDropdown.remove();
							uiSelectConsigneeListSelect.removeClass("frm-custom-dropdown-origin");

							for(var x in oGetAllConsignee)
							{
								var sHtml = '<option value="'+oGetAllConsignee[x]['id']+'">'+oGetAllConsignee[x]['name']+'</option>';
									uiSelectConsigneeListSelect.append(sHtml);
							}

							uiSelectConsigneeListSelect.transformDD();

							$(".display-consignee-qty").number(true, 2);

							var uiDisplayConsigneeBag = uiParent.find(".display-consignee-bag");

							if(uiSetMethodByBulk.is(":checked") === true)
							{
								uiDisplayConsigneeBag.attr("disabled", true);
							}else{
								uiDisplayConsigneeBag.attr("disabled", false);
							}


					});
				}
			});



		

	}



	function bindEvents(sAction, ui)
	{
		if(sCurrentPath.indexOf("consignee_receiving") > -1){
			
			_getAllReceivingConsignee(function(oData){
				receiving_view_all = oData;

				renderElement('buildAllReceivingConsignee', oData);
			});

		}else if(sCurrentPath.indexOf("receiving/consignee_create") > -1){

			cr8v_platform.localStorage.delete_local_storage({name:"dataUploadedDocument"});

			_getCurrentDateTime();

			_documentEvents(true);

			_notesEvent();

			_createCancelConfirmation();

			_triggerModeOfDelivery();

			_getProducts(function(oData){
				oProducts = oData;

				_buildProductListDropDown(oData);

				_triggerAddProductItem();
			});

			_getVesselName(function(oData){
				_buildVesselNameDropDown(oData);
				_buildVesselNameForTruckDropDown(oData);
			});

			_getUnitOfMeasure(function(oData){

				cr8v_platform.localStorage.set_local_storage({
					"name" : "unit_of_measures",
					"data" : oData
				});


				var intervalMeasures = setInterval(function(){
					var getSelecteddata = cr8v_platform.localStorage.get_local_storage({name:"unit_of_measures"});
					if(getSelecteddata !== null)
					{
						_buildUnitMeasureDropDown($(".bill-lading-container"));
						clearInterval(intervalMeasures);
					}
				}, 300);

			});

			_getConsignee(function(oData){
				oConsigneeName = oData;
				oTempConsigneeName = oData;
			});

			
			CDropDownRetract.retractDropdown($("#addSelectModeDelivery"));
			

			setTimeout(function(){
				$('#generate-data-view').removeClass('showed');
			},500);



			_itemsEvent();

			_numberOnlyQuantity();
			_checkCreatingReceivingConsignee();

		}else if(sCurrentPath.indexOf("receiving/consignee_ongoing") > -1){

			

			_getSelectedReceivingConsignee(function(oData){
				oSelectedData = oData;
				// console.log(JSON.stringify(oSelectedData));
				renderElement('renderReceivingConsigneeData', oSelectedData);
				
			});

			_getConsignee(function(oData){
				oConsigneeName = oData;
				oTempConsigneeName = oData;
			});

			_getAllStorage(function(oData){
				oStorages = oData;
			});

			_getUnitOfMeasure(function(oData){

				cr8v_platform.localStorage.set_local_storage({
					"name" : "unit_of_measures",
					"data" : oData
				});


				var intervalMeasures = setInterval(function(){
					var getSelecteddata = cr8v_platform.localStorage.get_local_storage({name:"unit_of_measures"});
					if(getSelecteddata !== null)
					{
						clearInterval(intervalMeasures);
					}
				}, 300);

			});

			
			setTimeout(function(){
				$('#generated-data-view').removeClass('showed');
			},500);

		}
	}



	function renderElement(sType, oData)
	{
		if(sCurrentPath.indexOf("consignee_receiving") > -1){
			if(sType == 'buildAllReceivingConsignee' && Object.keys(oData).length > 0)
			{	
				_displayAllReceivingConsignee(oData);
			}else{
				$("#displayAllReceivingConsigneeList").hide();
			}
		}else if(sCurrentPath.indexOf("receiving/consignee_ongoing") > -1){

			if(sType == 'renderReceivingConsigneeData' && Object.keys(oData).length > 0)
			{	
				_displaySelectedReceivingConsignee(oData);
			}

		}
	}

	return {
		bindEvents : bindEvents,
		renderElement : renderElement
	}

})();

$(document).ready(function(){

	if(sCurrentPath.indexOf("receiving/consignee_receiving") > -1){
		CCReceivingConsigneeGoods.bindEvents();

	}else if(sCurrentPath.indexOf("receiving/consignee_create") > -1){
		CCReceivingConsigneeGoods.bindEvents();

	}else if(sCurrentPath.indexOf("receiving/consignee_ongoing") > -1){
		CCReceivingConsigneeGoods.bindEvents();
	}

	

});