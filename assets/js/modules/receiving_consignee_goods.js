
var CCReceivingConsigneeGoods = (function(){
	var failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg",
		arrGetAllProducts = {},
		oStoredDataFromProducts = [],
		oStoredReceiveConsigneeProduct = [],
		oStoredReceiveConsigneeProductIds = [],
		oStoredReceiveConsigneeItems = [],
		oDisplayGetAll = [],
		oCompeleteEditProduct = [],
		oUploadedDocuments = {},
		oItemReceivingBalance = {},
		oBagsItemBalance = {},
		oStorages = {},
		oBatchPerItem = {},
		bBatchEditMode = false,
		sBatchEditKey = "",
		oConsigneeNamesCollects = '';


	function _add()
	{
		var oStoredReceiveConsigneeProductItems = cr8v_platform.localStorage.get_local_storage({name:"getAllReceivingConsigneeProdutcts"}),
			oStoredReceiveConsigneeNoteItems = cr8v_platform.localStorage.get_local_storage({name:"setReceiveConsigneeAddNoteStored"}),
			oStoredReceiveConsigneeModeDelivery = cr8v_platform.localStorage.get_local_storage({name:"modeOfDelivery"}),
			oStoredReceiveConsigneeItems = cr8v_platform.localStorage.get_local_storage({name:"CurrentReceivingConsigneeItems"}),
			oStoredReceivedConsigneeDocuments = cr8v_platform.localStorage.get_local_storage({name:"dataUploadedDocument"}),
			sPurchaseOrder = $("#addInputPoNumber").val(),
			iReceivedNumber = $("#setReceivingNumber").val(),
			sModeDelivery  = $("#addSelectModeDelivery").val();


			// var oData  =  {
			// 		"receiving_number":iReceivedNumber,
			// 		"purchase_order" : sPurchaseOrder,
			// 		"mode_of_delivery":sModeDelivery,
			// 		"product_data":oStoredReceiveConsigneeProductItems,
			// 		"note_data":oStoredReceiveConsigneeNoteItems,
			// 		"delivery_mode_data":oStoredReceiveConsigneeModeDelivery,
			// 		"consignee_items":oStoredReceiveConsigneeItems,
			// 		"uploaded_documents":oStoredReceivedConsigneeDocuments
			// 	 }

			// 	console.log( JSON.stringify(oData));

			

		var oOptions = {
			url : BASEURL+"receiving/add_receiving_consignee_goods",
			type : "POST",
			data : {
					"receiving_number":iReceivedNumber,
					"purchase_order" : sPurchaseOrder,
					"mode_of_delivery":sModeDelivery,
					"product_data":JSON.stringify(oStoredReceiveConsigneeProductItems),
					"note_data":JSON.stringify(oStoredReceiveConsigneeNoteItems),
					"delivery_mode_data":JSON.stringify(oStoredReceiveConsigneeModeDelivery),
					"consignee_items":JSON.stringify(oStoredReceiveConsigneeItems),
					"uploaded_documents":JSON.stringify(oStoredReceivedConsigneeDocuments)
				 },
			async : false,
			success : function(oResponse)
			{
				console.log(oResponse);

				var options = {title : "Successfully Added", message : "ConsigneeGoods added!", speed : "slow", withShadow : true, type: "success"};
				$("body").feedback(options);
	
				
				cr8v_platform.localStorage.delete_local_storage({name:"displayAllReceivingConsigneeData"});
				cr8v_platform.localStorage.delete_local_storage({name:"getAllConsigneeReceiveConsignee"});
				cr8v_platform.localStorage.delete_local_storage({name:"getAllProductsReceiveConsignee"});
				cr8v_platform.localStorage.delete_local_storage({name:"getAllVesselData"});
				cr8v_platform.localStorage.delete_local_storage({name:"getAllVesselName"});
				cr8v_platform.localStorage.delete_local_storage({name:"SetSelectedProductForReceivingConsignee"});
				cr8v_platform.localStorage.delete_local_storage({name:"dataUploadedDocument"});
				cr8v_platform.localStorage.delete_local_storage({name:"getAllReceivingConsigneeProdutcts"});
				cr8v_platform.localStorage.delete_local_storage({name:"setReceiveConsigneeAddNoteStored"});
				cr8v_platform.localStorage.delete_local_storage({name:"modeOfDelivery"});
				cr8v_platform.localStorage.delete_local_storage({name:"CurrentReceivingConsigneeItems"});
				cr8v_platform.localStorage.delete_local_storage({name:"setNewConsigneeRow"});
				
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	function _update()
	{
		var oStoredReceiveConsigneeProductItems = cr8v_platform.localStorage.get_local_storage({name:"getAllReceivingConsigneeProdutcts"}),
			oStoredReceiveConsigneeNoteItems = cr8v_platform.localStorage.get_local_storage({name:"setReceiveConsigneeAddNoteStored"}),
			oStoredReceiveConsigneeModeDelivery = cr8v_platform.localStorage.get_local_storage({name:"modeOfDelivery"}),
			oStoredReceiveConsigneeItems = cr8v_platform.localStorage.get_local_storage({name:"CurrentReceivingConsigneeItems"}),
			oGetCurrentSelectedReceivedConsignee = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"}),
			oSetSelectedReceivingConsigneeItem = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"}),
			sPurchaseOrder = $("#display-edit-consignee-vessel-poNum").val(),
			iReceivedNumber = $("#setReceivingNumber").val();
			// sModeDelivery  = $("#display-edit-vessel-mode-of-delivery").val();

			// console.log("ok");
			// console.log(JSON.stringify(oStoredReceiveConsigneeModeDelivery))
			

			// var odata  = {
			// 		"receiving_id":oGetCurrentSelectedReceivedConsignee.value,
			// 		"purchase_order" : sPurchaseOrder,
			// 		"mode_of_delivery":sModeDelivery,
			// 		"product_data":oStoredReceiveConsigneeProductItems,
			// 		"delivery_mode_data":oStoredReceiveConsigneeModeDelivery,
			// 		"consignee_items":oStoredReceiveConsigneeItems
			// 	 }


			// 	 console.log(JSON.stringify(odata));


		var oOptions = {
			url : BASEURL+"receiving/update_receiving_consignee_goods",
			type : "POST",
			data : {
					"receiving_id":oGetCurrentSelectedReceivedConsignee.value,
					"purchase_order" : sPurchaseOrder,
					//"mode_of_delivery":sModeDelivery,
					"product_data":JSON.stringify(oStoredReceiveConsigneeProductItems),
					"delivery_mode_data":JSON.stringify(oStoredReceiveConsigneeModeDelivery),
					"consignee_items":JSON.stringify(oStoredReceiveConsigneeItems)
				 },
			async : false,
			success : function(oResponse)
			{
				console.log(oResponse);

				var options = {title : "Successfully Added", message : "ConsigneeGoods added!", speed : "slow", withShadow : true, type: "success"};
				$("body").feedback(options);


					window.location.href = BASEURL+"receiving/consignee_ongoing_vessel";
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);

		cr8v_platform.localStorage.delete_local_storage({name:"setSelectedReceivingConsigneeItem"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllReceivingConsigneeProdutcts"});
		cr8v_platform.localStorage.delete_local_storage({name:"setReceiveConsigneeAddNoteStored"});
		cr8v_platform.localStorage.delete_local_storage({name:"modeOfDelivery"});
		cr8v_platform.localStorage.delete_local_storage({name:"CurrentReceivingConsigneeItems"});
		// cr8v_platform.localStorage.delete_local_storage({name:"setCurrentDisplayReceivedId"})
		cr8v_platform.localStorage.delete_local_storage({name:"setNewConsigneeRow"});
		cr8v_platform.localStorage.delete_local_storage({name:"displayAllReceivingConsigneeData"});
		cr8v_platform.localStorage.delete_local_storage({name:"currentReceivingNumber"});

	}

	function _updateTruck()
	{
		var oStoredReceiveConsigneeProductItems = cr8v_platform.localStorage.get_local_storage({name:"getAllReceivingConsigneeProdutcts"}),
			oStoredReceiveConsigneeNoteItems = cr8v_platform.localStorage.get_local_storage({name:"setReceiveConsigneeAddNoteStored"}),
			oStoredReceiveConsigneeModeDelivery = cr8v_platform.localStorage.get_local_storage({name:"modeOfDelivery"}),
			oStoredReceiveConsigneeItems = cr8v_platform.localStorage.get_local_storage({name:"CurrentReceivingConsigneeItems"}),
			oGetCurrentSelectedReceivedConsignee = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"}),
			oSetSelectedReceivingConsigneeItem = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"}),
			sPurchaseOrder = $("#display-edit-consignee-truck-poNum").val(),
			iReceivedNumber = $("#setReceivingNumber").val(),
			sModeDelivery  = $("#display-edit-truck-mode-of-delivery").val();

			// console.log("ok");
			console.log(JSON.stringify(oStoredReceiveConsigneeItems))

		var oOptions = {
			url : BASEURL+"receiving/update_receiving_consignee_goods",
			type : "POST",
			data : {
					"receiving_id":oGetCurrentSelectedReceivedConsignee.value,
					"purchase_order" : sPurchaseOrder,
					"mode_of_delivery":sModeDelivery,
					"product_data":JSON.stringify(oStoredReceiveConsigneeProductItems),
					"delivery_mode_data":JSON.stringify(oStoredReceiveConsigneeModeDelivery),
					"consignee_items":JSON.stringify(oStoredReceiveConsigneeItems)
				 },
			async : false,
			success : function(oResponse)
			{
				console.log(oResponse);

				var options = {title : "Successfully Added", message : "ConsigneeGoods added!", speed : "slow", withShadow : true, type: "success"};
				$("body").feedback(options);

					window.location.href = BASEURL+"receiving/consignee_ongoing_truck";

				
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);

		cr8v_platform.localStorage.delete_local_storage({name:"setSelectedReceivingConsigneeItem"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllReceivingConsigneeProdutcts"});
		cr8v_platform.localStorage.delete_local_storage({name:"setReceiveConsigneeAddNoteStored"});
		cr8v_platform.localStorage.delete_local_storage({name:"modeOfDelivery"});
		cr8v_platform.localStorage.delete_local_storage({name:"CurrentReceivingConsigneeItems"});
		// cr8v_platform.localStorage.delete_local_storage({name:"setCurrentDisplayReceivedId"})
		cr8v_platform.localStorage.delete_local_storage({name:"setNewConsigneeRow"});
		cr8v_platform.localStorage.delete_local_storage({name:"displayAllReceivingConsigneeData"});
		cr8v_platform.localStorage.delete_local_storage({name:"currentReceivingNumber"});

	}

	function _saveUpdateProductPerItem(oData)
	{
		var receivingID = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"});

		var oOptions = {
			url : BASEURL+"receiving/save_update_product_per_item",
			type : "POST",
			data : {"receiving_id":receivingID.value, "data":JSON.stringify(oData)},
			success : function(oResponse){
				// console.log(oResponse);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _addNotes()
	{
		var getCurrentSelectedReceivedConsignee = cr8v_platform.localStorage.get_local_storage({name:"addReceivingNotesSave"});

		// console.log(JSON.stringify(getCurrentSelectedReceivedConsignee));

		var oOptions = {
			url : BASEURL+"/receiving/add_selected_receiving_notes",
			type : "POST",
			data : {"note_information": JSON.stringify(getCurrentSelectedReceivedConsignee)},
			async : false,
			success: function(oResponse)
			{
				var options = {title : "Add Recieving Notes", message : "Successfully Added", speed : "slow", withShadow : true, type : "success",};
				$("body").feedback(options);
				console.log(JSON.stringify(oResponse.data));

				$("#display-receiving-notes").load(location.href + " #display-receiving-notes", function(){
						_dispalyReceivingNotes(oResponse.data);
						// CCategories.get();
						// $(".close-me").trigger("click");
					});
				
				


				cr8v_platform.localStorage.delete_local_storage({name:"addReceivingNotesSave"});
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	function getProducts()
	{
		var oOptions = {
			url : BASEURL+"/receiving/get_all_products",
			type : "GET",
			data : {"oparams": "no"},
			async : false,
			success: function(oResponse)
			{
				// console.log(JSON.stringify(oResponse.data));
				// arrGetAllProducts = oResponse.data;

				if(oResponse.data === null){

				}else{
					cr8v_platform.localStorage.set_local_storage({
						name : "getAllProductsReceiveConsignee",
						data : oResponse.data
					});
					CCReceivingConsigneeGoods.renderElement("displayItemsForProductList", oResponse.data);
					CCReceivingConsigneeGoods.renderElement("displayItemsForProductListTruck", oResponse.data);
					CCReceivingConsigneeGoods.bindEvents("AddNewItemProdReceivingConsigneeGoods");

				}

				
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getVesselName()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "receiving/get_vessel_name",
			returnType : "json",
			success : function(oResult) {

				if(oResult.data === null){

				}else{
					cr8v_platform.localStorage.set_local_storage({
						name : "getAllVesselName",
						data : oResult.data
					});
					CCReceivingConsigneeGoods.renderElement("displayItemsForVesselList", oResult.data);
					CCReceivingConsigneeGoods.renderElement("displayItemsForVesselListForTruck", oResult.data);
				}

			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getAllStorage()
	{
		var oOptions = {
			type : "GET",
			data : {test : true},
			returnType : "json",
			url : BASEURL + "storages/get_all_storages",
			success : function(oResult) {
				oStorages = oResult;
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getCompleteTruckInfo()
	{

		var oReceivingId = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"});

		console.log(oReceivingId);

		var oOptions = {
				url : BASEURL+"receiving/get_receiving_consignee_truck_complete",
				type : "POST",
				data: {"receiving_id":oReceivingId.value},
				returnType: "json",
				beforeSend: function(){
					$('#generated-data-view').addClass('showed');
				},
				success: function(oResult){
					if(oResult.data === null)
					{
						console.log("Not Found");
					}else{

						CCReceivingConsigneeGoods.renderElement("displayCompletedConsigneeGoodsTruck", oResult.data);


						cr8v_platform.localStorage.set_local_storage({
							name : "setSelectedReceivingConsigneeItemComplete",
							data : oResult.data
						});
						// console.log(JSON.stringify(oResult.data));
					}
				}
			}

			cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getCompleteVesselInfo()
	{

		var oReceivingId = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"});

		// console.log(oReceivingId);

		var oOptions = {
				url : BASEURL+"receiving/get_receiving_consignee_vessel_complete",
				type : "POST",
				data: {"receiving_id":oReceivingId.value},
				returnType: "json",
				beforeSend: function(){
					$('#generated-data-view').addClass('showed');
				},
				success: function(oResult){
					if(oResult.data === null)
					{
						console.log("Not Found");
					}else{

						CCReceivingConsigneeGoods.renderElement("displayCompletedConsigneeGoodsVessel", oResult.data);

						cr8v_platform.localStorage.set_local_storage({
							name : "setSelectedReceivingConsigneeItemComplete",
							data : oResult.data
						});
						// console.log(JSON.stringify(oResult.data));
					}
				}
			}

			cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _getCurrentDateTime()
	{
		var oOptions = {
			type : "POST",
			data : { data : "none" },
			url : BASEURL + "receiving/get_current_date_time",
			returnType : "json",
			success : function(oResult) {

				cr8v_platform.localStorage.set_local_storage({
					name : "setCurrentDateTime",
					data : {"date":oResult.data.date, "dateFormat":oResult.data.date_formatted}
				})
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getConsignee()
	{
		var oOptions = {
			url : BASEURL+"consignee/get_consignee_names",
			type : "GET",
			data : {"oparams": "no"},
			async : false,
			success: function(oResponse)
			{
				// console.log(JSON.stringify(oResponse.data));
				// arrGetAllProducts = oResponse.data;

				if(oResponse.data === null){
					cr8v_platform.localStorage.set_local_storage({
						name : "getAllConsigneeReceiveConsignee",
						data : ""
					});
				}else{
					cr8v_platform.localStorage.set_local_storage({
						name : "getAllConsigneeReceiveConsignee",
						data : oResponse.data
					});

					oConsigneeNamesCollects = oResponse.data;

					//console.log(oConsigneeNamesCollects);
				}

				
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function getUnitOfMeasure()
	{
		var oOptions = {
			type : "GET",
			data : { data : "none" },
			url : BASEURL + "products/get_unit_of_measure",
			returnType : "json",
			success : function(oResult) {
				// console.log(JSON.stringify(oResult.data));
				if(oResult.data === null)
				{
					cr8v_platform.localStorage.set_local_storage({
						"name" : "unit_of_measures",
						"data" : ""
					});
				}else{
					cr8v_platform.localStorage.set_local_storage({
						"name" : "unit_of_measures",
						"data" : oResult.data
					});
				}

				if(typeof callBack == 'function'){
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function displayAllReceivingConsignee()
	{


		var oOptions = {
			url : BASEURL+"receiving/display_all_receiving_consignee_goods",
			type : "GET",
			data : {"data":"none"},
			returnType : "json",
			async : false,
			success : function(oResult){
				if(oResult.data === null)
				{

				}else{
					oDisplayGetAll = oResult.data;
					// console.log(JSON.stringify(oDisplayGetAll));
					cr8v_platform.localStorage.set_local_storage({
						name:"displayAllReceivingConsigneeData",
						data:oDisplayGetAll
					});
				}
			}

		} 
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function displaySelectedReceivingConsigneeVessel()
	{
		var getCurrentSelectedReceivedConsignee = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"});
		// console.log(getCurrentSelectedReceivedConsignee.value);
		var oOptions = {
			url : BASEURL+"receiving/get_current_selected_receiving_consignee_vessel",
			type : "POST",
			data: {"receiving_id":getCurrentSelectedReceivedConsignee.value},
			returnType: "json",
			success: function(oResult){
				if(oResult.data === null)
				{
					console.log("Not Found");
				}else{
					cr8v_platform.localStorage.set_local_storage({
						name : "setSelectedReceivingConsigneeItem",
						data : oResult.data
					});
					// console.log(JSON.stringify(oResult.data));
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	function displaySelectedReceivingConsigneeTruck()
	{
		var getCurrentSelectedReceivedConsignee = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"});
		// console.log(getCurrentSelectedReceivedConsignee.value);
		var oOptions = {
			url : BASEURL+"receiving/get_current_selected_receiving_consignee_truck",
			type : "POST",
			data: {"receiving_id":getCurrentSelectedReceivedConsignee.value},
			returnType: "json",
			success: function(oResult){
				if(oResult.data === null)
				{
					console.log("Not Found");
				}else{
					cr8v_platform.localStorage.set_local_storage({
						name : "setSelectedReceivingConsigneeItem",
						data : oResult.data
					});
					// console.log(JSON.stringify(oResult.data));
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);

	}

	function _itemsEvent()
	{
		var methods = {
			displayToSelect : function(oData, bShowResult){
				var oTimeOut = {},
					uiSelect = $('#selectProductListSelect'),
					uiParent = uiSelect.parent(),
					uiSibling = uiParent.find('.frm-custom-dropdown');
				// uiSibling.remove();

				// uiSelect.removeClass("frm-custom-dropdown-origin");

				// uiSelect.html("");

				for(var i in oData){
					var sHtml = '<option value="'+oData[i]['id']+'">'+oData[i]['name']+'</option>';
					uiSelect.append(sHtml);
				}

				uiSelect.transformDD();


				var uiInput = uiParent.find('input.dd-txt');


				if(bShowResult){
					uiParent.find('.frm-custom-icon').click();
					uiInput.focus();
				}

				

				uiInput.off("keyup").on("keyup", function(e){
					var val = $(this).val();
					uiInput.attr("value",val);
					
					clearTimeout(oTimeOut);
					if(val.length > 0 && e.keyCode !=8 && e.keyCode !=48){
						oTimeOut = setTimeout(function(){
							methods.search(val, true);
						}, 3000);	
					}
					

				});
			},
			getItem : function(id){
				for(var i in oItems){
					if(oItems[i]["id"] == id){
						return oItems[i];
					}
				}
			},
			search : function(sKey, bShowResult){
				var oOptions = {
					type : "POST",
					data : {
						key : sKey,
						limit : 30,
						sorting : "asc",
						sort_field : "name"
					},
					url : BASEURL + "products/search",
					returnType : "json",
					success : function(oResult) {
						oItems = oResult.data;
						methods.displayToSelect(oItems, bShowResult);
					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);
			}
		};

		methods.search("");
	}

	function _displayProductItems(oItems)
	{

		// console.log(JSON.stringify(oItems));
		var uiContainer = $("#displayProductItemList"),
			uiTemplate = $('.item-template-product'),
			iCountComplete = 0,
			iCountCheck = 0;

			uiContainer.html("");

		for(var i in oItems){
			var item = oItems[i];
			
			var uiCloneTemplate = uiTemplate.clone();
			uiCloneTemplate.removeClass("item-template-product").show();
			uiCloneTemplate.data(item);
			uiCloneTemplate.find(".display-consignee-sku-name").html(" SKU #"+item.sku+" - "+item.product_name);
			uiCloneTemplate.find(".display-consignee-prod-img").css('background-image', 'url('+item.product_image+')');
			uiCloneTemplate.find(".display-consignee-vendor-name").html(item.vendor_name);
			uiCloneTemplate.find(".display-consignee-loading-method").html(item.loading_method);

			if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1  || sCurrentPath.indexOf("consignee_complete_truck") > -1 )
			{
				uiCloneTemplate.find(".display-consignee-qtr-to-receive").html($.number(item.qty, 0)+" "+item.measures_name.toUpperCase()+" ("+item.bag+" Bags)").data({
					"quantity":item.qty,
					"unitOfMesurement":item.measures_name.toUpperCase()
				});

				uiCloneTemplate.find(".display-bag-per-batch").show();

			}else{
				uiCloneTemplate.find(".display-consignee-qtr-to-receive").html($.number(item.qty, 0)+" "+item.measures_name.toUpperCase()).data({
					"quantity":item.qty,
					"unitOfMesurement":item.measures_name.toUpperCase()
				});

			}

			



			uiCloneTemplate.attr("prod-id", item.product_inventory_id);


			_displayConsigneeDistribution(item.consignee_info, uiCloneTemplate);
			_displayEditConsigneeDistribution(item.consignee_info, uiCloneTemplate);

			
			uiContainer.append(uiCloneTemplate);
			

			_createCollapseProduct(uiCloneTemplate);
			_createEditProduct(oItems);
			_createCancelProduct();
			_createEnterProductDetails();
			_createEnterProductDetailsCancel();
			_displayProductInfoSelected(uiCloneTemplate);

			_addBatchForProduct();


			oItemReceivingBalance[item.product_inventory_id] = parseFloat(item.qty);
			
			 oBatchPerItem[item.product_inventory_id] = {};

			 var iGetTotalBag = 0;

			 if(item.hasOwnProperty("storage_assignment")){
				if(Object.keys(item.storage_assignment).length > 0){
					for(var x in item.storage_assignment){
						var storageAssignment = item.storage_assignment[x],
							batch = storageAssignment.batch;

						for(var n in batch){
							var recordBatch = oBatchPerItem[item.product_inventory_id],
							len = Object.keys(recordBatch).length,
							oArrData = batch[n]["location"];

							var lastArr = oArrData[oArrData.length - 1];
							// console.log(JSON.stringify(lastArr));
							
							oBatchPerItem[item.product_inventory_id][len] = {
								"location" : lastArr,
								"item_id" : item.product_id,
								"batch_name" : batch[n]["name"],
								"batch_amount" : batch[n]["quantity"],
								"bag_amount" : batch[n]["bag_amount"],
								"uom" : item.measures_name,
								"uom_id" : item.unit_of_measure_id,
								"hierarchy" : batch[n]["location"]
							};

							iGetTotalBag += parseInt(batch[n]["bag_amount"]);
							
						}
					}

					oBagsItemBalance[item.product_inventory_id] = parseInt(item.bag) - iGetTotalBag;

					_displayBatches(item.product_inventory_id);

					iCountComplete++;

				}
			}

			// console.log(item);
			if(Object.keys(oBatchPerItem[item.product_inventory_id]).length > 0){
				uiCloneTemplate.find(".gray-color").removeClass("gray-color");
			}



			if(item.hasOwnProperty("discrepancy")){
				if(Object.keys(item.discrepancy).length > 0){

					var oGatherDiscrepancyData = {
						received_quantity : item.discrepancy.received_quantity,
						discrepancy_type : item.discrepancy.type,
						discrepancy_value : item.discrepancy.discrepancy_quantity,
						discrepancy_reason : item.discrepancy.remarks,
						discrepancy_cause : item.discrepancy.cause,
						unit_of_measure : item.measures_name.toUpperCase()
					}

					_displayQuantityCheckSuccess(uiCloneTemplate, oGatherDiscrepancyData);

				}
			}

			iCountCheck++;


		}

		//console.log(iCountComplete);
		//console.log(iCountCheck);

		if(iCountComplete == iCountCheck)
		{
			$('.complete-receiving-record').attr('disabled',false);

			_addCompleteReceivingRecord();
			
		}else{
			$('.complete-receiving-record').attr('disabled',true);
		}


		_saveUpdateCosigneeProductDetails();
		_CheckOngoingReceivingConsignee()

	}

	function _displayQuantityCheckProduct(iTotal)
	{
		var uiTemplateContainer =  $(".record-item"),
			oGatherDiscrepancy = {},
			oStoredDiscrepancy = [];

			$.each(uiTemplateContainer, function(){
				var uiThis = $(this),
					uiQuantityInput = uiThis.find('.set-receive-quantity'),
					iProdId =uiThis.data("product_id"),
					iProdInventoryId =uiThis.data("product_inventory_id"),
					iUnitMeasureId = uiThis.data("unit_of_measure_id"),
					uiGetCause = uiThis.find('.set-discrepancy-cause'),
					uiGetRemarks = uiThis.find('.set-discrepancy-remarks'),
					total = 0,
					sthisQuantity = 0;

					

					uiQuantityInput.off("keyup.setQuantity").on("keyup.setQuantity", function(){
						var uiThisInput = $(this),
							uiGetQuantityData = uiThis.find(".display-consignee-qtr-to-receive"),
							sGetProdUnitMeasurement = uiGetQuantityData.data("unitOfMesurement"),
							iGetProdQuantity = uiGetQuantityData.data("quantity"),
							iProductQuantity = parseFloat(iGetProdQuantity),
							sThisVal =uiThisInput.val(),
							uiDisplayDiscrepancy = uiThis.find(".display-discrepancy-result");


							sthisQuantity = parseFloat(sThisVal);

							uiQuantityInput.number(true, 2);


							if(sthisQuantity < iProductQuantity){
								var total = Math.round((iProductQuantity - sthisQuantity) * 100) / 100;
								uiDisplayDiscrepancy.html($.number(total, 2)+" "+sGetProdUnitMeasurement);
								uiDisplayDiscrepancy.data("discrepancy", {
									"value" : total,
									"status" : "down"
								});
								// console.log("down");


							}else if(sthisQuantity > iProductQuantity){
								var total = Math.round((sthisQuantity - iProductQuantity) * 100) / 100;
								uiDisplayDiscrepancy.html("+"+$.number(total, 2)+" "+sGetProdUnitMeasurement);
								uiDisplayDiscrepancy.data("discrepancy", {
									"value" : total,
									"status" : "up"
								});
								// console.log("up");
							}else{
								if(sthisQuantity.length > 0){
									$(this).val(iProductQuantity);
									uiDisplayDiscrepancy.html(" 0 "+sGetProdUnitMeasurement);
									uiDisplayDiscrepancy.data("discrepancy", {
										"value" : 0,
										"status" : "none"
									});
									return false;
								}else{
									uiDisplayDiscrepancy.html($.number(iProductQuantity, 2)+" "+sGetProdUnitMeasurement);
									uiDisplayDiscrepancy.data("discrepancy",{
										"value" : iProductQuantity,
										"status" : "none"
									});
								}
								// console.log("none");
							}

							


					});

				oGatherDiscrepancy = {
					product_inventory_qty_id : iProdInventoryId,
					product_id : iProdId,
					discrepancy_quantity : total,
					received_quantity : sthisQuantity,
					type : iUnitMeasureId,
					cause : uiGetCause.val(),
					remarks : uiGetRemarks.val()
				}

				oStoredDiscrepancy.push(oGatherDiscrepancy);

							

				

			});

			cr8v_platform.localStorage.set_local_storage({
				name : "setCurrentQuantityCheck",
				data : oStoredDiscrepancy
			});

	}

	function _validateProductQuantityCheck()
	{
		//This is for validation 
		$("#validateProdcutQuantityForm").cr8vformvalidation({
			'preventDefault' : true,
		    'data_validation' : 'datavalid',
		    'input_label' : 'labelinput',
		    'onValidationError': function(arrMessages) {
		        // alert(JSON.stringify(arrMessages)); //shows the errors
		        var options = {title : "Quantity Check Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger",};
				$("body").feedback(options);
	        },
		    'onValidationSuccess': function() {

		 


				var	uiTemplateContainer =  $(".record-item"),
					oGatherDiscrepancy = {},
					oStoredDiscrepancy = [];

				$.each(uiTemplateContainer, function(){
					var uiThis = $(this),
						uiQuantityInput = uiThis.find('.set-receive-quantity'),
						iProdId =uiThis.data("product_id"),
						iProdInventoryId =uiThis.data("product_inventory_id"),
						iUnitMeasureId = uiThis.data("unit_of_measure_id"),
						uiGetCause = uiThis.find('.set-discrepancy-cause'),
						uiGetRemarks = uiThis.find('.set-discrepancy-remarks'),
						uiGetQuantityData = uiThis.find(".display-consignee-qtr-to-receive"),
						sGetProdUnitMeasurement = uiGetQuantityData.data("unitOfMesurement"),
						iGetProdQuantity = uiGetQuantityData.data("quantity"),
						iProductQuantity = parseFloat(iGetProdQuantity),
						sThisVal = parseFloat(uiQuantityInput.val()),
						uiDisplayDiscrepancy = uiThis.find(".display-discrepancy-result");
						oGetdata = uiDisplayDiscrepancy.data("discrepancy"),
						iTotal = oGetdata.value;

						// console.log(sThisVal);



					oGatherDiscrepancy = {
						product_inventory_qty_id : iProdInventoryId,
						product_id : iProdId,
						discrepancy_quantity : iTotal,
						received_quantity : sThisVal,
						type : iUnitMeasureId,
						cause : uiGetCause.val(),
						remarks : uiGetRemarks.val()
					}



					oStoredDiscrepancy.push(oGatherDiscrepancy);

				});

				cr8v_platform.localStorage.set_local_storage({
					name : "setCurrentQuantityCheck",
					data : oStoredDiscrepancy
				});



				var oCurrentProductQuantity = cr8v_platform.localStorage.get_local_storage({name:"setCurrentQuantityCheck"});

				console.log(JSON.stringify(oCurrentProductQuantity));
				
	        }
		});
	}

	function _validateBatchAndQuantityCheck()
	{


		var error = 0,
			uiItemContainer = $("#displayProductItemList"),
			uiItems = uiItemContainer.find("[prod-id]"),
			errorMessage = [],
			oProductsData = {};

			$.each(uiItems, function(){
				var uiThis = $(this),
					data = uiThis.data(),
					productName = data.product_name,
					oBatch = data.batch,
					uiReceiveQuantity = uiThis.find('.set-receive-quantity'),
					uiDiscrepancy = uiThis.find(".display-discrepancy-result"),
					dataDiscrepancy = uiDiscrepancy.data("discrepancy"),
					uiSelectCause = uiThis.find('.set-discrepancy-cause'),
					uiReason = uiThis.find('.set-discrepancy-remarks'),
					uiDisplayConsigneeQtrToReceive = uiThis.find(".display-consignee-qtr-to-receive").data(),
					uiSecondText = uiThis.find(".second-text").data(),
					iToReceiveValue = parseFloat(uiDisplayConsigneeQtrToReceive.quantity),
					iTotalBatch = parseFloat(uiSecondText.total_batch_quantity),
					iLen = Object.keys(oProductsData).length;


				
				oProductsData[iLen] = {
					"receiving_item_record_id" : data.product_inventory_id,
					"product_id" : data.product_id
				};

				if(!oBatch){
					error++;
					errorMessage.push("Add Batch For -"+productName);
				}else{
					oProductsData[iLen]["batch"] = oBatch;
				}

				if(uiReceiveQuantity.val().trim().length == 0){
					error++;
					errorMessage.push("Add Received Quantity -"+productName);
				}else{
					oProductsData[iLen]["received_quantity"] = parseFloat(uiReceiveQuantity.val());
				}

					// if(iToReceiveValue != iTotalBatch)
					// {
						if(!dataDiscrepancy){
						error++;
						errorMessage.push("Add Discrepancy -"+productName);
					// }else{
						if(dataDiscrepancy.status == "up" || dataDiscrepancy.status == "down"){
							if(uiReason.val().trim().length == 0){
								error++;
								errorMessage.push("Add Reason For Discrepancy -"+productName);
							}
						}else{
							var discrepancyValue = parseFloat(dataDiscrepancy.value);
							if(discrepancyValue > 0 && uiReason.val().trim().length == 0){
								error++;
								errorMessage.push("Add Reason For Discrepancy -"+productName);
							}
						}
						
						oProductsData[iLen]["discrepancy_type"] = dataDiscrepancy.status;
						oProductsData[iLen]["discrepancy_value"] = dataDiscrepancy.value;
						oProductsData[iLen]["discrepancy_reason"] = uiReason.val();
						oProductsData[iLen]["discrepancy_cause"] = uiSelectCause.val();
					// }
				}

				

				if(oItemReceivingBalance[data.product_inventory_id]){
					var parsedData = parseFloat(oItemReceivingBalance[data.product_inventory_id]);
					if(parsedData != 0){
						error++;
						errorMessage.push("Storage Assignment Invalid -"+productName);
					}
				}else if(oItemReceivingBalance[data.product_inventory_id] != 0){
					error++;
					errorMessage.push("Storage Assignment Invalid -"+productName);
				}


			});

		

			if(error > 0){
				$("body").feedback({title : "Invalid Input", message : errorMessage.join("<br />"), type : "danger", icon : failIcon});
			}else{
				var uiModal = $('[modal-id="are-you-sure"]'),
					uiClose = uiModal.find(".close-me"),
					uiConfirm = uiModal.find("button.submit-btn");
				
				uiModal.addClass("showed");

				uiConfirm.off("click").on("click", function(){
					uiClose.click();
					_completeProductDetails(oProductsData);
				});

				uiClose.off("click").on("click", function(){
					uiModal.removeClass("showed");
				});
			}

		
	}

	function _completeProductDetails(oProductsData)
	{
		var oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "setCurrentDisplayReceivedId"}),
			receivingId = oRecord.value,
			uiModal = $('[modal-id="complete-product-details"]');

		var oOptions = {
			type : "POST",
			data : {
				"receiving_id" : receivingId,
				"receiving_record" : JSON.stringify(oProductsData)
			},
			url : BASEURL + "receiving/complete_product_details",
			returnType : "json",
			beforeSend: function(){
				uiModal.addClass('showed');
			},
			success : function(oResult) {
				
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}


	function _addBatchForProduct()
	{
		var uiTemplateContainer =  $(".record-item"),
			batchModal = $('[modal-id="add-batch"]'),
			batchModalClose = batchModal.find(".close-me"),
			confirmAddBatch = $("#confirmAddBatch"),
			oGatherDiscrepancy = {},
			oStoredDiscrepancy = [];



			// console.log();

			$.each(uiTemplateContainer, function(){
				var uiThisParent = $(this),
					oRecord = uiThisParent.data(),
					uiShowBatchModalBtn = uiThisParent.find('[modal-target="add-batch"]');


					uiShowBatchModalBtn.off("click.addBatch").on("click.addBatch", function(e){

						e.preventDefault();


						batchModal.addClass('showed');

						batchModal.find(".modal-body").find(".modal-head").find(".text-left").html("Add Batch");


							if(typeof oBagsItemBalance[oRecord.product_inventory_id] == 'undefined')
							{
								oBagsItemBalance[oRecord.product_inventory_id] = parseInt(oRecord.bag);
							}

							//console.log(oBagsItemBalance[oRecord.product_inventory_id]);

						var	uiGetQuantityData = uiThisParent.find(".display-consignee-qtr-to-receive"),
							sGetProdUnitMeasurement = uiGetQuantityData.data("unitOfMesurement"),
							uigetDiscrepancy = uiThisParent.find(".discrepancy"),
							oDiscrepancy= uigetDiscrepancy.data(),
							itemReceivingBalance = 0,
							iBagBalance = oBagsItemBalance[oRecord.product_inventory_id],
							iQtyToReceive = uiGetQuantityData.data("quantity");

							if(typeof oDiscrepancy.discrepancy == 'undefined')
							{
								itemReceivingBalance = oItemReceivingBalance[oRecord.product_inventory_id];
								
							}
							else
							{

								var iTotalBalance = parseFloat(iQtyToReceive) - parseFloat(oDiscrepancy.discrepancy.total);
								itemReceivingBalance = iTotalBalance;


							}


						if(bBatchEditMode){
							var batchAmount = parseFloat(oBatchPerItem[oRecord.product_inventory_id][sBatchEditKey]["batch_amount"]);
							itemReceivingBalance = itemReceivingBalance + batchAmount;
						}


						var uiNameSku = batchModal.find(".product-name-sku"),
							uiVesselName = batchModal.find(".product-vessel-name"),
							uiInputName = batchModal.find(".input-batch-name"),
							uiToReceive = batchModal.find(".to-receive"),
							uiInputAmount = batchModal.find(".input-batch-amount"),
							uiAmountUom = batchModal.find(".batch-amount-uom"),
							uiReceiveBal = batchModal.find(".receiving-balance"),
							iReceivingTotalBal = parseFloat(itemReceivingBalance),
							uiGetVesselName = $("#display-selected-vessel-name-temp"),
							uiInputBagsAmount = batchModal.find(".input-bags-amount"),
							uiBagsInputDivs = batchModal.find(".bags-input-divs"),
							uiBagQuantityLabel = batchModal.find(".to-receive-bags"),
							uiBalanceBags = batchModal.find(".receiving-balance-bags"),
							sVesselName = uiGetVesselName.text().trim(),
							iBagStored = 0,
							iQtyStored = 0,
							timeoutType = {};


							if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1){
								if(oRecord.loading_method == "Bags")
								{
									uiBagsInputDivs.show();
									uiInputBagsAmount.val("");
									uiBagQuantityLabel.show();

								}else{
									uiBagsInputDivs.hide();
								}
							}



							//For quantity
							if(iReceivingTotalBal < 0){
								uiReceiveBal.html($.number(iReceivingTotalBal, 0)+" "+oRecord.measures_name).css({"color":"red"});
							}else{
								uiReceiveBal.html($.number(iReceivingTotalBal, 0)+" "+oRecord.measures_name).css({"color":"black"});
							}


							//For  Bags
							if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1){
								if(oRecord.loading_method == "Bags")
								{
									if(iBagBalance < 0){
										uiBalanceBags.html("( "+$.number(iBagBalance, 0)+" Bags )").css({"color":"red"});
									}else{
										uiBalanceBags.html("( "+$.number(iBagBalance, 0)+" Bags )").css({"color":"black"});
									}

								}else{
									
								}
							}

							uiNameSku.html("SKU No. "+oRecord.sku+" - "+oRecord.product_name);
							uiVesselName.html(sVesselName);
							uiToReceive.html($.number(oRecord.qty, 0)+" "+oRecord.measures_name);

							uiBagQuantityLabel.html("( "+oRecord.bag+" Bags )");
							
							uiAmountUom.html(oRecord.measures_name);
							uiInputAmount.number(true, 0);
							uiInputBagsAmount.number(true, 0);

							uiInputName.val("");
							uiInputAmount.val("");


							uiInputBagsAmount.off("keyup.changeBags").on("keyup.changeBags", function(){
								var uiBagsThis = $(this),
									sBagsVal = uiBagsThis.val(),
									iBagsVal = parseInt(sBagsVal),
									iBagsBal = iBagBalance;

									clearTimeout(timeoutType);

									timeoutType = setTimeout(function(){
									if(sBagsVal.trim().length > 0){
										var iTotal = iBagsBal - iBagsVal;
										if(iTotal > iBagsBal){
											iTotal = iBagsBal;
											uiBalanceBags.html('( - '+$.number(iTotal, 0)+" Bags )").css({"color":"red"});
											//iReceivingTotalBal = iTotal;

											iBagStored = iTotal;
										}else if(iTotal < 0){

											uiBalanceBags.html('( - '+$.number(Math.abs(iTotal), 0)+" Bags )").css({"color":"red"});
											//uiThis.val(iBalance);
											//iReceivingTotalBal = 0;
											iBagStored = iTotal;
										}else{
											if(iTotal < 0)
											{
												uiBalanceBags.html("( "+$.number(iTotal, 0)+" Bags )").css({"color":"red"});

											}else{

												uiBalanceBags.html("( "+$.number(iTotal, 0)+" Bags )").css({"color":"black"});
											}
											//iReceivingTotalBal = iTotal;
											iBagStored = iTotal;
										}
									}else{
										if(iBagsBal < 0)
											{
												uiBalanceBags.html("( "+$.number(iBagsBal, 0)+" Bags )").css({"color":"red"});
											}else{

												uiBalanceBags.html("( "+$.number(iBagsBal, 0)+" Bags )").css({"color":"black"});
											}
										//iReceivingTotalBal = iBalance;
										iBagStored = iBagsBal;
									}

									
							
									clearTimeout(timeoutType);
								},500);

							});


							uiInputAmount.off('keyup.changeamount').on('keyup.changeamount', function(){

								var uiThis = $(this),
									sThisVal = uiThis.val(),
									iThisVal = parseFloat(sThisVal),
									iBalance = parseFloat(itemReceivingBalance),
									uom = oRecord.measures_name;
								
								clearTimeout(timeoutType);
				
								timeoutType = setTimeout(function(){
									if(sThisVal.trim().length > 0){
										var iTotal = iBalance - iThisVal;
										if(iTotal > iBalance){
											iTotal = iBalance;
											uiReceiveBal.html('- '+$.number(iTotal, 0)+" "+uom).css({"color":"red"});
											//iReceivingTotalBal = iTotal;
											iQtyStored = iTotal;
										}else if(iTotal < 0){

											uiReceiveBal.html('- '+$.number(Math.abs(iTotal), 0)+" "+uom).css({"color":"red"});
											//uiThis.val(iBalance);
											//iReceivingTotalBal = 0;
											iQtyStored = iTotal;
										}else{
											if(iTotal < 0)
											{
												uiReceiveBal.html($.number(iTotal, 0)+" "+uom).css({"color":"red"});
											}else{

												uiReceiveBal.html($.number(iTotal, 0)+" "+uom).css({"color":"black"});
											}
											//iReceivingTotalBal = iTotal;
											iQtyStored = iTotal;
										}
									}else{
										if(iBalance < 0)
											{
												uiReceiveBal.html($.number(iBalance, 0)+" "+uom).css({"color":"red"});
											}else{

												uiReceiveBal.html($.number(iBalance, 0)+" "+uom).css({"color":"black"});
											}
											iQtyStored = iBalance;
										//iReceivingTotalBal = iBalance;
									}

									

									clearTimeout(timeoutType);
								},500);

							});

							_displayBatchStorages(oStorages);

							batchModalClose.off('click').on('click', function(){
								batchModal.removeClass("showed");
								bBatchEditMode = false;
							});

							confirmAddBatch.off('click').on('click', function(){
								var uiSelectedLocation = $(".item-bread.temporary"),
									oLocation = uiSelectedLocation.data(),
									error = 0,
									arrErrorMsg = [];
								
								if(oItemReceivingBalance[oRecord.product_inventory_id] == 0 && !bBatchEditMode){
									error++;
									arrErrorMsg.push("Receiving Balance Already Been Used");
								}else{
									
									if(uiInputName.val().trim().length == 0){
										error++;
										arrErrorMsg.push("Enter Batch Name");
									}

									if(!oLocation){
										error++;
										arrErrorMsg.push("Select Location");
									}

									if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1){
											if(oRecord.loading_method == "Bags")
											{
												if(uiInputBagsAmount.val().trim() == "" || uiInputBagsAmount.val().trim() == 0 || uiInputBagsAmount.val().trim() == "0")
												{
													error++;
													arrErrorMsg.push("Enter Bags Amount");
												}

												//if(){}
												
												//console.log(iQtyStored);
												//console.log(iBagStored);
												//console.log(iReceivingTotalBal);
												//console.log(iBagBalance);

												if(iQtyStored < 0){
													error++;
													arrErrorMsg.push("Remaining Balance of Batch Amount is lower than zero");
												}
												else if(iQtyStored == 0 && iBagStored > 0){
													error++;
													arrErrorMsg.push("Quantity has been reach it's limit however ther the Bags are not yet");
												}


												if(iBagStored < 0){
													error++;
													arrErrorMsg.push("Remaining Balance of Bags Amount is lower than zero");
												}
												else if( iBagStored == 0 &&  iQtyStored > 0){
													error++;
													arrErrorMsg.push("Bags has been reach it's limit however ther the Quantity are not yet");
												}
												
											}
										}
									


									




									
								}

								if(error > 0){
									$("body").feedback({title : "Message", message : arrErrorMsg.join("<br />"), type : "danger", icon : failIcon});
								}else{
									// Add batch
									var recordBatch = oBatchPerItem[oRecord.product_inventory_id],
										len = Object.keys(recordBatch).length,
										iBagsAmount = 0,
										iBagsAmountTruck = 0;

										if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1){
											if(oRecord.loading_method == "Bags")
											{
												iBagsAmountTruck = uiInputBagsAmount.val().trim();

												iBagsAmount = parseInt(uiInputBagsAmount.val().trim());

												iBagBalance -= iBagsAmount;
											}
										}
									
										

										

										

									// 	console.log(JSON.stringify(oRecord));

										if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1)
										{
											// Add batch
											if(!bBatchEditMode){
												oBatchPerItem[oRecord.product_inventory_id][len] = {
													"location" : oLocation,
													"item_id" : oRecord.product_id,
													"batch_name" : uiInputName.val(),
													"batch_amount" : uiInputAmount.val(),
													"bag_amount" : iBagsAmountTruck,
													"uom" : oRecord.measures_name,
													"uom_id" : oRecord.unit_of_measure_id,
													"hierarchy" : _getBreadCrumbsHierarchy()
												};
											}else{
												oBatchPerItem[oRecord.product_inventory_id][sBatchEditKey] = {
													"location" : oLocation,
													"item_id" : oRecord.product_id,
													"batch_name" : uiInputName.val(),
													"batch_amount" : uiInputAmount.val(),
													"bag_amount" : iBagsAmountTruck,
													"uom" : oRecord.product_unit_of_measure,
													"uom_id" : oRecord.unit_of_measure_id,
													"hierarchy" : _getBreadCrumbsHierarchy()
												}
												bBatchEditMode = false;
											}
										}else{

											// Add batch
											if(!bBatchEditMode){
												oBatchPerItem[oRecord.product_inventory_id][len] = {
													"location" : oLocation,
													"item_id" : oRecord.product_id,
													"batch_name" : uiInputName.val(),
													"batch_amount" : uiInputAmount.val(),
													"uom" : oRecord.measures_name,
													"uom_id" : oRecord.unit_of_measure_id,
													"hierarchy" : _getBreadCrumbsHierarchy()
												};
											}else{
												oBatchPerItem[oRecord.product_inventory_id][sBatchEditKey] = {
													"location" : oLocation,
													"item_id" : oRecord.product_id,
													"batch_name" : uiInputName.val(),
													"batch_amount" : uiInputAmount.val(),
													"uom" : oRecord.product_unit_of_measure,
													"uom_id" : oRecord.unit_of_measure_id,
													"hierarchy" : _getBreadCrumbsHierarchy()
												}
												bBatchEditMode = false;
											}

										}

										


									oItemReceivingBalance[oRecord.product_inventory_id] = iReceivingTotalBal;

									oBagsItemBalance[oRecord.product_inventory_id] = iBagBalance;
									
									$('[modal-target="add-batch"]').show();
									$("[modal-id='add-batch'] .close-me:visible").click();


									_displayBatches(oRecord.product_inventory_id);

									_displayBatchHover(uiThisParent, "true");


								}

							});

							setTimeout(function(){
								if(typeof callBack == 'function'){
									callBack();
								}
							}, 100);

							// _displayBatchStorages(oStorages);


					});

			});

	}

	function _getBreadCrumbsHierarchy()
	{
		var breadCrumbsHierarchy = $("#breadCrumbsHierarchy"),
			uiMainBread = breadCrumbsHierarchy.find(".bggray-7cace5"),
			oMainBread = uiMainBread.data(),
			oResponse = {};
		
		oResponse[0] = oMainBread;

		var uiNextAllBread = uiMainBread.nextAll(".item-bread");
		$.each(uiNextAllBread, function(){
			var thisData = $(this).data();
			oResponse[Object.keys(oResponse).length] = thisData;
		});

		return oResponse;

	}

	function _displayBatchStorages(oStorages)
	{
		var batchModal = $('[modal-id="add-batch"]'),
			breadCrumbsHierarchy = $("#breadCrumbsHierarchy"),
			batchDisplay = $("#batchDisplay"),
			oSelectedUI = [],
			oDisplayedData = {},
			oToPlaceData = {},
			doubleClickLevel = 0;

		batchDisplay.html("");

		var sTemplate = '<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor hier-item">'+
				'<div class="display-inline-mid width-20percent overflow-hide half-border-radius">'+
					'<img src="" alt="images" class="width-100percent">'+
				'</div>'+
				'<div class="display-inline-mid">'+
					'<p class="font-14 font-400 location-name"></p>'+
					'<p class="font-12 font-400 italic qty-in-location"> </p>'+
				'</div>'+
			'</div>';

		var sBackArrow = '<i style="margin-right:4px;" class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small padding-right-10 border-black back-arrow"></i>',
			sNextIcon = '<span class="display-inline-mid padding-all-5 greater-than">&gt;</span>',
			sItemBread = '<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid item-bread"></p>',
			sMainBread = '<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5"> </p>',
			sActiveBread = '<span class="active-bread"></span>';
		

		var setActiveEvent = function(uiThis){
			batchModal.find(".bggray-light").removeClass('bggray-light active-hier');
			
			var uiTraget = uiThis,
				oData = uiTraget.data();

			if(!uiTraget.hasClass("hier-item")){
				uiTraget = uiTraget.closest(".hier-item");
				oData = uiTraget.data();
			}

			if(uiTraget.closest(".hier-item").length > 0){
				uiTraget.closest(".hier-item").addClass('bggray-light active-hier');
			}else if(uiTraget.hasClass("hier-item")){
				uiTraget.addClass('bggray-light');
			}

			if(Object.keys(oSelectedUI).length > 0){
				oToPlaceData = oData;
				var	uiNext = $(sNextIcon),
					uiBread = $(sItemBread);
				breadCrumbsHierarchy.find(".temporary").remove();
				uiNext.addClass("temporary");
				uiBread.addClass("temporary");
				breadCrumbsHierarchy.append(uiNext);
				uiBread.html(oData.name);
				uiBread.data(oData);
				breadCrumbsHierarchy.append(uiBread);
			}

		}

		var displayBreadCrumbsStorage = function(){
			var uiBread = $(sMainBread),
				oData = selectedUI.data();
			uiBread.html(oData.name);
			
			breadCrumbsHierarchy.find(".greater-than").remove();
			

			breadCrumbsHierarchy.append(uiBread);
		}

		var displayBreadCrumbsItems = function(){
			breadCrumbsHierarchy.find(".greater-than").remove();
			breadCrumbsHierarchy.find(".item-bread").remove();
			breadCrumbsHierarchy.find(".bggray-7cace5").remove();

			for(var i in oSelectedUI){
				var uiMainBread = $(sMainBread),
					uiNext = $(sNextIcon),
					uiBread = $(sItemBread);
				 if(i == "0"){
				 	uiMainBread.html(oSelectedUI[i]["name"]);
				 	uiMainBread.data(oSelectedUI[i]);
					breadCrumbsHierarchy.append(uiMainBread);
				 }else{
					breadCrumbsHierarchy.append(uiNext);
					uiBread.html(oSelectedUI[i]["name"]);
					uiBread.data(oSelectedUI[i]);
					breadCrumbsHierarchy.append(uiBread);
				 }
			}
		}

		var selectHierarchy = function(uiThis){
			var oData = uiThis.data(),
				iLevel = oData.iLevel,
				sQtyInLocationParent = oData.sQtyInLocationParent,
				sFirstLevelID = "",
				oDataToDisplay = {};
			
			if(oData.children.length > 0){
				oSelectedUI[doubleClickLevel] = oData;
				doubleClickLevel++;
				oDisplayedData[doubleClickLevel] = oData.children;
				displayChildren(oData.children);
				displayBreadCrumbsItems();
			}
			
			
		}

		var displayChildren = function(oChildren){
			
			if(Object.keys(oChildren).length > 0){
				batchDisplay.html("");
			}
			for(var i in oChildren){
				var uiItem = $(sTemplate),
					data = oChildren[i];
				
				data["children"] = data.children;
				uiItem.find(".location-name").html(data.name);
				uiItem.find(".qty-in-location").html("Number of Locations inside = "+Object.keys(data.children).length);
				//uiItem.find("img").attr("src", CGetProfileImage.getDefault(data.name));
				uiItem.find("img").attr("src", BASEURL+"assets/images/folder-circle-blue.png");
				uiItem.data(data);
				uiItem.attr("location-id", data.id);

				uiItem.single_double_click(
					function(){
						setActiveEvent($(this));
					},
					function(){
						selectHierarchy($(this));
					}
				);

				batchDisplay.append(uiItem);
			}

		}

		var uiBackArrow = $(sBackArrow);

		uiBackArrow.on("click", function(){
			if(doubleClickLevel > 0){
				doubleClickLevel = doubleClickLevel - 1;
				delete oSelectedUI[doubleClickLevel];
				if(doubleClickLevel > 0){
					displayChildren(oDisplayedData[doubleClickLevel]);
				}else if(doubleClickLevel == 0){
					displayStorages(oStorages);
				}

				displayBreadCrumbsItems();
			}
		});

		breadCrumbsHierarchy.html("").append(uiBackArrow);	
		
		var makeHierarchy = function(oData){
			var flat = {};
			for(var i in oData){
				var k = oData[i]["id"];
				oData[i]["children"] = [];
				flat[k] = oData[i];
			}

			for (var i in flat) {
				var parentkey = flat[i].parent_id;
				if(flat[parentkey]){
					flat[parentkey].children.push(flat[i]);
				}
			}

			var root = [];
			for (var i in flat) {
				var parentkey = flat[i].parent_id;
				if (!flat[parentkey]) {
					root.push(flat[i]);
				}
			}
			return root;
		};

		var displayStorages = function(oStorages){
			batchDisplay.html("");
			currentDisplayedData = [];
			for(var i in oStorages){
				var uiItem = $(sTemplate),
					data = oStorages[i],
					oHierarchy = makeHierarchy(data.location);
				
				data["children"] = oHierarchy[0]["children"];
				uiItem.find(".location-name").html(data.name);
				//uiItem.find("img").attr("src", CGetProfileImage.getDefault(data.name));
				uiItem.find("img").attr("src", BASEURL+"assets/images/folder-circle-blue.png");
				uiItem.find(".qty-in-location").html(data.capacity).number(true, 2).append(" "+data.measure_name);
				batchDisplay.append(uiItem);
				uiItem.data(data);
				uiItem.data("sQtyInLocationParent", uiItem.find(".qty-in-location").html());
				uiItem.attr("storage-id", data.id);
				
				uiItem.single_double_click(
					function(){
						setActiveEvent($(this));
					},
					function(){
						selectHierarchy($(this));
					}
				);
			}

			oDisplayedData = {};
			oDisplayedData[0] = oStorages;
		}

		displayStorages(oStorages);

	}

	function _addBatchEvent(e, callBack)
	{
		var uiTarget = $(e.target),
			uiList = uiTarget.closest('.record-item'),
			oRecord = uiList.data(),
			batchModal = $('[modal-id="add-batch"]'),
			batchModalClose = batchModal.find(".close-me"),
			confirmAddBatch = $("#confirmAddBatch"),
			uigetDiscrepancy = uiList.find(".discrepancy"),
			oDiscrepancy= uigetDiscrepancy.data(),
			itemReceivingBalance = 0,
			iQtyToReceive = oRecord.qty;

			
			if(typeof oDiscrepancy.discrepancy == 'undefined')
			{
				itemReceivingBalance = oItemReceivingBalance[oRecord.product_inventory_id];
			}
			else
			{

				var iTotalBalance = parseFloat(iQtyToReceive) - parseFloat(oDiscrepancy.discrepancy.total);
				itemReceivingBalance = iTotalBalance;
			}

		

		batchModal.addClass("showed");

	
		batchModal.find(".modal-body").find(".modal-head").find(".text-left").html("Edit Batch");



		// if(bBatchEditMode){
		// 	var batchAmount = parseFloat(oBatchPerItem[oRecord.product_inventory_id][sBatchEditKey]["batch_amount"]);
		// 	itemReceivingBalance = itemReceivingBalance + batchAmount;
		// }
// 		console.log(JSON.stringify(oRecord));

		var uiNameSku = batchModal.find(".product-name-sku"),
			uiInputName = batchModal.find(".input-batch-name"),
			uiToReceive = batchModal.find(".to-receive"),
			uiInputAmount = batchModal.find(".input-batch-amount"),
			uiInputBagsAmount = batchModal.find(".input-bags-amount"),
			uiBagsInputDivs = batchModal.find(".bags-input-divs"),
			uiGetInputAmount = uiInputAmount.data("value"),
			uiReceiveBal = batchModal.find(".receiving-balance"),
			uiBatchAmountUom = batchModal.find(".batch-amount-uom"),
			iReceivingTotalBal = parseFloat(itemReceivingBalance),
			timeoutType = {};

			// console.log(uiGetInputAmount.data("value"));

			uiBatchAmountUom.html(oRecord.measures_name.toUpperCase());

			uiNameSku.html("SKU No. "+oRecord.sku+" - "+oRecord.product_name);
			uiToReceive.html($.number(oRecord.qty, 0)+" "+oRecord.measures_name);
			uiReceiveBal.html($.number(itemReceivingBalance, 0)+" "+oRecord.measures_name);
			uiInputAmount.number(true, 0);
			uiInputBagsAmount.number(true, 0);


			uiInputName.val("");
			uiInputAmount.val("");

			if(iReceivingTotalBal > 0){
				uiReceiveBal.html($.number(iReceivingTotalBal, 0)+" "+oRecord.measures_name);

			}else{
				uiReceiveBal.html($.number(iReceivingTotalBal, 0)+" "+oRecord.measures_name).css({"color":"red"});

			}




		
		uiInputAmount.off('keyup.changeamount').on('keyup.changeamount', function(){
			var uiThis = $(this),
				sThisVal = uiThis.val(),
				iThisVal = parseFloat(sThisVal),
				iBalance = parseFloat(itemReceivingBalance),
				iConvertAmount = parseFloat(uiGetInputAmount),
				iTotal = 0,
				uom = oRecord.measures_name;
				// console.log(iThisVal);

			clearTimeout(timeoutType);

			timeoutType = setTimeout(function(){
				if(sThisVal.trim().length > 0 && iThisVal != 0){

						// console.log(iThisVal);
							if(iThisVal < iConvertAmount){
								// var getBal = iThisVal - uiGetInputAmount;
									if(iBalance < 0)
									{
										var iSubNum = iThisVal - iConvertAmount;
										console.log("less Than");
										console.log(iBalance)
										var iGetBal = iBalance - iSubNum;

										if(iGetBal < 0){

											iTotal = -Math.abs(iGetBal);
											uiReceiveBal.html($.number(iTotal, 0)+" "+uom).css({"color":"red"});
										}else{
											iTotal = Math.abs(iGetBal);
											uiReceiveBal.html($.number(iTotal, 0)+" "+uom).css({"color":"black"});
										}

									}else{
										var iSubNum = iConvertAmount - iThisVal;
										var iGetBal = iBalance + iSubNum;
										console.log("greater than");
										console.log(iSubNum);
										if(iGetBal < 0){

											iTotal = -Math.abs(iGetBal);
											uiReceiveBal.html($.number(iTotal, 2)+" "+uom).css({"color":"red"});
										}else{
											iTotal = Math.abs(iGetBal);
											uiReceiveBal.html($.number(iTotal, 2)+" "+uom).css({"color":"black"});
										}
									}



								// console.log("dito dapat adasds");
								// iTotal = iBalance - iThisVal;
								
							}else if(iThisVal > iConvertAmount)
							{
								if(iBalance < 0)
								{
									var iSubNum = iThisVal - iConvertAmount;
									console.log("less Than");
									console.log(iBalance)
									var iGetBal = iBalance - iSubNum;

									if(iGetBal < 0){

										iTotal = -Math.abs(iGetBal);
										uiReceiveBal.html($.number(iTotal, 0)+" "+uom).css({"color":"red"});
									}else{
										iTotal = Math.abs(iGetBal);
										uiReceiveBal.html($.number(iTotal, 0)+" "+uom).css({"color":"black"});
									}

								}else{
										var iSubNum = iConvertAmount - iThisVal;
										var iGetBal = iBalance + iSubNum;
										console.log("greater than");
										console.log(iSubNum);
										if(iGetBal < 0){

											iTotal = -Math.abs(iGetBal);
											uiReceiveBal.html($.number(iTotal, 0)+" "+uom).css({"color":"red"});
										}else{
											iTotal = Math.abs(iGetBal);
											uiReceiveBal.html($.number(iTotal, 0)+" "+uom).css({"color":"black"});
										}
									}
							}

							// }else if(iThisVal > uiGetInputAmount)
							// {
							// 	console.log("dito sa second");
							// 	var getBal = iThisVal - uiGetInputAmount;
							// 	iTotal = iBalance + getBal;
							// 	uiReceiveBal.html('<i class="fa fa-minus" aria-hidden="true"></i>'+$.number(Math.abs(iTotal), 2)+" "+uom).css({"color":"red"});
							// }else{
							// 	console.log("dito");
							// 	if(iTotal < 0)
							// 	{
							// 		uiReceiveBal.html('<i class="fa fa-minus" aria-hidden="true"></i>'+$.number(iTotal, 2)+" "+uom).css({"color":"red"});
							// 	}else{

							// 		uiReceiveBal.html($.number(iTotal, 2)+" "+uom).css({"color":"black"});
							// 	}
							// }
						
				}else{
					var iGetTotal = iBalance;
					console.log(iGetTotal);
					console.log("dito dapat");

					if(iGetTotal < 0)
						{
							uiReceiveBal.html($.number(iGetTotal, 0)+" "+uom).css({"color":"red"});
						}else{

							uiReceiveBal.html($.number(iGetTotal, 0)+" "+uom).css({"color":"black"});
						}
					//iReceivingTotalBal = iBalance;
				}
				clearTimeout(timeoutType);
			},500);


		});

		// console.log(oRecord);
		// console.log(oStorages);

		_displayBatchStorages(oStorages);

		batchModalClose.off('click').on('click', function(){
			batchModal.removeClass("showed");
			bBatchEditMode = false;
		});

		confirmAddBatch.off('click').on('click', function(){

				


			var uiSelectedLocation = $(".item-bread.temporary"),
				oLocation = uiSelectedLocation.data(),
				error = 0,
				arrErrorMsg = [],
				recordBatch = oBatchPerItem[oRecord.product_inventory_id],
				len = Object.keys(recordBatch).length;

			if(oItemReceivingBalance[oRecord.product_inventory_id] == 0 && !bBatchEditMode){
				error++;
				arrErrorMsg.push("Receiving Balance Already Been Used");
			}else{
				if(!oLocation){
					error++;
					arrErrorMsg.push("Select Location");
				}


				if(uiInputName.val().trim().length == 0){
					error++;
					arrErrorMsg.push("Enter Batch Name");
				}

				if(uiInputName.val().trim().length == 0){
					error++;
					// arrErrorMsg.push("Enter Batch Name");
				}

				if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1){
					if(oRecord.loading_method == "Bags")
					{
						if(uiInputBagsAmount.val().trim() == "" || uiInputBagsAmount.val().trim() == 0 || uiInputBagsAmount.val().trim() == "0")
						{
							error++;
							arrErrorMsg.push("Enter Bags Amount");
						}

					}
				}

				
			}

			if(uiInputAmount.val().trim().length == 0 || uiInputAmount.val() == 0)
			{
				error++;
				arrErrorMsg.push("Enter Batch Amount");
			} 

			if(error > 0){
				$("body").feedback({title : "Message", message : arrErrorMsg.join(","), type : "danger", icon : failIcon});
			}else{
				iBagsAmountTruck = 0;

				if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1){
					if(oRecord.loading_method == "Bags")
					{
						iBagsAmountTruck = uiInputBagsAmount.val().trim();
					}
				}

				
				if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1)
				{
					// Add batch
					if(!bBatchEditMode){
						oBatchPerItem[oRecord.product_inventory_id][len] = {
							"location" : oLocation,
							"item_id" : oRecord.product_id,
							"batch_name" : uiInputName.val(),
							"batch_amount" : uiInputAmount.val(),
							"bag_amount" : iBagsAmountTruck,
							"uom" : oRecord.measures_name,
							"uom_id" : oRecord.unit_of_measure_id,
							"hierarchy" : _getBreadCrumbsHierarchy()
						};
					}else{
						oBatchPerItem[oRecord.product_inventory_id][sBatchEditKey] = {
							"location" : oLocation,
							"item_id" : oRecord.product_id,
							"batch_name" : uiInputName.val(),
							"batch_amount" : uiInputAmount.val(),
							"bag_amount" : iBagsAmountTruck,
							"uom" : oRecord.measures_name,
							"uom_id" : oRecord.unit_of_measure_id,
							"hierarchy" : _getBreadCrumbsHierarchy()
						}
						bBatchEditMode = false;
					}
				}else{

					// Add batch
					if(!bBatchEditMode){
						oBatchPerItem[oRecord.product_inventory_id][len] = {
							"location" : oLocation,
							"item_id" : oRecord.product_id,
							"batch_name" : uiInputName.val(),
							"batch_amount" : uiInputAmount.val(),
							"uom" : oRecord.measures_name,
							"uom_id" : oRecord.unit_of_measure_id,
							"hierarchy" : _getBreadCrumbsHierarchy()
						};
					}else{
						oBatchPerItem[oRecord.product_inventory_id][sBatchEditKey] = {
							"location" : oLocation,
							"item_id" : oRecord.product_id,
							"batch_name" : uiInputName.val(),
							"batch_amount" : uiInputAmount.val(),
							"uom" : oRecord.measures_name,
							"uom_id" : oRecord.unit_of_measure_id,
							"hierarchy" : _getBreadCrumbsHierarchy()
						}
						bBatchEditMode = false;
					}

				}
				oItemReceivingBalance[oRecord.product_inventory_id] = iReceivingTotalBal;
				

				$('[modal-target="add-batch"]').show();
				$("[modal-id='add-batch'] .close-me:visible").click();

				_displayBatches(oRecord.product_inventory_id);

				_displayBatchHover(uiList, "true");

			}

		});

		setTimeout(function(){
			if(typeof callBack == 'function'){
				callBack();
			}
		}, 100);

	}

	function _displayBatches(prodID)
	{
		var oBatch = oBatchPerItem[prodID],
			productContainer = $(".record-item[prod-id='"+prodID+"']"),
			uiAssignmentDetails = productContainer.find(".assignment-display"),
			uiTable = uiAssignmentDetails.find(".tbl-4c3h"),
			uiTableSibling = uiTable.nextAll(),
			uiMarker1 =  productContainer.find(".marker1"),
			uiHibeAddBatchBtn = uiAssignmentDetails.find(".hide-method"),
			uiBagPerDisplay = productContainer.find(".display-bag-per-batch"),
			uiSpanReceivingTotal = productContainer.find(".received-quantity"),
			uiSpanDiscrepancy = productContainer.find(".discrepancy"),
			receivingData = productContainer.find(".display-consignee-qtr-to-receive").data(),
			qtyToReceive = parseFloat(receivingData.quantity),
			uiListContainer = productContainer.find(".storage-assign-panel.position-rel").find(".btn-hover-storage"),
			uom = receivingData["unitOfMesurement"];

			// console.log(qtyToReceive);

		
		uiTableSibling.remove();
		uiHibeAddBatchBtn.show();

		var template = {};

		if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1  || sCurrentPath.indexOf("consignee_complete_truck") > -1 )
		{
			uiBagPerDisplay.show();

			var template = {
				container : "<div class='transfer-prod-location font-0 bggray-white'></div>",
				total : '<div class="total-amount-storage margin-top-10"> <p class="first-text">Total</p><p class="second-text"> </p> <div class="clear"></div></div>',
				hier_text : '<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>',
				hier_arrow : '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>',
				list : '<div class="storage-assign-panel position-rel">'+
							'<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change">'+
								'<div class="width-25percent display-inline-mid text-center">'+
									'<p class="font-14 font-400 batch-name"> </p>'+
								'</div>'+
								'<div class="width-50percent text-center display-inline-mid height-auto line-height-25 hierarchy">'+											
								'</div>'+
								'<div class="width-15percent display-inline-mid text-center">'+
									'<p class="font-14 font-400 quantity"></p>'+
								'</div>'+
								'<div class="width-10percent display-inline-mid text-center">'+
									'<p class="font-14 font-400 bag-quantity"></p>'+
								'</div>'+
							'</div>'+
							'<div class="btn-hover-storage" style="height: 55px; display: block; opacity: 0;">'+
								'<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>'+
								'<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>'+
							'</div>'+
						'</div>',
			not_yet_assigned : '<div class="transfer-prod-location font-0 bggray-white position-rel">'+
						'<div class="width-100percent padding-top-10 padding-bottom-10 show-ongoing-content margin-bottom-20 background-change">'+
							'<div class="width-100percent display-inline-mid text-center">'+
								'<p class="font-14 font-400 italic">Not Yet Assigned</p>'+
							'</div>'+
						'</div>'+
					'</div>'
			};
		}else{
			
			var template = {
				container : "<div class='transfer-prod-location font-0 bggray-white'></div>",
				total : '<div class="total-amount-storage margin-top-10"> <p class="first-text">Total</p><p class="second-text"> </p> <div class="clear"></div></div>',
				hier_text : '<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>',
				hier_arrow : '<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>',
				list : '<div class="storage-assign-panel position-rel">'+
							'<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change">'+
								'<div class="width-25percent display-inline-mid text-center">'+
									'<p class="font-14 font-400 batch-name"> </p>'+
								'</div>'+
								'<div class="width-50percent text-center display-inline-mid height-auto line-height-25 hierarchy">'+											
								'</div>'+
								'<div class="width-25percent display-inline-mid text-center">'+
									'<p class="font-14 font-400 quantity"></p>'+
								'</div>'+
							'</div>'+
							'<div class="btn-hover-storage" style="height: 55px; display: block; opacity: 0;">'+
								'<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>'+
								'<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>'+
							'</div>'+
						'</div>',
			not_yet_assigned : '<div class="transfer-prod-location font-0 bggray-white position-rel">'+
						'<div class="width-100percent padding-top-10 padding-bottom-10 show-ongoing-content margin-bottom-20 background-change">'+
							'<div class="width-100percent display-inline-mid text-center">'+
								'<p class="font-14 font-400 italic">Not Yet Assigned</p>'+
							'</div>'+
						'</div>'+
					'</div>'
			};

		}

		



		
		var uiContainer = $(template.container),
			stripChange = false,
			iTotal = 0,
			iBagTotal = 0,
			unitOfMeasure = "";
		
		uiContainer.insertAfter(uiTable);

		for(var i in oBatch){
			var uiListContainer = $(template.list),
				oHierarchy = oBatch[i]["hierarchy"],
				uom = oBatch[i]["uom"],
				batch_amount = oBatch[i]["batch_amount"],
				bag_amount = oBatch[i]["bag_amount"],
				uiRemoveBatch = uiListContainer.find('[modal-target="remove-batch"]'),
				uiEditBatch = uiListContainer.find("[modal-target='edit-batch']");

			

			// Remove Batch
			uiRemoveBatch.attr("batch-key", i);
			uiEditBatch.attr("batch-key", i);


			uiEditBatch.off('click').on('click', function(e){
					var uiThis = $(this),
						uiList = uiThis.closest(".storage-assign-panel"),
						uiThisParent = uiThis.closest(".transfer-prod-location"),
						uiParent = uiThis.closest(".record-item"),
						uiParentData = uiParent.data(),
						batchData = uiParentData.batch,
						batchKey = uiThis.attr("batch-key"),
						uiTotalContainer = uiParent.find(".total-amount-storage"),
						oBatchData = {};

// 						console.log(JSON.stringify(uiParentData));
					
					sBatchEditKey = batchKey;
					bBatchEditMode = true;

					for(var i in batchData){
						if(i == batchKey){
							oBatchData = batchData[i];	
							// 	console.log(batchData[i]);
							$(".input-batch-amount").data("value", oBatchData.batch_amount);
						}
					}


					_addBatchEvent(e, function(){

						var breadCrumbsHierarchy = $("#breadCrumbsHierarchy"),
							uiBreadParent = breadCrumbsHierarchy.parent(),
							batchDisplay = $("#batchDisplay"),
							oHierarchy = oBatchData.hierarchy,
							oQueue = {};


							// console.log(JSON.stringify(oHierarchy));
						
						$(".input-batch-name").val(oBatchData.batch_name);
						$(".input-batch-amount").val(oBatchData.batch_amount);



						if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1){
							if(uiParentData.loading_method == "Bags")
							{
								$(".input-bags-amount").val(oBatchData.bag_amount);
								$(".bags-input-divs").show();
								// uiInputBagsAmount.val(oBatchData.bag_amount);
								//console.log(oBatchData.bag_amount);
							}else{
								uiBagsInputDivs.hide();
							}
						}


						batchDisplay.css("opacity", 0);
						var uiLoader = $("<div loader='true'></div>");
						uiLoader.css({
							"position" : "absolute",
							"top" : "0px",
							"left" : "0px",
							"width" : "100%",
							"height" : "100%",
							"background-color" : "#444",
							"color" : "#fff",
							"text-align" : "center"
						});
						var uiSpinner = $('<div><i class="fa fa-cog fa-spin fa-3x fa-fw margin-bottom"></i>Loading..</div>');
						uiSpinner.css({
							"position" : "absolute",
							"width" : "30px",
							"height" : "30px",
							"top" : "40%",
							"left" : "50%",
							"margin-left" : "-15px",
							"margin-right" : "-15px"
						});
						uiLoader.html(uiSpinner);
						uiBreadParent.append(uiLoader);
						uiBreadParent.css("position", "relative");

						for(var i in oHierarchy){
							oQueue[i] = {};
							if(i == "0"){
								oQueue[i]["selector"] = "[storage-id='"+oHierarchy[i]["id"]+"']";
							}else{
								oQueue[i]["selector"] = "[location-id='"+oHierarchy[i]["id"]+"']";
							}

							oQueue[i]["callback"] = function(){
								var oThis = this, 
									selector = oThis.selector,
									uiLocation = $(selector);

								uiLocation.click();
								setTimeout(function(){
									if(typeof oThis.nextcall == 'function'){
										uiLocation.click();
										setTimeout(function(){
											var oNext = oThis.nextobj;
											oThis.nextcall.apply(oNext);

											if(oNext.hasOwnProperty("last")){
												if(oNext.last){
													setTimeout(function(){
														batchDisplay.css("opacity", 1);
														$("[loader='true']").remove();
													},1300);
												}
											}
										},500);
									}
								}, 30);
							}
						}
						
						var timeoutCall = function(obj){
							setTimeout(function(){
								var que = obj,
									callback = que.callback;
								callback.apply(que);
							}, 500);
						}
						
						for(var i in oQueue){
							var queNext = oQueue[parseInt(i) + 1];
							if(queNext){
								oQueue[i]['callback'].bind(queNext);
								oQueue[i]['nextobj'] = queNext;
								oQueue[i]['nextcall'] = oQueue[parseInt(i) + 1]['callback'];
								oQueue[i]['nextcall_from'] = oQueue[i]["selector"];
								oQueue[i]['nextcall_to'] = oQueue[parseInt(i) + 1]["selector"];
							}else{
								oQueue[i]['last'] = true;
							}
							timeoutCall(oQueue[i]);
						}
						 

					});
				});

			uiRemoveBatch.off('click').on('click', function(){
				var uiThis = $(this),
					uiList = uiThis.closest(".storage-assign-panel"),
					uiThisParent = uiThis.closest(".transfer-prod-location"),
					uiParent = uiThis.closest(".record-item"),
					uiParentData = uiParent.data(),
					uom = uiParentData.unit_of_measure_id,
					batchData = uiParentData.batch,
					batchKey = uiThis.attr("batch-key"),
					uiTotalContainer = uiParent.find(".total-amount-storage"),
					uiDiscrepancy = uiParent.find(".discrepancy"),
					uiReceivingTotal = uiParent.find(".received-quantity"),
					iBatchAmount = 0,
					iBagAmount = 0;


					
						
				
				console.log(batchData);
				for(var i in batchData){
					if(i == batchKey){
						iBatchAmount = parseFloat(batchData[i]["batch_amount"]);
						iBagAmount = parseInt(batchData[i]["bag_amount"]);
						delete batchData[i];	
					}
				}
				//console.log(batchData);

				var counter = 0,
					newBatchData = {};
				for(var i in batchData){
					newBatchData[counter] = batchData[i];
					counter++;
				}

				uiParent.data("batch", newBatchData);
				uiList.remove();

				if(Object.keys(newBatchData).length == 0){
					var uiNotYetAssigned = $(template.not_yet_assigned);

					batchData = {};
					uiTotalContainer.remove();

					uiNotYetAssigned.insertAfter(uiTable);

					oItemReceivingBalance[uiParentData.product_inventory_id] = parseFloat(uiParentData.quantity);

					if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1)
					{
						oBagsItemBalance[uiParentData.product_inventory_id] = parseInt(uiParentData.bag);
					}

					

					uiDiscrepancySpan = uiDiscrepancy.find("span");
					uiDiscrepancySpan.html($.number(0, 2));
					
					uiReceivingTotal.html($.number(0, 2) + " "+uom);
					// console.log("wala na ");
					iTotal = 0;
					_setQuantityCheckProduct(uiContainer, iTotal);

				}else{
					var iBal = parseFloat(oItemReceivingBalance[uiParentData.product_inventory_id]),
						iTotal = 0;

					iTotal = iBal + iBatchAmount;

					oItemReceivingBalance[uiParentData.product_inventory_id] = iTotal;

					if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1)
					{
						var iBagBal = parseInt(oBagsItemBalance[uiParentData.product_inventory_id]),
							iBagTotal = 0;

							iBagTotal = iBagBal + iBagAmount;

							oBagsItemBalance[uiParentData.product_inventory_id] = iBagTotal;
					}
					
					oBatchPerItem[uiParentData.product_inventory_id] = newBatchData;
					
					_displayBatches(uiParentData.product_inventory_id);

					_displayBatchHover(uiParent, "true");
					// console.log("merun pa");
				}

				//oItemReceivingBalance[oRecord.id];


			});
			// End Remove Batch

			unitOfMeasure = uom;
			
			if(stripChange){
				uiListContainer.find(".background-change").removeClass("background-change");
			}

			uiListContainer.find(".batch-name").html(oBatch[i]["batch_name"]);
			
			
			for(var x in oHierarchy){
				var uiHierContainer = uiListContainer.find(".hierarchy"),
					uiHierArrow = $(template.hier_arrow),
					uiHierText = $(template.hier_text);
				
				uiHierText.html(oHierarchy[x]["name"]);
				uiHierContainer.append(uiHierText);
				if((Object.keys(oHierarchy).length - 1) != x){
					uiHierContainer.append(uiHierArrow);	
				}
			}

			uiListContainer.find(".quantity").html($.number(batch_amount, 0)+" "+uom);
			uiListContainer.find(".bag-quantity").html($.number(bag_amount, 0));

			
			
			uiContainer.append(uiListContainer);

			if(stripChange){
				stripChange = false;
			}else{
				stripChange = true;
			}

			iTotal = iTotal + parseFloat(batch_amount);


			iBagTotal += parseInt(bag_amount);


		}


		var uiTotal = $(template.total);


		if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1  || sCurrentPath.indexOf("consignee_complete_truck") > -1 )
		{
			uiTotal.find(".second-text").html($.number(iTotal, 0)+" "+unitOfMeasure+"<span style='width: 100px; float: right; text-align: right; margin-right: 25px;' class='third-text'>"+iBagTotal+"</span>").data({"total_batch_quantity":iTotal, "total_bag_quantity":iBagTotal});
		}else{
			uiTotal.find(".second-text").html($.number(iTotal, 0)+" "+unitOfMeasure).data({"total_batch_quantity":iTotal});
		}


		uiTotal.insertAfter(uiContainer);


		_setQuantityCheckProduct(uiContainer, iTotal);


		productContainer.data("batch", oBatch);


	}

	function _setQuantityCheckProduct(uiContainer, iTotal)
	{
		var uiParent = uiContainer.closest(".record-item"),
			oItemData = uiParent.data(),
			uiQuantityInput = uiParent.find('.set-receive-quantity'),
			uiGetCause = uiParent.find('.set-discrepancy-cause'),
			uiRemoveDropdown = uiParent.find(".set-dropdown-data").find(".frm-custom-dropdown");
			uiDisplayDiscrepancy = uiParent.find('.discrepancy'),
			uiGetRemarks = uiParent.find('.set-discrepancy-remarks'),
			iTotal = parseFloat(iTotal),
			qtyToReceive = parseFloat(oItemData.qty),
			computedReceiving = qtyToReceive - iTotal;

			// uiRemoveDropdown.remove();

			uiQuantityInput.html(iTotal+" "+oItemData.measures_name.toUpperCase());


			if(computedReceiving == 0)
			{
				uiDisplayDiscrepancy.html(0);
				uiParent.find(".discrepancy").data("discrepancy", {
					"value" : 0,
					"type" : "0",
					"total" : iTotal
				});

			}else{
				if(computedReceiving < 0){
					uiDisplayDiscrepancy.html('<i class="fa fa-arrow-up" aria-hidden="true" style="font-size:11px; margin-right:3px; margin-bottom:2px;"></i>'+$.number(Math.abs(computedReceiving), 2) +" "+oItemData.measures_name.toUpperCase());
					uiParent.find(".discrepancy").data("discrepancy", {
						"value" : Math.abs(computedReceiving),
						"type" : "up",
						"total" : iTotal
					});
				}else{
					uiDisplayDiscrepancy.html('<i class="fa fa-arrow-down" aria-hidden="true" style="font-size:11px; margin-right:3px; margin-bottom:2px;"></i>'+$.number(computedReceiving, 2)+" "+oItemData.measures_name.toUpperCase());
					uiParent.find(".discrepancy").data("discrepancy", {
						"value" : computedReceiving,
						"type" : "down",
						"total" : iTotal
					});
				}
			}

			 if(oItemData.hasOwnProperty("discrepancy")){
				if(Object.keys(oItemData.discrepancy).length > 0){
					uiGetRemarks.val(oItemData.discrepancy["remarks"]);
					// uiGetCause.val(oItemData.discrepancy["cause"])

					uiGetCause.html("");

					var sHtml = '<option value="'+oItemData.discrepancy["cause"]+'" selected>'+oItemData.discrepancy["cause"]+'</option>'+
								'<option value="Others">Others</option>'+
								'<option value="Extra">Extra</option>';
					uiGetCause.append(sHtml);									

					


					uiParent.find(".set-dropdown-data").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt").val(oItemData.discrepancy["cause"]);
				}
			}

		
	}

	function _saveUpdateCosigneeProductDetails()
	{
		var btnTriggerSaveProdButton = $(".marker1"),
			oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"});

		btnTriggerSaveProdButton.off("click.saveUpdate").on("click.saveUpdate", function(e){
			e.preventDefault();
			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
				oItemData = uiParent.data(),
				uiMarker1Cancel = uiParent.find(".marker1-cancel"),
				uiEditingCancel = uiParent.find(".editing-cancel"),
				uiEditingButton = uiParent.find(".editing-button"),
				uiAddingBatch = uiParent.find(".adding-batch"),
				uiQuantityCheckHide = uiParent.find(".quantity-check-hide"),
				uiQuantityCheckBtn = uiParent.find(".quantity-check-btn"),
				uiExclamationMark = uiParent.find(".exclamation-mark"),
				uiCheckMark = uiParent.find(".check-mark"),
				uiGetRemarks = uiParent.find('.set-discrepancy-remarks'),
				uiGetCause = uiParent.find('.set-discrepancy-cause'),
				uiGetTotalBatchQty = uiParent.find(".second-text"),
				oGetTotalBatchQty = uiGetTotalBatchQty.data("total_batch_quantity"),
				iTotalBatchQty = parseFloat(oGetTotalBatchQty),
				uiDiscrepancy = uiParent.find(".discrepancy"),
				oDiscrepancy = uiDiscrepancy.data();
				getText = uiThis.text(),
				oStoreProductDetailsPerItem = [],
				oStoredError = [],
				iDiscrepancy = 0,
				iDiscrepancyType = 0,
				iTotalQtyToReceived = 0;

			if(getText == "Enter Item Details"){
				// console.log(oGetTotalBatchQty);
				if(typeof oGetTotalBatchQty == 'undefined'){
					oStoredError.push("Storage Assignment must not be empty");
				}

				if(typeof oDiscrepancy.discrepancy == 'undefined')
				{
					iDiscrepancy = 0;
					iDiscrepancyType = 0;

				}
				else
				{
					if(oDiscrepancy.discrepancy.type == "0")
					{

					}else{
						iDiscrepancy = oDiscrepancy.discrepancy.value;
						iTotalQtyToReceived = oDiscrepancy.discrepancy.total;
						if(oDiscrepancy.discrepancy.type == "up")
						{
							iDiscrepancyType = 1;
						}else if(oDiscrepancy.discrepancy.type == "down"){
							iDiscrepancyType = 2;
						}else{
							iDiscrepancyType = 0;
						}


						if(uiGetRemarks.val().trim() == "")
						{
							oStoredError.push("Quantity Check Remarks is Required");
						}

						if(uiGetCause.val() == "")
						{
							oStoredError.push("Cause of Discrepancy is Required");
						}


					}

					
				}


				if(oStoredError.length === 0)
				{

					// console.log("succes");
					// console.log(oItemData.product_inventory_id);

					var oGatherProductDetailsPerItem = {
						receiving_item_record_id : oItemData.product_inventory_id,
						product_id : oItemData.product_id,
						batch : oItemData.batch,
						received_quantity : iTotalQtyToReceived,
						discrepancy_type : iDiscrepancyType,
						discrepancy_value : iDiscrepancy,
						discrepancy_reason : uiGetRemarks.val().trim(),
						discrepancy_cause : uiGetCause.val()

					}
					oStoreProductDetailsPerItem.push(oGatherProductDetailsPerItem);
					

					_completeProductDetails(oStoreProductDetailsPerItem);

					// console.log(JSON.stringify(oStoreProductDetailsPerItem));

					var oSetDiscrepancy = {
						received_quantity : iTotalQtyToReceived,
						discrepancy_type : iDiscrepancyType,
						discrepancy_value : iDiscrepancy,
						discrepancy_reason : uiGetRemarks.val().trim(),
						discrepancy_cause : uiGetCause.val(),
						unit_of_measure : oItemData.measures_name.toUpperCase()
					}

					uiParent.data("discrepancy", oSetDiscrepancy);


					uiThis.html('Enter Item Details');
					uiMarker1Cancel.hide();
					uiEditingCancel.hide();
					$(".to-editing-page").show();
					uiThis.show();
					uiEditingButton.show();



					uiAddingBatch.hide();

					
					uiQuantityCheckHide.show();
					uiQuantityCheckBtn.hide();

					uiExclamationMark.hide();
					uiCheckMark.show();

					$("body").feedback({title : "Update Product Details", message : "Successfully Updated", type : "success"});


					_displayQuantityCheckSuccess(uiParent, oSetDiscrepancy);

				}else{
					
					// console.log(JSON.stringify(oStoredError))

					$("body").feedback({title : "Updating Product Details", message : oStoredError.join("<br />"), type : "danger", icon : failIcon});

					uiThis.html('Save Details');
				}
				
			}

			oStoredError = [];

			

		});
	}


	function _displayQuantityCheckSuccess(ui, oData)
	{	

		var uiParent = ui.closest(".border-top.border-blue.box-shadow-dark"),
			uiDisplaySuccessReceiveQty = uiParent.find(".display-success-receive-qty"),
			uiDisplaySuccessReasonDiscrepancy = uiParent.find(".display-success-reason-discrepancy"),
			uiDisplaySuccessDiscrepancy = uiParent.find(".display-success-discrepancy"),
			uiDisplaySuccessDiscrepancyRemarks = uiParent.find(".display-success-discrepancy-remarks"),
			uiDisplaySuccessIcon = uiParent.find(".caution.display-inline-mid");
			uiMarker1 = uiParent.find(".marker1");
			sDisplayType = "",
			sRemarks = "";

			uiMarker1.text("Update Product Details");

			uiDisplaySuccessIcon.find(".check-mark").show();
			uiDisplaySuccessIcon.find(".exclamation-mark").hide();
			

			uiParent.find(".gray-color").removeClass("gray-color");

				

				if(oData["discrepancy_type"] == 1)
				{
					sDisplayType = '<i class="fa fa-minus" aria-hidden="true"></i>';
				}

				(oData["discrepancy_reason"] != "") ? sRemarks = oData["discrepancy_reason"] : sRemarks = "None";

				uiDisplaySuccessReceiveQty.html($.number(oData["received_quantity"], 0)+" "+oData["unit_of_measure"]);
				uiDisplaySuccessReasonDiscrepancy.html(oData["discrepancy_cause"]);
				uiDisplaySuccessDiscrepancy.html(sDisplayType+" "+$.number(oData["discrepancy_value"], 0)+" "+oData["unit_of_measure"]);
				uiDisplaySuccessDiscrepancyRemarks.html(sRemarks);



	}

	function _displayBatchHover(uiMain, ifTrue)
	{
		var uiHoverStorage = uiMain.find(".storage-assign-panel.position-rel").find(".btn-hover-storage");


		if(ifTrue == "true")
		{

			$.each(uiHoverStorage, function(){
				
				var uiThisHover = $(this);
				uiThisHover.off("mouseenter").on("mouseenter", function(){
					uiThisHover.css({"opacity":"1"});
				});
				uiThisHover.off("mouseleave").on("mouseleave", function(){
					uiThisHover.css({"opacity":"0"});
				});

			});
			ifTrue = "false";	
		}else{
			ifTrue = "false";	

			$.each(uiHoverStorage, function(){
				
				var uiThisHover = $(this);
				uiThisHover.off("mouseenter").on("mouseenter", function(){
					uiThisHover.css({"opacity":"0"});
				});
				uiThisHover.off("mouseleave").on("mouseleave", function(){
					uiThisHover.css({"opacity":"0"});
				});

			});
			
		}

		ifTrue = "false";	

	}

	function _createCollapseProduct(uiTemplate)
	{
		var uidDisplayConsigneeSkuNameBtn = $(".display-consignee-sku-name");

		uidDisplayConsigneeSkuNameBtn.off("click.collapeIt").on("click.collapeIt", function(e){
			e.preventDefault();
			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark");
				// iProductId = uiParent.data("product_id");

				// accordion
				$(".panel-group .panel-heading").each(function(){
					var ps = $(this).next(".panel-collapse");
					var ph = ps.find(".panel-body").outerHeight();

					if(ps.hasClass("in")){
						$(this).find("h4").addClass("active")
					}

					$(this).find("a").off("click").on("click",function(e){
						e.preventDefault();
						ps.css({height:ph});

						if(ps.hasClass("in")){
							$(this).find("h4").removeClass("active");
							$(this).find(".fa").removeClass("fa-caret-down").addClass("fa-caret-right");
							ps.removeClass("in");
						}else{
							$(this).find("h4").addClass("active");
							$(this).find(".fa").removeClass("fa-caret-right").addClass("fa-caret-down");
							ps.addClass("in");
						}

						setTimeout(function(){
							ps.removeAttr("style")
						},500);
					});
				});
					// accordion for product-inventory 
					// $(".panel-title").off("click").on("click", function() {		
					// 	$(".panel-title i").removeClass("fa-caret-right").addClass("fa-caret-down");
					// });
					// $(".panel-title .caret-click").off("click").on("click", function() {
					// 	$(this).removeClass(".fa-caret-right").addClass(".fa-caret-down");
					// });


				// console.log(iProductId);
		});

	}

	function _createEditProduct(oItems)
	{
		var btnTriggerEditingButton = $(".trigger-editing-button");

		btnTriggerEditingButton.off("click.editprod").on("click.editprod", function(e){
			e.preventDefault();
			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
				uiCollapseIn = uiParent.find(".panel-collapse.collapse"),
				iGetProductInventoryID = uiParent.data("product_inventory_id"),
				uiMarker1Btn = uiParent.find(".marker1"),
				uiMarker1Cancel = uiParent.find(".marker1-cancel"),
				uiEditingCancel = uiParent.find(".editing-cancel"),
				uiDisplayLabel = uiParent.find(".display-label"),
				uiHideLabel = uiParent.find(".hide-label"),
				uidisplayDistribution = uiParent.find(".display-distribution"),
				uiHideDistribution = uiParent.find(".hide-distribution"),
				uiHideLink = uiParent.find(".hide-link"),
				btnHoverStorage = uiParent.find(".btn-hover-storage"),
				uiAddingBatch = uiParent.find(".adding-batch"),
				uiStorageCheckBtn = uiParent.find(".storage-check-btn"),
				uiLoadingMethodContainer = uiParent.find(".loading-method-container"),
				uiStConsigneeItems = uiParent.find(".set-consignee-items"),
				uiDisplayEditConsigneeDistibution = uiParent.find(".display-edit-consignee-distibution");

				
					var getText = uiThis.text();
					if (getText == 'Edit'){

						uiThis.html('Save Changes');

						uiMarker1Btn.hide();
						uiMarker1Cancel.hide();

						$('.to-editing-page').hide();
						uiEditingCancel.show();


						uiDisplayLabel.hide();
						uiHideLabel.css({'display':'inline-block'});


						uidisplayDistribution.hide();
						uiHideDistribution.show();

						uiHideLink.show();
						btnHoverStorage.show();



						uiStConsigneeItems.show();
						uiDisplayEditConsigneeDistibution.hide();
						
						uiThis.addClass('active');
						uiCollapseIn.addClass("in");

					} else if (getText == 'Save Changes'){

						uiThis.html('Edit');

					}

					uiLoadingMethodContainer.show();
					uiStorageCheckBtn.show();
					

		});
	}

	function _createCancelProduct()
	{

		var btnEditingCancel = $(".editing-cancel");

		btnEditingCancel.off("click.cancel").on("click.cancel", function(e){
			e.preventDefault();

			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
				uiEditingButton = uiParent.find(".editing-button"),
				uiMarker1Btn = uiParent.find(".marker1"),
				uiHideLabel = uiParent.find(".hide-label"),
				uiDisplayLabel = uiParent.find(".display-label"),
				uidisplayDistribution = uiParent.find(".display-distribution"),
				uiHideDistribution = uiParent.find(".hide-distribution"),
				uiHideLink = uiParent.find(".hide-link"),
				uiAddingBatch = uiParent.find(".adding-batch"),
				btnHoverStorage = uiParent.find(".btn-hover-storage"),
				uiQuantityCheckHide = uiParent.find(".quantity-check-hide"),
				uiQuantityCheckBtn = uiParent.find(".quantity-check-btn"),
				// uiQuantityCheckBtnWithValue = uiParent.find(".quantity-check-btn-with-value"),
				uiStConsigneeItems = uiParent.find(".set-consignee-items"),
				uiDisplayEditConsigneeDistibution = uiParent.find(".display-edit-consignee-distibution");



				uiThis.hide();
				$('.to-editing-page').show();
				uiEditingButton.text('Edit');

				uiMarker1Btn.show();

				uiHideLabel.hide();
				uiDisplayLabel.show();
				uidisplayDistribution.show();
				uiHideDistribution.hide();
				uiHideLink.hide();
				uiAddingBatch.hide();
				btnHoverStorage.hide();
				uiQuantityCheckHide.show();
				uiQuantityCheckBtn.hide();
				// uiQuantityCheckBtnWithValue.show();
				uiDisplayEditConsigneeDistibution.show();
				uiStConsigneeItems.hide();


		})


		// $('.editing-cancel').on('click',function(){
		// 	if(controller == true){
		// 		$('.marker1').hide();
		// 	}else{
		// 		$('.marker1').show();
		// 	}
		// 	$(this).hide();
		// 	$('.editing-button').text('Edit');
		// 	$('.to-editing-page').show();
		// 	$('.hide-label').hide();
		// 	$('.display-label').show();
		// 	$('.display-distribution').show();
		// 	$('.hide-distribution').hide();
		// 	$('.hide-link').hide();
		// 	$('.adding-batch').hide();
		// 	$('.btn-hover-storage').hide();
		// 	$('.quantity-check-hide').hide();
		// 	$('.quantity-check-btn').hide();
		// 	$('.quantity-check-btn-with-value').show();
		// });
	}

	function _createEnterProductDetails()
	{

		var btnEditingCancel = $(".marker1");

		btnEditingCancel.off("click.cancel").on("click.cancel", function(e){
			e.preventDefault();

			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
				uiCollapseIn = uiParent.find(".panel-collapse.collapse"),
				uiEditingButton = uiParent.find(".editing-button"),
				uiMarker1Cancel = uiParent.find(".marker1-cancel"),
				uiEditingCancel = uiParent.find(".editing-cancel"),
				uiEditingButton = uiParent.find(".editing-button"),
				uiAddingBatch = uiParent.find(".adding-batch"),
				uiListContainer = uiParent.find(".storage-assign-panel.position-rel").find(".btn-hover-storage"),
				uiQuantityCheckHide = uiParent.find(".quantity-check-hide"),
				uiQuantityCheckBtn = uiParent.find(".quantity-check-btn"),
				uiExclamationMark = uiParent.find(".exclamation-mark"),
				uiCheckMark = uiParent.find(".check-mark"),
				uiStorageCheckBtn = uiParent.find(".storage-check-btn"),
				uiStConsigneeItems = uiParent.find(".set-consignee-items"),
				uiDisplayEditConsigneeDistibution = uiParent.find(".display-edit-consignee-distibution");


				var text1 = uiThis.text();
// 				console.log(text1);

				if(text1 == 'Save Details'){
					// $('.complete-receiving-record').attr('disabled',false);

					uiThis.html('Enter Item Details');

					uiListContainer.find(".btn-hover-storage").css({'opacity' :'0'});

					_displayBatchHover(uiParent, "false");

					//console.log("save");

					
				}else if (text1 == 'Enter Item Details'){
					$('.complete-receiving-record').attr('disabled',true);
					uiThis.html('Save Details');
					$(".to-editing-page").hide();
					uiEditingButton.hide();
					uiMarker1Cancel.show();

					// $.each(uiListContainer, function(){
					// 	var uiThisHover = $(this);
					// 	uiThisHover.off("mouseenter").on("mouseenter", function(){
					// 		uiThisHover.css({"opacity":"1"});
					// 	});
					// 	uiThisHover.off("mouseleave").on("mouseleave", function(){
					// 		uiThisHover.css({"opacity":"0"});
					// 	});

					// });

					_displayBatchHover(uiParent, "true");


					uiQuantityCheckHide.hide();
					uiQuantityCheckBtn.show();
					uiAddingBatch.show();

					uiStorageCheckBtn.show();

					//console.log("Enter Prod");


					uiThis.addClass('active');
					uiCollapseIn.addClass("in");
					
					
				}else if(text1 == 'Update Product Details')
				{
					uiThis.html('Save Details');
					$(".to-editing-page").hide();
					uiEditingButton.hide();
					uiMarker1Cancel.show();

					// $.each(uiListContainer, function(){
					// 	var uiThisHover = $(this);
					// 	uiThisHover.off("mouseenter").on("mouseenter", function(){
					// 		uiThisHover.css({"opacity":"1"});
					// 	});
					// 	uiThisHover.off("mouseleave").on("mouseleave", function(){
					// 		uiThisHover.css({"opacity":"0"});
					// 	});

					// });

					_displayBatchHover(uiParent, "true");


					uiQuantityCheckHide.hide();
					uiQuantityCheckBtn.show();
					uiAddingBatch.show();

					uiStorageCheckBtn.show();

					uiThis.addClass('active');
					uiCollapseIn.addClass("in");
				}

				uiStorageCheckBtn.show();

		});

	}

	function _createEnterProductDetailsCancel()
	{
		var uiMarker1Cancel = $(".marker1-cancel");

		uiMarker1Cancel.off("click.cancel").on("click.cancel", function(e){
			e.preventDefault();

			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
				oItemData = uiParent.data(),
				uiEditingCancel = uiParent.find(".editing-cancel"),
				uiEditingButton = uiParent.find(".editing-button"),
				uiMarker1Btn = uiParent.find(".marker1"),
				uidisplayDistribution = uiParent.find(".display-distribution"),
				uiHideDistribution = uiParent.find(".hide-distribution"),
				uiAddingBatch = uiParent.find(".adding-batch"),
				uiListContainer = uiParent.find(".storage-assign-panel.position-rel").find(".btn-hover-storage"),
				uiStorageCheckBtn = uiParent.find(".storage-check-btn"),
				uiQuantityCheckBtnWithValue = uiParent.find(".quantity-check-btn-with-value"),
				uiQuantityCheckBtn = uiParent.find(".quantity-check-btn"),
				uiQuantityCheckHide = uiParent.find(".quantity-check-hide"),
				uiStConsigneeItems = uiParent.find(".set-consignee-items"),
				uiDisplayEditConsigneeDistibution = uiParent.find(".display-edit-consignee-distibution");

				uiListContainer.off("mouseenter").on("mouseenter", function(){
					$(this).css({"opacity":"0"});
				});

				var text1 = uiThis.text();


				 if(oItemData.hasOwnProperty("batch") && oItemData.hasOwnProperty("discrepancy")){

					if((Object.keys(oItemData.batch).length > 0) && (Object.keys(oItemData.discrepancy).length > 0) ){
						uiMarker1Btn.html('Update Product Details');
					}else{
						uiMarker1Btn.html('Enter Item Details');
					}
				 }else{
				 	uiMarker1Btn.html('Enter Item Details');
				 }




				uiThis.hide();
				uiEditingCancel.hide();
				$(".to-editing-page").show();

				uiMarker1Btn.show();
				uiEditingButton.show();
				

				uiHideDistribution.hide();
				uidisplayDistribution.show();

				uiStorageCheckBtn.show();

				uiAddingBatch.hide();
				
				uiQuantityCheckBtn.hide();
				uiQuantityCheckHide.show();


		});
	}

	function _displayProductInfoSelected(uiCloneTemplate)
	{

		var uiThis = uiCloneTemplate,
			uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
			uiGetUnitMeasurement = uiParent.data("unit_of_measure_id"),
			oItemDaTa = uiParent.data(),
			oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
			oGetAllConsignee = cr8v_platform.localStorage.get_local_storage({name:"getAllConsigneeReceiveConsignee"}),
			uiLoadingMethodContainer = uiParent.find(".loading-method-container"),
			uiSetQtyToBulk = uiParent.find(".set-qty-to-bulk"),
			uiSetQtyToBag = uiParent.find(".set-qty-to-bag"),
			uiUnitOfMeasure = uiParent.find(".unit-of-measure"),
			uiRemoveDropdown = uiParent.find(".get-dropdown-here").find(".frm-custom-dropdown");

			uiRemoveDropdown.remove();

			var sHtml = "";
			
			// console.log(uiGetProdInventoryID);

			if(sCurrentPath.indexOf("consignee_ongoing_vessel") > -1)
			{
				sHtml = '<div class="f-left width-35percent hide-label">'+
						'	<div class="f-left trigger-bulk-radio">'+
						'		<input type="radio" value="Bulk" class="display-inline-mid width-20px default-cursor set-method-by-bulk'+oItemDaTa.product_inventory_id+'" name="bag-or-bulk'+oItemDaTa.product_inventory_id+'" id="bulk'+oItemDaTa.product_inventory_id+'">'+
						'		<label for="bulk'+oItemDaTa.product_inventory_id+'" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>'+
						'	</div>'+		
						'	<div class="clear"></div>'+
						'</div>';

			}else{
				sHtml = '<div class="f-left width-35percent hide-label">'+
						'	<div class="f-left trigger-bulk-radio">'+
						'		<input type="radio" value="Bulk" class="display-inline-mid width-20px default-cursor set-method-by-bulk'+oItemDaTa.product_inventory_id+'" name="bag-or-bulk'+oItemDaTa.product_inventory_id+'" id="bulk'+oItemDaTa.product_inventory_id+'">'+
						'		<label for="bulk'+oItemDaTa.product_inventory_id+'" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>'+
						'	</div>'+
						'	<div class="f-left margin-left-20 trigger-bag-radio">'+																	
						'		<input type="radio" value="Bags" class="display-inline-mid width-20px default-cursor set-method-by-bags'+oItemDaTa.product_inventory_id+'" name="bag-or-bulk'+oItemDaTa.product_inventory_id+'" id="bags'+oItemDaTa.product_inventory_id+'">'+
						'		<label for="bags'+oItemDaTa.product_inventory_id+'" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bags</label>'+
						'	</div>'+								
						'	<div class="clear"></div>'+
						'</div>';
			}

			

			var uiGetContainer = $(sHtml);
			uiLoadingMethodContainer.append(sHtml);

			var uiSetMethodByBulk = uiParent.find(".set-method-by-bulk"+oItemDaTa.product_inventory_id),
				uiSetMethodByBags = uiParent.find(".set-method-by-bags"+oItemDaTa.product_inventory_id);



				// console.log(JSON.stringify(oItemDaTa));

			if(oItemDaTa.loading_method == "Bulk")
			{
				uiSetMethodByBulk.attr("checked", true);
				uiSetQtyToBag.attr("disabled", true).val("");

			}else{
				uiSetMethodByBags.attr("checked", true);
			}

			uiSetQtyToBulk.val(oItemDaTa.qty);
			uiSetQtyToBulk.number(true, 2);

			if(oItemDaTa.bag != "0")
			{
				uiSetQtyToBag.val(oItemDaTa.bag);
			}

			for(var x in oUnitOfMeasures)
			{

				if(uiGetUnitMeasurement == oUnitOfMeasures[x]['id'])
				{
					sHtmlMeasure = '<option value="'+oUnitOfMeasures[x]['id']+'" selected>'+oUnitOfMeasures[x]['name']+'</option>';
					uiUnitOfMeasure.append(sHtmlMeasure);
				}else{
					if(oUnitOfMeasures[x]['name'] == "kg" || oUnitOfMeasures[x]['name'] == "mt" || oUnitOfMeasures[x]['name'] == "L" )
					{					
						sHtmlMeasure = '<option value="'+oUnitOfMeasures[x]['id']+'">'+oUnitOfMeasures[x]['name']+'</option>';
						uiUnitOfMeasure.append(sHtmlMeasure);
					}
					
				}


			}
			uiUnitOfMeasure.show().css({
				"padding" : "3px 4px",
				"width" :  "50px"
			});




			var uiSetConsigneeItemsContainer = uiParent.find(".set-consignee-items"),
				iCountConsign = 0;

				uiSetConsigneeItemsContainer.html("");

					var sHtmls = '<div class="transfer-prod-location font-0 bggray-white position-rel display-consignee-distribution">';

			for(var i in oItemDaTa.consignee_info){
				var consignee = oItemDaTa.consignee_info[i];
				// console.log(JSON.stringify(consignee));
				var ibagsQty = "";

				(consignee.bag != 0) ? ibagsQty = consignee.bag : ibagsQty = "";

				consignee.distribution_qty

				var sHtml = '<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change display-consignee-items set-cosignee-per-item" count-id="'+iCountConsign+'">'+
							'	<div class="width-50percent display-inline-mid text-center">'+
							'		<div class="select large hide-distribution">'+
							'			<select class="transform-dd select-option-consignee selectConsigneeListSelect">'+
							'				<option value="'+consignee.consignee_id+'">'+consignee.name+'</option>'+
							'			</select>'+
							'		</div>'+
							'	</div>'+
							'	<div class="width-45percent display-inline-mid text-center">'+
							'		<div class="hide-distribution">'+
							'			<input type="text" class="t-small display-inline-mid width-70px margin-left-20 display-consignee-qty" value="'+consignee.distribution_qty+'">'+
							'			<p class="display-inline-mid font-300 margin-left-10 margin-right-15 set-measurement-name">'+consignee.measures_name+'</p>'+
							'			<input type="text" class="t-small display-inline-mid width-70px margin-left-10 display-consignee-bag" value="'+ibagsQty+'">'+
							'			<p class="display-inline-mid margin-left-10 font-300">Bags</p>'+
							'			<div class="display-inline-mid width-20px margin-left-10">'+
							'				<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor deleteProductConsigneeOngoing">'+
							'			</div>'+
							'		</div>'+
							'	</div>'+
							'</div>';
					var oPassData = $(sHtml);
						oPassData.data(consignee);

				uiSetConsigneeItemsContainer.append(oPassData);

				_removeSelectedConsignee();
				iCountConsign++;

			}

				sHtmls = '</div>';
				uiSetConsigneeItemsContainer.append(sHtmls);

				_triggerAddConsigneeItem(uiThis, iCountConsign);


			var uiDisplayConsigneeQty = uiParent.find(".display-consignee-qty"),
				uiDisplayConsigneeBag = uiParent.find(".display-consignee-bag");
				uiDisplayConsigneeQty.number(true, 0);


			if(oItemDaTa.loading_method == "Bulk")
			{

				uiDisplayConsigneeBag.attr("disabled", true).val("");

			}else{
				uiDisplayConsigneeBag.attr("disabled", false);
			}


			// FOR CONSIGNEE DROPDOWN
			var uiSelectConsigneeListSelect = $(".selectConsigneeListSelect"),
			uiParentDropdown = uiSelectConsigneeListSelect.parent(),
			uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');

			uiSiblingDropdown.remove();
			uiSelectConsigneeListSelect.removeClass("frm-custom-dropdown-origin");

			for(var x in oGetAllConsignee)
			{
				var sHtml = '<option value="'+oGetAllConsignee[x]['id']+'">'+oGetAllConsignee[x]['name']+'</option>';
					uiSelectConsigneeListSelect.append(sHtml);
			}

			uiSelectConsigneeListSelect.transformDD();

			CDropDownRetract.retractDropdown(uiSelectConsigneeListSelect);


			_saveUpdateCosigneeProduct();

			CCReceivingConsigneeGoods.bindEvents("triggerLoadingMethod");
			CCReceivingConsigneeGoods.bindEvents("triggerUnitOfMeasure");


	}

	function _triggerAddConsigneeItem(uiCloneTemplate, iCountConsign)
	{
		var uiThis = uiCloneTemplate,
			uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
			oItemDaTa = uiParent.data(),
			uiTriggerAddConsignee = uiParent.find(".trigger-add-consignee"),
			uiSetConsigneeItemsContainer = uiParent.find(".set-consignee-items"),
			uiSetMethodByBulk =uiParent.find(".set-method-by-bulk"+oItemDaTa.product_inventory_id),
			oGetAllConsignee = "",
			iCount = iCountConsign + 1;

			var getConsigneeInterval = setInterval(function(){
				oGetAllConsignee = cr8v_platform.localStorage.get_local_storage({name:"getAllConsigneeReceiveConsignee"});

				if(oGetAllConsignee !== null)
				{
					clearInterval(getConsigneeInterval);

					uiTriggerAddConsignee.off("click.addConsign").on("click.addConsign", function(e){
						e.preventDefault();



						var sHtml = '<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change display-consignee-items set-cosignee-per-item" count-id="'+iCount+' ">'+
										'	<div class="width-50percent display-inline-mid text-center">'+
										'		<div class="select large ">'+
										'			<select class="transform-dd select-option-consignee selectConsigneeListSelect">'+
										'			</select>'+
										'		</div>'+
										'	</div>'+
										'	<div class="width-45percent display-inline-mid text-center">'+
										'		<div class="hide-distribution">'+
										'			<input type="text" class="t-small display-inline-mid width-70px margin-left-20 display-consignee-qty" value="">'+
										'			<p class="display-inline-mid font-300 margin-left-10 margin-right-15 set-measurement-name">'+oItemDaTa.measures_name+'</p>'+
										'			<input type="text" class="t-small display-inline-mid width-70px margin-left-10 display-consignee-bag" value="">'+
										'			<p class="display-inline-mid margin-left-10 font-300">Bags</p>'+
										'			<div class="display-inline-mid width-20px margin-left-10">'+
										'				<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor deleteProductConsigneeOngoing">'+
										'			</div>'+
										'		</div>'+
										'	</div>'+
										'</div>';
							var oPassData = $(sHtml);
							uiSetConsigneeItemsContainer.append(oPassData);

							_removeSelectedConsignee();

							iCount++;

							// FOR CONSIGNEE DROPDOWN
							var uiSelectConsigneeListSelect = $(".selectConsigneeListSelect"),
							uiParentDropdown = uiSelectConsigneeListSelect.parent(),
							uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown');

							uiSiblingDropdown.remove();
							uiSelectConsigneeListSelect.removeClass("frm-custom-dropdown-origin");

							for(var x in oGetAllConsignee)
							{
								var sHtml = '<option value="'+oGetAllConsignee[x]['id']+'">'+oGetAllConsignee[x]['name']+'</option>';
									uiSelectConsigneeListSelect.append(sHtml);
							}

							uiSelectConsigneeListSelect.transformDD();

							CDropDownRetract.retractDropdown(uiSelectConsigneeListSelect);

							$(".display-consignee-qty").number(true, 0);

							var uiDisplayConsigneeBag = uiParent.find(".display-consignee-bag");

							if(uiSetMethodByBulk.is(":checked") === true)
							{
								uiDisplayConsigneeBag.attr("disabled", true);
							}else{
								uiDisplayConsigneeBag.attr("disabled", false);
							}


					});
				}
			});



		

	}

	function _saveUpdateCosigneeProduct()
	{
		var btnTriggerEditingButton = $(".trigger-editing-button"),
			oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
			oGetAllConsignee = cr8v_platform.localStorage.get_local_storage({name:"getAllConsigneeReceiveConsignee"});

		btnTriggerEditingButton.off("click.saveUpdate").on("click.saveUpdate", function(e){
			e.preventDefault();

			var uiThis = $(this),
				uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
				oItemData = uiParent.data(),
				sBagOrBulk = uiParent.find('input[name="bag-or-bulk'+oItemData.product_inventory_id+'"]:checked').val(),
				uiSetQtyToBulk = uiParent.find(".set-qty-to-bulk"),
				uiSetQtyToBag = uiParent.find(".set-qty-to-bag"),
				iSetQtyToBag = parseFloat(uiSetQtyToBag.val()),
				uiUnitOfMeasure = uiParent.find(".unit-of-measure"),
				uiDisplayConsigneeLoadingMethod = uiParent.find(".display-consignee-loading-method"),
				uiDisplayConsigneeQtrToReceive = uiParent.find(".display-consignee-qtr-to-receive"),
				uiGetAllConsigneeItems = uiParent.find(".set-consignee-items").find(".set-cosignee-per-item"),
				uiMarker1Btn = uiParent.find(".marker1"),
				uiEditingCancel = uiParent.find(".editing-cancel"),
				uiHideLabel = uiParent.find(".hide-label"),
				uiDisplayLabel = uiParent.find(".display-label"),
				uidisplayDistribution = uiParent.find(".display-distribution"),
				uiHideDistribution = uiParent.find(".hide-distribution"),
				uiHideLink = uiParent.find(".hide-link"),
				btnHoverStorage = uiParent.find(".btn-hover-storage"),
				uiAddingBatch = uiParent.find(".adding-batch"),
				uiStConsigneeItems = uiParent.find(".set-consignee-items"),
				uiDisplayEditConsigneeDistibution = uiParent.find(".display-edit-consignee-distibution"),
				sUniteMeasurementName = "",
				oGatherConsigneeData= [],
				oCheckConsigneeSum = [],
				oGatherBagsData = [],
				getText = uiThis.text();

				if(getText == "Edit")
				{

					// console.log(receivingID.value);
					// console.log(JSON.stringify(oItemData));

					for(var x in oUnitOfMeasures)
					{
						if(uiUnitOfMeasure.val() == oUnitOfMeasures[x]["id"])
						{
							sUniteMeasurementName = oUnitOfMeasures[x]["name"];
						}
					}



					$.each(uiGetAllConsigneeItems, function(){
						var uiConsignThis = $(this),
							uiConsigneeID = uiConsignThis.find(".selectConsigneeListSelect"),
							uiGetCosigneeQty = uiConsignThis.find(".display-consignee-qty"),
							uiGetCosigneeBag = uiConsignThis.find(".display-consignee-bag"),
							sGetConsigneeName = "";

							oCheckConsigneeSum.push(uiGetCosigneeQty.val());
							oGatherBagsData.push(uiGetCosigneeBag.val());

							for(var x in oGetAllConsignee)
							{
								if(uiConsigneeID.val() == oGetAllConsignee[x]["id"])
								{
									sGetConsigneeName = oGetAllConsignee[x]["name"];
								}
							}

							var oCollectConsign = {
								distribution_qty : uiGetCosigneeQty.val(),
								bag : uiGetCosigneeBag.val(),
								consignee_id : uiConsigneeID.val(),
								name : sGetConsigneeName,
								unit_of_measure_id : uiUnitOfMeasure.val(),
								measures_name : sUniteMeasurementName
							}

							oGatherConsigneeData.push(oCollectConsign);
							
					});

					var total = 0,
						iBagsTotal= 0;

					$.each(oCheckConsigneeSum,function() {
					    total += parseFloat(this);
					});

					$.each(oGatherBagsData,function() {
					    iBagsTotal += parseInt(this);
					});


					//console.log(total);
					//console.log(parseFloat(uiSetQtyToBulk.val()));
					
					if(total != parseFloat(uiSetQtyToBulk.val())){
						// console.log("hindi ok");
						uiThis.html('Save Changes');
						$("body").feedback({title : "Edit Product", message : "Consignee Distribution Quantity must be equal to Product Quantity", type : "danger", icon : failIcon});
					
					}
					else if(uiSetQtyToBag.val() == "" && isNaN( Number( iBagsTotal ) ) === false)
					{
						uiThis.html('Save Changes');
						$("body").feedback({title : "Edit Product", message : "Consignee Distribution Bags must be equal to Product Bags", type : "danger", icon : failIcon});


					}
					else if(uiSetQtyToBag.val() != "" && isNaN( Number( iBagsTotal ) ) === true)
					{
						uiThis.html('Save Changes');
						$("body").feedback({title : "Edit Product", message : "Consignee Distribution Bags must be equal to Product Bags", type : "danger", icon : failIcon});

						
					}
					else if(iSetQtyToBag != iBagsTotal && ((isNaN( Number( iSetQtyToBag ) ) === false) &&  (isNaN( Number( iBagsTotal ) ) === false)))
					{
						uiThis.html('Save Changes');
						$("body").feedback({title : "Edit Product", message : "Consignee Distribution Bags must be equal to Product Bags", type : "danger", icon : failIcon});


					}else{

						var oGatherAllPrdInvt  = {
								product_id : oItemData.product_id,
								product_inventory_id : oItemData.product_inventory_id,
								qty : parseFloat(uiSetQtyToBulk.val()),
								bag : uiSetQtyToBag.val(),
								unit_of_measure_id : uiUnitOfMeasure.val(),
								measures_name : sUniteMeasurementName,
								loading_method : sBagOrBulk,
								consignee_info : oGatherConsigneeData
							}



							_saveUpdateProductPerItem(oGatherAllPrdInvt);

							var qtyValue = parseFloat(uiSetQtyToBulk.val());

							var oSetDataItem = {
								qty : qtyValue.toFixed(2),
								loading_method : sBagOrBulk,
								measures_name : sUniteMeasurementName,
								unit_of_measure_id : uiUnitOfMeasure.val()

							}



							uiParent.data(oSetDataItem);

							oItemReceivingBalance[oItemData.product_inventory_id] = parseFloat(uiSetQtyToBulk.val());
							
							// console.log(JSON.stringify(oGatherConsigneeData));
			
							var options = {title : "Update Product", message : "Successfully Updated", speed : "slow", withShadow : true, type: "success"};
							$("body").feedback(options);

							_displayUpdatedConsigneeDistribution(oGatherConsigneeData, uiParent);

							uiDisplayConsigneeLoadingMethod.html(sBagOrBulk);

							uiDisplayConsigneeQtrToReceive.html(parseFloat(uiSetQtyToBulk.val())+" "+sUniteMeasurementName.toUpperCase()+" ("+uiSetQtyToBag.val()+" Bags)").data({
								"quantity": uiSetQtyToBulk.val(),
								"unitOfMesurement":sUniteMeasurementName.toUpperCase()
							});

							oGatherConsigneeData = [];



							uiMarker1Btn.show();

							$('.to-editing-page').show();
							uiEditingCancel.hide();
							

							uiHideLabel.hide();
							uiDisplayLabel.show();

							uidisplayDistribution.show();
							uiHideDistribution.hide();

							uiHideLink.hide();	

							btnHoverStorage.hide();

							uiAddingBatch.hide();

							uiStConsigneeItems.hide();
							uiDisplayEditConsigneeDistibution.show();

				




						

							
						









						// if(uiSetQtyToBag.val() != "" && isNaN( Number( iBagsTotal ) ) === false)
						// {
							
						// 	if(iSetQtyToBag != iBagsTotal)
						// 	{
						// 		uiThis.html('Save Changes');
						// 		$("body").feedback({title : "Edit Product", message : "Consignee Distribution Bags must be equal to Product Bags", type : "danger", icon : failIcon});
						// 		console.log("first ito");
						// 	}
						// }else if(uiSetQtyToBag.val() == "" && isNaN( Number( iBagsTotal ) ) === false)
						// {
						// 	uiThis.html('Save Changes');
						// 	$("body").feedback({title : "Edit Product", message : "Consignee Distribution Bags must be equal to Product Bags", type : "danger", icon : failIcon});
						// 	console.log("second ito");
						// }else{


							

						// }


						





						





					}





					

				}


		});
	}

	function _CheckOngoingReceivingConsignee()
	{


		// CHECK IF READY TO  COMPLETE
		var BtnMarker1 = $(".marker1");

		BtnMarker1.off("click.checkit").on("click.checkit", function(){
			var uiThis = $(this),
				text1 = uiThis.text();

			if(text1 == 'Update Product Details' || text1 == 'Enter Item Details'){

				var uiGetMainContainer = $("#displayProductItemList").find(".border-top.border-blue.box-shadow-dark"),
					iCountComplete = 0,
					iCountCheck = 0;

				$.each(uiGetMainContainer, function(){
					var uiThisContainer = $(this),
						oItemData = uiThisContainer.data();

						if( (oItemData.hasOwnProperty("batch")) && (oItemData.hasOwnProperty("discrepancy"))){
							if( (Object.keys(oItemData.batch).length > 0) && (Object.keys(oItemData.discrepancy).length > 0))
							{
								iCountComplete++;
							}
						}
						iCountCheck++;


					// console.log(JSON.stringify(oItemData));
					// iCountComplete++;


				});
				console.log(iCountComplete);
				console.log(iCountCheck);

				if(iCountComplete == iCountCheck)
				{
					$('.complete-receiving-record').attr('disabled',false);
					_updateReceivingRecordComplete();
					_addCompleteReceivingRecord();
				}else{
					$('.complete-receiving-record').attr('disabled',true);

				}
			}

		});

	}

	function _updateReceivingRecordComplete()
	{
		var receivingId = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"});
		var oOptions = {
			url : BASEURL+"receiving/update_to_compelete_record",
			type : "POST",
			data : {"receiving_id":receivingId.value},
			success : function(oResponse){
				console.log(oResponse);
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _removeProductInventory(iProdInventoryId)
	{
		oAjax = {
			url : BASEURL+"receiving/remove_product_inventory",
			type : "POST",
			data : {"product_inventory_id":iProdInventoryId},
			success : function(oResponse){
				console.log(oResponse);
				
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oAjax);

	}

	function _addCompleteReceivingRecord()
	{		
		var oGetAllItem = {},
			uiBerthingStartDate = $("#berthing-start-date"),
			uiBerthingStartTime = $("#berthing-start-time"),
			uiUnloadingStartDate = $("#unloading-start-date"),
			uiUnloadingStartTime = $("#unloading-start-time"),
			uiUnloadingEndDate = $("#unloading-end-date"),
			uiUnloadingEndTime  =$("#unloading-end-time"),
			uiDurationDateTime = $("#duration-date-time"),
			uiDurationDate = $("#departure-date"),
			uiDurationTime = $("#departure-time"),
			btnConfirmCompleteReceivingRecord = $("#confirm-complete-receiving-record"),
			uiTriggerComputeDuration = $(".trigger-compute-duration"),
			iDurationDateTime = 0,
			iResultDates = 0,
			arrErrorCount = [],
			oGetFromData = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"});

			if(oGetFromData !== null)
			{
				oGetAllItem = oGetFromData;
			}else{
				

				var getCompleteInterval = setInterval(function(){
					var oGetFromComplete = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItemComplete"});
						
					if(oGetFromComplete !== null)
					{
						oGetAllItem = oGetFromComplete;
						clearInterval(getCompleteInterval);
					}

				}, 300);


			}



			
				uiBerthingStartDate.val(oGetAllItem["berthing_time_date"]);
				uiBerthingStartTime.val(oGetAllItem["berthing_time_set"]);


			


			uiTriggerComputeDuration.off("click.duration change.durations").on("click.duration change.durations", function(){

					if(uiUnloadingStartDate.val().trim() != "" 
						&& uiUnloadingStartTime.val().trim() != "" 
						&& uiUnloadingEndDate.val().trim() != ""
						&& uiUnloadingEndTime.val().trim() != "")
					{
						var sUnloadingStartDate = uiUnloadingStartDate.val().split('/').join("-"),
							sUnloadingEndDate = uiUnloadingEndDate.val().split('/').join("-"),
							sStartTime = _convertHours(uiUnloadingStartTime.val()),
							sEndTime = _convertHours(uiUnloadingEndTime.val()),
							sStartDateTime = sUnloadingStartDate+" "+sStartTime,
							sEndDateTiem = sUnloadingEndDate+" "+sEndTime;

							var sGetTimesTampStart = _convertDateAndHoursToTimeStamp(sStartDateTime),
								sGetTimesTampEnd = _convertDateAndHoursToTimeStamp(sEndDateTiem),
								sGetResultTimeStamp = sGetTimesTampEnd - sGetTimesTampStart,
								sConvertToml = (sGetResultTimeStamp) * 1000;

								iResultDates = sGetResultTimeStamp;

						// uiDurationDateTime.html("");
						var date = new Date(sConvertToml);
							var str = date.getUTCDate()-1 + " days, "+date.getUTCHours() + " hours, "+date.getUTCMinutes() + " minutes, "+date.getUTCSeconds() + " seconds. ";

							uiDurationDateTime.text(str);

							// console.log("ok");

							iDurationDateTime = sConvertToml;
							// console.log(iDurationDateTime);
					}
				});




			btnConfirmCompleteReceivingRecord.off("click.confirmed").on("click.confirmed", function(){

				(uiBerthingStartDate.val().trim() == "") ? arrErrorCount.push("Berthing Date Start Required") : "";
				(uiBerthingStartTime.val().trim() == "") ? arrErrorCount.push("Berthing Time Start Required") : "";
				(uiUnloadingStartDate.val() == "") ? arrErrorCount.push("Unloading Date Start Required") : "";
				(uiUnloadingStartTime.val() == "") ? arrErrorCount.push("Unloading Time Start Required") : "";
				(uiUnloadingEndDate.val() == "") ? arrErrorCount.push("Unloading Date End Required") : "";
				(uiUnloadingEndTime.val() == "") ? arrErrorCount.push("Unloading Time End Required") : "";
				(uiDurationDate.val() == "") ? arrErrorCount.push("Duration Date Required") : "";
				(uiDurationTime.val() == "") ? arrErrorCount.push("Duration Time Required") : "";

				(iResultDates < 0) ? arrErrorCount.push("Unloading Date/Time End must greater than Unloading Date/Time Start") : "";

				if(arrErrorCount.length > 0)
				{
					$("body").feedback({title : "Completing Receiving Record", message : arrErrorCount.join("<br />"), type : "danger", icon : failIcon});
				}else{

					


					var oData = {
						receiving_id : oGetAllItem["receiving_id"],
						berthing_date : uiBerthingStartDate.val().trim(),
						berthing_time : uiBerthingStartTime.val().trim(),
						start_unloading_date : uiUnloadingStartDate.val(),
						start_unloading_time : uiUnloadingStartTime.val(),
						end_unloading_date : uiUnloadingEndDate.val(),
						end_unloading_time : uiUnloadingEndTime.val(),
						duration_date : uiDurationDate.val(),
						duration_time : uiDurationTime.val(),
						unloading_duration : iDurationDateTime
					}


					_saveCompleteReceivingRecordFormVessel(oData);

					var options = {title : "Successfully Completed", message : "Consignee Goods Completed!", speed : "slow", withShadow : true, type: "success"};
					$("body").feedback(options);

				}

				arrErrorCount = [];
			});
	}

	function _receivingListAddCompleteRecord()
	{
		var uiShowModalComplete = $(".show-modal-complete");


			uiShowModalComplete.off("click.checkIfWhat").on("click.checkIfWhat", function(){
				var uiThisClick = $(this),
					uiOrigin = uiThisClick.attr("data-origin"),
					uiParent = uiThisClick.closest(".displayReceiveConsignRow"),
					oItemData = uiParent.data();
					

					if(uiOrigin == "Vessel")
					{
						_receivingListAddVessel(oItemData);

						// console.log(JSON.stringify(oItemData));

						$("body").css({overflow:'hidden'});
					
						$(".show-complete-record-vessel-modal").addClass("showed");

						$(".show-complete-record-vessel-modal .close-me").on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$(".show-complete-record-vessel-modal").removeClass("showed");
						});

					}
					else if(uiOrigin == "Truck")
					{
						
						$("body").css({overflow:'hidden'});
					
						$(".show-complete-record-truck-modal").addClass("showed");

						$(".show-complete-record-truck-modal .close-me").on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$(".show-complete-record-truck-modal").removeClass("showed");
						});

						_receivingListAddTruck(oItemData);
					}
					
					


			});
	}

	function _receivingListAddTruck(oItemData)
	{
		var uiSetTareIn = $("#set-tare-in"),
			uiSetTareOut = $("#set-tare-out"),
			uiSetNetWeight = $("#set-net-weight"),
			uiTriggerComputeNetWeight = $(".trigger-compute-net-weight"),
			btnCompleteReceivingRecordTruck = $("#complete-receiving-record-truck"),
			iTotal = 0;

			uiSetTareIn.number(true, 2);
			uiSetTareOut.number(true, 2);


			uiTriggerComputeNetWeight.off("keyup.compute").on("keyup.compute", function(){
				var iGetTareIn = parseFloat(uiSetTareIn.val()),
					iGetTareOut = parseFloat(uiSetTareOut.val());

					if(uiSetTareIn.val().trim() != "" && uiSetTareOut.val().trim() != "")
					{
						uiSetNetWeight.html("");

						iTotal = iGetTareIn - iGetTareOut;
						uiSetNetWeight.html($.number(iTotal, 2)+" MT");
					}	
			});

			btnCompleteReceivingRecordTruck.off("click.confirmed").on("click.confirmed", function(e){
				e.preventDefault();
				if(uiSetTareIn.val().trim() != "" && uiSetTareOut.val().trim() != "")
				{
					$("body").feedback({title : "Complete Receiving Record", message : "Successfully Complete", type : "success"});

					var oData = {
						"tare_in" : parseFloat(uiSetTareIn.val().trim()),
						"tare_out" : parseFloat(uiSetTareOut.val().trim()),
						"net_weight" : $.number(iTotal, 2)
					}


					_saveConsigneeOngoingTruck(oData);
				}else{
					$("body").feedback({title : "Complete Receiving Record", message : "Please fill up Truck Tare In and Tare Out", type : "danger", icon : failIcon});
				}

			});

	}

	function _receivingListAddVessel(oItemData)
	{
		var uiBerthingStartDate = $("#berthing-start-date"),
			uiBerthingStartTime = $("#berthing-start-time"),
			uiUnloadingStartDate = $("#unloading-start-date"),
			uiUnloadingStartTime = $("#unloading-start-time"),
			uiUnloadingEndDate = $("#unloading-end-date"),
			uiUnloadingEndTime  =$("#unloading-end-time"),
			uiDurationDateTime = $("#duration-date-time"),
			uiDurationDate = $("#departure-date"),
			uiDurationTime = $("#departure-time"),
			btnConfirmCompleteReceivingRecord = $("#confirm-complete-receiving-record"),
			uiTriggerComputeDuration = $(".trigger-compute-duration"),
			iDurationDateTime = 0,
			arrErrorCount = [];

			uiBerthingStartDate.val(oItemData.departure_date);
			uiBerthingStartTime.val(oItemData.departure_time);



			uiTriggerComputeDuration.off("click.duration change.durations").on("click.duration change.durations", function(){

				if(uiUnloadingStartDate.val().trim() != "" 
					&& uiUnloadingStartTime.val().trim() != "" 
					&& uiUnloadingEndDate.val().trim() != ""
					&& uiUnloadingEndTime.val().trim() != "")
				{
					var sUnloadingStartDate = uiUnloadingStartDate.val().split('/').join("-"),
						sUnloadingEndDate = uiUnloadingEndDate.val().split('/').join("-"),
						sStartTime = _convertHours(uiUnloadingStartTime.val()),
						sEndTime = _convertHours(uiUnloadingEndTime.val()),
						sStartDateTime = sUnloadingStartDate+" "+sStartTime,
						sEndDateTiem = sUnloadingEndDate+" "+sEndTime;

						var sGetTimesTampStart = _convertDateAndHoursToTimeStamp(sStartDateTime),
							sGetTimesTampEnd = _convertDateAndHoursToTimeStamp(sEndDateTiem),
							sGetResultTimeStamp = sGetTimesTampEnd - sGetTimesTampStart,
							sConvertToml = (sGetResultTimeStamp) * 1000;

					// uiDurationDateTime.html("");
					var date = new Date(sConvertToml);
						var str = date.getUTCDate()-1 + " days, "+date.getUTCHours() + " hours, "+date.getUTCMinutes() + " minutes, "+date.getUTCSeconds() + " seconds. ";

						uiDurationDateTime.text(str);

						// console.log("ok");

						iDurationDateTime = sConvertToml;
						// 	console.log(iDurationDateTime);
				}
			});

			btnConfirmCompleteReceivingRecord.off("click.confirmed").on("click.confirmed", function(){

				(uiBerthingStartDate.val().trim() == "") ? arrErrorCount.push("Berthing Date Start Required") : "";
				(uiBerthingStartTime.val().trim() == "") ? arrErrorCount.push("Berthing Time Start Required") : "";
				(uiUnloadingStartDate.val() == "") ? arrErrorCount.push("Unloading Date Start Required") : "";
				(uiUnloadingStartTime.val() == "") ? arrErrorCount.push("Unloading Time Start Required") : "";
				(uiUnloadingEndDate.val() == "") ? arrErrorCount.push("Unloading Date End Required") : "";
				(uiUnloadingEndTime.val() == "") ? arrErrorCount.push("Unloading Time End Required") : "";
				(uiDurationDate.val() == "") ? arrErrorCount.push("Duration Date Required") : "";
				(uiDurationTime.val() == "") ? arrErrorCount.push("Duration Time Required") : "";

				if(arrErrorCount.length > 0)
				{
					$("body").feedback({title : "Completing Receiving Record", message : arrErrorCount.join("<br />"), type : "danger", icon : failIcon});
				}else{

					


					var oData = {
						receiving_id : oItemData.receivingId,
						berthing_date : uiBerthingStartDate.val().trim(),
						berthing_time : uiBerthingStartTime.val().trim(),
						start_unloading_date : uiUnloadingStartDate.val(),
						start_unloading_time : uiUnloadingStartTime.val(),
						end_unloading_date : uiUnloadingEndDate.val(),
						end_unloading_time : uiUnloadingEndTime.val(),
						duration_date : uiDurationDate.val(),
						duration_time : uiDurationTime.val(),
						unloading_duration : iDurationDateTime
					}

					_saveCompleteReceivingRecordFormVessel(oData);

					var options = {title : "Successfully Completed", message : "Consignee Goods Completed!", speed : "slow", withShadow : true, type: "success"};
					$("body").feedback(options);

				}

				arrErrorCount = [];
			});
	}


	function _convertHours(time)
	{
		// Convert 10:15 PM to 22:15

		// var time = uiUnloadingEndTime.val();
		var hours = Number(time.match(/^(\d+)/)[1]);
		var minutes = Number(time.match(/:(\d+)/)[1]);
		var AMPM = time.match(/\s(.*)$/)[1];
		if(AMPM == "PM" && hours<12) hours = hours+12;
		if(AMPM == "AM" && hours==12) hours = hours-12;
		var sHours = hours.toString();
		var sMinutes = minutes.toString();
		if(hours<10) sHours = "0" + sHours;
		if(minutes<10) sMinutes = "0" + sMinutes;
		// console.log(sHours + ":" + sMinutes);
		var sGetHoursAndMins =  sHours + ":" + sMinutes;

		return sGetHoursAndMins;
	}

	function _convertDateAndHoursToTimeStamp(sStartDateTime)
	{

		var datum = Date.parse(sStartDateTime);
 			return datum/1000;

	}

	function _displayConsigneeDistribution(oConsigneeItems, uiCloneTemplate)
	{
			var uiConsigneeContainer = uiCloneTemplate.find(".display-consignee-distribution"),
				uiConsigneeTemplate = uiConsigneeContainer.find(".display-consignee-items");
			
			uiConsigneeContainer.html("");
			for(var x in oConsigneeItems)
			{
				
				oConsignee = oConsigneeItems[x];

				var uiCloneConsigneeTemplate = uiConsigneeTemplate.clone();
				uiCloneConsigneeTemplate.removeClass("display-consignee-items").show();
				uiCloneConsigneeTemplate.data(oConsignee);
				uiCloneConsigneeTemplate.find(".display-consignee-distribution-name").html(oConsignee.name);

				if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1)
				{
					uiCloneConsigneeTemplate.find(".display-consignee-distribution-quantity").html($.number(oConsignee.distribution_qty, 0)+" "+oConsignee.measures_name.toUpperCase()+" ("+oConsignee.bag+" Bags)");
				}else{
					uiCloneConsigneeTemplate.find(".display-consignee-distribution-quantity").html($.number(oConsignee.distribution_qty, 0)+" "+oConsignee.measures_name.toUpperCase());
				}
				

				uiConsigneeContainer.append(uiCloneConsigneeTemplate);
			}
	}

	function _displayEditConsigneeDistribution(oConsigneeItems, uiCloneTemplate)
	{
			var uiConsigneeContainer = uiCloneTemplate.find(".display-edit-consignee-distibution"),
				uiParentThis = uiConsigneeContainer.parent(),
				uiConsigneeTemplate = uiParentThis.find(".display-edit-consignee-items");
			
			uiConsigneeContainer.html("");
			for(var x in oConsigneeItems)
			{
				
				oConsignee = oConsigneeItems[x];


				var uiCloneConsigneeTemplate = uiConsigneeTemplate.clone();
				uiCloneConsigneeTemplate.removeClass("display-edit-consignee-items").show();
				uiCloneConsigneeTemplate.data(oConsignee);
				uiCloneConsigneeTemplate.find(".display-consignee-distribution-name").html(oConsignee.name);

				if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1  || sCurrentPath.indexOf("consignee_complete_truck") > -1 )
				{
					uiCloneConsigneeTemplate.find(".display-consignee-distribution-quantity").html($.number(oConsignee.distribution_qty, 0)+" "+oConsignee.measures_name.toUpperCase()+" ("+oConsignee.bag+" Bags)");
				}else{
					uiCloneConsigneeTemplate.find(".display-consignee-distribution-quantity").html($.number(oConsignee.distribution_qty, 0)+" "+oConsignee.measures_name.toUpperCase());
				}
				

				uiConsigneeContainer.append(uiCloneConsigneeTemplate);
			}
	}

	function _displayUpdatedConsigneeDistribution(oConsigneeItems, uiCloneTemplate){

		var uiConsigneeContainer = uiCloneTemplate.find(".display-edit-consignee-distibution");

			uiConsigneeContainer.html("");

			for(var x in oConsigneeItems)
			{

				var sHtml = '<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change" >'+
							'	<div class="width-50percent display-inline-mid text-center">'+
							'		<p class="font-14 font-400 display-distribution display-consignee-distribution-name">'+oConsigneeItems[x]["name"]+'</p>'+
							'	</div>'+
							'	<div class="width-45percent display-inline-mid text-center">'+
							'		<p class="font-14 font-400 display-distribution display-consignee-distribution-quantity">'+oConsigneeItems[x]["distribution_qty"]+' '+oConsigneeItems[x]["measures_name"].toUpperCase()+'('+oConsigneeItems[x]["bag"]+' Bags)</p>'+
							'	</div>'+
							'</div>';
					var oGetData = $(sHtml);

					uiConsigneeContainer.append(oGetData);

			}


	}

	function _dispalyReceivingNotes(oNoteItems)
	{
		var uiNotesContainer = $("#display-receiving-notes"),
			uiNotesTemplate = $(".display-receiving-notes-row");

			uiNotesContainer.html("");

			// console.log("asdsa");

			for(var x in oNoteItems)
			{
				var oNotes = oNoteItems[x],
					sMessage = "",
					iMessageCount = oNotes.note.length;

					if(iMessageCount > 300)
					{
						sMessage = oNotes.note.substr(0, 300);
						sMessage +="...";
					}else{
						sMessage = oNotes.note;
					}

				var uiNotesCloneTemplate = uiNotesTemplate.clone();
				uiNotesCloneTemplate.removeClass("display-receiving-notes-row").show();
				uiNotesCloneTemplate.data(oNotes);
				uiNotesCloneTemplate.data({"message":oNotes.note});
				uiNotesCloneTemplate.find(".display-receiving-notes-subject").html("Subject: "+oNotes.subject);
				uiNotesCloneTemplate.find(".display-receiving-notes-date-created").html("Date/Time: "+oNotes.date_created);
				uiNotesCloneTemplate.find(".display-receiving-notes-message").html(sMessage);

				uiNotesContainer.append(uiNotesCloneTemplate);
				// console.log(JSON.stringify(oNotes));

				_triggerNoteLessMore()
			}

	}
	function _displayEditProductItems(oItems)
	{
		var uiDisplayEditProductCurrentContainer = $("#displayEditProductCurrent"),
			uiDisplayEditProductCurrentAll = $(".display-edit-product-current-all"),
			uiCloneTemplate ="";

			uiDisplayEditProductCurrentContainer.html("");

		for(var x in oItems)
		{
			// console.log(JSON.stringify(oItems[x]));
			var item = oItems[x];

			var uiCloneTemplate = uiDisplayEditProductCurrentAll.clone();
			uiCloneTemplate.removeClass("display-edit-product-current-all").show();
			uiCloneTemplate.data(item);
			uiCloneTemplate.find(".display-edit-vessel-product-name-sku").html(" SKU #"+item.sku+" - "+item.product_name);
			uiCloneTemplate.find(".display-edit-vessel-product-image").attr("src", item.product_image);

			uiDisplayEditProductCurrentContainer.append(uiCloneTemplate);
			CCReceivingConsigneeGoods.bindEvents('displayEditProductCurrentRow');
			
		}
		
	}

	function _saveCompleteReceivingRecordFormVessel(oData)
	{
		oAjax = {
			url : BASEURL+"receiving/save_complete_receiving_record_vessel",
			type : "POST",
			data : {"data":JSON.stringify(oData)},
			success : function(oResponse){

				window.location.href = BASEURL+"receiving/consignee_complete_vessel";
			}
		}
		cr8v_platform.CconnectionDetector.ajax(oAjax);

		cr8v_platform.localStorage.delete_local_storage({name:"setSelectedReceivingConsigneeItem"});
		cr8v_platform.localStorage.delete_local_storage({name:"SetSelectedProductForReceivingConsignee"});
		cr8v_platform.localStorage.delete_local_storage({name:"dataUploadedDocument"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllConsigneeReceiveConsignee"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllProductsReceiveConsignee"});
		cr8v_platform.localStorage.delete_local_storage({name:"getAllVesselData"});
		

	}

	function _saveConsigneeOngoingTruck(oData)
	{
		var oReceivingId = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"}),
			iReceivedId = oReceivingId.value;

		var oOptions = {
			url : BASEURL+"receiving/save_consignee_ongoing_truck_tare",
			type : "POST",
			data : {"receiving_id":iReceivedId, "data":JSON.stringify(oData)},
			success : function(oResponse){
				console.log(oResponse);
				window.location.href = BASEURL+"receiving/consignee_complete_truck";
			}

		}
		cr8v_platform.CconnectionDetector.ajax(oOptions);


		cr8v_platform.localStorage.get_local_storage({name:"currentReceivingNumber"});
		cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"});

	}

	function _documentEvents(bTemporary)
	{
		var btnUploadDocs =  $('[modal-target="upload-documents"]'),
			oRecord = cr8v_platform.localStorage.get_local_storage({"name" : "setCurrentDisplayReceivedId"}),
			receivingId = 0;

			if(oRecord !== null){

				receivingId = oRecord.value
			}else{
				receivingId = 0;
			}
			// console.log(receivingId);

		btnUploadDocs.off('click');

	
		var extraData = { "receiving_id" : receivingId };

		if(bTemporary === true){
			extraData["temporary"] = true;
		}

		btnUploadDocs.cr8vFileUpload({
			url : BASEURL + "receiving/upload_document_consignee",
			accept : "pdf,csv",
			filename : "attachment",
			extraField : extraData,
			onSelected : function(bIsValid){
				if(bIsValid){
					//selected valid
				}
			},
			success : function(oResponse){
				if(oResponse.status){
					$('[cr8v-file-upload="modal"] .close-me').click();
					var oDocs = oResponse.data[0]["documents"];
					if(bTemporary){
						oUploadedDocuments[Object.keys(oUploadedDocuments).length] = oResponse.data[0]["documents"][0];
						_displayDocuments(oUploadedDocuments);
					}else{
						_displayDocuments(oDocs);
					}

					// console.log(JSON.stringify(oDocs));
				}
			},
			error : function(error){
				console.log(error);
				$("body").feedback({title : "Message", message : "Was not able to upload, file maybe too large", type : "danger", icon : failIcon});
			}
		});

		// console.log("documents in");
	}

	function _displayDocuments(oDocs)
	{
		// console.log(JSON.stringify(oDocs));

		var bChangeBg = false,
			iCountDocument = 0;

		var uiContainer = $("#displayDocuments"),
			sTemplate = '<div class="table-content position-rel tbl-dark-color">'+
							'<div class="content-show padding-all-10">'+
								'<div class="width-85per display-inline-mid padding-left-10">'+
									'<i class="fa font-30 display-inline-mid width-50px icon"></i>'+
									'<p class=" display-inline-mid doc-name">Document Name</p>'+
								'</div>'+
								'<p class=" display-inline-mid date-time"></p>'+
							'</div>'+
							'<div class="content-hide" style="height: 50px;">'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30 view-document">View</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid download-document">'+
									'<button class="btn general-btn">Download</button>'+
								'</a>'+
								'<a href="#" class="display-inline-mid">'+
									'<button class="btn general-btn padding-left-30 padding-right-30">Print</button>'+
								'</a>'+
							'</div>'+
						'</div>';

		uiContainer.html("");
		for(var i in oDocs){
			var uiList = $(sTemplate),
				ext = (oDocs[i]["document_path"].substr(oDocs[i]["document_path"].lastIndexOf('.') + 1)).toLowerCase();

			if(ext == 'csv'){
				uiList.find(".icon").addClass('fa-file-excel-o');
			}else if(ext == 'pdf'){
				uiList.find(".icon").addClass('fa-file-pdf-o');
			}

			uiList.data(oDocs[i]);
			uiList.find(".doc-name").html(oDocs[i]["document_name"]);
			uiList.find(".date-time").html(oDocs[i]["date_added_formatted"]);
			if(bChangeBg){
				uiList.removeClass('tbl-dark-color');
				bChangeBg = false;
			}else{
				bChangeBg = true;
			}


			uiContainer.append(uiList);
			
		}



		cr8v_platform.localStorage.set_local_storage({
			name : "dataUploadedDocument",
			data : oDocs
		});

		_downloadDocument(oDocs);

	}

	function _downloadDocument(oDocs)
	{
		
		var	btnDownloadDocument = $(".download-document"),
			btnViewDocument = $(".view-document");


		btnDownloadDocument.off("mouseenter.download").on("mouseenter.download", function(e){
			e.preventDefault();
			var uiDownloadThis = $(this),
				uiParentContainer = uiDownloadThis.closest('.table-content.position-rel'),
				uiGetDataPath = uiParentContainer.data("document_path"),
				uiGetDataName = uiParentContainer.data("document_name");

				uiDownloadThis.attr("href", uiGetDataPath);
				uiDownloadThis.attr("download", uiGetDataName);

		});


		btnViewDocument.off("click.viewDocument").on("click.viewDocument", function(e){
			e.preventDefault();
			var uiParentContainer = $(this).closest('.table-content.position-rel'),
				uiGetDataPath = uiParentContainer.data("document_path");

				window.open(uiGetDataPath, '_blank');
		});
	}

	function _createCancelConfirmation()
	{
		var cancelBtn = $(".cancel-creating-record");

		cancelBtn.off('click.cancelCreating').on('click.cancelCreating', function(){

			$("body").css({overflow:'hidden'});
				
			$("#cancelModal").addClass("showed");

			$("#cancelModal .close-me").on("click",function(){
				$("body").css({'overflow-y':'initial'});
				$("#cancelModal").removeClass("showed");
			});

			var uiConfirmCancelBtn = $("#confirmCancel");

			 uiConfirmCancelBtn.off('click.confirmedcancel').on('click.confirmedcancel', function(){
			 	window.location.href = BASEURL+"receiving/consignee_receiving";
			 });

		});
	}

	function buildUnitOfMeasure()
	{
		var uiSelectMeasurement = $(".unit-of-measure"),
			oGetData = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
			oData = oGetData;
		
		uiSelectMeasurement.html("");
		for(var x in oData)
		{
			if(oData[x]['name'] == "kg" || oData[x]['name'] == "mt" || oData[x]['name'] == "L" )
			{					
				if(oData[x]['name'] == "mt"){
					var sHtmlMeasure = '<option value="'+oData[x]['id']+'" selected>'+oData[x]['name']+'</option>';
				 	uiSelectMeasurement.append(sHtmlMeasure);
				}else{
					var sHtmlMeasure = '<option value="'+oData[x]['id']+'">'+oData[x]['name']+'</option>';
				 	uiSelectMeasurement.append(sHtmlMeasure);
				}
			}

		}

		$(".unit-of-measure").show().css({
			"padding" : "3px 4px",
			"width" :  "50px"
		});
		$(".remove-measure-drop").find(".frm-custom-dropdown").remove();
	}

	function buildUnitOfMeasureBillMeasurement()
	{
		var uiSelectMeasurement = $(".unit-of-measure"),
			oGetData = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
			oData = oGetData;
		
		uiSelectMeasurement.html("");
		for(var x in oData)
		{
			if(oData[x]['name'] == "kg" || oData[x]['name'] == "mt" || oData[x]['name'] == "L" )
			{
				if(oData[x]['name'] == "mt"){
					var sHtmlMeasure = '<option value="'+oData[x]['id']+'" selected>'+oData[x]['name']+'</option>';
				 	uiSelectMeasurement.append(sHtmlMeasure);
				}else{
					var sHtmlMeasure = '<option value="'+oData[x]['id']+'">'+oData[x]['name']+'</option>';
				 	uiSelectMeasurement.append(sHtmlMeasure);
				}
				
			}

		}

		$(".unit-of-measure").show().css({
			"padding" : "3px 4px",
			"width" :  "50px"
		});
		$(".remove-measure-drop").find(".frm-custom-dropdown").remove();

		_triggerUnitMeasureBillVolume();
	}

	function _triggerUnitMeasure(ui)
	{
		var uiContainer = ui.closest(".product_selected_receive_consign"),
			uiQuantity = uiContainer.find(".qty-received"),
			uiMeasure = uiContainer.find(".unit-of-measure"),
			sSelected = uiMeasure.find("option:selected").text(),
			sSelectUnit = uiMeasure.text(),
			uiLabel = uiContainer.find(".display-unit-measure-label"),
			sMeasurement = '';
			
			uiLabel.html(sSelected.toUpperCase());

			sMeasurement = sSelected.toLowerCase();

			uiMeasure.off("change.measureGet").on("change.measureGet", function(){
				var uiThis = $(this),
					iQuantity = uiQuantity.val().trim(),
					sMeasured = uiThis.find("option:selected").text(),
					iResult = 0;
					
					uiLabel.html(sMeasured.toUpperCase());

					iResult = UnitConverter.convert(sMeasurement, sMeasured.toLowerCase(), iQuantity);

					uiQuantity.val(iResult);

					sMeasurement = sMeasured.toLowerCase();
					
			});

	}

	function _triggerUnitMeasureBillVolume()
	{
		var uiAddInputVesselLandingVolume = $("#addInputVesselLandingVolume"),
			uiBillMeasurement = $("#bill-lading-volume-measure"),
			uiDefaultMeasure = uiBillMeasurement.find("option:selected").text().toLowerCase(),
			sGetMeasurementVal = '';

			sGetMeasurementVal = uiDefaultMeasure;

			uiBillMeasurement.off("change.changeBillAmount").on("change.changeBillAmount", function(){
				var uiThis = $(this),
					sMeasurementName = uiThis.find("option:selected").text().toLowerCase(),
					sValue = uiAddInputVesselLandingVolume.val().trim(),
					iResult = 0;

					iResult = UnitConverter.convert(sGetMeasurementVal, sMeasurementName, sValue);

					uiAddInputVesselLandingVolume.val(iResult);

					sGetMeasurementVal = sMeasurementName;

			});
	}

	function _triggerUnitMeasureBillVolumeEdit()
	{
		var uiAddInputVesselLandingVolume = $("#display-edit-vessel-bill-of-landing"),
			uiBillMeasurement = $("#bill-lading-volume-measure"),
			uiDefaultMeasure = uiBillMeasurement.find("option:selected").text().toLowerCase(),
			sGetMeasurementVal = '';

			sGetMeasurementVal = uiDefaultMeasure;

			uiBillMeasurement.off("change.changeBillAmount").on("change.changeBillAmount", function(){
				var uiThis = $(this),
					sMeasurementName = uiThis.find("option:selected").text().toLowerCase(),
					sValue = uiAddInputVesselLandingVolume.val().trim(),
					iResult = 0;

					// console.log(sGetMeasurementVal);
					// console.log(sMeasurementName);
					// console.log(sValue);

					iResult = UnitConverter.convert(sGetMeasurementVal, sMeasurementName, sValue);

					uiAddInputVesselLandingVolume.val(iResult);

					sGetMeasurementVal = sMeasurementName;

			});
	}

	function _removeSelectedConsignee()
	{
		var  uiBtn = $(".deleteProductConsigneeOngoing");
			
			uiBtn.off("click.deleteOngoing").on("click.deleteOngoing", function(){
				var uiThis = $(this),
					uiParent = uiThis.closest(".display-consignee-items");

					uiParent.remove();
			});
	}

	function _buildUnitOfMeasureEditVessel(sUnitMeaure)
	{
		var uiSelectMeasurement = $(".unit-of-measure"),
			oGetData = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
			oData = oGetData;
		
		uiSelectMeasurement.html("");
		for(var x in oData)
		{
			if(oData[x]['name'] == "kg" || oData[x]['name'] == "mt" || oData[x]['name'] == "L" )
			{	
				

				 	if(oData[x]['name'] == sUnitMeaure){
						var sHtmlMeasure = '<option value="'+oData[x]['id']+'" selected>'+oData[x]['name']+'</option>';
				 		uiSelectMeasurement.append(sHtmlMeasure);
					}else{
						var sHtmlMeasure = '<option value="'+oData[x]['id']+'">'+oData[x]['name']+'</option>';
				 		uiSelectMeasurement.append(sHtmlMeasure);
					}
				
			}

		}

		$(".unit-of-measure").show().css({
			"padding" : "3px 4px",
			"width" :  "50px"
		});
		$(".remove-measure-drop").find(".frm-custom-dropdown").remove();

		_triggerUnitMeasureBillVolumeEdit()
	}

	function _triggerNoteLessMore()
	{
		var uiBtn = $(".show-notes-messge-length");

		uiBtn.off("click.showNotes").on("click.showNotes", function(e){
			e.preventDefault();
			var uiThis = $(this),
				uiParent = uiThis.closest(".notes-contain"),
				sMessage = uiParent.data("message"),
				uiMessage = uiParent.find(".show-notes-message");

				if(uiThis.text() == "Show More")
				{
					uiMessage.html(sMessage);
					uiThis.text("Show Less");
					
				}else{
					var sMessageContainer = "",
						iMessageCount = sMessage.length;

						if(iMessageCount > 300)
						{
							sMessageContainer = sMessage.substr(0, 300);
							sMessageContainer +="...";
						}else{
							sMessageContainer = sMessage;
						}

						uiMessage.html(sMessageContainer);
						uiThis.text("Show More");

				}

		});

	}
	


	function bindEvents(sAction, ui)
	{

		if(sCurrentPath.indexOf("consignee_create") > -1) 
		{

			_createCancelConfirmation();

			
			var uiConfirmSaveNewConsigneeGoodsBtn = $(".confirmSaveNewConsigneeGoods"),
				uiSaveReceivingNumber = $("#save-receiving-number");

			uiConfirmSaveNewConsigneeGoodsBtn.off("click.confirmAdd").on("click.confirmAdd", function(e){
				e.preventDefault();
				$("body").css({overflow:'hidden'});
				
				$("#confirmCreateRecord").addClass("showed");

				$("#confirmCreateRecord .close-me").off("click").on("click",function(){
					$("body").css({'overflow-y':'initial'});
					$("#confirmCreateRecord").removeClass("showed");
				});
			});

			uiSaveReceivingNumber.off("click.saveReceivingNum").on("click.saveReceivingNum", function(){
				var iGetReceivingNumber = $("#setReceivingNumber").val(),
					iSetNumber = parseInt(iGetReceivingNumber);

				cr8v_platform.localStorage.set_local_storage({
					name : "currentReceivingNumber",
					data : {value:iSetNumber}
				});
			});



			if(sAction === 'AddNewReceivingConsigneeGoods')
			{
				_documentEvents(true);

				var uiaddSaveNewConsigneeGoodsBtn = $(".addSaveNewConsigneeGoods"),
					sAddSelectModeDelivery  = $("#addSelectModeDelivery"),
					sAddInputPoNumber = $("#addInputPoNumber"),
					sAddInputVesselName = $("#addInputVesselName"),
					sAddInputVesselHatches = $("#addInputVesselHatches"),
					sAddInputVesselType = $("#addInputVesselType"),
					sAddInputVesselCaptain = $("#addInputVesselCaptain"),
					sAddInputVesselDischargeType = $("#addInputVesselDischargeType"),
					sAddInputVesselLandingVolume = $("#addInputVesselLandingVolume"),
					iBillLadingVolume = $("#bill-lading-volume-measure"),
					sAddInputVesselBerthingDate = $("#addInputVesselBerthingDate"),
					sAddInputVesselTransferTime = $("#addInputVesselTransferTime"),
					sAddInputTruckOrigin = $("#addInputTruckOrigin"),
					saddInputTruckings = $("#addInputTruckings"),
					sAddInputTruckPlateNumber = $("#addInputTruckPlateNumber"),
					sAddInputTruckDriverName = $("#addInputTruckDriverName"),
					saddInputTruckLicenseNum= $("#addInputTruckLicenseNum"),
					oCollectModeOfDelivery = {},
					iSetReceivingNumber = $("#setReceivingNumber").val();
					

				//This is for validation 
				$("#addReceivingConsigneeGoodsForm").cr8vformvalidation({
					'preventDefault' : true,
				    'data_validation' : 'datavalid',
				    'input_label' : 'labelinput',
				    'onValidationError': function(arrMessages) {
				// 	alert(JSON.stringify(arrMessages)); //shows the errors
				       
								console.log(JSON.stringify(arrMessages));
						
				  //       var options = {title : "Add Recieving Consignee Goods Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger",};
						// $("body").feedback(options);
			        },
				    'onValidationSuccess': function() {

				    	$(".modal-close.close-me").trigger("click");

				    	$("body").css({overflow:'hidden'});
						
						$("#SaveconfirmCreateRecord").addClass("showed");

						$("#SaveconfirmCreateRecord .close-me").on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$("#SaveconfirmCreateRecord").removeClass("showed");
						});

						var uiDisplayReceiveConsigneeGoodsId = $("#displayReceiveConsigneeGoodsId");
							uiDisplayReceiveConsigneeGoodsId.text("Receiving No. "+iSetReceivingNumber+" has been created.");


						// FOR ADDING PRODUCTS

						var uiProductContainer = $("#displayReceivingConsigneeProductSelects .product_selected_receive_consign"),
							oGetAllData = {},
							iCountAll = 0;


							$.each(uiProductContainer, function(){
								var uiThis = $(this),
									iProductId = uiThis.data("ProdId"),
									iQtyUnitMeasure = uiThis.find(".unit-of-measure").val(),
									oGetCurrentReceivingConsigneeSelected = cr8v_platform.localStorage.get_local_storage({name:"getAllReceivingConsigneeProdutcts"}),
									sVendorName = $("#addReceiveConsigneeVendor"+iProductId).val().trim().toLowerCase(),
									sGetBags = "",
									uiCheckBulkBag = '';


									// FOR ADDING CONSIGNEE
									var uiConsigneeContainer = $("#addConsigneeItem"+iProductId).find(".addedReceivedConsigneeItem"),
										oConsignData = {},
										iCountConsign = 0;

										uiCheckBulkBag = uiThis.find("input[name='bag-or-bulk"+iProductId+"']:checked").val();

										$.each(uiConsigneeContainer, function(){
											// console.log(iCountConsign);
											// iCountConsign++;
											// console.log(iProductId);
											var uiConsignThis = $(this),
												oGetCurrentReceivingConsigneeItems = cr8v_platform.localStorage.get_local_storage({name:"CurrentReceivingConsigneeItems"}),
												uiSelectConsignAddQty = uiConsignThis.find(".selectConsignAddQty").val(),
												iAddConsigneeQuantity = uiConsignThis.find(".addConsigneeQuantity").val(),
												sAddConsigneeMeasurement = uiConsignThis.find(".addConsigneeMeasurement").val(),
												iAddConsigneeBagQty = uiConsignThis.find(".addConsigneeBagQty").val();

												// console.log()

												if(oGetCurrentReceivingConsigneeItems === null)
												{
													oConsignData = {
														product_id : iProductId,
														consignee_id : uiSelectConsignAddQty,
														distribution_qty : iAddConsigneeQuantity,
														unit_of_measure_id : iQtyUnitMeasure,
														bag : iAddConsigneeBagQty
														//loading_method: uiCheckBulkBag

													}
													oStoredReceiveConsigneeItems.push(oConsignData);

													cr8v_platform.localStorage.set_local_storage({
														name: "CurrentReceivingConsigneeItems",
														data: oStoredReceiveConsigneeItems
													});
													oStoredReceiveConsigneeItems = [];
												}else{
													oStoredReceiveConsigneeItems = [];
													var iCount = 0,
														oGetCurrentReceivingConsigneeItemsAll = cr8v_platform.localStorage.get_local_storage({name:"CurrentReceivingConsigneeItems"});

													for(var i in oGetCurrentReceivingConsigneeItemsAll)
													{
														oConsignData = {
															product_id : oGetCurrentReceivingConsigneeItemsAll[i]["product_id"],
											    			consignee_id : oGetCurrentReceivingConsigneeItemsAll[i]["consignee_id"],
											    			distribution_qty : oGetCurrentReceivingConsigneeItemsAll[i]["distribution_qty"],
											    			unit_of_measure_id :  oGetCurrentReceivingConsigneeItemsAll[i]["unit_of_measure_id"],
											    			bag :  oGetCurrentReceivingConsigneeItemsAll[i]["bag"]
											    			//loading_method:  oGetCurrentReceivingConsigneeItemsAll[i]["loading_method"]
											
											    		}
											    		oStoredReceiveConsigneeItems.push(oConsignData);

											    		if(
											    			uiSelectConsignAddQty == oGetCurrentReceivingConsigneeItemsAll[i]["consignee_id"] &&
											    			iProductId ==  oGetCurrentReceivingConsigneeItemsAll[i]["product_id"]
											    			)
														{
															iCount++;
														}
														
													}


													if(iCount == 0)
													{
														var oConsignDataNew = {
															product_id : iProductId,
															consignee_id : uiSelectConsignAddQty,
															distribution_qty : iAddConsigneeQuantity,
															unit_of_measure_id : iQtyUnitMeasure,
															bag : iAddConsigneeBagQty
															//loading_method: uiCheckBulkBag
														}
														oStoredReceiveConsigneeItems.push(oConsignDataNew);

													}
													
													
													cr8v_platform.localStorage.set_local_storage({
														name: "CurrentReceivingConsigneeItems",
														data: oStoredReceiveConsigneeItems
													});

													
												}

												// var oGetCurrentReceivingConsigneeItemsAll = cr8v_platform.localStorage.get_local_storage({name:"CurrentReceivingConsigneeItems"});

										});






									oStoredReceiveConsigneeProduct = [];

									($("#qtyReceiveg"+iProductId).val() != "") ? sGetBags = $("#qtyReceiveg"+iProductId).val() : sGetBags = "";



									if(oGetCurrentReceivingConsigneeSelected === null)
									{
										oGetAllData = {
											product_id:iProductId,
											vendor : sVendorName,
											loading_method :  uiCheckBulkBag,
											unit_of_measure :  $("#unit-measurement"+iProductId).val(),
											qty_to_receive :  $("#qtyReceiveKg"+iProductId).val(),
											bag :  sGetBags
										}
										oStoredReceiveConsigneeProduct.push(oGetAllData);
									}else{
										for(var i in oGetCurrentReceivingConsigneeSelected)
										{
											oGetAllData = {
								    			product_id:oGetCurrentReceivingConsigneeSelected[i]["product_id"],
								    			vendor : oGetCurrentReceivingConsigneeSelected[i]["vendor"],
								    			loading_method :  oGetCurrentReceivingConsigneeSelected[i]["loading_method"],
								    			unit_of_measure :  oGetCurrentReceivingConsigneeSelected[i]["unit_of_measure"],
								    			qty_to_receive :  oGetCurrentReceivingConsigneeSelected[i]["qty_to_receive"],
								    			bag :  oGetCurrentReceivingConsigneeSelected[i]["bag"],
								    		}
								    		oStoredReceiveConsigneeProduct.push(oGetAllData);
										}
										
										if(iProductId != oGetCurrentReceivingConsigneeSelected[i]["product_id"])
										{
											var oGetAllDataNew = {
												product_id:iProductId,
												vendor : sVendorName,
												loading_method :  uiCheckBulkBag,
												unit_of_measure :  $("#unit-measurement"+iProductId).val(),
												qty_to_receive :  $("#qtyReceiveKg"+iProductId).val(),
												bag :  sGetBags
											}
											oStoredReceiveConsigneeProduct.push(oGetAllDataNew);
										}
										
										
									}

									cr8v_platform.localStorage.set_local_storage({
										name: "getAllReceivingConsigneeProdutcts",
										data: oStoredReceiveConsigneeProduct
									});
							

			
							});
					
							oStoredReceiveConsigneeProduct = [];

						var uiCreateLinkToGo = $("#create-link-to-go"),
							sModeDeliveryCheck = sAddSelectModeDelivery.val();

							if(sModeDeliveryCheck == "vessel")
							{
								uiCreateLinkToGo.attr("href", BASEURL+"receiving/consignee_ongoing_vessel");
							}else if(sModeDeliveryCheck == "truck")
							{
								uiCreateLinkToGo.attr("href", BASEURL+"receiving/consignee_ongoing_truck");
							}

						_add();


						
			        }
				});


				uiaddSaveNewConsigneeGoodsBtn.off('click.addSaveNewConsigneeGoods').on('click.addSaveNewConsigneeGoods', function(e){
					e.preventDefault();

					var sModeDelivery = '',
						oError = [],
						uiPurchaseNumber =$("#addInputPoNumber"),
						uiVendorName = $(".validate-vendor"),
						uiSelectProductListSelect = $("#selectProductListSelect"),
						uiQtyReceived = $(".qty-received"),
						uiaddConsigneeQuantity = $(".addConsigneeQuantity");

					// Start Validation for consignee
						var uiContainerProducts = $("#displayReceivingConsigneeProductSelects").find(".product_selected_receive_consign"),
							iCountProd = 0;

							if(uiContainerProducts.length == 0)
							{
								oError.push("Add Product Item First");
							}

						$.each(uiContainerProducts, function(){
							var uiThisProd = $(this),
								uiQtyReceived = uiThisProd.find(".qty-received"),
								uiQtyBags = uiThisProd.find(".qty-bags"),
								uiSkuProdName = uiThisProd.find(".font-16.font-bold.f-left"), 
								iQtyReceived = parseFloat(uiQtyReceived.val()),
								iQtyBags = parseFloat(uiQtyBags.val()),
								uiAddedReceivedConsigneeItem = uiThisProd.find(".addedReceivedConsigneeItem"),
								uiBagSet = uiThisProd.find(".bag_set"),
								uiQtyBags = uiThisProd.find(".qty-bags"),
								oGatherConsigneeSelected = [],
								oGatherConsigneeQty = [],
								oGatherConsigneeBags = [];

								
								$.each(uiAddedReceivedConsigneeItem, function(){
									uiThisGetConsignee = $(this),
									uiGetConsigneeItem = uiThisGetConsignee.find(".addConsigneeQuantity"),
									uiGetConsigneeBags = uiThisGetConsignee.find(".addConsigneeBagQty"),
									uiGetConsigneeName = uiThisGetConsignee.find(".selectConsignAddQty"),
									iGetConsigneeName = uiGetConsigneeName.val(),				
									iGetConsigneeItem = parseFloat(uiGetConsigneeItem.val()),
									iGetConsigneeBags = parseFloat(uiGetConsigneeBags.val());

									oGatherConsigneeQty.push(iGetConsigneeItem);
									oGatherConsigneeBags.push(iGetConsigneeBags);

									oGatherConsigneeSelected.push(iGetConsigneeName);
									
								});

								var total = 0,
									iBagsTotal = 0;

								$.each(oGatherConsigneeQty,function() {
								    total += parseInt(this);
								});

								
								$.each(oGatherConsigneeBags,function() {
								    iBagsTotal += parseInt(this);
								});


								//check if consignee name is already used
								 var myConsigneeNameArr = oGatherConsigneeSelected.sort(),
								 	iCountConsignee = 0;

								for (var i = 0; i < myConsigneeNameArr.length; i++) {
									if (myConsigneeNameArr.indexOf(myConsigneeNameArr[i]) == myConsigneeNameArr.lastIndexOf(myConsigneeNameArr[i])) { 

									}else{
										iCountConsignee++;
									}
								} 

								if(iCountConsignee != 0)
								{
									oError.push("Consignee Distribution Name must be unique in "+uiSkuProdName.text());
								}

								if(uiBagSet.is(":checked"))
								{
									if(uiQtyBags.val().trim() == "" || uiQtyBags.val().trim() == 0 || uiQtyBags.val().trim() == "0")
									{
										oError.push("Bags value must not empty to "+uiSkuProdName.text());
									}
								}
				

								
								
								if(iQtyReceived != total){
									oError.push("Consignee Distribution Quantity not equal to "+uiSkuProdName.text());
									
								}


								if(uiQtyBags.val() != "")
								{
									
									if(iQtyBags != iBagsTotal)
									{
										oError.push("Consignee Distribution Bags not equal to "+uiSkuProdName.text());
									}
								}else if(uiQtyBags.val() == "" && isNaN( Number( iBagsTotal ) ) === false)
								{
									oError.push("Consignee Distribution Bags not equal to "+uiSkuProdName.text());
								}



								
						});


					// End





					

					sModeDelivery = sAddSelectModeDelivery.val();


					// 	(uiPurchaseNumber.val().trim() == "") ? oError.push("Purchase Order is Required") : "";
					(sAddSelectModeDelivery.val().trim() == "") ? oError.push("Mode Of Delivery is Required") : "";
					(uiVendorName.val() == "") ? oError.push("Product Vendor is Required") : "";
					(uiSelectProductListSelect.val() == "") ? oError.push("Please Select Product") : "";
					(uiQtyReceived.val() == "") ? oError.push("Quantity to Receive is Required") : "";
					(uiaddConsigneeQuantity.val() == "") ? oError.push("Consignee Quantity to Receive is Required") : "";

					if(sModeDelivery == "vessel")
					{
						sAddInputVesselName.attr("datavalid", "required");
						sAddInputVesselHatches.attr("datavalid", "required");
						sAddInputVesselType.attr("datavalid", "required");
						sAddInputVesselCaptain.attr("datavalid", "required");
						sAddInputVesselDischargeType.attr("datavalid", "required");
						sAddInputVesselLandingVolume.attr("datavalid", "required");
						//sAddInputVesselBerthingDate.attr("datavalid", "required");
						//sAddInputVesselTransferTime.attr("datavalid", "required");

						sAddInputTruckOrigin.attr("datavalid", false);
						saddInputTruckings.attr("datavalid", false);
						sAddInputTruckPlateNumber.attr("datavalid", false);
						sAddInputTruckDriverName.attr("datavalid", false);
						saddInputTruckLicenseNum.attr("datavalid", false);

						oCollectModeOfDelivery = {};

						oCollectModeOfDelivery = {
							modeType : sModeDelivery,
							vesselName : sAddInputVesselName.val().trim(),
							hatches : sAddInputVesselHatches.val().trim(),
							vesselType : sAddInputVesselType.val().trim(),
							vesselCaptain : sAddInputVesselCaptain.val().trim(),
							dischargeType : sAddInputVesselDischargeType.val().trim(),
							loadingVolume : sAddInputVesselLandingVolume.val().trim(),
							unit_bill_volume : iBillLadingVolume.val(),
							berthingDate : sAddInputVesselBerthingDate.val().trim(),
							transferTime : sAddInputVesselTransferTime.val().trim()
						}

						cr8v_platform.localStorage.set_local_storage({
							name: "modeOfDelivery",
							data : oCollectModeOfDelivery
						});


						
						(sAddInputVesselName.val().trim() == "") ?  oError.push("Vessel Name is Required") : "";
						(sAddInputVesselHatches.val().trim() == "") ?  oError.push("Vessel Hatches is Required") : "";
						(sAddInputVesselType.val().trim() == "") ?  oError.push("Vessel Type is Required") : "";
						(sAddInputVesselCaptain.val().trim() == "") ?  oError.push("Vessel Name is Required") : "";
						(sAddInputVesselDischargeType.val().trim() == "") ?  oError.push("Vessel Discharge Type is Required") : "";
						(sAddInputVesselLandingVolume.val().trim() == "") ?  oError.push("Vessel Bill of Landing Volume is Required") : "";
						// (sAddInputVesselBerthingDate.val().trim() == "") ?  oError.push("Vessel Berthing Date is Required") : "";
						// (sAddInputVesselTransferTime.val().trim() == "") ?  oError.push("Vessel Berthing Time is Required") : "";

					}
					else if(sModeDelivery == "truck")
					{
						sAddInputTruckOrigin.attr("datavalid", "required");
						saddInputTruckings.attr("datavalid", "required");
						sAddInputTruckPlateNumber.attr("datavalid", "required");
						sAddInputTruckDriverName.attr("datavalid", "required");
						saddInputTruckLicenseNum.attr("datavalid", "required");

						sAddInputVesselName.attr("datavalid", false);
						sAddInputVesselHatches.attr("datavalid", false);
						sAddInputVesselType.attr("datavalid", false);
						sAddInputVesselCaptain.attr("datavalid", false);
						sAddInputVesselDischargeType.attr("datavalid", false);
						sAddInputVesselLandingVolume.attr("datavalid", false);
						//sAddInputVesselBerthingDate.attr("datavalid", false);
						//sAddInputVesselTransferTime.attr("datavalid", false);

						oCollectModeOfDelivery = {};

						oCollectModeOfDelivery = {
							modeType : sModeDelivery,
							vesselOrigin : sAddInputTruckOrigin.val().trim(),
							trucking : saddInputTruckings.val().trim(),
							plateNumber : sAddInputTruckPlateNumber.val().trim(),
							driverName : sAddInputTruckDriverName.val().trim(),
							licenseNum : saddInputTruckLicenseNum.val().trim()
						}

						cr8v_platform.localStorage.set_local_storage({
							name: "modeOfDelivery",
							data : oCollectModeOfDelivery
						});

						
						(sAddInputTruckOrigin.val().trim() == "") ?  oError.push("Vessel Origin is Required") : "";
						(saddInputTruckings.val().trim() == "") ? oError.push("Trucking is Required") : "";
						(sAddInputTruckPlateNumber.val().trim() == "") ?  oError.push("Truck Plate Number is Required") : "";
						(sAddInputTruckDriverName.val().trim() == "") ?  oError.push("Truck Driver Name is Required") : "";
						(saddInputTruckLicenseNum.val().trim() == "") ?  oError.push("License Number is Required") : "";

					}



					if(oError.length !== 0)
					{

						$("body").feedback({title : "Adding Recieving Consignee Goods", message : oError.join("<br />"), type : "danger", icon : failIcon});

					}else{
						$("#addReceivingConsigneeGoodsForm").submit();
					}


					

				});
			
				setTimeout(function(){
					$('#generated-data-view').removeClass('showed');
				});
				
			}
			else if(sAction === 'AddNewReceivingConsigneeGoodsNotes')
			{
				_getCurrentDateTime();

				var uiAddRecievingConsigneeNotesSaveBtn = $("#addRecievingConsigneeNotesSave"),
					sAddReceivingConsigneeNoteSubject = $("#AddReceivingConsigneeNoteSubject"),
					sAddReceivingConsigneeNoteMessage = $("#AddReceivingConsigneeNoteMessage"),
					oGetSetRecievingConsigneeNotes = cr8v_platform.localStorage.get_local_storage({name:"setReceiveConsigneeAddNoteStored"}),
					oSetOdata = {},
					oGatheredData =[],
					iCount = 1,
					sDateGet = "",
					sDateFormatGet = "";

					var setCurrentInterval = setInterval(function(){
						var oGetDateTime = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDateTime"});
						if(oGetDateTime !== null)
						{
							sDateGet = oGetDateTime.date;
							sDateFormatGet = oGetDateTime.dateFormat;
							clearInterval(setCurrentInterval);
						}
					}, 300);
		
					sAddReceivingConsigneeNoteSubject.val("");
					sAddReceivingConsigneeNoteMessage.val("");
					
				//This is for validation 
					$("#addReceivingConsigneeNoteForm").cr8vformvalidation({
						'preventDefault' : true,
					    'data_validation' : 'datavalid',
					    'input_label' : 'labelinput',
					    'onValidationError': function(arrMessages) {
					        // alert(JSON.stringify(arrMessages)); //shows the errors
					        var options = {title : "Add Notes Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger"};
							$("body").feedback(options);
				        },
					    'onValidationSuccess': function() {

					    	if(oGetSetRecievingConsigneeNotes === null)
					    	{
					    		oSetOdata = {
					    			id : iCount,
					    			userId: iConstUserId,
					    			subject : sAddReceivingConsigneeNoteSubject.val().trim(),
						    		message : sAddReceivingConsigneeNoteMessage.val().trim(),
						    		datetime : sDateGet,
						    		displayDate : sDateFormatGet
						    	}

						    	oGatheredData.push(oSetOdata);
						    	
					    	}
					    	else
					    	{
					    		for(var x in oGetSetRecievingConsigneeNotes)
					    		{
					    			oSetOdata = {
					    				id : iCount,
					    				userId: iConstUserId,
							    		subject : oGetSetRecievingConsigneeNotes[x]["subject"],
							    		message : oGetSetRecievingConsigneeNotes[x]["message"],
							    		datetime : oGetSetRecievingConsigneeNotes[x]["datetime"],
							    		displayDate :oGetSetRecievingConsigneeNotes[x]["displayDate"]
							    	}
							    	oGatheredData.push(oSetOdata);
							    	iCount++;
					    		}

					    		oSetOdata = {
					    			id : iCount,
					    			userId: iConstUserId,
					    			subject : sAddReceivingConsigneeNoteSubject.val().trim(),
						    		message : sAddReceivingConsigneeNoteMessage.val().trim(),
						    		datetime : sDateGet,
						    		displayDate : sDateFormatGet
						    	}

						    	oGatheredData.push(oSetOdata);

					    	}

					    	
					    	
					    	cr8v_platform.localStorage.set_local_storage({
						    		name : "setReceiveConsigneeAddNoteStored",
						    		data : oGatheredData
						    	});

					    	$(".modal-close.close-me").trigger("click");

					    	CCReceivingConsigneeGoods.renderElement('displayReceivingConsigneeNotes', oGatheredData);

					    	
							var options = {title : "Add Notes", message : "Successfully Added", speed : "slow", withShadow : true,  type : "success"};
							$("body").feedback(options);
				        }
					});

				uiAddRecievingConsigneeNotesSaveBtn.off("click.AddRecievingConsigneeNotesSave").on("click.AddRecievingConsigneeNotesSave", function(){
					
					$("#addReceivingConsigneeNoteForm").submit();
					

				});

				
			}
			else if(sAction === 'AddNewItemProdReceivingConsigneeGoods')
			{
				// console.log("okkk");

				_itemsEvent();

				var uiaddItemProductConsigneeBtn = $("#addItemProductConsigneeBtn"),
					oGetSelectedReceiveConsigneeProduct = cr8v_platform.localStorage.get_local_storage({name:"getAllProductsReceiveConsignee"}),
					oGetCurrentSelectedReceivedConsigneeProduct = cr8v_platform.localStorage.get_local_storage({name:"SetSelectedProductForReceivingConsignee"}),
				 	uiGetDataBtn = $(".setProductListSelect").find(".frm-custom-dropdown").find(".frm-custom-dropdown-option").find(".option"),
				 	uiTempSelected = $('#selectProductListSelect option:selected'),
				 	sGetAttr = "",
				 	oGatherDataFromProducts = {};

// 				 	console.log();
					

					sGetAttr = uiTempSelected.val();


					uiGetDataBtn.off('click.uiOptions').on('click.uiOptions', function(e){
						e.preventDefault();
						sGetAttr = $(this).attr("data-value");
					});
					


				uiaddItemProductConsigneeBtn.off('click.addItemProductConsigneeBtn').on('click.addItemProductConsigneeBtn', function(e){
					e.preventDefault();
					
					var oSetGatherDataFromProducts = {},
						oTempStoreData = [];

					// console.log(sGetAttr);
					if(sGetAttr != "")
					{
					
						for(var x in oGetSelectedReceiveConsigneeProduct)
						{

							if(oGetSelectedReceiveConsigneeProduct[x]["id"] == sGetAttr)
							{

								if(oGetCurrentSelectedReceivedConsigneeProduct === null)
								{

									oSetGatherDataFromProducts = {
										id : sGetAttr,
										name : oGetSelectedReceiveConsigneeProduct[x]["name"],
										sku : oGetSelectedReceiveConsigneeProduct[x]["sku"],
										product_category : oGetSelectedReceiveConsigneeProduct[x]["product_category"],
										description : oGetSelectedReceiveConsigneeProduct[x]["description"],
										image : oGetSelectedReceiveConsigneeProduct[x]["image"],
										product_shelf_life : oGetSelectedReceiveConsigneeProduct[x]["product_shelf_life"],
										threshold_min : oGetSelectedReceiveConsigneeProduct[x]["threshold_min"],
										threshold_maximum : oGetSelectedReceiveConsigneeProduct[x]["threshold_maximum"],
										unit_price : oGetSelectedReceiveConsigneeProduct[x]["unit_price"],
										is_deleted : oGetSelectedReceiveConsigneeProduct[x]["is_deleted"]
									}

									oStoredDataFromProducts.push(oSetGatherDataFromProducts);
									// console.log(oStoredDataFromProducts);
								}
								else
								{


									for(var x in oGetCurrentSelectedReceivedConsigneeProduct)
									{
										oGatherDataFromProducts = {
											id : oGetCurrentSelectedReceivedConsigneeProduct[x]["id"],
											name : oGetCurrentSelectedReceivedConsigneeProduct[x]["name"],
											sku : oGetCurrentSelectedReceivedConsigneeProduct[x]["sku"],
											product_category : oGetCurrentSelectedReceivedConsigneeProduct[x]["product_category"],
											description : oGetCurrentSelectedReceivedConsigneeProduct[x]["description"],
											image : oGetCurrentSelectedReceivedConsigneeProduct[x]["image"],
											product_shelf_life : oGetCurrentSelectedReceivedConsigneeProduct[x]["product_shelf_life"],
											threshold_min : oGetCurrentSelectedReceivedConsigneeProduct[x]["threshold_min"],
											threshold_maximum : oGetCurrentSelectedReceivedConsigneeProduct[x]["threshold_maximum"],
											unit_price : oGetCurrentSelectedReceivedConsigneeProduct[x]["unit_price"],
											is_deleted : oGetCurrentSelectedReceivedConsigneeProduct[x]["is_deleted"]
										}
										oStoredDataFromProducts.push(oGatherDataFromProducts);
									}

									oSetGatherDataFromProducts = {
										id : sGetAttr,
										name : oGetSelectedReceiveConsigneeProduct[x]["name"],
										sku : oGetSelectedReceiveConsigneeProduct[x]["sku"],
										product_category : oGetSelectedReceiveConsigneeProduct[x]["product_category"],
										description : oGetSelectedReceiveConsigneeProduct[x]["description"],
										image : oGetSelectedReceiveConsigneeProduct[x]["image"],
										product_shelf_life : oGetSelectedReceiveConsigneeProduct[x]["product_shelf_life"],
										threshold_min : oGetSelectedReceiveConsigneeProduct[x]["threshold_min"],
										threshold_maximum : oGetSelectedReceiveConsigneeProduct[x]["threshold_maximum"],
										unit_price : oGetSelectedReceiveConsigneeProduct[x]["unit_price"],
										is_deleted : oGetSelectedReceiveConsigneeProduct[x]["is_deleted"]
									}
									oStoredDataFromProducts.push(oSetGatherDataFromProducts);

								}




								cr8v_platform.localStorage.set_local_storage({
									name : "SetSelectedProductForReceivingConsignee",
									data : oStoredDataFromProducts
								});



								// console.log(JSON.stringify(oStoredDataFromProducts));
							}

						}

						oTempStoreData.push(oSetGatherDataFromProducts);

						CCReceivingConsigneeGoods.renderElement("displaySelectedProductForReceivingConsignee", oTempStoreData);
						// oStoredDataFromProducts = [];
					}else{
						$("body").feedback({title : "Add Item", message : "Please Add Item First", type : "danger", icon : failIcon});
					}


				});

				
			}
			else if(sAction === 'getAllProductsSeletedForReceiveConsignee')
			{
				
				var uiDisplayReceivingConsigneeProductSelectsGet = $("#displayReceivingConsigneeProductSelects"),
					oReceiveConsigneeData = cr8v_platform.localStorage.get_local_storage({name:"SetSelectedProductForReceivingConsignee"}),
					oGetAllConsigneeReceiveConsignee = cr8v_platform.localStorage.get_local_storage({name:"getAllConsigneeReceiveConsignee"}),
					oNewConsignees = [],
					uiDeleteProdBtn = $(".deleteProd"),
					uiConfirmDeleteProd = $("#confirmDelete"),
					uiSelectConsigneeListSelect = ui.find(".selectConsigneeListSelect"),
					uiParentDropdown = uiSelectConsigneeListSelect.parent(),
					uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown'),
					uiConsigneeDistContainsdsd = ui.closest(".addConsigneeItemAddNumber").find(".addedReceivedConsigneeItem").length,
					uiConsigneeDistContain = ui.closest(".addConsigneeItemAddNumber").find(".addedReceivedConsigneeItem"),
					btnAddMore = ui.closest(".product_selected_receive_consign").find(".addConsigneeRow");

					uiSiblingDropdown.remove();
					uiSelectConsigneeListSelect.removeClass("frm-custom-dropdown-origin");

					if(uiConsigneeDistContainsdsd == 0)
					{
						var sHtml = '<option value="">Consignee Name</option>';

						for(var x in oConsigneeNamesCollects)
						{
							sHtml = '<option value="'+oConsigneeNamesCollects[x]['id']+'">'+oConsigneeNamesCollects[x]['name']+'</option>';
							uiSelectConsigneeListSelect.append(sHtml);
						}

						uiSelectConsigneeListSelect.transformDD();


					}else{


						for(var x in oConsigneeNamesCollects)
						{
							oNewConsignees.push(oConsigneeNamesCollects[x]);
						}

						$.each(uiConsigneeDistContain, function(){
							var uiMainThis = $(this),
								uiSelects = uiMainThis.find(".selectConsigneeListSelect option:selected");

								$.each(oNewConsignees, function(i, el){
									if (this.id == uiSelects.val()){
									   oNewConsignees.splice(i, 1);
									}
								});
						});

						if(Object.keys(oNewConsignees).length == 0)
						{
							btnAddMore.hide();
							ui.remove();
						}else{
							var sHtml = '<option value="">Consignee Name</option>';

							for(var x in oNewConsignees)
							{
								sHtml = '<option value="'+oNewConsignees[x]['id']+'">'+oNewConsignees[x]['name']+'</option>';
								uiSelectConsigneeListSelect.append(sHtml);
							}

							uiSelectConsigneeListSelect.transformDD();

						}

						

						// console.log(JSON.stringify(oNewConsignees));

					}

					


					uiDeleteProdBtn.off("click.deleteProd").on("click.deleteProd", function(){
						var uiParent = $(this).closest(".product_selected_receive_consign"),
							iProdId = uiParent.data("ProdId");
						
						$("body").css({overflow:'hidden'});

						$("#trashModal").addClass("showed");

						$("#trashModal .close-me").on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$("#trashModal").removeClass("showed");
						});


						uiConfirmDeleteProd.off("click.deleteNow").on("click.deleteNow", function(){
							var oRemoveData = oReceiveConsigneeData;
							// console.log(JSON.stringify(oRemoveData));
								uiParent.remove();
								

								$.each(oRemoveData, function(i, el){
								    if (this.id == iProdId){
								        oRemoveData.splice(i, 1);
								    }
								});

								cr8v_platform.localStorage.set_local_storage({
									name : "SetSelectedProductForReceivingConsignee",
									data : oRemoveData
								});
								oStoredDataFromProducts = [];

								if(oRemoveData.length > 0){
									// 	CCReceivingConsigneeGoods.renderElement("displaySelectedProductForReceivingConsignee", oRemoveData);
								}else{ 
									uiDisplayReceivingConsigneeProductSelectsGet.html(""); 

									cr8v_platform.localStorage.delete_local_storage({name : "SetSelectedProductForReceivingConsignee"});
								}

								

								$(".modal-close.close-me").trigger("click");

								var options = {
								  form : "box",
								  autoShow : true,
								  type : "success",
								  title  : "Delete Product",
								  message : "Successfully Deleted",
								  speed : "slow",
								  withShadow : true
								}
								
								$("body").feedback(options);
						});

					});

					var uiBulkSet = $(".bulk_set"),
						uiBagSet = $(".bag_set"),
						btnAddConsigneeRow = $(".addConsigneeRow"),
						oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
						uiUnitOfMeasure = ui.find(".unit-of-measure");

						for(var x in oUnitOfMeasures)
						{
							if(oUnitOfMeasures[x]['name'] == "kg" || oUnitOfMeasures[x]['name'] == "mt" || oUnitOfMeasures[x]['name'] == "L" )
							{	
								if(oUnitOfMeasures[x]['name'] == "mt"){
									sHtmlMeasure = '<option value="'+oUnitOfMeasures[x]['id']+'" selected>'+oUnitOfMeasures[x]['name']+'</option>';
								 	ui.find(".unit-of-measure").append(sHtmlMeasure);
								}else{
									sHtmlMeasure = '<option value="'+oUnitOfMeasures[x]['id']+'">'+oUnitOfMeasures[x]['name']+'</option>';
								 	ui.find(".unit-of-measure").append(sHtmlMeasure);
								}	
								
							}

						}

						ui.find(".unit-of-measure").show().css({
							"padding" : "3px 4px",
							"width" :  "50px"
						});

						_triggerUnitMeasure(ui);


						uiUnitOfMeasure.off("change.selectMeasurement").on("change.selectMeasurement", function(){
							var uiParent = $(this).closest(".product_selected_receive_consign"),
								iProdId = uiParent.data("ProdId"),
								sGetMeasurementValue = $(this).val(),
								sTriggerLocation = uiParent.find(".unit-of-measure"),
								sTriggerRemoveSelected = uiParent.find(".unit-of-measure").find("option[value!='"+sGetMeasurementValue+"']"),
								sTriggerMeasermentValue = uiParent.find(".unit-of-measure").find("option[value='"+sGetMeasurementValue+"']");
								
								sTriggerRemoveSelected.attr("selected", false);
								sTriggerMeasermentValue.attr("selected", true);

								sTriggerLocation.load(location.href+" "+sTriggerLocation);


						
						});



					uiBulkSet.off("click.bulk").on("click.bulk", function(){
						var uiParent = $(this).closest(".product_selected_receive_consign"),
							iProdId = uiParent.data("ProdId"),
							uiQtyReceiveg = $("#qtyReceiveg"+iProdId),
							uiqtyAddReceivedConsignee = $(".qtyAddReceivedConsignee"+iProdId);

							if($(this).is(":checked"))
							{
								uiQtyReceiveg.val("");
								uiQtyReceiveg.attr("disabled", true);
								uiqtyAddReceivedConsignee.attr("disabled", true);
							}
					});

					uiBagSet.off("click.bag").on("click.bag", function(){
						var uiParent = $(this).closest(".product_selected_receive_consign"),
							iProdId = uiParent.data("ProdId"),
							uiQtyReceiveg = $("#qtyReceiveg"+iProdId),
							uiqtyAddReceivedConsignee = $(".qtyAddReceivedConsignee"+iProdId);

							if($(this).is(":checked"))
							{
								uiQtyReceiveg.attr("disabled", false);
								uiqtyAddReceivedConsignee.attr("disabled", false);
							}
					});


					// FOR ADDING CONSIGNEE NUMBER

					// END

					btnAddConsigneeRow.off("click.AddConsigneeRow").on("click.AddConsigneeRow", function(e){
						e.preventDefault();
						var uiParent = $(this).closest(".product_selected_receive_consign"),
							iProdId = uiParent.data("ProdId"),
							uiAddReceivedConsigneeItemsContainer = $("#addConsigneeItem"+iProdId),
							uiAddConsigneeNumberContainer = uiParent.find("#addConsigneeItem"+iProdId).find(".addedReceivedConsigneeItem"),
							iCount = 1;

							$.each(uiAddConsigneeNumberContainer, function(){ iCount++; });

							if(iCount == 1)
							{
								var newCountRow = iCount+1
								cr8v_platform.localStorage.set_local_storage({
									name : "setNewConsigneeRow",
									data : {value:newCountRow}
								});
							}
							else{
								var newCountRow = iCount+1
								cr8v_platform.localStorage.set_local_storage({
									name : "setNewConsigneeRow",
									data : {value:iCount}
								});
							}



							CCReceivingConsigneeGoods.renderElement("AddReceivedConsigneeItems", iProdId);

					});



				// DELETE PRODUCT CONSIGNEE
				var uiProductConsigneeContainer = $("#displayReceivingConsigneeProductSelects .product_selected_receive_consign");

				$.each(uiProductConsigneeContainer, function(){
					var uiConsigneeThis = $(this),
						iGetProdId = uiConsigneeThis.data("ProdId"),
						iCount = 0,
						uiConsigneeInsideProductContainer = $("#addConsigneeItem"+iGetProdId+" .addedReceivedConsigneeItem");

						$.each(uiConsigneeInsideProductContainer, function(){
							var uConsignInsideProdThis = $(this);


								uiDeleteConsigneBtn  = uConsignInsideProdThis.find(".deleteProductConsignee");

								uiDeleteConsigneBtn.off("click.deleteConsignProd").on("click.deleteConsignProd", function(){
									

										uConsignInsideProdThis.remove();									

									for(var x in oConsigneeNamesCollects)
									{
										oNewConsignees.push(oConsigneeNamesCollects[x]);
									}


									var uiMainConsignee = uiConsigneeThis.find(".addedReceivedConsigneeItem")
										iCountDelete = 0;

									$.each(uiMainConsignee, function(){
										var iRowsThis = $(this),
											inumbers = iRowsThis.find('.add-consignee-numbers');

											iCountDelete++;

											inumbers.html(iCountDelete);
									});

									
							
								});
						});

						var uiMainConsignee = uiConsigneeThis.find(".addedReceivedConsigneeItem")
							iCountDelete = 0;

						$.each(uiMainConsignee, function(){
							var iRowsThis = $(this),
								inumbers = iRowsThis.find('.add-consignee-numbers');

								iCountDelete++;

								inumbers.html(iCountDelete);

						});



						
				});


					

			}

		}
		else if(sCurrentPath.indexOf("consignee_receiving") > -1)
		{
			if(sAction === 'setConsigneeGoodsId')
			{
				var uiSetConsigneeReceiveIdBtn = $(".setConsigneeReceiveId"),
					btnShowModalComplete = $(".show-modal-complete");

				 uiSetConsigneeReceiveIdBtn.off("click.consigneeBtn").on("click.consigneeBtn", function(){
				 	uiParent = $(this).closest(".displayReceiveConsignRow"),
				 	iReceivedId = uiParent.data("receivingId");

				 	cr8v_platform.localStorage.set_local_storage({
				 		name : "setCurrentDisplayReceivedId",
				 		data : {value:iReceivedId}
				 	});
				 });

				 btnShowModalComplete.off("click.consigneeBtnID").on("click.consigneeBtnID", function(){
				 	uiParent = $(this).closest(".displayReceiveConsignRow"),
				 	iReceivedId = uiParent.data("receivingId");

				 	cr8v_platform.localStorage.set_local_storage({
				 		name : "setCurrentDisplayReceivedId",
				 		data : {value:iReceivedId}
				 	});

				 });

			}
		}
		else if(sCurrentPath.indexOf("consignee_ongoing_vessel") > -1)
		{
			

			if(sAction === 'addNotesSave'){

				_documentEvents();

				var uiSaveReceivingNotesBtn = $("#add-notes-save"),
					sAddNoteSubject = $("#addNoteSubject"),
					sAddNoteMessage = $("#addNoteMessage"),
					iGetReceivedID = ui["receiving_id"],
					sDateGet = "",
					sDateFormatGet = "";
					

					var currentDateInterval = setInterval(function(){
						oGetDateTime = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDateTime"});
						
						if(oGetDateTime !== null)
						{
							sDateGet = oGetDateTime.date;
							sDateFormatGet = oGetDateTime.dateFormat;
						}
						clearInterval(currentDateInterval);
						
					}, 300);


					//This is for validation 
					$("#addReceivingNoteForm").cr8vformvalidation({
						'preventDefault' : true,
					    'data_validation' : 'datavalid',
					    'input_label' : 'labelinput',
					    'onValidationError': function(arrMessages) {
					        // alert(JSON.stringify(arrMessages)); //shows the errors
					        var options = {title : "Add Recieving Notes Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger",};
							$("body").feedback(options);
				        },
					    'onValidationSuccess': function() {

					    	var setData = [{
					    		"subject":sAddNoteSubject.val(),
					    		"message":sAddNoteMessage.val(),
					    		"receiving_id":iGetReceivedID,
					    		"datetime":sDateGet
					    	}];
					    	// console.log(JSON.stringify(setData));

					    	cr8v_platform.localStorage.set_local_storage({
					    		name : "addReceivingNotesSave",
					    		data : setData
					    	});

					    	$(".modal-close.close-me").trigger("click");

					    	_addNotes();
							
				        }
					});

					uiSaveReceivingNotesBtn.off("click.addNotesValidate").on("click.addNotesValidate", function(){
						$("#addReceivingNoteForm").submit();
						// console.log("ojsss");
					});

			}
			else if(sAction === 'validateProductQuantity')
			{
				// console.log("Ok ba to");
				var uiCheckProductQuantityBtn = $(".check-product-quantity");

				uiCheckProductQuantityBtn.off("click.validate").on("click.validate", function(){
					$(this).addClass("confirmValidationProduct");
						_addBatchForProduct();
						

					$(".confirmValidationProduct").off("click.confirmProduct").on("click.confirmProduct", function(){
						// console.log("ok naman pala");
						// $("#validateProdcutQuantityForm").submit();
						_validateBatchAndQuantityCheck();
					});

				});





				// _validateProductQuantityCheck();
				
			}
			else if(sAction === 'triggerLoadingMethod')
			{
				var btnTriggerBulkRadio = $(".trigger-bulk-radio"),
					btnTriggerBagRadio = $(".trigger-bag-radio");

				btnTriggerBulkRadio.off("click.radio").on("click.radio", function(){
					var uiThis = $(this),
						uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
						uiSetQtyToBag = uiParent.find(".set-qty-to-bag"),
						uiDisplayConsigneeBag = uiParent.find(".display-consignee-bag");

						uiSetQtyToBag.attr("disabled", true).val("");
						uiDisplayConsigneeBag.attr("disabled", true).val("");

				});

				btnTriggerBagRadio.off("click.radio").on("click.radio", function(){
					var uiThis = $(this),
						uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
						uiSetQtyToBag = uiParent.find(".set-qty-to-bag"),
						uiDisplayConsigneeBag = uiParent.find(".display-consignee-bag");


						uiSetQtyToBag.attr("disabled", false).val("");
						uiDisplayConsigneeBag.attr("disabled", false).val("");

				});
			}
			else if(sAction === 'triggerUnitOfMeasure')
			{
				var uiUnitOfMeasure = $(".unit-of-measure"),
					oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
					sMeasurementName = "",
					sStoreLastMeasurement = "";

					

				uiUnitOfMeasure.off("focus.measuress").on("focus.measuress", function(){
					var uiFocusThis = $(this).find("option:selected").text().toLowerCase();

					//console.log(uiFocusThis);

					sStoreLastMeasurement = uiFocusThis;

				}).off("change.measure").on("change.measure", function(){
					var uiThis = $(this),
						uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
						uiQuantityPerItem = uiParent.find(".set-qty-to-bulk"),
						sSelectedmeasurement = uiThis.find("option:selected").text().toLowerCase(),
						uiSetMeasurementName = uiParent.find(".set-measurement-name"),
						iResult = 0;

						for(var x in oUnitOfMeasures)
						{
							if(uiThis.val() == oUnitOfMeasures[x]["id"])
							{
								sMeasurementName = oUnitOfMeasures[x]["name"];
							}
						}
						
						console.log(sStoreLastMeasurement);
						console.log(sSelectedmeasurement);
						console.log(iResult);

						iResult = UnitConverter.convert(sStoreLastMeasurement, sSelectedmeasurement, uiQuantityPerItem.val().trim());

						uiQuantityPerItem.val(iResult);


						uiSetMeasurementName.html(sMeasurementName.toUpperCase());

						sStoreLastMeasurement = sSelectedmeasurement;

						uiThis.blur();


						
				});

			}

			
		}
		else if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1)
		{
			if(sAction === 'addNotesSave'){

				_documentEvents();

				var uiSaveReceivingNotesBtn = $("#add-notes-save"),
					sAddNoteSubject = $("#addNoteSubject"),
					sAddNoteMessage = $("#addNoteMessage"),
					iGetReceivedID = ui["receiving_id"],
					sDateGet = "",
					sDateFormatGet = "";

					var currentDateInterval = setInterval(function(){
						oGetDateTime = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDateTime"});
						
						if(oGetDateTime !== null)
						{
							sDateGet = oGetDateTime.date;
							sDateFormatGet = oGetDateTime.dateFormat;
						}
						clearInterval(currentDateInterval);
						
					}, 300);


					//This is for validation 
					$("#addReceivingNoteForm").cr8vformvalidation({
						'preventDefault' : true,
					    'data_validation' : 'datavalid',
					    'input_label' : 'labelinput',
					    'onValidationError': function(arrMessages) {
					        // alert(JSON.stringify(arrMessages)); //shows the errors
					        var options = {title : "Add Recieving Notes Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger",};
							$("body").feedback(options);
				        },
					    'onValidationSuccess': function() {

					    	var setData = [{
					    		"subject":sAddNoteSubject.val(),
					    		"message":sAddNoteMessage.val(),
					    		"receiving_id":iGetReceivedID,
					    		"datetime":sDateGet
					    	}];
					    	// console.log(JSON.stringify(setData));

					    	cr8v_platform.localStorage.set_local_storage({
					    		name : "addReceivingNotesSave",
					    		data : setData
					    	});

					    	$(".modal-close.close-me").trigger("click");

					    	_addNotes();
							
				        }
					});

					uiSaveReceivingNotesBtn.off("click.addNotesValidate").on("click.addNotesValidate", function(){
						$("#addReceivingNoteForm").submit();
					});

			}
			else if(sAction === 'validateProductQuantity')
			{
				// console.log("Ok ba to");
				var uiCheckProductQuantityBtn = $(".check-product-quantity");

				uiCheckProductQuantityBtn.off("click.validate").on("click.validate", function(){
					$(this).addClass("confirmValidationProduct");
						_addBatchForProduct();
						

					$(".confirmValidationProduct").off("click.confirmProduct").on("click.confirmProduct", function(){
						// console.log("ok naman pala");
						// $("#validateProdcutQuantityForm").submit();
						_validateBatchAndQuantityCheck();
					});

				});





				// _validateProductQuantityCheck();
				
			}
			else if(sAction === 'triggerLoadingMethod')
			{
				var btnTriggerBulkRadio = $(".trigger-bulk-radio"),
					btnTriggerBagRadio = $(".trigger-bag-radio");

				btnTriggerBulkRadio.off("click.radio").on("click.radio", function(){
					var uiThis = $(this),
						uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
						uiSetQtyToBag = uiParent.find(".set-qty-to-bag"),
						uiDisplayConsigneeBag = uiParent.find(".display-consignee-bag");

						uiSetQtyToBag.attr("disabled", true).val("");
						uiDisplayConsigneeBag.attr("disabled", true).val("");

				});

				btnTriggerBagRadio.off("click.radio").on("click.radio", function(){
					var uiThis = $(this),
						uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
						uiSetQtyToBag = uiParent.find(".set-qty-to-bag"),
						uiDisplayConsigneeBag = uiParent.find(".display-consignee-bag");


						uiSetQtyToBag.attr("disabled", false).val("");
						uiDisplayConsigneeBag.attr("disabled", false).val("");

				});
			}
				else if(sAction === 'triggerUnitOfMeasure')
			{
				var uiUnitOfMeasure = $(".unit-of-measure"),
					oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
					sMeasurementName = "",
					sStoreLastMeasurement = "";

					

				uiUnitOfMeasure.off("focus.measuress").on("focus.measuress", function(){
					var uiFocusThis = $(this).find("option:selected").text().toLowerCase();

					//console.log(uiFocusThis);

					sStoreLastMeasurement = uiFocusThis;

				}).off("change.measure").on("change.measure", function(){
					var uiThis = $(this),
						uiParent = uiThis.closest(".border-top.border-blue.box-shadow-dark"),
						uiQuantityPerItem = uiParent.find(".set-qty-to-bulk"),
						sSelectedmeasurement = uiThis.find("option:selected").text().toLowerCase(),
						uiSetMeasurementName = uiParent.find(".set-measurement-name"),
						iResult = 0;

						for(var x in oUnitOfMeasures)
						{
							if(uiThis.val() == oUnitOfMeasures[x]["id"])
							{
								sMeasurementName = oUnitOfMeasures[x]["name"];
							}
						}
						
						//console.log(sStoreLastMeasurement);
						//console.log(sSelectedmeasurement);
						//console.log(iResult);

						iResult = UnitConverter.convert(sStoreLastMeasurement, sSelectedmeasurement, uiQuantityPerItem.val().trim());

						uiQuantityPerItem.val(iResult);


						uiSetMeasurementName.html(sMeasurementName.toUpperCase());

						sStoreLastMeasurement = sSelectedmeasurement;

						uiThis.blur();


						
				});

			}
			else if(sAction === 'showReceivingConsigneeComplete')
			{
				var btnShowModal = $("#show-receiving-truck-complete"),
					uiSetTareIn = $("#set-tare-in"),
					uiSetTareOut = $("#set-tare-out"),
					uiSetNetWeight = $("#set-net-weight"),
					uiTriggerComputeNetWeight = $(".trigger-compute-net-weight"),
					btnCompleteReceivingRecordTruck = $("#complete-receiving-record-truck"),
					iTotal = 0;

					uiSetTareIn.number(true, 0);
					uiSetTareOut.number(true, 0);

				btnShowModal.off("click.showModal").on("click.showModal", function(e){
					e.preventDefault();

					$("body").css({overflow:'hidden'});
						
					$("#show-complete-receiving-for-truck").addClass("showed");

					$("#show-complete-receiving-for-truck .close-me").on("click",function(){
						$("body").css({'overflow-y':'initial'});
						$("#show-complete-receiving-for-truck").removeClass("showed");
					});
				});


					uiTriggerComputeNetWeight.off("keyup.compute").on("keyup.compute", function(){
						var iGetTareIn = parseFloat(uiSetTareIn.val()),
							iGetTareOut = parseFloat(uiSetTareOut.val());

							if(uiSetTareIn.val().trim() != "" && uiSetTareOut.val().trim() != "")
							{
								uiSetNetWeight.html("");

								iTotal = iGetTareIn - iGetTareOut;
								uiSetNetWeight.html($.number(iTotal, 2)+" MT");
							}	
					});

					btnCompleteReceivingRecordTruck.off("click.confirmed").on("click.confirmed", function(e){
						e.preventDefault();
						if(uiSetTareIn.val().trim() != "" && uiSetTareOut.val().trim() != "")
						{
							$("body").feedback({title : "Complete Receiving Record", message : "Successfully Complete", type : "success"});

							var oData = {
								"tare_in" : parseFloat(uiSetTareIn.val().trim()),
								"tare_out" : parseFloat(uiSetTareOut.val().trim()),
								"net_weight" : $.number(iTotal, 2)
							}


							_saveConsigneeOngoingTruck(oData);
						}else{
							$("body").feedback({title : "Complete Receiving Record", message : "Please fill up Truck Tare In and Tare Out", type : "danger", icon : failIcon});
						}

					});
				

				

				



			}
		}
		else if(sCurrentPath.indexOf("consignee_edit_vessel") > -1)
		{
			if(sAction === 'EditNewReceivingConsigneeGoodsValid')
			{
				

				var uiEditSaveNewConsigneeGoodsBtn = $("#editSaveNewConsigneeGoods"),
					uiDisplayEditConsigneeVesselName = $("#display-edit-vessel-name"),
					uiDisplayEditConsigneeVesselHatches = $("#display-edit-vessel-hatches"),
					uiDisplayEditConsigneeVesselType = $("#display-edit-vessel-type"),
					uiDisplayEditConsigneeVesselCaptain = $("#display-edit-vessel-captain"),
					uiDisplayEditConsigneeVesselDischargeType = $("#display-edit-vessel-discharge-type"),
					uiDisplayEditConsigneeVesselBillOfLanding = $("#display-edit-vessel-bill-of-landing"),
					uiDisplayEditConsigneeVesselBerthingDate = $("#display-edit-vessel-berthing-date"),
					uiDisplayEditConsigneeVesselBerthingTime = $("#display-edit-vessel-berthing-time");
					

				//This is for validation 
				$("#EditReceivingConsigneeGoodsForm").cr8vformvalidation({
					'preventDefault' : true,
				    'data_validation' : 'datavalid',
				    'input_label' : 'labelinput',
				    'onValidationError': function(arrMessages) {
				        // alert(JSON.stringify(arrMessages)); //shows the errors
				        var options = {title : "Edit Recieving Consignee Goods Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger",};
						$("body").feedback(options);
			        },
				    'onValidationSuccess': function() {



						// FOR ADDING PRODUCTS

						var uiProductContainer = $("#displayEditReceivingConsigneeProductSelects .product_selected_receive_consign"),
							oGetAllData = {},
							iCountAll = 0;


							$.each(uiProductContainer, function(){
								var uiThis = $(this),
									iProductId = uiThis.data("ProdId"),
									oGetCurrentReceivingConsigneeSelected = cr8v_platform.localStorage.get_local_storage({name:"getAllReceivingConsigneeProdutcts"}),
									sVendorName = $("#addReceiveConsigneeVendor"+iProductId).val().trim().toLowerCase(),
									uiQuantityMeasurement = uiThis.find(".unit-of-measure"),
									sGetBags = "",
									uiCheckBulkBag = '';


									// FOR ADDING CONSIGNEE
									var uiConsigneeContainer = $("#addConsigneeItem"+iProductId+" .addedReceivedConsigneeItem"),
										oConsignData = {},
										iCountConsign = 0;

										uiCheckBulkBag = uiThis.find("input[name='bag-or-bulk"+iProductId+"']:checked").val();

										$.each(uiConsigneeContainer, function(){
											var uiConsignThis = $(this),
												oGetCurrentReceivingConsigneeItems = cr8v_platform.localStorage.get_local_storage({name:"CurrentReceivingConsigneeItems"}),
												uiSelectConsignAddQty = uiConsignThis.find(".selectConsignAddQty").val(),
												iAddConsigneeQuantity = uiConsignThis.find(".addConsigneeQuantity").val(),
												sAddConsigneeMeasurement = uiConsignThis.find(".addConsigneeMeasurement").val(),
												iAddConsigneeBagQty = uiConsignThis.find(".addConsigneeBagQty").val();

												// console.log(uiSelectConsignAddQty);
												// console.log(iAddConsigneeQuantity);
												// console.log(sAddConsigneeMeasurement);
												// console.log(iAddConsigneeBagQty);

												if(oGetCurrentReceivingConsigneeItems === null)
												{
													oConsignData = {
														product_id : iProductId,
														consignee_id : uiSelectConsignAddQty,
														distribution_qty : iAddConsigneeQuantity,
														unit_of_measure_id : uiQuantityMeasurement.val(),
														bag : iAddConsigneeBagQty,
														loading_method: uiCheckBulkBag

													}
													oStoredReceiveConsigneeItems.push(oConsignData);

													cr8v_platform.localStorage.set_local_storage({
														name: "CurrentReceivingConsigneeItems",
														data: oStoredReceiveConsigneeItems
													});
													oStoredReceiveConsigneeItems = [];
												}else{
													var iCount = 0,
														oGetCurrentReceivingConsigneeItemsAll = cr8v_platform.localStorage.get_local_storage({name:"CurrentReceivingConsigneeItems"});

													for(var i in oGetCurrentReceivingConsigneeItemsAll)
													{
														oConsignData = {
															product_id : oGetCurrentReceivingConsigneeItemsAll[i]["product_id"],
											    			consignee_id : oGetCurrentReceivingConsigneeItemsAll[i]["consignee_id"],
											    			distribution_qty : oGetCurrentReceivingConsigneeItemsAll[i]["distribution_qty"],
											    			unit_of_measure_id :  oGetCurrentReceivingConsigneeItemsAll[i]["unit_of_measure_id"],
											    			bag :  oGetCurrentReceivingConsigneeItemsAll[i]["bag"],
											    			loading_method:  oGetCurrentReceivingConsigneeItemsAll[i]["loading_method"]
											
											    		}
											    		oStoredReceiveConsigneeItems.push(oConsignData);

											    		if(uiSelectConsignAddQty == oGetCurrentReceivingConsigneeItems[i]["consignee_id"])
														{
															iCount++;
														}
														
													}

													if(iCount == 0)
													{
														var oConsignDataNew = {
															product_id : iProductId,
															consignee_id : uiSelectConsignAddQty,
															distribution_qty : iAddConsigneeQuantity,
															unit_of_measure_id : uiQuantityMeasurement.val(),
															bag : iAddConsigneeBagQty,
															loading_method: uiCheckBulkBag
														}
														oStoredReceiveConsigneeItems.push(oConsignDataNew);

													}
													
													cr8v_platform.localStorage.set_local_storage({
														name: "CurrentReceivingConsigneeItems",
														data: oStoredReceiveConsigneeItems
													});

													oStoredReceiveConsigneeItems = [];
													
												}

												//console.log("icountto labas");
												
													

												
											// oStoredReceiveConsigneeItems = [];
											

										});
										
										oStoredReceiveConsigneeItems = [];



										var oGetCurrentReceivingConsigneeItemsAll = cr8v_platform.localStorage.get_local_storage({name:"CurrentReceivingConsigneeItems"}),
											getSelecteddataOld = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"}),
											iCounntOldConsign = 0,
											iCountTry = 0;
											oStoredReceiveConsigneeItems = [];
											for(var e in getSelecteddataOld["product_info"])
											{
												if(getSelecteddataOld["product_info"][e]["consignee_info"] !== null)
												{	
													iCounntOldConsign++;
												}	
											}

											if(iCounntOldConsign != 0)
											{
												var icountto = 0;
												oStoredReceiveConsigneeItems = [];
												
												for(var w in getSelecteddataOld["product_info"])
												{
													
													if(getSelecteddataOld["product_info"][w]["consignee_info"] !== null)
													{
														
														for(var s in getSelecteddataOld["product_info"][w]["consignee_info"])
														{
															// console.log(iCountTry);
															iCountTry++;
															// console.log(JSON.stringify(getSelecteddataOld["product_info"][w]["consignee_info"]));

															var oConsignData = {
																product_id : getSelecteddataOld["product_info"][w]["product_id"],
												    			consignee_id : getSelecteddataOld["product_info"][w]["consignee_info"][s]["consignee_id"],
												    			distribution_qty : getSelecteddataOld["product_info"][w]["consignee_info"][s]["distribution_qty"],
												    			unit_of_measure_id :  getSelecteddataOld["product_info"][w]["consignee_info"][s]["unit_of_measure_id"],
												    			bag :  getSelecteddataOld["product_info"][w]["consignee_info"][s]["bag"],
												    			loading_method:  getSelecteddataOld["product_info"][w]["loading_method"]
												
												    		}
												    		oStoredReceiveConsigneeItems.push(oConsignData);

														}
													}
													// console.log(JSON.stringify(oStoredReceiveConsigneeItems));
													console.log("icountto");
													icountto++;

													
												}

												for(var q in oGetCurrentReceivingConsigneeItemsAll)
												{
													var oConsignData = {
														product_id : oGetCurrentReceivingConsigneeItemsAll[q]["product_id"],
										    			consignee_id : oGetCurrentReceivingConsigneeItemsAll[q]["consignee_id"],
										    			distribution_qty : oGetCurrentReceivingConsigneeItemsAll[q]["distribution_qty"],
										    			unit_of_measure_id :  oGetCurrentReceivingConsigneeItemsAll[q]["unit_of_measure_id"],
										    			bag :  oGetCurrentReceivingConsigneeItemsAll[q]["bag"],
										    			loading_method:  oGetCurrentReceivingConsigneeItemsAll[q]["loading_method"]
										
										    		}
										    		oStoredReceiveConsigneeItems.push(oConsignData);
												}

												cr8v_platform.localStorage.set_local_storage({
													name: "CurrentReceivingConsigneeItems",
													data: oStoredReceiveConsigneeItems
												});



											}
											oStoredReceiveConsigneeItems = [];



									oStoredReceiveConsigneeProduct = [];

									($("#qtyReceiveg"+iProductId).val() != "") ? sGetBags = $("#qtyReceiveg"+iProductId).val() : sGetBags = "";



									if(oGetCurrentReceivingConsigneeSelected === null)
									{
										oGetAllData = {
											product_id:iProductId,
											vendor : sVendorName,
											loading_method :  uiCheckBulkBag,
											unit_of_measure_id :  $("#unit-measurement"+iProductId).val(),
											qty_to_receive :  $("#qtyReceiveKg"+iProductId).val(),
											bag :  sGetBags
										}
										oStoredReceiveConsigneeProduct.push(oGetAllData);
									}else{
										for(var i in oGetCurrentReceivingConsigneeSelected)
										{
											oGetAllData = {
								    			product_id:oGetCurrentReceivingConsigneeSelected[i]["product_id"],
								    			vendor : oGetCurrentReceivingConsigneeSelected[i]["vendor"],
								    			loading_method :  oGetCurrentReceivingConsigneeSelected[i]["loading_method"],
								    			unit_of_measure_id :  oGetCurrentReceivingConsigneeSelected[i]["unit_of_measure_id"],
								    			qty_to_receive :  oGetCurrentReceivingConsigneeSelected[i]["qty_to_receive"],
								    			bag :  oGetCurrentReceivingConsigneeSelected[i]["bag"],
								    		}
								    		oStoredReceiveConsigneeProduct.push(oGetAllData);
										}
										
										if(iProductId != oGetCurrentReceivingConsigneeSelected[i]["product_id"])
										{
											var oGetAllDataNew = {
												product_id:iProductId,
												vendor : sVendorName,
												loading_method :  uiCheckBulkBag,
												unit_of_measure_id :  $("#unit-measurement"+iProductId).val(),
												qty_to_receive :  $("#qtyReceiveKg"+iProductId).val(),
												bag :  sGetBags
											}
											oStoredReceiveConsigneeProduct.push(oGetAllDataNew);
										}
										
										
									}

									cr8v_platform.localStorage.set_local_storage({
										name: "getAllReceivingConsigneeProdutcts",
										data: oStoredReceiveConsigneeProduct
									});
							
							});
					
							oStoredReceiveConsigneeProduct = [];


						// Merge Old Product and new Product
						var oReceiveConsigneeData = cr8v_platform.localStorage.get_local_storage({name:"getAllReceivingConsigneeProdutcts"}),
							getSelecteddataOld = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"});
							// console.log(JSON.stringify(getSelecteddataOld));
							
								if(getSelecteddataOld["product_info"] === null && oReceiveConsigneeData !== null)
								{
									cr8v_platform.localStorage.set_local_storage({
										name : "getAllReceivingConsigneeProdutcts",
										data : oReceiveConsigneeData
									});
									console.log("first");
								}
								else if(getSelecteddataOld["product_info"] !== null && oReceiveConsigneeData === null)
								{
									var oCreateData = {};

									for(var x in getSelecteddataOld["product_info"])
									{
										oCreateData = {
											product_id : getSelecteddataOld["product_info"][x]["product_id"],
											vendor : getSelecteddataOld["product_info"][x]["vendor_name"],
											loading_method : getSelecteddataOld["product_info"][x]["loading_method"],
											unit_of_measure_id : getSelecteddataOld["product_info"][x]["unit_of_measure_id"],
											qty_to_receive : getSelecteddataOld["product_info"][x]["qty"],
											bag : getSelecteddataOld["product_info"][x]["bag"]
										}

										oCompeleteEditProduct.push(oCreateData);

									}

										cr8v_platform.localStorage.set_local_storage({
											name : "getAllReceivingConsigneeProdutcts",
											data : oCompeleteEditProduct
										});
										console.log("second");
								}
								else if(getSelecteddataOld["product_info"] !== null && oReceiveConsigneeData !== null)
								{
									for(var x in getSelecteddataOld["product_info"])
									{
										oCreateData = {
											product_id : getSelecteddataOld["product_info"][x]["product_id"],
											vendor : getSelecteddataOld["product_info"][x]["vendor_name"],
											loading_method : getSelecteddataOld["product_info"][x]["loading_method"],
											unit_of_measure_id : getSelecteddataOld["product_info"][x]["unit_of_measure_id"],
											qty_to_receive : getSelecteddataOld["product_info"][x]["qty"],
											bag : getSelecteddataOld["product_info"][x]["bag"]
										}

										oCompeleteEditProduct.push(oCreateData);

									}

									for(var i in oReceiveConsigneeData)
									{
										var oCreateDataNew = {
											product_id :oReceiveConsigneeData[i]["product_id"],
											vendor : oReceiveConsigneeData[i]["vendor"],
											loading_method : oReceiveConsigneeData[i]["loading_method"],
											unit_of_measure_id : oReceiveConsigneeData[i]["unit_of_measure_id"],
											qty_to_receive : oReceiveConsigneeData[i]["qty_to_receive"],
											bag : oReceiveConsigneeData[i]["bag"]
										}
										oCompeleteEditProduct.push(oCreateDataNew);
									}

									// console.log("thirdd");

									cr8v_platform.localStorage.set_local_storage({
										name : "getAllReceivingConsigneeProdutcts",
										data : oCompeleteEditProduct
									});
								}
							

							var checkProducts = cr8v_platform.localStorage.get_local_storage({name:"getAllReceivingConsigneeProdutcts"});

							// console.log(console.log(checkProducts));

						_update();


						
			        }
				});


				uiEditSaveNewConsigneeGoodsBtn.off('click.editSaveNewConsigneeGood').on('click.editSaveNewConsigneeGood', function(e){
					e.preventDefault();
					var oError = [];

					var uiDisplayEditConsigneeVesselPoNum = $("#display-edit-consignee-vessel-poNum"),
						uiDisplayEditConsigneeVesselName = $("#display-edit-vessel-name"),
						uiDisplayEditConsigneeVesselHatches = $("#display-edit-vessel-hatches"),
						uiDisplayEditConsigneeVesselType = $("#display-edit-vessel-type"),
						uiDisplayEditConsigneeVesselCaptain = $("#display-edit-vessel-captain"),
						uiDisplayEditConsigneeVesselDischargeType = $("#display-edit-vessel-discharge-type"),
						uiDisplayEditConsigneeVesselBillOfLanding = $("#display-edit-vessel-bill-of-landing"),
						uiDisplayEditConsigneeVesselBerthingDate = $("#display-edit-vessel-berthing-date"),
						uiDisplayEditConsigneeVesselBerthingTime = $("#display-edit-vessel-berthing-time"), 
						uiReceiveConsigneeVendorItem = $(".receiveConsigneeVendorItem"),
						uiConsigneeItem = $(".addConsigneeQuantity"),
						iBillLadingVolume = $("#bill-lading-volume-measure"),
						uiQtyReceived = $(".qty-received");





					// Start Validation for consignee
						var uiContainerProducts = $("#displayEditReceivingConsigneeProductSelects").find(".product_selected_receive_consign"),
							iCountProd = 0;

							$.each(uiContainerProducts, function(){
								var uiThisProd = $(this),
									uiQtyReceived = uiThisProd.find(".qty-received"),
									uiQtyBags = uiThisProd.find(".qty-bags"),
									uiSkuProdName = uiThisProd.find(".font-16.font-bold.f-left"), 
									iQtyReceived = parseFloat(uiQtyReceived.val()),
									iQtyBags = parseFloat(uiQtyBags.val()),
									uiAddedReceivedConsigneeItem = uiThisProd.find(".addedReceivedConsigneeItem"),
									oGatherConsigneeSelected = [],
									oGatherConsigneeQty = [],
									oGatherConsigneeBags = [];

									$.each(uiAddedReceivedConsigneeItem, function(){
										uiThisGetConsignee = $(this),
										uiGetConsigneeItem = uiThisGetConsignee.find(".addConsigneeQuantity"),
										uiGetConsigneeBags = uiThisGetConsignee.find(".addConsigneeBagQty"),					
										iGetConsigneeItem = parseFloat(uiGetConsigneeItem.val()),
										iGetConsigneeBags = parseFloat(uiGetConsigneeBags.val()),
										uiGetConsigneeName = uiThisGetConsignee.find(".selectConsignAddQty"),
										iGetConsigneeName = uiGetConsigneeName.val();

										oGatherConsigneeQty.push(iGetConsigneeItem);
										oGatherConsigneeBags.push(iGetConsigneeBags);

										oGatherConsigneeSelected.push(iGetConsigneeName);
										
									});

									var total = 0,
										iBagsTotal = 0;

									$.each(oGatherConsigneeQty,function() {
									    total += parseInt(this);
									});

									
									$.each(oGatherConsigneeBags,function() {
									    iBagsTotal += parseInt(this);
									});


									//check if consignee name is already used
									 var myConsigneeNameArr = oGatherConsigneeSelected.sort(),
										iCountConsignee = 0;

									for (var i = 0; i < myConsigneeNameArr.length; i++) {
										if (myConsigneeNameArr.indexOf(myConsigneeNameArr[i]) == myConsigneeNameArr.lastIndexOf(myConsigneeNameArr[i])) { 

										}else{
											iCountConsignee++;
										}
									} 

									if(iCountConsignee != 0)
									{
										oError.push("Consignee Distribution Name must be unique in "+uiSkuProdName.text());
									}

									
									
									if(iQtyReceived != total){
										oError.push("Consignee Distribution Quantity not equal to "+uiSkuProdName.text());
										
									}


									if(uiQtyBags.val() != "")
									{
										
										if(iQtyBags != iBagsTotal)
										{
											oError.push("Consignee Distribution Bags not equal to "+uiSkuProdName.text());
										}
									}else if(uiQtyBags.val() == "" && isNaN( Number( iBagsTotal ) ) === false)
									{
										oError.push("Consignee Distribution Bags not equal to "+uiSkuProdName.text());
									}



									
							});


					// End

					//(uiDisplayEditConsigneeVesselPoNum.val().trim() == "") ? oError.push("Purchase Order is Required") : "";
					(uiQtyReceived.val() == "") ? oError.push("Quantity to Receive is Required") : "";
					(uiConsigneeItem.val() == "") ? oError.push("Consignee Quantity to Receive is Required") : "";
					(uiReceiveConsigneeVendorItem.val() == "") ? oError.push("Product Vendor is Required") : "";

					(uiDisplayEditConsigneeVesselName.val().trim() == "") ?  oError.push("Vessel Name is Required") : "";
					(uiDisplayEditConsigneeVesselHatches.val().trim() == "") ?  oError.push("Vessel Hatches is Required") : "";
					(uiDisplayEditConsigneeVesselType.val().trim() == "") ?  oError.push("Vessel Type is Required") : "";
					(uiDisplayEditConsigneeVesselCaptain.val().trim() == "") ?  oError.push("Vessel Name is Required") : "";
					(uiDisplayEditConsigneeVesselDischargeType.val().trim() == "") ?  oError.push("Vessel Discharge Type is Required") : "";
					(uiDisplayEditConsigneeVesselBillOfLanding.val().trim() == "") ?  oError.push("Vessel Bill of Landing Volume is Required") : "";
					//(uiDisplayEditConsigneeVesselBerthingDate.val().trim() == "") ?  oError.push("Vessel Berthing Date is Required") : "";
					//(uiDisplayEditConsigneeVesselBerthingTime.val().trim() == "") ?  oError.push("Vessel Berthing Time is Required") : "";

					


						oCollectModeOfDelivery = {
							modeType : "vessel",
							vesselName : uiDisplayEditConsigneeVesselName.val(),
							hatches : uiDisplayEditConsigneeVesselHatches.val().trim(),
							vesselType : uiDisplayEditConsigneeVesselType.val().trim(),
							vesselCaptain : uiDisplayEditConsigneeVesselCaptain.val().trim(),
							dischargeType : uiDisplayEditConsigneeVesselDischargeType.val().trim(),
							loadingVolume : uiDisplayEditConsigneeVesselBillOfLanding.val().trim(),
							berthingDate : uiDisplayEditConsigneeVesselBerthingDate.val().trim(),
							berthingMeasure : iBillLadingVolume.val(),
							transferTime : uiDisplayEditConsigneeVesselBerthingTime.val().trim()
						}

						console.log(uiDisplayEditConsigneeVesselBerthingDate.val().trim());

						cr8v_platform.localStorage.set_local_storage({
							name: "modeOfDelivery",
							data : oCollectModeOfDelivery
						});



						

					
					if(oError.length !== 0)
					{
						// var errorFOrModeDelivery = cr8v_platform.localStorage.get_local_storage({name:"ErrorFormodeOfDelivery"});
						// console.log(JSON.stringify(errorFOrModeDelivery))

						$("body").feedback({title : "Edit Recieving Consignee Goods", message : oError.join("<br />"), type : "danger", icon : failIcon});

					}else{
						$("#EditReceivingConsigneeGoodsForm").submit();
					}



				});


			}
			else if(sAction === 'EditNewItemProdReceivingConsigneeGoods')
			{
				// console.log("okkk");

				_itemsEvent();

				var uiaddItemProductConsigneeBtn = $("#addItemProductConsigneeBtn"),
					oGetSelectedReceiveConsigneeProduct = "",
					oGetCurrentSelectedReceivedConsigneeProduct = cr8v_platform.localStorage.get_local_storage({name:"SetSelectedProductForReceivingConsignee"}),
				 	uiGetEditDataBtn = $(".setProductListSelect").find(".frm-custom-dropdown").find(".frm-custom-dropdown-option").find(".option"),
				 	sGetAttr = "",
				 	oGatherDataFromProducts = {};

				
				var oSelectedInterval = setInterval(function(){
					oGetSelectedReceiveConsigneeProduct = cr8v_platform.localStorage.get_local_storage({name:"getAllProductsReceiveConsignee"});
					if(oGetSelectedReceiveConsigneeProduct !== null)
					{
						clearInterval(oSelectedInterval);
						

					}

				});

				uiaddItemProductConsigneeBtn.off('click.editItemProductConsigneeBtn').on('click.editItemProductConsigneeBtn', function(e){
							e.preventDefault();

							var oSetGatherDataFromProducts = {},
								oTempStoreData = [];

							sGetAttr = $("#selectProductListSelect").val();
							// console.log(sGetAttr);

							for(var x in oGetSelectedReceiveConsigneeProduct)
							{

								if(oGetSelectedReceiveConsigneeProduct[x]["id"] == sGetAttr)
								{

									if(oGetCurrentSelectedReceivedConsigneeProduct === null)
									{



										oSetGatherDataFromProducts = {
											id : sGetAttr,
											name : oGetSelectedReceiveConsigneeProduct[x]["name"],
											sku : oGetSelectedReceiveConsigneeProduct[x]["sku"],
											product_category : oGetSelectedReceiveConsigneeProduct[x]["product_category"],
											description : oGetSelectedReceiveConsigneeProduct[x]["description"],
											image : oGetSelectedReceiveConsigneeProduct[x]["image"],
											product_shelf_life : oGetSelectedReceiveConsigneeProduct[x]["product_shelf_life"],
											threshold_min : oGetSelectedReceiveConsigneeProduct[x]["threshold_min"],
											threshold_maximum : oGetSelectedReceiveConsigneeProduct[x]["threshold_maximum"],
											unit_price : oGetSelectedReceiveConsigneeProduct[x]["unit_price"],
											is_deleted : oGetSelectedReceiveConsigneeProduct[x]["is_deleted"]
										}

										oStoredDataFromProducts.push(oSetGatherDataFromProducts);
										// console.log(oStoredDataFromProducts);
									}
									else
									{


										for(var i in oGetCurrentSelectedReceivedConsigneeProduct)
										{
											oGatherDataFromProducts = {
												id : oGetCurrentSelectedReceivedConsigneeProduct[i]["id"],
												name : oGetCurrentSelectedReceivedConsigneeProduct[i]["name"],
												sku : oGetCurrentSelectedReceivedConsigneeProduct[i]["sku"],
												product_category : oGetCurrentSelectedReceivedConsigneeProduct[i]["product_category"],
												description : oGetCurrentSelectedReceivedConsigneeProduct[i]["description"],
												image : oGetCurrentSelectedReceivedConsigneeProduct[i]["image"],
												product_shelf_life : oGetCurrentSelectedReceivedConsigneeProduct[i]["product_shelf_life"],
												threshold_min : oGetCurrentSelectedReceivedConsigneeProduct[i]["threshold_min"],
												threshold_maximum : oGetCurrentSelectedReceivedConsigneeProduct[i]["threshold_maximum"],
												unit_price : oGetCurrentSelectedReceivedConsigneeProduct[i]["unit_price"],
												is_deleted : oGetCurrentSelectedReceivedConsigneeProduct[i]["is_deleted"]
											}
											oStoredDataFromProducts.push(oGatherDataFromProducts);
										}

										var oSetGatherDataFromProducts = {
											id : sGetAttr,
											name : oGetSelectedReceiveConsigneeProduct[x]["name"],
											sku : oGetSelectedReceiveConsigneeProduct[x]["sku"],
											product_category : oGetSelectedReceiveConsigneeProduct[x]["product_category"],
											description : oGetSelectedReceiveConsigneeProduct[x]["description"],
											image : oGetSelectedReceiveConsigneeProduct[x]["image"],
											product_shelf_life : oGetSelectedReceiveConsigneeProduct[x]["product_shelf_life"],
											threshold_min : oGetSelectedReceiveConsigneeProduct[x]["threshold_min"],
											threshold_maximum : oGetSelectedReceiveConsigneeProduct[x]["threshold_maximum"],
											unit_price : oGetSelectedReceiveConsigneeProduct[x]["unit_price"],
											is_deleted : oGetSelectedReceiveConsigneeProduct[x]["is_deleted"]
										}
										oStoredDataFromProducts.push(oSetGatherDataFromProducts);

									}




									cr8v_platform.localStorage.set_local_storage({
										name : "SetSelectedProductForReceivingConsignee",
										data : oStoredDataFromProducts
									});




									// console.log(JSON.stringify(oStoredDataFromProducts));
								}


							}

							oTempStoreData.push(oSetGatherDataFromProducts);
							CCReceivingConsigneeGoods.renderElement("displayEditSelectedProductForReceivingConsignee", oTempStoreData);
							
							//var oGetCurrentSelectedReceivedConsigneeProduct = cr8v_platform.localStorage.get_local_storage({name:"SetSelectedProductForReceivingConsignee"});
							// console.log(JSON.stringify(oGetCurrentSelectedReceivedConsigneeProduct))
							// oStoredDataFromProducts = [];


						});



				

				
			}
			else if(sAction === 'getEditAllProductsSeletedForReceiveConsignee')
			{

				
				var uiDisplayReceivingConsigneeProductSelectsGet = $("#displayEditReceivingConsigneeProductSelects"),
					oReceiveConsigneeData = cr8v_platform.localStorage.get_local_storage({name:"SetSelectedProductForReceivingConsignee"}),
					oGetAllConsigneeReceiveConsignee = cr8v_platform.localStorage.get_local_storage({name:"getAllConsigneeReceiveConsignee"}),
					oNewConsignees = [],
					uiDeleteProdBtn = $(".deleteProd"),
					uiConfirmDeleteProd = $("#confirmDelete"),
					uiSelectConsigneeListSelect = ui.find(".selectConsigneeListSelect"),
					uiParentDropdown = uiSelectConsigneeListSelect.parent(),
					uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown'),
					uiConsigneeDistContainsdsd = ui.closest(".addConsigneeItemAddNumber").find(".addedReceivedConsigneeItem").length,
					uiConsigneeDistContain = ui.closest(".addConsigneeItemAddNumber").find(".addedReceivedConsigneeItem"),
					btnAddMore = ui.closest(".product_selected_receive_consign").find(".addConsigneeRow");

					uiSiblingDropdown.remove();
					uiSelectConsigneeListSelect.removeClass("frm-custom-dropdown-origin");


					if(uiConsigneeDistContainsdsd == 0)
					{
						var sHtml = '<option value="">Consignee Name</option>';

						for(var x in oConsigneeNamesCollects)
						{
							sHtml = '<option value="'+oConsigneeNamesCollects[x]['id']+'">'+oConsigneeNamesCollects[x]['name']+'</option>';
							uiSelectConsigneeListSelect.append(sHtml);
						}

						uiSelectConsigneeListSelect.transformDD();

						CDropDownRetract.retractDropdown(uiSelectConsigneeListSelect);


					}else{


						for(var x in oConsigneeNamesCollects)
						{
							oNewConsignees.push(oConsigneeNamesCollects[x]);
						}

						$.each(uiConsigneeDistContain, function(){
							var uiMainThis = $(this),
								uiSelects = uiMainThis.find(".selectConsigneeListSelect option:selected");

								$.each(oNewConsignees, function(i, el){
									if (this.id == uiSelects.val()){
									   oNewConsignees.splice(i, 1);
									}
								});
						});

						if(Object.keys(oNewConsignees).length == 0)
						{
							btnAddMore.hide();
							ui.remove();
						}else{
							var sHtml = '<option value="">Consignee Name</option>';

							for(var x in oNewConsignees)
							{
								sHtml = '<option value="'+oNewConsignees[x]['id']+'">'+oNewConsignees[x]['name']+'</option>';
								uiSelectConsigneeListSelect.append(sHtml);
							}

							uiSelectConsigneeListSelect.transformDD();

							CDropDownRetract.retractDropdown(uiSelectConsigneeListSelect);

						}

						

						// console.log(JSON.stringify(oNewConsignees));

					}



					uiDeleteProdBtn.off("click.deleteProd").on("click.deleteProd", function(){
						var uiParent = $(this).closest(".product_selected_receive_consign"),
							iProdId = uiParent.data("ProdId");
						
						$("body").css({overflow:'hidden'});
						

						$("#trashModal").addClass("showed");

						$("#trashModal .close-me").on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$("#trashModal").removeClass("showed");
						});


						uiConfirmDeleteProd.off("click.deleteNow").on("click.deleteNow", function(){
							var oRemoveData = oReceiveConsigneeData;
							// console.log(JSON.stringify(oRemoveData));

								

								$.each(oRemoveData, function(i, el){
								    if (this.id == iProdId){
								        oRemoveData.splice(i, 1);
								    }
								});

								cr8v_platform.localStorage.set_local_storage({
									name : "SetSelectedProductForReceivingConsignee",
									data : oRemoveData
								});
								oStoredDataFromProducts = [];

								uiParent.remove();

								if(oRemoveData.length > 0){
									// CCReceivingConsigneeGoods.renderElement("displayEditSelectedProductForReceivingConsignee", oRemoveData);
								}else{ 
									uiDisplayReceivingConsigneeProductSelectsGet.html(""); 

									cr8v_platform.localStorage.delete_local_storage({name : "SetSelectedProductForReceivingConsignee"});
								}

								

								$(".modal-close.close-me").trigger("click");

								var options = {
								  form : "box",
								  autoShow : true,
								  type : "success",
								  title  : "Delete Product",
								  message : "Successfully Deleted",
								  speed : "slow",
								  withShadow : true
								}
								
								$("body").feedback(options);
						});

					});

					var uiBulkSet = $(".bulk_set"),
						uiBagSet = $(".bag_set"),
						btnAddConsigneeRow = $(".addConsigneeRow"),
						oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
						uiUnitOfMeasure = ui.find(".unit-of-measure");
						
						for(var x in oUnitOfMeasures)
						{
							if(oUnitOfMeasures[x]['name'] == "kg" || oUnitOfMeasures[x]['name'] == "mt" || oUnitOfMeasures[x]['name'] == "L" )
							{					
								sHtmlMeasure = '<option value="'+oUnitOfMeasures[x]['id']+'">'+oUnitOfMeasures[x]['name']+'</option>';
								 ui.find(".unit-of-measure").append(sHtmlMeasure);
							}

						}

						ui.find(".unit-of-measure").show().css({
							"padding" : "3px 4px",
							"width" :  "50px"
						});

						_triggerUnitMeasure(ui);


						uiUnitOfMeasure.off("change.selectMeasurement").on("change.selectMeasurement", function(){
							var uiParent = $(this).closest(".product_selected_receive_consign"),
								iProdId = uiParent.data("ProdId"),
								sGetMeasurementValue = $(this).val(),
								sTriggerMeasermentValue = $("#unit-measurement"+iProdId);

								 $("#unit-measurement"+iProdId+" option[value='"+sGetMeasurementValue+"']").attr("selected", "selected");


						
						});

					

// 					setTimeout(function(){
// 						ui.find(".bulk_set").bind("click", function(){

// 							var uiParent = $(this).closest(".product_selected_receive_consign"),
// 								iProdId = uiParent.data("ProdId"),
// 								uiQtyReceiveg = $("#qtyReceiveg"+iProdId),
// 								uiqtyAddReceivedConsignee = $(".qtyAddReceivedConsignee"+iProdId);

// 								if($(this).is(":checked"))
// 								{
// 									uiQtyReceiveg.val("");
// 									uiQtyReceiveg.attr("disabled", true);
// 									uiqtyAddReceivedConsignee.attr("disabled", true);
// 								}
// 						});
						
// 					}, 2000);

					uiBulkSet.off("click.bulk").on("click.bulk", function(){
						var uiParent = $(this).closest(".product_selected_receive_consign"),
							iProdId = uiParent.data("ProdId"),
							uiQtyReceiveg = $("#qtyReceiveg"+iProdId),
							uiqtyAddReceivedConsignee = $(".qtyAddReceivedConsignee"+iProdId);

							if($(this).is(":checked"))
							{
								uiQtyReceiveg.val("");
								uiQtyReceiveg.attr("disabled", true);
								uiqtyAddReceivedConsignee.attr("disabled", true);
							}
					});

					uiBulkSet.trigger("click.bulk");

					uiBagSet.off("click.bag").on("click.bag", function(){
						var uiParent = $(this).closest(".product_selected_receive_consign"),
							iProdId = uiParent.data("ProdId"),
							uiQtyReceiveg = $("#qtyReceiveg"+iProdId),
							uiqtyAddReceivedConsignee = $(".qtyAddReceivedConsignee"+iProdId);

							if($(this).is(":checked"))
							{
								uiQtyReceiveg.attr("disabled", false);
								uiqtyAddReceivedConsignee.attr("disabled", false);
							}
					});


					btnAddConsigneeRow.off("click.AddConsigneeRow").on("click.AddConsigneeRow", function(e){
						e.preventDefault();
						var uiParent = $(this).closest(".product_selected_receive_consign"),
							iProdId = uiParent.data("ProdId"),
							uiAddReceivedConsigneeItemsContainer = $("#addConsigneeItem"+iProdId),
							uiAddConsigneeNumberContainer = uiParent.find("#addConsigneeItem"+iProdId).find(".addedReceivedConsigneeItem"),
							iCount = 1;

							$.each(uiAddConsigneeNumberContainer, function(){ iCount++; });

							if(iCount == 1)
							{
								var newCountRow = iCount+1
								cr8v_platform.localStorage.set_local_storage({
									name : "setNewConsigneeRow",
									data : {value:newCountRow}
								});
							}
							else{
								var newCountRow = iCount+1
								cr8v_platform.localStorage.set_local_storage({
									name : "setNewConsigneeRow",
									data : {value:iCount}
								});
							}



							CCReceivingConsigneeGoods.renderElement("AddReceivedConsigneeItems", iProdId);

					});



				// DELETE PRODUCT CONSIGNEE
				var uiProductConsigneeContainer = $("#displayEditReceivingConsigneeProductSelects .product_selected_receive_consign");

				$.each(uiProductConsigneeContainer, function(){
					var uiConsigneeThis = $(this),
						iGetProdId = uiConsigneeThis.data("ProdId"),
						iCount = 0,
						uiConsigneeInsideProductContainer = $("#addConsigneeItem"+iGetProdId+" .addedReceivedConsigneeItem");

						$.each(uiConsigneeInsideProductContainer, function(){
							var uConsignInsideProdThis = $(this);


								uiDeleteConsigneBtn  = uConsignInsideProdThis.find(".deleteProductConsignee");

								uiDeleteConsigneBtn.off("click.deleteConsignProd").on("click.deleteConsignProd", function(){
									uConsignInsideProdThis.remove();
									// console.log("asds");

									var uiMainConsignee = uiConsigneeThis.find(".addedReceivedConsigneeItem")
										iCountDelete = 0;

									$.each(uiMainConsignee, function(){
										var iRowsThis = $(this),
											inumbers = iRowsThis.find('.add-consignee-numbers');

											iCountDelete++;

											inumbers.html(iCountDelete);
									});
								});

						});
				});

			}
			else if(sAction === 'displayEditProductCurrentRow')
			{
				var oGetSelectedReceiveId = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"}),
					oGetSelecteddata = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"}),
					uiDeleteEditSeletedProductBtn = $(".deleteEditSeletedProduct"),
					oGatheredDataSelected = oGetSelecteddata["product_info"],
					uiDeleteConfirmBtn = $("#confirmDelete"); 

				

				uiDeleteEditSeletedProductBtn.off("click.deleteProduct").on("click.deleteProduct", function(){
					var uiThisBtn = $(this);

					$("body").css({overflow:'hidden'});
						

					$("#trashModal").addClass("showed");

					$("#trashModal .close-me").on("click",function(){
						$("body").css({'overflow-y':'initial'});
						$("#trashModal").removeClass("showed");
					});

					uiDeleteConfirmBtn.off("click.confirmDelete").on("click.confirmDelete", function(){
					 	var uiGetParent = uiThisBtn.closest(".display-products-info");
							iProductId = uiGetParent.data("product_id"),
							iProdIntevID = uiGetParent.data("product_inventory_id");

								_removeProductInventory(iProdIntevID);

								$.each(oGetSelecteddata["product_info"], function(i, el){
								    if (this.product_id == iProductId){
								        oGetSelecteddata["product_info"].splice(i, 1);
								    }

								});

								cr8v_platform.localStorage.set_local_storage({
									name : "setSelectedReceivingConsigneeItem",
									data : oGetSelecteddata
								});



								$("#displayEditProductCurrent").load(location.href + " #displayEditProductCurrent", function(){
									var oGetSelecteddataNew = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"});

									_displayEditProductItems(oGetSelecteddataNew["product_info"]);
								});

								$(".modal-close.close-me").trigger("click");
					});
	
				});

				

				
			}
			
		}
		else if(sCurrentPath.indexOf("consignee_edit_truck") > -1)
		{
			if(sAction === 'EditNewItemProdReceivingConsigneeGoodsTruck')
			{
				// console.log("okkk");

				_itemsEvent();

				var uiaddItemProductConsigneeBtn = $("#addItemProductConsigneeBtn"),
					oGetSelectedReceiveConsigneeProduct = "",
					oGetCurrentSelectedReceivedConsigneeProduct = "",
				 	uiGetEditDataBtn = $(".setProductListSelect").find(".frm-custom-dropdown").find(".frm-custom-dropdown-option").find(".option"),
				 	sGetAttr = "",
				 	oGatherDataFromProducts = {};

					
					var SelectedConsignInterval = setInterval(function(){
						oGetSelectedReceiveConsigneeProduct = cr8v_platform.localStorage.get_local_storage({name:"getAllProductsReceiveConsignee"});
						oGetCurrentSelectedReceivedConsigneeProduct = cr8v_platform.localStorage.get_local_storage({name:"SetSelectedProductForReceivingConsignee"});

						if(oGetSelectedReceiveConsigneeProduct !== null && oGetCurrentSelectedReceivedConsigneeProduct !== null)
						{
							clearInterval(SelectedConsignInterval);
						}

					}, 300);


				uiaddItemProductConsigneeBtn.off('click.editItemProductConsigneeBtn').on('click.editItemProductConsigneeBtn', function(e){
					e.preventDefault();

					var oSetGatherDataFromProducts = {},
						oTempStoreData = [];

					sGetAttr = $("#selectProductListSelect").val();
					// console.log(sGetAttr);
					
					for(var x in oGetSelectedReceiveConsigneeProduct)
					{
						
						if(oGetSelectedReceiveConsigneeProduct[x]["id"] == sGetAttr)
						{

							if(oGetCurrentSelectedReceivedConsigneeProduct === null)
							{
								


								oSetGatherDataFromProducts = {
									id : sGetAttr,
									name : oGetSelectedReceiveConsigneeProduct[x]["name"],
									sku : oGetSelectedReceiveConsigneeProduct[x]["sku"],
									product_category : oGetSelectedReceiveConsigneeProduct[x]["product_category"],
									description : oGetSelectedReceiveConsigneeProduct[x]["description"],
									image : oGetSelectedReceiveConsigneeProduct[x]["image"],
									product_shelf_life : oGetSelectedReceiveConsigneeProduct[x]["product_shelf_life"],
									threshold_min : oGetSelectedReceiveConsigneeProduct[x]["threshold_min"],
									threshold_maximum : oGetSelectedReceiveConsigneeProduct[x]["threshold_maximum"],
									unit_price : oGetSelectedReceiveConsigneeProduct[x]["unit_price"],
									is_deleted : oGetSelectedReceiveConsigneeProduct[x]["is_deleted"]
								}

								oStoredDataFromProducts.push(oSetGatherDataFromProducts);
								// console.log(oStoredDataFromProducts);
							}
							else
							{


								for(var i in oGetCurrentSelectedReceivedConsigneeProduct)
								{
									oGatherDataFromProducts = {
										id : oGetCurrentSelectedReceivedConsigneeProduct[i]["id"],
										name : oGetCurrentSelectedReceivedConsigneeProduct[i]["name"],
										sku : oGetCurrentSelectedReceivedConsigneeProduct[i]["sku"],
										product_category : oGetCurrentSelectedReceivedConsigneeProduct[i]["product_category"],
										description : oGetCurrentSelectedReceivedConsigneeProduct[i]["description"],
										image : oGetCurrentSelectedReceivedConsigneeProduct[i]["image"],
										product_shelf_life : oGetCurrentSelectedReceivedConsigneeProduct[i]["product_shelf_life"],
										threshold_min : oGetCurrentSelectedReceivedConsigneeProduct[i]["threshold_min"],
										threshold_maximum : oGetCurrentSelectedReceivedConsigneeProduct[i]["threshold_maximum"],
										unit_price : oGetCurrentSelectedReceivedConsigneeProduct[i]["unit_price"],
										is_deleted : oGetCurrentSelectedReceivedConsigneeProduct[i]["is_deleted"]
									}
									oStoredDataFromProducts.push(oGatherDataFromProducts);
								}

								var oSetGatherDataFromProducts = {
									id : sGetAttr,
									name : oGetSelectedReceiveConsigneeProduct[x]["name"],
									sku : oGetSelectedReceiveConsigneeProduct[x]["sku"],
									product_category : oGetSelectedReceiveConsigneeProduct[x]["product_category"],
									description : oGetSelectedReceiveConsigneeProduct[x]["description"],
									image : oGetSelectedReceiveConsigneeProduct[x]["image"],
									product_shelf_life : oGetSelectedReceiveConsigneeProduct[x]["product_shelf_life"],
									threshold_min : oGetSelectedReceiveConsigneeProduct[x]["threshold_min"],
									threshold_maximum : oGetSelectedReceiveConsigneeProduct[x]["threshold_maximum"],
									unit_price : oGetSelectedReceiveConsigneeProduct[x]["unit_price"],
									is_deleted : oGetSelectedReceiveConsigneeProduct[x]["is_deleted"]
								}
								oStoredDataFromProducts.push(oSetGatherDataFromProducts);

							}


							

							cr8v_platform.localStorage.set_local_storage({
								name : "SetSelectedProductForReceivingConsignee",
								data : oStoredDataFromProducts
							});




							// console.log(JSON.stringify(oStoredDataFromProducts));
						}

						
					}

					oTempStoreData.push(oSetGatherDataFromProducts);

					CCReceivingConsigneeGoods.renderElement("displayEditSelectedProductForReceivingConsignee", oTempStoreData);	
					// var oGetCurrentSelectedReceivedConsigneeProduct = cr8v_platform.localStorage.get_local_storage({name:"SetSelectedProductForReceivingConsignee"});
					// console.log(JSON.stringify(oGetCurrentSelectedReceivedConsigneeProduct))
					// oStoredDataFromProducts = [];


				});

				
			}
			else if(sAction === 'getEditAllProductsSeletedForReceiveConsigneeTruck')
			{

				var uiDisplayReceivingConsigneeProductSelectsGet = $("#displayEditReceivingConsigneeProductSelects"),
					oReceiveConsigneeData = cr8v_platform.localStorage.get_local_storage({name:"SetSelectedProductForReceivingConsignee"}),
					oGetAllConsigneeReceiveConsignee = cr8v_platform.localStorage.get_local_storage({name:"getAllConsigneeReceiveConsignee"}),
					oNewConsignees = [],
					uiDeleteProdBtn = $(".deleteProd"),
					uiConfirmDeleteProd = $("#confirmDelete"),
					uiSelectConsigneeListSelect = ui.find(".selectConsigneeListSelect"),
					uiParentDropdown = uiSelectConsigneeListSelect.parent(),
					uiSiblingDropdown= uiParentDropdown.find('.frm-custom-dropdown'),
					uiConsigneeDistContainsdsd = ui.closest(".addConsigneeItemAddNumber").find(".addedReceivedConsigneeItem").length,
					uiConsigneeDistContain = ui.closest(".addConsigneeItemAddNumber").find(".addedReceivedConsigneeItem"),
					btnAddMore = ui.closest(".product_selected_receive_consign").find(".addConsigneeRow");

					uiSiblingDropdown.remove();
					uiSelectConsigneeListSelect.removeClass("frm-custom-dropdown-origin");


					if(uiConsigneeDistContainsdsd == 0)
					{
						var sHtml = '<option value="">Consignee Name</option>';

						for(var x in oConsigneeNamesCollects)
						{
							sHtml = '<option value="'+oConsigneeNamesCollects[x]['id']+'">'+oConsigneeNamesCollects[x]['name']+'</option>';
							uiSelectConsigneeListSelect.append(sHtml);
						}

						uiSelectConsigneeListSelect.transformDD();

						CDropDownRetract.retractDropdown(uiSelectConsigneeListSelect);


					}else{


						for(var x in oConsigneeNamesCollects)
						{
							oNewConsignees.push(oConsigneeNamesCollects[x]);
						}

						$.each(uiConsigneeDistContain, function(){
							var uiMainThis = $(this),
								uiSelects = uiMainThis.find(".selectConsigneeListSelect option:selected");

								$.each(oNewConsignees, function(i, el){
									if (this.id == uiSelects.val()){
									   oNewConsignees.splice(i, 1);
									}
								});
						});

						if(Object.keys(oNewConsignees).length == 0)
						{
							btnAddMore.hide();
							ui.remove();
						}else{
							var sHtml = '<option value="">Consignee Name</option>';

							for(var x in oNewConsignees)
							{
								sHtml = '<option value="'+oNewConsignees[x]['id']+'">'+oNewConsignees[x]['name']+'</option>';
								uiSelectConsigneeListSelect.append(sHtml);
							}

							uiSelectConsigneeListSelect.transformDD();

							CDropDownRetract.retractDropdown(uiSelectConsigneeListSelect);

						}

						

						// console.log(JSON.stringify(oNewConsignees));

					}





					uiDeleteProdBtn.off("click.deleteProd").on("click.deleteProd", function(){
						var uiParent = $(this).closest(".product_selected_receive_consign"),
							iProdId = uiParent.data("ProdId");
						
						$("body").css({overflow:'hidden'});
						

						$("#trashModal").addClass("showed");

						$("#trashModal .close-me").on("click",function(){
							$("body").css({'overflow-y':'initial'});
							$("#trashModal").removeClass("showed");
						});


						uiConfirmDeleteProd.off("click.deleteNow").on("click.deleteNow", function(){
							var oRemoveData = oReceiveConsigneeData;
							// console.log(JSON.stringify(oRemoveData));

								$.each(oRemoveData, function(i, el){
								    if (this.id == iProdId){
								        oRemoveData.splice(i, 1);
								    }
								});

								cr8v_platform.localStorage.set_local_storage({
									name : "SetSelectedProductForReceivingConsignee",
									data : oRemoveData
								});
								oStoredDataFromProducts = [];

								uiParent.remove();

								if(oRemoveData.length > 0){
									// CCReceivingConsigneeGoods.renderElement("displayEditSelectedProductForReceivingConsignee", oRemoveData);
								}else{ 
									uiDisplayReceivingConsigneeProductSelectsGet.html(""); 

									cr8v_platform.localStorage.delete_local_storage({name : "SetSelectedProductForReceivingConsignee"});
								}

								

								$(".modal-close.close-me").trigger("click");

								var options = {
								  form : "box",
								  autoShow : true,
								  type : "success",
								  title  : "Delete Product",
								  message : "Successfully Deleted",
								  speed : "slow",
								  withShadow : true
								}
								
								$("body").feedback(options);
						});

					});

					var uiBulkSet = $(".bulk_set"),
						uiBagSet = $(".bag_set"),
						btnAddConsigneeRow = $(".addConsigneeRow"),
						oUnitOfMeasures = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"}),
						uiUnitOfMeasure = ui.find(".unit-of-measure");
						
						for(var x in oUnitOfMeasures)
						{
							if(oUnitOfMeasures[x]['name'] == "kg" || oUnitOfMeasures[x]['name'] == "mt" || oUnitOfMeasures[x]['name'] == "L" )
							{					
								sHtmlMeasure = '<option value="'+oUnitOfMeasures[x]['id']+'">'+oUnitOfMeasures[x]['name']+'</option>';
								 ui.find(".unit-of-measure").append(sHtmlMeasure);
							}

						}

						ui.find(".unit-of-measure").show().css({
							"padding" : "3px 4px",
							"width" :  "50px"
						});

						_triggerUnitMeasure(ui);

						uiUnitOfMeasure.off("change.selectMeasurement").on("change.selectMeasurement", function(){
							var uiParent = $(this).closest(".product_selected_receive_consign"),
								iProdId = uiParent.data("ProdId"),
								sGetMeasurementValue = $(this).val(),
								sTriggerMeasermentValue = $("#unit-measurement"+iProdId);

								 $("#unit-measurement"+iProdId+" option[value='"+sGetMeasurementValue+"']").attr("selected", "selected");


						
						});



					uiBulkSet.off("click.bulk").on("click.bulk", function(){
						var uiParent = $(this).closest(".product_selected_receive_consign"),
							iProdId = uiParent.data("ProdId"),
							uiQtyReceiveg = $("#qtyReceiveg"+iProdId),
							uiqtyAddReceivedConsignee = $(".qtyAddReceivedConsignee"+iProdId);

							if($(this).is(":checked"))
							{
								uiQtyReceiveg.val("");
								uiQtyReceiveg.attr("disabled", true);
								uiqtyAddReceivedConsignee.attr("disabled", true);
							}
					});

					uiBagSet.off("click.bag").on("click.bag", function(){
						var uiParent = $(this).closest(".product_selected_receive_consign"),
							iProdId = uiParent.data("ProdId"),
							uiQtyReceiveg = $("#qtyReceiveg"+iProdId),
							uiqtyAddReceivedConsignee = $(".qtyAddReceivedConsignee"+iProdId);

							if($(this).is(":checked"))
							{
								uiQtyReceiveg.attr("disabled", false);
								uiqtyAddReceivedConsignee.attr("disabled", false);
							}
					});


					btnAddConsigneeRow.off("click.AddConsigneeRow").on("click.AddConsigneeRow", function(e){
						e.preventDefault();
						var uiParent = $(this).closest(".product_selected_receive_consign"),
							iProdId = uiParent.data("ProdId"),
							uiAddReceivedConsigneeItemsContainer = $("#addConsigneeItem"+iProdId),
							uiAddConsigneeNumberContainer = uiParent.find("#addConsigneeItem"+iProdId).find(".addedReceivedConsigneeItem"),
							iCount = 1;

							$.each(uiAddConsigneeNumberContainer, function(){ iCount++; });

							if(iCount == 1)
							{
								var newCountRow = iCount+1
								cr8v_platform.localStorage.set_local_storage({
									name : "setNewConsigneeRow",
									data : {value:newCountRow}
								});
							}
							else{
								var newCountRow = iCount+1
								cr8v_platform.localStorage.set_local_storage({
									name : "setNewConsigneeRow",
									data : {value:iCount}
								});
							}



							CCReceivingConsigneeGoods.renderElement("AddReceivedConsigneeItems", iProdId);

							

					});



				// DELETE PRODUCT CONSIGNEE
				var uiProductConsigneeContainer = $("#displayEditReceivingConsigneeProductSelects .product_selected_receive_consign");

				$.each(uiProductConsigneeContainer, function(){
					var uiConsigneeThis = $(this),
						iGetProdId = uiConsigneeThis.data("ProdId"),
						iCount = 0,
						uiConsigneeInsideProductContainer = $("#addConsigneeItem"+iGetProdId+" .addedReceivedConsigneeItem");

						$.each(uiConsigneeInsideProductContainer, function(){
							var uConsignInsideProdThis = $(this);


								uiDeleteConsigneBtn  = uConsignInsideProdThis.find(".deleteProductConsignee");

								uiDeleteConsigneBtn.off("click.deleteConsignProd").on("click.deleteConsignProd", function(){
									uConsignInsideProdThis.remove();
									

									for(var x in oConsigneeNamesCollects)
									{
										oNewConsignees.push(oConsigneeNamesCollects[x]);
									}


									var uiMainConsignee = uiConsigneeThis.find(".addedReceivedConsigneeItem")
										iCountDelete = 0;

									$.each(uiMainConsignee, function(){
										var iRowsThis = $(this),
											inumbers = iRowsThis.find('.add-consignee-numbers');

											iCountDelete++;

											inumbers.html(iCountDelete);
									});
									// console.log("asds");
								});

						});
				});

			}
			else if(sAction === 'displayEditProductCurrentRow')
			{
				var oGetSelectedReceiveId = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"}),
					oGetSelecteddata = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"}),
					uiDeleteEditSeletedProductBtn = $(".deleteEditSeletedProduct"),
					oGatheredDataSelected = oGetSelecteddata["product_info"],
					uiDeleteConfirmBtn = $("#confirmDelete"); 

				

				uiDeleteEditSeletedProductBtn.off("click.deleteProduct").on("click.deleteProduct", function(){
					var uiThisBtn = $(this);

					$("body").css({overflow:'hidden'});
						

					$("#trashModal").addClass("showed");

					$("#trashModal .close-me").on("click",function(){
						$("body").css({'overflow-y':'initial'});
						$("#trashModal").removeClass("showed");
					});

					uiDeleteConfirmBtn.off("click.confirmDelete").on("click.confirmDelete", function(){
					 	var uiGetParent = uiThisBtn.closest(".display-products-info");
							iProductId = uiGetParent.data("product_id"),
							iProdIntevID = uiGetParent.data("product_inventory_id");

								_removeProductInventory(iProdIntevID);

								$.each(oGetSelecteddata["product_info"], function(i, el){
								    if (this.product_id == iProductId){
								        oGetSelecteddata["product_info"].splice(i, 1);
								    }

								});

								cr8v_platform.localStorage.set_local_storage({
									name : "setSelectedReceivingConsigneeItem",
									data : oGetSelecteddata
								});



								$("#displayEditProductCurrent").load(location.href + " #displayEditProductCurrent", function(){
									var oGetSelecteddataNew = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"});

									_displayEditProductItems(oGetSelecteddataNew["product_info"]);
								});

								$(".modal-close.close-me").trigger("click");
					});
	
				});


			}
			else if(sAction === 'EditNewReceivingConsigneeGoodsValidTruck')
			{
				

				var uiEditSaveNewConsigneeGoodsBtn = $("#editSaveNewConsigneeGoods"),
					uiDisplayEditConsigneeTruckName = $("#display-edit-truck-name"),
					uiDisplayEditConsigneeTruckTrucking = $("#display-edit-consignee-truck-trucking"),
					uiDisplayEditConsigneeTruckPlateNumber = $("#display-edit-consignee-truck-plate-number"),
					uiDisplayEditConsigneeTruckDriverName = $("#display-edit-consignee-truck-driver-name"),
					uiDisplayEditConsigneeTruckLicenseNumber = $("#display-edit-consignee-truck-license-number");
					

				//This is for validation 
				$("#EditTruckReceivingConsigneeGoodsForm").cr8vformvalidation({
					'preventDefault' : true,
				    'data_validation' : 'datavalid',
				    'input_label' : 'labelinput',
				    'onValidationError': function(arrMessages) {
				        // alert(JSON.stringify(arrMessages)); //shows the errors
				  //       var options = {title : "Edit Recieving Consignee Goods Error!", message : "Please Fill up all required filled", speed : "slow", withShadow : true, icon : failIcon, type : "danger",};
						// $("body").feedback(options);
			        },
				    'onValidationSuccess': function() {



						// FOR ADDING PRODUCTS

						var uiProductContainer = $("#displayEditReceivingConsigneeProductSelects .product_selected_receive_consign"),
							oGetAllData = {},
							iCountAll = 0;


							$.each(uiProductContainer, function(){
								var uiThis = $(this),
									iProductId = uiThis.data("ProdId"),
									oGetCurrentReceivingConsigneeSelected = cr8v_platform.localStorage.get_local_storage({name:"getAllReceivingConsigneeProdutcts"}),
									sVendorName = $("#addReceiveConsigneeVendor"+iProductId).val().trim().toLowerCase(),
									uiQuantityMeasurement = uiThis.find(".unit-of-measure"),
									sGetBags = "",
									uiCheckBulkBag = '';


									// FOR ADDING CONSIGNEE
									var uiConsigneeContainer = $("#addConsigneeItem"+iProductId+" .addedReceivedConsigneeItem"),
										oConsignData = {},
										iCountConsign = 0;

										uiCheckBulkBag = uiThis.find("input[name='bag-or-bulk"+iProductId+"']:checked").val();

										$.each(uiConsigneeContainer, function(){
											var uiConsignThis = $(this),
												oGetCurrentReceivingConsigneeItems = cr8v_platform.localStorage.get_local_storage({name:"CurrentReceivingConsigneeItems"}),
												uiSelectConsignAddQty = uiConsignThis.find(".selectConsignAddQty").val(),
												iAddConsigneeQuantity = uiConsignThis.find(".addConsigneeQuantity").val(),
												sAddConsigneeMeasurement = uiConsignThis.find(".addConsigneeMeasurement").val(),
												iAddConsigneeBagQty = uiConsignThis.find(".addConsigneeBagQty").val();

												// console.log(uiSelectConsignAddQty);
												// console.log(iAddConsigneeQuantity);
												// console.log(sAddConsigneeMeasurement);
												// console.log(iAddConsigneeBagQty);

												if(oGetCurrentReceivingConsigneeItems === null)
												{
													oConsignData = {
														product_id : iProductId,
														consignee_id : uiSelectConsignAddQty,
														distribution_qty : iAddConsigneeQuantity,
														unit_of_measure_id : uiQuantityMeasurement.val(),
														bag : iAddConsigneeBagQty,
														loading_method: uiCheckBulkBag

													}
													oStoredReceiveConsigneeItems.push(oConsignData);

													cr8v_platform.localStorage.set_local_storage({
														name: "CurrentReceivingConsigneeItems",
														data: oStoredReceiveConsigneeItems
													});
													oStoredReceiveConsigneeItems = [];
												}else{
													var iCount = 0,
														oGetCurrentReceivingConsigneeItemsAll = cr8v_platform.localStorage.get_local_storage({name:"CurrentReceivingConsigneeItems"});

													for(var i in oGetCurrentReceivingConsigneeItemsAll)
													{
														oConsignData = {
															product_id : oGetCurrentReceivingConsigneeItemsAll[i]["product_id"],
											    			consignee_id : oGetCurrentReceivingConsigneeItemsAll[i]["consignee_id"],
											    			distribution_qty : oGetCurrentReceivingConsigneeItemsAll[i]["distribution_qty"],
											    			unit_of_measure_id :  oGetCurrentReceivingConsigneeItemsAll[i]["unit_of_measure_id"],
											    			bag :  oGetCurrentReceivingConsigneeItemsAll[i]["bag"],
											    			loading_method:  oGetCurrentReceivingConsigneeItemsAll[i]["loading_method"]
											
											    		}
											    		oStoredReceiveConsigneeItems.push(oConsignData);

											    		if(uiSelectConsignAddQty == oGetCurrentReceivingConsigneeItems[i]["consignee_id"])
														{
															iCount++;
														}
														
													}

													if(iCount == 0)
													{
														var oConsignDataNew = {
															product_id : iProductId,
															consignee_id : uiSelectConsignAddQty,
															distribution_qty : iAddConsigneeQuantity,
															unit_of_measure_id : uiQuantityMeasurement.val(),
															bag : iAddConsigneeBagQty,
															loading_method: uiCheckBulkBag
														}
														oStoredReceiveConsigneeItems.push(oConsignDataNew);

													}
													
													cr8v_platform.localStorage.set_local_storage({
														name: "CurrentReceivingConsigneeItems",
														data: oStoredReceiveConsigneeItems
													});

													oStoredReceiveConsigneeItems = [];
													
												}

												console.log("icountto labas");
												
													

												
											// oStoredReceiveConsigneeItems = [];
											

										});
										
										oStoredReceiveConsigneeItems = [];



										var oGetCurrentReceivingConsigneeItemsAll = cr8v_platform.localStorage.get_local_storage({name:"CurrentReceivingConsigneeItems"}),
											getSelecteddataOld = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"}),
											iCounntOldConsign = 0,
											iCountTry = 0;
											oStoredReceiveConsigneeItems = [];
											for(var e in getSelecteddataOld["product_info"])
											{
												if(getSelecteddataOld["product_info"][e]["consignee_info"] !== null)
												{	
													iCounntOldConsign++;
												}	
											}

											if(iCounntOldConsign != 0)
											{
												var icountto = 0;
												oStoredReceiveConsigneeItems = [];
												
												for(var w in getSelecteddataOld["product_info"])
												{
													
													if(getSelecteddataOld["product_info"][w]["consignee_info"] !== null)
													{
														
														for(var s in getSelecteddataOld["product_info"][w]["consignee_info"])
														{
															// console.log(iCountTry);
															iCountTry++;
															// console.log(JSON.stringify(getSelecteddataOld["product_info"][w]["consignee_info"]));

															var oConsignData = {
																product_id : getSelecteddataOld["product_info"][w]["product_id"],
												    			consignee_id : getSelecteddataOld["product_info"][w]["consignee_info"][s]["consignee_id"],
												    			distribution_qty : getSelecteddataOld["product_info"][w]["consignee_info"][s]["distribution_qty"],
												    			unit_of_measure_id :  getSelecteddataOld["product_info"][w]["consignee_info"][s]["unit_of_measure_id"],
												    			bag :  getSelecteddataOld["product_info"][w]["consignee_info"][s]["bag"],
												    			loading_method:  getSelecteddataOld["product_info"][w]["loading_method"]
												
												    		}
												    		oStoredReceiveConsigneeItems.push(oConsignData);

														}
													}
													// console.log(JSON.stringify(oStoredReceiveConsigneeItems));
													console.log("icountto");
													icountto++;

													
												}

												for(var q in oGetCurrentReceivingConsigneeItemsAll)
												{
													var oConsignData = {
														product_id : oGetCurrentReceivingConsigneeItemsAll[q]["product_id"],
										    			consignee_id : oGetCurrentReceivingConsigneeItemsAll[q]["consignee_id"],
										    			distribution_qty : oGetCurrentReceivingConsigneeItemsAll[q]["distribution_qty"],
										    			unit_of_measure_id :  oGetCurrentReceivingConsigneeItemsAll[q]["unit_of_measure_id"],
										    			bag :  oGetCurrentReceivingConsigneeItemsAll[q]["bag"],
										    			loading_method:  oGetCurrentReceivingConsigneeItemsAll[q]["loading_method"]
										
										    		}
										    		oStoredReceiveConsigneeItems.push(oConsignData);
												}

												cr8v_platform.localStorage.set_local_storage({
													name: "CurrentReceivingConsigneeItems",
													data: oStoredReceiveConsigneeItems
												});



											}
											oStoredReceiveConsigneeItems = [];



										

									oStoredReceiveConsigneeProduct = [];

									($("#qtyReceiveg"+iProductId).val() != "") ? sGetBags = $("#qtyReceiveg"+iProductId).val() : sGetBags = "";



									if(oGetCurrentReceivingConsigneeSelected === null)
									{
										oGetAllData = {
											product_id:iProductId,
											vendor : sVendorName,
											loading_method :  uiCheckBulkBag,
											unit_of_measure_id :  $("#unit-measurement"+iProductId).val(),
											qty_to_receive :  $("#qtyReceiveKg"+iProductId).val(),
											bag :  sGetBags
										}
										oStoredReceiveConsigneeProduct.push(oGetAllData);
									}else{
										for(var i in oGetCurrentReceivingConsigneeSelected)
										{
											oGetAllData = {
								    			product_id:oGetCurrentReceivingConsigneeSelected[i]["product_id"],
								    			vendor : oGetCurrentReceivingConsigneeSelected[i]["vendor"],
								    			loading_method :  oGetCurrentReceivingConsigneeSelected[i]["loading_method"],
								    			unit_of_measure_id :  oGetCurrentReceivingConsigneeSelected[i]["unit_of_measure_id"],
								    			qty_to_receive :  oGetCurrentReceivingConsigneeSelected[i]["qty_to_receive"],
								    			bag :  oGetCurrentReceivingConsigneeSelected[i]["bag"],
								    		}
								    		oStoredReceiveConsigneeProduct.push(oGetAllData);
										}
										
										if(iProductId != oGetCurrentReceivingConsigneeSelected[i]["product_id"])
										{
											var oGetAllDataNew = {
												product_id:iProductId,
												vendor : sVendorName,
												loading_method :  uiCheckBulkBag,
												unit_of_measure_id :  $("#unit-measurement"+iProductId).val(),
												qty_to_receive :  $("#qtyReceiveKg"+iProductId).val(),
												bag :  sGetBags
											}
											oStoredReceiveConsigneeProduct.push(oGetAllDataNew);
										}
										
										
									}

									cr8v_platform.localStorage.set_local_storage({
										name: "getAllReceivingConsigneeProdutcts",
										data: oStoredReceiveConsigneeProduct
									});
							
							});
					
							oStoredReceiveConsigneeProduct = [];


						// Merge Old Product and new Product
						var oReceiveConsigneeData = cr8v_platform.localStorage.get_local_storage({name:"getAllReceivingConsigneeProdutcts"}),
							getSelecteddataOld = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"});
							// console.log(JSON.stringify(getSelecteddataOld));
							
								if(getSelecteddataOld["product_info"] === null && oReceiveConsigneeData !== null)
								{
									cr8v_platform.localStorage.set_local_storage({
										name : "getAllReceivingConsigneeProdutcts",
										data : oReceiveConsigneeData
									});
									console.log("first");
								}
								else if(getSelecteddataOld["product_info"] !== null && oReceiveConsigneeData === null)
								{
									var oCreateData = {};

									for(var x in getSelecteddataOld["product_info"])
									{
										oCreateData = {
											product_id : getSelecteddataOld["product_info"][x]["product_id"],
											vendor : getSelecteddataOld["product_info"][x]["vendor_name"],
											loading_method : getSelecteddataOld["product_info"][x]["loading_method"],
											unit_of_measure_id : getSelecteddataOld["product_info"][x]["unit_of_measure_id"],
											qty_to_receive : getSelecteddataOld["product_info"][x]["qty"],
											bag : getSelecteddataOld["product_info"][x]["bag"]
										}

										oCompeleteEditProduct.push(oCreateData);

									}

										cr8v_platform.localStorage.set_local_storage({
											name : "getAllReceivingConsigneeProdutcts",
											data : oCompeleteEditProduct
										});
										console.log("second");
								}
								else if(getSelecteddataOld["product_info"] !== null && oReceiveConsigneeData !== null)
								{
									for(var x in getSelecteddataOld["product_info"])
									{
										oCreateData = {
											product_id : getSelecteddataOld["product_info"][x]["product_id"],
											vendor : getSelecteddataOld["product_info"][x]["vendor_name"],
											loading_method : getSelecteddataOld["product_info"][x]["loading_method"],
											unit_of_measure_id : getSelecteddataOld["product_info"][x]["unit_of_measure_id"],
											qty_to_receive : getSelecteddataOld["product_info"][x]["qty"],
											bag : getSelecteddataOld["product_info"][x]["bag"]
										}

										oCompeleteEditProduct.push(oCreateData);

									}

									for(var i in oReceiveConsigneeData)
									{
										var oCreateDataNew = {
											product_id :oReceiveConsigneeData[i]["product_id"],
											vendor : oReceiveConsigneeData[i]["vendor"],
											loading_method : oReceiveConsigneeData[i]["loading_method"],
											unit_of_measure_id : oReceiveConsigneeData[i]["unit_of_measure_id"],
											qty_to_receive : oReceiveConsigneeData[i]["qty_to_receive"],
											bag : oReceiveConsigneeData[i]["bag"]
										}
										oCompeleteEditProduct.push(oCreateDataNew);
									}

									// console.log("thirdd");

									cr8v_platform.localStorage.set_local_storage({
										name : "getAllReceivingConsigneeProdutcts",
										data : oCompeleteEditProduct
									});
								}
							

							var checkProducts = cr8v_platform.localStorage.get_local_storage({name:"getAllReceivingConsigneeProdutcts"});

							// console.log(console.log(checkProducts));

						_updateTruck();


						
			        }
				});


				uiEditSaveNewConsigneeGoodsBtn.off('click.editSaveNewConsigneeGood').on('click.editSaveNewConsigneeGood', function(e){
					e.preventDefault();
					// alert("asdsa");

					var oError = [],
						sPurchaseOrder = $("#display-edit-consignee-truck-poNum"),
						sReceiveConsigneeVendorItem = $(".receiveConsigneeVendorItem"),
						sQtyReceived = $(".qty-received"),
						sAddConsigneeQuantity = $(".addConsigneeQuantity");






					// Start Validation for consignee
					var uiContainerProducts = $("#displayEditReceivingConsigneeProductSelects").find(".product_selected_receive_consign"),
						iCountProd = 0;

						$.each(uiContainerProducts, function(){
							var uiThisProd = $(this),
								uiQtyReceived = uiThisProd.find(".qty-received"),
								uiQtyBags = uiThisProd.find(".qty-bags"),
								uiSkuProdName = uiThisProd.find(".font-16.font-bold.f-left"), 
								iQtyReceived = parseFloat(uiQtyReceived.val()),
								iQtyBags = parseFloat(uiQtyBags.val()),
								uiAddedReceivedConsigneeItem = uiThisProd.find(".addedReceivedConsigneeItem"),
								oGatherConsigneeSelected = [],
								oGatherConsigneeQty = [],
								oGatherConsigneeBags = [];

								$.each(uiAddedReceivedConsigneeItem, function(){
									uiThisGetConsignee = $(this),
									uiGetConsigneeItem = uiThisGetConsignee.find(".addConsigneeQuantity"),
									uiGetConsigneeBags = uiThisGetConsignee.find(".addConsigneeBagQty"),
									iGetConsigneeName = uiThisGetConsignee.find(".selectConsignAddQty"),				
									iGetConsigneeItem = parseFloat(uiGetConsigneeItem.val()),
									iGetConsigneeBags = parseFloat(uiGetConsigneeBags.val());
									uiGetConsigneeName = uiThisGetConsignee.find(".selectConsignAddQty"),
									iGetConsigneeName = uiGetConsigneeName.val(),
									oGatherConsigneeQty.push(iGetConsigneeItem);
									oGatherConsigneeBags.push(iGetConsigneeBags);

									oGatherConsigneeSelected.push(iGetConsigneeName);
									
								});

								var total = 0,
									iBagsTotal = 0;

								$.each(oGatherConsigneeQty,function() {
								    total += parseInt(this);
								});

								
								$.each(oGatherConsigneeBags,function() {
								    iBagsTotal += parseInt(this);
								});


								//check if consignee name is already used
								 var myConsigneeNameArr = oGatherConsigneeSelected.sort(),
									iCountConsignee = 0;

								for (var i = 0; i < myConsigneeNameArr.length; i++) {
									if (myConsigneeNameArr.indexOf(myConsigneeNameArr[i]) == myConsigneeNameArr.lastIndexOf(myConsigneeNameArr[i])) { 

									}else{
										iCountConsignee++;
									}
								} 

								if(iCountConsignee != 0)
								{
									oError.push("Consignee Distribution Name must be unique in "+uiSkuProdName.text());
								}

								
								
								if(iQtyReceived != total){
									oError.push("Consignee Distribution Quantity not equal to "+uiSkuProdName.text());
									
								}


								if(uiQtyBags.val() != "")
								{
									
									if(iQtyBags != iBagsTotal)
									{
										oError.push("Consignee Distribution Bags not equal to "+uiSkuProdName.text());
									}
								}else if(uiQtyBags.val() == "" && isNaN( Number( iBagsTotal ) ) === false)
								{
									oError.push("Consignee Distribution Bags not equal to "+uiSkuProdName.text());
								}



								
						});


					// End


					//(sPurchaseOrder.val().trim() == "") ? oError.push("Purchase Order is Required") : "";
					(sQtyReceived.val() == "") ? oError.push("Quantity to Receive is Required") : "";
					(sAddConsigneeQuantity.val() == "") ? oError.push("Consignee Quantity to Receive is Required") : "";
					(sReceiveConsigneeVendorItem.val() == "") ? oError.push("Product Vendor is Required") : "";

					(uiDisplayEditConsigneeTruckName.val().trim() == "") ?  oError.push("Vessel Name is Required") : "";
					(uiDisplayEditConsigneeTruckTrucking.val().trim() == "") ?  oError.push("Trucking is Required") : "";
					(uiDisplayEditConsigneeTruckPlateNumber.val().trim() == "") ?  oError.push("Plate Number is Required") : "";
					(uiDisplayEditConsigneeTruckDriverName.val().trim() == "") ?  oError.push("Driver Name is Required") : "";
					(uiDisplayEditConsigneeTruckLicenseNumber.val().trim() == "") ?  oError.push("License Number Type is Required") : "";








						oCollectModeOfDelivery = {
							modeType : "truck",
							vessel_name : uiDisplayEditConsigneeTruckName.val(),
							trucking : uiDisplayEditConsigneeTruckTrucking.val().trim(),
							plate_number : uiDisplayEditConsigneeTruckPlateNumber.val().trim(),
							driver_name : uiDisplayEditConsigneeTruckDriverName.val().trim(),
							license_name : uiDisplayEditConsigneeTruckLicenseNumber.val().trim()
						}

						// console.log(uiDisplayEditConsigneeVesselBerthingDate.val().trim());

						cr8v_platform.localStorage.set_local_storage({
							name: "modeOfDelivery",
							data : oCollectModeOfDelivery
						});

						if(oError.length !== 0)
						{
							$("body").feedback({title : "Edit Recieving Consignee Goods", message : oError.join("<br />"), type : "danger", icon : failIcon});

						}else{

							$("#EditTruckReceivingConsigneeGoodsForm").submit();
						}

							

				});


			}
		}
		

		// FOR UPLOADING DOCUMENTS
		
	}



	function renderElement(sType, oData)
	{

		if(sCurrentPath.indexOf("consignee_create") > -1) 
		{
			
			if(sType === 'displayReceivingConsigneeNotes')
			{

				var uiAddReceivingConsigneeNotesDisplay = $("#AddReceivingConsigneeNotesDisplay");

					uiAddReceivingConsigneeNotesDisplay.html("");

					for(var x in oData)
					{
						var sMessage = "",
							iMessageCount = oData[x]["message"].length;

							if(iMessageCount > 300)
							{
								sMessage = oData[x]["message"].substr(0, 300);
								sMessage +="...";
							}else{
								sMessage = oData[x]["message"];
							}
					
						var sHtml = '<div class="border-full padding-all-10 margin-left-18 margin-top-10 notes-contain">'+
										'<div class="border-bottom-small border-gray padding-bottom-10">'+
											'<p class="f-left font-14 font-400">Subject: '+oData[x]["subject"]+'</p>'+
											'<p class="f-right font-14 font-400">Date/Time: '+oData[x]["displayDate"]+'</p>'+
											'<div class="clear"></div>'+
										'</div>'+
										'<p class="font-14 font-400 no-padding-left padding-all-10 show-notes-message">'+sMessage+'</p>'+
										'<a href="" class="f-right padding-all-10 font-400 show-notes-messge-length" >Show More</a>'+
										'<div class="clear"></div>'+
									'</div>';
							var oSetHtmlData = $(sHtml);
								oSetHtmlData.data({
									"notesId":oData[x]["id"],
									"message":oData[x]["message"]
								});
								uiAddReceivingConsigneeNotesDisplay.append(oSetHtmlData);


							_triggerNoteLessMore();
					}
			}
			else if(sType === 'displayItemsForProductList')
			{
				var oSetDataPoductListReceivingConsignee = oData,
					uiSetProductListSelect = $("#selectProductListSelect"),
					uiParent = uiSetProductListSelect.parent(),
					uiSibling = uiParent.find('.frm-custom-dropdown');

					uiSibling.remove();
					uiSetProductListSelect.removeClass("frm-custom-dropdown-origin");



					var sHtml = '<option value="">Select Item</option>';

					for(var x in oSetDataPoductListReceivingConsignee)
					{
						sHtml = '<option value="'+oSetDataPoductListReceivingConsignee[x]['id']+'">'+oSetDataPoductListReceivingConsignee[x]['name']+'</option>';
						uiSetProductListSelect.append(sHtml);
					}

					uiSetProductListSelect.transformDD();

					CDropDownRetract.retractDropdown(uiSetProductListSelect);

			}
			else if(sType === 'displayItemsForVesselList')
			{
				
				
				var oSetDataVesselListReceivingConsignee = oData,
					uiSetVesselListSelect = $("#addInputVesselName"),
					uiParent = uiSetVesselListSelect.parent(),
					uiSibling = uiParent.find('.frm-custom-dropdown');

					uiSibling.remove();
					uiSetVesselListSelect.removeClass("frm-custom-dropdown-origin");



					var sHtml = '<option value="">Select Vessel</option>';

					if(oSetDataVesselListReceivingConsignee === 0){
						sHtml ='<option value="">No Vessels Found</option>';
						uiSetVesselListSelect.append(sHtml);
					}else{
						for(var x in oSetDataVesselListReceivingConsignee)
						{
							sHtml = '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'">'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
							uiSetVesselListSelect.append(sHtml);
						}

					}

					
					uiSetVesselListSelect.transformDD();

					CDropDownRetract.retractDropdown(uiSetVesselListSelect);

			}
			else if(sType === 'displayItemsForVesselListForTruck')
			{
				
				
				var oSetDataVesselListReceivingConsignee = oData,
					uiSetVesselListSelect = $("#addInputTruckOrigin"),
					uiParent = uiSetVesselListSelect.parent(),
					uiSibling = uiParent.find('.frm-custom-dropdown');

					uiSibling.remove();
					uiSetVesselListSelect.removeClass("frm-custom-dropdown-origin");



					var sHtml = '<option value="">Select Vessel</option>';

					if(oSetDataVesselListReceivingConsignee === 0){
						sHtml ='<option value="">No Vessels Found</option>';
						uiSetVesselListSelect.append(sHtml);
					}else{
						for(var x in oSetDataVesselListReceivingConsignee)
						{
							sHtml = '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'">'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
							uiSetVesselListSelect.append(sHtml);
						}

					}

					
					uiSetVesselListSelect.transformDD();

			}
			else if(sType === 'displaySelectedProductForReceivingConsignee' && Object.keys(oData).length > 0)
			{

				var uiDisplayReceivingConsigneeProductSelects = $("#displayReceivingConsigneeProductSelects"),
					sDefaultImg = "",
					uiAddInputVesselName = $(".mode-of-deliver").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt"),
					iCountConsignee = 1;


					
					for(var x in oData)
					{

						(oData[x]["image"] != "") ? sDefaultImg = oData[x]["image"] : sDefaultImg = DEFAULT_PROD_IMG;
						var iCustomId = oData[x]["id"];

							var sHtml ='<div class="border-full padding-all-20 margin-top-10 product_selected_receive_consign">'+
										'	<div class="bggray-white">'+
										'		<div class="height-100percent display-inline-mid width-230px">'+
										'			<img src="'+sDefaultImg+'" alt="" class="height-100percent width-100percent">'+
										'		</div>'+
										'		<div class="display-inline-top width-75percent padding-left-10">'+
										'			<div class="padding-all-15 bg-light-gray">'+
										'				<p class="font-16 font-bold f-left">SKU# '+oData[x]["sku"]+' - '+oData[x]["name"]+'</p>'+
										'				<div class="f-right width-20px margin-left-10">'+
										'					<img  src="'+BASEURL+'assets/images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor deleteProd">'+
										'				</div>'+
										'				<div class="clear"></div>'+
										'			</div>'+
										'				<div class="padding-all-15 text-left">'+
										'					<p class="f-left no-margin-all width-15percent font-bold">Vendor</p>'+
										'					<p class="f-left no-margin-all width-25percent"><input type="text" id="addReceiveConsigneeVendor'+iCustomId+'" datavalid="required"  class="display-inline-mid width-150px height-26 t-small validate-vendor receiveConsigneeVendorItem"></p>'+
										'					<p class="f-left no-margin-all width-19percent font-bold">Loading Method</p>'+
										'					<div class="f-left  margin-left-10 for_bulk_set" >'+
										'						<input type="radio" value="Bulk" class="display-inline-mid width-20px default-cursor bulk_set" name="bag-or-bulk'+iCustomId+'" id="bulk'+iCustomId+'">'+
										'						<label for="bulk'+iCustomId+'" class="display-inline-mid font-14 margin-top-5 default-cursor ">By Bulk</label>'+
										'					</div>'+
										'					<div class="f-left margin-left-50 for_bag_set">'+															
										'						<input type="radio" value="Bags" class="display-inline-mid width-50px default-cursor bag_set"  name="bag-or-bulk'+iCustomId+'" id="piece'+iCustomId+'">'+
										'						<label for="piece'+iCustomId+'" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bags</label>'+
										'					</div>'+
										'					<div class="clear"></div>'+
										'				</div>'+
										'				<div class="padding-all-15 bg-light-gray text-left font-0">'+
										'					<p class="display-inline-mid width-15percent font-bold">Shelf Life</p>'+
										'					<p class="display-inline-mid width-25percent">'+oData[x]["product_shelf_life"]+'</p>'+
										'					<p class="display-inline-mid width-20per font-14 font-bold">Qty to Receive</p>'+								
										'					<input type="text" id="qtyReceiveKg'+iCustomId+'" datavalid="required" class="display-inline-mid width-70px height-26 t-small qty-received" name="bag-or-bulk">'+
										'					<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10"><select class="unit-of-measure" id="unit-measurement'+iCustomId+'"></select>  in</p>'+ 
										'					<input type="text" id="qtyReceiveg'+iCustomId+'" class="display-inline-mid width-70px height-26 t-small qty-bags" name="bag-or-bulk">'+
										'					<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">Bags</p>'+
										'					<div class="clear"></div>'+
										'				</div>'+
										'			<div class="padding-all-15 text-left">'+
										'				<p class="f-left  font-bold">Consignee Distribution:</p>'+								
										'				<div class="clear"></div>'+
										'			</div>'+
										'			<div class="padding-all-15 text-left bg-light-gray addConsigneeItemAddNumber" id="addConsigneeItem'+iCustomId+'">'+
										'				<div class="margin-top-10 addedReceivedConsigneeItem" >'+
										'					<p class="font-bold display-inline-mid add-consignee-numbers getvaluetext'+iCustomId+'">1</p>'+
										'					<div class="select large display-inline-mid margin-left-10 setConsigneeListSelect">'+
										'						<select class="selectConsigneeListSelect selectConsignAddQty">'+
										'						</select>'+
										'					</div>'+
										'					<p class="font-bold display-inline-mid margin-left-10">Quantity</p>'+
										'					<input type="text" class="t-small display-inline-mid width-70px margin-left-10 addConsigneeQuantity">'+
										'					<p class="display-inline-mid font-300 margin-left-10 margin-right-15 display-unit-measure-label" style="width: 10px;">KG</p> in'+
										'					<input type="text" class="t-small display-inline-mid width-70px addConsigneeBagQty qtyAddReceivedConsignee'+iCustomId+'">'+
										'					<p class="display-inline-mid margin-left-10 font-300">Bags</p>'+
										'					<div class="display-inline-mid width-20px margin-left-10">'+
										'						<img src="../assets/images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor deleteProductConsignee">'+
										'					</div>'+
										'				</div>'+									
										'			</div>'+
										'			<a href="" class="f-right margin-top-10 font-500 addConsigneeRow" >+ Add Consignee</a>'+
										'			<div class="clear"></div>'+
										'		</div>'+
										'	</div>'+
										'</div>';

								var oSetData = $(sHtml);
									oSetData.data({
										"ProdId": oData[x]["id"]
									});

									if(uiAddInputVesselName.val().trim() == "Vessel")
									{
										oSetData.find(".for_bag_set").hide();
										oSetData.find("#bulk"+iCustomId).attr("checked", true);
										oSetData.find(".qty-bags").attr("disabled", true);
										oSetData.find(".addConsigneeBagQty").attr("disabled", true);
									}else{
										oSetData.find(".for_bag_set").show();
										oSetData.find("#piece"+iCustomId).attr("checked", true);
										oSetData.find(".qty-bags").attr("disabled", false);
										oSetData.find(".addConsigneeBagQty").attr("disabled", false);
									}



									uiDisplayReceivingConsigneeProductSelects.append(oSetData);



									CCReceivingConsigneeGoods.bindEvents('getAllProductsSeletedForReceiveConsignee', oSetData);

									$('#qtyReceiveKg'+iCustomId).number(true, 0);
									$('.addConsigneeQuantity').number(true, 0);


					}



			}
			else if(sType === 'AddReceivedConsigneeItems')
			{
				var iReceivedConsignedId = oData,
					uiAddConsigneeItemGenerate = $("#addConsigneeItem"+oData),
					uiParent = uiAddConsigneeItemGenerate.closest(".product_selected_receive_consign"),
					uibBulk_set = uiParent.find(".bulk_set"),
					uiSetNewConsigneeNewRow = cr8v_platform.localStorage.get_local_storage({name:"setNewConsigneeRow"});

					var sHtml = '<div class="margin-top-10 addedReceivedConsigneeItem">'+
								'	<p class="font-bold display-inline-mid add-consignee-numbers getvaluetext'+iReceivedConsignedId+'">'+uiSetNewConsigneeNewRow.value+'</p>'+
								'	<div class="select large display-inline-mid margin-left-10 setConsigneeListSelect">'+
								'		<select class="selectConsigneeListSelect selectConsignAddQty">'+
								'		</select>'+
								'	</div>'+
								'	<p class="font-bold display-inline-mid margin-left-10">Quantity</p>'+
								'	<input type="text" class="t-small display-inline-mid width-70px margin-left-10 addConsigneeQuantity">'+
								'	<p class="display-inline-mid font-300 margin-left-10 margin-right-15 display-unit-measure-label" style="width: 10px;">KG</p> in'+
								'	<input type="text" class="t-small display-inline-mid width-70px addConsigneeBagQty qtyAddReceivedConsignee'+iReceivedConsignedId+'">'+
								'	<p class="display-inline-mid margin-left-10 font-300">Bags</p>'+
								'	<div class="display-inline-mid width-20px margin-left-10">'+
								'		<img src="../assets/images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor deleteProductConsignee">'+
								'	</div>'+
								'</div>';

						var oSetData = $(sHtml);
							oSetData.data({
								"iConsigneeId": oData
							});

							uiAddConsigneeItemGenerate.append(oSetData);
							// $("select").transformDD();

							CCReceivingConsigneeGoods.bindEvents('getAllProductsSeletedForReceiveConsignee', oSetData);

							cr8v_platform.localStorage.delete_local_storage({name:"setNewConsigneeRow"});

							$('.addConsigneeQuantity').number(true, 0);

							var uiAddConsigneeBagQty = uiParent.find(".addConsigneeBagQty");

							if(uibBulk_set.is(":checked") === true)
							{
								uiAddConsigneeBagQty.attr("disabled", true);
							}else{
								uiAddConsigneeBagQty.attr("disabled", false);
							}


			}
		}
		else if(sCurrentPath.indexOf("consignee_receiving") > -1){
			if(sType === 'displayReceivingConsigneeGoods' && Object.keys(oData).length > 0)
			{
				// console.log(JSON.stringify(oData));

				var uiDisplayAllReceiveConsigneeContainer = $("#displayAllReceivingConsigneeList"),
					uiCheckViewRecord = "";


				var uiDisplayTotalRow = $("#total-row-consignee"),
					iTotal = Object.keys(oData).length;

					uiDisplayTotalRow.html("Total Records: "+iTotal);

					

				uiDisplayAllReceiveConsigneeContainer.html("");

				for(var x in oData)
				{
					var imageGet = CGetProfileImage.getDefault(oData[x]["owner"])
						uicreateLink = "";


						if(oData[x]["origin"] == "Vessel" && oData[x]["status"] == "Complete")
						{
							uicreateLink = BASEURL+'receiving/consignee_complete_vessel';
						}else if(oData[x]["origin"] == "Vessel" && oData[x]["status"] == "Ongoing")
						{
							uicreateLink = BASEURL+'receiving/consignee_ongoing_vessel';
						}

						if(oData[x]["origin"] == "Truck" && oData[x]["status"] == "Complete")
						{
							uicreateLink = BASEURL+'receiving/consignee_complete_truck';
						}
						else if(oData[x]["origin"] == "Truck" && oData[x]["status"] == "Ongoing")
						{
							uicreateLink = BASEURL+'receiving/consignee_ongoing_truck';
						}



						if(oData[x]["status"] == "Complete" && oData[x]["status_id"] == 1)
						{
							uiCheckViewRecord = '	<div class="hover-tbl " style="height: 100%;">'+
												'		<a href="'+uicreateLink+'" class="setConsigneeReceiveId">'+
												'			<button class="btn general-btn margin-right-10">View Record</button>'+
												'		</a>'+
												'	</div>';

												// console.log("1");
						}else if(oData[x]["status"] == "Ongoing" && oData[x]["status_id"] == 2){

							uiCheckViewRecord = '	<div class="hover-tbl " style="height: 100%;">'+
												'		<a href="'+uicreateLink+'" class="setConsigneeReceiveId">'+
												'			<button class="btn general-btn margin-right-10">View Record</button>'+
												'		</a>'+
												'		<button class="btn general-btn modal-trigger show-modal-complete" data-origin="'+oData[x]["origin"]+'">Complete Receiving Record</button>'+
												'	</div>';
												// console.log("2");
						}else if(oData[x]["status"] == "Ongoing" && oData[x]["status_id"] == 0){

							uiCheckViewRecord = '	<div class="hover-tbl " style="height: 100%;">'+
												'		<a href="'+uicreateLink+'" class="setConsigneeReceiveId">'+
												'			<button class="btn general-btn margin-right-10">View Record</button>'+
												'		</a>'+
												'		<a href="'+uicreateLink+'" class="setConsigneeReceiveId">'+	
												'			<button class="btn general-btn modal-trigger">Enter Item Details</button>'+
												'		</a>'+
												'	</div>';
												// console.log("0");
						}


					var sHtml = '<div class="tbl-like hover-transfer-tbl displayReceiveConsignRow">'+
								'	<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">'+
								'		<div class="display-inline-mid text-center width-10percent text-left">'+
								'			<p class=" font-400 font-14 f-none text-center no-padding-left width-100percent ">'+oData[x]["receiving_number"]+'</p>'+					
								'		</div>'+
								'		<div class="display-inline-mid text-center width-10percent">'+
								'			<p class=" font-400 font-14 f-none width-100percent no-padding-left">'+oData[x]["purchase_order"]+'</p>'+
								'		</div>'+
								'		<div class="display-inline-mid text-center width-10percent ">'+
								'			<p class=" font-400 font-14 f-none width-100percent no-padding-left">'+oData[x]["issued_date"]+'</p>'+
								'		</div>'+
								'		<div class="display-inline-mid text-center width-10percent ">'+
								'			<p class=" font-400 font-14 f-none width-100percent no-padding-left">'+oData[x]["origin"]+'</p>'+	
								'		</div>'+
								'		<div class=" display-inline-mid font-400 text-center width-30percent">'+
								'			<ul class=" text-left text-indent-20 padding-left-50 font-14 width-100percent" id="displayProductHere'+oData[x]["receiving_id"]+'">';										
											
										for(var i in oData[x]["prducts"])
										{
											sHtml +=  '<li>#'+oData[x]["prducts"][i]["sku"]+' - '+oData[x]["prducts"][i]["name"]+'</li>';
											
										}	

						sHtml +=	'	</ul>'+
								'		</div>'+
								'		<div class="font-400 display-inline-mid text-center width-20percent ">'+
								'			<div class="owner-img width-30per display-inline-mid">'+
								'				<img src="'+imageGet+'" alt="Profile Owner" class="width-60percent">'+
								'			</div>'+
								'			<p class="width-100px font-400 font-14 display-inline-mid f-none">'+oData[x]["owner"]+'</p>	'+							
								'		</div>'+
								'		<div class="display-inline-mid text-center width-10percent">'+
								'			<p class=" font-400 font-14 f-none width-100percent">'+oData[x]["status"]+'</p>'+						
								'		</div>'+
								'	</div>'+
									uiCheckViewRecord+
								'</div>';
								// '+uicreateLink+'
								

							var oSetData = $(sHtml);
								oSetData.data({
									"receivingId":oData[x]["receiving_id"],
									"departure_date":oData[x]["departure_date"],
									"departure_time":oData[x]["departure_time"],
								});
								uiDisplayAllReceiveConsigneeContainer.append(oSetData);

							CCReceivingConsigneeGoods.bindEvents("setConsigneeGoodsId", oSetData);


							_receivingListAddCompleteRecord();


							setTimeout(function(){
								$('#generated-data-view').removeClass('showed');
							}, 300);
							

				}
			}
		}
		else if(sCurrentPath.indexOf("consignee_ongoing_vessel") > -1)
		{
			if(sType === 'displaySelectedConsigneeGoodsVessel' && Object.keys(oData).length > 0)
			{
				// _documentEvents();
				//console.log(JSON.stringify(oData))
				// console.log(JSON.stringify(oData["receiving_documents"]));

				var uiDisplaySelectedReceiveNumber = $("#displaySelectedReceiveNumberTemp"),
					uiDisplaySelectedPONum = $("#displaySelectedPONumTemp"),
					uiDisplaySelectedDateIssued = $("#displaySelectedDateIssuedTemp"),
					uiDisplaySelectedVesselName = $("#display-selected-vessel-name-temp"),
					uiDisplaySelectedVesselType = $("#display-selected-vessel-type-temp"),
					uiDisplaySelectedVesselCaptain = $("#display-selected-vessel-captain-temp"),
					uiDisplaySelectedVesselHatches = $("#display-selected-vessel-hatches-temp"),
					uiDisplaySelectedVesselDischargeType = $("#display-selected-vessel-discharge-type-temp"),
					uiDisplaySelectedVesselBillLoadingVol = $("#display-selected-vessel-bill-loading-volume-temp"),
					uiDisplaySelectedVesselBertingTimeStart = $("#display-selected-vessel-berthing-time-start-temp"),
					uiDisplaySelectedVesselBertingDateStart = $("#display-selected-vessel-berthing-date-start-temp"),
					uiDisplaySelectedVesselBertingTime = $("#display-selected-vessel-berthing-time-temp");


				var sPoNum = '',
					sBerthingDateTime = "";

					(oData["purchase_order"] != "") ? sPoNum = oData["purchase_order"] : sPoNum = "None";

					if(oData["berthing_time"] != ""){
						sBerthingDateTime = oData["berthing_time"];
						uiDisplaySelectedVesselBertingTime.removeClass("gray-color");
						$("#display-selected-vessel-berthing-label").removeClass("gray-color");
					}else{
						sBerthingDateTime = "Not Yet Assigned";
					}
					

					uiDisplaySelectedReceiveNumber.html("Receiving No. "+oData["receiving_number"]);
					uiDisplaySelectedPONum.html(sPoNum);
					uiDisplaySelectedDateIssued.html(oData["date_issued"]);
					uiDisplaySelectedVesselName.html(oData["vessel_name"]);
					uiDisplaySelectedVesselType.html(oData["vessel_type"]);
					uiDisplaySelectedVesselCaptain.html(oData["vessel_captain"]);
					uiDisplaySelectedVesselHatches.html(oData["hatches"]);
					uiDisplaySelectedVesselDischargeType.html(oData["discharge_type"]);
					uiDisplaySelectedVesselBillLoadingVol.html(oData["bill_of_landing_volume"]+" "+oData["bill_measure"].toUpperCase());
					uiDisplaySelectedVesselBertingTime.html(sBerthingDateTime);
					uiDisplaySelectedVesselBertingTimeStart.val(oData["berthing_time_set"]);
					uiDisplaySelectedVesselBertingDateStart.val(oData["berthing_time_date"]);

					_displayProductItems(oData["product_info"]);

					_dispalyReceivingNotes(oData["receiving_notes"]);

					_displayDocuments(oData["receiving_documents"]);

					CDropDownRetract.retractDropdown($(".set-discrepancy-cause"));

			}

		}
		else if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1)
		{
			if(sType === 'displaySelectedConsigneeGoodsTruck' && Object.keys(oData).length > 0)
			{
				// console.log(JSON.stringify(oData));
				

				var uiDisplaySelectedReceiveNumber = $("#displaySelectedReceiveNumberTruckTemp"),
					uiDisplaySelectedPONum = $("#displaySelectedPONumTruckTemp"),
					uiDisplaySelectedDateIssued = $("#displaySelectedDateIssuedTruckTemp"),
					uiDisplaySelectedVesselOrigin = $("#display-selected-vessel-name-temp"),
					uiDisplaySelectedVesselTrucking = $("#display-selected-trucking-temp"),
					uiDisplaySelectedVesselDriversName= $("#display-selected-driver-name-temp"),
					uiDisplaySelectedVesselLicense = $("#display-selected-license-name-temp"),
					uiDisplaySelectedVesselPlateNumber = $("#display-selected-plate-number-temp");


					var sPoNum = '';

					(oData["purchase_order"] != "") ? sPoNum = oData["purchase_order"] : sPoNum = "None";
					

					uiDisplaySelectedReceiveNumber.html("Receiving No. "+oData["receiving_number"]);
					uiDisplaySelectedPONum.html(sPoNum);
					uiDisplaySelectedDateIssued.html(oData["date_issued"]);
					uiDisplaySelectedVesselOrigin.html(oData["vessel_name"]);
					uiDisplaySelectedVesselTrucking.html(oData["trucking"]);
					uiDisplaySelectedVesselDriversName.html(oData["driver_name"]);
					uiDisplaySelectedVesselLicense.html(oData["license_name"]);
					uiDisplaySelectedVesselPlateNumber.html(oData["plate_number"]);

					_displayProductItems(oData["product_info"]);

					_dispalyReceivingNotes(oData["receiving_notes"]);

					_displayDocuments(oData["receiving_documents"]);

					CDropDownRetract.retractDropdown($(".set-discrepancy-cause"));
			}
		}
		else if(sCurrentPath.indexOf("consignee_edit_vessel") > -1)
		{

			if(sType === 'displayEditSelectedConsigneeGoodsVessel' && Object.keys(oData).length > 0)
			{

				// console.log(JSON.stringify(oData));
				var uiDisplayEditConsigneeVesselReceivingNumHead = $("#display-edit-consignee-vessel-receivingNum-head"),
				uiDisplayEditConsigneeVesselReceivingNum = $("#display-edit-consignee-vessel-receivingNum"),
				uiDisplayEditConsigneeVesselPoNum = $("#display-edit-consignee-vessel-poNum"),
				uiDisplayEditConsigneeVesselName = $("#display-edit-vessel-name"),
				uiDisplayEditConsigneeVesselHatches = $("#display-edit-vessel-hatches"),
				uiDisplayEditConsigneeVesselType = $("#display-edit-vessel-type"),
				uiDisplayEditConsigneeVesselCaptain = $("#display-edit-vessel-captain"),
				uiDisplayEditConsigneeVesselDischargeType = $("#display-edit-vessel-discharge-type"),
				uiDisplayEditConsigneeVesselBillOfLanding = $("#display-edit-vessel-bill-of-landing"),
				uiDisplayEditConsigneeVesselBerthingDate = $("#display-edit-vessel-berthing-date"),
				uiDisplayEditConsigneeVesselBerthingTime = $("#display-edit-vessel-berthing-time");

				uiDisplayEditConsigneeVesselReceivingNumHead.html("Receiving No. "+oData["receiving_number"]);
				uiDisplayEditConsigneeVesselReceivingNum.html("Receiving No. "+oData["receiving_number"]);
				uiDisplayEditConsigneeVesselPoNum.val(oData["purchase_order"]);
				uiDisplayEditConsigneeVesselName.html(oData["vessel_name"]);
				uiDisplayEditConsigneeVesselHatches.val(oData["hatches"]);
				uiDisplayEditConsigneeVesselType.val(oData["vessel_type"]);
				uiDisplayEditConsigneeVesselCaptain.val(oData["vessel_captain"]);
				uiDisplayEditConsigneeVesselDischargeType.val(oData["discharge_type"]);
				uiDisplayEditConsigneeVesselBillOfLanding.val(oData["bill_of_landing_volume"]);
				uiDisplayEditConsigneeVesselBerthingDate.val(oData["berthing_time_date"]);
				uiDisplayEditConsigneeVesselBerthingTime.val(oData["berthing_time_set"]);

				_displayEditProductItems(oData["product_info"]);

				_buildUnitOfMeasureEditVessel(oData["bill_measure"]);


				// console.log(JSON.stringify(oData["product_info"]));
			}
			else if(sType === 'displayItemsForProductList')
			{
				var oSetDataPoductListReceivingConsignee = oData,
					uiSetProductListSelect = $("#selectProductListSelect"),
					uiParent = uiSetProductListSelect.parent(),
					uiSibling = uiParent.find('.frm-custom-dropdown');

					uiSibling.remove();
					uiSetProductListSelect.removeClass("frm-custom-dropdown-origin");



					var sHtml = '<option value="">Select Item</option>';

					for(var x in oSetDataPoductListReceivingConsignee)
					{
						sHtml = '<option value="'+oSetDataPoductListReceivingConsignee[x]['id']+'">'+oSetDataPoductListReceivingConsignee[x]['name']+'</option>';
						uiSetProductListSelect.append(sHtml);
					}

					uiSetProductListSelect.transformDD();

			}
			else if(sType === 'displayEditSelectedProductForReceivingConsignee' && Object.keys(oData).length > 0)
			{

				var uiDisplayEditReceivingConsigneeProductSelects = $("#displayEditReceivingConsigneeProductSelects"),
					sDefaultImg = "",
					iCountConsignee = 1;

					// uiDisplayEditReceivingConsigneeProductSelects.html("");
					// console.log(JSON.stringify(oData));
					
					for(var x in oData)
					{


						// console.log(oData[x]["id"]);
						(oData[x]["image"] != "") ? sDefaultImg = oData[x]["image"] : sDefaultImg = DEFAULT_PROD_IMG;
						var iCustomId = oData[x]["id"];

							var sHtml ='<div class="border-full padding-all-20 margin-top-10 product_selected_receive_consign">'+
										'	<div class="bggray-white">'+
										'		<div class="height-100percent display-inline-mid width-230px">'+
										'			<img src="'+sDefaultImg+'" alt="" class="height-100percent width-100percent">'+
										'		</div>'+
										'		<div class="display-inline-top width-75percent padding-left-10">'+
										'			<div class="padding-all-15 bg-light-gray">'+
										'				<p class="font-16 font-bold f-left">SKU# '+oData[x]["sku"]+' - '+oData[x]["name"]+'</p>'+
										'				<div class="f-right width-20px margin-left-10">'+
										'					<img  src="'+BASEURL+'assets/images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor deleteProd">'+
										'				</div>'+
										'				<div class="clear"></div>'+
										'			</div>'+
										'				<div class="padding-all-15 text-left">'+
										'					<p class="f-left no-margin-all width-15percent font-bold">Vendor</p>'+
										'					<p class="f-left no-margin-all width-25percent"><input type="text" id="addReceiveConsigneeVendor'+iCustomId+'" datavalid="required"  class="display-inline-mid width-150px height-26 t-small receiveConsigneeVendorItem"></p>'+
										'					<p class="f-left no-margin-all width-19percent font-bold">Loading Method</p>'+
										'					<div class="f-left  margin-left-10" >'+
										'						<input type="radio" value="Bulk" class="display-inline-mid width-20px default-cursor bulk_set"  name="bag-or-bulk'+iCustomId+'" id="bulk'+iCustomId+'">'+
										'						<label for="bulk'+iCustomId+'" class="display-inline-mid font-14 margin-top-5 default-cursor ">By Bulk</label>'+
										'					</div>'+
										//'					<div class="f-left margin-left-50">'+															
										//'						<input type="radio" value="Bags" class="display-inline-mid width-50px default-cursor bag_set" checked="checked" name="bag-or-bulk'+iCustomId+'" id="piece'+iCustomId+'">'+
										//'						<label for="piece'+iCustomId+'" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bags</label>'+
										//'					</div>'+
										'					<div class="clear"></div>'+
										'				</div>'+
										'				<div class="padding-all-15 bg-light-gray text-left font-0">'+
										'					<p class="display-inline-mid width-15percent font-bold">Shelf Life</p>'+
										'					<p class="display-inline-mid width-25percent">'+oData[x]["product_shelf_life"]+'</p>'+
										'					<p class="display-inline-mid width-20per font-14 font-bold">Qty to Receive</p>'+								
										'					<input type="text" id="qtyReceiveKg'+iCustomId+'" datavalid="required" class="display-inline-mid width-70px height-26 t-small qty-received" name="bag-or-bulk">'+
										'					<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10"><select class="unit-of-measure" id="unit-measurement'+iCustomId+'"></select> in</p>'+
										'					<input type="text" id="qtyReceiveg'+iCustomId+'" class="display-inline-mid width-70px height-26 t-small qty-bags"" name="bag-or-bulk">'+
										'					<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">Bags</p>'+
										'					<div class="clear"></div>'+
										'				</div>'+
										'			<div class="padding-all-15 text-left">'+
										'				<p class="f-left  font-bold">Consignee Distribution:</p>'+								
										'				<div class="clear"></div>'+
										'			</div>'+
										'			<div class="padding-all-15 text-left bg-light-gray addConsigneeItemAddNumber" id="addConsigneeItem'+iCustomId+'">'+
										'				<div class="margin-top-10 addedReceivedConsigneeItem" >'+
										'					<p class="font-bold display-inline-mid add-consignee-numbers getvaluetext'+iCustomId+'">1</p>'+
										'					<div class="select large display-inline-mid margin-left-10 setConsigneeListSelect">'+
										'						<select class="selectConsigneeListSelect selectConsignAddQty">'+
										'						</select>'+
										'					</div>'+
										'					<p class="font-bold display-inline-mid margin-left-10">Quantity</p>'+
										'					<input type="text" class="t-small display-inline-mid width-70px margin-left-10 addConsigneeQuantity">'+
										'					<p class="display-inline-mid font-300 margin-left-10 margin-right-15 display-unit-measure-label" style="width: 10px;">KG</p> in'+
										'					<input type="text" class="t-small display-inline-mid width-70px addConsigneeBagQty qtyAddReceivedConsignee'+iCustomId+'">'+
										'					<p class="display-inline-mid margin-left-10 font-300">Bags</p>'+
										'					<div class="display-inline-mid width-20px margin-left-10">'+
										'						<img src="../assets/images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor deleteProductConsignee">'+
										'					</div>'+
										'				</div>'+									
										'			</div>'+
										'			<a href="" class="f-right margin-top-10 font-500 addConsigneeRow" >+ Add Consignee</a>'+
										'			<div class="clear"></div>'+
										'		</div>'+
										'	</div>'+
										'</div>';

								var oSetData = $(sHtml);
									oSetData.data({
										"ProdId": oData[x]["id"]
									});
										
									oSetData.find(".bulk_set").attr("checked", true);

									uiDisplayEditReceivingConsigneeProductSelects.append(oSetData);

									



									CCReceivingConsigneeGoods.bindEvents('getEditAllProductsSeletedForReceiveConsignee', oSetData);

									$('#qtyReceiveKg'+iCustomId).number(true, 0);
									$('.addConsigneeQuantity').number(true, 0);


					}



			}
			else if(sType === 'AddReceivedConsigneeItems')
			{
				var iReceivedConsignedId = oData,
					uiAddConsigneeItemGenerate = $("#addConsigneeItem"+oData),
					uiParent = uiAddConsigneeItemGenerate.closest(".product_selected_receive_consign"),
					uibBulk_set = uiParent.find(".bulk_set"),
					uiSetNewConsigneeNewRow = cr8v_platform.localStorage.get_local_storage({name:"setNewConsigneeRow"});

					var sHtml = '<div class="margin-top-10 addedReceivedConsigneeItem">'+
								'	<p class="font-bold display-inline-mid add-consignee-numbers getvaluetext'+iReceivedConsignedId+'">'+uiSetNewConsigneeNewRow.value+'</p>'+
								'	<div class="select large display-inline-mid margin-left-10 setConsigneeListSelect">'+
								'		<select class="selectConsigneeListSelect selectConsignAddQty">'+
								'		</select>'+
								'	</div>'+
								'	<p class="font-bold display-inline-mid margin-left-10">Quantity</p>'+
								'	<input type="text" class="t-small display-inline-mid width-70px margin-left-10 addConsigneeQuantity">'+
								'	<p class="display-inline-mid font-300 margin-left-10 margin-right-15 display-unit-measure-label" style="width: 10px;">KG</p> in'+
								'	<input type="text" class="t-small display-inline-mid width-70px addConsigneeBagQty qtyAddReceivedConsignee'+iReceivedConsignedId+'">'+
								'	<p class="display-inline-mid margin-left-10 font-300">Bags</p>'+
								'	<div class="display-inline-mid width-20px margin-left-10">'+
								'		<img src="../assets/images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor deleteProductConsignee">'+
								'	</div>'+
								'</div>';

						var oSetData = $(sHtml);
							oSetData.data({
								"iConsigneeId": oData
							});

							uiAddConsigneeItemGenerate.append(oSetData);
							// $("select").transformDD();

							CCReceivingConsigneeGoods.bindEvents('getEditAllProductsSeletedForReceiveConsignee', oSetData);

							cr8v_platform.localStorage.delete_local_storage({name:"setNewConsigneeRow"});

							$('.addConsigneeQuantity').number(true, 0);

							var uiAddConsigneeBagQty = uiParent.find(".addConsigneeBagQty");

							if(uibBulk_set.is(":checked") === true)
							{
								uiAddConsigneeBagQty.attr("disabled", true);
							}else{
								uiAddConsigneeBagQty.attr("disabled", false);
							}



			}
			else if(sType === 'displayEditItemsForVesselList')
			{
				var interval = {};

				interval = setInterval(function(){
					
					var oSetDataVesselListReceivingConsignee = cr8v_platform.localStorage.get_local_storage({name:"getAllVesselData"});
					
					if(oSetDataVesselListReceivingConsignee !== null){
						var oGetAllDataReceiving = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"}),
							uiSetVesselListSelect = $("#display-edit-vessel-name"),
							uiParent = uiSetVesselListSelect.parent(),
							uiSibling = uiParent.find('.frm-custom-dropdown');

						uiSibling.remove();
						uiSetVesselListSelect.removeClass("frm-custom-dropdown-origin");

						// var sHtml = '<option value="">Select Vessel</option>';

						if(oSetDataVesselListReceivingConsignee === 0){
							sHtml ='<option value="">No Vessels Found</option>';
							uiSetVesselListSelect.append(sHtml);
						}else{
							for(var x in oSetDataVesselListReceivingConsignee)
							{
								if(oSetDataVesselListReceivingConsignee[x]['name'] ==  oGetAllDataReceiving["vessel_name"]){
									sHtml += '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'" selected>'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
								}

								sHtml += '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'">'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
								uiSetVesselListSelect.append(sHtml);
							}

						}

						uiSetVesselListSelect.transformDD();

						CDropDownRetract.retractDropdown(uiSetVesselListSelect);

						clearInterval(interval);	
					}

				},300);
				
				
				
			}
			else if(sType === 'displayEditItemsForVesselListForTruck')
			{


				var intervalss = {};

				intervalss = setInterval(function(){
					
					var oSetDataVesselListReceivingConsignee = cr8v_platform.localStorage.get_local_storage({name:"getAllVesselData"});
					
					if(oSetDataVesselListReceivingConsignee !== null){
						var oGetAllDataReceiving = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"}),
							uiSetVesselListSelect = $("#display-edit-truck-name"),
							uiParent = uiSetVesselListSelect.parent(),
							uiSibling = uiParent.find('.frm-custom-dropdown');

						uiSibling.remove();
						uiSetVesselListSelect.removeClass("frm-custom-dropdown-origin");

						// console.log(JSON.stringify(oSetDataVesselListReceivingConsignee));

						// var sHtml = '<option value="">Select Vessel</option>';

						if(oSetDataVesselListReceivingConsignee === 0){
							sHtml ='<option value="">No Vessels Found</option>';
							uiSetVesselListSelect.append(sHtml);
						}else{
							for(var x in oSetDataVesselListReceivingConsignee)
							{
								if(oSetDataVesselListReceivingConsignee[x]['name'] ==  oGetAllDataReceiving["vessel_name"]){
									sHtml += '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'" selected>'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
								}

								sHtml += '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'">'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
								uiSetVesselListSelect.append(sHtml);
							}

						}

						uiSetVesselListSelect.transformDD();

						CDropDownRetract.retractDropdown(uiSetVesselListSelect);

						clearInterval(intervalss);	
					}

				},300);
				
				
// 				var oSetDataVesselListReceivingConsignee = cr8v_platform.localStorage.get_local_storage({name:"getAllVesselName"}),
// 					uiSetVesselListSelect = $("#display-edit-truck-name"),
// 					uiParent = uiSetVesselListSelect.parent(),
// 					uiSibling = uiParent.find('.frm-custom-dropdown');

// 					uiSibling.remove();
// 					uiSetVesselListSelect.removeClass("frm-custom-dropdown-origin");



// 					var sHtml = '<option value="">Select Vessel</option>';

// 					if(oSetDataVesselListReceivingConsignee === 0){
// 						sHtml ='<option value="">No Vessels Found</option>';
// 						uiSetVesselListSelect.append(sHtml);
// 					}else{
// 						for(var x in oSetDataVesselListReceivingConsignee)
// 						{
// 							sHtml = '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'">'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
// 							uiSetVesselListSelect.append(sHtml);
// 						}

// 					}



					
// 					uiSetVesselListSelect.transformDD();

			}
		}
		else if(sCurrentPath.indexOf("consignee_edit_truck") > -1)
		{
			if(sType === 'displayEditSelectedConsigneeGoodsTruck' && Object.keys(oData).length > 0)
			{

				// console.log(JSON.stringify(oData));
				var uiDisplayEditConsigneeTruckReceivingNumHead = $("#display-edit-consignee-truck-receivingNum-head"),
					uiDisplayEditConsigneeTruckReceivingNum = $("#display-edit-consignee-truck-receivingNum"),
					uiDisplayEditConsigneeTrucklPoNum = $("#display-edit-consignee-truck-poNum"),
					uiDisplayEditConsigneeTrucklTrucking = $("#display-edit-consignee-truck-trucking"),
					uiDisplayEditConsigneeTrucklPlateNumber = $("#display-edit-consignee-truck-plate-number"),
					uiDisplayEditConsigneeTrucklDriverName = $("#display-edit-consignee-truck-driver-name"),
					uiDisplayEditConsigneeTruckName = $("#isplay-edit-truck-name"),
					uiDisplayEditConsigneeTrucklLicenseNumber = $("#display-edit-consignee-truck-license-number");

				
					uiDisplayEditConsigneeTruckReceivingNumHead.html("Receiving No. "+oData["receiving_number"]);
					uiDisplayEditConsigneeTruckReceivingNum.html("Receiving No. "+oData["receiving_number"]);
					uiDisplayEditConsigneeTrucklPoNum.val(oData["purchase_order"]);
					uiDisplayEditConsigneeTrucklTrucking.val(oData["trucking"]);
					uiDisplayEditConsigneeTrucklPlateNumber.val(oData["plate_number"]);
					uiDisplayEditConsigneeTrucklDriverName.val(oData["driver_name"]);
					uiDisplayEditConsigneeTrucklLicenseNumber.val(oData["license_name"]);

					uiDisplayEditConsigneeTruckName.val(oData["vessel_name"])


				_displayEditProductItems(oData["product_info"]);

				_displayDocuments(oData["receiving_documents"]);
				// console.log(JSON.stringify(oData["product_info"]));
			}
			else if(sType === 'displayEditItemsForVesselListForTruck')
			{

				var intervalss = {};

				intervalss = setInterval(function(){
					
					var oSetDataVesselListReceivingConsignee = cr8v_platform.localStorage.get_local_storage({name:"getAllVesselData"});
					
					if(oSetDataVesselListReceivingConsignee !== null){
						var oGetAllDataReceiving = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"}),
							uiSetVesselListSelect = $("#display-edit-truck-name"),
							uiParent = uiSetVesselListSelect.parent(),
							uiSibling = uiParent.find('.frm-custom-dropdown');

						uiSibling.remove();
						uiSetVesselListSelect.removeClass("frm-custom-dropdown-origin");


						if(oSetDataVesselListReceivingConsignee === 0){
							sHtml ='<option value="">No Vessels Found</option>';
							uiSetVesselListSelect.append(sHtml);
						}else{
							for(var x in oSetDataVesselListReceivingConsignee)
							{
								if(oSetDataVesselListReceivingConsignee[x]['name'] ==  oGetAllDataReceiving["vessel_name"]){
									sHtml += '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'" selected>'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
								}

								sHtml += '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'">'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
								uiSetVesselListSelect.append(sHtml);
							}

						}

						uiSetVesselListSelect.transformDD();

						CDropDownRetract.retractDropdown(uiSetVesselListSelect);

						clearInterval(intervalss);	
					}

				},300);
				
				
				// var oSetDataVesselListReceivingConsignee = cr8v_platform.localStorage.get_local_storage({name:"getAllVesselName"})
				// 	oGetAllDataReceiving = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"}),
				// 	uiSetVesselListSelect = $("#display-edit-truck-name"),
				// 	uiParent = uiSetVesselListSelect.parent(),
				// 	uiSibling = uiParent.find('.frm-custom-dropdown');

				// 	uiSibling.remove();
				// 	uiSetVesselListSelect.removeClass("frm-custom-dropdown-origin");



				// 	var sHtml = '<option value="">Select Vessel</option>';

				// 	if(oSetDataVesselListReceivingConsignee === 0){
				// 		sHtml ='<option value="">No Vessels Found</option>';
				// 		uiSetVesselListSelect.append(sHtml);
				// 	}else{
				// 		for(var x in oSetDataVesselListReceivingConsignee)
				// 		{
				// 			if(oSetDataVesselListReceivingConsignee[x]['name'] ==  oGetAllDataReceiving["vessel_name"]){
				// 				sHtml += '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'" selected>'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
				// 			}

				// 			sHtml += '<option value="'+oSetDataVesselListReceivingConsignee[x]['id']+'">'+oSetDataVesselListReceivingConsignee[x]['name']+'</option>';
				// 			uiSetVesselListSelect.append(sHtml);
				// 		}

				// 	}




					
				// 	uiSetVesselListSelect.transformDD();

			}
			else if(sType === 'displayItemsForProductListTruck')
			{
				var oSetDataPoductListReceivingConsignee = oData,
					uiSetProductListSelect = $("#selectProductListSelect"),
					uiParent = uiSetProductListSelect.parent(),
					uiSibling = uiParent.find('.frm-custom-dropdown');

					uiSibling.remove();
					uiSetProductListSelect.removeClass("frm-custom-dropdown-origin");



					var sHtml = '<option value="">Select Item</option>';

					for(var x in oSetDataPoductListReceivingConsignee)
					{
						sHtml = '<option value="'+oSetDataPoductListReceivingConsignee[x]['id']+'">'+oSetDataPoductListReceivingConsignee[x]['name']+'</option>';
						uiSetProductListSelect.append(sHtml);
					}

					uiSetProductListSelect.transformDD();

			}
			else if(sType === 'displayEditSelectedProductForReceivingConsignee' && Object.keys(oData).length > 0)
			{

				var uiDisplayEditReceivingConsigneeProductSelects = $("#displayEditReceivingConsigneeProductSelects"),
					sDefaultImg = "",
					iCountConsignee = 1;

					uiDisplayEditReceivingConsigneeProductSelects.html("");
					// console.log(JSON.stringify(oData));
					
					for(var x in oData)
					{


						// console.log(oData[x]["id"]);
						(oData[x]["image"] != "") ? sDefaultImg = oData[x]["image"] : sDefaultImg = DEFAULT_PROD_IMG;
						var iCustomId = oData[x]["id"];

							var sHtml ='<div class="border-full padding-all-20 margin-top-10 product_selected_receive_consign">'+
										'	<div class="bggray-white">'+
										'		<div class="height-100percent display-inline-mid width-230px">'+
										'			<img src="'+sDefaultImg+'" alt="" class="height-100percent width-100percent">'+
										'		</div>'+
										'		<div class="display-inline-top width-75percent padding-left-10">'+
										'			<div class="padding-all-15 bg-light-gray">'+
										'				<p class="font-16 font-bold f-left">SKU# '+oData[x]["sku"]+' - '+oData[x]["name"]+'</p>'+
										'				<div class="f-right width-20px margin-left-10">'+
										'					<img  src="'+BASEURL+'assets/images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor deleteProd">'+
										'				</div>'+
										'				<div class="clear"></div>'+
										'			</div>'+
										'				<div class="padding-all-15 text-left">'+
										'					<p class="f-left no-margin-all width-15percent font-bold">Vendor</p>'+
										'					<p class="f-left no-margin-all width-25percent"><input type="text" id="addReceiveConsigneeVendor'+iCustomId+'" datavalid="required"  class="display-inline-mid width-150px height-26 t-small receiveConsigneeVendorItem"></p>'+
										'					<p class="f-left no-margin-all width-19percent font-bold">Loading Method</p>'+
										'					<div class="f-left  margin-left-10" >'+
										'						<input type="radio" value="Bulk" class="display-inline-mid width-20px default-cursor bulk_set" name="bag-or-bulk'+iCustomId+'" id="bulk'+iCustomId+'">'+
										'						<label for="bulk'+iCustomId+'" class="display-inline-mid font-14 margin-top-5 default-cursor ">By Bulk</label>'+
										'					</div>'+
										'					<div class="f-left margin-left-50">'+															
										'						<input type="radio" value="Bags" class="display-inline-mid width-50px default-cursor bag_set" checked="checked" name="bag-or-bulk'+iCustomId+'" id="piece'+iCustomId+'">'+
										'						<label for="piece'+iCustomId+'" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bags</label>'+
										'					</div>'+
										'					<div class="clear"></div>'+
										'				</div>'+
										'				<div class="padding-all-15 bg-light-gray text-left font-0">'+
										'					<p class="display-inline-mid width-15percent font-bold">Shelf Life</p>'+
										'					<p class="display-inline-mid width-25percent">'+oData[x]["product_shelf_life"]+'</p>'+
										'					<p class="display-inline-mid width-20per font-14 font-bold">Qty to Receive</p>'+								
										'					<input type="text" id="qtyReceiveKg'+iCustomId+'" datavalid="required" class="display-inline-mid width-70px height-26 t-small qty-received" name="bag-or-bulk">'+
										'					<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10"><select class="unit-of-measure" id="unit-measurement'+iCustomId+'"></select> in</p>'+
										'					<input type="text" id="qtyReceiveg'+iCustomId+'" class="display-inline-mid width-70px height-26 t-small qty-bags"" name="bag-or-bulk">'+
										'					<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">Bags</p>'+
										'					<div class="clear"></div>'+
										'				</div>'+
										'			<div class="padding-all-15 text-left">'+
										'				<p class="f-left  font-bold">Consignee Distribution:</p>'+								
										'				<div class="clear"></div>'+
										'			</div>'+
										'			<div class="padding-all-15 text-left bg-light-gray addConsigneeItemAddNumber" id="addConsigneeItem'+iCustomId+'">'+
										'				<div class="margin-top-10 addedReceivedConsigneeItem" >'+
										'					<p class="font-bold display-inline-mid add-consignee-numbers getvaluetext'+iCustomId+'">1</p>'+
										'					<div class="select large display-inline-mid margin-left-10 setConsigneeListSelect">'+
										'						<select class="selectConsigneeListSelect selectConsignAddQty">'+
										'						</select>'+
										'					</div>'+
										'					<p class="font-bold display-inline-mid margin-left-10">Quantity</p>'+
										'					<input type="text" class="t-small display-inline-mid width-70px margin-left-10 addConsigneeQuantity">'+
										'					<p class="display-inline-mid font-300 margin-left-10 margin-right-15 display-unit-measure-label" style="width: 10px;">KG</p> in'+
										'					<input type="text" class="t-small display-inline-mid width-70px addConsigneeBagQty qtyAddReceivedConsignee'+iCustomId+'">'+
										'					<p class="display-inline-mid margin-left-10 font-300">Bags</p>'+
										'					<div class="display-inline-mid width-20px margin-left-10">'+
										'						<img src="../assets/images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor deleteProductConsignee">'+
										'					</div>'+
										'				</div>'+									
										'			</div>'+
										'			<a href="" class="f-right margin-top-10 font-500 addConsigneeRow" >+ Add Consignee</a>'+
										'			<div class="clear"></div>'+
										'		</div>'+
										'	</div>'+
										'</div>';

								var oSetData = $(sHtml);
									oSetData.data({
										"ProdId": oData[x]["id"]
									});
									uiDisplayEditReceivingConsigneeProductSelects.append(oSetData);

									CCReceivingConsigneeGoods.bindEvents('getEditAllProductsSeletedForReceiveConsigneeTruck', oSetData);

									$('#qtyReceiveKg'+iCustomId).number(true, 0);
									$('.addConsigneeQuantity').number(true, 0);



					}
			}
			else if(sType === 'AddReceivedConsigneeItems')
			{
				var iReceivedConsignedId = oData,
					uiAddConsigneeItemGenerate = $("#addConsigneeItem"+oData),
					uiParent = uiAddConsigneeItemGenerate.closest(".product_selected_receive_consign"),
					uibBulk_set = uiParent.find(".bulk_set"),
					uiSetNewConsigneeNewRow = cr8v_platform.localStorage.get_local_storage({name:"setNewConsigneeRow"});

					var sHtml = '<div class="margin-top-10 addedReceivedConsigneeItem">'+
								'	<p class="font-bold display-inline-mid add-consignee-numbers getvaluetext'+iReceivedConsignedId+'">'+uiSetNewConsigneeNewRow.value+'</p>'+
								'	<div class="select large display-inline-mid margin-left-10 setConsigneeListSelect">'+
								'		<select class="selectConsigneeListSelect selectConsignAddQty">'+
								'		</select>'+
								'	</div>'+
								'	<p class="font-bold display-inline-mid margin-left-10">Quantity</p>'+
								'	<input type="text" class="t-small display-inline-mid width-70px margin-left-10 addConsigneeQuantity">'+
								'	<p class="display-inline-mid font-300 margin-left-10 margin-right-15 display-unit-measure-label" style="width: 10px;">KG</p> in'+
								'	<input type="text" class="t-small display-inline-mid width-70px addConsigneeBagQty qtyAddReceivedConsignee'+iReceivedConsignedId+'">'+
								'	<p class="display-inline-mid margin-left-10 font-300">Bags</p>'+
								'	<div class="display-inline-mid width-20px margin-left-10">'+
								'		<img src="../assets/images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor deleteProductConsignee">'+
								'	</div>'+
								'</div>';

						var oSetData = $(sHtml);
							oSetData.data({
								"iConsigneeId": oData
							});

							uiAddConsigneeItemGenerate.append(oSetData);
							// $("select").transformDD();

							CCReceivingConsigneeGoods.bindEvents('getEditAllProductsSeletedForReceiveConsigneeTruck', oSetData);

							cr8v_platform.localStorage.delete_local_storage({name:"setNewConsigneeRow"});

							$('.addConsigneeQuantity').number(true, 0);

							var uiAddConsigneeBagQty = uiParent.find(".addConsigneeBagQty");

							if(uibBulk_set.is(":checked") === true)
							{
								uiAddConsigneeBagQty.attr("disabled", true);
							}else{
								uiAddConsigneeBagQty.attr("disabled", false);
							}

						


			}
		}
		else if(sCurrentPath.indexOf("consignee_complete_truck") > -1)
		{
			if(sType === 'displayCompletedConsigneeGoodsTruck' && Object.keys(oData).length > 0)
			{
				//console.log(JSON.stringify(oData));

				var uiDisplayCompleteReceivingNumber = $("#display-complete-receiving-number"),
					uidDisplayCompleteReceivingPurchaseOrder = $("#display-complete-receiving-purchase-order"),
					uiDisplayCompleteReceivingDateIssued = $("#display-complete-receiving-date-issued"),
					uiDisplayCompleteReceivingVesselName = $("#display-complete-receiving-vessel-name"),
					uiDisplayCompleteReceivingTrucking = $("#display-complete-receiving-trucking"),
					uiDisplayCompleteReceivingDriverName = $("#display-complete-receiving-driver-name"),
					uiDisplayCompleteReceivingLicenseNumber = $("#display-complete-receiving-license-number"),
					uiDisplayCompleteReceivingPlateNumber = $("#display-complete-receiving-plate-number"),
					uiDisplayCompleteReceivingTareIn = $("#display-complete-receiving-tare-in"),
					uiDisplayCompleteReceivingNTareOut = $("#display-complete-receiving-tare-out"),
					uiDisplayCompleteReceivingNetWeight = $("#display-complete-receiving-net-weight"),
					sPONumVar = '';

					(oData["purchase_order"] != '') ? sPONumVar = oData["purchase_order"] : sPONumVar = "None";

					uiDisplayCompleteReceivingNumber.html("Receiving No. "+oData["receiving_number"]+ "(Complete)");
					uidDisplayCompleteReceivingPurchaseOrder.html(sPONumVar);
					uiDisplayCompleteReceivingDateIssued.html(oData["date_issued"]);
					uiDisplayCompleteReceivingVesselName.html(oData["vessel_name"]);
					uiDisplayCompleteReceivingTrucking.html(oData["trucking"]);
					uiDisplayCompleteReceivingDriverName.html(oData["driver_name"]);
					uiDisplayCompleteReceivingLicenseNumber.html(oData["license_name"]);
					uiDisplayCompleteReceivingPlateNumber.html(oData["plate_number"]);
					uiDisplayCompleteReceivingTareIn.html(oData["tare_in"]+" MT");
					uiDisplayCompleteReceivingNTareOut.html(oData["tare_out"]+" MT");
					uiDisplayCompleteReceivingNetWeight.html(oData["net_weight"]+" MT");

					_displayProductItems(oData["product_info"]);

					_dispalyReceivingNotes(oData["receiving_notes"]);

					_displayDocuments(oData["receiving_documents"]);

					setTimeout(function(){
						$('#generated-data-view').removeClass('showed');
					},300);

			}
		}
		else if(sCurrentPath.indexOf("consignee_complete_vessel") > -1)
		{
			if(sType === 'displayCompletedConsigneeGoodsVessel' && Object.keys(oData).length > 0)
			{
				//console.log(JSON.stringify(oData));

				var uiDisplayCompleteReceivingNumber = $("#display-complete-receiving-number"),
					uidDisplayCompleteReceivingPurchaseOrder = $("#display-complete-receiving-purchase-order"),
					uiDisplayCompleteReceivingDateIssued = $("#display-complete-receiving-date-issued"),
					uiDisplayCompleteReceivingVesselName = $("#display-complete-receiving-vessel-name"),
					uiDisplayCompleteReceivingVesselType = $("#display-complete-receiving-vessel-type"),
					uiDisplayCompleteReceivingVesselCaptain = $("#display-complete-receiving-vessel-captain"),
					uiDisplayCompleteReceivingVesselHatches = $("#display-complete-receiving-vessel-hatches"),
					uiDisplayCompleteReceivingVesselDischargeType = $("#display-complete-receiving-vessel-discharge-type"),
					uiDisplayCompleteReceivingVesselLandingVolume = $("#display-complete-receiving-vessel-landing-volume"),
					uiDisplayCompleteReceivingBerthing = $("#display-complete-receiving-berthing"),
					uiDisplayCompleteReceivingUnloadingStart = $("#display-complete-receiving-unloading-start"),
					uiDisplayCompleteReceivingUnloadingEnd = $("#display-complete-receiving-unloading-end"),
					uiDisplayCompleteReceivingUnloadingDuration = $("#display-complete-receiving-unloading-duration");
					uiDisplayCompleteReceivingDisplayCompleteReceivingDepartureTime = $("#display-complete-receiving-departure-time"),
					sPONumVar = '';

					(oData["purchase_order"] != '') ? sPONumVar = oData["purchase_order"] : sPONumVar = "None";

					uiDisplayCompleteReceivingNumber.html("Receiving No. "+oData["receiving_number"]+ "(Complete)");
					uidDisplayCompleteReceivingPurchaseOrder.html(sPONumVar);
					uiDisplayCompleteReceivingDateIssued.html(oData["date_issued"]);
					uiDisplayCompleteReceivingVesselName.html(oData["vessel_name"]);
					uiDisplayCompleteReceivingVesselType.html(oData["vessel_type"]);
					uiDisplayCompleteReceivingVesselCaptain.html(oData["vessel_captain"]);
					uiDisplayCompleteReceivingVesselHatches.html(oData["hatches"]);
					uiDisplayCompleteReceivingVesselDischargeType.html(oData["discharge_type"]);
					uiDisplayCompleteReceivingVesselLandingVolume.html(oData["bill_of_landing_volume"]+" "+oData["bill_measure"].toUpperCase());
					uiDisplayCompleteReceivingBerthing.html(oData["complete_berthing_time"]);
					uiDisplayCompleteReceivingUnloadingStart.html(oData["complete_start_unloading"]);
					uiDisplayCompleteReceivingUnloadingEnd.html(oData["complete_end_unloading"]);

					uiDisplayCompleteReceivingDisplayCompleteReceivingDepartureTime.html(oData["complete_departure_time"]);


					_displayProductItems(oData["product_info"]);

					_dispalyReceivingNotes(oData["receiving_notes"]);

					_displayDocuments(oData["receiving_documents"]);



					var iConvertAmount = parseInt(oData["complete_unloading_duration"]);

					var date = new Date(iConvertAmount);
					var str = date.getUTCDate()-1 + " days, "+date.getUTCHours() + " hours ";


					uiDisplayCompleteReceivingUnloadingDuration.html(str);

					setTimeout(function(){
						$('#generated-data-view').removeClass('showed');
					},300);

			}
		}

	}

	return {
		buildUnitOfMeasure : buildUnitOfMeasure,
		buildUnitOfMeasureBillMeasurement : buildUnitOfMeasureBillMeasurement,
		getAllStorage : getAllStorage,
		oUploadedDocuments : oUploadedDocuments,
		displaySelectedReceivingConsigneeTruck : displaySelectedReceivingConsigneeTruck,
		displaySelectedReceivingConsigneeVessel : displaySelectedReceivingConsigneeVessel,
		displayAllReceivingConsignee : displayAllReceivingConsignee,
		getVesselName : getVesselName,
		getUnitOfMeasure : getUnitOfMeasure,
		getConsignee : getConsignee,
		_itemsEvent : _itemsEvent,
		getProducts : getProducts,
		getCompleteTruckInfo : getCompleteTruckInfo,
		getCompleteVesselInfo : getCompleteVesselInfo,
		bindEvents : bindEvents,
		renderElement : renderElement
	}
})();

$(document).ready(function(){
	$("select").transformDD();


	if(sCurrentPath.indexOf("receiving/consignee_create") > -1) 
	{

		$('#generated-data-view').addClass('showed');
			

		cr8v_platform.localStorage.delete_local_storage({name:"SetSelectedProductForReceivingConsignee"})
		cr8v_platform.localStorage.delete_local_storage({name:"setReceiveConsigneeAddNoteStored"});

		var uiAddReceivingConsigneeNoteButton = $("#AddReceivingConsigneeNote");

		uiAddReceivingConsigneeNoteButton.off("click.addReceiveConsignNote").on("click.addReceiveConsignNote", function(e){
			e.preventDefault();
			CCReceivingConsigneeGoods.bindEvents("AddNewReceivingConsigneeGoodsNotes");
		});

		CCReceivingConsigneeGoods.bindEvents("AddNewReceivingConsigneeGoods");
		

		CCReceivingConsigneeGoods.getProducts();
		CCReceivingConsigneeGoods.getVesselName();

		
		
				



		$(".mode-of-deliver.select div.option").click(function() {
			var selectedDelivery = $(this).text();
			switch (selectedDelivery) {
				case 'Vessel' :
					$(".vessel-delivery").slideDown();
					$(".truck-delivery").slideUp();
					break;
				case 'Truck' :
					$(".vessel-delivery").slideUp();
					$(".truck-delivery").slideDown();
					break;
				default : 
					break;
			}
		});

		$(".mode-of-deliver.select div.option").trigger('click');



		CCReceivingConsigneeGoods.getConsignee();
		CCReceivingConsigneeGoods.getUnitOfMeasure();

		var measure_interval = setInterval(function(){
			var getUnitMeasure = cr8v_platform.localStorage.get_local_storage({"name":"unit_of_measures"});
			if(getUnitMeasure !== null)
			{
				CCReceivingConsigneeGoods.buildUnitOfMeasureBillMeasurement();
				clearInterval(measure_interval);
			}
		}, 300);

		

		cr8v_platform.localStorage.delete_local_storage({name:"getAllReceivingConsigneeProdutcts"});
		cr8v_platform.localStorage.delete_local_storage({name:"setReceiveConsigneeAddNoteStored"});
		cr8v_platform.localStorage.delete_local_storage({name:"modeOfDelivery"});
		cr8v_platform.localStorage.delete_local_storage({name:"CurrentReceivingConsigneeItems"});
		cr8v_platform.localStorage.delete_local_storage({name:"setNewConsigneeRow"});
		cr8v_platform.localStorage.delete_local_storage({name:"setSelectedReceivingConsigneeItem"});
		cr8v_platform.localStorage.delete_local_storage({name:"setCurrentDisplayReceivedId"});

		CDropDownRetract.retractDropdown($("#addSelectModeDelivery"));

		$("#addInputVesselLandingVolume").number(true, 2);

					
	}else if(sCurrentPath.indexOf("consignee_receiving") > -1){
		// console.log("logs")
		var oOptions = {
			url : BASEURL+"receiving/display_all_receiving_consignee_goods",
			type : "GET",
			data : {"data":"none"},
			returnType : "json",
			async : false,
			beforeSend: function(){
				$('#generated-data-view').addClass('showed');
			},
			success : function(oResult){
				if(Object.keys(oResult.data).length > 0)
				{
					oDisplayGetAll = oResult.data;
					CCReceivingConsigneeGoods.renderElement("displayReceivingConsigneeGoods", oDisplayGetAll);

					cr8v_platform.localStorage.set_local_storage({
						name:"displayAllReceivingConsigneeData",
						data:oDisplayGetAll
					});
				}else{
					$('#generated-data-view').removeClass('showed');
					
				}

			},
			completed : function(){

			}

		} 
		cr8v_platform.CconnectionDetector.ajax(oOptions);

		cr8v_platform.localStorage.delete_local_storage({name:"currentReceivingNumber"});
		cr8v_platform.localStorage.delete_local_storage({name:"dataUploadedDocument"});
		cr8v_platform.localStorage.delete_local_storage({name:"displayAllReceivingConsigneeData"});
		cr8v_platform.localStorage.delete_local_storage({name:"setSelectedReceivingConsigneeItemComplete"});

		
	}
	else if(sCurrentPath.indexOf("consignee_ongoing_vessel") > -1)
	{

		$(".loading-method-container").hide();
		$(".set-consignee-items").hide();
		$(".btn-hover-storage").hide();
		$(".storage-check-btn").show();
		

		CCReceivingConsigneeGoods.getConsignee();
		CCReceivingConsigneeGoods.getAllStorage();

		var iCreateidBtn = $(".receiving-info-edit");

		var getReceivingId = "",
			getCurrentSelectedReceivedConsigneeNumber = cr8v_platform.localStorage.get_local_storage({name:"currentReceivingNumber"}),
			getCurrentSelectedReceivedConsigneeId = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"});


			if(getCurrentSelectedReceivedConsigneeNumber !== null)
			{
				console.log("first");

				var oOptions = {
					url : BASEURL+"receiving/get_current_selected_receiving_consignee_vessel_1",
					type : "POST",
					data: {"receiving_number":getCurrentSelectedReceivedConsigneeNumber.value},
					returnType: "json",
					success: function(oResult){
						if(oResult.data === null)
						{
							console.log("Not Found");
						}else{
							console.log("ok first");

							cr8v_platform.localStorage.set_local_storage({
								name : "setSelectedReceivingConsigneeItem",
								data : oResult.data
							});

							cr8v_platform.localStorage.set_local_storage({
								name : "setCurrentDisplayReceivedId",
								data : {value:oResult.data.receiving_id}
							});

							CCReceivingConsigneeGoods.renderElement("displaySelectedConsigneeGoodsVessel", oResult.data);
							CCReceivingConsigneeGoods.bindEvents("addNotesSave", oResult.data);


							// console.log(JSON.stringify(oResult.data));
						}
					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);

			}
			else if(getCurrentSelectedReceivedConsigneeId !== null)
			{
				console.log("second");

				var oOptions = {
					url : BASEURL+"receiving/get_current_selected_receiving_consignee_vessel",
					type : "POST",
					data: {"receiving_id":getCurrentSelectedReceivedConsigneeId.value},
					returnType: "json",
					success: function(oResult){
						if(oResult.data === null)
						{
							console.log("Not Found");
						}else{
							// console.log(JSON.stringify(oResult.data));

							CCReceivingConsigneeGoods.renderElement("displaySelectedConsigneeGoodsVessel", oResult.data);
							CCReceivingConsigneeGoods.bindEvents("addNotesSave", oResult.data);

							cr8v_platform.localStorage.set_local_storage({
								name : "setSelectedReceivingConsigneeItem",
								data : oResult.data
							});
							
						}
					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);

			}


			CCReceivingConsigneeGoods.bindEvents("validateProductQuantity");

			iCreateidBtn.off("click.createId").on("click.createId", function(){
				var oReceivingData = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"});

				cr8v_platform.localStorage.set_local_storage({
			 		name : "setCurrentDisplayReceivedId",
			 		data : {value:oReceivingData["receiving_id"]}
			 	});
			});


		

	}
	else if(sCurrentPath.indexOf("consignee_ongoing_truck") > -1)
	{
		$(".loading-method-container").hide();
		$(".set-consignee-items").hide();
		$(".btn-hover-storage").hide();
		$(".storage-check-btn").show();
		

		CCReceivingConsigneeGoods.getConsignee();
		CCReceivingConsigneeGoods.getAllStorage();

		var iCreateidBtn = $(".receiving-info-edit");

		var getCurrentSelectedReceivedConsigneeNumber = cr8v_platform.localStorage.get_local_storage({name:"currentReceivingNumber"}),
			getCurrentSelectedReceivedConsigneeId = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"}),
			iGetValue = 0;
			// console.log(getCurrentSelectedReceivedConsigneeId.value);

		if(getCurrentSelectedReceivedConsigneeNumber !== null)
		{
			var oOptions = {
				url : BASEURL+"receiving/get_current_selected_receiving_consignee_truck_1",
				type : "POST",
				data: {"receiving_number":getCurrentSelectedReceivedConsigneeNumber.value},
				returnType: "json",
				success: function(oResult){
					if(oResult.data === null)
					{
						console.log("Not Found");
					}else{

						cr8v_platform.localStorage.set_local_storage({
							name : "setSelectedReceivingConsigneeItem",
							data : oResult.data
						});

						cr8v_platform.localStorage.set_local_storage({
							name : "setCurrentDisplayReceivedId",
							data : {value:oResult.data.receiving_id}
						});

						CCReceivingConsigneeGoods.renderElement("displaySelectedConsigneeGoodsTruck", oResult.data);

						CCReceivingConsigneeGoods.bindEvents("addNotesSave", oResult.data);

						
						//console.log(JSON.stringify(oResult.data));

					}
				}
			}

			cr8v_platform.CconnectionDetector.ajax(oOptions);
		}
		else if(getCurrentSelectedReceivedConsigneeId !== null)
		{
			var oOptions = {
				url : BASEURL+"receiving/get_current_selected_receiving_consignee_truck",
				type : "POST",
				data: {"receiving_id":getCurrentSelectedReceivedConsigneeId.value},
				returnType: "json",
				success: function(oResult){
					if(oResult.data === null)
					{
						console.log("Not Found");
					}else{

						CCReceivingConsigneeGoods.renderElement("displaySelectedConsigneeGoodsTruck", oResult.data);

						CCReceivingConsigneeGoods.bindEvents("addNotesSave", oResult.data);


						cr8v_platform.localStorage.set_local_storage({
							name : "setSelectedReceivingConsigneeItem",
							data : oResult.data
						});
						//console.log(JSON.stringify(oResult.data));
					}
				}
			}

			cr8v_platform.CconnectionDetector.ajax(oOptions);
		}

		CCReceivingConsigneeGoods.bindEvents("validateProductQuantity");
		CCReceivingConsigneeGoods.bindEvents("showReceivingConsigneeComplete");


		// var getCurrentSelectedReceivedConsignee = cr8v_platform.localStorage.get_local_storage({name:"setCurrentDisplayReceivedId"});
		// console.log(getCurrentSelectedReceivedConsignee.value);
		// console.log(getCurrentSelectedReceivedConsignee.value);
		
		// cr8v_platform.localStorage.delete_local_storage({name:"currentReceivingNumber"});

		iCreateidBtn.off("click.createId").on("click.createId", function(){
			var oReceivingData = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"});

			cr8v_platform.localStorage.set_local_storage({
		 		name : "setCurrentDisplayReceivedId",
		 		data : {value:oReceivingData["receiving_id"]}
		 	});
		});


	}
	else if(sCurrentPath.indexOf("consignee_edit_vessel") > -1)
	{
		CCReceivingConsigneeGoods.bindEvents("EditNewReceivingConsigneeGoodsValid");
		CCReceivingConsigneeGoods.bindEvents("EditNewItemProdReceivingConsigneeGoods");

		cr8v_platform.localStorage.delete_local_storage({name:"SetSelectedProductForReceivingConsignee"})
		cr8v_platform.localStorage.delete_local_storage({name:"setReceiveConsigneeAddNoteStored"});

		CCReceivingConsigneeGoods.displaySelectedReceivingConsigneeVessel();
		CCReceivingConsigneeGoods.getUnitOfMeasure();
		
		
		var interval = setInterval(function(){
			var getSelecteddata = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"});
			if(getSelecteddata !== null)
			{
				CCReceivingConsigneeGoods.renderElement("displayEditSelectedConsigneeGoodsVessel", getSelecteddata);
				clearInterval(interval);
			}
		}, 300);

		var measure_interval = setInterval(function(){
			var getUnitMeasure = cr8v_platform.localStorage.get_local_storage({name:"unit_of_measures"});
			if(getUnitMeasure !== null)
			{
				// CCReceivingConsigneeGoods.buildUnitOfMeasure();
				clearInterval(measure_interval);
			}
		}, 300);
		
		
		// console.log(JSON.stringify(getSelecteddata));

		CCReceivingConsigneeGoods.getProducts();
		CCReceivingConsigneeGoods.getConsignee();
		// CCReceivingConsigneeGoods.renderElement("displayItemsForProductList");

		CCReceivingConsigneeGoods.renderElement("displayEditItemsForVesselList");
		CCReceivingConsigneeGoods.renderElement("displayEditItemsForVesselListForTruck");

		cr8v_platform.localStorage.delete_local_storage({name:"currentReceivingNumber"});

		$("#display-edit-vessel-bill-of-landing").number(true, 2);

	}
	else if(sCurrentPath.indexOf("consignee_edit_truck") > -1)
	{	
		CCReceivingConsigneeGoods.bindEvents("EditNewReceivingConsigneeGoodsValidTruck");
		CCReceivingConsigneeGoods.bindEvents("EditNewItemProdReceivingConsigneeGoodsTruck");

		cr8v_platform.localStorage.delete_local_storage({name:"SetSelectedProductForReceivingConsignee"})
		cr8v_platform.localStorage.delete_local_storage({name:"setReceiveConsigneeAddNoteStored"});

		CCReceivingConsigneeGoods.displaySelectedReceivingConsigneeTruck();
		CCReceivingConsigneeGoods.getUnitOfMeasure();

		var interval = setInterval(function(){
			var getSelecteddata = cr8v_platform.localStorage.get_local_storage({name:"setSelectedReceivingConsigneeItem"});
			if(getSelecteddata !== null)
			{
				CCReceivingConsigneeGoods.renderElement("displayEditSelectedConsigneeGoodsTruck", getSelecteddata);
				clearInterval(interval);
			}
		}, 300);

		var measure_interval = setInterval(function(){
			var getUnitMeasure = cr8v_platform.localStorage.get_local_storage({name:"unit_of_measures"});
			if(getUnitMeasure !== null)
			{
				
				clearInterval(measure_interval);
			}
		}, 300);

	
		

		CCReceivingConsigneeGoods.getProducts();

		CCReceivingConsigneeGoods.getConsignee();

		
		CCReceivingConsigneeGoods.renderElement("displayEditItemsForVesselListForTruck");
		
		cr8v_platform.localStorage.delete_local_storage({name:"currentReceivingNumber"});
	}
	else if(sCurrentPath.indexOf("consignee_complete_truck") > -1)
	{
	
		CCReceivingConsigneeGoods.getCompleteTruckInfo();
	}
	else if(sCurrentPath.indexOf("consignee_complete_vessel") > -1)
	{
		CCReceivingConsigneeGoods.getCompleteVesselInfo();
	}
	


	

	

	// var getImage = CGetProfileImage.getDefault("Nesasd");
	// console.log(getImage);
});