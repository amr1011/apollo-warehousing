var CReports = (function(){


	var	failIcon = BASEURL + "assets/js/plugins/feedback/images/fail.svg",
		oFeedback = {
		speed : 'mid',
		icon : failIcon,
		title : 'Fail!',
		message : ''
	};

	function initializer()
	{
		if(sCurrentPath.indexOf("display_reports") > -1)
		{
			// _getAllVessel();
			_getAllConsignee();

			bindEvents('triggerGenerateReport');
			cr8v_platform.localStorage.delete_local_storage({name : 'oCargoWithdrawal'});
		}
		else if(sCurrentPath.indexOf("cargo_withdrawal") > -1) {

			var m_names = new Array("January", "February", "March", 
                                     "April", "May", "June", "July", "August", "September", 
                                     "October", "November", "December");

			var uiContainer = $('.center-main')
				uiTemplate = uiContainer.find('.date-range'),
				oDateTitle = cr8v_platform.localStorage.get_local_storage({"name":"oCargoWithdrawal"}),
				sDateFrom = new Date(oDateTitle.datefrom),
				curr_dateDateFrom = sDateFrom.getDate(),
				curr_monthDateFrom = sDateFrom.getMonth(),
				curr_yearDateFrom = sDateFrom.getFullYear(),
				getDateFrom = m_names[curr_monthDateFrom] + " " + curr_dateDateFrom + ", " + curr_yearDateFrom,
				sDateTo = new Date(oDateTitle.dateto),
				curr_dateDateTo = sDateTo.getDate(),
				curr_monthDateTo = sDateTo.getMonth(),
				curr_yearDateTo = sDateTo.getFullYear(),
				getDateTo = m_names[curr_monthDateTo] + " " + curr_dateDateTo + ", " + curr_yearDateTo;

              
				uiTemplate.html("Cargo Withdrawal Summary for "+getDateFrom+" - "+getDateTo);
				_generateCargoWithdrawalSummary();

		}


		
	}


	/*function _getAllVessel()
	{
		var oOptions = {
				url : BASEURL+"reports/get_all_vessel",
				type : "POST",
				data: {"wala":"adas"},
				returnType: "json",
				success: function(oResult){
					renderElement("buildCargoWithdrawalSummaryVessel", oResult.data);
				}
			}

			cr8v_platform.CconnectionDetector.ajax(oOptions);
	}*/

	function _getAllConsignee()
	{
		var oOptions = {
				url : BASEURL+"reports/get_consignee_names",
				type : "POST",
				data: {"wala":"adas"},
				returnType: "json",
				success: function(oResult){
					renderElement("buildCargoWithdrawalSummaryConsignee", oResult.data);
				}
			}

			cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _convertDateAndHoursToTimeStamp(sStartDateTime)
  {

    var datum = Date.parse(sStartDateTime);
       return datum/1000;

  }



	function _getConsigneeVessels(cID) {
		var oOptions = {
			type : 'POST',
			data : {id : cID},
			url : BASEURL + 'reports/get_consignee_vessels',
			returnType : 'json',
			success : function(oResult) {
				renderElement("buildCargoWithdrawalSummaryVessel", oResult.data);
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _generateCargoWithdrawalSummary(){

		var oData = cr8v_platform.localStorage.get_local_storage({"name":"oCargoWithdrawal"});
		

		var oOptions = {
					type : 'POST',
					data : oData,
					url : BASEURL + 'reports/generateCargoWithdrawalSummary',
					returnType : 'json',
					success : function(oResult) {
						oFeedback['message'] = oResult.message;

						if(!oResult.status) {

							$('body').feedback(oFeedback);	

						}
						else{
							renderElement("buildCargoSummaryDisplay",oResult.data);
							
						}

					}
				}

				cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function _displaySelectedReportsConsignee(oData, ui)
	{
		var uitemplate = $(".add-consignee-contain"),
			uiCLoneTemp = uitemplate.clone();

			uiCLoneTemp.show();

			uiCLoneTemp.removeClass("add-consignee-contain");

			ui.append(uiCLoneTemp);


			var uiContainer = ui.find(".per-consignee-record");


		for(var x in oData)
		{
			var item = oData[x],
				uiCloneTemplate = ui.find(".consignee-per-item").clone();

				uiCloneTemplate.removeClass("consignee-per-item");

				uiCloneTemplate.find(".profile_name").html(item.name);
				uiCloneTemplate.find(".profile_name").attr("data-id", item.id);

			uiContainer.append(uiCloneTemplate);
		}

		ui.find(".consignee-per-item").remove();


		_triggerDropdownRetract(ui);
		_triggerDropdownItemConsignee();
		

	}

	function _displaySelectedReportsVessel(oData, ui)
	{

		ui.find('.vessel-contain').remove();
		var uitemplate = $(".add-vessel-contain"),
			uiCLoneTemp = uitemplate.clone();

			uiCLoneTemp.show();

			uiCLoneTemp.removeClass("add-vessel-contain");


			ui.append(uiCLoneTemp);


			var uiContainer = ui.find(".per-vessel-record");


		for(var x in oData)
		{
			var item = oData[x],
				uiCloneTemplate = ui.find(".vessel-per-item").clone();

				uiCloneTemplate.removeClass("vessel-per-item");

				uiCloneTemplate.find(".profile_name").html(item.name);
				uiCloneTemplate.find(".profile_name").attr("data-id", item.id);

			uiContainer.append(uiCloneTemplate);
		}

		ui.find(".vessel-per-item").remove();


		_triggerDropdownRetract(ui);
		_triggerDropdownItemVessel();
		

	}


	function _triggerSelectItemConsignee(ui){


		var uiCollectData = ui.find('.consignee-item'),
			uiParent = ui.closest('.consignee-contain'),
			uiContainer = uiParent.find('.collected-textbox'),
			oCnames = []
			oCid = [];

		$.each(uiCollectData,function(){

			var uiThis = $(this),
				
				sName = uiThis.find('.profile_name').text(),
				iID = uiThis.find('.profile_name').attr('data-id');

				if(uiThis.hasClass('marked')){
					/*uiContainer.html(sName+', ');
					console.log(sName);*/
					oCnames.push(sName);
					oCid.push(iID);
				}
		});

		uiContainer.html(oCnames.join(', '));
		if(oCid.length > 0){

			_getConsigneeVessels(oCid.join(', '));

		}


	}

	function _triggerSelectItemVessel(ui){


		var uiCollectData = ui.find('.vessel-item'),
			uiParent = ui.closest('.vessel-contain'),
			uiContainer = uiParent.find('.collected-textbox'),
			oCnames = [];

		$.each(uiCollectData,function(){

			var uiThis = $(this),
				
				sName = uiThis.find('.profile_name').text(),
				iID = uiThis.find('.profile_name').attr('data-id');

				if(uiThis.hasClass('marked')){
					/*uiContainer.html(sName+', ');
					console.log(sName);*/
					oCnames.push(sName);
				}
		});

		uiContainer.html(oCnames.join(', '));



	}


	function _triggerDropdownItemConsignee() {
		
		var uiBtn = $('.consignee-item');

		uiBtn.off('click.addCheck').on('click.addCheck',function(){
			var uiThis = $(this),
				uiParent = uiThis.closest('.per-consignee-record');

			if(uiThis.hasClass("marked"))
			{
				uiThis.removeClass("marked");
			}else{
				uiThis.addClass("marked");
			}

			_triggerSelectItemConsignee(uiParent);

		});		
	}

	function _triggerDropdownItemVessel() {
		
		var uiBtn = $('.vessel-item');

		uiBtn.off('click.addCheck').on('click.addCheck',function(){
			var uiThis = $(this),
				uiParent = uiThis.closest('.per-vessel-record');

			if(uiThis.hasClass("marked"))
			{
				uiThis.removeClass("marked");
			}else{
				uiThis.addClass("marked");
			}

			_triggerSelectItemVessel(uiParent);

		});		
	}

	function _triggerDropdownRetract(ui)
	{
		var uiPopPerson = ui.find(".popup_person_list"),
			uiDropdown = ui.find(".member-container");

		uiPopPerson.hide();
	    var popup_person = true;
	    uiDropdown.off("click").on("click", function() {
	      if (popup_person == true) {
	        uiPopPerson.slideDown();      
	        popup_person = false;
	      } else {
	        uiPopPerson.slideUp();        
	        popup_person =  true;
	      }      
	    });
	}

	function _displayCargoSummary(oData){
		var uiContainer = $("#display-selected-cargo"),
			uiTemplate = $(".set-withdrawal-item"),
			iTotal =0;

			// uiContainer.html("");

			for(var x in oData)
			{
				var item = oData[x],
					uiCloneTemplate = uiTemplate.clone(),
					uiTotalTemplate = $(".last-panel"),
					iTotal = iTotal + parseFloat(item.net_mt);

					uiCloneTemplate.removeClass("set-withdrawal-item");

					uiCloneTemplate.find(".cdr-no").html(item.cdr_no);
					uiCloneTemplate.find(".date-of-exit").html(item.date_of_exit);
					uiCloneTemplate.find(".consignee").html(item.consignee);
					uiCloneTemplate.find(".vessel").html(item.vessel);
					uiCloneTemplate.find(".commodity").html(item.commodity);
					uiCloneTemplate.find(".type").html(item.type);
					uiCloneTemplate.find(".no-of-bags-bulk").html(item.no_of_bags_bulk);
					uiCloneTemplate.find(".trucking-small-vessel-name").html(item.trucking_small_vessel);
					uiCloneTemplate.find(".plate-no").html(item.plate_no);
					uiCloneTemplate.find(".gatepass-no").html(item.gatepas_no);
					uiCloneTemplate.find(".cargo-swapping").html(item.cargo_swapping);
					uiCloneTemplate.find(".net-mt").html(item.net_mt);
					uiTotalTemplate.find('.total-net').html(iTotal);

					uiCloneTemplate.insertBefore(uiTotalTemplate);

					uiCloneTemplate.show();

					// uiContainer.append(uiCloneTemplate);
			}
	}


	function bindEvents(sAction, ui)
	{

		if(sAction == 'triggerGenerateReport'){
			$('.generate-cargo').off('click').on('click', function() {

				var uiThis = $(this),
					uiParent = uiThis.closest('[modal-id="cargo-withdrawal-summary"]'),
					uiCargoDateFrom = uiParent.find('#cargo-from').find('.cargo-with-date-from'),
					uiCargoDateTo = uiParent.find('#cargo-to').find('.cargo-with-date-to'),
					sCargoStartDate = uiCargoDateFrom.val().split('/').join("-"),
					sCargoEndDate = uiCargoDateTo.val().split('/').join("-"),
					uiConsignee = uiParent.find('#consignee-records').find('.member-container').find('.collected-textbox'),
					uiVessel = uiParent.find('#vessel-records').find('.member-container').find('.collected-textbox'),
					getCargoDateFrom,
					getCargoDateTo,
					getConsignee,
					getVessel,
					compareStartDate,
					compareEndDate,
					oConsignee = [],
					iConsigneeCollect = '',
					oVessel = [],
					iVesselCollect = '';

					getCargoDateFrom = uiCargoDateFrom.val();
					getCargoDateTo = uiCargoDateTo.val();
					getConsignee = uiConsignee.text();
					getVessel = uiVessel.text();


					var oConsigneeName = getConsignee.split(', '),
						oVesselName = getVessel.split(', '),
						iCountConsignee = oConsigneeName.length,
						iCountVessel = oVesselName.length,
						iCount = 0,
						iCount2 = 0;

						for(var x = 0; x<iCountConsignee; x++){
							iCount++;
								
								if(iCount == iCountConsignee){
									iConsigneeCollect += "'"+oConsigneeName[x]+"'";
								}
								else{
									iConsigneeCollect += "'"+oConsigneeName[x]+"',";
								}
						}

						for(var x = 0; x<iCountVessel; x++){
							iCount2++;
								
								if(iCount2 == iCountVessel){
									iVesselCollect += "'"+oVesselName[x]+"'";
								}
								else{
									iVesselCollect += "'"+oVesselName[x]+"',";
								}	
						}

					var compareStartDate = _convertDateAndHoursToTimeStamp(sCargoStartDate),
						compareEndDate = _convertDateAndHoursToTimeStamp(sCargoEndDate);

					if (compareStartDate > compareEndDate) {
						oFeedback['message'] = 'Invalid Date Range Supplied!'
						$('body').feedback(oFeedback);
					}
					else if(getCargoDateFrom == "" || getCargoDateTo == "" || iConsigneeCollect == "" || iVesselCollect == ""){
						oFeedback['message'] = 'Please fill up all fields!'
						$('body').feedback(oFeedback);
					}
					else{
							

							var oGatherCargo = {
								'consignee' : iConsigneeCollect,
								'vessel' : iVesselCollect,
								'datefrom' : getCargoDateFrom,
								'dateto' : getCargoDateTo
							}

							cr8v_platform.localStorage.set_local_storage({
								name : "oCargoWithdrawal",
								data : oGatherCargo
							});

							window.location.href = BASEURL+"reports/cargo_withdrawal";

					}

					 
				});

		}
		
	}

	function renderElement(sType, oData)
	{
		if(sType == "buildCargoWithdrawalSummaryVessel")
		{
			var uiCargoConsignee = $("#vessel-records");

			_displaySelectedReportsVessel(oData, uiCargoConsignee);
		}
		else if(sType == "buildCargoWithdrawalSummaryConsignee")
		{
			var uiCargoConsignee = $("#consignee-records");

			_displaySelectedReportsConsignee(oData, uiCargoConsignee);
		}
		else if(sType == "buildCargoSummaryDisplay"){
			_displayCargoSummary(oData);
		}

	}


	return {
		bindEvents : bindEvents,
		renderElement : renderElement,
		initializer : initializer
	}



})();

$(document).ready(function(){

	CReports.initializer();

	

});