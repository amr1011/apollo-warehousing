$.fn.zenTextEdit = function(options){
	var settings = $.extend({width : "100%",height : "85%"},options);

	return this.each(function(){
		var $this = $(this).hide();

		var containerDiv = $("<div/>",{
			css : {
				marginBottom : "10px",
				width : settings.width,
				height : settings.height,
			}
		});

		$this.after(containerDiv); 
		var editor = $("<iframe/>",{
			"class" : "text-edit",
			frameborder : "0",
			css : {
				width : settings.width,
				height : "90%",
				backgroundImage : "url('../assets/images/ui/notepad.png')",
				backgroundPositionY : "23px"
			}
		}).appendTo(containerDiv).get(0);

		editor.contentWindow.document.open();
		editor.contentWindow.document.close();
		editor.contentWindow.document.designMode="on";

		$(".text-edit").contents().find("body").css({
			fontFamily : "'Roboto', arial, sans-serif'",
			fontSize : "12px",
			lineHeight : "20px"
		});

		var buttonPane = $("<div/>",{
			"class" : "text_editor",
			css : {
				width : settings.width,
				backgroundColor : "#fff"
			}
		}).prependTo(containerDiv);

		var btnBold = $("<div/>",{
			"class" : "text_deco",
			html : "<i class='fa fa-bold'></i>",
			data : {
				commandName : "bold"
			},
			click : execCommand 
		}).appendTo(buttonPane );

		var btnUnderline = $("<div/>",{
			"class" : "text_deco",
			html : "<i class='fa fa-underline'></i>",
			data : {
				commandName : "underline"
			},
			click : execCommand 
		}).appendTo(buttonPane );

		var btnItalic = $("<div/>",{
			"class" : "text_deco",
			html : "<i class='fa fa-italic'></i>",
			data : {
				commandName : "italic"
			},
			click : execCommand 
		}).appendTo(buttonPane );		

		function execCommand (e) {
			$(this).toggleClass("selected");
			var contentWindow = editor.contentWindow;
			contentWindow.focus();
			contentWindow.document.execCommand($(this).data("commandName"), false, "");
			contentWindow.focus();
			return false;
		}	
	});
}