/**
 * @preserve
 * email unversioned
 * author pilo cortes
 */
(function( $ ) {
	// default settings, can be overridden by $.extend
	/* var settings = {
		url : 'http://api.becr8v.com/send_mail',
		to : 'kenneth@cr8vwebsolutions.com',
		from : '',
		subject : '',
		onValidationFail : '',
		onError : '',
		onSuccess : '',
		callBack : ''
	}; */
	
	$.emailer = function( options, settings ) {
		if ( options ) { $.extend( settings, options );	}
		
		// start validation
		var valid = true;
		for(x in options.fields){
			var field = options.fields[x];
			if(field.required){
				/*
				$(field.id).removeClass(options.invalidClass);
				if($.trim($(field.id).val()).length==0){
					valid = false;
					$(field.id).addClass(options.invalidClass);
				}
				*/
				if(typeof settings.beforeValidation == 'function') settings.beforeValidation($(field.id));
				if($.trim($(field.id).val()).length==0){
					valid = false;
					if(typeof settings.onInvalid == 'function') settings.onInvalid($(field.id));
				}
			}
		}
		
		//if valid
		if(valid){
			var sMsg = '<table>';
			for(x in options.fields){
				var field = options.fields[x];
				sMsg += '<tr><td>' + field.label + '</td><td>' + $.trim($(field.id).val()) + '</td></tr>';
			}
			sMsg += '</table>';
			
			/* // command for header hack - alternate options
			$.post(settings.url, { from:escape(settings.from), to:escape(settings.to), subject:escape(settings.subject), message:sMsg }, function(responseText){
					if(responseText=='success'){
						if(typeof settings.onSuccess == 'function') settings.onSuccess.call(this);
					} else {
						if(typeof settings.onError == 'function') settings.onError.call(this);
					}
				});
			*/
			
			// for iframe xdomian post
			fnPostXDomain(settings.url,{ from:settings.from, to:escape(settings.to), subject:settings.subject, message:sMsg });
			
			if(typeof settings.callBack == 'function') settings.callBack.call(this);
			
		} else {
			if(typeof settings.onValidationFail == 'function') settings.onValidationFail.call(this);
		}
	};
})( jQuery );

function fnPostXDomain(sUrl,params) {
  var iframe = document.createElement("iframe");
  var uniqueString = "becr8v_api";
  document.body.appendChild(iframe);
  iframe.id = "becr8v_iframe";
  iframe.style.display = "none";
  iframe.contentWindow.name = uniqueString;

  var form = document.createElement("form");
  form.target = uniqueString;
  form.action = sUrl;
  form.method = "POST";

  for(var x in params){
	  var input = document.createElement("input");
	  input.type = "hidden";
	  input.name = x;
	  input.value = params[x];
	  form.appendChild(input);
  }

  document.body.appendChild(form);
  form.submit();
}
