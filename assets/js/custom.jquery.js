$(window).load(function(){
	$.localScroll(
		{
			hash:true, 
			offset: {top: -105},
			duration: 3000,
			easing:'easeOutExpo'
		}
	);
	$('#mobilenav ul li a').localScroll(
		{
			hash:true, 
			offset: {top: -105},
			duration: 5000,
			easing:'easeOutExpo'
		}
	);
	
	// mobile navigation
	$('#mobileprojects_button').click(function(event){
		$('#mainwrapper').animate({ left: -100 }, 1000);
		$('#mobileprojects').animate({ right: 0 }, 1000);
		$('#mobilenav').delay(300).animate({ left: '-100%' }, 1000);
	});
	$('#mobilenav ul li a').click(function(){
		$('#mainwrapper').animate({ left: 0 }, 500);
		$('#mobilenav').animate({ left: '-100%' }, 1000);
		$('#mobileprojects').animate({ right: '-100%' }, 1000);
	});
	$('#mobilemenu_button').click(function(event){
		$('#mobilenav').animate({ left: 0 }, 1000);
		$('#mainwrapper').animate({ left: 100 }, 1200);
		$('#mobileprojects').delay(300).animate({ right: '-100%' }, 1000);
	});
	$('#wrapper').click(function(){
		$('#mainwrapper').animate({ left: 0 }, 500);
		$('#mobilenav').animate({ left: '-100%' }, 1000);
		$('#mobileprojects').animate({ right: '-100%' }, 1000);
	});
	//  delay = will only process code within delay(function() { ... }) every 100ms.
	//	fire jQuery on device not lower than 1025 pixel resolution
	
	// fire jquery on desktop only
	var delay = (function(){
		var timer = 0;
		return function(callback, ms){
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		};
	})();
	
	$(function() {

		var pause = 100; 

		$(window).resize(function() {
			
			delay(function() {
			
				var width = $(window).width();
		
				if( width >= 1025 ) {
					
		
				// Navigation Position
				var homePos = $('#home_link').position();
				var loginPos = $('#login_link').position();
				var timekeepingPos = $('#timekeeping_link').position();
				var trackingPos = $('#tracking_link').position();
				var payrollPos = $('#payroll_link').position();
				var payslipPos = $('#payslip_link').position();
				var contactusPos = $('#contactus_link').position();
				var hcmPos = $('#hcm_link').position();
				var affordablePos = $('#affordable_link').position();
				var contactPos = $('#contact_link').position();
				var currentPos = $('.current_nav').position();

				// Navigation Arrow Animation
				$('nav ul li a').hover(function(){
					$('#nav_arrow').stop(true,false).animate({ left: homePos.left + 10 }, 300);
				},function(){
					$('#nav_arrow').stop(true,false).delay(300).animate({ left: currentPos.left + 10 }, 300);
				});

				$('#login_link').hover(function(){
					$('#nav_arrow').stop(true,false).animate({ left: loginPos.left + 18 }, 300);
				},function(){
					$('#nav_arrow').stop(true,false).delay(300).animate({ left: currentPos.left + 10 }, 300);
				});

				$('#timekeeping_link').hover(function(){
					$('#nav_arrow').stop(true,false).animate({ left: timekeepingPos.left + 30 }, 300);
				},function(){
					$('#nav_arrow').stop(true,false).delay(300).animate({ left: currentPos.left + 10 }, 300);
				});

				$('#tracking_link').hover(function(){
					$('#nav_arrow').stop(true,false).animate({ left: trackingPos.left + 15 }, 300);
				},function(){
					$('#nav_arrow').stop(true,false).delay(300).animate({ left: currentPos.left + 10 }, 300);
				});

				$('#payroll_link').hover(function(){
					$('#nav_arrow').stop(true,false).animate({ left: payrollPos.left + 15 }, 300);
				},function(){
					$('#nav_arrow').stop(true,false).delay(300).animate({ left: currentPos.left + 10 }, 300);
				});

				$('#payslip_link').hover(function(){
					$('#nav_arrow').stop(true,false).animate({ left: payslipPos.left + 50 }, 300);
				},function(){
					$('#nav_arrow').stop(true,false).delay(300).animate({ left: currentPos.left + 10 }, 300);
				});

				$('#contactus_link').hover(function(){
					$('#nav_arrow').stop(true,false).animate({ left: contactusPos.left + 25 }, 300);
				},function(){
					$('#nav_arrow').stop(true,false).delay(300).animate({ left: currentPos.left + 10 }, 300);
				});

				$('#hcm_link').hover(function(){
					$('#nav_arrow').stop(true,false).animate({ left: hcmPos.left + 2 }, 300);
				},function(){
					$('#nav_arrow').stop(true,false).delay(300).animate({ left: currentPos.left + 10 }, 300);
				});

				$('#affordable_link').hover(function(){
					$('#nav_arrow').stop(true,false).animate({ left: affordablePos.left + 25 }, 300);
				},function(){
					$('#nav_arrow').stop(true,false).delay(300).animate({ left: currentPos.left + 10 }, 300);
				});

				$('#contact_link').hover(function(){
					$('#nav_arrow').stop(true,false).animate({ left: contactPos.left + 22 }, 300);
				},function(){
					$('#nav_arrow').stop(true,false).delay(300).animate({ left: currentPos.left + 10 }, 300);
				});

				// scroll bug fix
				/* $(window).scroll(function(){
					var scroll = $(window).scrollTop();
					if ( scroll + 1 >= 118 )
					{
						$('#wrapper').css('margin-top','128px');
						$('header').css('top', '0');
					}
					else {
						$('#wrapper').css('margin-top','0');
					}
				}); */
				// Top
				$(window).scroll(function() {
					var scroll = $(window).scrollTop();
					if ( scroll >= 0 ) {	
						$('#nav_arrow').stop(true,false).animate({ left: homePos.left + 10 }, 300);				
					} 
				});
				
				// Panel 1
				var content1Pos = $('.content_mainpanel_1').position();
				var content1Height = $('.content_mainpanel_1').height();
				$(window).scroll(function() {
					var scroll = $(window).scrollTop();
					if ( scroll >= Math.round(content1Pos.top) - 117 ) {	
						$('#nav_arrow').stop(true,false).animate({ left: loginPos.left + 18 }, 300);				
					} 
				});
				// Panel 2 

				var content2Pos = $('.content_mainpanel_2').position();
				var content2Height = $('.content_mainpanel_2').height();
				function shine() {
					$('.content2_animpanel .content2_shiner').delay(800).animate({ left: '100%'}, 1500);
					
					 window.setTimeout(function() { shine() }, 2300)
				}

				$(window).scroll(function() {
					var scroll = $(window).scrollTop();
					if ( scroll >= Math.round(content2Pos.top) - 117 ) {	
						$('#nav_arrow').stop(true,false).animate({ left: timekeepingPos.left + 30 }, 300)
						$('.content2_panel').animate({ right: '0px'}, 1000);
						$('.content2_imagepanel').animate({ left: '0px'}, 1000, function(){
							shine()
						});
						
					} 
				});

				// Panel 3 

				var content3Pos = $('.content_mainpanel_3').position();
				var content3Height = $('.content_mainpanel_3').height();
				$('.content3_panel img').fadeOut();

				$(window).scroll(function() {
					var scroll = $(window).scrollTop();
					if ( scroll >= Math.round(content3Pos.top) - 117 ) {
						$('#nav_arrow').stop(true,false).animate({ left: trackingPos.left + 15 }, 300);
						$('#content3_cursor').animate({ left: '16%', bottom: 310},2000, function(){
							$('#generate_payroll').css('background-position', '0px -75px');
							$('#content3_payroll').animate({ height: 500, width: 473, left: 50, top: 0 }, 700);
							$('.content3_panel img').delay(100).fadeIn(1000);
							$('#generate_payroll').fadeOut(500);
						});	
					} 
				});

				// Panel 4

				var content4Pos = $('.content_mainpanel_4').position();
				var content4Height = $('.content_mainpanel_4').height();

				$(window).scroll(function() {
					var scroll = $(window).scrollTop();
					if ( scroll >= Math.round(content4Pos.top) - 117 ) {	
						$('#nav_arrow').stop(true,false).animate({ left: payrollPos.left + 15 }, 300);
						$('.c4_img4').delay(200).animate({ bottom : '0%'}, 700, 'easeOutSine');
						$('.c4_img3').delay(500).animate({ bottom : '0%'}, 700, 'easeOutSine');
						$('.c4_img2').delay(600).animate({ bottom : '0%'}, 700, 'easeOutSine');
						$('.c4_img1').delay(700).animate({ bottom : '0%'}, 700, 'easeOutSine');
					}
				});

				// Panel 5

				var content5Pos = $('.content_mainpanel_5').offset();
				var content5Height = $('.content_mainpanel_5').height();

				$(window).bind('scroll',function() {
					var scroll = $(window).scrollTop();
					if ( scroll >= Math.round(content5Pos.top) - 117 ) {	
						$('#nav_arrow').stop(true,false).animate({ left: payslipPos.left + 50 }, 300);
						$('.content5_panel').animate({ left: '0%', opacity: 1}, 500, 'easeOutSine');
						$('.content5_imagepanel').animate({ right: '0%'}, 'easeOutSine',function(){
							$('.c5_attention').delay(100).animate({ height: 200, width: 223, left: '-20%', bottom: 0}, 500);
						});
					}
				});

				// Panel 6

				var content6Pos = $('.content_mainpanel_6').position();
				var content6Height = $('.content_mainpanel_6').height();

				function loop() {
					$('.thumb_on').delay(300).animate({ height: 0}, 1000);
					$('#sensor').delay(300).animate({ top: 0}, 1000);
					
					$('#sensor').delay().fadeOut().animate({ top: 450}, 1000).fadeIn(3000);
					$('.thumb_on').delay().fadeOut().animate({ height: '100%'}, 1000).fadeIn(3000);
					
					 window.setTimeout(function() { loop() }, 4600)
				}
					
				$(window).bind('scroll',function() {
					var scroll = $(window).scrollTop();
					if ( scroll >= Math.round(content6Pos.top) - 117 ) {	
						$('#nav_arrow').stop(true,false).animate({ left: contactusPos.left + 25 }, 300);
						loop()
					}
				});

				// Panel 7

				var content7Pos = $('.content_mainpanel_7').position();
				var content7Height = $('.content_mainpanel_7').height();

				$(window).scroll(function() {
					var scroll = $(window).scrollTop();
					if ( scroll >= Math.round(content7Pos.top) - 117 ) {	
						$('#nav_arrow').stop(true,false).animate({ left: hcmPos.left + 2 }, 300);
						$('.content7_panel').delay(300).animate({ left: 0, opacity: 1 }, 700, 'easeOutSine');
						$('.content7_imagepanel').delay(300).animate({ right: '0%'}, 700, 'easeOutSine');
					}
				});

				// Panel 8

				var content8Pos = $('.content_mainpanel_8').position();
				var content8Height = $('.content_mainpanel_8').height();

				$(window).scroll(function() {
					var scroll = $(window).scrollTop();
					if ( scroll + 1 >= Math.round(content8Pos.top) - 117) {	
						$('#nav_arrow').stop(true,false).animate({ left: affordablePos.left + 25 }, 300);
						$('.content8_check').delay(0).animate({ width: 210}, 1000);
					}
				});
				
				
				
				
				// Contact Us

				var ccontactPos = $('.contact_mainpanel').position();
				var contactHeight = $('.contact_mainpanel').height();
				
				$(window).scroll(function() {
					var scroll  = $(window).scrollTop();
					//callbacks when scroll happens
					
					if($(window).scrollTop() + $(window).height() == $(document).height() )  {	
						
						$('#nav_arrow').stop(true,false).animate({ left: contactPos.left + 22 }, 300);
						
					}
				});
				
				
		
				$('.quote').delay(1000).animate({ left: 0, opacity: 1}, 1000);
			
	
	
	
	}
	// end jQuery fire on desktop only	
	
	// jQuery fix for mobile
	else
	{
		// variable init
		$('.content3_panel img').fadeOut();
		function shine() {
			$('.content2_animpanel .content2_shiner').animate({ left: '100%'}, 1500);
			
			 window.setTimeout(function() { 
				mobileshine() 
				} , 2300)
		}
		function mobileloop() {
			$('.thumb_on').delay(300).animate({ height: 0}, 1000);
			$('#sensor').delay(300).animate({ top: 0}, 1000);
			
			$('#sensor').delay(300).fadeOut().animate({ top: 242}, 1000).fadeIn(3000);
			$('.thumb_on').delay(300).fadeOut().animate({ height: '100%'}, 1000).fadeIn(3000);
			
			 window.setTimeout(function() { mobileloop() }, 4600)
		}
		
		// animate to take effect even if scrollTo conditions is false
		
		$('.content2_panel').animate({ right: '0px'}, 1000);
		$('.content2_imagepanel').animate({ left: '0px'}, 1000, function(){
			mobileshine()
		});
		$('#content3_cursor').animate({ left: '16%', bottom: 310},2000, function(){
			$('#generate_payroll').css('background-position', '0px -75px');
			$('#content3_payroll').animate({ height: 'auto%', width: '100%', left: 0, top: 70 }, 700);
			$('.content3_panel img').delay(100).fadeIn(1000);
			$('#generate_payroll').fadeOut(500);
		});	
		$('.c4_img4').delay(200).animate({ bottom : '0%'}, 700, 'easeOutSine');
		$('.c4_img3').delay(500).animate({ bottom : '0%'}, 700, 'easeOutSine');
		$('.c4_img2').delay(600).animate({ bottom : '0%'}, 700, 'easeOutSine');
		$('.c4_img1').delay(700).animate({ bottom : '0%'}, 700, 'easeOutSine');
		$('.content5_panel').animate({ left: '0%', opacity: 1}, 500, 'easeOutSine');
		$('.content5_imagepanel').animate({ right: '20%'}, 'easeOutSine',function(){
			$('.c5_attention').animate({ height: 135, width: 150, left: '-10%', bottom: 0}, 500);
		});
		mobileloop()
		$('.content7_panel').animate({ left: 0, opacity: 1 }, 700, 'easeOutSine');
		$('.content8_check').animate({ width: 100}, 1000);
	}	
		}, pause );
	
	});
		
	$(window).resize();

	});		
});