$(document).ready(function(){

	$(".modal-trigger").off("click").on("click",function(){
		$("body").css({overflow:'hidden'});
		var tm = $(this).attr("modal-target");

		$("div[modal-id~='"+tm+"']").addClass("showed");

		$("div[modal-id~='"+tm+"'] .close-me").on("click",function(){
			$("body").css({'overflow-y':'initial'});
			$("div[modal-id~='"+tm+"']").removeClass("showed");
		});
	});

	//datepicker
	$(".dp:not(.time)").datetimepicker({pickTime: false});
	$(".dp.time").datetimepicker({pickDate: false});

	// navi
	$(".side-nav-container").mCustomScrollbar({
		scrollInertia:150
	});

	$(".nav-ticker").each(function(){
		$(this).off("click").on("click",function(e){
			e.preventDefault();
			var snh, snc;

			snh = $(this).next(".subnav-panel").find("ul").height();
			snc = $(this).next(".subnav-panel");
			
			snc.toggleClass("show");


			if(snc.hasClass("show")){
				snc.css({height:snh});
				setTimeout(function(){
					snc.removeAttr("style");
				},500)
			}else{
				snc.css({height:snh});
				setTimeout(function(){
					snc.removeAttr("style");
				},100)
			}

		});
	});

	// accordion
	$(".panel-group .panel-heading").each(function(){
		var ps = $(this).next(".panel-collapse");
		var ph = ps.find(".panel-body").outerHeight();

		if(ps.hasClass("in")){
			$(this).find("h4").addClass("active")
		}

		$(this).find("a").off("click").on("click",function(e){
			e.preventDefault();
			ps.css({height:ph});

			if(ps.hasClass("in")){
				$(this).find("h4").removeClass("active");
				$(this).find(".fa").removeClass("fa-caret-down").addClass("fa-caret-right");
				ps.removeClass("in");
			}else{
				$(this).find("h4").addClass("active");
				$(this).find(".fa").removeClass("fa-caret-up");
				ps.addClass("in");
			}

			setTimeout(function(){
				ps.removeAttr("style")
			},500);
		});
	});
	
	// notification
	$("header .announcement").off("click").on("click",function(){
		if($(".notif").hasClass("hide")){
			$(".notif").removeClass("hide");
			$(".notif").addClass("showing");
			setTimeout(function(){
				$(".notif").removeClass("showing");
				$(".notif").addClass("show");
			},200);
		}else{
			$(".notif").removeClass("show");
			$(".notif").addClass("hidding");
			setTimeout(function(){
				$(".notif").removeClass("hidding");
				$(".notif").addClass("hide");
			},200);
		}
	}).on("blur",function(){
		$(".notif").removeClass("show");
		$(".notif").addClass("hidding");
		setTimeout(function(){
			$(".notif").removeClass("hidding");
			$(".notif").addClass("hide");
		},200);
	});

	//setting
	$(".settings").off("click").on("click",function(){
		var setL = $(this).offset().left - 23.75;
		
		$(".settings-box").css({left:setL});
		if($(".settings-box").hasClass("showed")){
			$(".settings-box").stop().fadeOut(300,function(){
				$(this).removeClass("showed")
			});
		}else{
			$(".settings-box").stop().fadeIn(300,function(){
				$(this).addClass("showed")
			});
		}
	});

	// deals interact show/hide
	$(".show-hide-interact").off("click").on("click",function(){
		if($(".show-interact").hasClass("showed")){
			$(".show-interact").stop().fadeOut(500,function(){
				$(".edit-interact").stop().fadeIn(500);
				$(".show-interact").removeClass("showed");
			});
		}else{
			$(".edit-interact").stop().fadeOut(500,function(){
				$(".show-interact").stop().fadeIn(500);
				$(".show-interact").addClass("showed");
			});
		}
	});

	// deals
	$(".email.template").css({"display":"block"});
	$(".template-type").find(".option").off("click").on("click",function(){

		var val = $(this).attr("data-value");
		$(".template").removeAttr("style");
		$("."+val+".template").css({"display":"block"});
	});

	//tooltip
	$("body").append("<div class='show-dialogue'></div>");
	if($("body .tool-tip").length > 0){
		$(".tool-tip").each(function(){
			var tthtml = $(this).attr("tt-html");

			$(this).mouseenter(function(){
				var ttx = $(this).offset().left;
				var tty = $(this).offset().top - ($(".show-dialogue").height() + 30) ;
				$(".show-dialogue").css({top:tty,left:ttx}).html(tthtml).stop().fadeIn(300);
			}).mouseleave(function(){
				$(".show-dialogue").stop().html(tthtml).fadeOut(300);
			});
		});
	}	

	 $('.search-button1').off("click").on("click", function(){
        $('.container-show').toggleClass("show");        
        $('.search-button-display1').toggleClass("show");
     })
	 $('.search-button2').off("click").on("click", function() {
	 	$('.container-show1').toggleClass("show");
	 })

	 $('.search-button').off("click").on("click", function(){
        $('.search-button-display').toggleClass("show");
    })
	 

	 // receiving delivery detail
	 	// default value
	 $('.ship-name').hide();
	 $('.air-name').hide();
	 $('.item-input').hide();
	 $('.ship-next').hide();
	 $('.air-next').hide();
	 $('.item-next').hide();
	 $('.modepayment').hide();
	 $('.select-payment').hide();
	 $('.forward-name').hide();
	 $('.forwarder').hide();


	 $('.select div.option').click(function(){
	 	var deldetails = $(this).text();	 	
	 	switch (deldetails) {
	 		case 'Ship':
	 			$('.forward-name').hide();
	 			$('.forwarder').hide();
	 			$('.ship-name').show();
	 			$('.ship-next').show();
				$('.air-next').hide();
	 			$('.air-name').hide();
	 			$('.item-input').show();	 			
	 			$('.item-next').show();	 			
	 			break;
	 		case 'Air':
	 			$('.forward-name').hide();
	 			$('.forwarder').hide();
	 			$('.ship-name').hide();
	 			$('.ship-next').hide();
	 			$('.air-name').show();
	 			$('.air-next').show();
	 			$('.item-input').show();	 			
	 			$('.item-next').show();	 			
	 			break;
	 		case 'Forwarder':
	 			$('.forward-name').show();
	 			$('.forwarder').show();
	 			$('.ship-name').hide();
				$('.ship-next').hide();
				$('.air-name').hide();
				$('.air-next').hide();
				$('.item-input').hide();				
				$('.item-next').hide();
				break;
	 		case 'Empty':
	 			$('.forward-name').hide();
	 			$('.forwarder').hide();
	 			$('.ship-name').hide();
				$('.ship-next').hide();
				$('.air-name').hide();
				$('.air-next').hide();
				$('.item-input').hide();				
				$('.item-next').hide();
				break;
	 	}
	 })


	 //receiving radiobutton product transfer delivery 
	 $('input[name=delivery]').click(function() {
	 	if ($('.prodel').is(':checked')) {	 		
	 		$('.modepayment').show();
	 		$('.select-payment').show();

	 		$('.origin').hide(); 
	 		$('.select-origin').hide();	 		
	 		$('.mode-del').hide();
	 		$('.select-mode').hide();

	 		$('.air-next').hide();
	 		$('.air-name').hide();
 			$('.ship-name').hide();
 			$('.ship-next').hide()
			$('.item-input').hide();				
			$('.item-next').hide();

	 	} else {
	 		$('select-mode').val('empty');	 		
	 		$('.modepayment').hide();
	 		$('.select-payment').hide();

	 		$('.origin').show(); 
	 		$('.select-origin').show();	 		
	 		$('.mode-del').show();
	 		$('.select-mode').show();
	 	}})

	 $(".close-me").click(function() {
	 	
	 	$(".display-input").hide();
	 	$(".btn-modal-display").hide();
	 	$(".display-text").show();
	
	 });

	 $(".display-input").hide();
	 $(".btn-modal-display").hide();
	 $(".edit-display").off("click").on("click", function() {
	 	$(".display-input").show();
	 	$(".display-text").hide();
	 	$(".btn-modal-display").show();
	 });


	 $('.wh-select .select div.option').click(function() {
		var selectedValue = $(this).text();			
		switch(selectedValue) {
			case 'Product': 
				window.location.href="product-inventory-1.php"
				break;
			case 'Storage':
				window.location.href="product-inventory-2.php"
				break;
		}
	});


	 // loading accordion	 
 	// if ($(".loading-type .panel-collapse").hasClass("in") == true) {	 		
 	// 	$(".loading-type .panel-collapse .panel-body").addClass("padding-bottom-90");
 	// } else {
 	// 	$(".loading-type .panel-collapse .panel-body").removeClass(".padding-bottom-90");
 	// }	
	 

 	$(".content-bullet").hide();
	 // product inventory list
	 $(".bullet").off("click").on("click", function() {
	 	$(".content-list").fadeOut();
	 	$(".content-bullet").fadeIn();
	 	$(this).css({
	 		'color':'#000',
	 		'fontSize':'20px'
	 	});
	 	$(".list").css({
	 		'color':'#808080',
	 		'fontSize':'18px'
	 	});
	 });
	 $(".list").off("click").on("click",function() {
	 	$(".content-list").fadeIn();
	 	$(".content-bullet").fadeOut();
	 	$(this).css({
	 		'color':'#000',
	 		'fontSize':'20px'
	 	});
	 	$(".bullet").css({
	 		'color':'#808080',
	 		'fontSize':'18px'
	 	});
	 });
	 
	 // transfer product dropdown
	 $('.drop-button div.option').click(function () {
	 	var myValue = $(this).text();
	 	switch(myValue) {
	 		case 'Stock Goods':
	 			window.location.href="transfer-products-stock.php"
	 			break;
	 		case 'Consignee' :
	 			window.location.href="transfer-products.php"
	 			break;
	 	}
	 });

	 // withdraw product dropdown
	 $('.drop-button-withdraw div.option').click(function () {
	 	var myValue = $(this).text();
	 	switch(myValue) {
	 		case 'Stock Goods':
	 			window.location.href="withdraw-product-stock.php"
	 			break;
	 		case 'Consignee' :
	 			window.location.href="withdraw-product.php"
	 			break;
	 	}
	 });

	 // withdraw product radio button 
	$(".truck-tbl").hide();
	$(".vessel-radio label").off("click").on("click", function(){
		$(".truck-tbl").hide();
		$(".vessel-tbl").show();
		$(".mod-text").text("SRS No.");
	});
	$(".truck-radio label").off("click").on("click", function(){
		$(".truck-tbl").show();
		$(".vessel-tbl").hide();
		$(".mod-text").text("Scale Ticket No.:");
	});


	// withdraw modal
	$(".void-button").off("click").on("click", function() {
		$(".button-content button").hide();
		$(".status-result").text("Void");
		$(".date-trans").text("N/A");
	})

	$(".complete-button").off("click").on("click", function(){
		$(".button-content button").hide();
		$(".status-result").text("Complete");
		$(".method-loading").text("Manual Labor");
		$(".area-loading").text("Area 54");
		$(".date-trans").text("September 11, 2015");
		$(".method-loading").text("Manual Labor");
		$(".time-start-loading").text("09/11/15 | 09:00 AM");
		$(".time-end-loading").text("09/11/15 | 09:00 AM");
		$(".duration-loading").text("1 Day, 0 Hours");
		$(".gross-weight").text("1500 Kilograms");
		$(".net-weight").text("500 Kilograms");
		$(".final-weight").text("1000 Kilograms");
	})


	// accordion for product-inventory 
	$(".panel-title").off("click").on("click", function() {		
		$(".panel-title i").removeClass("fa-caret-right").addClass("fa-caret-down");
	});
	$(".panel-title .caret-click").off("click").on("click", function() {
		$(this).removeClass(".fa-caret-right").addClass(".fa-caret-down");
	});

	// hover button on product inventory	
	$(".with-button button").hide();
	$(".with-button").hover(function() {
		$(".with-button button").fadeIn();
	});

	$(".with-button").mouseleave(function() {
		$(".with-button button").fadeOut();
	})
});
