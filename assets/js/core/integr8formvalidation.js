/**
 * Form Validation v 0.1.2
 * will handle form validation upon submission.
 * simple form validation from Integr8 
 *
 * @author: Jonathan Subion
 * @adviser: Dominic Canillo
 *
 *  Changelog:
 *      v 0.1.2
 *          -Added multiple email check support (split using ",")
 *          -Added comments
 *      v 0.1
 *          -initial check
 */
(function($)
{
    'use strict';
    /**
     *  Configurations
     *  @oDefaults = default settings upon initialization.
     */
    var oConfig = {},
    oDefaults = {
        'min_length' : 0,
        'max_length' : 0,
        'data_validation' : 'data-validation',
        'input_label' : 'input-label'
        },
    oMethods = //for future use, as of now, its no use.
        {
            'destroy' : function(param)
            {
                console.log("destroy");
            },
            'set_message' : function(param)
            {
                console.log("set message");
            }
        },
    bCheckBoxFlag = false,

    /**
     *  Set of Validation Rules.
     *  @message = "correct" signifies that the value will pass through the check
     *  if not, the message will appear on the array return.
     *
     *  returns a string composition of "correct" or an "error message"
     */
    oValidationRules = 
        {
            'numeric' : //checks if value contains numerical numbers only.
	            function(val,label)
	            {
                    var message = label +" is not numeric";
        			var numericExpression = /^[0-9]+$/;
	            	if(val.match(numericExpression))
                    {
                        message = "correct";
	            	}
	            	return message;
	            },
            'alpha' : //checks if value contains alphabetical letters only
            	function(val,label)
	            {
                    var message = label +" is not alphabetical";
        			var alphaExp = /^[a-zA-Z]+$/;
	            	if(val.match(alphaExp))
	            	{
	            		message = "correct";
	            	}
	            	return message;
	            },
            'alpha_numeric' : //checks if value contains alpha-numeric only
            	function(val,label)
	            {
                    var message = label +" is not alphanumeric";
        			var alphaNumExp = /^[0-9a-zA-Z]+$/;
	            	if(val.match(alphaNumExp))
	            	{
	            		message = "correct";
	            	}
	            	return message;
	            },
            'email' : //checks if an email is a valid email address
            	function(val,label)
	            {
                    var message = label+" "+val+" is not a valid email address";
        			var emailExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	            	var iCountforSplit = 0;
                    var myvalues = "";
                    var splChars = ",";
                    for (var i = 0; i < val.length; i++)
                    {
                        if (splChars.indexOf(val.charAt(i)) != -1) //check if email has "," for split
                        {
                            iCountforSplit++;
                        }
                    }
                    if(iCountforSplit > 0)
                    {
                        val = val.split(",");
                        $.each(val, function(k, v)
                        {
                            if (!v.match(emailExp))
                            {
                                myvalues += v + " "; //error emails in one var @myvalues
                            }
                        });
                        if(myvalues != "")
                        {
                            message = label+" "+myvalues+" is not a valid email address"; //error message adding all error emails
                        }
                        else
                        {
                            message = "correct";
                        }
                    }
                    else
                    {
                        if(val.match(emailExp))
                        {
                            message = "correct";
                        }
                    }
	            	return message;
	            },
            'required': // checks if value is not null
            	function(val,label)
	            {
                    var message = "correct";
        			if(val.length == 0)
	            	{
	            		message = label +" is required";
	            	}
	            	return message;
	            },
	        'cellphone': //checks if value is a cellphone number, also workds with telephone number
	        	function(val,label)
	        	{
                    var message = label+" is not a valid cellphone number ";
                    var phoneno = /^\d{10}$/;
        			var cellno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                    var intno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
                    var cellno_international = /^\+?([0-9]{1,3})\)?[-. ]?([0-9]{1,3})?[-. ]?([0-9]{1,4})?[-. ]?([0-9]{1,4})$/;
                    var cellno_domestic = /^\(?([0-9]{3,4})\)?[-. ]?([0-9]{3,4})[-. ]?([0-9]{3,4})$/;
					if(val.match(phoneno) || val.match(cellno) || val.match(intno) || val.match(cellno_international) || val.match(cellno_domestic))
	            	{
	            		message = "correct";
	            	}
	        		return message;
	        	},
	        'isdecimal': //checks if value is a number with decimal
	        	function(val,label)
	        	{
                    var message = label+" is not a decimal number ";
        			var decimal = /^[0-9]+\.[0-9]+$/; 
					if(val.match(decimal))
	            	{
	            		message = "correct";
	            	}
                    return message;
	        	}
        };

    /**
     *  Function for checking the length of the input value
     *  @iLength = integer containing the length of the value, (required)
     *  @iMin = integer for minimum length
     *  @iMax = integer for maximum length
     *  returns the boolean "check"
     */
    function lengthcheck(iLength,iMin,iMax)
    {
     	var check = false;
     	if(iMin == 0 && iMax == 0) //checks if max/min lengths are unset
     	{
     		check = true;
     	}
        else if (iMin == 0 || iMax == 0) //checks if only one (min,max) is set
        {
            if(iMin == 0)
            {
                if(iMax >= iLength)
                {
                    check = true;
                }
            }
            else if(iMax == 0)
            {
                if(iMin <= iLength)
                {
                    check = true;
                }
            }
        }
     	else
     	{
     		if(iMin <= iLength && iMax >= iLength) //checks when both (min,max) is set
     		{
     			check = true;
     		}
     	}
     	return check;
    }

     /**
     *  Function for comparing the values
     *  @value = 1st value to be compared
     *  @sCompare = string name of the attribute to be compared
     *  @input_label = string of the attribute from Configuration 
     *  returns the boolean "check"
     */
    function comparevalues(value,sCompare,input_label)
    {
        var value2 = $( "*["+input_label+"='"+sCompare+"']" ).val();
        if(value == value2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

     /**
     *  function for XSS filtering
     *  removes tags like "<script></script>"
     *  ex. value -> "<script>alert('doomed~');</script>"
     *  the result will be "alert('doomed~')".
     *
     *  @text - the value of the input text, $(this).val()
     *
     *  returns the "stripped" version of the input
     */
    function stripHTML(text){
        var regex = /(<([^>]+)>)/ig;
        return text.replace(regex, "");
    } 

    /**
     *  function for running the form validation
     *  @uiElem - contains the element of the formvalidation (usually form), inserted as $(this) from the main function
     *  @param - configuration of the user (not required)
     *
     *  return: an array, will return a null value of array if validation is a success
    */
    function initialize(uiElem, param)
    {
    	oConfig = $.extend(oDefaults, param);
    	var result = [];
        bCheckBoxFlag = false;
        uiElem.find("input, select, textarea").each(function(){
        	if(typeof($(this).attr(oConfig.data_validation)) != 'undefined') //checks if data_validation attribute is present on the element
        	{
                var ui = $(this);
        		var arrRules = $(this).attr(oConfig.data_validation); //data_validation values
        		var mylabel = $(this).attr(oConfig.input_label); // input_label value
        		var value = stripHTML($(this).val()); // stripped version of the value
                var iMin = oConfig.min_length;
                var iMax = oConfig.max_length;
        		if(typeof(oConfig[mylabel]) != 'undefined') //checks for specific rules 
        		{
                    if(typeof(oConfig[mylabel]['min_length']) != 'undefined')
                    {
                        iMin = oConfig[mylabel]['min_length'];
                    }
                    if(typeof(oConfig[mylabel]['max_length']) != 'undefined')
                    {
                        iMax = oConfig[mylabel]['max_length'];
                    }
                    if(typeof(oConfig[mylabel]['compare_input']) != 'undefined')
                    {
                        var sCompareInput = oConfig[mylabel]['compare_input'];
                        if(!comparevalues(value,sCompareInput,oConfig.input_label)) //comparevalues
                        {
                            result.push({'error_message':mylabel+" does not match with "+sCompareInput});
                        }
                    }
        		}
                if(!lengthcheck(value.length,iMin,iMax)) //lengthcheck
                {
                    result.push({'error_message':mylabel+"'s length does not match with form's min-max value"});
                }
        		arrRules = arrRules.split(" ");
                $.each(arrRules, function(k, v)
                {
                    if (typeof (oValidationRules[v]) == 'function')
                    {// oValidationRules[v] is a function from oValidationRules, 
                    	var checkhold = oValidationRules[v]( value, mylabel );
                    	if(checkhold != 'correct')
                    	{
                    		result.push({'error_message':checkhold});
                    	}
                    }
                });
        	}
        });

        // Check for checkbox at least one selected rule
        if(uiElem.find('input['+oConfig.data_validation+'="checkbox-atleast-one"]').length > 0){
            var uiCheckbox = uiElem.find('input['+oConfig.data_validation+'="checkbox-atleast-one"]'),
                iCountElem = uiCheckbox.length,
                sCheckBoxLabel = uiCheckbox.attr(oConfig.input_label),
                errorCount = 0;
            $.each(uiCheckbox, function(index, el) {
                if($(el).prop('checked') === false){
                    errorCount++;
                }
            });

            if(iCountElem === errorCount){
                result.push({'error_message' : 'Select at least one from '+sCheckBoxLabel});
            }
        }
		return result;
    }
    /**
     *  Main function for cr8vformvalidation
     *  @param -> configurations of the user
     *  @Successful(no 'error_message') -> submit form
     *  @Fail - > stops the form on submittions, then run the "onValidationError" function (if available) from param
     *
     */
    $.fn.cr8vformvalidation = function(param)
    {
        $(this).off('submit.integr8formvalidation').on("submit.integr8formvalidation",function(e){
            if(param.preventDefault){
                e.preventDefault();
            }
            var result = initialize($(this),param);  
            if(result.length <= 0)
            {//Successful
                if(typeof(oConfig.onValidationSuccess) == "function"){
                    oConfig.onValidationSuccess(e);
                }
            }
            else
            {//Fail
                e.preventDefault();
                if(typeof(oConfig.onValidationError) == "function"){
                    oConfig.onValidationError(result);
                }
            }
        });

    }
})(jQuery);

/**

v 0.1.2 validation rules list
numeric
alpha
alpha_numeric
email
required
cellphone
isdecimal

***sample usage

<input type="text" datavalid="email required" labelinput="Email Add" name="myemail"/>

$("#myform").integr8formvalidation({
    'data_validation' : 'datavalid',
    'input_label' : 'labelinput',
    'min_length': 2,
    'max_length': 20,
        'Password': 
        {
            'min_length': 4,
            'max_length': 15
        },
        'Username': {
            'min_length': 6,
            'max_length': 8
        },
        'Confirm Password':
        {
            'compare_input': 'Password'
        },
    'onValidationError': function(arrMessages) {
        alert(JSON.stringify(arrMessages)); //shows the errors
        },
    'onValidationSuccess': function() {
        alert("Success"); //function if you have something to do upon submission
        }
});

**/