var CGetProfileImage = (function(){

	function getDefault(img)
	{
		var profile = "",
			buildImg = "";

		profile = img.trim().charAt(0).toLowerCase();
		buildImg = BASEURL+"assets/images/profile/profile_"+profile+".png";

		return buildImg;
	}

	return {
		getDefault : getDefault
	}

})();

