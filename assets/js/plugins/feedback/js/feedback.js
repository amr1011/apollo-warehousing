/*
*
* System Feedback Framework 2.0 
* @author	Dru Moncatar
* @docs	on going..
*
* A jQuery plugin which implements a feedback system for basic 
* kind of events in the web application.
*
*/

(function($) {

	$.fn.feedback = function(oOptions) 
	{
		var feedbackName = "myFeedback";
		var feedbackTitle = null;
		var feedbackMessage = null;
		var feedbackFooter = null;
		$('#' + feedbackName).remove();
		var element = $(this);
		var oSettings = {};

		function _bindEvents()
		{
			$("#feedback-closer").click(function(){
				$("#" + feedbackName).hide();
			});
		}

		function _setDefaults() 
		{
			oSettings =  oOptions || {};
			if(oSettings.form == undefined) { oSettings.form = "box"; }
			if(oSettings.authoShow == undefined) { oSettings.authoShow = true; }
			if(oSettings.type == undefined) { oSettings.type = "default"; }
			if(oSettings.title == undefined) { oSettings.title = "Title here"; }
			if(oSettings.message == undefined) { oSettings.message = "Message"; }
			if(oSettings.speed == undefined) { oSettings.speed = "slow"; }
			if(oSettings.icon == undefined) { oSettings.icon = "http://localhost/apollo/assets/js/plugins/feedback/images/success.svg"; }
			if(oSettings.withShadow == undefined) { oSettings.withShadow = false; }

			return oSettings;
		}

		function _getTemplate() 
		{
			var html = "<div id='" + feedbackName + "' class='feedback-body feedback-centered' style=''>"
			+	"<div id='feedback-icon'>"	
			+		"<img style='height:70px; width:50px;' src=" + oSettings.icon + ">"
			+	"</div>"
			+	"<div id='feedback-contents'>"
			+		"<div id='feedback-title'>"
			+		"</div>"
			+		"<div id='feedback-message'>"
			+		"</div>"
			+	"</div>"
			+	"<div id='feedback-callbacks'>"
			+		"<div id='feedback-closer'>"
			+			"<i class='fa fa-times-circle fa-2x' aria-hidden='true'></i>"
			+		"</div>"
			+		"<div id='feedback-reply'>"
			//+			"<"
			+		"</div>"
			+	"</div>";

			return html;
		}

		function _setFeedbackDom() 
		{
			feedbackIcon = $("#feedback-icon");
			feedbackTitle = $("#feedback-title");
			feedbackMessage = $("#feedback-message");

			return {
				feedbackTitle,
				feedbackMessage,
				feedbackIcon
			};
		}

		function _setFeedbackForm(theFeedback) 
		{
			switch(oSettings.form) {
				case "box":
					theFeedback.removeClass("rounded");
					theFeedback.addClass("box");
					theFeedback.removeClass("inline");
				break;
				case "rounded":
					theFeedback.addClass("rounded");
					theFeedback.removeClass("box");
					theFeedback.removeClass("inline");
				break;
				case "inline":
					theFeedback.removeClass("rounded");
					theFeedback.removeClass("box");
					theFeedback.addClass("inline");
				break;
				default:
					theFeedback.removeClass("rounded");
					theFeedback.removeClass("box");
					theFeedback.removeClass("inline");
				break;
			}
			if(oSettings.form == "box") {
				theFeedback.addClass("box")
			} else {
				theFeedback.addClass("default");
			}
			return theFeedback;
		}

		function _setIconForm(theIcon)
		{
			if(oSettings.form == "rounded") {
				theIcon.addClass("icon-rounded");	
			} else {
				theIcon.removeClass("icon-rounded");
			}
			return theIcon;
		}

		function _setFeedbackType(theFeedback) 
		{
			switch(oSettings.type) {
				case "success" :
					theFeedback.removeClass("warning");
					theFeedback.removeClass("danger");
					theFeedback.removeClass("default");
					theFeedback.addClass("success");
				break;

				case "warning" :
					theFeedback.removeClass("success");
					theFeedback.removeClass("danger");
					theFeedback.removeClass("default");
					theFeedback.addClass("warning");
				break;

				case "danger" :
					theFeedback.removeClass("warning");
					theFeedback.removeClass("success");
					theFeedback.removeClass("default");
					theFeedback.addClass("danger");
				break;

				case "default" :
					theFeedback.removeClass("warning");
					theFeedback.removeClass("danger");
					theFeedback.removeClass("success");
					theFeedback.addClass("default");
				break;

				default :
					theFeedback.removeClass("warning");
					theFeedback.removeClass("danger");
					theFeedback.removeClass("success");
					theFeedback.addClass("default");
				break;
			}

			return theFeedback;
		}

		function _setFeedbackTitle(feedbackTitle) 
		{
			feedbackTitle.html(oSettings.title);
			return feedbackTitle;
		}

		function _setFeedbackMessage(feedbackMessage) 
		{
			feedbackMessage.html(oSettings.message);
			return feedbackMessage;
		}

		function _setFeedbackIcon(feedbackIcon) 
		{
			if(oSettings.form == "rounded")	{
				feedbackIcon.html("<img id='feedback_icon' src=" + oSettings.icon + " style='height:70px; width:70px;' class='icon-rounded'>");
			} else {
				feedbackIcon.html("<img id='feedback_icon' src=" + oSettings.icon + " style='height:70px; width:70px;'>");
			}
			
			return feedbackIcon;
		}

		function _setFeedbackShadow(theFeedback)
		{
			if(oSettings.withShadow)
			{
				theFeedback.addClass("with-shadow");
			} else
			{
				theFeedback.removeClass("with-shadow");
			}
			return theFeedback;
		}

		function _setFeedbackShow(theFeedback)
		{
			if(oSettings.authoShow) {
				theFeedback.show();
			} else {
				theFeedback.css("display", "none");
			}
			return theFeedback;
		}

		function _setFeedbackSpeed(theFeedback)
		{
			switch(oSettings.speed) {
				case "slow":
					//theFeedback.fadeOut(12000);
				break;

				case "mid":
					//theFeedback.fadeOut(10000);
				break;

				case "fast":
					//theFeedback.fadeOut(4000);
				break;
			}
			return theFeedback;
		}

		function _renderUi(theFeedback) 
		{
			element.append(theFeedback);
			return element;
		}

		function _buildFeedback(oSettings) 
		{	
			var oTemplate = _getTemplate();
			var theFeedback = $(oTemplate);

			oSettings = _setDefaults();
			_setFeedbackForm(theFeedback);
			_setFeedbackType(theFeedback);
			_setFeedbackShadow(theFeedback);
			_setFeedbackShow(theFeedback);
			_renderUi(theFeedback);
			_setFeedbackSpeed(theFeedback);
			return element;
		}

		function _setFeedbackMesages(oSettings) 
		{
			var theIcon = $("#feedback_icon");
			_setFeedbackDom();
			_setFeedbackIcon(feedbackIcon);
			_setFeedbackTitle(feedbackTitle);
			_setFeedbackMessage(feedbackMessage);
			_setIconForm(theIcon);
			return element;
		}

		function _initialize()
		{
			_buildFeedback(oSettings);
			_setFeedbackMesages(oSettings);
			_bindEvents();
		}

		_initialize();
		return element;
	}
})(jQuery);

/*
* SAMPLE USAGE
*var option = {
*	title : "This is the title",
*	message : "This is the message",
*	speed : "slow",
*	form : "rounded",
*	autoShow : false,
*	withShadow : false,
*	icon : "localhost/yoursite/images/sample.png"
*};
*$("body").feedback(option);
*/
