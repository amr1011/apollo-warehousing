(function($){

	$.fn.cr8vFileUpload = function(options){

		/* The element currently working on */
		ui = this;

		if(!options){
			if(typeof options == 'string' && options.toLowerCase() == 'getdata'){
				return ui.data("cr8vFileUpload");
			}
		}


		ui.data("cr8vFileUpload", {
			"files" : [],
			"originalElement" : ui
		});


		/*
		* Default options for building the upload file
		*/
		var defaults = {
			url : "",
			accept : "*",
			filename : "attachment",
			extraField : {},
			onSelected : function(){},
			success : function(){},
			error : function(){}
		},

		/*
		* Variable for overrided default values
		*/
		opts = $.extend(defaults, options),

		bSelectedFile = false;
		

		function updateData(key, val){
			var oData = ui.data("cr8vFileUpload");
			oData[key] = val;
			ui.data("cr8vFileUpload", oData);
		}

		function getArrayAccepted(){
			if(opts.accept !== '*'){
				return opts.accept.split(',');
			}else{
				return opts.accept;
			}
		}

		function isAccepted(arrAccepted, ext){
			for(var i in arrAccepted){
				if(arrAccepted[i] == ext){
					return true;
				}
			}
			return false;
		}

		function showInvalidFile()
		{
			var uiModal = $("[cr8v-file-upload='modal']"),
				uiContent = uiModal.find('.modal-content');

			var uiMessageContainer = $("<div class='message-container'></div>"),
				uiMessage = $("<div class='message'></div>");

			
			uiMessageContainer.css({
				"position" : "absolute",
				"background-color" : "rgba(136, 136, 136, 0.7)",
				"top" : "0px",
				"left" : "0px",
				"width" : "100%",
				"height" : "100%"
			});

			uiMessage.css({
				"padding" : "10px",
				"background-color" : "rgb(255, 255, 255)",
				"color" : "#111",
				"width" : "200px",
				"min-height" : "40px",
				"line-height" : "16px",
				"font-size" : "20px",
				"border-radius" : "10px",
				"text-align" : "center",
				"box-shadow" : "0px 3px 5px #888",
				"position" : "absolute",
				"top" : "20%",
				"left" : "30%"
			});

			uiMessage.html("Invalid selected file");

			uiMessage.appendTo(uiMessageContainer);
			uiMessageContainer.appendTo(uiContent);

			setTimeout(function(){
				uiMessageContainer.remove();
			},2000);

		}

		function inputEvent(uiInput){

			uiInput.off("change").on("change", function(e){
				var fileName = $(this).val(),
					ext = (fileName.substr(fileName.lastIndexOf('.') + 1)).toLowerCase(),
					uiForm = ui.find('form'),
					arrAccepted = getArrayAccepted(),
					bIsAccepted = isAccepted(arrAccepted, ext),
					uiModal = $('[cr8v-file-upload="modal"]'),
					inputDocName = uiModal.find('.document-name');

				if(bIsAccepted){
					if($(this)[0].files.length == 1){
						inputDocName.val($(this)[0].files[0]["name"]);
					}
					bSelectedFile = true;
					opts.onSelected(true);
					return true;
				}else{
					bSelectedFile = false;
					showInvalidFile();
					opts.onSelected(false);
					return false;
				}
			});
		}

		function bindModalEvents(){
			var uiModal = $('[cr8v-file-upload="modal"]'),
				uiFileName = uiModal.find(".document-name"),
				uiUpload = uiModal.find(".confirm-upload"),
				uiBrowser = uiModal.find(".browse"),
				uiClose = uiModal.find(".close-me");

			uiClose.off('click.cr8vFileUploadClose').on('click.cr8vFileUploadClose', function(e){
				e.stopPropagation();
				uiModal.removeClass('showed');
				uiFileName.val("");
			});

			uiBrowser.off("click.cr8vFileUploadBrowse").on("click.cr8vFileUploadBrowse", function(){
				ui.find("input[type='file']").trigger('click');
			});


			uiUpload.off('click.cr8vFileUploadUpload').on('click.cr8vFileUploadUpload', function(e){
				e.stopPropagation();
				e.preventDefault();
				var uiThis = $(this);
				if(bSelectedFile){
					var uifile = ui.find('input[type="file"]');
					uifile.attr("name", opts.filename);
					var files = uifile[0].files;
					var data = new FormData();
					$.each(files, function(key, value){
				        data.append(opts.filename, value);
				    });

				    for(var i in opts.extraField){
				    	data.append(i, opts.extraField[i]);
				    }
				    if(uiFileName.val().trim().length > 0){
				    	data.append("name", uiFileName.val());
				    }

					data.append("csrf_apollo_token", $.cookie('csrf_apollo_cookie'));

				    $.ajax({
				    	type : "post",
				    	url : opts.url,
				    	data : data,
				    	dataType : 'json',
				    	processData : false,
				    	contentType : false,
				    	beforSend : uiThis.html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="color: #fff; font-size: 17px;"></i>'),
				    	success: opts.success,
				    	compelete : uiThis.html('Upload'),
				        error: opts.error
				    });

				    bindModalEvents();
				    var newUiInput = $("<input type='file' name='attachment' style='display:none;' />");
				    uifile.replaceWith(newUiInput);
				    inputEvent(newUiInput);
				}
			});
		}


		function showModal(){
			$('[cr8v-file-upload="modal"]').addClass('showed');
			bindModalEvents();
		}

		function clickEvent(){
			ui.off('click.cr8vFileUpload').on('click.cr8vFileUpload', function(e){
				e.stopPropagation();
				showModal();
			});
		}

		function appendInputFile(){	
			var uiInput = $("<input type='file' name='"+opts.filename+"' />");
			uiInput.hide();
			ui.append(uiInput);
			updateData("input", uiInput);
			inputEvent(uiInput);
		}

		appendInputFile();
		clickEvent();
	};

	/* lets create modal html */
	if($('[cr8v-file-upload="modal"]').length == 0){
		var sHtml = '<div class="modal-container" cr8v-file-upload="modal">'+
					'<div class="modal-body small">'+
						'<div class="modal-head">'+
							'<h4 class="text-left">Upload Document</h4>	'+	
							'<div class="modal-close close-me"></div>'+
						'</div>'+
						'<div class="modal-content text-left" style="position:relative;">'+
							'<div class="text-left padding-bottom-10">'+
								'<p class="font-14 font-400 no-margin-all display-inline-mid width-125px">Document Name:</p>'+
								'<input type="text" class="width-200px display-inline-mid document-name" />'+
								'<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 browse">Browse</button>'+
							'</div>'+
						'</div>'+
						'<div class="modal-footer text-right">'+
							'<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>'+
							'<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 confirm-upload">Upload</button>'+
						'</div>'+
					'</div>'+
				'</div>';
		$("body").append(sHtml);
	}

})(jQuery);

/*
$("button").cr8vFileUpload({
	url : "path/where/to/upload",
	accept : "pdf,csv",
	filename : "attachment",
	extraField : { otherdata: "myname" },
	onSelected : function(){},
	success : function(){},
	error : function(){}
});
*/