(function($){

	$.fn.cr8vUpload = function(options){

		/*
		* Default options for building the upload file
		*/
		var defaults = {
			url : "",
			backgroundPhotoUrl : "",
			autoUpload : false,
			accept : "*",
			multiple : false,
			fileName : "attachment",
			titleText : "Upload Photo",
			backgroundColor : "#d8d8d8",
			extraField : {},
			onSelected : function(){},
			success : function(){},
			error : function(){}
		},

		/*
		* Variable for overrided default values
		*/
		opts = $.extend(defaults, options),

		/* This will be the instance when plugin was called*/
		instance = {},
		
		/* The element currently working on */
		ui = this;

		var methods = {
			'generateID' : function(){
				var chars = "ABCDEFGHIJKLMOPQRSTUVWXYZ",
					charLen = 12,
					result = "";

				for( var i=0; i < charLen; i++ ){
					result += chars.charAt(Math.floor(Math.random() * chars.length));
				}

				return result;
			},
			'generateAcceptArray' : function(){
				if(opts.accept !== '*'){
					return opts.accept.split(',');
				}else{
					return opts.accept;
				}
			},
			'init' : function(){
				ui.attr("cr8v-upload", "true");
				instance['opts'] = opts;
				instance['original_ui'] = ui;
				instance['accepted'] = methods.generateAcceptArray();
				return methods.build();
			},
			'template' : function(){
				var sHtml = '<div class="upload-wrapper">';
					sHtml += '<div class="camera-title-wrapper">';
						sHtml += '<div class="camera"> </div>';
						sHtml += '<p class="title">'+opts.titleText+'</p>';
					sHtml += '</div>';
					sHtml += '<div class="message-holder"></div>';
				sHtml += '</div>';

				return $(sHtml);
			},
			'formTempalte' : function(){
				var sHtml = '<form>';
					sHtml += '<input type="file" name="'+opts.fileName+'" />';
				sHtml += '</form>';

				var uiForm = $(sHtml);

				if(opts.multiple){
					var input = uiForm.find('input');

					input.prop({
						"name" : opts.fileName+"[]",
						"multiple" : true
					});
				}

				uiForm.css("display", "none");

				return uiForm;
			},
			'setCSS' : function(uiElem){
				var uploadSquare = uiElem,
					cameraTitleWrapper = uiElem.find('.camera-title-wrapper'),
					messageHolder = uiElem.find('.message-holder'),
					cameraHolder = uiElem.find('.camera'),
					titleContainer = uiElem.find('.title');

				uploadSquare.css({
					"background-color" : opts.backgroundColor,
					"border" : "1px solid rgba(0,0,0,0.2)",
					"cursor" : "pointer",
					"height" : "130px",
					"width" : "130px",
					"text-align" : "center",
					"margin" : "5px",
					"font-family" : "Arial",
					"position" : "relative"
				});

				cameraHolder.css({
					"display" : "inline-block",
					"background-image" : "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAp1JREFUaIHtmc9rE0EUxz9JiqnanmyRiooHTxUphXoVyVHwpqV60Jv+DV4FIaCIotJQPEhV6EkvXk1FPXjxJFoMGFO9KOLBH4dGaNbD24Vlskt23m5mE8wXHmQW3ne+38nOzJtZGGF4UQYuAx+AbcBTxDegBuxzrJ0x4KVSdFR8BQ65NHAhQ/FB3HclvgC864OBv8ABFwZO9UF8EDddGHjVRwN/gD1aYTNAFXgL/O6jSG20gQZwFzhsij8J/BwAkUljCzgfiF/w3eUtyja2gRMArwdAjDbeFPwfQ4ti3gLSYixjviawBqwjm90P//kUMAtUgLNkXDZk8S42gdMk+0eLwCLQyqjv1ASrwK6EgxXGbuBh3gaqCuEmrudlYDVG0DxS4zeQ/aWNnBmWgbmYnEeuDTTpfm12Avd65HV8c+NG7gSw6dLAmQjxLyzyn0eYWHJl4CPdq02vkY+KZYOjiG5lsk64anQ8rxy5Dt1zomrLo9mJ1432JQUHyMnuovGsriGyHbkZI7+h4Ahiw+Dab8uhKebKyPk1QBvYYckRzg1P5jJS6yfG0BdzGgNTRruVov+m0Z62JdAYmDXaqonn45nRPmJLoDFQMdo17OcRfs5KD+7ERDbxiW7jNQXPHYOjhK6cUC1/i0bn40h5kDS/jqw4YZxTalEltZB63jRRQ3bYuLwOMvKm+Engi0sDHnIYicIccvm0gazpW8B74DZwNCZnLYUOdaIHXIsRZIMbKTWkSvaQw8iEQvgk6UY+MwMesnoskWxZLiET9nMWfWd9sbWJjGodee+/+8+nkU2qghg9mFWHo5u5vFEC9gLH8haixArIv/CAbCazy3hC6Gq0AFxB/73Xddwi5l53AXjMYH6t+QU8BY5HCR9hhP8V/wB6jQ3akmHdQgAAAABJRU5ErkJggg==')",
					"background-repeat" : "no-repeat",
					"background-size" : "contain",
					"margin-top" : "30px",
					"width" : "38px",
					"height" : "35px",
					"opacity" : "0.5",
					"top" : "10%",
					"left" : 0,
					"right" : 0,
					"margin-left" : "auto",
					"margin-right" : "auto",
					"position" : "absolute"
				});

				titleContainer.css({
					"text-align" : "center",
					"font-size" : "15px",
					"color" : "#6C7A89",
					"position" : "absolute",
					"left" : 0,
					"right" : 0,
					"margin-left" : "auto",
					"margin-right" : "auto",
					"top" : "65%"
				});

				cameraTitleWrapper.css({
					"position" : "relative",
					"height" : "100%",
					"width" : "100%"
				});

				messageHolder.css({
					"position" : "absolute",
					"bottom" : "0px",
					"height" : "40px",
					"width" : "100%",
					"background-color" : "rgba(0,0,0,0.5)",
					"color" : "#fff",
					"text-align" : "center",
					"font-size" : "13px",
					"justify-content" : "center",
					"align-items" : "center",
					"display" : "none"
				});

			},
			'setBackgroundImage' : function(uiElem){
				var uploadSquare = uiElem,
					cameraTitleWrapper = uiElem.find('.camera-title-wrapper'),
					cameraHolder = uiElem.find('.camera'),
					titleContainer = uiElem.find('.title');

				if(opts.backgroundPhotoUrl){

					uiElem.css({
						"background-image" : "url("+opts.backgroundPhotoUrl+")",
						"background-size" : "cover",
						"background-repeat" : "no-repeat"
					});

					cameraTitleWrapper.hide();
					cameraTitleWrapper.css("background-color","rgba(0,0,0,0.5)");

					titleContainer.css({
						"color" : "#fff"
					});

					titleContainer.html("Change Photo");

					cameraHolder.css({
						"filter" : "invert(100%)",
						"-webkit-filter" : "invert(100%)",
						"opacity" : "1"
					});

					uploadSquare.hover(
						function(){
							cameraTitleWrapper.fadeIn( "fast" );
						},
						function(){
							cameraTitleWrapper.hide();
						}
					);
				}

			},
			'isAccepted' : function(arrAccepted, ext){
				for(var i in arrAccepted){
					if(arrAccepted[i] == ext){
						return true;
					}
				}
				return false;
			},
			'showInvalidFile' : function(ui){
				var messageHolder = ui.find('.message-holder');
				messageHolder.html("Invalid file format");
				messageHolder.css("display" , "flex");
				messageHolder.show();
				setTimeout(function(){ messageHolder.hide(); },3000);
			},
			'bindEvent' : function(uiElem){
				var currUiInput = uiElem.find('input[type="file"]'),
					uiForm = uiElem.find('form');

				uiForm.on('submit', function(e){
					e.preventDefault();
					e.stopPropagation();

					var files = $(this).find('input[type="file"]')[0].files;
					var data = new FormData();
					$.each(files, function(key, value){
				        data.append(opts.fileName, value);
				    });

				    for(var i in opts.extraField){
				    	data.append(i, opts.extraField[i]);
				    }

					data.append("csrf_apollo_token", $.cookie('csrf_apollo_cookie'));

				    $.ajax({
				    	type : "post",
				    	url : opts.url,
				    	data : data,
				    	dataType : 'json',
				    	processData : false,
				    	contentType : false,
				    	success: opts.success,
				        error: opts.error
				    });

				});

				currUiInput.on('click', function(e){
					e.stopPropagation();
				});

				currUiInput.on("change", function(e){
					var fileName = $(this).val(),
						ext = (fileName.substr(fileName.lastIndexOf('.') + 1)).toLowerCase(),
						uiParent = $(this).closest('[cr8v-upload]'),
						uiForm = uiParent.find('form'),
						uploadWrapper = uiParent.find('.upload-wrapper'),
						oData = uiParent.data(),
						arrAccepted = oData.accepted,
						bIsAccepted = methods.isAccepted(arrAccepted, ext);

					if(bIsAccepted){
						if($(this)[0].files.length == 1){
							if(opts.autoUpload){
						    	uiForm.submit();
						    }
							var fr = new FileReader();
						    fr.onload = function(){
						    	opts.backgroundPhotoUrl = fr.result;
						        methods.setBackgroundImage(uploadWrapper);
						    }
						    fr.readAsDataURL($(this)[0].files[0]);
						}else{

						}
						
						opts.onSelected(true);
						return true;
					}else{
						methods.showInvalidFile(uiParent);
						opts.onSelected(false);
						return false;
					}
				});

				uiElem.on('click', function(){
					var uiInput = $(this).find('input[type="file"]');

					uiInput.click();
				});

				instance['startUpload'] = function(){
					var oData = ui.data(),
						uiForm = oData.form;

					uiForm.submit();
				};

				instance['updateOptions'] = function(options){
					opts = $.extend(defaults, options);
				}

			},
			'build' : function(){
				var uiParent = ui.parent(),
					uiTemplate = methods.template(),
					generatedID = methods.generateID(),
					uiForm = methods.formTempalte();

				uiTemplate.prop("id", generatedID);
				instance['id'] = generatedID;

				uiTemplate.append(uiForm);
				instance['form'] = uiForm;

				methods.setCSS(uiTemplate);

				methods.bindEvent(uiTemplate);

				methods.setBackgroundImage(uiTemplate);

				ui.html(uiTemplate);

				for(var i in instance){
					ui.data(""+i, instance[i]);
				}

				return instance;
			}
		};

		return methods.init();

	};

})(jQuery);


/*
* SAMPLE USAGE

var uploadInstance = $('#upload').cr8vUpload({
	url : "http://localhost/apollo/upload_plugin/upload",
	backgroundPhotoUrl : "http://www.lovemarks.com/wp-content/uploads/profile-avatars/default-avatar-tech-guy.png",
	titleText : "Change Photo",
	accept : "jpg,png",
	autoUpload : false,
	onSelected : function(bool_valid){},
	success : function(data, textStatus, jqXHR){
		
	},
	error : function(jqXHR, textStatus, errorThrown){
		
	}
});

//to upload
uploadInstance.startUpload();

*/