(function(){
	var ua = navigator.userAgent,
		isMobileWebkit = /WebKit/.test(ua) && /Mobile/.test(ua);

	if (isMobileWebkit) {
		$('html').addClass('mobile');
	}

	$(function(){
		var iScrollInstance;

		if (isMobileWebkit) {
			iScrollInstance = new iScroll('mainwrapper');

			$('#scroller').stellar({
				scrollProperty: 'transform',
				horizontalScrolling: false,
				hideDistantElements: true,
				hideElement: function($elem) { $elem.fadeOut(500); },
				showElement: function($elem) { $elem.fadeIn(500); }	
			});
		} else {
			$.stellar({
				horizontalScrolling: false,
				hideDistantElements: true,
			});
		}
	});

})();