
var CDropDownRetract = (function(){

	function retractDropdown(ui)
	{
		var uiParent = ui.closest(".select"),
			uiDropDowns = uiParent.find(".frm-custom-dropdown").find(".frm-custom-dropdown-option");

		$(window).click(function() {
			uiDropDowns.css({"display":"none"});
			uiDropDowns.removeClass("option-visible");
		});
	}

	return {
		retractDropdown : retractDropdown
	}

})();

