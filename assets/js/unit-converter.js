var UnitConverter = (function(){
	var arrOfUnits = [];

	var constants = {
		"m3" : {
			"m3" : 1, "l" : 1000, "kg" : 2406.53,  "mt" : 2.41
		},
		"l" : {
			"m3" : 0.001, "l" : 1, "kg" : 1, "mt" : 0.0000010
		},
		"kg" : {
			"m3" : 0.00042, "l" : 1, "kg" : 1, "mt" : 0.001
		},
		"mt" : {
			"m3" : 0.42, "l" : 1000, "kg" : 1000, "mt" : 1
		}
	};

	constants["m3"]["bag"] = parseFloat(constants["m3"]["kg"] * .02);
	constants["l"]["bag"] = parseFloat(constants["l"]["kg"] * .02);
	constants["mt"]["bag"] = parseFloat(constants["mt"]["kg"] * .02);
	constants["kg"]["bag"] = parseFloat(constants["kg"]["kg"] * .02);

	constants["bag"] = {
		"m3" : constants.m3.bag,
		"l" : constants.l.bag,
		"kg" : 50,
		"bag" : 1,
		"mt" : constants.mt.bag
	}


	function getUnitOfMeasures(callBack){
		var oOptions = {
			type : "POST",
			data : { "x" : "none" },
			url : BASEURL + "products/get_unit_of_measure",
			returnType : "json",
			success : function(oResult) {
				if(typeof callBack == 'function'){
					callBack(oResult.data);
				}
			}
		}

		cr8v_platform.CconnectionDetector.ajax(oOptions);
	}

	function setUnitOfMeasures(data){
		for(var i in data){
			arrOfUnits.push(data[i]["name"]);
		}
	}

	function _getConstantData(opts){
		
		var data = constants[opts.unitFrom];
		return data;
	}

	function _convertData(opts){
		var constantData = _getConstantData(opts);
		
		var valFrom = constantData[opts.unitFrom],
			valTo = constantData[opts.unitTo];

		if(Object.keys(constantData).length == 0){
			return "No convertion found";
		}
		return opts.qty * valTo;
	}

	return {
		convert : function(sUnitFrom, sUnitTo, number){
			if(sUnitFrom && sUnitTo && number){
				var opts = {
					unitFrom : sUnitFrom,
					unitTo : sUnitTo,
					qty : parseFloat(number)
				};
				return _convertData(opts);
			}
		},
		getUnitOfMeasures : getUnitOfMeasures,
		setUnitOfMeasures : setUnitOfMeasures
	}
})();

$(document).ready(function(){
	UnitConverter.getUnitOfMeasures(function(data){
		console.log("setUnitOfMeasures",data);
		UnitConverter.setUnitOfMeasures(data);
	});
});