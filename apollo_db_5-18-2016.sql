-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2016 at 03:18 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `apollo_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `base_product`
--

CREATE TABLE IF NOT EXISTS `base_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` varchar(9) NOT NULL,
  `name` varchar(160) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `batch`
--

CREATE TABLE IF NOT EXISTS `batch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_assignment_id` int(11) NOT NULL,
  `name` varchar(160) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `unit_of_measure_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `storage_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=112 ;

--
-- Dumping data for table `batch`
--

INSERT INTO `batch` (`id`, `storage_assignment_id`, `name`, `quantity`, `unit_of_measure_id`, `location_id`, `storage_id`, `status`, `is_deleted`) VALUES
(7, 4, 'Batch L', '5000.00', 6, 9, 1, 0, 1),
(8, 4, 'Batch Dos', '1664.00', 6, 4, 1, 0, 1),
(9, 5, 'Batchumax', '4000.00', 9, 14, 2, 0, 0),
(10, 5, 'Batch Max', '1000.00', 9, 18, 2, 0, 0),
(21, 11, 'Batch A', '3.00', 5, 4, 1, 0, 0),
(22, 11, 'Batch U', '2.00', 5, 9, 1, 0, 0),
(28, 14, 'fdgdf', '5000.00', 7, 4, 1, 0, 1),
(29, 15, 'asdasd', '100.00', 5, 4, 1, 0, 0),
(30, 16, 'What', '50.00', 5, 4, 1, 0, 0),
(31, 16, 'Hoho', '50.00', 5, 6, 1, 0, 0),
(32, 17, '15asd', '90.00', 5, 4, 1, 0, 0),
(33, 17, 'dasdas', '30.00', 5, 4, 1, 0, 0),
(36, 19, 'A', '6000.00', 6, 4, 1, 0, 1),
(37, 19, 'B', '5000.00', 6, 12, 2, 0, 1),
(38, 4, 'dasfasdf', '5000.00', 6, 4, 1, 0, 1),
(39, 4, 'booo', '2000.00', 6, 9, 1, 0, 1),
(40, 21, 'dasfasdf', '4500.00', 6, 4, 1, 0, 1),
(41, 21, 'Goo', '1000.00', 6, 9, 1, 0, 1),
(42, 22, 'Mwah', '4500.00', 6, 4, 1, 0, 0),
(43, 22, 'Koos', '1000.00', 6, 9, 1, 0, 0),
(44, 23, 'Batc A', '200.00', 6, 4, 1, 0, 1),
(45, 23, 'Batch U', '350.00', 6, 4, 1, 0, 1),
(46, 27, 'Batc A', '200.00', 6, 4, 1, 0, 1),
(47, 27, 'Batch U', '350.00', 6, 4, 1, 0, 1),
(48, 28, 'Batch U', '350.00', 6, 4, 1, 0, 1),
(49, 29, 'Batch U', '350.00', 6, 4, 1, 0, 1),
(50, 29, 'Batch JJJJ', '200.00', 6, 6, 1, 0, 1),
(51, 30, 'Batch X', '450.00', 6, 4, 1, 0, 1),
(52, 30, 'Batch JJJJ', '200.00', 6, 6, 1, 0, 1),
(53, 31, 'Batch X', '450.00', 6, 4, 1, 0, 1),
(54, 31, 'Batch JJJJ', '200.00', 6, 6, 1, 0, 1),
(55, 32, 'Batch X', '450.00', 6, 4, 1, 0, 0),
(56, 32, 'Batch JJJJ', '200.00', 6, 6, 1, 0, 0),
(57, 33, 'Batch Koko', '1500.00', 7, 6, 1, 0, 1),
(58, 33, 'Batcj Looo', '1200.00', 7, 4, 1, 0, 1),
(59, 34, 'Batch Miko', '5000.00', 5, 6, 1, 0, 0),
(60, 34, 'Batch June', '1500.00', 5, 4, 1, 0, 0),
(61, 35, 'Batch Koko', '1500.00', 7, 6, 1, 0, 0),
(62, 35, 'Batcj Looo', '1200.00', 7, 4, 1, 0, 0),
(63, 36, 'Batch M', '5000.00', 7, 6, 1, 0, 1),
(64, 36, 'Batch Q', '3500.00', 7, 8, 1, 0, 1),
(65, 37, 'Batch KK', '1500.00', 5, 9, 1, 0, 1),
(66, 37, 'Batch Joo', '1500.00', 5, 6, 1, 0, 1),
(67, 38, 'Batch Q', '3500.00', 7, 8, 1, 0, 1),
(68, 39, 'Batch Q', '3500.00', 7, 8, 1, 0, 1),
(69, 40, 'Batch Q', '3500.00', 5, 8, 1, 0, 0),
(70, 40, 'Batch Moo', '3500.00', 5, 4, 1, 0, 0),
(71, 37, 'Batch Yooo', '2000.00', 5, 9, 1, 0, 0),
(72, 41, 'Batch Joom', '6000.00', 7, 8, 1, 0, 1),
(73, 42, 'Batch Joom', '6000.00', 7, 8, 1, 0, 1),
(74, 43, 'Batch U', '600.00', 5, 4, 1, 0, 1),
(75, 44, 'Batch Joom', '6000.00', 7, 8, 1, 0, 1),
(76, 44, 'Batch Complete', '3000.00', 7, 4, 1, 0, 1),
(77, 45, 'Batch Joom', '6000.00', 7, 8, 1, 0, 1),
(78, 45, 'Batch Complete', '3200.00', 7, 4, 1, 0, 1),
(79, 46, 'Batch Joom', '6000.00', 7, 8, 1, 0, 1),
(80, 46, 'Batch Complete', '3200.00', 7, 4, 1, 0, 1),
(81, 46, 'Weew', '200.00', 7, 4, 1, 0, 1),
(82, 47, 'Batch X', '8000.00', 7, 3, 1, 0, 1),
(83, 48, 'Batch X', '8000.00', 7, 3, 1, 0, 0),
(84, 48, 'Batch 0', '1000.00', 7, 9, 1, 0, 0),
(85, 49, 'Batch U', '600.00', 5, 4, 1, 0, 0),
(86, 49, 'Batch plus', '10.00', 5, 4, 1, 0, 0),
(87, 50, 'Batchjij', '500.00', 6, 6, 1, 0, 1),
(88, 51, 'Batchjij', '500.00', 6, 6, 1, 0, 0),
(89, 52, 'Dru Batch', '50.00', 6, 3, 1, 0, 0),
(90, 52, 'Nelson batch', '5.00', 6, 3, 1, 0, 0),
(91, 53, 'Batch BABA', '250.00', 6, 4, 1, 0, 0),
(92, 53, 'BATCH D', '250.00', 6, 4, 1, 0, 0),
(93, 53, 'BATCH M', '5.00', 6, 4, 1, 0, 0),
(94, 54, 'Batch M', '1000.00', 6, 13, 2, 0, 1),
(95, 54, 'Batch M', '1000.00', 6, 13, 2, 0, 1),
(96, 54, 'Batch M', '1000.00', 6, 13, 2, 0, 1),
(97, 54, 'Batch M', '1000.00', 6, 13, 2, 0, 1),
(98, 54, 'Batch M', '1000.00', 6, 13, 2, 0, 1),
(99, 54, 'Batch M', '1000.00', 6, 13, 2, 0, 1),
(100, 55, 'Batch M', '1000.00', 6, 13, 2, 0, 0),
(101, 55, 'Batch M', '600.00', 6, 4, 1, 0, 0),
(102, 56, 'Batch Moo', '600.00', 6, 3, 1, 0, 1),
(103, 57, 'Batch Booo', '550.00', 6, 6, 1, 0, 0),
(104, 58, 'Batch U', '450.00', 6, 4, 1, 0, 0),
(105, 59, 'Batch ABC', '400.00', 5, 3, 1, 0, 0),
(106, 59, 'Batch Mook', '105.00', 5, 5, 1, 0, 0),
(107, 60, 'BATCH 1', '6000.00', 6, 4, 1, 0, 0),
(108, 61, 'Batch JUIXX', '500.00', 5, 4, 1, 0, 0),
(109, 62, 'BATCH 808', '500.00', 5, 13, 2, 0, 0),
(110, 63, 'Batch Yuuu', '200.00', 6, 3, 1, 0, 0),
(111, 64, 'Batch Hello', '1323.00', 5, 4, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `batch_adjust_map`
--

CREATE TABLE IF NOT EXISTS `batch_adjust_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) NOT NULL,
  `method` varchar(50) NOT NULL COMMENT 'add or subtract',
  `adjustment_quantity` decimal(10,2) NOT NULL,
  `reason` text NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `batch_remove_map`
--

CREATE TABLE IF NOT EXISTS `batch_remove_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) NOT NULL,
  `reason` text NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `batch_status_map`
--

CREATE TABLE IF NOT EXISTS `batch_status_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `reason` text NOT NULL,
  `is_deleted` tinyint(4) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `company_id` int(11) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `company_id`, `is_deleted`) VALUES
(1, 'Minions', 1, 0),
(2, 'Nvidia', 1, 0),
(3, 'Wranggler', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `category_type`
--

CREATE TABLE IF NOT EXISTS `category_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_type`
--

INSERT INTO `category_type` (`id`, `name`) VALUES
(1, 'solid'),
(2, 'liquid'),
(3, 'gas');

-- --------------------------------------------------------

--
-- Table structure for table `category_type_map`
--

CREATE TABLE IF NOT EXISTS `category_type_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `category_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `category_type_map`
--

INSERT INTO `category_type_map` (`id`, `category_id`, `category_type_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 2, 3),
(5, 3, 1),
(6, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `consignee`
--

CREATE TABLE IF NOT EXISTS `consignee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET latin1 NOT NULL,
  `alias` varchar(11) NOT NULL,
  `gb_billing_mode` varchar(160) NOT NULL,
  `address` text NOT NULL,
  `notes` text NOT NULL,
  `image` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `consignee`
--

INSERT INTO `consignee` (`id`, `name`, `alias`, `gb_billing_mode`, `address`, `notes`, `image`) VALUES
(1, 'Rock Consignee', 'R C', '0', 'Exercitation blanditiis taciti vulputate vulputate ullam! Atque egestas. Dicta pharetra.', 'Pellentesque, lobortis bibendum cum. Dolor! Beatae accusantium, duis maiores molestie.', 'http://localhost/apollo/uploads/images/26jSjP5lN2rtCXeeliN4.png'),
(2, 'Sublime Editor', 'S E', '0', 'Suscipit cras interdum lacus laudantium at irure blandit provident hymenaeos.', 'Illum pede sagittis nulla natus. Nunc incidunt dolore donec mauris.', 'http://localhost/apollo/uploads/images/j3TogrCbfSfHmzCTcg4f.JPG'),
(3, 'Anonymous Consignee', 'Anonymouses', '0', 'Rhoncus dignissimos asperiores quibusdam', 'Sollicitudin doloribus eiusmod ultricies ridiculus quisque possimus aspernatur semper egestas.asd', 'http://localhost/apollo/uploads/images/ik2zQHQ1HVgFl5unJNgt.png');

-- --------------------------------------------------------

--
-- Table structure for table `contact_person`
--

CREATE TABLE IF NOT EXISTS `contact_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `position` varchar(75) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `alternate_contact_number` varchar(15) NOT NULL,
  `email_address` varchar(70) NOT NULL,
  `consignee_id` int(11) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `image` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `contact_person`
--

INSERT INTO `contact_person` (`id`, `name`, `position`, `contact_number`, `alternate_contact_number`, `email_address`, `consignee_id`, `is_deleted`, `date_created`, `image`) VALUES
(1, 'Allan Delfin', 'Developer', '091785653541', '', 'a.l_taw2@yahoo.com', 1, 0, '2016-04-29 14:25:40', 'http://localhost/apollo/uploads/images/sriwQlL1yEjZskw7SQuD.png'),
(2, 'Kambing', 'Manager', '151879465', '', 'a.l_taw2@yahoo.com', 2, 0, '2016-04-29 14:28:01', 'http://localhost/apollo/uploads/images/zdG5EQECW7oopCBLxqwy.jpg'),
(3, 'Allan Delfin', 'Web Dev', '13213123', '', 'a.l_taw2@yahoo.com', 3, 0, '2016-05-05 02:43:57', 'http://localhost/apollo/uploads/images/1QulIUramlUZWLB9WfKu.png');

-- --------------------------------------------------------

--
-- Table structure for table `courier`
--

CREATE TABLE IF NOT EXISTS `courier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `courier`
--

INSERT INTO `courier` (`id`, `name`) VALUES
(1, 'Nido'),
(2, 'Mookioe'),
(3, 'Mokil'),
(4, 'Randall'),
(5, 'dasdasd'),
(6, 'Hellicopter'),
(7, 'Randall'),
(8, 'Courrier X'),
(9, 'Joomla'),
(10, 'Kemekeme'),
(11, 'Bajula'),
(12, 'BBBB'),
(13, 'Courrny'),
(14, 'MusikLaban'),
(15, 'Randall'),
(16, 'Courier XYZ'),
(17, 'Someone');

-- --------------------------------------------------------

--
-- Table structure for table `discrepancy`
--

CREATE TABLE IF NOT EXISTS `discrepancy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_inventory_qty_id` int(11) NOT NULL,
  `discrepancy_quantity` decimal(10,2) NOT NULL,
  `received_quantity` decimal(10,2) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0-none, 1-up, 2-down',
  `cause` varchar(160) NOT NULL,
  `remarks` text NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `discrepancy`
--

INSERT INTO `discrepancy` (`id`, `product_inventory_qty_id`, `discrepancy_quantity`, `received_quantity`, `type`, `cause`, `remarks`, `is_deleted`) VALUES
(3, 4, '336.00', '7000.00', 0, '', '', 0),
(4, 3, '500.00', '4500.00', 0, 'other', 'Wala lang', 0),
(10, 28, '2.00', '5.00', 0, 'other', 'dsa asd as', 0),
(13, 29, '0.00', '5000.00', 0, 'other', '', 0),
(14, 10, '0.00', '100.00', 0, 'other', '', 0),
(15, 11, '0.00', '100.00', 0, 'other', 'asdasdasd', 0),
(16, 5, '20.00', '120.00', 0, 'other', 'da asda sd', 0),
(17, 30, '150.00', '650.00', 0, 'other', 'Nemo nostrum nunc malesuada natus aperiam. Aliquam sociosqu praesent quam.', 0),
(18, 31, '200.00', '2700.00', 0, 'other', 'asdasdasd', 0),
(19, 32, '500.00', '6500.00', 0, 'other', 'Mollit mauris, molestie eligendi? Pharetra molestiae eget pretium, itaque deserunt.', 0),
(20, 33, '1000.00', '7000.00', 0, 'other', 'Iure architecto dictumst odit beatae libero urna magnam, provident irure.', 0),
(21, 34, '0.00', '2000.00', 0, '', '', 0),
(22, 35, '0.00', '9000.00', 0, 'other', 'rwerwerwer', 0),
(23, 36, '110.00', '610.00', 1, 'other', 'adsdasdasd fdaf afa s', 0),
(24, 6, '0.00', '500.00', 0, 'other', 'sfafasasd', 0),
(25, 37, '5.00', '55.00', 1, 'other', 'asfasdasdfa', 0),
(26, 40, '5.00', '505.00', 0, 'Others', 'asdasd asd as d', 0),
(27, 42, '100.00', '1600.00', 1, 'other', 'vb vdsaf asdf asdf ', 0),
(28, 41, '50.00', '550.00', 1, 'other', 'Adipisicing fugiat rhoncus amet proin beatae a taciti, viverra voluptates.', 0),
(29, 43, '50.00', '450.00', 0, 'Others', 'dasff das ads fawe', 0),
(30, 44, '5.00', '505.00', 0, 'Others', 'asd asd asdas das d', 0),
(31, 45, '0.00', '6000.00', 1, 'Others', 'adsas das d', 0),
(32, 47, '0.00', '500.00', 0, 'other', '', 0),
(33, 46, '0.00', '500.00', 0, 'other', '', 0),
(34, 48, '0.00', '200.00', 0, 'other', '', 0),
(35, 49, '0.00', '1323.00', 0, 'other', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `equipment_list`
--

CREATE TABLE IF NOT EXISTS `equipment_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `record_type` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(160) NOT NULL,
  `record_log_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `equipment_list`
--

INSERT INTO `equipment_list` (`id`, `record_type`, `name`, `record_log_id`) VALUES
(3, 2, 'euiptor', 8),
(4, 2, 'eweg', 6),
(5, 2, 'turnilyo', 8),
(6, 2, 'egypt', 8);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_documents`
--

CREATE TABLE IF NOT EXISTS `inventory_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_name` varchar(75) NOT NULL,
  `document_path` varchar(155) NOT NULL,
  `date_added` datetime NOT NULL,
  `receiving_log_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `inventory_documents`
--

INSERT INTO `inventory_documents` (`id`, `document_name`, `document_path`, `date_added`, `receiving_log_id`, `status`, `version`) VALUES
(1, 'Code ethics', 'http://localhost/apollo/uploads/documents/ONOkSAYs2nKcfMTT0RCo.pdf', '2016-04-11 20:49:04', 1, 0, 1),
(2, 'Getting Started', 'http://localhost/apollo/uploads/documents/AtjUC3SMmUTZE1WeQQ7K.pdf', '2016-04-11 20:49:51', 1, 0, 1),
(3, 'Getting Started', 'http://localhost/apollo/uploads/documents/feD2sZ9FBIVDaw8M5Yvs.pdf', '2016-04-11 20:49:52', 1, 0, 1),
(4, 'weeew', 'http://localhost/apollo/uploads/documents/Ml0fN0JWKNRyXavji7eV.pdf', '2016-04-11 20:52:10', 1, 0, 1),
(5, 'GitHub for Windows', 'http://localhost/apollo/uploads/documents/54aCa2S6UtuA9lyPd7c0.pdf', '2016-04-11 20:52:39', 1, 0, 1),
(6, 'Delf', 'http://localhost/apollo/uploads/documents/7OvLIH8s1SctSijxDqyR.pdf', '2016-04-11 21:00:21', 1, 0, 1),
(7, 'Cook', 'http://localhost/apollo/uploads/documents/8fwaIdiGdju6Z0bGTgZ6.pdf', '2016-04-11 21:00:36', 1, 0, 1),
(8, 'Delfin-Pagibig-Account.pdf', 'http://localhost/apollo/uploads/documents/a9xfek5cC3Sb4GoiL3A8.pdf', '2016-04-11 21:26:31', 1, 0, 1),
(9, 'CSV FILE', 'http://localhost/apollo/uploads/documents/2y2ptJ7WgznYU91om5Mv.csv', '2016-04-11 21:36:55', 1, 0, 1),
(10, 'FD-Harvest-Chrome-Features-List.pdf', 'http://localhost/apollo/uploads/documents/XL0RIK3Al2YWb7O74WcP.pdf', '2016-04-11 21:51:51', 1, 0, 1),
(11, 'Pdf Natin', 'http://localhost/apollo/uploads/documents/s2Z3qLtNQmY1Iqc5u4nP.pdf', '2016-04-11 22:17:35', 3, 0, 1),
(12, 'Csv Mo!', 'http://localhost/apollo/uploads/documents/8SeA7VYdpQj3uHuWj5ce.csv', '2016-04-11 22:17:51', 3, 0, 1),
(13, 'cr8v handbook', 'http://localhost/apollo/uploads/documents/rL9DZxRQvQgCOEWHgq0h.pdf', '2016-04-13 16:11:47', 3, 0, 1),
(14, '3PNoChNoBrkf.pdf', 'http://localhost/apollo/uploads/documents/BI1ZYSQNAVxU4AqNt4nq.pdf', '2016-04-13 17:42:23', 3, 0, 1),
(15, '3PNoChNoBrkf.pdf', 'http://localhost/apollo/uploads/documents/BY271OSLXkhcn8ZMMkcm.pdf', '2016-04-13 17:50:45', 3, 0, 1),
(16, '3PNoChNoBrkf.pdf', 'http://localhost/apollo/uploads/documents/I936vgansKWPW4sFNdlB.pdf', '2016-04-13 17:52:17', 3, 0, 1),
(17, '3PNoChNoBrkf.pdf', 'http://localhost/apollo/uploads/documents/fbOgWb6dOQrvWoC7Uy1v.pdf', '2016-04-13 17:52:48', 3, 0, 1),
(18, 'Allan Delfin', 'http://localhost/apollo/uploads/documents/PxtEh4k0mmpxF85JiCby.pdf', '2016-04-13 18:02:19', 3, 0, 1),
(19, '3PNoChNoBrkf.pdf', 'http://localhost/apollo/uploads/documents/Pv4sn3yqvWZVlpfVtbDS.pdf', '2016-04-13 18:02:58', 3, 0, 1),
(20, 'Test PDF', 'http://localhost/apollo/uploads/documents/5va1ay2j6VLeg3qXsWDf.pdf', '2016-04-14 15:31:22', 2, 0, 1),
(21, '3PNoChNoBrkf.pdf', 'http://localhost/apollo/uploads/documents/uHuWj5cem6GLAlERsUjp.pdf', '2016-04-19 15:31:18', 5, 0, 1),
(22, 'Keeloo', 'http://localhost/apollo/uploads/documents/pJsvjhvWHznhEOWtLJv1.pdf', '2016-04-19 23:54:16', 5, 0, 1),
(23, 'asdasdasd', 'http://localhost/apollo/uploads/documents/YnoIzR7DdWcwpIRd5MO2.pdf', '2016-04-20 00:09:46', 6, 0, 1),
(24, 'Wees', 'http://localhost/apollo/uploads/documents/aP4r9QNbCsbfMvbemjhd.pdf', '2016-04-25 20:59:37', 7, 0, 1),
(25, 'Docs PDF', 'http://localhost/apollo/uploads/documents/wcqLaQAZSeiLYPCjjCbl.pdf', '2016-04-25 21:27:12', 8, 0, 1),
(26, 'test', 'http://localhost/apollo/uploads/documents/e5sEbV76EKHvlUG0ve39.pdf', '2016-04-27 23:01:17', 9, 0, 1),
(27, '3PNoChNoBrkf.pdf', 'http://localhost/apollo/uploads/documents/mTnrWErEvtFGge18WgX5.pdf', '2016-04-27 23:02:49', 9, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(12) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `number`, `date`) VALUES
(1, 112255, '2016-04-28 20:47:08'),
(2, 596132, '2016-04-13 22:15:52'),
(3, 0, '2016-04-12 22:17:56'),
(4, 0, '2016-04-11 17:07:57'),
(5, 0, '2016-04-12 22:11:19'),
(6, 432423, '2016-04-21 00:09:59'),
(7, 12864521, '2016-04-28 20:59:48'),
(8, 789789789, '2016-04-07 21:26:57'),
(9, 885566, '2016-04-04 03:26:21'),
(10, 60000, '2016-04-04 03:56:49'),
(11, 0, '2016-04-19 20:50:47'),
(12, 123, '2016-05-06 22:12:38'),
(13, 500, '2016-05-04 22:13:36'),
(14, 123, '2016-05-17 02:57:23'),
(15, 4324132, '2016-05-11 23:34:24'),
(16, 132123, '2016-05-18 03:07:23');

-- --------------------------------------------------------

--
-- Table structure for table `loading_area`
--

CREATE TABLE IF NOT EXISTS `loading_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(160) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `loading_area`
--

INSERT INTO `loading_area` (`id`, `name`, `is_deleted`, `date_created`) VALUES
(1, 'Loading Area A', 0, '0000-00-00 00:00:00'),
(2, 'Loading Area B', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `loading_type`
--

CREATE TABLE IF NOT EXISTS `loading_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(160) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(75) NOT NULL,
  `type` int(11) NOT NULL,
  `storage_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `name`, `type`, `storage_id`, `parent_id`, `is_deleted`) VALUES
(1, 'Deftones', 1, 1, 0, 0),
(2, 'Shelf Qube', 1, 1, 1, 0),
(3, 'Shelf Xion', 1, 1, 2, 0),
(4, 'Shelf Tier', 1, 1, 3, 0),
(5, 'Shelf Beam', 1, 1, 2, 0),
(6, 'Shelf June', 1, 1, 5, 0),
(7, 'Shelf Square', 1, 1, 1, 0),
(8, 'Shelf Jude', 1, 1, 7, 0),
(9, 'Shelf Nitro', 1, 1, 8, 0),
(10, 'Puff Warehouse', 1, 2, 0, 0),
(11, 'Jury A', 1, 2, 10, 0),
(12, 'Jury A 1', 1, 2, 11, 0),
(13, 'Jury A 1 1', 1, 2, 12, 0),
(14, 'Jury A 2', 1, 2, 11, 0),
(15, 'Jury A 2 1', 1, 2, 14, 0),
(16, 'Jury B', 1, 2, 10, 0),
(17, 'Jury B 1', 1, 2, 16, 0),
(18, 'Jury B 2', 1, 2, 16, 0),
(19, 'Jury C', 1, 2, 10, 0),
(20, 'Jury C 1', 1, 2, 19, 0),
(21, 'Jury C 2', 1, 2, 19, 0);

-- --------------------------------------------------------

--
-- Table structure for table `location_type`
--

CREATE TABLE IF NOT EXISTS `location_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(160) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mode_of_transfer`
--

CREATE TABLE IF NOT EXISTS `mode_of_transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(160) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mode_of_transfer`
--

INSERT INTO `mode_of_transfer` (`id`, `name`) VALUES
(1, 'mode tranfer A'),
(2, 'mode tranfer B');

-- --------------------------------------------------------

--
-- Table structure for table `officer`
--

CREATE TABLE IF NOT EXISTS `officer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `officer`
--

INSERT INTO `officer` (`id`, `name`) VALUES
(1, 'Milo'),
(2, 'KewKwe'),
(3, 'Wino'),
(4, 'Delfin'),
(5, 'asddasas'),
(6, 'Jomar'),
(7, 'Delfin'),
(8, 'Lorenz'),
(9, 'Fruity'),
(10, 'AAAA'),
(11, 'Nelson'),
(12, 'Kamikazee'),
(13, 'Lorenz'),
(14, 'Lorenz'),
(15, 'Delfin');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(160) NOT NULL,
  `sku` varchar(9) NOT NULL,
  `product_category` tinyint(4) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(150) NOT NULL,
  `product_shelf_life` decimal(10,2) NOT NULL,
  `threshold_min` decimal(10,2) NOT NULL,
  `threshold_maximum` decimal(10,2) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `sku`, `product_category`, `description`, `image`, `product_shelf_life`, `threshold_min`, `threshold_maximum`, `unit_price`, `is_active`, `is_deleted`) VALUES
(1, 'Kambing', '000000001', 1, 'Proident facere placeat! Quae quia pretium hymenaeos.', 'http://localhost/apollo/uploads/images/XavUAlKCY2wIAn43mV5Y.jpg', '200.00', '500.00', '6000.00', '5000.00', 1, 0),
(2, 'Rock', '000000002', 1, 'Et enim maecenas accusamus! Sed modi, cursus.', 'http://localhost/apollo/uploads/images/5lu5b7vaOz62VkZ31so4.png', '65.00', '300.00', '4500.00', '4500.00', 0, 0),
(3, 'Anonymous', '000000003', 2, 'Quasi nulla urna. Sapiente natoque iusto dolore.', 'http://localhost/apollo/uploads/images/eDf3ra3oDDQYauIZTJeb.png', '65.00', '500.00', '7500.00', '650.00', 1, 0),
(4, 'Random', '000000004', 2, 'Odio porttitor officia penatibus asperiores mauris, nunc.', 'http://localhost/apollo/uploads/images/PB6IaaXr8lRMttiNnpe6.JPG', '100.00', '65.00', '260.00', '856.00', 0, 0),
(5, 'ckeditor', '000000005', 3, 'Nisi hymenaeos, auctor eiusmod, sem eiusmod pariatur.', 'http://localhost/apollo/uploads/images/Av8NizXtfYccDzOBdFhK.JPG', '350.00', '1000.00', '8000.00', '48.00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_composition`
--

CREATE TABLE IF NOT EXISTS `product_composition` (
  `product_id` int(11) NOT NULL,
  `base_product_id` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_composition`
--

INSERT INTO `product_composition` (`product_id`, `base_product_id`, `quantity`, `is_deleted`) VALUES
(3, 1, '3.00', 0),
(4, 2, '4.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_inventory`
--

CREATE TABLE IF NOT EXISTS `product_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity_received` decimal(10,2) NOT NULL,
  `method` varchar(20) NOT NULL DEFAULT 'add',
  `unit_of_measure_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  `product_id` int(11) NOT NULL,
  `last_update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `product_inventory`
--

INSERT INTO `product_inventory` (`id`, `quantity_received`, `method`, `unit_of_measure_id`, `date_created`, `status`, `product_id`, `last_update_date`) VALUES
(1, '610.00', 'add', 0, '2016-04-26 20:42:08', 0, 2, '2016-04-26 20:42:08'),
(2, '9000.00', 'add', 0, '2016-04-26 20:42:08', 0, 3, '2016-04-26 20:42:08'),
(3, '4500.00', 'add', 0, '2016-04-26 21:13:11', 0, 3, '2016-04-26 21:13:11'),
(4, '7000.00', 'add', 0, '2016-04-26 21:14:57', 0, 5, '2016-04-26 21:14:57'),
(5, '1600.00', 'add', 0, '2016-05-02 14:47:48', 0, 3, '2016-05-02 14:47:48'),
(6, '550.00', 'add', 0, '2016-05-02 14:49:03', 0, 1, '2016-05-02 14:49:03'),
(7, '450.00', 'add', 0, '2016-05-03 17:38:49', 0, 2, '2016-05-03 17:38:49'),
(8, '505.00', 'add', 0, '2016-05-03 17:38:49', 0, 4, '2016-05-03 17:38:49'),
(9, '6000.00', 'add', 0, '2016-05-04 18:46:13', 0, 5, '2016-05-04 18:46:13'),
(10, '500.00', 'add', 0, '2016-05-04 19:00:02', 0, 2, '2016-05-04 19:00:02'),
(11, '500.00', 'add', 0, '2016-05-04 19:00:02', 0, 3, '2016-05-04 19:00:02'),
(12, '200.00', 'add', 6, '2016-05-05 15:37:52', 0, 2, '2016-05-05 15:37:52'),
(13, '1323.00', 'add', 5, '2016-05-11 19:08:14', 0, 2, '2016-05-11 19:08:14');

-- --------------------------------------------------------

--
-- Table structure for table `product_inventory_qty`
--

CREATE TABLE IF NOT EXISTS `product_inventory_qty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qty` decimal(10,2) NOT NULL,
  `product_receiving_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `loading_method` varchar(20) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `unit_of_measure_id` int(11) NOT NULL,
  `bag` int(11) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `product_inventory_qty`
--

INSERT INTO `product_inventory_qty` (`id`, `qty`, `product_receiving_id`, `product_id`, `loading_method`, `vendor_id`, `unit_of_measure_id`, `bag`, `is_deleted`) VALUES
(1, '500.00', 1, 2, 'bulk', 1, 6, 0, 0),
(2, '650.00', 1, 4, 'piece', 2, 7, 0, 0),
(3, '5000.00', 2, 3, 'bulk', 3, 9, 0, 0),
(4, '6664.00', 3, 5, 'bulk', 4, 6, 0, 0),
(5, '100.00', 4, 3, 'bulk', 5, 5, 0, 0),
(6, '500.00', 4, 1, 'bulk', 5, 6, 0, 0),
(7, '100.00', 4, 5, 'bulk', 5, 5, 0, 0),
(8, '100.00', 4, 3, 'bulk', 5, 5, 0, 0),
(9, '100.00', 4, 3, 'bulk', 5, 5, 0, 0),
(10, '100.00', 4, 1, 'bulk', 5, 5, 0, 0),
(11, '100.00', 4, 2, 'bulk', 5, 5, 0, 0),
(12, '100.00', 4, 5, 'bulk', 5, 5, 0, 0),
(13, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(14, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(15, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(16, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(17, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(18, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(19, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(20, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(21, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(22, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(23, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(24, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(25, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(26, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(27, '100.00', 4, 4, 'bulk', 5, 5, 0, 0),
(28, '3.00', 5, 3, 'bulk', 6, 5, 0, 0),
(30, '500.00', 7, 2, 'bulk', 8, 6, 0, 0),
(31, '2500.00', 8, 2, 'piece', 9, 7, 0, 1),
(32, '6000.00', 8, 4, 'bulk', 10, 5, 0, 1),
(33, '6000.00', 8, 2, 'bulk', 11, 5, 0, 1),
(34, '2000.00', 8, 3, 'bulk', 12, 5, 0, 1),
(35, '9000.00', 8, 3, 'piece', 13, 7, 0, 0),
(36, '500.00', 8, 2, 'bulk', 14, 5, 0, 0),
(37, '50.00', 9, 3, 'bulk', 15, 6, 0, 0),
(38, '500.00', 10, 2, 'Bulk', 11, 5, 0, 0),
(39, '100.00', 11, 5, 'Bulk', 16, 5, 0, 0),
(40, '500.00', 12, 2, 'Bulk', 11, 6, 0, 0),
(41, '500.00', 13, 1, 'bulk', 17, 6, 0, 0),
(42, '1500.00', 14, 3, 'bulk', 18, 6, 0, 0),
(43, '500.00', 15, 2, 'Bulk', 19, 6, 0, 0),
(44, '500.00', 15, 4, 'Bags', 20, 5, 100, 0),
(45, '6000.00', 16, 5, 'Bulk', 21, 6, 0, 0),
(46, '500.00', 17, 3, 'bulk', 22, 5, 0, 0),
(47, '500.00', 17, 2, 'bulk', 19, 5, 0, 0),
(48, '200.00', 18, 2, 'bulk', 23, 6, 0, 0),
(49, '1323.00', 19, 2, 'bulk', 24, 5, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_withdrawal`
--

CREATE TABLE IF NOT EXISTS `product_withdrawal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qty` decimal(10,2) NOT NULL,
  `unit_of_measure` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `container_id` int(11) NOT NULL,
  `withdrawal_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `receiving_consignee_map`
--

CREATE TABLE IF NOT EXISTS `receiving_consignee_map` (
  `receiving_log_id` int(11) NOT NULL,
  `consignee_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `distribution_qty` decimal(10,2) NOT NULL,
  `unit_of_measure_id` int(11) NOT NULL,
  `bag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receiving_consignee_map`
--

INSERT INTO `receiving_consignee_map` (`receiving_log_id`, `consignee_id`, `product_id`, `distribution_qty`, `unit_of_measure_id`, `bag`) VALUES
(12, 2, 2, '250.00', 6, 0),
(12, 1, 2, '250.00', 5, 0),
(15, 1, 2, '250.00', 6, 0),
(15, 2, 2, '250.00', 6, 0),
(15, 1, 4, '300.00', 5, 60),
(15, 2, 4, '200.00', 5, 40),
(16, 3, 5, '6000.00', 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `receiving_log`
--

CREATE TABLE IF NOT EXISTS `receiving_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `receiving_number` int(11) NOT NULL,
  `purchase_order` varchar(20) NOT NULL,
  `date_received` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `received_by` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `officer_id` int(11) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `receiving_log`
--

INSERT INTO `receiving_log` (`id`, `receiving_number`, `purchase_order`, `date_received`, `status`, `type`, `received_by`, `invoice_id`, `officer_id`, `courier_id`, `date_created`) VALUES
(13, 1, '14513216541', '2016-05-02 14:49:03', 1, 1, 10000, 12, 11, 13, '2016-05-02 22:12:38'),
(14, 2, '0321651', '2016-05-02 14:47:48', 1, 1, 10000, 13, 12, 14, '2016-05-02 22:13:36'),
(15, 1, '1651165', '2016-05-03 17:38:49', 1, 2, 10000, 0, 0, 0, '2016-05-03 20:18:32'),
(16, 2, '5000', '2016-05-04 18:46:13', 1, 2, 10000, 0, 0, 0, '2016-05-05 02:45:22'),
(17, 3, '4324324234324', '2016-05-04 19:00:02', 1, 1, 10000, 14, 13, 15, '2016-05-05 02:57:23'),
(18, 4, '03216541', '2016-05-05 15:37:52', 1, 1, 10000, 15, 14, 16, '2016-05-05 23:34:24'),
(19, 5, '87654324', '2016-05-11 19:08:14', 1, 1, 10000, 16, 15, 17, '2016-05-12 03:07:23');

-- --------------------------------------------------------

--
-- Table structure for table `receiving_notes`
--

CREATE TABLE IF NOT EXISTS `receiving_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `receiving_log_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subject` varchar(160) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `receiving_notes`
--

INSERT INTO `receiving_notes` (`id`, `receiving_log_id`, `note`, `user_id`, `date_created`, `subject`, `is_deleted`) VALUES
(1, 1, 'Aliqua adipiscing quaerat at curabitur optio cras ullam tempore mollitia, aliquid sagittis. Rem.', 10000, '2016-04-11 12:46:52', 'Wihtin', 0),
(2, 1, 'Nam ridiculus assumenda, assumenda exercitation reiciendis animi. Fames. Fugiat molestie doloremque parturient occaecati delectus cillum, quis? Vulputate magna iure quasi.', 10000, '2016-04-11 12:47:03', 'Nam ridiculus', 0),
(3, 3, 'saddsa', 10000, '2016-04-13 08:15:24', 'ads', 0),
(4, 5, 'Eligendi cupidatat consectetuer? Ultrices vivamus venenatis nulla consequatur. Bibendum aenean.', 10000, '2016-04-19 07:31:33', 'Eligendi cupidatat', 0),
(5, 5, 'Per euismod sapiente nesciunt quaerat ab phasellus inventore quisque! Convallis.', 10000, '2016-04-19 15:54:26', 'Per euismod', 0),
(6, 6, 'Eu nesciunt conubia erat! Cupidatat blandit perspiciatis reiciendis odio. Aut.', 10000, '2016-04-19 16:09:55', 'nesciunt conubia ', 0),
(7, 7, 'Amet, adipisicing purus minima. Arcu praesent varius mi rem cras.', 10000, '2016-04-25 12:59:44', 'Amet', 0),
(8, 8, 'Do wisi accumsan? Nesciunt eros, adipiscing soluta animi luctus! Viverra.', 10000, '2016-04-25 13:27:22', 'Do wisi', 0);

-- --------------------------------------------------------

--
-- Table structure for table `storage`
--

CREATE TABLE IF NOT EXISTS `storage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_id` varchar(160) NOT NULL,
  `name` varchar(160) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL,
  `address` text NOT NULL,
  `storage_type` tinyint(1) NOT NULL,
  `image` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `capacity` decimal(10,2) NOT NULL,
  `measurement_id` int(11) NOT NULL,
  `capacity_measurement` varchar(50) NOT NULL,
  `high_setpoint` decimal(10,2) NOT NULL,
  `low_setpoint` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `storage`
--

INSERT INTO `storage` (`id`, `storage_id`, `name`, `is_active`, `company_id`, `address`, `storage_type`, `image`, `description`, `capacity`, `measurement_id`, `capacity_measurement`, `high_setpoint`, `low_setpoint`) VALUES
(1, '98453123', 'Deftones', 1, 1, 'Mollis ullamcorper montes facilis voluptatem facilis dignissimos posuere scelerisque eu.', 1, 'http://localhost/apollo/uploads/images/rtpmMdWvSmRsjCtGccEa.jpg', 'Morbi, sapien eveniet! Lacinia amet, ab eros duis quod mattis.', '2000.00', 6, 'weight', '1800.00', '350.00'),
(2, '122266', 'Puff Warehouse', 1, 1, 'Aliquip nostrum, maecenas interdum sequi, corporis! Volutpat nisi.', 1, 'http://localhost/apollo/uploads/images/7PImaIwfWAphxEerxEkc.png', 'Fugiat eaque commodi tellus nobis nostrum, blanditiis cillum.', '30000.00', 9, 'volume', '25000.00', '1000.00');

-- --------------------------------------------------------

--
-- Table structure for table `storage_assignment`
--

CREATE TABLE IF NOT EXISTS `storage_assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `receiving_log_id` int(11) NOT NULL,
  `product_inventory_qty_id` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `storage_assignment`
--

INSERT INTO `storage_assignment` (`id`, `receiving_log_id`, `product_inventory_qty_id`, `total`, `is_deleted`) VALUES
(4, 3, 4, '7000.00', 1),
(5, 2, 3, '5000.00', 0),
(11, 5, 28, '5.00', 0),
(14, 6, 29, '5000.00', 0),
(15, 4, 10, '100.00', 0),
(16, 4, 11, '100.00', 0),
(17, 4, 5, '120.00', 0),
(18, 6, 29, '5500.00', 0),
(19, 3, 4, '7000.00', 1),
(20, 3, 4, '7000.00', 1),
(21, 3, 4, '5500.00', 1),
(22, 3, 4, '5500.00', 0),
(23, 7, 30, '550.00', 1),
(27, 7, 30, '550.00', 1),
(28, 7, 30, '350.00', 1),
(29, 7, 30, '550.00', 1),
(30, 7, 30, '650.00', 1),
(31, 7, 30, '650.00', 1),
(32, 7, 30, '650.00', 0),
(33, 8, 31, '2000.00', 1),
(34, 8, 32, '2000.00', 1),
(35, 8, 31, '2000.00', 1),
(36, 8, 33, '2000.00', 1),
(37, 8, 34, '2000.00', 1),
(38, 8, 33, '2000.00', 1),
(39, 8, 33, '2000.00', 1),
(40, 8, 33, '2000.00', 1),
(41, 8, 35, '6000.00', 1),
(42, 8, 35, '6000.00', 1),
(43, 8, 36, '600.00', 1),
(44, 8, 35, '9000.00', 1),
(45, 8, 35, '9200.00', 1),
(46, 8, 35, '9400.00', 1),
(47, 8, 35, '8000.00', 1),
(48, 8, 35, '9000.00', 0),
(49, 8, 36, '610.00', 0),
(50, 4, 6, '500.00', 1),
(51, 4, 6, '500.00', 0),
(52, 9, 37, '55.00', 0),
(53, 12, 40, '505.00', 0),
(54, 14, 42, '1000.00', 1),
(55, 14, 42, '1600.00', 0),
(56, 13, 41, '600.00', 1),
(57, 13, 41, '550.00', 0),
(58, 15, 43, '450.00', 0),
(59, 15, 44, '505.00', 0),
(60, 16, 45, '6000.00', 0),
(61, 17, 47, '500.00', 0),
(62, 17, 46, '500.00', 0),
(63, 18, 48, '200.00', 0),
(64, 19, 49, '1323.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `storage_category_map`
--

CREATE TABLE IF NOT EXISTS `storage_category_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_id` int(11) NOT NULL,
  `category_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `storage_category_map`
--

INSERT INTO `storage_category_map` (`id`, `storage_id`, `category_type_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `storage_hierarchy`
--

CREATE TABLE IF NOT EXISTS `storage_hierarchy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_name` varchar(160) NOT NULL,
  `hierarchy_name` varchar(160) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `storage_hierarchy`
--

INSERT INTO `storage_hierarchy` (`id`, `main_name`, `hierarchy_name`, `parent_id`, `date_created`, `is_deleted`) VALUES
(1, 'Ranker', 'Rank 1', 0, '2016-04-11 20:26:23', 0),
(2, 'Ranker', 'Rank 2', 1, '2016-04-11 20:26:23', 0),
(3, 'Ranker', 'Rank 3', 2, '2016-04-11 20:26:23', 0),
(4, 'Leverage', 'Level 1', 0, '2016-04-11 20:26:34', 0),
(5, 'Leverage', 'Level 2', 4, '2016-04-11 20:26:34', 0),
(6, 'Leverage', 'Level 3', 5, '2016-04-11 20:26:34', 0),
(7, 'Leverage', 'Level 4', 6, '2016-04-11 20:26:34', 0),
(8, 'Shelfer', 'Main Shelf', 0, '2016-04-11 20:27:01', 0),
(9, 'Shelfer', 'Inner Shelf', 8, '2016-04-11 20:27:01', 0),
(10, 'Shelfer', 'Shelf', 9, '2016-04-11 20:27:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `storage_type`
--

CREATE TABLE IF NOT EXISTS `storage_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(160) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `storage_type`
--

INSERT INTO `storage_type` (`id`, `name`) VALUES
(1, 'warehouse'),
(2, 'silo');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_documents`
--

CREATE TABLE IF NOT EXISTS `transfer_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_name` varchar(160) NOT NULL,
  `document_path` varchar(155) NOT NULL,
  `date_added` datetime NOT NULL,
  `transfer_log_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transfer_log`
--

CREATE TABLE IF NOT EXISTS `transfer_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mode_of_transfer_id` int(11) NOT NULL,
  `to_number` varchar(9) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_issued` datetime NOT NULL,
  `date_start` datetime NOT NULL,
  `date_complete` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `vessel_origin_id` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL,
  `reason_for_transfer` text NOT NULL,
  `qty_to_transfer` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transfer_notes`
--

CREATE TABLE IF NOT EXISTS `transfer_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_log_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `subject` varchar(160) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transfer_origin`
--

CREATE TABLE IF NOT EXISTS `transfer_origin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_map_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `storage_location` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `unit_of_measure` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `destination_container` int(11) NOT NULL,
  `origin_container` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transfer_product_map`
--

CREATE TABLE IF NOT EXISTS `transfer_product_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `transfer_log_id` int(11) NOT NULL,
  `unit_of_measure_id` int(11) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL,
  `transfer_method` varchar(160) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transfer_requestor`
--

CREATE TABLE IF NOT EXISTS `transfer_requestor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(160) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `truck_receiving_log`
--

CREATE TABLE IF NOT EXISTS `truck_receiving_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin` varchar(45) NOT NULL,
  `driver_name` varchar(75) NOT NULL,
  `license_name` varchar(75) NOT NULL,
  `plate_number` varchar(10) NOT NULL,
  `receiving_log_id` int(11) NOT NULL,
  `tare_in` decimal(10,2) NOT NULL,
  `tare_out` decimal(10,2) NOT NULL,
  `net_weight` decimal(10,2) NOT NULL,
  `trucking` varchar(160) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `unit_of_measures`
--

CREATE TABLE IF NOT EXISTS `unit_of_measures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `unit_of_measures`
--

INSERT INTO `unit_of_measures` (`id`, `name`) VALUES
(5, 'kg'),
(6, 'mt'),
(7, 'pc'),
(8, 'bag'),
(9, 'L');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE IF NOT EXISTS `vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `name`) VALUES
(1, 'zoom'),
(2, 'joke'),
(3, 'lolo'),
(4, 'sasasaas'),
(5, 'girl'),
(6, 'sdads'),
(7, 'weeeee'),
(8, 'rock'),
(9, 'beach'),
(10, 'bicol'),
(11, 'rock vendor'),
(12, 'anono'),
(13, 'hoogoot'),
(14, 'lorenz'),
(15, 'kamikazee'),
(16, 'vendor ck'),
(17, 'disney'),
(18, 'global x'),
(19, 'vendor a'),
(20, 'random vendor'),
(21, 'ck vendor'),
(22, 'vendor x'),
(23, 'rock vendor abc'),
(24, 'wala');

-- --------------------------------------------------------

--
-- Table structure for table `vessel_list`
--

CREATE TABLE IF NOT EXISTS `vessel_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(160) NOT NULL,
  `image` text NOT NULL,
  `alias` varchar(160) NOT NULL,
  `date_created` datetime NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `vessel_list`
--

INSERT INTO `vessel_list` (`id`, `name`, `image`, `alias`, `date_created`, `is_deleted`) VALUES
(1, 'asdasd', '', 'asdasd', '0000-00-00 00:00:00', 1),
(2, 'Delfin Vessel', 'http://localhost/apollo/uploads/images/tlkOXUyWLjphkF5FEyir.jpg', 'DiVi', '0000-00-00 00:00:00', 0),
(3, 'Vessel X', 'http://localhost/apollo/uploads/images/ZoGRVyIlXIns3uoxGXZp.JPG', 'Vessel X', '0000-00-00 00:00:00', 0),
(4, 'Deftones', 'http://localhost/apollo/uploads/images/2zW3EnpVLCmxHLS5jMyj.jpg', 'Chino Moreno', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vessel_receiving_log`
--

CREATE TABLE IF NOT EXISTS `vessel_receiving_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin` varchar(100) NOT NULL,
  `vessel_type` varchar(50) NOT NULL,
  `captain` varchar(160) NOT NULL,
  `discharge_type` varchar(160) NOT NULL,
  `hatches` varchar(20) NOT NULL,
  `bill_of_landing_volume` varchar(160) NOT NULL,
  `start_unloading` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_unloading` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `departure_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `receiving_log` int(11) NOT NULL,
  `duration` timestamp NOT NULL,
  `unloading_duration` int(11) NOT NULL,
  `berthing_time` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `vessel_receiving_log`
--

INSERT INTO `vessel_receiving_log` (`id`, `origin`, `vessel_type`, `captain`, `discharge_type`, `hatches`, `bill_of_landing_volume`, `start_unloading`, `end_unloading`, `departure_time`, `receiving_log`, `duration`, `unloading_duration`, `berthing_time`) VALUES
(1, '2', 'D vessel type', 'D Captain', 'D Discharge', 'Hatch D', '123-Bill Landin', '2016-04-29 06:34:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, '0000-00-00 00:00:00', 0, '2016-05-02 06:33:00'),
(2, '3', 'Type A', 'Captaion', 'Type X', 'Hatch X', '123123213', '2016-05-03 21:38:00', '2016-05-04 22:38:00', '2016-05-06 02:38:00', 15, '0000-00-00 00:00:00', 90000000, '2016-05-04 12:17:00'),
(3, '2', 'Vessel Type', 'Vessel Captain', 'Discharge Type', 'Hatches', 'BILL-00-11', '2016-05-09 21:45:00', '2016-05-11 22:46:00', '2016-05-12 02:46:00', 16, '0000-00-00 00:00:00', 176460000, '2016-05-11 11:44:00');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_batch`
--

CREATE TABLE IF NOT EXISTS `withdrawal_batch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `qty` decimal(10,2) NOT NULL,
  `withdrawal_product_id` int(11) NOT NULL,
  `unit_of_measure_id` int(11) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `withdrawal_batch`
--

INSERT INTO `withdrawal_batch` (`id`, `batch_id`, `location_id`, `qty`, `withdrawal_product_id`, `unit_of_measure_id`, `is_deleted`) VALUES
(1, 107, 4, '4545.00', 11, 5, 0),
(2, 104, 4, '1500.00', 12, 8, 0),
(3, 104, 4, '500.00', 13, 5, 0),
(4, 104, 4, '500.00', 13, 5, 0),
(5, 105, 3, '123.00', 6, 5, 0),
(6, 104, 4, '231.00', 7, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_cdr_details`
--

CREATE TABLE IF NOT EXISTS `withdrawal_cdr_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdrawal_log_id` int(11) NOT NULL,
  `cdr_no` varchar(160) NOT NULL,
  `gate_pass_ctrl_no` varchar(160) NOT NULL,
  `gate_pass_swap` tinyint(4) NOT NULL DEFAULT '0',
  `consignee_id` int(11) NOT NULL DEFAULT '0',
  `weight_in` decimal(10,2) NOT NULL,
  `weight_out` decimal(10,2) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `withdrawal_cdr_details`
--

INSERT INTO `withdrawal_cdr_details` (`id`, `withdrawal_log_id`, `cdr_no`, `gate_pass_ctrl_no`, `gate_pass_swap`, `consignee_id`, `weight_in`, `weight_out`, `is_deleted`) VALUES
(2, 8, '324234', '233322', 0, 0, '6000.00', '3250.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_company_details`
--

CREATE TABLE IF NOT EXISTS `withdrawal_company_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdrawal_log_id` int(11) NOT NULL,
  `requestor` varchar(160) NOT NULL,
  `designation` varchar(160) NOT NULL,
  `department` varchar(160) NOT NULL,
  `request_for_withdrawal` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_consignee_details`
--

CREATE TABLE IF NOT EXISTS `withdrawal_consignee_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdrawal_log_id` int(11) NOT NULL,
  `atl_number` varchar(160) NOT NULL,
  `cargo_swap` tinyint(4) NOT NULL DEFAULT '0',
  `mode_of_delivery` varchar(100) NOT NULL,
  `consignee_id` int(11) NOT NULL,
  `origin_vessel_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `withdrawal_consignee_details`
--

INSERT INTO `withdrawal_consignee_details` (`id`, `withdrawal_log_id`, `atl_number`, `cargo_swap`, `mode_of_delivery`, `consignee_id`, `origin_vessel_id`) VALUES
(2, 2, '62434', 0, 'vessel', 1, 1),
(3, 3, '324234', 0, 'truck', 1, 1),
(4, 4, '86542323434', 0, 'vessel', 1, 1),
(5, 5, '7345345345', 0, 'vessel', 1, 2),
(6, 6, '542343', 1, 'truck', 2, 2),
(7, 7, '3524234', 0, 'truck', 1, 2),
(8, 8, '635456334', 1, 'vessel', 2, 2),
(9, 9, '654325', 0, 'truck', 1, 2),
(10, 10, '4234234', 0, 'truck', 1, 2),
(11, 11, '65234234', 0, 'vessel', 2, 3),
(12, 12, '4354345', 0, 'vessel', 1, 3),
(13, 13, '546356245245', 0, 'truck', 1, 2),
(14, 14, '435345', 0, 'vessel', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_consignee_swap`
--

CREATE TABLE IF NOT EXISTS `withdrawal_consignee_swap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdrawal_log_id` int(11) NOT NULL,
  `consignee_id_from` int(11) NOT NULL,
  `consignee_id_to` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `withdrawal_consignee_swap`
--

INSERT INTO `withdrawal_consignee_swap` (`id`, `withdrawal_log_id`, `consignee_id_from`, `consignee_id_to`, `is_deleted`) VALUES
(1, 1, 1, 3, 0),
(2, 6, 2, 3, 0),
(3, 8, 2, 3, 1),
(4, 8, 1, 3, 1),
(5, 8, 2, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_documents`
--

CREATE TABLE IF NOT EXISTS `withdrawal_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdrawal_log_id` int(11) NOT NULL,
  `document_name` varchar(160) NOT NULL,
  `document_path` varchar(250) NOT NULL,
  `date_added` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `withdrawal_documents`
--

INSERT INTO `withdrawal_documents` (`id`, `withdrawal_log_id`, `document_name`, `document_path`, `date_added`, `status`) VALUES
(1, 1, 'ABXYZ', 'http://localhost/apollo/uploads/documents/JE08XtyxLNf38dN2xPqj.pdf', '2016-05-10 13:02:01', 0),
(2, 1, 'ATL-FILE', 'http://localhost/apollo/uploads/documents/WeQeOwhE7hrUkCyPp6lM.pdf', '2016-05-10 13:02:04', 0),
(3, 2, 'Abzs', 'http://localhost/apollo/uploads/documents/JPbZl44zwxZcAK25oCOX.pdf', '2016-05-10 14:03:56', 0),
(4, 2, 'ATL-FILE', 'http://localhost/apollo/uploads/documents/Q7m5zwWHY53972wpD6HX.pdf', '2016-05-10 14:04:00', 0),
(5, 3, 'ATL-FILE', 'http://localhost/apollo/uploads/documents/PQ1BzrjD5ZqHl44bPLuT.pdf', '2016-05-10 14:09:18', 0),
(6, 3, 'Apollo Change Order Table.pdf', 'http://localhost/apollo/uploads/documents/Yo00NnOxbijVJTPFxocI.pdf', '2016-05-10 15:16:30', 0),
(7, 3, 'Delfin-Pagibig-Account.pdf', 'http://localhost/apollo/uploads/documents/XvzqB5dP32XWMp2CLPab.pdf', '2016-05-10 15:17:08', 0),
(8, 3, 'dsaadsasds', 'http://localhost/apollo/uploads/documents/43sGB617qjxVLw0Jl3RA.pdf', '2016-05-10 15:19:37', 0),
(9, 3, 'OOOOOO', 'http://localhost/apollo/uploads/documents/soyuQFVSnKuc2gZNihCv.pdf', '2016-05-10 15:20:01', 0),
(10, 3, 'SO_process.pdf', 'http://localhost/apollo/uploads/documents/6o6FWGF8MM1f5KoRPcvi.pdf', '2016-05-10 15:23:17', 0),
(11, 4, 'ATL-FILE', 'http://localhost/apollo/uploads/documents/e0VlOXHYuKVClCsh0iDo.pdf', '2016-05-10 19:01:59', 0),
(12, 5, 'ATL-FILE', 'http://localhost/apollo/uploads/documents/xYiWyoNp8LYJSNbpRJfo.pdf', '2016-05-11 19:16:54', 0),
(13, 6, 'ATL-FILE', 'http://localhost/apollo/uploads/documents/cTHisvqfCwUhMb1nyc6Z.pdf', '2016-05-11 19:45:24', 0),
(14, 7, 'ATL-FILE', 'http://localhost/apollo/uploads/documents/w39OuXP1CnlZ8aFiLEh3.pdf', '2016-05-11 20:56:20', 0),
(15, 8, 'ATL-FILE', 'http://localhost/apollo/uploads/documents/dvhhC6pCBmQE1fb7ddxd.pdf', '2016-05-11 21:51:15', 0),
(20, 8, 'CDR-FILE', 'http://localhost/apollo/uploads/documents/mbw1eGaN3ykyPpIE0lxV.pdf', '2016-05-14 01:06:25', 0),
(21, 13, 'ATL-FILE', 'http://localhost/apollo/uploads/documents/06VYq0ikrhCc97Jz9g0U.pdf', '2016-05-14 02:00:49', 0),
(22, 14, 'ATL-FILE', 'http://localhost/apollo/uploads/documents/qxXu3SSIJfHFwSgi3vT5.pdf', '2016-05-14 02:20:24', 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_loading_information`
--

CREATE TABLE IF NOT EXISTS `withdrawal_loading_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdrawal_log_id` int(11) NOT NULL,
  `loading_area_id` int(11) NOT NULL,
  `handling_method` varchar(160) NOT NULL,
  `est_date_time` datetime NOT NULL,
  `datetime_start` datetime DEFAULT NULL,
  `datetime_end` datetime DEFAULT NULL,
  `duration` int(11) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `withdrawal_loading_information`
--

INSERT INTO `withdrawal_loading_information` (`id`, `withdrawal_log_id`, `loading_area_id`, `handling_method`, `est_date_time`, `datetime_start`, `datetime_end`, `duration`, `is_deleted`) VALUES
(1, 8, 2, 'auto-bagging machine', '2016-05-31 05:00:00', '2016-05-18 02:58:00', '2016-05-24 02:58:00', 518400, 0),
(2, 9, 2, 'auto-bagging machine', '2016-05-10 02:10:00', NULL, NULL, 0, 0),
(3, 6, 2, 'auto-bagging machine', '2016-05-10 22:17:00', '2016-05-11 09:17:00', '2016-05-13 10:17:00', 0, 0),
(4, 7, 1, 'auto-bagging machine', '2016-05-10 02:45:00', NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_log`
--

CREATE TABLE IF NOT EXISTS `withdrawal_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdrawal_number` varchar(9) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `date_created` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `withdrawal_log`
--

INSERT INTO `withdrawal_log` (`id`, `withdrawal_number`, `user_id`, `type`, `date_created`, `status`, `is_deleted`) VALUES
(2, '2', 10000, 2, '2016-05-10 14:03:56', 0, 0),
(3, '3', 10000, 2, '2016-05-10 14:09:17', 2, 0),
(4, '4', 10000, 2, '2016-05-10 19:01:57', 0, 0),
(5, '5', 10000, 2, '2016-05-11 19:16:50', 0, 0),
(6, '6', 10000, 2, '2016-05-11 19:45:22', 4, 0),
(7, '7', 10000, 2, '2016-05-11 20:56:18', 3, 0),
(8, '8', 10000, 2, '2016-05-11 21:51:13', 1, 0),
(9, '9', 10000, 2, '2016-05-12 18:57:02', 3, 0),
(10, '10', 10000, 2, '2016-05-14 01:55:55', 0, 0),
(11, '11', 10000, 2, '2016-05-14 01:57:07', 0, 0),
(12, '12', 10000, 2, '2016-05-14 01:58:39', 0, 0),
(13, '13', 10000, 2, '2016-05-14 02:00:27', 0, 0),
(14, '14', 10000, 2, '2016-05-14 02:20:23', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_notes`
--

CREATE TABLE IF NOT EXISTS `withdrawal_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdrawal_log_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `subject` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `withdrawal_notes`
--

INSERT INTO `withdrawal_notes` (`id`, `withdrawal_log_id`, `note`, `subject`, `user_id`, `date_created`, `is_deleted`) VALUES
(1, 1, 'Gravida penatibus ipsum autem taciti consequat curae, placerat, aspernatur mollitia aenean eaque! Adipisci dolorum? Augue voluptatem ullamcorper torquent rhoncus nonummy.', 'Gravida penatibus', 10000, '2016-05-10 13:01:56', 0),
(2, 2, 'Doloribus soluta fuga vel nostrum corporis accusamus. Porta augue, iusto totam iusto? Earum nulla mollis condimentum distinctio. Blanditiis wisi nihil.', 'Doloribus soluta', 10000, '2016-05-10 13:58:03', 0),
(3, 3, 'rwerewrewrewrewrew', 'raarere', 10000, '2016-05-10 15:36:46', 0),
(4, 3, 'Eget temporibus. Aliquid justo? Volutpat montes, deleniti varius venenatis pulvinar blanditiis nascetur suspendisse! Qui. Congue nullam quas ut. Phasellus illo.', 'Eget temporibus', 10000, '2016-05-10 15:38:19', 0),
(5, 3, 'Eget temporibus. Aliquid justo? Volutpat montes, deleniti varius venenatis pulvinar blanditiis nascetur suspendisse! Qui. Congue nullam quas ut. Phasellus illo.', 'Eget temporibus', 10000, '2016-05-10 15:38:47', 0),
(6, 3, 'Doloribus odio sit unde, perferendis eum? Nec dolore! Aliquet do irure lectus quibusdam, dis sapiente aperiam alias mollit corporis corporis.', 'Doloribus odio', 10000, '2016-05-10 15:41:39', 0),
(7, 3, 'Vero habitasse elit pharetra voluptates natoque fusce praesent, penatibus, perspiciatis cras rutrum, per? Sollicitudin voluptate, a maxime quas laoreet voluptatum.', 'Vero habitasse', 10000, '2016-05-10 15:42:27', 0),
(8, 2, 'Ipsum fuga nonummy quos? Qui, nisl curae consectetur laboris aspernatur aliquip? Dolor, porttitor error, ridiculus nonummy quos sodales sunt ullamco.', 'Ipsum fuga nonummy quos', 10000, '2016-05-10 15:44:25', 0),
(9, 6, 'Fringilla venenatis nisl vel harum, malesuada torquent nunc consectetuer dolorum temporibus? Quaerat itaque voluptate, odit eros semper tortor ex? Fermentum.', 'Fringilla', 10000, '2016-05-13 02:19:42', 0),
(10, 8, 'Saepe nulla neque reiciendis iaculis laudantium! Repellendus dolore dolorum adipiscing lorem torquent? Similique litora taciti hic suspendisse felis eveniet, adipiscing.', 'Saepe nulla neque', 10000, '2016-05-13 04:29:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_product`
--

CREATE TABLE IF NOT EXISTS `withdrawal_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdrawal_log_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `loading_method` varchar(60) NOT NULL,
  `unit_of_measure_id` int(11) NOT NULL,
  `qty` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `withdrawal_product`
--

INSERT INTO `withdrawal_product` (`id`, `withdrawal_log_id`, `product_id`, `loading_method`, `unit_of_measure_id`, `qty`, `status`) VALUES
(1, 1, 2, 'bulk', 5, '123.00', 0),
(2, 2, 2, 'bulk', 6, '450.00', 0),
(3, 3, 4, 'bag', 8, '10.10', 0),
(4, 4, 5, 'bag', 8, '600.00', 0),
(5, 5, 4, 'bulk', 5, '100.00', 0),
(6, 6, 4, 'bulk', 5, '123.00', 0),
(7, 7, 2, 'bulk', 5, '231.00', 0),
(8, 8, 4, 'bulk', 5, '234234.00', 1),
(9, 8, 2, 'bulk', 5, '324234.00', 1),
(10, 8, 4, 'bulk', 5, '12321.00', 1),
(11, 8, 5, 'bulk', 5, '4545.00', 0),
(12, 8, 2, 'bag', 8, '1500.00', 0),
(13, 9, 2, 'bulk', 5, '1000.00', 0),
(14, 10, 4, 'bulk', 6, '50.00', 0),
(15, 11, 4, 'bulk', 6, '600.00', 0),
(16, 12, 2, 'bulk', 9, '6000.00', 0),
(17, 13, 2, 'bulk', 5, '3221.00', 0),
(18, 13, 5, 'bulk', 5, '2131.00', 0),
(19, 14, 2, 'bulk', 5, '123.00', 1),
(20, 14, 4, 'bulk', 5, '3213.00', 0),
(21, 14, 2, 'bulk', 5, '234234.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_seal`
--

CREATE TABLE IF NOT EXISTS `withdrawal_seal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdrawal_log_id` int(11) NOT NULL,
  `number` varchar(160) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `withdrawal_seal`
--

INSERT INTO `withdrawal_seal` (`id`, `withdrawal_log_id`, `number`, `is_deleted`) VALUES
(1, 8, 'SAD232', 0),
(2, 8, 'HF3232', 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_truck_details`
--

CREATE TABLE IF NOT EXISTS `withdrawal_truck_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdrawal_log_id` int(11) NOT NULL,
  `trucking` varchar(160) NOT NULL,
  `plate_no` varchar(160) NOT NULL,
  `driver_name` varchar(160) NOT NULL,
  `license_no` varchar(160) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `withdrawal_truck_details`
--

INSERT INTO `withdrawal_truck_details` (`id`, `withdrawal_log_id`, `trucking`, `plate_no`, `driver_name`, `license_no`, `is_deleted`) VALUES
(1, 1, '5434aseawe', 'rw234423', 'areawerwer', 'waerawer', 0),
(2, 3, 'Allan', '123890', 'Delfin', 'ABC-123-432', 0),
(3, 6, 'Wahsdaas', 'wereqwojrkoqierj', 'ehwirugewhrh', 'eruiwhwuierhwe', 0),
(4, 7, 'asdasd', 'adsasdads', 'asdads', 'asdads', 0),
(5, 8, 'ratwawte', 'tawtwaetawe', 'taewtaewtawe', 'taweatwe', 1),
(6, 8, 'dfdfds', 'fdsfdsfsd', 'fdsfdssfd', 'sfdfsd', 1),
(7, 9, 'faafd', 'fdsafads', 'fsdafads', 'fdasafsd', 0),
(8, 10, '234asd 324', '432ASDASd', 'Delfin', 'aSDS 23 ', 0),
(9, 13, 'fsadaf', 'ewrwara', 'aewrawe', 'rawer', 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal_vessel_details`
--

CREATE TABLE IF NOT EXISTS `withdrawal_vessel_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `withdrawal_log_id` int(11) NOT NULL,
  `vessel_id` int(11) NOT NULL,
  `destination` varchar(160) NOT NULL,
  `captain` varchar(160) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `withdrawal_vessel_details`
--

INSERT INTO `withdrawal_vessel_details` (`id`, `withdrawal_log_id`, `vessel_id`, `destination`, `captain`, `is_deleted`) VALUES
(1, 0, 2, 'Hong Kong', 'Negro', 0),
(2, 2, 2, 'Brazil', 'Nene Hilario', 0),
(3, 4, 1, 'Philippines', 'Coca Cola', 0),
(4, 5, 3, 'Wherever', 'Hello', 0),
(5, 8, 3, 'fsdafsdafsda', 'fdasfdaasfdfd', 1),
(6, 8, 4, 'gfagdafsdfsa', 'fdasdffadsasdf', 1),
(7, 8, 3, 'USA', 'Lorenz', 0),
(8, 11, 4, 'Somewhere out there', 'Lorenz', 0),
(9, 12, 2, 'Soem er er', 'Lolo RAra', 0),
(10, 14, 3, 'Destination', 'Captain', 0);

-- --------------------------------------------------------

--
-- Table structure for table `worker_list`
--

CREATE TABLE IF NOT EXISTS `worker_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `record_type` int(11) NOT NULL DEFAULT '0',
  `record_log_id` int(11) NOT NULL,
  `name` varchar(75) NOT NULL,
  `worker_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `worker_list`
--

INSERT INTO `worker_list` (`id`, `record_type`, `record_log_id`, `name`, `worker_type`) VALUES
(3, 2, 8, 'vapor', 3),
(4, 2, 6, 'yehey', 4),
(5, 2, 6, 'jajaja', 5),
(6, 2, 8, 'nike', 6),
(7, 2, 8, 'addidas', 3),
(8, 2, 8, 'hello', 7);

-- --------------------------------------------------------

--
-- Table structure for table `worker_type`
--

CREATE TABLE IF NOT EXISTS `worker_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(160) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `worker_type`
--

INSERT INTO `worker_type` (`id`, `name`) VALUES
(1, 'developer'),
(2, 'dev'),
(3, 'sample'),
(4, 'wehehe'),
(5, 'lala'),
(6, 'shirt'),
(7, 'abc');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
