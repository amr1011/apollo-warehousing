<?php
/** application/libraries/MY_Form_validation **/ 
class Unit_converter 
{

  private $CI;
  private $products_model;

  public function __construct()
  {
    $this->CI =& get_instance();
    $this->CI->load->model('products/products_model');
    $this->products_model = $this->CI->products_model;
    $this->build_constants();
    //Do your magic here
  }
  
  public $constants = array();

   public function build_constants()
   {
      $this->constants = array(
        "m3"=>array(
            "m3" => 1, "l" => 1000, "kg" => 2406.53,  "mt" => 2.41
          ),
          "l"=> array(
            "m3" => 0.001, "l" => 1, "kg" => 1, "mt" => 0.0000010
          ),
          "kg"=> array(
            "m3" => 0.00042, "l" => 1, "kg" => 1, "mt" => 0.001
          ),
          "mt"=> array(
            "m3" => 0.42, "l" => 1000, "kg" => 1000, "mt" => 1
          )
      );


      $this->constants["m3"]["bag"] = floatval($this->constants["m3"]["kg"] * .02);
      $this->constants["l"]["bag"] = floatval($this->constants["l"]["kg"] * .02);
      $this->constants["mt"]["bag"] = floatval($this->constants["mt"]["kg"] * .02);
      $this->constants["kg"]["bag"] = floatval($this->constants["kg"]["kg"] * .02);

      $this->constants["bag"] = array(
        "m3" => $this->constants["m3"]["bag"],
        "l" => $this->constants["l"]["bag"],
        "kg" => 50,
        "bag" => 1,
        "mt" => $this->constants["mt"]["bag"]
      );
   }

   public function get_constants($options){

      $data = $this->constants[$options["unit_from"]];
      return $data;

   }

   public function get_product_model(){
    return $this->products_model->get_unit_of_measure();
   }

   public function convert_measurement($unit_from, $unit_to, $quantity)
   {
      $options = array(
          "unit_from"=>strtolower($unit_from),
          "unit_to"=>strtolower($unit_to),
          "quantity"=>floatval($quantity)
        );

      $value = $this->convert_now($options);
      return  $value;
   }

   public function convert_now($options)
   {  
      $unit = $this->get_constants($options);


      $val_to = $unit[$options["unit_to"]];

      return $options["quantity"] * $val_to;


   }

}


// $c = new MY_Unit_Converter();
// $r = $c->show_result();

// var_dump($r);
// exit();