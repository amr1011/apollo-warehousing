<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Storage_location_management extends CI_Controller {

	public function index()
	{
		if(IS_DEVELOPMENT === true)
		{
			$user_development = array(
				"id" => 10000,
				"email" => "apollo@email.com",
				"username" => "apollo",
				"password" => "62cc2d8b4bf2d8728120d052163a77df",
				"company_id" => 1,
				"role_id" => 1,
				"account_type" => 1,
				"first_name" => "apollo",
				"last_name" => "developer",
				"middle_name" => ""
			);

			$this->session->set_userdata('apollo_user', $user_development);

			// redirect(BASEURL . 'products/product_management');
			$this->home();
		}
		else
		{
			/*LOGIN PROCESS*/
		}
	}

	public function home()
	{
		$data["title"] = "Storage Location Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("storage_location_management/storage_location_management_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function add_storage_location_management()
	{
		$data["title"] = "Storage Location Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("storage_location_management/add_storage_location_management_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function view_storage_location()
	{
		$data["title"] = "Storage Location Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("storage_location_management/view_storage_location_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function storage_location_edit_new_storage()
	{
		$data["title"] = "Storage Location Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("storage_location_management/edit_storage_location_view", $data);
		$this->load->view("includes/footer.php");
	}

}

/* End of file store_location_management.php */
/* Location: ./application/modules/store_location_management/controllers/store_location_management.php */