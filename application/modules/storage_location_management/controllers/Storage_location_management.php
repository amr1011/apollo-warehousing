<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Storage_location_management extends CI_Controller {

	public function index()
	{
		$user_session = $this->session->userdata('apollo_user');
		if($user_session === null){
			redirect(BASEURL.'login');
		}else{
			$this->home();
		}
	}

	public function home()
	{
		$data["title"] = "Storage Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("storage_location_management/storage_location_management_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function add_storage_location_management()
	{
		$data["title"] = "Storage Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("storage_location_management/add_storage_location_management_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function view_storage_location()
	{
		$data["title"] = "Storage Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("storage_location_management/view_storage_location_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function storage_location_edit_new_storage()
	{
		$data["title"] = "Storage Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("storage_location_management/edit_storage_location_view", $data);
		$this->load->view("includes/footer.php");
	}

}

/* End of file store_location_management.php */
/* Location: ./application/modules/store_location_management/controllers/store_location_management.php */