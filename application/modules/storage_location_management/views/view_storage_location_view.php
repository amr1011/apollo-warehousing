<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">
				<ul>
					<li><a href="<?php echo BASEURL; ?>storage/home">Storage Management</a></li>
					<li><span>&gt;</span></li>					
					<li><strong class="black-color">ID 1234566 - Warehouse 1</strong></li>
					
				</ul>
			</div>
			<div class="semi-main">
				<div class="f-right text-right width-250px  margin-bottom-30">		
					<a href="<?php echo BASEURL; ?>storage_location_management/storage_location_edit_new_storage">			
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-15 padding-right-15">Edit Storage Details</button>
					</a>
				</div>
				<div class="clear"></div>

				<div class="border-top border-blue bggray-white box-shadow-light padding-all-20 margin-bottom-20">
					<img id="view_storage_image" class="img-responsive f-left width-30per margin-top-10">
					<div class="f-left width-70per margin-left-20">
						<table class="tbl-4c3h width-100per">
							<tbody>
								<tr>
									<td colspan="4" class="text-left">
										<p class="font-bold font-20 black-color">ID No. <span id="view_storage_id">123456</span> - <span id="view_storage_name">Warehouse</span></p>
									</td>
								</tr>
								<tr>
									<td class="text-left width-35percent"> 
										<p class="font-bold black-color">Category</p>
									</td>
									<td class="text-left">
										<p id="view_storage_categories" class="black-color ">Solid, Liquid, Gas</p>
									</td>
								</tr>
								<tr>
									<td class="text-left width-35percent"> 
										<p class="font-bold black-color">Location Address:</p>
									</td>
									<td class="text-left" colspan="3">
										<p id="view_storage_address" class="black-color">123 Something something St., Kapuso Subd., Siyudad City</p>
									</td>									
								</tr>
								<tr>
									<td class="text-left width-35percent"> 
										<p class="font-bold black-color">Description:</p>
									</td>
									<td class="text-left" colspan="3">
										<p id="view_storage_description" class="black-color">The most cool warehouse of all</p>
									</td>									
								</tr>
								<tr>
									<td class="text-left width-35percent"> 
										<p class="font-bold black-color">Capacity Measurement:</p>
									</td>
									<td class="text-left" colspan="3">
										<p class="black-color"><span id="view_capacity_measurement"></span> </p>
									</td>									
								</tr>
								<tr>
									<td class="text-left width-35percent"> 
										<p class="font-bold black-color">Maximum Capacity:</p>
									</td>
									<td class="text-left" colspan="3">
										<p class="black-color"><span id="view_storage_capacity"></span> <span class="view_storage_measurement"></span></p>
									</td>									
								</tr>
								<tr>
									<td class="text-left width-35percent"> 
										<p class="font-bold black-color">High Setpoint:</p>
									</td>
									<td class="text-left" colspan="3">
										<p class="black-color"><span id="view_high_setpoint"></span> <span class="view_storage_measurement"></span></p>
									</td>									
								</tr>
								<tr>
									<td class="text-left width-35percent"> 
										<p class="font-bold black-color">Low Setpoint:</p>
									</td>
									<td class="text-left" colspan="3">
										<p class="black-color"><span id="view_low_setpoint"></span> <span class="view_storage_measurement"></span></p>
									</td>									
								</tr>
												
						</tbody></table>
					</div>
					<div class="clear"></div> 				
				
				</div>

				

				<div class="bggray-white box-shadow-light border-top border-blue padding-all-20 width-100percent text-left width-100percent margin-bottom-30 font-0">
					
					<div class="width-100percent">
						<p class="no-margin-all font-20 font-500 width-100percent">Storage Specification</p>
						<div class="width-100percent padding-top-20">
							<p class="no-margin-all display-inline-mid font-500 ">Storage Type: </p>
							<p class="display-inline-mid font-400 font-14 margin-left-10" id="storageTypeName">Warehouse</p>
						</div>
						<div class="width-100percent padding-top-10">
							<p class="no-margin-all display-inline-mid font-500 ">Storage Hierarchy: </p>
							<p class="display-inline-mid font-400 font-14 margin-left-10" id="hierarchyName"></p>
						</div>
					</div>

					<div class="width-100percent border-full padding-top-5 padding-bottom-5 margin-top-15">
						<div class="panel-group sample-accordion ">	
							<div class="accordion_custom border-top-none no-margin-bottom" id="storageHIerarchyListDisplayViewMode">
							</div>
						</div>
					</div>					
				</div>												
			</div>			
		</div>
	</div>