<div class="main">
	<div class="breadcrumbs no-margin-left padding-left-50">
		<ul>
			<li><a href="<?php echo BASEURL; ?>storage_location_management">Storage Management</a></li>
			<li><span>&gt;</span></li>					
			<li><strong class="black-color"><span id="crumb_edit_storage_id">ID 1234566</span> - <span id="crumb_edit_storage_name">Warehouse 1</span></strong></li>
		</ul>
	</div>
	<div class="semi-main">
		<form id="formEditStorage">

			<div class="f-left margin-top-10">	
				<i class="fa fa-trash-o default-cursor font-20 gray-color modal-trigger delete-button" modal-target="trash-modal"></i>
			</div>
			<div class="f-right text-right width-250px  margin-bottom-30">
				<a href="<?php echo BASEURL . "storage_location_management"; ?>">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				</a>
				<button id="submit_edit_storage" type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-15 padding-right-15">Save Changes</button>
			</div>
			<div class="clear"></div>

			<div class="bggray-white box-shadow-light border-top border-blue padding-all-20 width-100percent text-left width-100percent margin-bottom-30">
				<div id="upload_photo" class="position-rel default-cursor bggray-white display-inline-mid">
				</div>
				<div class="display-inline-top width-400px margin-all-10">
					<div class="width-100percent margin-bottom-10">
						<p class="font-bold black-color no-margin-all f-left width-120px font-14 padding-top-5">Storage ID:</p>
						<input type="text" id="edit_storage_id" class="width-270px t-small f-left width-240px padding-all-10 margin-left-10" placeholder="Storage ID">
						<div class="clear"></div>
					</div>

					<div class="width-100percent">
						<p class="font-bold black-color no-margin-all f-left width-120px padding-top-5">Location Address:</p>
						<textarea id="edit_storage_address" datavalid="required" labelinput="Location Address" class="width-270px f-left margin-left-10 height-110px font-14 " placeholder="Enter Location Address Here"></textarea>
						<div class="clear"></div>
					</div>
				</div>
				<div class="display-inline-top width-390px margin-all-10">
					<div class="width-100percent margin-bottom-10">
						<p class="font-bold black-color no-margin-all f-left width-110px padding-top-5">Storage Name:</p>
						<input id="edit_storage_name" datavalid="required" labelinput="Storage Name" type="text" class="t-small f-left width-270px padding-all-10" placeholder="Storage name...">
						<div class="clear"></div>
					</div>

					<div class="width-100percent ">
						<p class="font-bold black-color no-margin-all f-left width-110px font-14 padding-top-5">Description:</p>
						<textarea id="edit_storage_description" datavalid="required" labelinput="Storage Description" class="width-270px f-left  height-110px font-14" placeholder="Enter Description Here"></textarea>
						<div class="clear"></div>
					</div>
				</div>

			</div>

			<div class="bggray-white box-shadow-light border-top border-blue padding-all-20 width-100percent text-left width-100percent margin-bottom-30 font-0">
				<div class="display-inline-mid width-60percent ">
					<div class="display-inline-mid">
						<p class="font-20 font-500 padding-bottom-30">Storage Capacity</p>
						<p class="font-14 font-bold black-color padding-bottom-30">Capacity Measurement:</p>
						<p class="font-14 font-bold black-color padding-bottom-30">Maximum Capacity:</p>
						<p class="font-14 font-bold black-color padding-bottom-30">High Setpoint:</p>
						<p class="font-14 font-bold black-color padding-bottom-30">Low Setpoint:</p>
					</div>

					<div class="display-inline-top margin-left-20">
						<div class="width-100percent padding-top-50 padding-bottom-15">
							<div class="select display-block large ">
								<select id="capacityMeasure">
									<option value="weight">Weight</option>
									<option value="volume">Volume</option>
								</select>
							</div>
						</div>

						<div class="width-100percent">
							<input id="maximumStorage" type="text" datavalid="required" labelinput="Maximum Storage" class="super-width-100px t-small f-left  padding-all-10 ">
							<div class="select padding-bottom-11 margin-left-10 small">
								<select id="selectUnitOfMeasure">
								</select>
							</div>
						</div>

						<div class="width-100percent">
							<input id="highSetPoint" type="text" datavalid="required" labelinput="High Setpoint" class="super-width-100px t-small f-left  padding-all-10 ">								
							<p class="padding-left-10 padding-top-5 f-left unit-of-measure-display"> </p>
							<div class="clear"></div>
						</div>

						<div class="width-100percent padding-top-15">
							<input id="lowSetPoint" type="text" datavalid="required" labelinput="Low Setpoint" class="super-width-100px t-small f-left  padding-all-10 ">
							<p class="padding-left-10 padding-top-5 f-left unit-of-measure-display"> </p>
							<div class="clear"></div>
						</div>

					</div>
				</div>
			

				<div class="display-inline-top width-40percent">
					<p class="font-20 font-bold">Storage Category</p>
					<div class="display-inline-mid padding-left-15 padding-top-20" id="categoryTypeContainerEdit">
						 
					</div>
				</div>
				
			</div>

			<div class="bggray-white box-shadow-light border-top border-blue padding-all-20 width-100percent text-left width-100percent margin-bottom-30 font-0">
				
				<div class="width-100percent">
					<p class="no-margin-all font-20 font-500 width-100percent">Storage Specification</p>
					<div class="width-100percent padding-top-20">
						<p class="no-margin-all display-inline-mid font-bold black-color font-14">Storage Type: </p>
						<div class="select large padding-left-20 ">
							<select id="storageType">
							</select>
						</div>
						<p class="no-margin-all display-inline-mid padding-left-30 font-bold black-color font-14">Storage Hierarchy:</p>
						<div class="select large padding-left-20">
							<select id="hierarchySelect"></select>
						</div>
					</div>
				</div>

				
				<div class="width-100percent border-full padding-top-5 padding-bottom-5 margin-top-30">
					<p class="no-margin-all padding-all-5 font-400 font-14 f-left">Double-click the location name to edit. Click on the buttons on the right to add or remove locations</p>

					<div class="f-right padding-top-5">
						<div class="display-inline-mid  height-22px width-20px   text-center  margin-right-10">
							<i class="fa fa-minus font-20 text-left default-cursor gray-color" id="removeListHierarchyEdit"></i>
						</div>
						<div class="display-inline-mid  height-22px width-20px   text-center  margin-right-10">
							<i class="fa fa-plus font-20 default-cursor gray-color" id="addListHierarchyEdit" ></i>	
						</div>
						
					</div>
					<div class="clear"></div>

				</div>

				<div class="panel-group sample-accordion ">	

					<div class="accordion_custom border-top-none no-margin-bottom" id="storageHIerarchyListDisplay">
					</div>						
				</div>	

			</div>


		</form>
	</div>
</div>

<div class="modal-container" modal-id="update-storage">
	<div class="modal-body small">				

		<div class="modal-head">
			<h4 class="text-left">Message</h4>				
		</div>

		<!-- content -->
		<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
			<div class="padding-all-10">
				<p class="font-16 font-400" style="line-height:16px;">Updating...</p>
			</div>
		</div>
	
		<div class="modal-footer text-right" style="height: 50px;">
		</div>
	</div>	
</div>
