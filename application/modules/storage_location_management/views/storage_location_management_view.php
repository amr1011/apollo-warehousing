<div class="main">
			<div class="center-main">

				<div class="mod-select padding-left-3 padding-top-60">			
					<div class="select f-left width-350px ">
						<select>
							<option value="Displaying All Active Product"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>						
				</div>
				<div class="f-right margin-top-10">	
					<a href="<?php echo BASEURL; ?>storage_location_management/add_storage_location_management">
						<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn">Add New Storage</button>				
					</a>
					<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn dropdown-btn">View Filter / Search</button>					
				</div>
				<div class="clear"></div>

				<div class="dropdown-search">
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr>
								<td class="font-15 font-400 black-color">Search</td>
								<td>
									<div class="select">
										<select>
											<option value="items">Items</option>
										</select>
									</div>
								</td>
								<td>
									<input type="text" class="t-small width-100per" />
								</td>
							</tr>
							<tr>
								<td class="font-15 font-400 black-color">Filter By:</td>
								<td>
									<div class="select">
										<select>
											<option value="calender">Calendar Here</option>
										</select>
									</div>
								</td>
								<td class="search-action">
									<div class="f-left">
										<div class="select width-150px">
											<select>
												<option value="origin">Origin</option>
											</select>
										</div>
										<div class="select width-150px">
											<select>
												<option value="status">Status</option>
											</select>
										</div>
									</div>
									<div class="f-right margin-left-30">
										<p class="display-inline-mid">Sort By:</p>
										<div class="select width-150px display-inline-mid">
											<select>
												<option value="withdrawal No.">Withdrawal No.</option>
											</select>
										</div>
										<div class="select width-150px display-inline-mid">
											<select>
												<option value="ascending">Ascending</option>
												<option value="descending">Descending</option>
											</select>
										</div>
									</div>
									<div class="clear"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10">
						<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
						<p class="display-inline-mid font-15 font-bold margin-left-5">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 margin-right-30">Search</button>
					<div class="clear"></div>
					<div class="search-result">
						<div class="margin-left-30">
							<p class="font-400 font-15 black-color f-left">Who can also view this Search Result? </p>
							<div class="clear"></div>
							
							<div class="f-left">
								<div class="display-inline-mid margin-right-10">
									<input type="radio" id="me" name="search-result" class="width-20px">
									<label for="me" class="margin-bottom-5 default-cursor font-15 black-color font-400">Only Me</label>
								</div>
								<div class="display-inline-mid margin-right-50">
									<input type="radio" id="selected" name="search-result" class="width-20px">
									<label for="selected" class="margin-bottom-5 default-cursor font-15 black-color font-400">With Selected Members</label>
								</div>
								<div class="display-inline-mid member-selection">
									<div class="member-container">
										<p>1 Member Selected</p>
										<i class=" fa fa-caret-down"></i>
									</div>
									<div class="popup_person_list">
										<div class="popup_person_list_div">
											
											<div class="thumb_list_view small marked">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="<?php echo BASEURL;?>assets/images/profile/profile_d.png" />
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="<?php echo BASEURL;?>assets/images/ui/check_icon.svg">
												</div>
											</div>

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="<?php echo BASEURL;?>assets/images/profile/profile_d.png" />
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="<?php echo BASEURL;?>assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="<?php echo BASEURL;?>assets/images/profile/profile_d.png" />
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="<?php echo BASEURL;?>assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="<?php echo BASEURL;?>assets/images/profile/profile_d.png" />
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia Ricardo Gomez Kuala Lumpur Eklabu</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="<?php echo BASEURL;?>assets/images/ui/check_icon.svg">
												</div>
											</div>	

										</div>										
									</div>
								</div>
								<div class="display-inline-mid margin-left-10">								
									<!-- dropdown here with effects  -->
									<button type="button" class="general-btn">Save Search Results</button>
								</div>
							</div>
							<div class="clear"></div>
						</div>

					</div>
				</div>
				<div id="storage_location_list"class="width-100percent bggray-white">
					<table class="tbl-4c3h margin-top-30">
						<thead>
							<tr>
								<th class="width-15percent black-color">Storage ID</th>
								<th class="width-25percent black-color">Storage Name</th>
								<th class="width-15percent black-color">Category</th>
								<th class="width-15percent black-color">Type</th>
								<th class="width-30percent black-color">Address</th>												
							</tr>
						</thead>
					</table>

					<!-- <div class="tbl-like">
					
						<div class="first-tbl height-auto text-left">
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">123456</p>
							<p class="width-25percent font-400 font-14 display-inline-mid f-none text-center ">Warehouse 1</p>
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center ">Solid, Liquid, Gas</p>
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center ">Warehouse</p>
							<p class="width-28percent font-400 font-14 display-inline-mid f-none text-center ">123 Something something St., Kapuso Sub., Siyudad City</p>							
						</div>

						<div class="hover-tbl height-100per">
							<a href="<?php echo BASEURL;?>storage_location_management/view_storage_location">
								<button class="btn general-btn ">View Storage Settings</button>
							</a>
						</div>

					</div>

					<div class="tbl-like">
						<div class="first-tbl height-auto text-left">
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">123456</p>
							<p class="width-25percent font-400 font-14 display-inline-mid f-none text-center">Warehouse 2</p>
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">Solid, Liquid, Gas</p>
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">Warehouse</p>
							<p class="width-28percent font-400 font-14 display-inline-mid f-none text-center">123 Something something St., Kapuso Sub., Siyudad City</p>
							
						</div>
						<div class="hover-tbl height-100per"
>							<a href="<?php echo BASEURL; ?>storage_location_management/view_storage_location" class="">
								<button class="btn general-btn">View Storage Settings</button>
							</a>
						</div>
					</div>

					<div class="tbl-like">
						<div class="first-tbl height-auto text-left">
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">123456</p>
							<p class="width-25percent font-400 font-14 display-inline-mid f-none text-center">Silo 1</p>
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">Solid</p>
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">Silo</p>
							<p class="width-28percent font-400 font-14 display-inline-mid f-none text-center">123 Something something St., Kapuso Sub., Siyudad City</p>							
						</div>
						<div class="hover-tbl height-100per">
							<a href="<?php echo BASEURL; ?>storage_location_management/view_storage_location" class="">
								<button class="btn general-btn">View Storage Settings</button>
							</a>
						</div>
					</div>

					<div class="tbl-like">
						<div class="first-tbl height-auto text-left ">
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">123456</p>
							<p class="width-25percent font-400 font-14 display-inline-mid f-none text-center">Silo 2</p>
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">Solid</p>
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">Silo</p>
							<p class="width-28percent font-400 font-14 display-inline-mid f-none text-center">123 Something something St., Kapuso Sub., Siyudad City</p>							
						</div>
						<div class="hover-tbl height-100per">
							<a href="<?php echo BASEURL; ?>storage_location_management/view_storage_location" class="">
								<button class="btn general-btn">View Storage Settings</button>
							</a>
						</div>
					</div>

					<div class="tbl-like">
						<div class="first-tbl height-auto text-left">
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">987654</p>
							<p class="width-25percent font-400 font-14 display-inline-mid f-none text-center">Stock Room 2</p>
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">Solid</p>
							<p class="width-15percent font-400 font-14 display-inline-mid f-none text-center">Stock Room</p>
							<p class="width-28percent font-400 font-14 display-inline-mid f-none text-center">123 Something something St., Kapuso Sub., Siyudad City</p>							
						</div>
						<div class="hover-tbl height-100per">
							<a href="<?php echo BASEURL; ?>storage_location_management/view_storage_location" class="">
								<button class="btn general-btn">View Storage Settings</button>
							</a>
						</div>
					</div> -->
				</div>

			</div>
		</div>