<div class="main">	
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL; ?>team_management">Team Management </a></li>
					<li><span>&gt;</span></li>						
					<li class="team-name font-bold black-color">Operations Team</li>
				</ul>
			
			</div>	
			<div class="semi-main margin-bottom-50">
				<div class="f-right">
					<button type="button" class="btn general-btn modal-trigger  padding-left-20 padding-right-20" modal-target="add-user">Add User</button>
					<button type="button" class="btn btn-edit-team general-btn margin-left-10 modal-trigger  padding-left-20 padding-right-20" modal-target="edit-team">Edit Team</button>
				</div>
				<div class="clear"></div>
				<div class="user-list padding-all-20 table-view bggray-white border-blue border-top margin-top-30 box-shadow-light">
					<p class="font-20 black-color font-bold f-left">Members</p>
					<div class="clear"></div>

					<div class="bdg-nolink width-20per text-center position-rel user-template" style="display:none;">
						<div class="hover-me border-radius-half position-rel">
							<div class="half-border-radius overflow-hide width-130px margin-auto">
								<img src="../assets/images/profile/kat.jpg" alt="big-user-head" class="default-cursor height-130px width-130px user-image" >
								<div class="team-img-cover modal-trigger" modal-target="view-user"></div>
							</div>
							<p class="font-14 font-500 black-color user-name">Sarah Menguita</p>
						</div>
					</div>

					<!-- <div class="bdg-nolink width-20per text-center position-rel">
						<div class="hover-me border-radius-half position-rel">
							<div class="half-border-radius overflow-hide width-130px margin-auto">
								<img src="../assets/images/profile/angel1.jpg" alt="big-user-head" class="default-cursor height-130px width-130px " >
								<div class="team-img-cover modal-trigger" modal-target="view-user"></div>
							</div>
							<p class="font-14 font-500 black-color">Angel Locsin</p>
						</div>						
					</div>

					<div class="bdg-nolink width-20per text-center position-rel ">
						<div class="hover-me border-radius-half position-rel">
							<div class="half-border-radius overflow-hide width-130px margin-auto">
								<img src="../assets/images/profile/kat.jpg" alt="big-user-head" class="default-cursor height-130px width-130px " >
								<div class="team-img-cover modal-trigger" modal-target="view-user"></div>
							</div>
							<p class="font-14 font-500 black-color">Liza Soberano</p>
						</div>
						
					</div>

					<div class="bdg-nolink width-20per text-center position-rel ">
						<div class="hover-me border-radius-half position-rel">
							<div class="half-border-radius overflow-hide width-130px margin-auto">
								<img src="../assets/images/profile/kat1.jpg" alt="big-user-head" class="default-cursor height-130px width-130px " >
								<div class="team-img-cover modal-trigger" modal-target="view-user"></div>
							</div>
							<p class="font-14 font-500 black-color">Kathryn Bernado</p>
						</div>
						
					</div> -->

					<!-- <div class="bdg-nolink width-20per text-center position-rel  ">
						<div class="hover-me border-radius-half position-rel">
							<div class="half-border-radius overflow-hide width-130px margin-auto">
								<img src="../assets/images/profile/nadine.jpg" alt="big-user-head" class="default-cursor height-130px width-130px " >
								<div class="team-img-cover modal-trigger" modal-target="view-user"></div>
							</div>
							<p class="font-14 font-500 black-color">Nadine Lustre</p>
						</div>						
					</div> -->
				</div>
			</div>
		</div>


		<!-- ================================================== -->
		<!-- ==================EDIT TEAM MODAL================= -->
		<!-- ================================================== -->
		<div class="edit-team-modal modal-container " modal-id="edit-team">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left ">Edit Team</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">	
					<p class="display-inline-mid no-margin-all font-400 padding-right-20 font-14">Team Name </p>
					<input id="team_name" type="text" class="display-inline-mid width-300px">
				</div>
				<div class="  modal-footer margin-bottom-10">
					<i class="btn-delete-team fa fa-trash-o font-22 icon-hover default-cursor f-left padding-top-5 gray-color"></i>
					<div class="f-right width-200px">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<button type="button" id="submit_edit_team" class="font-12 btn btn-primary font-12 display-inline-mid">Save Changes</button>
					</div>
					<div class="clear"></div>
				</div>
			</div>	
		</div>
		<!-- ================================================== -->
		<!-- ===============END EDIT TEAM MODAL================ -->
		<!-- ================================================== -->


		<!-- ================================================== -->
		<!-- ================VIEW USER MODAL=================== -->
		<!-- ================================================== -->
		<div class="modal-container margin-top-30 user-modal" modal-id="view-user">
			<div class="modal-body medium">				

				<div class="modal-head">
					<h4 class="text-left">View User</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content">
					<p class="edit-user text-right  blue-color close-me modal-trigger default-cursor no-margin-all font-16" modal-target="edit-user"><i class="btn-edit-user fa fa-pencil font-20 margin-right-10 gray-color"></i></p>
					<div class="default-cursor text-center half-border-radius overflow-hide width-130px  margin-auto">
						<img src="../assets/images/profile/nadine.jpg" alt="Profile Picture" class="height-130px user-pic">
					</div>

					<div class="width-100per padding-top-10 font-0">
						<!--left content-->
						<div class="display-inline-top width-50per">
							<div class="width-100per">
								<p class="width-100px display-inline-mid font-bold black-color font-14 margin-bottom-10">First Name:</p>
								<p class="user-fname display-inline-mid font-14 font-300 margin-bottom-10">Donna</p>
							</div>

							<div class="width-100per">
								<p class="display-inline-mid width-100px font-bold black-color font-14 margin-bottom-10">Last Name:</p>
								<p class="user-lname display-inline-mid font-14 font-300 margin-bottom-10">Diaz</p>
							</div>

							<div class="width-100per">
								<p class="display-inline-mid width-100px font-14 font-bold black-color margin-bottom-10">User Role:</p>
								<p class="user-role display-inline-mid font-14 font-300 margin-bottom-10">Admin</p>
							</div>
						</div>
						<!--right content-->
						<div class="display-inline-top width-50per">
							<div class="width-100per">
								<p class="font-bold black-color display-inline-mid width-100px font-14 margin-bottom-10">User Name:</p>
								<p class="user-username font-300 display-inline-mid font-14 margin-bottom-10">donnadiaz2013</p>
							</div>

							<div class="width-100per">
								<p class="font-bold black-color width-100px display-inline-mid font-14 margin-bottom-10">Password:</p>
								<p class="user-password font-300 display-inline-mid font-14 margin-bottom-5">***********</p>
								<!-- <a href="#" class="show-password-view f-right font-300 font-14">Show Password</a> -->
								<div class="clear"></div>
							</div>
							
						</div>
					</div>

					<div class="width-100per padding-all-10 bggray-middark  margin-top-10">
						<p class="font-500 font-14 padding-bottom-5 no-margin-all">System Permissions:</p>
						<!-- view user  -->

						<div class="view-content1 display-inline-mid width-200px" >
							<p class="font-300 font-14 no-margin-left margin-bottom-10 margin-top-15">Products Modules</p>
							<div>
								<input type="checkbox" class="default-cursor view-user-permission" data-id="1" id="receiving-view" disabled>
								<label for="receiving-view" class="font-14 font-300 default-cursor color-modal">Receiving</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor view-user-permission" data-id="2" id="withdrawal-view" disabled>

								<label for="withdrawal-view" class="font-14  default-cursor color-modal font-300">Withdrawal</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor view-user-permission" data-id="3" id="transfer-view" disabled>
								<label for="transfer-view" class="font-14  default-cursor color-modal font-300">Transfer</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor view-user-permission" data-id="4" id="product-view" disabled>

								<label for="product-view" class="font-14 font-300  default-cursor">Product Inventory</label>
							</div>
						</div>

						<div class="view-content2 display-inline-mid width-250p">
							<p class="font-14 no-margin-left margin-bottom-10 margin-top-15 font-300">System Configuration Modules</p>
							<div>
								<input type="checkbox" class="default-cursor view-user-permission" data-id="5" id="pro-manage-view"  disabled>
								<label for="pro-manage-view" class="font-14 font-300 default-cursor">Product Management</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor view-user-permission" data-id="6" id="storage-loc-view"  disabled>
								<label for="storage-loc-view" class="font-14 font-300 default-cursor" >Storage Location Management</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor view-user-permission" data-id="7" id="vessel-view" disabled>
								<label for="vessel-view" class="font-14 font-300  default-cursor">Vessel Management</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor view-user-permission" data-id="8" id="consignee-view" disabled>
								<label for="consignee-view" class="font-14 font-300 default-cursor">Consignee Management</label>
							</div>
						</div>

						<div class="view-content3 display-inline-top">
							<div class="margin-top-35">
								<input type="checkbox" class="default-cursor view-user-permission" data-id="9" id="team-view" disabled>

								<label for="team-view" class="font-14 font-300 default-cursor">Team Management</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor view-user-permission" data-id="10" id="system-view" disabled>

								<label for="system-view" class="font-14 font-300 default-cursor">System Settings</label>
							</div>
						</div>				
					</div>
				</div>
				<div class="modal-footer ">
					
				</div>
			</div>
		</div>
		<!-- ================================================== -->
		<!-- ==============END VIEW USER MODAL================= -->
		<!-- ================================================== -->


		<!-- ================================================== -->
		<!-- =================ADD USER MODAL=================== -->
		<!-- ================================================== -->
		<div class="modal-container margin-top-30" modal-id="add-user">
			<div class="modal-body large">				
				<div class="modal-head">
					<h4 class="text-left">Add User</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content">
					<p class="font-14 font-400 light-red-color text-right">Fields with asterisk "*" are required</p>
				
					<div class="image text-center upload-user margin-auto" style="font-size: 0;
															    height: 130px;
															    width: 130px;
															    text-align: center;
															    margin: 5px;">
						<i class="camera fa fa-camera margin-top-35 default-cursor"></i>
						<p class="gray-color font-14 font-400">Upload Photo</p>
					</div>

					<table class="width-100per margin-top-20">
						<tbody>
							<tr>
								<td class="font-bold font-14 black-color width-100px">First Name: <span class="light-red-color font-14">*</span></td>
								<td class="width-100px"><input id="fname" type="text" class="width-200px no-margin-all"></td>
								<td class="font-bold font-14 black-color width-80px">User Name: <span class="light-red-color font-14">*</span></td>
								<td><input id="username" type="text" class="width-200px no-margin-all"></td>

							</tr>
							<tr>
								<td class="font-bold font-14 black-color width-80px">Last Name:</td>
								<td class="width-100px"><input id="lname" type="text" class="width-200px no-margin-all"></td>
								<td class="font-bold font-14 black-color width-80px">Password: <span class="light-red-color font-14">*</span></td>
								<td><input id="password" type="password" class="width-200px no-margin-all"></td>
							</tr>
							<tr>
								<td class="font-bold font-14 black-color width-80px">User Role: <span class="light-red-color font-14">*</span></td>
								<td class="">
									<div class="add-user-role select medium">
										<select class="user-role-dd">
											<option value="role1">Admin</option>
											<option value="role2">Super Visor</option>
											<option value="role4">Operator</option>
										</select>
									</div>
								</td>
								<td colspan="2" class="text-right padding-right-30 padding-bottom-25"><a href="#" class="show-password margin-left-10 font-14 font-300">Show Password</a><a href="#" class="hide-password margin-left-10 font-14 font-300 hidden">Hide Password</a></td>
							</tr>
						</tbody>
					</table>

					<div class="width-100per padding-all-10 bggray-middark  margin-top-10">
						<p class="font-500 padding-bottom-5 no-margin-all font-14 ">System Permissions: <span class="light-red-color font-14">*</span></p>
						<!-- add user  -->

						<div class="add-content1 display-inline-mid width-200px" >
							<p class="font-300 font-15 no-margin-left margin-bottom-10 margin-top-15 ">Products Modules</p>
							<div>
								<input type="checkbox" class="default-cursor permission-box" data-id="1" id="receiving-add" name="permission_box">
								<label for="receiving-add" class="font-14 font-300 light-gray-color default-cursor">Receiving</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor permission-box" data-id="2" id="withdrawal-add" name="permission_box">
								<label for="withdrawal-add" class="font-14 font-300 default-cursor">Withdrawal</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor permission-box" data-id="3" id="transfer-add" name="permission_box">
								<label for="transfer-add" class="font-14 font-300 default-cursor">Transfer</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor permission-box" data-id="4" id="product-add" name="permission_box">
								<label for="product-add" class="font-14 font-300 default-cursor">Product Inventory</label>
							</div>
						</div>

						<div class="add-content2 display-inline-mid width-250px margin-left-30">
							<p class="font-300 font-14 no-margin-left margin-bottom-10 margin-top-15">System Configuration Modules</p>
							<div>
								<input type="checkbox" class="default-cursor permission-box" data-id="5" id="pro-manage-add" name="permission_box">
								<label for="pro-manage-add" class="font-14 font-300 default-cursor">Product Management</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor permission-box" data-id="6" id="storage-loc-add" name="permission_box">
								<label for="storage-loc-add" class="font-14 font-300 default-cursor">Storage Location Management</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor permission-box" data-id="7" id="vessel-add" name="permission_box">
								<label for="vessel-add" class="font-14 font-300 default-cursor">Vessel Management</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor permission-box" data-id="8" id="consignee-add" name="permission_box">
								<label for="consignee-add" class="font-14 font-300 default-cursor">Consignee Management</label>
							</div>
						</div>

						<div class="add-content3 display-inline-top margin-left-10">
							<div class="margin-top-35">
								<input type="checkbox" class="default-cursor permission-box" data-id="9" id="team-add" name="permission_box">
								<label for="team-add" class="font-14 font-300 default-cursor">Team Management</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor permission-box" data-id="10" id="system-add" name="permission_box">
								<label for="system-add" class="font-14 font-300 default-cursor">System Settings</label>
							</div>
						</div>				
					</div>

				</div>
				<div class="modal-footer text-right margin-bottom-10">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="submit_add_user" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-15 padding-right-15">Add User</button>
				</div>
			</div>
		</div>
		<!-- ================================================== -->
		<!-- ===============END ADD USER MODAL================= -->
		<!-- ================================================== -->


		<!-- ================================================== -->
		<!-- ================EDIT USER MODAL=================== -->
		<!-- ================================================== -->
		<div class="modal-container margin-top-30 edit-user-modal" modal-id="edit-user">
			<div class="modal-body large">				

				<div class="modal-head">
					<h4 class="text-left">Edit User</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content">
					<p class="font-14 font-400 light-red-color text-right">Fields with asterisk "*" are required</p>
					<div class="default-cursor text-center half-border-radius overflow-hide width-130px  margin-auto position-rel change-img">
						<img src="../assets/images/profile/sarah.jpg" alt="Profile Picture" class="height-130px hover-edit-imghere">
						<div class="hover-edit-img">
							<i class="fa fa-camera fa-2x white-color margin-top-40"></i>
							<p class="white-color">Change Photo</p>
						</div>
					</div>

					
					<!-- <div class="image text-center upload-user margin-auto" style="font-size: 0;
															    height: 130px;
															    width: 130px;
															    text-align: center;
															    margin: 5px;">
						<i class="camera fa fa-camera margin-top-35 default-cursor"></i>
						<p class="gray-color font-14 font-400">Upload Photo</p>
					</div> -->

					<table class="width-100per margin-top-10">
						<tbody>
							<tr>
								<td class="font-bold font-14 black-color width-100px">First Name: <span class="light-red-color font-14">*</span></td>
								<td class="width-100px"><input id="edit_fname" type="text" class="width-200px no-margin-all"></td>
								<td class="font-bold font-14 black-color width-80px">User Name: <span class="light-red-color font-14">*</span></td>
								<td><input id="edit_username" type="text" class="width-200px no-margin-all"></td>

							</tr>
							<tr>
								<td class="font-bold font-14 black-color width-80px">Last Name:</td>
								<td class="width-100px"><input type="text" id="edit_lname" class="width-200px no-margin-all"></td>
								<td class="font-bold font-14 black-color width-80px">Password: <span class="light-red-color font-14">*</span></td>
								<td><input type="password" id="edit_password" class="width-200px no-margin-all" placeholder="************"></td>
							</tr>
							<tr>
								<td class="font-bold font-14 black-color width-80px">User Role: <span class="light-red-color font-14">*</span></td>
								<td class="">
									<div class="edit-user-role select medium">
										<select id="edit_user_role">
											<option value="role1">Admin</option>
											<option value="role2">Super Visor</option>
											<option value="role4">Operator</option>
										</select>
									</div>
								</td>
								<td colspan="2" class="text-right padding-right-30 padding-bottom-25"><a href="#" class="show-password margin-left-10 font-14 font-300">Show Password</a><a href="#" class="hide-password margin-left-10 font-14 font-300 hidden">Hide Password</a></td>
							</tr>
						</tbody>
					</table>

					<div class="width-100per padding-all-10 bggray-middark  margin-top-10">
						<p class="font-500 font-14 padding-bottom-5 no-margin-all">System Permissions:</p>
						<!-- edit user  -->

						<div class="edit-content1 display-inline-mid width-200px" >
							<p class="font-300 font-14 no-margin-left margin-bottom-10 margin-top-15">Products Modules</p>
							<div>
								<input type="checkbox" class="default-cursor edit-user-permission" data-id="1" id="receiving-edit">
								<label for="receiving-edit" class="font-14 font-300  default-cursor">Receiving</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor edit-user-permission" data-id="2" id="withdrawal-edit">
								<label for="withdrawal-edit" class="font-14 font-300 default-cursor">Withdrawal</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor edit-user-permission" data-id="3" id="transfer-edit">
								<label for="transfer-edit" class="font-14 font-300 default-cursor">Transfer</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor edit-user-permission" data-id="4" id="product-edit">
								<label for="product-edit" class="font-14 font-300 default-cursor">Product Inventory</label>
							</div>
						</div>

						<div class="edit-content2 display-inline-mid width-300px margin-left-10">
							<p class="font-300 font-14 no-margin-left margin-bottom-10 margin-top-15">System Configuration Modules</p>
							<div>
								<input type="checkbox" class="default-cursor edit-user-permission" data-id="5" id="pro-manage-edit">
								<label for="pro-manage-edit" class="font-14 font-300 default-cursor">Product Management</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor edit-user-permission" data-id="6" id="storage-loc-edit">
								<label for="storage-loc-edit" class="font-14 font-300 default-cursor">Storage Location Management</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor edit-user-permission" data-id="7" id="vessel-edit">
								<label for="vessel-edit" class="font-14 font-300 default-cursor">Vessel Management</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor edit-user-permission" data-id="8" id="consignee-edit">
								<label for="consignee-edit" class="font-14 font-300 default-cursor">Consignee Management</label>
							</div>
						</div>

						<div class="edit-content3 display-inline-top">
							<div class="margin-top-35">
								<input type="checkbox" class="default-cursor edit-user-permission" data-id="9" id="team-edit">
								<label for="team-edit" class="font-14 font-300 default-cursor">Team Management</label>
							</div>
							<div class="margin-top-5">
								<input type="checkbox" class="default-cursor edit-user-permission" data-id="10" id="system-edit">
								<label for="system-edit" class="font-14 font-300 default-cursor">System Settings</label>
							</div>
						</div>				
					</div>
				</div>
				<div class="modal-footer text-right">
					<div class="f-left">
						<i class="btn-delete-user fa fa-trash-o gray-color font-20 margin-top-5 default-cursor"></i>
					</div>
					<div class="f-right width-200px">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<button type="button" class="font-12 btn-submit-edit-user btn btn-primary font-12 display-inline-mid padding-left-15 padding-right-15">Save Changes</button>
					</div>
				</div>
			</div>
		</div>
		<!-- ================================================== -->
		<!-- ===============END EDIT USER MODAL================ -->
		<!-- ================================================== -->

	
		<!-- ================================================== -->
		<!-- =============GENERATING VIEW MODAL================ -->
		<!-- ================================================== -->
		<div class="modal-container modal-generate-view" modal-id="generate-view">
		    <div class="modal-body small">
		        <div class="modal-head">
		            <h4 class="text-left">Message</h4>
		        </div>
		        <!-- content -->
		        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
		            <div class="padding-all-10 bggray-7cace5">
		                <p class="modal-message font-16 font-400 white-color">Generating view, please wait...</p>
		            </div>
		        </div>
		        <div class="modal-footer text-right" style="height: 50px;">
		        </div>
		    </div>
		</div>
		<!-- ================================================== -->
		<!-- ===========END GENERATING VIEW MODAL============== -->
		<!-- ================================================== -->


		<!-- ================================================== -->
		<!-- ================DELETE TEAM MODAL================= -->
		<!-- ================================================== -->
		<div class="modal-container delete-team-modal" modal-id="delete-team-modal">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Delete Team</h4>				
					<div class="modal-close close-me btn-cancel-edit-record"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<p class="font-14 font-400">Are you sure you want to delete <span class="team-name">1234567890</span>?</p>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="btn-confirm-delete-team font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">Confirm</button>
				</div>
			</div>	
		</div>
		<!-- ================================================== -->
		<!-- =============END DELETE TEAM MODAL================ -->
		<!-- ================================================== -->

		<!-- ================================================== -->
		<!-- ================DELETE USER MODAL================= -->
		<!-- ================================================== -->
		<div class="modal-container delete-user-modal" modal-id="delete-user-modal">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Delete User</h4>				
					<div class="modal-close close-me btn-cancel-edit-record"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<p class="font-14 font-400">Are you sure you want to delete <span class="user-name">1234567890</span>'s account?</p>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="btn-confirm-delete-user font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">Confirm</button>
				</div>
			</div>	
		</div>
		<!-- ================================================== -->
		<!-- =============END DELETE USER MODAL================ -->
		<!-- ================================================== -->