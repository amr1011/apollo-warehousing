<div class="main ">					
		<div class="semi-main margin-bottom-50 padding-top-30">
			<div class="margin-top-30 text-right">
				<div class="mod-select ">			
					<div class="select f-left width-250px ">
						<select>
							<option value="Displaying All Teams"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>						
				</div>

				<button type="button" class="btn general-btn modal-trigger display-inline-mid padding-left-40 padding-right-40 " modal-target="add-team">Add Team</button>
				<button type="button" class="btn general-btn search-button display-inline-mid margin-left-10  dropdown-btn">View Filters/Search</button>
			</div>

			<div class="dropdown-search">
				<div class="triangle">
					<i class="fa fa-caret-up"></i>
				</div>
				<table>
					<tbody>
						<tr>
							<td>Search</td>
							<td>
								<div class="select">
									<select>
										<option value="items">Items</option>
									</select>
								</div>
							</td>
							<td>
								<input type="text" class="t-small width-100per" />
							</td>
						</tr>
						<tr>
							<td>Filter By:</td>
							<td>
								<div class="select">
									<select>
										<option value="calender">Calendar Here</option>
									</select>
								</div>
							</td>
							<td class="search-action">
								<div class="f-left">
									<div class="select width-150px">
										<select>
											<option value="origin">Origin</option>
										</select>
									</div>
									<div class="select width-150px">
										<select>
											<option value="status">Status</option>
										</select>
									</div>
								</div>
								<div class="f-right margin-left-30">
									<p class="display-inline-mid">Sort By:</p>
									<div class="select width-150px display-inline-mid">
										<select>
											<option value="withdrawal No.">Withdrawal No.</option>
										</select>
									</div>
									<div class="select width-150px display-inline-mid">
										<select>
											<option value="ascending">Ascending</option>
											<option value="descending">Descending</option>
										</select>
									</div>
								</div>
								<div class="clear"></div>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="f-left margin-top-10 margin-left-10">
					<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
					<p class="display-inline-mid font-15 font-bold">Save Search Result</p>
				</div>
				<button type="button" class="btn general-btn font-bold f-right margin-top-10 margin-right-30">Search</button>
				<div class="clear"></div>

			</div>
			<!-- big rounded images -->
			<div class="padding-all-20 table-view bggray-white border-blue border-top margin-top-30 box-shadow-light team-list">

				<div class="bdg-nolink width-20per margin-left-10 text-center position-rel team-template" style="display:none;">
					<div class="hover-me">
						<!-- <a href="<?php //echo BASEURL;?>/team_management/view_team" class="position-rel"> -->
							<img alt="big-user-head" class="border-radius-half default-cursor height-130px width-130px">
							<div class="team-mngt-cover"></div>
						<!-- </a> -->
					
						<p class="font-15 font-500 black-color team-name">Cargo Team</p>
						<p class="font-14 font-400 black-color no-padding-top team-number-of-members">4 Members</p>
					</div>
				</div>

				<!-- <div class="bdg-nolink width-20per position-rel text-center " style="display:none;">
					<div class="hover-me">
						<a href="<?php echo BASEURL;?>/team_management/view_team" class="position-rel">
							<img src="<?php echo BASEURL;?>assets/images/profile/profile_h.png" alt="big-user-head" class="default-cursor border-radius-half height-130px width-130px hover-me" >
							<div class="team-mngt-cover"></div>
						</a>
						
						<p class="font-15 font-500 black-color">Handler Team</p>
						<p class="font-14 font-400 black-color no-padding-top ">4 Members</p>
					</div>					
				</div>	

				<div class="bdg-nolink width-20per text-center position-rel team-template" style="display:none;">
					<div class="hover-me">
						<a href="<?php echo BASEURL;?>/team_management/view_team" class="position-rel">
							<img src="<?php echo BASEURL;?>assets/images/profile/profile_i.png" alt="big-user-head" class="default-cursor border-radius-half height-130px width-130px hover-me">
							<div class="team-mngt-cover"></div>
						</a>
												
						<p class="font-15 font-500 black-color">IT Team</p>
						<p class="font-14 font-400 black-color no-padding-top ">4 Members</p>
					</div>
				</div> -->

				<!-- <div class="bdg-nolink width-20per text-center position-rel" style="display:none;">
					<div class="hover-me">
						<a href="<?php echo BASEURL;?>/team_management/view_team" class="position-rel">
							<img src="<?php echo BASEURL;?>assets/images/profile/profile_n.png" alt="big-user-head" class="default-cursor border-radius-half height-130px width-130px hover-me">
							<div class="team-mngt-cover"></div>
						</a>
											
						<p class="font-15 font-500 black-color">Non-Operations</p>
						<p class="font-14 font-400 black-color no-padding-top ">4 Members</p>
					</div>
				</div>

				<div class="bdg-nolink width-20per text-center position-rel" style="display:none;">
					<div class="hover-me">
						<a href="<?php echo BASEURL;?>/team_management/view_team" class="position-rel">
							<img src="<?php echo BASEURL;?>assets/images/profile/profile_o.png" alt="big-user-head" class="default-cursor border-radius-half height-130px width-130px hover-me">
							<div class="team-mngt-cover"></div>
						</a>
											
						<p class="font-15 font-500 black-color">Operations</p>
						<p class="font-14 font-400 black-color no-padding-top ">4 Members</p>
					</div>
				</div> -->

			</div>
			
			
		</div> <!-- for center -->		
	</div>	<!-- for main -->			
</section>


<div class="modal-container add-team-modal" modal-id="add-team">
	<div class="modal-body small">				

		<div class="modal-head">
			<h4 class="text-left">Add Team</h4>				
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content text-left">	
			<p class="display-inline-mid no-margin-all font-400 padding-right-20 font-15">Team Name </p>
			<input id="team_name" type="text" class="display-inline midt-small width-300px" placeholder="Team Name">
		</div>
		<div class="modal-footer text-right margin-bottom-10">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
			<button id="submit_add_team" type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Add Team</button>
		</div>
	</div>	
</div>

<!-- ================================================== -->
<!-- =============GENERATING VIEW MODAL================ -->
<!-- ================================================== -->
<div class="modal-container modal-generate-view" modal-id="generate-view">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="modal-message font-16 font-400 white-color">Generating view, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>
<!-- ================================================== -->
<!-- ===========END GENERATING VIEW MODAL============== -->
<!-- ================================================== -->