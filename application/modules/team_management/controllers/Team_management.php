<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team_management extends CI_Controller {

	public function index()
	{
		$data["title"] = "Team Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav", $data);
		$this->load->view("team_management_view", $data);
		$this->load->view("includes/footer");
	}

	public function view_team()
	{
		$data["title"] = "Team Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav", $data);
		$this->load->view("view_team", $data);
		$this->load->view("includes/footer");
	}

	public function upload() 
	{
		$response = array();
		$arr_response = array();

		$status = false;
		$message = "Fail";
		if(isset($_FILES["attachment"])){
			$name = $_FILES["attachment"]["name"];
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 20; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    
		    $file_name = $randomString . "." .$ext;

		    move_uploaded_file($_FILES["attachment"]["tmp_name"], UPLOAD_IMAGE_PATH.$file_name);

		    $arr_response["path"] = UPLOAD_IMAGE_PATH.$file_name;
		    $arr_response["location"] = BASEURL . "uploads/images/" . $file_name;
		    $status = true;
		    $message = "Succes";
		}

		$response["status"] = $status;
		$response["message"] = $message;
		$response["data"] = $arr_response;
		echo json_encode($response);
	}

}

/* End of file Team_management.php */
/* Location: ./application/modules/team_management/controllers/Team_management.php */
