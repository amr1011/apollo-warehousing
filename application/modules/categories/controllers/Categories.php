<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MX_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		$this->load->model('Categories_model');

		$response = array(
			"status" => false,
			"message"=>"",
			"data"=>array()
			);
	}

	public function index()
	{
				
	}

	public function add()
	{
		header("Content-Type: application/json");
		
		$company_id = trim($this->input->post('company_id'));
		$category_name = trim($this->input->post('category_name'));
		$arr_category_type = json_decode($this->input->post('category_type'), true);

		$this->form_validation->set_rules('category_name', 'Category Name', 'trim|required');

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
		else
		{
			$get_id = $this->Categories_model->add($category_name, $company_id);
				
			foreach ($arr_category_type as $key => $value) {

				if($arr_category_type[$key]["value"] == 1)
				{
					$type_data = array(
					"category_id" => $get_id,
					"category_type_id" => $arr_category_type[$key]["categoryId"]
					);
					$this->Categories_model->add_category_type($type_data);
				}

			}

			$response["status"] = true;
			$response["message"] = "Successfully Added";
			$response["data"] = array("category_id"=>$get_id);
			echo json_encode($response);

		}

	}

	public function edit()
	{
	
		$qualifiers = json_decode($this->input->post('qualifiers'), true);
		$data = json_decode($this->input->post('data'), true);

		// print_r($data);
		// exit();
	
		$this->form_validation->set_rules("qualifiers", "Qualifiers", "callback_validation_for_edit_qualifiers");
		$this->form_validation->set_rules("data", "Data", "callback_validation_for_edit_data");

		$query = array();
		$arr_set_data = array();
		$arr_set_catType = array();

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
		else
		{
			
			$counter = 0;

			if( count($qualifiers) > 0 ){
				foreach ($qualifiers as $key => $value) {
					$data_val = $qualifiers[$key]["value"];
					$new_val = "";

					if(gettype($data_val) == "string")
					{
						$new_val = "'".$data_val."'";
					}
					else if(gettype($data_val) == "double")
					{
						$new_val = doubleval($data_val);
					}
					else if(gettype($data_val) == "integer")
					{
						$new_val = intval($data_val);
					}


					if($counter == 0)
					{
						$query[$qualifiers[$key]["field"]] = $new_val;
					}
					else
					{
						$query[$qualifiers[$key]["field"]] = $new_val;
						//$query .= ' AND '.$qualifiers[$key]["field"]. $qualifiers[$key]["operation"].$new_val;
					}

					$counter++;
				}
			}

			if($data !== null AND json_last_error() === JSON_ERROR_NONE)
			{ 
				foreach ($data as $key => $value) {

					if(isset($data[$key]["category_name"])){
						$arr_set_data["name"] = $data[$key]["category_name"];
					}
					if(isset($data[$key]["company_id"])){
						$arr_set_data["company_id"] = $data[$key]["company_id"];
					}
					if(isset($data[$key]["is_deleted"])){
						$arr_set_data["is_deleted"] = $data[$key]["is_deleted"];
					}
					if(isset($data[$key]["category_id"])){
						//$arr_set_data["category_id"] = $data[$key]["category_id"];

						$this->Categories_model->delete_category_type($data[$key]["category_id"]);
					}

					if(isset($data[$key]["category_type"]) AND (count($data[$key]["category_type"]) != 0) )
					{
						
						foreach ($data[$key]["category_type"] as $typeKeys => $typeValues) {
						
							$categoryId = $data[$key]["category_id"];
							$catTypeId = $data[$key]["category_type"][$typeKeys]["categoryId"];
							$catTypeValue = $data[$key]["category_type"][$typeKeys]["value"];

								if($catTypeValue == 1){
								
									$this->Categories_model->edit_category_type($categoryId, $catTypeId);
								}
							
						}
					}
					
				}

				
			}

			$showQuery = $this->Categories_model->edit($arr_set_data, $query);
			
			$response["status"] = true;
			$response["message"] = "Successfully Updated";
			$response["data"] = array("affected_rows"=>$showQuery);
			echo json_encode($response);
		}

	}

	
	public function validation_for_edit_qualifiers()
	{
		$arr_qualifiers = json_decode($this->input->post("qualifiers"), true);

		if($arr_qualifiers !== null AND json_last_error() === JSON_ERROR_NONE)
		{ 
			foreach ($arr_qualifiers as $key => $value) {
				
				if(
					(
						($arr_qualifiers[$key]["field"] != "") AND 
						($arr_qualifiers[$key]["operation"] != "") AND 
						($arr_qualifiers[$key]["value"] != "")
					) === false
				)
				{
					$this->form_validation->set_message("validation_for_edit_qualifiers", "Qualifiers is invalid");
					return false;
				}
				
			}
		}
	}

	public function validation_for_edit_data()
	{
		$arr_data = json_decode($this->input->post("data"), true);

		if($arr_data !== null AND json_last_error() === JSON_ERROR_NONE)
		{ 
			foreach ($arr_data as $key => $value) {

				// print_r($arr_data[$key]["name"]);
				// exit();

				if(isset($arr_data[$key]["category_name"]) AND trim($arr_data[$key]["category_name"]) == "" )
				{
					$this->form_validation->set_message("validation_for_edit_data", "Category Name is Required");
					return false;
				}
				if(isset($arr_data[$key]["company_id"]) AND trim($arr_data[$key]["company_id"]) == "" )
				{
					$this->form_validation->set_message("validation_for_edit_data", "Company ID is Required");
					return false;
				}

				if(isset($arr_data[$key]["is_deleted"]) AND trim($arr_data[$key]["is_deleted"]) == "" )
				{
					$this->form_validation->set_message("validation_for_edit_data", "Data is invalid");
					return false;
				}

				if(isset($arr_data[$key]["category_type"]) AND (count($arr_data[$key]["category_type"]) == 0) )
				{
					$this->form_validation->set_message("validation_for_edit_data", "Category Type is invalid");
					return false;
				}
				

				// if(
				// 	(
				// 		(isset($arr_data[$key]["name"])) AND
				// 		(isset($arr_data[$key]["company_id"])) AND
				// 		(isset($arr_data[$key]["is_deleted"]))
				// 	) === false
				// )
				// {
					
				// }
				
			}

		}
		else
		{
			$this->form_validation->set_message("validation_for_edit_data", "Data is invalid");
			return false;
		}
	}

	public function archive()
	{
		

		$category_id = $this->input->post('category_id');

		$this->form_validation->set_rules("category_id", "Category ID", "trim|required|integer|callback_is_archive_exist");
		

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
		else
		{
			$result = $this->Categories_model->archive($category_id);
			$response["status"] = true;
			$response["message"] = "Successfully Deleted";
			$response["data"] = array("affected_rows"=>$result);
			echo json_encode($response);
		}
	}

	public function is_archive_exist()
	{
		$category_id = $this->input->post('category_id');

		$check_category = $this->Categories_model->check_category($category_id);

		if($check_category == 0)
		{
			$this->form_validation->set_message("is_archive_exist", "Category ID not found");
			return false;
		}
	}


	public function get()
	{	
		$company_id = trim($this->input->post('company_id'));
		$id = trim($this->input->post('id'));
		$name = trim($this->input->post('name'));

		// $this->form_validation->set_rules("company_id", "Company Id", "trim|callback_get_company_id_category_validation");
		$this->form_validation->set_rules("company_id", "Company Id", "trim");
		$this->form_validation->set_rules("id", "Category ID", "trim|integer|callback_get_id_category_validation");
		$this->form_validation->set_rules("name", "Category Name", "trim|alpha_numeric");
		// $this->form_validation->set_rules("name", "Category Name", "trim|alpha_numeric|callback_get_name_category_validation");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
			echo validation_errors();
		}
		else
		{
			if($company_id != "" AND $id == "" AND $name == "" )
			{
				$company_id = "WHERE c.company_id='".$company_id."'";
				$id ="";
				$name = "";
			}
			else if($name != "" AND $id == "" AND $company_id == "")
			{
				$id ="";
				$company_id = "";
				$name = "WHERE c.name='".$name."'";
			}
			else if($name == "" AND $id != "" AND $company_id == "")
			{
				$id ="WHERE c.name='".$name."'";
				$company_id = "";
				$name = "";
			}
			else if($id != "" AND $name != "" AND $company_id != "")
			{
				$name = "WHERE c.company_id='".$company_id."'";
				$name = "AND  c.name='".$name."'";
				$id ="AND c.id=".$id;
			}
			else
			{
				$name = "";
				$id = "";
				$company_id = "";
			}

			 $result = $this->Categories_model->get($company_id, $id, $name);

			 // print_r($result);
			 // exit();

			 foreach ($result as $key => $value) {
			 	unset($result[$key]["category_type"]);
			 	$result[$key]["category_types"] = $this->Categories_model->get_category_type_map($value["category_id"]);
			 }
			
			header("Content-Type: application/json");
			$response["status"] = true;
			$response["message"] = "Successfully get Data";
			$response["data"] = $result;
			echo json_encode($response);

			

		}

	}

	public function get_company_id_category_validation()
	{
		$company_id = trim($this->input->post('company_id'));

		if($company_id != "")
		{
			$check_company_id = $this->Categories_model->check_category_company_id($company_id);

			if($check_company_id == 0)
			{
				$this->form_validation->set_message("get_company_id_category_validation", "No Result");
				return false;
			}
			else
			{
				return true;
			}
		}
	}

	public function get_id_category_validation()
	{
		$id = trim($this->input->post('id'));

		if($id != "")
		{
			$check_id = $this->Categories_model->check_category($id);

			if($check_id == 0)
			{
				$this->form_validation->set_message("get_id_category_validation", "No Result");
				return false;
			}
			else
			{
				return true;
			}
		}
	}

	public function get_name_category_validation()
	{
		$name = trim($this->input->post('name'));

		if($name != "")
		{
			$check_name = $this->Categories_model->check_category_name($name);

			if($check_name == 0)
			{
				$this->form_validation->set_message("get_name_category_validation", "No Result");
				return false;
			}
		}
	}

	public function get_category_type()
	{
		$result = $this->Categories_model->get_category_type();

		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Successfully get Data";
		$response["data"] = $result;
		echo json_encode($response);
 

	}



	

	
	






}

/* End of file Categories.php */
/* Location: ./application/modules/categories/controllers/Categories.php */