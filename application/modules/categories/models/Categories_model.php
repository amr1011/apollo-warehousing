<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_model extends CI_Model {

	public function __contruct()
	{
		parent:: __contruct();
	}

	public function add($category_name, $company_id)
	{
		$this->db->insert("category", array("name"=>$category_name, "company_id"=>$company_id));
		return $this->db->insert_id();
	}

	public function add_category_type($data){
		$this->db->insert("category_type_map", $data);
		return $this->db->insert_id();
	}

	public function edit($data, $query)
	{
		// $sql = "UPDATE category SET '".$data."' WHERE '".$query."'"; 
		// print_r($query);
		// exit();
		$this->db->where($query);
		$this->db->update("category", $data);
		return $this->db->affected_rows();
	}
	public function delete_category_type($category_id)
	{
		$this->db->where('category_id', $category_id);
		$this->db->delete("category_type_map");
	}

	public function edit_category_type($categoryId, $catTypeId)
	{
		$data = array(
			"category_id"=>$categoryId,
			"category_type_id"=>$catTypeId
			);
		$this->db->insert("category_type_map", $data);

	}

	public function archive($category_id)
	{
		$this->db->where('id', $category_id);
		$this->db->update("category", array("is_deleted"=>1));
		return $this->db->affected_rows();
	}


	public function get($company_id="", $id="", $name="")
	{
		/*$sql = "SELECT c.id as category_id, c.name as category_name, ct.name as category_type FROM category_type_map ctm
				LEFT JOIN category c ON  c.id = ctm.category_id
				LEFT JOIN category_type ct ON ct.id = ctm.category_type_id
				$company_id $name $id AND c.is_deleted<>1 GROUP BY category_id";*/

		$sql = "SELECT c.id as category_id, c.name as category_name FROM category c
				$company_id $name $id AND c.is_deleted<>1 GROUP BY c.id";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function check_category_company_id($company_id)
	{
		$sql = "SELECT company_id FROM category WHERE company_id='{$company_id}'";
		$result = $this->db->query($sql);
		return $result->num_rows();

	}
	
	public function check_category($category_id)
	{
		$sql = "SELECT id FROM category WHERE id='{$category_id}'";
		$result = $this->db->query($sql);
		return $result->num_rows();

	}

	public function check_category_name($name)
	{
		$sql = "SELECT name FROM category WHERE name='{$name}'";
		$result = $this->db->query($sql);
		return $result->num_rows();

	}

	public function get_category_type()
	{
		$sql = "SELECT * FROM category_type";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_category_type_map($id){
		$sql = "SELECT ct.id as id, ct.name as name FROM category_type_map ctm LEFT JOIN category_type ct ON ctm.category_type_id = ct.id WHERE ctm.category_id = {$id} ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

}

/* End of file Categories_model.php */
/* Location: ./application/modules/categories/models/Categories_model.php */