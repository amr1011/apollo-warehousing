
		<div class="main">
			<div class="semi-main">
				<!--upper content-->
				<div class="upper-content width-100per padding-top-60 ">
					<!--upper left content-->
					<div class="product-categories display-inline-top width-48per margin-right-20">
						<h3 class="text-left">Item Categories</h3>

						<div class="border-top border-blue margin-top-20 box-shadow-dark padding-bottom-20 bggray-white font-400 limit-container-box">

							<div class="padding-right-20 margin-top-10 margin-bottom-10 text-right">
								<a href="javascript:void(0)" id="addProductlink" class="modal-trigger default-cursor font-12" modal-target="add-new-category">+ Add Item Category</a>
							</div>

							<div class="product-table width-95per margin-auto" >
								<table class="tbl-4c3h">
									<thead>
										<th class="black-color">Category Name</th>
										<th class="black-color">Category Type</th>
									</thead>
								</table>
								<div id="displayAllCategories">
									
								</div>
								<!-- <div class="table-content position-rel">
									<div class="content-show height-auto width-100per padding-top-20 padding-bottom-20 tbl-dark-color">
										<div class="text-center width-50per display-inline-mid">
											<p>Grains</p>
										</div>
										<div class="text-center width-50per display-inline-mid">
											<p>Solid</p>
										</div>
									</div>
									<div class="content-hide">
										<button class="btn general-btn modal-trigger" modal-target="cccc">Edit Product Category</button>
									</div>
								</div>

								<div class="table-content position-rel">
									<div class="content-show height-auto width-100per padding-top-20 padding-bottom-20">
										<div class="text-center width-50per display-inline-mid">
											<p>Oil</p>
										</div>
										<div class="text-center width-50per display-inline-mid">
											<p>Liquid</p>
										</div>
									</div>
									<div class="content-hide">
										<button class="btn general-btn modal-trigger" modal-target="edit-product-category">Edit Product Category</button>
									</div>
								</div>

								<div class="table-content position-rel">
									<div class="content-show height-auto width-100per padding-top-20 padding-bottom-20 tbl-dark-color ">
										<div class="text-center width-50per display-inline-mid">
											<p>Noble Gas</p>
										</div>
										<div class="text-center width-50per display-inline-mid">
											<p>Gas</p>
										</div>
									</div>
									<div class="content-hide">
										<button class="btn general-btn modal-trigger" modal-target="edit-product-category">Edit Product Category</button>
									</div>
								</div> -->
							</div>
						</div>
					</div>

					<!--end of upper left content-->

					<!--right upper content-->
					<div class="hierarchy-categories display-inline-top width-48per">
						<h3 class="text-left">Storage Hierarchy</h3>
						<div class="border-top border-blue margin-top-20 box-shadow-dark padding-bottom-20 bggray-white font-400 limit-container-box">
							<div class="padding-right-20 margin-top-10 margin-bottom-10 text-right">
								<a href="#" class="modal-trigger" modal-target="add-storage-hierarchy">+ Add Storage Hierarchy</a>
							</div>

							<div class="hierarchy-table width-95per margin-auto">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Hierarchy Name</th>
											<th class="black-color">Hierarchy Items</th>
										</tr>
									</thead>
								</table>

							</div>
						</div>
					<!--end of upper right content-->

				
					</div>
				</div>
			<!--end of upper content-->


			<!--lower content-->
				<div class="lower-content width-100per padding-top-20 ">
					<!--upper left content-->
					<div class="loading-areas-categories display-inline-top width-48per margin-right-20">
						<h3 class="text-left">Loading Area</h3>
						<div class="border-top border-blue margin-top-20 box-shadow-dark padding-bottom-20 bggray-white font-400 limit-container-box">
							<div class="padding-right-20 margin-top-10 margin-bottom-10 text-right">
								<a href="#" class="modal-trigger" modal-target="add-loading-area">+ Add Loading Area</a>
								

							</div>

							<div class="loading-areas-table width-95per margin-auto" id=loading-area>
								<table class="tbl-1c1h">
									<thead>
										<th class="black-color font-bold">Area Name</th>
									</thead>
								</table>

								<div id="displayAreaName">
									
								</div>

								<!--<div class="table-content position-rel">
									<div class="content-show height-auto width-100per padding-top-20 padding-bottom-20 tbl-dark-color">
										<div class="text-center width-100per display-inline-mid">
											<p>Auto Bulk 1</p>
										</div>
									</div>
									<div class="content-hide">
										<button class="btn general-btn modal-trigger" modal-target="edit-loading-area">Edit Loading Area</button>
									</div>
								</div>-->

							</div>
						</div>
					</div>

					<!--end of lower left content-->

					<!--right lower content-->
					<div class="loading-type-categories display-inline-top width-48per">
						<h3 class="text-left">Loading Type</h3>
						<div class="border-top border-blue margin-top-20 box-shadow-dark padding-bottom-20 bggray-white font-400 limit-container-box">
							<div class="padding-right-20 margin-top-10 margin-bottom-10 text-right">
								<a href="#" class="modal-trigger" modal-target="add-loading-type">+ Add Loading Type</a>
							</div>

							<div class="loading-type-table width-95per margin-auto">
								<table class="tbl-1c1h">
									<thead>
										<th class="black-color font-bold">Type Name</th>
									</thead>
								</table>

								<div id="displayLoadingType">
									
								</div>
								<!--<div class="table-content position-rel">
									<div class="content-show height-auto width-100per padding-top-20 padding-bottom-20 tbl-dark-color">
										<div class="text-center width-100per display-inline-mid">
											<p>Auto-Bulk Machine</p>
										</div>
										
									</div>
									<div class="content-hide">
										<button class="btn general-btn modal-trigger" modal-target="edit-loading-type">Edit Loading Type</button>
									</div>
								</div>-->
							</div>
						</div>
					<!--end of lower right content-->
				</div>
			</div>
			<!--end of lower content-->
			</div>
		</div>

		<!--start modal category -->
		<div class="modal-container" modal-id="add-new-category">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Add New Category</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<form id="formCategory">
						<p class="display-inline-mid no-margin-all font-300 font-14">Category Name :</p>
						<input type="text" datavalid="required" labelinput="Category Name" class="display-inline-mid width-280px" id="categoryName">

						<div class="width-100per  padding-top-20">
							<p class="display-inline-bottom no-margin-all font-14 display-inline-top font-300">Category Type :</p>

							<div class="display-inline-mid padding-left-15" id="modalAddNewCategoryTypeContainer">
								<div class="padding-bottom-10 ">
									<input type="checkbox" name="category_type[]" value="1"  id="solid" class="default-cursor">
									<label for ="solid" class="default-cursor font-300 font-14" >Solid</label>
								</div>
								<div class="padding-bottom-10 default-cursor">
									<input type="checkbox" name="category_type[]" value="2" class="default-cursor" id="liquid">
									<label for ="liquid" class="default-cursor font-300 font-14" >Liquid</label>
								</div>
								<div class="padding-bottom-10 default-cursor">
									<input type="checkbox" name="category_type[]" value="3" class="default-cursor" id="gas">
									<label for ="gas" class="default-cursor font-300 font-14" >Gas</label>
								</div>
							</div>

						</div>
					</form>	
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="saveCategory" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Add Category</button>
				</div>
			</div>	
		</div>
		<!--end of modal category-->

		<!--start of modal edit category-->
		<div class="modal-container" modal-id="edit-product-category">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Edit Category</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<form id="updateformCategory">	
						<p class="display-inline-mid no-margin-all font-400 font-15">Category Name :</p>
						<input type="text" datavalid="required" labelinput="Category Name" id="editCategoryNameInput" class="display-inline-mid width-280px">

						<div class="width-100per  padding-top-20">
							<p class="display-inline-bottom no-margin-all font-15 display-inline-top font-400">Category Type :</p>

							<div class="display-inline-mid padding-left-15" id="modalUpdateNewCategoryTypeContainer">
								<div class="padding-bottom-10 ">
									<input type="checkbox"  id="edit-solid" class="default-cursor">
									<label for ="edit-solid" class="default-cursor font-400 font-14"  >Solid</label>
								</div>
								<div class="padding-bottom-10 default-cursor">
									<input type="checkbox" class="default-cursor" id="edit-liquid">
									<label for ="edit-liquid" class="default-cursor font-400 font-14" >Liquid</label>
								</div>
								<div class="padding-bottom-10 default-cursor">
									<input type="checkbox" class="default-cursor" id="edit-gas">
									<label for ="edit-gas" class="default-cursor font-400 font-14" >Gas</label>
								</div>
							</div>

						</div>
					</form>
				</div>
			
				<div class="modal-footer">
					<div class="f-left icon-hover" id="deleteCategory">
						<i class="fa fa-trash-o font-22 text-left default-cursor gray-color"></i>
					</div>
					<div class="f-right width-250px">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<button type="button" id="editCategory" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Save Changes</button>
					</div>
					<div class="clear"></div>
				</div>
			</div>	
		</div>

		<!--end of modal edit category-->


		<!--start of modal add loading area-->
		<div class="modal-container" modal-id="add-loading-area">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Add Loading Area</h4>				
					<div class="modal-close close-me"></div>
				</div>
				<form id="AddLoadingAreaForm">
				<!-- content -->
				<div class="modal-content text-left">	
					<p class="display-inline-mid no-margin-all font-300 padding-right-20 font-14">Area Name:</p>
					<input type="text" id="areaName" class="display-inline midt-small width-300px" name="name" datavalid="required">
				</div>
				</form>
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-15 padding-right-15" id="addLoadingAreaBtn">Add Loading Area
					</button>
				</div>
			</div>	
		</div>
		<!--end of modal add loading area-->

		<!-- start of modal edit loading area-->
		<div class="modal-container" modal-id="edit-loading-area">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Edit Loading Area</h4>				
					<div class="modal-close close-me"></div>
				</div>
				<form id="EditLoadingAreaForm">
				<!-- content -->
				<div class="modal-content text-left">	
					<p class="display-inline-mid no-margin-all font-300 padding-right-20 font-14">Area Name:</p>
					<input type="text" class="display-inline midt-small width-300px" name="name" datavalid="required" id="editAreaName">
				</div>
				</form>
				<div class="modal-footer">
					<div class="f-left icon-hover" id="deleteAreaName">
						<i class="fa fa-trash-o font-22 text-left default-cursor gray-color"></i>
					</div>
					<div class="f-right width-250px">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30"id="update-area-name">Save Changes</button>
					</div>
					<div class="clear"></div>
				</div>
			</div>	
		</div>
		<!--end of modal edit loading area-->

		<!--start of modal add loading type-->
		<div class="modal-container" modal-id="add-loading-type">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Add Loading Type</h4>				
					<div class="modal-close close-me"></div>
				</div>
				<form id="AddLoadingTypeForm">
				<!-- content -->
				<div class="modal-content text-left">	
					<p class="display-inline-mid no-margin-all font-300 padding-right-20 font-14">Type Name:</p>
					<input type="text" class="display-inline midt-small width-300px" name="name" datavalid="required" id="typeName">
				</div>
				</form>
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-15 padding-right-15" id="addLoadingTypeBtn">Add Loading Type</button>
				</div>
			</div>	
		</div>
		<!--end of modal add loading type-->

		<!--Start of modal edit loading type-->
		<div class="modal-container" modal-id="edit-loading-type">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Edit Loading Type</h4>				
					<div class="modal-close close-me"></div>
				</div>
				<form id="EditLoadingTypeForm">
				<!-- content -->
				<div class="modal-content text-left">	
					<p class="display-inline-mid no-margin-all font-300 padding-right-20 font-14">Type Name:</p>
					<input type="text" class="display-inline midt-small width-300px" value="Auto-Bulk Machine" id="editTypeName" datavalid="required">
				</div>
				</form>
				<div class="modal-footer">
					<div class="f-left icon-hover" id="deleteTypeName">
						<i class="fa fa-trash-o font-22 text-left default-cursor gray-color"></i>
					</div>
					<div class="f-right width-250px">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30" id="updateType">Save Changes</button>
					</div>
					<div class="clear"></div>
				</div>
			</div>	
		</div>

		<!--end of modal edit loading type-->

		<!--Start of modal Add Storage Hierarchy-->
		<div class="modal-container" modal-id="add-storage-hierarchy">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left" id="storage-hierarchy-title">Add Storage Hierarchy</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="width-100per">
						<div class="width-100per">
							<div class="display-inline-mid ">
								<p class="no-margin-all font-15 font-400"> Hierarchy Name:</p>
							</div>
							<div class="display-inline-mid">
								<input type="text" placeholder="Hierarchy Name Here..." id="hierarchyName" class="width-330px">
							</div>
						</div>
						<br>
						<div class="width-100per">
							<div class="display-inline-mid ">
								<p class="no-margin-all font-15 font-400"> Disable Hierarchy :</p>
							</div>
							<div class="display-inline-mid">
								<input type="checkbox" name="disablehierarchy" value="disablehierarchy" id="disablehierarchy">
							</div>
						</div>

						<div class="width-100per" id="hidehierarchy"  >
							<div class="display-inline-top width-120px padding-top-20">
								<p class="no-margin-all font-15 font-400 "> Levels:</p>
								<p class="no-margin-all padding-top-10 italic" style="display: none;">(Double-click the item name to edit. Click on the buttons on the right to add or remove locations)</p>
							</div>
							<div class="display-inline-top width-295px bggray-white margin-top-15 margin-left-5" id="hierarchyDisplay" >
								<!-- <p class="font-400 border-bottom-medium padding-bottom-10 padding-top-5 ">1.<input type="text" value="Level 1" class="width-200px remove-border-radius "></p> -->
							</div>
							<div class=" display-inline-mid padding-top-10" disabled="disabled" >
								<div class="hierarchy-add half-border-radius height-22px width-20px text-center add-color-green  margin-top-15 margin-left-10">
									<i class="fa fa-plus font-22 text-left default-cursor padding-top-2 gray-color" id="plusHierarchy"></i>
								</div>

								<div class="hierarchy-minus half-border-radius height-22px width-20px text-center border-red margin-top-5 margin-left-10">
									<i class="fa fa-minus font-22 text-left default-cursor padding-top-2 gray-color" id="subtractHierarchy"></i>
								</div>

								<!-- <div class="hierarchy-up half-border-radius height-22px width-20px  text-center margin-top-5 margin-left-10">
									<i class="fa fa-arrow-up font-22 text-left default-cursor padding-top-2 gray-color" id="moveupHierarchy"></i>
								</div>

								<div class="hierarchy-down half-border-radius height-22px width-20px  text-center  margin-top-5 margin-left-10">
									<i class="fa fa-arrow-down font-22 text-left default-cursor padding-top-2 gray-color" id="movedownHierarchy"></i>
								</div> -->
							</div>

						</div>

					</div>	
				</div>
			
				<div class="modal-footer text-right">
					<div class="f-left icon-hover" id="hierarchyTrashContainer" style="display:none;">
						<i class="fa fa-trash-o font-22 text-left default-cursor gray-color"></i>
					</div>
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" id="addHierarchy">Add Hierarchy</button>
				</div>
			</div>	
		</div>

		<!--End of Modal add Storage Hierarchy-->
