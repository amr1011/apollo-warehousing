<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function index()
	{

		$user_session = $this->session->userdata('apollo_user');
		if($user_session === null){
			redirect(BASEURL.'login');
		}else{
			$this->system_settings();
			
		}


	}

	public function system_settings()
	{
		$data["title"] = "System Settings";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("settings/system_settings_view", $data);
		$this->load->view("includes/footer.php");
	}

	

	public function getAllArea()
	{
		$this->load->model("LoadingArea_model");
		$vessels = array();

		$vessels = $this->LoadingArea_model->gel_all();

		foreach ($vessels as $key => $value) {

			$vessels[$key]["date_formatted"] = date("M. d, Y", strtotime($value["date_created"]));
		}

		$response = array(
			"status" => true,
			"message" => "",
			"data" => $vessels
		);

		header("Content-Type:application/json;");
		echo json_encode($response);

	}
	/*public function get_single_loading_area($id)
	{
		$sql = "SELECT * FROM loading_area WHERE  id = $id";
		$query = $this->db->query($sql);
		return $query->result();
		//var_dump($query->result());
	}*/

	public function addArea() 
	{
		
		$name = $this->input->post('name');

		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[160]|regex_match[/[A-Za-z0-9\'\"]+$/]');
		
		if($this->form_validation->run())
		{
			$this->load->model('LoadingArea_model');

			$loadingArea_data = array(
				'name' => $name
			);

			 $this->LoadingArea_model->add_loading_area($loadingArea_data);
			//  $this->LoadingType_model->add_loading_type($loadingArea_data);
		}
		else
		{
			$response["status"] = false;
			$response["message"] = "failed";
			$response["data"] = $this->form_validation->error_array();
			header("Content-Type:application/json;");
			echo json_encode($response);
		}
	}

	public function updateArea() {
		$rows_changed = 0;
		$loadingarea_id = $this->input->post('id');
		
		$name = $this->input->post('name');

		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[160]|regex_match[/[A-Za-z0-9\'\"]+$/]');
		
		if($this->form_validation->run())
		{
			$this->load->model('LoadingArea_model');

			$loadingarea_data = array(
				'name' => $name
			);

			 $this->LoadingArea_model->update($loadingarea_id,$loadingarea_data);
		}
		else
		{
			$response["status"] = false;
			$response["message"] = "failed";
			$response["data"] = $this->form_validation->error_array();
			header("Content-Type:application/json;");
			echo json_encode($response);
		}
	}

	public function deleteArea() {
		$rows_changed = 0;
		$area_id = $this->input->post('id');
		
		$this->load->model('LoadingArea_model');	
		echo $this->LoadingArea_model->delete($area_id);
	}



	public function getAllType()
	{
		$this->load->model("LoadingType_model");
		$vessels = array();

		$vessels = $this->LoadingType_model->gel_all();

		foreach ($vessels as $key => $value) {

			$vessels[$key]["date_formatted"] = date("M. d, Y", strtotime($value["date_created"]));
		}

		$response = array(
			"status" => true,
			"message" => "",
			"data" => $vessels
		);

		header("Content-Type:application/json;");
		echo json_encode($response);

	}
	

	public function addType() {
		
		$name = $this->input->post('name');

		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[160]|regex_match[/[A-Za-z0-9\'\"]+$/]');
		
		if($this->form_validation->run())
		{
			$this->load->model('LoadingType_model');

			$type_data = array(
				'name' => $name
			);

			 $this->LoadingType_model->add_loading_type($type_data);
			
		}
		else
		{
			$response["status"] = false;
			$response["message"] = "failed";
			$response["data"] = $this->form_validation->error_array();
			header("Content-Type:application/json;");
			echo json_encode($response);
		}
	}

	public function updateType() {
		$rows_changed = 0;
		$type_id = $this->input->post('id');
		
		$name = $this->input->post('name');

		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[160]|regex_match[/[A-Za-z0-9\'\"]+$/]');
		
		if($this->form_validation->run())
		{
			$this->load->model('LoadingType_model');

			$type_data = array(
				'name' => $name
			);

			 $this->LoadingType_model->update($type_id,$type_data);
		}
		else
		{
			$response["status"] = false;
			$response["message"] = "failed";
			$response["data"] = $this->form_validation->error_array();
			header("Content-Type:application/json;");
			echo json_encode($response);
		}
	}

	public function deleteType() {
		$rows_changed = 0;
		$type_id = $this->input->post('id');
		
		$this->load->model('LoadingType_model');	
		echo $this->LoadingType_model->delete($type_id);
	}

}

/* End of file Settings.php */
/* Location: ./application/modules/settings/controllers/Settings.php */