<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loadingarea_model extends CI_Model 
{	
	
	public function gel_all()
	{
		$sql = "SELECT * FROM loading_area WHERE is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function gel_loading_area($id)
	{
		$sql = "SELECT * FROM loading_area WHERE id = {$id} AND is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function add_loading_area($loadingArea_data) 
	{
		$this->db->insert('loading_area', $loadingArea_data);
		return $this->db->insert_id();
	}

	public function delete($area_id) {
		// echo $product_id;
		$delete_details = array(
			"is_deleted" => 1
		);
		$this->db->where("id", $area_id);
		$this->db->update("loading_area", $delete_details);
		// print_r($this->db->last_query());
		return $this->db->affected_rows();
	}

	public function update($loadingarea_id, $loadingarea_data) {
		$this->db->where("id", $loadingarea_id);
		$this->db->update("loading_area", $loadingarea_data);
		return $this->db->affected_rows();
	}

}