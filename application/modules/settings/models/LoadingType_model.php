<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loadingtype_model extends CI_Model 
{	
	
	public function gel_all()
	{
		$sql = "SELECT * FROM loading_type WHERE is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function add_loading_type($type_data) 
	{
		$this->db->insert('loading_type', $type_data);
		return $this->db->insert_id();
	}


	

	public function delete($type_id) {
		// echo $product_id;
		$delete_details = array(
			"is_deleted" => 1
		);
		$this->db->where("id", $type_id);
		$this->db->update("loading_type", $delete_details);
		
		return $this->db->affected_rows();
	}

	public function update($type_id, $type_data) {
		$this->db->where("id", $type_id);
		$this->db->update("loading_type", $type_data);
		return $this->db->affected_rows();
	}

}