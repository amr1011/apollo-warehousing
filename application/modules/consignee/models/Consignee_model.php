<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consignee_model extends CI_Model {

	public function __construct()
	{
		parent:: __construct();
	}

	public function add_consignee($data)
	{

		$this->db->insert("consignee", $data);
		return $con_id = $this->db->insert_id();


	}

	public function add_contact_person($data)
	{
		$this->db->insert("contact_person", $data);
		return $this->db->insert_id();
	}

	public function	update_consignee_information($id, $data)
	{
		$this->db->where("id", $id);
		$this->db->update("consignee", $data);
		return $this->db->affected_rows();
	}

	public function get_all_consignee()
	{
		// $sql = "SELECT  *, cp.name as contact_name, cp.id as contact_id FROM consignee c
		// 		LEFT JOIN contact_person cp ON cp.consignee_id = c.id";
		$sql = "SELECT * FROM consignee ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_consignee_contact_person($id)
	{
		$sql = "SELECT * FROM contact_person WHERE consignee_id={$id}";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_selected_consignee($consign_id)
	{
		$sql = "SELECT * FROM consignee WHERE id={$consign_id}";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function delete_consignee_contact_person($id)
	{
		$this->db->where("consignee_id", $id);
		$this->db->delete("contact_person");
	}

	public function add_consignee_contact_person_information($contacts)
	{
		$this->db->insert("contact_person", $contacts);
	}

	public function delete_consignee($id)
	{
		$this->db->where("id", $id);
		$this->db->delete("consignee");
		return $this->db->affected_rows();
	}

	public function delete_consignee_contact_person_selected($id)
	{
		$this->db->where("consignee_id", $id);
		$this->db->delete("contact_person");
	}

	public function get_consignee_names()
	{
		$sql = "SELECT id, name FROM consignee ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

}

/* End of file consignee_model.php */
/* Location: ./application/modules/consignee/models/consignee_model.php */