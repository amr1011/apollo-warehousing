<div class="main mod-margin">		
			<!-- upper  -->
			<div class="top-content-btn padding-all-20 bggray-new-gray">
				<div class="mod-select ">			
					<div class="select f-left width-350px turn-gray">
						<select>
							<option value="Displaying All Consignees" html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>				
					<div class="f-right margin-top-10">
						<a href="<?php echo BASEURL; ?>consignee/add_consignee_view">
							<button type="button" class=" margin-right-10 btn general-btn">Add Consignee</button>
						</a>
						<button type="button" class="general-btn dropdown-btn">View Filters / Search</button>
					</div>
					<div class="clear"></div>									
				</div>
				<div class="dropdown-search margin-top-40">
					<div class="triangle new-triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr class="width-100percent">
								<td class="width-75px">Search</td>
								<td>
									<div class="select medium font-400">
										<select>
											<option value="items">Items</option>
										</select>
									</div>
								</td>
								<td class="width-100percent">
									<input type="text">
								</td>
							</tr>
							<tr class="width-100percent">
								<td>Filter By:</td>
								<td>
									<div class="input-group medium italic display-inline-mid fixed-datepicker">
										<input class="form-control dp  default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
										<span class="input-group-addon "><i class="fa fa-calendar"></i></span>
									</div>
								</td>
								<td class="search-action">
									<div class="f-left">
										
										<div class="select medium italic">
											<select>
												<option value="status">Status</option>
											</select>
										</div>
									</div>
									<div class="f-right margin-left-30">
										<p class="display-inline-mid">Sort By:</p>
										<div class="select medium display-inline-mid italic">
											<select>
												<option value="withdrawal No.">Transfer No.</option>
											</select>
										</div>
										<div class="select medium display-inline-mid italic">
											<select>
												<option value="ascending">Ascending</option>
												<option value="descending">Descending</option>
											</select>
										</div>
									</div>
									<div class="clear"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10">
						<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
						<p class="display-inline-mid font-4 font-bold">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 ">Search</button>
					<div class="clear"></div>
				</div>
			</div>

			<!-- left  -->
			<div class="content-consignee no-padding-top">
				
				<div id="sideViewConsigneeAll" class="left-brand-item">	

					<!-- <div class="item-list active">
						<div class="display-inline-mid image">
							<img src="<?php echo BASEURL;?>assets/images/nestle.jpg">
						</div>
						<div class="display-inline-mid text">
							<p>Nestle Philippines Incorporated</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="<?php echo BASEURL;?>assets/images/metrobank.png">
						</div>
						<div class="display-inline-mid text">
							<p>Metrobank Bank and Trust Company</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="<?php echo BASEURL;?>assets/images/shell.png">
						</div>
						<div class="display-inline-mid text">
							<p>Pilipinas Shell Petroleum Corporation</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="<?php echo BASEURL;?>assets/images/mercury.jpeg">
						</div>
						<div class="display-inline-mid text">
							<p>Mercury Drug Corporation</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="<?php echo BASEURL;?>assets/images/san-miguel.png">
						</div>
						<div class="display-inline-mid text">
							<p>San Miguel Food Incorporated</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="<?php echo BASEURL;?>assets/images/smart.png">
						</div>
						<div class="display-inline-mid text">
							<p>Smart Communications Incorporated</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="<?php echo BASEURL;?>assets/images/imgres.jpg">
						</div>
						<div class="display-inline-mid text">
							<p>Zuelling Pharma Corporation</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="<?php echo BASEURL;?>assets/images/shell.png">
						</div>
						<div class="display-inline-mid text">
							<p>Pilipinas Shell Petroleum Corporation</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="<?php echo BASEURL;?>assets/images/pldt.png">
						</div>
						<div class="display-inline-mid text">
							<p>Philippine Long Distance Telephone</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="<?php echo BASEURL;?>assets/images/nestle.jpg">
						</div>
						<div class="display-inline-mid text">
							<p>Nestle Philippines Incorporated</p>
						</div>
					</div>		 -->

					<!-- <div class="border-top-medium border-bottom-medium padding-all-5 padding-top-10 padding-bottom-10 text-left hover-item ">
						<div class="display-inline-mid  margin-left-5">
							<img src="../assets/images/profile/profile_i.png" alt="rounded-image"  class="width-50px" />
						</div>
						<p class="display-inline-mid font-14 font-400 margin-left-10 black-color width-220px">Nestle Philippines Incorporated</p>
					</div>
					<div class=" border-bottom-medium padding-all-5 padding-top-10 padding-bottom-10 text-left hover-item">
						<div class="display-inline-mid margin-left-5">
							<img src="../assets/images/profile/profile_n.png" alt="rounded-image" class="width-50px" />
						</div>
						<p class="display-inline-mid font-14 font-400 margin-left-10 black-color width-220px">Metropolitan Bank and Trust Company</p>
					</div>
					<div class=" border-bottom-medium padding-all-5 padding-top-10 padding-bottom-10 text-left hover-item">
						<div class="display-inline-mid margin-left-5">
							<img src="../assets/images/profile/profile_k.png" alt="rounded-image" class="width-50px" />
						</div>
						<p class="display-inline-mid font-14 font-400 margin-left-10 black-color width-220px">Pilipinas Shell Petroleum Corporation</p>
					</div>
					<div class=" border-bottom-medium padding-all-5 padding-top-10 padding-bottom-10 text-left hover-item">
						<div class="display-inline-mid margin-left-5">
							<img src="../assets/images/profile/profile_s.png" alt="rounded-image" class="width-50px" />
						</div>
						<p class="display-inline-mid font-14 font-400 margin-left-10 black-color width-220px">Mercury Drug Corporation</p>
					</div>
					<div class=" border-bottom-medium padding-all-5 padding-top-10 padding-bottom-10 text-left hover-item">
						<div class="display-inline-mid margin-left-5">
							<img src="../assets/images/profile/profile_b.png" alt="rounded-image" class="width-50px" />
						</div>
						<p class="display-inline-mid font-14 font-400 margin-left-10 black-color width-220px">San Miguel Foods Incorporated</p>
					</div>
					<div class=" border-bottom-medium padding-all-5 padding-top-10 padding-bottom-10 text-left hover-item">
						<div class="display-inline-mid margin-left-5">
							<img src="../assets/images/profile/profile_c.png" alt="rounded-image" class="width-50px" />
						</div>
						<p class="display-inline-mid font-14 font-400 margin-left-10 black-color width-220px">Smart Communications Incorporated</p>
					</div>
					<div class=" border-bottom-medium padding-all-5 padding-top-10 padding-bottom-10 text-left hover-item">
						<div class="display-inline-mid margin-left-5">
							<img src="../assets/images/profile/profile_a.png" alt="rounded-image" class="width-50px" />
						</div>
						<p class="display-inline-mid font-14 font-400 margin-left-10 black-color width-220px">Zuellig Pharma Corporation</p>
					</div>
					<div class=" border-bottom-medium padding-all-5 padding-top-10 padding-bottom-10 text-left hover-item">
						<div class="display-inline-mid margin-left-5">
							<img src="../assets/images/profile/profile_j.png" alt="rounded-image" class="width-50px" />
						</div>
						<p class="display-inline-mid font-14 font-400 margin-left-10 black-color width-220px">Pilipinas Shell Petroleum Corporation</p>
					</div>
					<div class=" border-bottom-medium padding-all-5 padding-top-10 padding-bottom-10 text-left hover-item">
						<div class="display-inline-mid margin-left-5">
							<img src="../assets/images/profile/profile_m.png" alt="rounded-image" class="width-50px" />
						</div>
						<p class="display-inline-mid font-14 font-400 margin-left-10 black-color width-220px">Philippine Long Distance Telephone Company</p>
					</div>
					<div class=" border-bottom-medium padding-all-5 padding-top-10 padding-bottom-10 text-left hover-item">
						<div class="display-inline-mid margin-left-5">
							<img src="../assets/images/profile/profile_r.png" alt="rounded-image" class="width-50px" />
						</div>
						<p class="display-inline-mid font-14 font-400 margin-left-10 black-color width-220px">Manila Electric Company</p>
					</div> -->

				</div>
			</div>

			<!-- right  -->
			<div class="content-item padding-all-10">
				<div>	

					<div class="min-width-800px width-800 margin-top-30">
						<!-- <div class="text-center display-inline-top margin-right-50 ">
							<div class="upload-square display-inline-top margin-top-80 default-cursor">
								<i class="fa fa-camera"></i>
								<p class="title-cam">Upload Photo</p>
							</div>
						</div>						
						<div class="display-inline-top">
							<div class="f-right width-200 margin-bottom-10 margin-top-20">								
								<button type="button" class="general-btn">Edit Consignee Information</button>
							</div>	
							<div class="clear"></div>

							<div>
								<h2 class="text-left font-20 black-color font-bold">Unifoods Inc.</h2>
								<table class="text-left margin-top-10">
									<tbody>
										<tr>
											<td class="width-180"><p class="font-15">Alias:</p></td>
											<td><p class="font-15">Unifoods</p></td>
										</tr>
										<tr>
											<td><p class="font-15">GP Billing Mode:</p></td>
											<td><p class="font-15">N/A</p></td>
										</tr>
										<tr>
											<td><p>Company Contact No.:</p></td>
											<td><p>4553344</p></td>
										</tr>
										<tr>
											<td><p class="font-15">Tax Identification No.:</p></td>
											<td><p class="font-15">123-456-789</p></td>
										</tr>
										<tr>
											<td><p class="font-15">Business Address</p></td>
											<td><p class="font-15">123 Bayanihan Road. Siyudad City, Manila</p></td>
										</tr>
									</tbody>
								</table>
							</div>					
						</div> -->

						<h3 class="f-left font-20">Consignee Information</h3>
						<div class="f-right">							
							<a href="javascript:void(0)" id="generateConsignIdBtn" class="font-15 font-400"> <i class="fa fa-pencil-square-o font-20 font-500 margin-right-5"></i>Edit Consignee</a>
						</div>										
						<div class="clear"></div>
						
						<div class="width-100percent box-shadow-dark margin-top-20 bggray-white consignee-information-box">
							<div class="image-here f-left">
								<img id="setConsigneeImage" src="<?php echo BASEURL;?>assets/images/nestle.jpg">								
							</div>
							<div class="text-here f-left">
								<div class="image-name">
									<p id="setConsigneeName">Nestle Philippines Incorporated</p>
									<p id="setConsigneeAlias">Nestle Alias</p>
								</div>
								<div class="billing-mode">
									<table>
										<tbody>
											<tr>
												<td>GP BIlling Mode:</td>
												<td id="setConsigneeBillingMode">Lorem Ipsum</td>
											</tr>
											<tr>
												<td>Business Address:</td>
												<td id="setConsigneeAddress">39 Ollie Squares Apt. 977, Pasig City - PH</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="clear"></div>
						</div>

						
					</div>
					
					<div class="min-width-800px width-800 margin-top-50 ">
						<div class="box-contact-person">	
							<h3 class="f-left font-20">Contact Person</h3>										
							<div class="clear"></div>

							<div id="dispalyConsigneeContactPersonBox">
										<!-- first container -->
					<!-- 			<div class=" width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person">
									<div class="display-inline-mid width-70px margin-left-25">
										<img src="<?php echo BASEURL;?>assets/images/profile/profile_r.png" alt="user icon" class="width-100percent" />
									</div>
									<div class=" parent-contact-name">
										<div class="contact-name width-95per">
											<p class="font-14 font-500">Richard Cruz</p>
											<p class="font-14 font-400 gray-color italic">Sales</p>
										</div>
									</div>								
									<div class="clear"></div>
									<div class="f-left padding-left-30 margin-top-15">
										<p class="font-14 font-500">Mobile 1:</p>
										<p class="font-14 font-500 margin-top-5">Mobile 2:</p>
										<p class="font-14 font-500 margin-top-5">Email:</p>												
									</div>
									<div class="f-right text-right padding-right-30 margin-top-15">
										<p class="font-14 font-400">(+63) 910-154-8924</p>
										<p class="font-14 font-400 margin-top-5">(+63) 910-789-5789</p>
										<p class="font-14 font-400 margin-top-5">mohr_rita@gmail.com</p>

									</div>
									<div class="clear"></div>
								</div>	

								<div class=" width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person margin-left-20">
									<div class="display-inline-mid width-70px margin-left-25">
										<img src="<?php echo BASEURL;?>assets/images/profile/profile_m.png" alt="user icon" class="width-100percent" />
									</div>
									<div class=" parent-contact-name">
										<div class="contact-name width-95per">
											<p class="font-14 font-500">Michael De Castro</p>
											<p class="font-14 font-400 gray-color italic">Sales</p>
										</div>
									</div>								
									<div class="clear"></div>
									<div class="f-left padding-left-30 margin-top-15">
										<p class="font-14 font-500">Mobile 1:</p>
										<p class="font-14 font-500 margin-top-5">Mobile 2:</p>
										<p class="font-14 font-500 margin-top-5">Email:</p>												
									</div>
									<div class="f-right text-right padding-right-30 margin-top-15">
										<p class="font-14 font-400">(+63) 910-154-8924</p>
										<p class="font-14 font-400 margin-top-5">(+63) 910-789-5789</p>
										<p class="font-14 font-400 margin-top-5">mohr_rita@gmail.com</p>

									</div>
									<div class="clear"></div>
								</div> -->
								
							</div>

						

							<div class="clear"></div>
											
							
						</div>						
					</div>

					<div class="min-width-800px width-800 margin-top-50 text-left">
						<h3>Notes</h3>
						<p class="font-14 font-400 margin-top-15" id="setConsigneeNotes"><!-- Successful businesses know the importance of building and maintaining good working
						relationships, whether it is with partners, employee, business or trade organizations, the
						government, media representatives, vendors, consumers or the community at large. A 
						business must carefully balance the benefits of these interpersonal relationships and should
						never allow these relationships to blind their judgement especially when it relates to what is in
						the best interest of the business's continued success and growth. -->
						</p>
					</div>

				</div>
			</div>
			<div class="clear"></div>
		</div>			
	</section>