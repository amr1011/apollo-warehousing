<div class="main mod-margin">		
			<!-- upper  -->
			<div class="top-content-btn padding-all-20 bggray-new-gray">
				<div class="mod-select ">			
					<!-- <div class="select f-left width-350px turn-gray">
						<select>
							<option value="Displaying All Consignees" html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>				 -->
					
					<div class="clear"></div>	
						
				</div>
			</div>

			<!-- left  -->
			<div class="content-consignee no-padding-top">
				
				<div id="sideViewConsigneeAllAdd" class="left-brand-item">		
					<!-- <div class="item-list active new-consignee-parent normal-cursor">
						<div class="new-consignee" >
							<i class="fa fa-plus display-inline-mid gray-color"></i>
							<p class="display-inline-mid">New Consignee</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="../assets/images/nestle.jpg">
						</div>
						<div class="display-inline-mid text">
							<p>Nestle Philippines Incorporated</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="../assets/images/metrobank.png">
						</div>
						<div class="display-inline-mid text">
							<p>Metrobank Bank and Trust Company</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="../assets/images/shell.png">
						</div>
						<div class="display-inline-mid text">
							<p>Pilipinas Shell Petroleum Corporation</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="../assets/images/mercury.jpeg">
						</div>
						<div class="display-inline-mid text">
							<p>Mercury Drug Corporation</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="../assets/images/san-miguel.png">
						</div>
						<div class="display-inline-mid text">
							<p>San Miguel Food Incorporated</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="../assets/images/smart.png">
						</div>
						<div class="display-inline-mid text">
							<p>Smart Communications Incorporated</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="../assets/images/imgres.jpg">
						</div>
						<div class="display-inline-mid text">
							<p>Zuelling Pharma Corporation</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="../assets/images/shell.png">
						</div>
						<div class="display-inline-mid text">
							<p>Pilipinas Shell Petroleum Corporation</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="../assets/images/pldt.png">
						</div>
						<div class="display-inline-mid text">
							<p>Philippine Long Distance Telephone</p>
						</div>
					</div>		
					<div class="item-list">
						<div class="display-inline-mid image">
							<img src="../assets/images/nestle.jpg">
						</div>
						<div class="display-inline-mid text">
							<p>Nestle Philippines Incorporated</p>
						</div>
					</div>	 -->	

				
				</div>
			</div>

			<!-- right  -->
			<div class="content-item padding-all-10">
				<div>
				<form id="AddConsigneeForm">

					<div class="min-width-800px width-800 margin-top-30">
						
						
						<h3 class="f-left font-20">Consignee Information</h3>
						<div class="f-right">							
							<p class="font-14 font-400 light-red-color">Fields with asterisk "*" are required</p>
						</div>										
						<div class="clear"></div>

						<div class="add-consignee-information margin-top-20 text-left">
							<div class="display-inline-top left-information">

								<div id="upload_consignee_img" class="margin-left-40 margin-top-20"></div>
								<!-- <div  class="upload-square margin-left-40 margin-top-20">
									
									<i class="fa fa-camera"></i>
									<p>Upload Photo</p>
								</div>	 -->
							</div>								

							<div class="display-inline-top right-information">
								
								<div class="consignee-first-information">
									<table>
										<tbody>
											<tr>
												<td><p class="font-14 font-400">Name: <span class="font-15  light-red-color">*</span></p>
												</td>
												<td>
													<input type="text" id="addConsigneeName" datavalid="required" labelinput="Consignee Name" class="t-small"  />
												</td>
											</tr>
											<tr>
												<td><p class="font-14 font-400">Alias:</p></td>
												<td>
													<input type="text" id="addConsigneeAlias" class="t-small"  />
												</td>
											</tr>
										</tbody>
									</table>								
								</div>

								<div class="consignee-second-information">
									<table>
										<tbody>
											<tr>
												<td><p class="font-14 font-400">GP Billing Mode: <span class="red-color font-15">*</span></p>
												</td>
												<td>
													<div class="select large">
														<select id="AddConsigneeBillingMode" datavalid="required" labelinput="Consignee Billing Mode">
															<!-- <option value=""><i>Select GP Billing Mode</i></option> -->
															<option value="Prepaid">Prepaid</option>
															<option value="Postpaid">Postpaid</option>
															<option value="Upon Entry">Upon Entry</option>
														</select>
													</div>
												</td>
											</tr>
											<tr>
												<td class="v-aligntop"><p class="font-14 font-400 margin-top-10">Business Address: <span class="red-color font-15">*</span></p></td>
												<td><textarea id="addConsigneeBusinessAdd" datavalid="required" labelinput="Business Address"></textarea></td>
											</tr>
										</tbody>
									</table>																	
								</div>
							</div>
						</div>
					</div>
					
					<div class="min-width-800px width-800 margin-top-30 ">
						<div class="box-contact-person">	
							<h3 class="f-left font-20">Contact Person</h3>		
							<div class="f-right margin-top-5">
								<a href="javascript:void(0)" id="addContactlinkModal" class=" font-14 font-400 modal-trigger" modal-target="add-contact-person"> <i class="fa fa-user margin-right-10 font-20"></i>Add Contact Person</a>								
							</div>
							<div class="clear"></div>
							<div class="f-left no-contact-hide" id="noContactPersonHide">	
								<p class="display-inline-mid margin-top-10 font-14">No contact person added yet</p>
								
							</div>
							<div class="clear"></div>											
							
						</div>	

						<div id="displayContactPersons" class="box-contact-person modal-person-hide">

							<!-- <div class=" width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person">
								<div class="display-inline-mid width-70px margin-left-25">
									<img src="../assets/images/profile/profile_r.png" alt="user icon" class="width-100percent">
								</div>
								<div class=" parent-contact-name">
									<div class="contact-name width-95per">
										<p class="font-14 font-500">Richard Cruz</p>
										<p class="font-14 font-400 gray-color italic">Sales</p>
									</div>
								</div>								
								<div class="clear"></div>
								<div class="f-left padding-left-30 margin-top-15">
									<p class="font-14 font-500">Mobile 1:</p>
									<p class="font-14 font-500 margin-top-5">Mobile 2:</p>
									<p class="font-14 font-500 margin-top-5">Email:</p>												
								</div>
								<div class="f-right text-right padding-right-30 margin-top-15">
									<p class="font-14 font-400">(+63) 910-154-8924</p>
									<p class="font-14 font-400 margin-top-5">(+63) 910-789-5789</p>
									<p class="font-14 font-400 margin-top-5">mohr_rita@gmail.com</p>

								</div>								
								<div class="clear"></div>
								<i class=" fa fa-pencil gray-color font-20 f-right margin-top-10 margin-right-30 default-cursor modal-trigger" modal-target="edit-contact-person"></i>
								<div class="clear"></div>
							</div> -->

							<!-- <div class=" width-50per margin-top-20 bggray-white box-shadow-dark f-left text-left contact-person margin-left-20">
								<div class="display-inline-mid width-70px margin-left-25">
									<img src="../assets/images/profile/profile_m.png" alt="user icon" class="width-100percent">
								</div>
								<div class=" parent-contact-name">
									<div class="contact-name width-95per">
										<p class="font-14 font-500">Michael De Castro</p>
										<p class="font-14 font-400 gray-color italic">Sales</p>
									</div>
								</div>								
								<div class="clear"></div>
								<div class="f-left padding-left-30 margin-top-15">
									<p class="font-14 font-500">Mobile 1:</p>
									<p class="font-14 font-500 margin-top-5">Mobile 2:</p>
									<p class="font-14 font-500 margin-top-5">Email:</p>												
								</div>
								<div class="f-right text-right padding-right-30 margin-top-15">
									<p class="font-14 font-400">(+63) 910-154-8924</p>
									<p class="font-14 font-400 margin-top-5">(+63) 910-789-5789</p>
									<p class="font-14 font-400 margin-top-5">mohr_rita@gmail.com</p>

								</div>
								<div class="clear"></div>
								<i class=" fa fa-pencil gray-color font-20 f-right margin-top-10 margin-right-30 default-cursor modal-trigger" modal-target="edit-contact-person"></i>
								<div class="clear"></div>
							</div> -->

							<div class="clear"></div>

						</div>					
					</div>
					<div class="clear"></div>	
					
					<div class="min-width-800px width-800 margin-top-30 notes-under-consignee">
						<div class="box-contact-person">	
							<h3 class="f-left font-20">Notes</h3>										
							<div class="clear"></div>
							<textarea id="addConsigneeNotes"  labelinput="Business Address" class="margin-top-10"></textarea>	

							<div class="f-right width-300px margin-top-20 text-right">
								<a href="<?php echo BASEURL;?>consignee/consignee_list">
									<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
								</a>
								<button type="button" id="AddConsigneeSubmit" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Add Consignee</button>
							</div>									
							<div class="Clear"></div>
						</div>						
					</div>
					

				</div>
			</div>
			<div class="clear"></div>
		</div>				
	</section>

	</form>	

	<!-- Here -->

	<div class="modal-container" modal-id="add-contact-person">
		<div class="modal-body small width-600px">				

			<div class="modal-head">
				<h4 class="text-left">Add Contact Person</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<form id="AddConsigneeContactPersonForm">
			<!-- content -->
				<div class="modal-content text-left">	
					<p class="font-14 font-400 f-right light-red-color">Fields with asterisk "*" are required</p>
					<div class="clear"></div>
					
					<div class="head-contact-person margin-bottom-15">
						<div id="add_new_contact_person_img" class="margin-left-20 image-here" style="background-color: #EDEDED; border: none;"></div>
						<!-- <div class="image-here margin-left-20">
							
							<i class="fa fa-camera"></i>
							<p>Upload Photo</p>
						</div> -->
						<div class="text-here">
							<table>
								<tbody>
									<tr>
										<td>
											<p class="font-14 font-400">Name: <span class="font-14 font-400 red-color">*</span></p>
										</td>
										<td>
											<input type="text" id="addConsigneeContactPersonName" datavalid="required" labelinput="Contact Person"  class="t-small" />
										</td>
									</tr>
									<tr>
										<td>
											<p class="font-14 font-400">Position:</p>
										</td>
										<td>
											<input type="text" id="addConsigneeContactPersonPosition" class="t-small" />
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="body-contact-person padding-top-10">
						<table>
							<tbody>
								<tr>
									<td class="width-180px">
										<p class="font-14 font-400">Contact Number <span class="font-500 font-14 red-color">*</span></p>
									</td>
									<td>
										<div class="select medium">
											<select>
												<option value="landline">LandLine</option>
												<option value="mobile">Mobile</option>
											</select>
										</div>
									</td>
									<td>
										<input type="text" id="addConsigneeContactNumber" datavalid="required" labelinput="Contact Number" class="t-small no-margin-left super-width-100per">
									</td>
								</tr>
								<tr>
									<td class="width-180px">
										<p class="width-180px font-14 font-400">Alternate Contact Number:</p>
									</td>
									<td>
										<div class="select medium">
											<select>
												<option value="landline">LandLine</option>
												<option value="mobile">Mobile</option>
											</select>
										</div>
									</td>
									<td>
										<input type="text" id="addConsigneeContactAlterNumber" class="t-small no-margin-left super-width-100per">
									</td>
								</tr>
								<tr>
									<td>
										<p class="font-14 font-400">Email: <span class="font-500 font-14 red-color">*</span></p>
									</td>
									<td colspan="2">
										<input type="text" id="addConsigneeContactPersonEmail" datavalid="email required" labelinput="Contact Email Address" class="t-large no-margin-left" />
									</td>								
								</tr>
							</tbody>
						</table>

														
					</div>

				</div>
			</form>

			<div class="modal-footer text-right margin-bottom-10">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" id="addConsigneeContactPersonSubmit" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30 modal-person-trigger">Add Contact Person</button>
			</div>
		</div>	
	</div>

	<div class="modal-container" modal-id="edit-contact-person">
		<div class="modal-body small width-600px">				

			<div class="modal-head">
				<h4 class="text-left">Edit Contact Person</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">	
				<p class="font-14 font-400 f-right light-red-color">Fields with asterisk "*" are required</p>
				<div class="clear"></div>
				
				<div class="head-contact-person margin-bottom-15">
					<div id="edit_new_contact_person_img" class="margin-left-20 image-here" style="background-color: #EDEDED; border: none;"></div>
					<!-- <div class="image-here">
						<i class="fa fa-camera"></i>
						<p>Upload Photo</p>
					</div> -->
					<div class="text-here">
						<table>
							<tbody>
								<tr>
									<td>
										<p class="font-14 font-400">Name: <span class="font-14 font-400 red-color">*</span></p>
									</td>
									<td>
										<input type="text" class="t-small" id="editConsigneeContactPersonName" datavalid="required" labelinput="Name" />
									</td>
								</tr>
								<tr>
									<td>
										<p class="font-14 font-400">Position:</p>
									</td>
									<td>
										<input type="text" class="t-small" id="editConsigneeContactPersonPosition"/>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="body-contact-person padding-top-10">
					<table>
						<tbody>
							<tr>
								<td class="width-180px">
									<p class="font-14 font-400">Contact Number <span class="font-500 font-14 red-color">*</span></p>
								</td>
								<td>
									<div class="select medium">
										<select>
											<option value="landline">LandLine</option>
											<option value="mobile">Mobile</option>
										</select>
									</div>
								</td>
								<td>
									<input type="text" class="t-small no-margin-left super-width-100per" datavalid="required" labelinput="Contact Number" id="editConsigneeContactPersonNumber">
								</td>
							</tr>
							<tr>
								<td class="width-180px">
									<p class="width-180px font-14 font-400">Alternate Contact Number:</p>
								</td>
								<td>
									<div class="select medium">
										<select>
											<option value="landline">LandLine</option>
											<option value="mobile">Mobile</option>
										</select>
									</div>
								</td>
								<td>
									<input type="text" class="t-small no-margin-left super-width-100per" id="editConsigneeContactPersonAlterNumber">
								</td>
							</tr>
							<tr>
								<td>
									<p class="font-14 font-400">Email: <span class="font-500 font-14 red-color">*</span></p>
								</td>
								<td colspan="2">
									<input type="email" class="t-large no-margin-left" datavalid="email required" labelinput="Contact Email Address"  id="editConsigneeContactPersonEmail" />
								</td>								
							</tr>
						</tbody>
					</table>

													
				</div>

			</div>
			<div id="editConsignessContactPersonModalFooter" class="modal-footer  margin-bottom-10">
				<!-- <i class="fa fa-trash-o f-left gray-color font-20 margin-top-5 default-cursor"></i>
				<div class="f-right text-right width-300px">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Save Changes</button>
				</div>
				<div class="clear"></div> -->
			</div>
		</div>	
	</div>