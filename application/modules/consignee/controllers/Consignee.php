<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consignee extends MX_Controller {


	public function __construct()
	{
		parent:: __construct();
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		$this->load->model('consignee_model');

		$response = array(
			"status" => false,
			"message"=>"",
			"data"=>array()
			);
	}
	
	public function index()
	{
		$data["title"] = "CONSIGNEE";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav", $data);
		$this->load->view("consignee_view", $data);
		$this->load->view("includes/footer");
	}

	public function consignee_list()
	{
		$data["title"] = "CONSIGNEE";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav", $data);
		$this->load->view("consignee_view", $data);
		$this->load->view("includes/footer");
	}

	public function edit_consignee()
	{
		$data["title"] = "CONSIGNEE";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav", $data);
		$this->load->view("edit_consignee_view", $data);
		$this->load->view("includes/footer");
	}

	public function add_consignee_view()
	{
		$data["title"] = "CONSIGNEE";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav", $data);
		$this->load->view("add_consignee_view", $data);
		$this->load->view("includes/footer");
	}
	

	public function add_consignee()
	{
		
		// $this->form_validation->set_error_delimiters('', '');

		$consignee_information = json_decode($this->input->post('consignee_information'), true);

		$this->form_validation->set_rules("consignee_information", "Contact Persons", "callback_add_consignee_information_validation");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"]= $this->form_validation->error_array();

			echo json_encode($response);
		}
		else
		{

			$data  = array(
				"name"=>$consignee_information["name"],
				"alias"=>($consignee_information["alias"] !== null) ? $consignee_information["alias"] : false,
				"gb_billing_mode"=>$consignee_information["gb_billing_mode"],
				"address"=>$consignee_information["address"],
				"notes"=>($consignee_information["notes"] !== null) ? $consignee_information["notes"] : false,
				"image"=>($consignee_information["image"] !== null) ? $consignee_information["image"] : false
				);

			$consignee_id = $this->consignee_model->add_consignee($data);

			$response["status"] = true;
			$response["message"] = "Successfully Added";
			$response["data"]= array("consignee_id"=>$consignee_id);

			echo json_encode($response);

			if($consignee_information["contact_person"] !== null AND json_last_error() === JSON_ERROR_NONE)
			{

				foreach ($consignee_information["contact_person"] as $key => $value) {
					// echo $consignee_information["contact_person"][$key]["name"];

					$contact_person = array(
							"name"=>$consignee_information["contact_person"][$key]["name"],
							"position"=>($consignee_information["contact_person"][$key]["position"] !== null) ? $consignee_information["contact_person"][$key]["position"] : false,
							"contact_number"=>($consignee_information["contact_person"][$key]["contact_number"] !== null) ? $consignee_information["contact_person"][$key]["contact_number"] : false,
							"alternate_contact_number"=>($consignee_information["contact_person"][$key]["alternate_contact_number"] !== null) ? $consignee_information["contact_person"][$key]["alternate_contact_number"] : false,
							"email_address"=>($consignee_information["contact_person"][$key]["email_address"] !== null) ? $consignee_information["contact_person"][$key]["email_address"] : false,
							"image"=>($consignee_information["contact_person"][$key]["image"] !== null) ? $consignee_information["contact_person"][$key]["image"] : false,
							"consignee_id"=>$consignee_id,
							"alternate_contact_number"=>($consignee_information["contact_person"][$key]["alternate_contact_number"] !== null) ? $consignee_information["contact_person"][$key]["alternate_contact_number"] : false,
							"is_deleted"=>0,
							"date_created"=> date("Y-m-d H:i:s")
						);

					$this->consignee_model->add_contact_person($contact_person);
				}
			}

		


		}
	}

	public function add_consignee_information_validation()
	{
		$consignee_information = json_decode($this->input->post('consignee_information'), true);

		if($consignee_information !== null AND json_last_error() === JSON_ERROR_NONE)
		{


			if($consignee_information["name"] == "")
			{
				$this->form_validation->set_message("add_consignee_information_validation", "Consignee Name is Required");
				return false;
			}

			if($consignee_information["gb_billing_mode"] == "")
			{
				$this->form_validation->set_message("add_consignee_information_validation", "Consignee GB Billing Mode is Required");
				return false;
			}

			if($consignee_information["address"] == "")
			{
				$this->form_validation->set_message("add_consignee_information_validation", "Consignee Address is Required");
				return false;
			}
		}
		else
		{
			$this->form_validation->set_message("add_consignee_information_validation", "Consignee Information is Required");
			return false;
		}
	}

	public function contact_person_validation()
	{
		$arr_contact_person = json_decode($this->input->post('contact_person'), true);
		$error = 0;

		if( count($arr_contact_person) == 0 )
		{
			return true;
		}

		if($arr_contact_person !== null AND json_last_error() === JSON_ERROR_NONE)
		{
			foreach ($arr_contact_person as $key => $value) {
				if( 
					((isset($arr_contact_person[$key]['name'] )) AND
					(isset($arr_contact_person[$key]['position'] )) AND
					(isset($arr_contact_person[$key]['contact_number'] )) AND
					(isset($arr_contact_person[$key]['email_address'] )) AND
					(isset($arr_contact_person[$key]['image'] ))) === false
				)
				{
					$this->form_validation->set_message("contact_person_validation", "Invalid Contact Person Values");
					return false;
				}
			}
		}

	}

	public function upload() 
	{
		$response = array();
		$arr_response = array();

		$status = false;
		$message = "Fail";
		if(isset($_FILES["attachment"])){
			$name = $_FILES["attachment"]["name"];
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 20; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    
		    $file_name = $randomString . "." .$ext;

		    move_uploaded_file($_FILES["attachment"]["tmp_name"], UPLOAD_IMAGE_PATH.$file_name);

		    $arr_response["path"] = UPLOAD_IMAGE_PATH.$file_name;
		    $arr_response["location"] = BASEURL . "uploads/images/" . $file_name;
		    $status = true;
		    $message = "Succes";
		}

		$response["status"] = $status;
		$response["message"] = $message;
		$response["data"] = $arr_response;
		echo json_encode($response);
	}

	public function upload_contact_person_image() 
	{
		$response = array();
		$arr_response = array();

		$status = false;
		$message = "Fail";
		if(isset($_FILES["attachment"])){
			$name = $_FILES["attachment"]["name"];
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 20; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    
		    $file_name = $randomString . "." .$ext;

		    move_uploaded_file($_FILES["attachment"]["tmp_name"], UPLOAD_IMAGE_PATH.$file_name);

		    $arr_response["path"] = UPLOAD_IMAGE_PATH.$file_name;
		    $arr_response["location"] = BASEURL . "uploads/images/" . $file_name;
		    $status = true;
		    $message = "Succes";
		}

		$response["status"] = $status;
		$response["message"] = $message;
		$response["data"] = $arr_response;
		echo json_encode($response);
	}



	public function add_contact_person()
	{

		$consignee_information = json_decode($this->input->post('consignee_information'), true);

		$this->form_validation->set_rules("consignee_information", "Consignee Information", "callback_consignee_information_validation");


		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"]= $this->form_validation->error_array();

			echo json_encode($response);
		}
		else
		{
			
			if($consignee_information !== null AND json_last_error() === JSON_ERROR_NONE)
			{ 
				$data = array(
					"name"=>$consignee_information['name'],
					"position"=>$consignee_information['position'],
					"contact_number"=>$consignee_information['contact_number'],
					"email_address"=>$consignee_information['email_address'],
					"image"=>$consignee_information['image'],
					"consignee_id"=>$consignee_information['consignee_id']
				);
				$data["alternate_contact_number"] = "";
				$data["is_deleted"] = 0;
				$data["date_created"] = date("Y-m-d H:i:s");

				if(isset($consignee_information["alternate_contact_number"]) AND $consignee_information["alternate_contact_number"] !="")
				{
					$data["alternate_contact_number"]= $consignee_information["alternate_contact_number"];
				}



				$result = $this->consignee_model->add_contact_person($data);

				$response["status"] = true;
				$response["message"] = "Successfully Added";
				$response["data"]= array("contact_person_id"=>$result);

				echo json_encode($response);
			}

		}

	}

	public function consignee_information_validation()
	{
		$arr_consignee_information = json_decode($this->input->post('consignee_information'), true);
		
		if( count($arr_consignee_information) == 0 )
		{
			return true;
		}

		if($arr_consignee_information !== null AND json_last_error() === JSON_ERROR_NONE)
		{ 
			if(
				(
					($arr_consignee_information["name"] != "") AND 
					($arr_consignee_information["position"]  != "") AND 
					($arr_consignee_information["contact_number"]  != "") AND 
					($arr_consignee_information["email_address"]  != "") AND 
					($arr_consignee_information["image"]  != "") AND 
					($arr_consignee_information["consignee_id"]  != "")
				) === false
			)
			{
				$this->form_validation->set_message("consignee_information_validation", "Invalid Consignee Information");
				return false;
			}
		}
	}


	public function update_consignee_information()
	{

		$id = $this->input->post('id');
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules('id', 'Cognsinee ID', 'trim|required|integer');
		$this->form_validation->set_rules('data', 'Cognsinee Data', 'trim|required|callback_update_consignee_information_validation');

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"]= $this->form_validation->error_array();

			echo json_encode($response);
		}else{
			 $arr_data = array();
			 $contacts = array();

			if($data !== null AND json_last_error() === JSON_ERROR_NONE)
			{ 
				
					
					if(isset($data['name']))
					{
						$arr_data['name'] = $data['name'];
					}

					if(isset($data['alias']))
					{
						$arr_data['alias'] = $data['alias'];
					}

					if(isset($data['gb_billing_mode']))
					{
						$arr_data['gb_billing_mode'] = $data['gb_billing_mode'];
					}

					if(isset($data['address']))
					{
						$arr_data['address'] = $data['address'];
					}

					if(isset($data['notes']))
					{
						$arr_data['notes'] = $data['notes'];
					}

					if(isset($data['image']))
					{
						$arr_data['image'] = $data['image'];
					}

				
					$result = $this->consignee_model->update_consignee_information($id, $arr_data);

					$this->consignee_model->delete_consignee_contact_person($id);


					foreach ($data['contact_person'] as $key => $value) {
						$contacts = array(
							"name"=>$data['contact_person'][$key]["name"],
							"position"=>$data['contact_person'][$key]["position"],
							"contact_number"=>$data['contact_person'][$key]["contact_number"],
							"alternate_contact_number"=>($data['contact_person'][$key]["alternate_contact_number"] !== null) ? $data['contact_person'][$key]["alternate_contact_number"] : false,
							"email_address"=>$data['contact_person'][$key]["email_address"],
							"image"=>$data['contact_person'][$key]["image"],
							"consignee_id"=>$data['contact_person'][$key]["consignee_id"],
							"date_created"=> date("Y-m-d H:i:s")
							);

						$this->consignee_model->add_consignee_contact_person_information($contacts);
						
					}
					
					$response["status"] = true;
					$response["message"] = "Successfully Updated";
					$response["data"]= array("affected_rows"=>$result);

					echo json_encode($response);
				
			}

		}
	}

	public function update_consignee_information_validation()
	{
		$data = json_decode($this->input->post('data'), true);

		if($data !== null AND json_last_error() === JSON_ERROR_NONE)
		{ 
			if(isset($data["name"]) AND trim($data["name"]) == "")
			{
				$this->form_validation->set_message("update_consignee_information_validation", "Consignee Name must not empty");
				return false;
			}


			if(isset($data["gb_billing_mode"]) AND trim($data["gb_billing_mode"]) == "")
			{
				$this->form_validation->set_message("update_consignee_information_validation", "Consignee GB Billing Mode must not empty");
				return false;
			}

			if(isset($data["address"]) AND trim($data["address"]) == "")
			{
				$this->form_validation->set_message("update_consignee_information_validation", "Consignee Address must not empty");
				return false;
			}

		}
		else
		{
			$this->form_validation->set_message("update_consignee_information_validation", "Invalid Consignee Information");
			return false;
		}
	}

	public function get_all_consignee()
	{
		$arr_main = array();
		$arr_result = array();

		$data = $this->consignee_model->get_all_consignee();

		$count =0;
		foreach($data as $key => $value)
		{
			$result = array(
				"id"=> $data[$key]["id"],
				"name"=> $data[$key]["name"],
				"alias"=> $data[$key]["alias"],
				"gb_billing_mode"=> $data[$key]["gb_billing_mode"],
				"address"=> $data[$key]["address"],
				"notes"=> $data[$key]["notes"],
				"image"=> $data[$key]["image"]
				);

			$contact_person = $this->consignee_model->get_consignee_contact_person($data[$key]["id"]);

			$result["contact_person"] = $contact_person;

			$count++;
			array_push($arr_main,$result);
		}


		header("Content-Type: application/json");
		$response["status"] = true;
		$response["message"] = "Successfully Updated";
		$response["data"]= $arr_main;

		echo json_encode($response);

	}

	public function get_selected_consignee()
	{
		$consign_id = $this->input->post('consignee_id');
		$arr_main = array();
		$arr_result = array();

		$data = $this->consignee_model->get_selected_consignee($consign_id);

		foreach($data as $key => $value)
		{
			$result = array(
				"id"=> $data[$key]["id"],
				"name"=> $data[$key]["name"],
				"alias"=> $data[$key]["alias"],
				"gb_billing_mode"=> $data[$key]["gb_billing_mode"],
				"address"=> $data[$key]["address"],
				"notes"=> $data[$key]["notes"],
				"image"=> $data[$key]["image"]
				);

			$contact_person = $this->consignee_model->get_consignee_contact_person($consign_id);

			$result["contact_person"] = $contact_person;

			array_push($arr_main,$result);
		}

		header("Content-Type: application/json");
		$response["status"] = true;
		$response["message"] = "Successfully Updated";
		$response["data"]= $arr_main;

		echo json_encode($response);



	}

	public function delete()
	{
		$data = json_decode($this->input->post('data'), true);
		$result = "";

		$this->form_validation->set_rules("data", "Consignee Information", "required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"]= $this->form_validation->error_array();

			echo json_encode($response);
		}
		else
		{

			foreach ($data as $key => $value) {
				
				$result = $this->consignee_model->delete_consignee($data[$key]["id"]);
				$this->consignee_model->delete_consignee_contact_person_selected($data[$key]["id"]);
			}


			$response["status"] = true;
			$response["message"] = "Successfully Updated";
			$response["data"]= array("affected_rows"=>$result);

			echo json_encode($response);
		}

	}

	public function get_consignee_names()
	{
		$result = $this->consignee_model->get_consignee_names();

		header("Content-Type: application/json");
		$response["status"] = true;
		$response["message"] = "Successful";
		$response["data"]= $result;

		echo json_encode($response);

	}
}

/* End of file Consignee.php */
/* Location: ./application/modules/consignee/controllers/Consignee.php */