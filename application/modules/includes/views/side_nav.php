
<!-- active state just add class 'active' at the <a>-->
<!-- add class 'show' to reveal submenu -->

<!-- Add Active link per page -->
	<?php 
	$active_links_head = "";

	if($title == "Dashboard")
	{
		$active_links_head = "dashboard";
	}else if($title == "INVENTORY" || $title == "INVENTORY - STOCK GOODS" || $title == "Stock Item Activity"){
		$active_links_head = "inventory";
	}else if($title == "RECEIVING - COMPANY GOODS"){
		$active_links_head = "receiving-company";
	}else if($title == "RECEIVING - CONSIGNEE GOODS"){
		$active_links_head = "receiving-consignee";
	}else if($title == "TRANSFER - COMPANY GOODS"){
		$active_links_head = "transfer-company";
	}else if($title == "TRANSFER - CONSIGNEE GOODS"){
		$active_links_head = "transfer-consignee";
	}else if($title == "WITHDRAWAL - COMPANY GOODS"){
		$active_links_head = "withdrawal-company";
	}else if($title == "WITHDRAWAL - CONSIGNEE GOODS"){
		$active_links_head = "withdrawal-consignee";
	}else if($title == "Reports"){
		$active_links_head = "reports";
	}else if($title == "Item Management"){
		$active_links_head = "product-management";
	}else if($title == "Storage Management"){
		$active_links_head = "storage-management";
	}else if($title == "Team Management"){
		$active_links_head = "team-management";
	}else if($title == "CONSIGNEE"){
		$active_links_head = "consignee";
	}else if($title == "Vessel List"){
		$active_links_head = "vessel-list";
	}else if($title == "System Settings"){
		$active_links_head = "system-settings";
	}



	// echo $uri;


	 ?>
<!-- End Add Active link per page -->





<section style-id="side-nav">
	<div class="logo apolo-white-bg">
		 <a href="<?php echo BASEURL; ?>"><img class="img-responsive  padding-right-5 margin-top-10" src="<?php echo base_url(); ?>assets/images/logo.svg"></a>

	</div>
	<div class="side-nav-container">
		<div class="side-nav-content">
			<div class="with-logo">				
				<p class="logo-wh">WAREHOUSE</p>				
			</div>
			<div>
				<a href="<?php echo BASEURL; ?>dashboard" class="<?php echo ($active_links_head == 'dashboard') ? 'active':'';?> link-trigger">
					<img class="img-responsive" alt="Dashboard" src="<?php echo base_url(); ?>assets/images/ui/dashboard.png"><p>Dashboard</p>
				</a>
			</div>

			<div>
				<a href="<?php echo BASEURL; ?>products/stock_inventory" class="<?php echo ($active_links_head == 'inventory') ? 'active':'';?> link-trigger"> 
					<img class="img-responsive" alt="Deals" src="<?php echo base_url(); ?>assets/images/ui/product-inv.png"><p>Inventory</p>
				</a>

				<!-- <a href="#" class="nav-ticker">
					<img class="img-responsive" alt="Deals" src="<?php echo base_url(); ?>assets/images/ui/product-inv.png"><p>Product Inventory</p>
				</a>

				<div class="subnav-panel config-panel">
					<ul>
						<li class="<?php echo (isset($title) AND $title == 'Stock Inventory')?'active':'';?>">
							<a href="<?php echo BASEURL; ?>products/stock_inventory" class=" link-trigger">Stock Goods</a>
						</li>
						<li class="<?php echo (isset($title) AND $title == 'INVENTORY - CONSIGNEE GOODS')?'active':'';?>" >
							<a href="<?php echo BASEURL; ?>products/consignee_inventory" class="link-trigger">Consigee Goods</a>
						</li>
					</ul>
				</div> -->
			</div>

			<div>				
				<a href="" class="nav-ticker">
					<img class="img-responsive" alt="Contacts" src="<?php echo base_url(); ?>assets/images/ui/receiving.png"><p>Receiving</p>
				</a>
				<div class="subnav-panel config-panel <?php echo ($active_links_head == 'receiving-company' || $active_links_head == 'receiving-consignee') ? 'show':'';?>">
					<ul>
						<li class="<?php echo ($active_links_head == 'receiving-company') ? 'active':'';?>">
							<a href="<?php echo BASEURL; ?>receiving/company_goods" class=" link-trigger">Company Goods</a>
						</li>
						<li class="<?php echo ($active_links_head == 'receiving-consignee') ? 'active':'';?>" >
							<a href="<?php echo BASEURL; ?>receiving/consignee_receiving" class="link-trigger">Consigee Goods</a>
						</li>
					</ul>
				</div>
			</div>

			<div>
				<a href="#" class="nav-ticker">
				<!-- <a href="../settings/transfer.php" class="<?php echo (isset($title) AND $title == 'Transfer')?'active':'';?> link-trigger"> -->
					<img class="img-responsive" alt="Contacts" src="<?php echo base_url(); ?>assets/images/ui/transfer.png"><p>Transfer</p>
				</a>
				<div class="subnav-panel config-panel <?php echo ($active_links_head == 'transfer-company' || $active_links_head == 'transfer-consignee') ? 'show':'';?>">
					<ul>
						<li class="<?php echo ($active_links_head == 'transfer-company') ? 'active':'';?>">
							<a href="<?php echo BASEURL; ?>transfers/company_transfer" class=" link-trigger">Company Goods</a>
						</li>
						<li class="<?php echo ($active_links_head == 'transfer-consignee') ? 'active':'';?>" >
							<a href="<?php echo BASEURL; ?>transfers/consign_transfer" class="link-trigger">Consignee Goods</a>
						</li>
					</ul>
				</div>
			</div>


			<div>				
				<a href="" class="nav-ticker">
					<img class="img-responsive" alt="Contacts" src="<?php echo base_url(); ?>assets/images/ui/receiving.png"><p>Withdrawals</p>
				</a>
				<div class="subnav-panel config-panel <?php echo ($active_links_head == 'withdrawal-company' || $active_links_head == 'withdrawal-consignee') ? 'show':'';?> ">
					<ul>
						<li class="<?php echo ($active_links_head == 'withdrawal-company') ? 'active':'';?>">
							<a href="<?php echo BASEURL; ?>withdrawals/company_record_list" class=" link-trigger">Company Goods</a>
						</li>
						<li class="<?php echo ($active_links_head == 'withdrawal-consignee') ? 'active':'';?>" >
							<a href="<?php echo BASEURL; ?>withdrawals/consignee_record_list" class="link-trigger">Consigee Goods</a>
						</li>
					</ul>
				</div>
			</div>

			<div>
				<a href="<?php echo BASEURL; ?>reports/display_reports" class="<?php echo ($active_links_head == 'reports') ? 'active':'';?> link-trigger">
					<img class="img-responsive" alt="Dashboard" src="<?php echo BASEURL; ?>assets/images/ui/report.svg"><p>Reports</p>
				</a>
			</div>

			
			<div>
				
				<a href="#" class="nav-ticker config-click">
					<img class="img-responsive" alt="Deals" src="<?php echo BASEURL; ?>assets/images/ui/configuration.png"><p>Configuration</p>
				</a>

				<div class="subnav-panel <?php echo ($active_links_head == 'product-management' || $active_links_head == 'storage-management' 
						|| $active_links_head == 'team-management' || $active_links_head == 'consignee' || $active_links_head == 'vessel-list' 
						|| $active_links_head == 'system-settings') ? 'show':'';?>">
					<ul>
						<li class="<?php echo ($active_links_head == 'product-management') ? 'active':'';?>">
							<a href="<?php echo BASEURL . "products/product_management" ?>" class=" link-trigger">Product Management</a>
						</li>
						<li class="<?php echo ($active_links_head == 'storage-management') ? 'active':'';?>" >
							<a href="<?php echo BASEURL; ?>storage_location_management" class="link-trigger">Storage Management</a>
						</li>
						<li class="<?php echo ($active_links_head == 'team-management') ? 'active':'';?>">
							<a href="<?php echo BASEURL;?>team_management" class=" link-trigger">Team Management</a>
						</li>
						<li class="<?php echo ($active_links_head == 'consignee') ? 'active':'';?>">
							<a href="<?php echo BASEURL;?>consignee/consignee_list" class=" link-trigger">Consignee List</a>
						</li>
						<li class="<?php echo ($active_links_head == 'vessel-list') ? 'active':'';?>">
							<a href="<?php echo BASEURL; ?>vessel_list" class=" link-trigger">Vessel List</a>
						</li>
						<li class="<?php echo ($active_links_head == 'system-settings') ? 'active':'';?>">
							<a href="<?php echo BASEURL ?>settings/system_settings" class=" link-trigger">System Settings</a>
						</li>
						<!-- <li class="<?php echo (isset($title) AND $title == 'Category Management')?'active':'';?>">
							<a href="../settings/category-management.php" class=" link-trigger">Category Management</a>
						</li>
						
						<li class="<?php echo (isset($title) AND $title == 'Storage Location Management')?'active':'';?>">
							<a href="../settings/store-management.php" class=" link-trigger">Storage Location Management</a>
						</li>
						<li class="<?php echo (isset($title) AND $title == 'Container Management')?'active':'';?>">
							<a href="../settings/container-manegement.php" class=" active link-trigger">Container Management</a>
						</li> -->
					</ul>
				</div>
			</div>

		</div>
	</div>
</section>

