<!-- For Deleting Modal -->
<div class="modal-container delete-modal" id="trashModal" modal-id="trash-modal">
	<div class="modal-body small">				

		<div class="modal-head">
			<h4 class="text-left ">Delete Information</h4>				
			<div class="modal-close close-me"></div>
		</div>
		<div class="modal-content">	
			<p class="font-15 black-color ">Are you sure you want to Delete?</p>
			
		</div>
		<div class="modal-footer text-right ">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid yes-btn confirm-delete" id="confirmDelete">Delete</button>
		</div>
	</div>	
</div>
<!-- End For Deleting Modal -->

<!-- cancel confirmation -->

	
	<div class="modal-container" id="cancelModal">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left ">Cancel Information</h4>				
				<div class="modal-close close-me"></div>
			</div>
			<div class="modal-content">	
				<p class="font-15 black-color ">Are you sure you want to cancel creating a record?</p>
				
			</div>
			<div class="modal-footer text-right ">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid " id="confirmCancel">Confirm</button>
			</div>
		</div>	
	</div>


	<!-- end Cancel -->

<!-- For Generating Modal -->
<div class="modal-container" id="generated-data-view" >
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>
<!-- End For Generating Modal -->

<!-- For Saving Modal -->
<div class="modal-container" id="generate-saving-data" >
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left"></h4>
        </div>
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Saving Data, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>

<!-- End  For Saving Modal -->



<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
	
	<!-- INDEPENDENT SCRIPT -->
	<script src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/offline.min.js"></script>
	<!-- END INDEPENDENT SCRIPT -->

	<!-- JQUERY DEPENDENCIES -->
	<script src="<?php echo base_url(); ?>assets/js/jquery.mousewheel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/customdropdown.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.ru.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/fullcalendar.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.cookie.js"></script>
	<!-- END JQUERY DEPENDENCIES -->
	
	<!-- CORE JS SCRIPTS -->
	<script src="<?php echo base_url(); ?>assets/js/core/platform.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/core/config.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/core/integr8formvalidation.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/core/ConnectionDetector.js"></script>
	<!-- END CORE JS SCRIPTS -->
	
	<!-- Unit of measure converter -->
	<script src="<?php echo base_url(); ?>assets/js/unit-converter.js"></script>
	<!-- end Unit of measure converter -->

	<!-- GET DEFAULT IMG JS -->
	<script src="<?php echo base_url(); ?>assets/js/plugins/get_profile_default_img.js"></script>
	<!-- END GET DEFAULT IMG JS -->

	<!-- UI SCRIPT -->
	<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.circliful.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.bxslider.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/plugins/highcharts.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/warehousing.js"></script>
	<!-- END UI SCRIPT -->


	<!-- MODULE SCRIPTS -->
	<script src="<?php echo base_url(); ?>assets/js/modules/products.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/categories.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/consignee.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/storages.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/vessel_list.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/receiving_company_goods.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/receiving_consignee_goods.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/transfer_company_goods.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/transfer_consignee_goods.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/loading_area.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/loading_type.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/withdrawal-consignee.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/withdrawal-company.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/product_inventory.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/users.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/dashboard.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/modules/reports.js"></script>
	<!-- MODULE SCRIPTS -->
	


	<!-- PLUGIN SCRIPTS -->
	<script src="<?php echo base_url(); ?>assets/js/plugins/feedback/js/feedback.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/plugins/cr8v-upload-plugin.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/plugins/cr8v-upload-document.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/plugins/jquery-number.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/plugins/tree.jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/plugins/single_double_click.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.PrintArea.js"></script>

	<!-- PLUGIN SCRIPTS -->

	<!-- EXTRA SCRIPTS -->
	<script src="<?php echo base_url(); ?>assets/js/ui_dropdown_retract.js"></script>
	<!-- END EXTRA SCRIPTS -->


	<script>
		
	</script>
	
	</body>
</html>

<script>
		$("select.transform-dd").transformDD();

		// for donut graph
		// $(".round-graph.wh1").circliful({
  //           showPercent : 0,
  //           percent: 100,
  //           backgroundColor: '#aaa9aa',           
  //           customTopText: '100000',
  //           customHtml : 'MT',
  //           textStyle: 'font-size: 15px; font-weight: 700;',
  //           textColor: '#666',
  //           text: 'Warehouse 1'   
  //       });
  //       $(".round-graph.wh2").circliful({
  //           showPercent : 0,
  //           percent: 50,           
  //           pointSize: 50,
  //           backgroundColor: '#aaa9aa',           
  //           customTopText: '100000',
  //           customHtml : 'MT',
  //           textStyle: 'font-size: 15px; font-weight: 700;',
  //           textColor: '#666',
  //           text: 'Warehouse 2'   
  //       });
  //       $(".round-graph.wh3").circliful({
  //           showPercent : 0,
  //           percent: 50,           
  //           pointSize: 50,
  //           backgroundColor: '#aaa9aa',           
  //           customTopText: '100000',
  //           customHtml : 'MT',
  //           textStyle: 'font-size: 15px; font-weight: 700;',
  //           textColor: '#666',
  //           text: 'Warehouse 3'   
  //       });
		// $(".round-percentage").circliful({           
  //           percent: 50,
  //           backgroundColor: '#aaa9aa', 
  //           textStyle: 'font-size: 12px; font-weight: 700;',
  //           textColor: '#666',
  //           text: 'Silo 1',           
  //       });
  //       $(".round-percentage1").circliful({            
  //           percent: 50,
  //           backgroundColor: '#aaa9aa', 
  //           textStyle: 'font-size: 12px; font-weight: 700;',
  //           textColor: '#666',
  //           text: 'Silo 2'                      
  //       });

        

        // for bar graph
		//$(function () {
    	// Create the chart
	    

	// 	$('.display-graph1').highcharts({
	//         chart: {
	//             type: 'column',
	//             height: 400,
	//             width: 480,
	//             marginTop: 80
	//         },
	//         title: {
	//             text: 'Products Withdrawn'
	//         },
	//         xAxis: {	        
	//             type: 'category'
	//         },
	//         yAxis: {
	//             title: {
	//                 text: ''
	//             }
	//         },
	//         legend: {
	//             enabled: false
	//         },

	//         tooltip: {
	//             headerFormat: '<span style="font-size:14px; font-weight: 700;"> {series.name} </span>',
	//             pointFormat: '<span style="color: #4c8e94; font-size: 14px; font-weight: 700;">{point.y}</span> <b> MT</b>'
	//         },

	//         series: [{
	//             name: 'Products Receivied:',
	//             colorByPoint: true,
	//             data: [{
	//                 name: 'M',
	//                 y: 56.33,
	//                 drilldown: 'Monday'
	//             }, {
	//                 name: 'T',
	//                 y: 50,
	//                 drilldown: 'Tuesday'
	//             }, {
	//                 name: 'W',
	//                 y: 10,
	//                 drilldown: 'Wednesday'
	//             }, {
	//                 name: 'TH',
	//                 y: 41,
	//                 drilldown: 'Thursday'
	//             }, {
	//                 name: 'F',
	//                 y: 36,
	//                 drilldown: 'Friday'
	//             }, {
	//                 name: 'S',
	//                 y: 18,
	//                 drilldown: 'Saturday'
	//             }, {
	//                 name: 'S',
	//                 y: 25,
	//                 drilldown: 'Sunday'
	            
	//             }]
	//         }],
	       
	//     });
	// });
	

     	
      
	</script>