<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.ico">

	<title>Apollo-Warehouse</title>
	
	<!-- jquery ui -->
	<link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">

	<!-- boostraps and more -->
	<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/custom-theme.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/global-theme.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/fullcalendar.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/circliful.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/jquery.bxslider.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="<?php echo base_url(); ?>assets/css/contacts.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/dashboard.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/settings.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/user-account.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/calendar.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/all-deals.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/header.css" rel="stylesheet">

	<!-- core css -->
	<link href="<?php echo base_url(); ?>assets/css/global/commons.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/global/section-content-panel.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/global/ui-widgets.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/global/navi.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/global/unique-widget-style.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/global/extra.css" rel="stylesheet">

	<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>

	<link href="http://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic" rel="stylesheet" type="text/css">

	<!-- Just for debugging purposes. Don't actually copy this line! -->
	<!--[if lt IE 9]><script src="../../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style id="holderjs-style" type="text/css"></style>

	<!-- plugins css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugins/feedback/css/feedback.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/tree.jquery.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css" type='text/css'>
	<!-- end of plugins css -->

	<!-- Override Css -->
	<link href="<?php echo base_url(); ?>assets/css/override.css" rel="stylesheet">
	<!-- end Override Css -->
</head>

<body>