<?php
	
	$user_data = $this->session->userdata('apollo_user');

?>


	<header class="">
		
		<div class="left-content tt-yellow-bg">
			<img src="<?php echo base_url(); ?>assets/images/ui/w-logo.jpg" alt="tiger-tak logo" class="main-logo" />
		</div>
		<h1 class="title-content"><?php echo isset($title)?$title:'';?></h1>			
		<div class="right-content">
			<img src="<?php echo $user_data["image"]; ?>" alt="user profilepic" />
			<p><?php echo $user_data["first_name"]." ".$user_data["last_name"]; ?></p>
			<a class="link  announcement" href="#">				
				<img title="Announcements" alt="Announcements" src="<?php echo base_url(); ?>assets/images/ui/nav_mic.svg"/>
			</a>
			<a class="link gear" href="#">
				<img title="Help" alt="Help" src="<?php echo base_url(); ?>assets/images/ui/nav_question.svg"/>
			</a>
			<a class="link settings" href="#">
				<img title="Settings" alt="Settings" src="<?php echo base_url(); ?>assets/images/ui/nav_gear.svg"/>
			</a>	
		</div>
		<div class="clear"></div>
	</header>	

	<section class="main-section">
		<!-- Log-in / Profile -->
		<div class="settings-box">
			<div class="arrow"></div>
			<div class="top-container text-left">
				<a href="javascript:void(0);">My Profile</a>
				<a href="<?php echo BASEURL."login/unset_user"; ?>">Log-out</a>
			</div>
		</div>

		<!-- for notificatioin -->
		<div class="notif hide">		
			<div class="notif-inside">
				<div class="arrow"></div>
				<div class="border-left-small top-container">
					<p class="f-left font-bold black-color font-16 margin-left-20 padding-top-10 padding-bottom-10">Notification</p>
					<a href="#" class="f-right padding-right-20 padding-top-10 font-14">Mark all as Read</a>
					<div class="clear"></div>
				</div>
				<div class="notif-content">
					<div class="announce-cont unread default-cursor padding-top-10 padding-bottom-10 padding-left-20 bggray-white">
						<img src="<?php echo base_url(); ?>assets/images/profile/profile_h.png" alt="user profile" class="f-left width-50px margin-right-10">
						<div class="f-left text-left margin-top-10 width-80per padding-bottom-5">
							<p><strong>Dennis Murphy</strong> has completed a <strong>Meeting with Jollibee</strong> about Zenhours Inquiry</p>
							<p class=" margin-top-5 gray-color font-12 italic">1 hour ago</p>
						</div>
						<div class="clear"></div>
					</div>
					<div class="announce-cont default-cursor border-top-small padding-top-10 padding-bottom-10 padding-left-20 bggray-white">
						<img src="<?php echo base_url(); ?>assets/images/profile/profile_e.png" alt="user profile" class="f-left width-50px margin-right-10">
						<div class="f-left text-left margin-top-10 width-80per padding-bottom-5">
							<p><strong>Eugene Price</strong> has a Meeting Schedule with <strong>Mark Dobbins about Zenhours Inquiry at 03:00PM</strong></p>
							<p class="margin-top-5 gray-color font-12 italic">5 hour ago</p>
						</div>
						<div class="clear"></div>
					</div>
					<div class="announce-cont unread default-cursor border-top-small padding-top-10 padding-bottom-10 padding-left-20 bggray-white">
						<img src="<?php echo base_url(); ?>assets/images/profile/profile_l.png" alt="user profile" class="f-left width-50px margin-right-10">
						<div class="f-left text-left margin-top-10 width-80per padding-bottom-5">
							<p><strong>Kathleen Gonzales</strong> has send an <strong>email to LonnieJDelgado@armyspy.com</strong> for TigerTak Deal</p>
							<p class="margin-top-5 gray-color font-12 italic">5 hour ago</p>
						</div>
						<div class="clear"></div>
					</div>
					<div class="announce-cont default-cursor border-top-small padding-top-10 padding-bottom-10 padding-left-20 bggray-white">
						<img src="<?php echo base_url(); ?>assets/images/profile/profile_l.png" alt="user profile" class="f-left width-50px margin-right-10">
						<div class="f-left text-left margin-top-10 width-80per padding-bottom-5">
							<p ><strong>Joe Riley</strong> has put Beyong Media's <strong>TigerTak Deal in Closing Status</strong></p>
							<p class="margin-top-5 gray-color font-12 italic">Tuesday, April 14, 2015 at 01:30PM</p>
						</div>
						<div class="clear"></div>
					</div>

					<div class="announce-cont default-cursor border-top-small padding-top-10 padding-bottom-10 padding-left-20 bggray-white">
						<img src="<?php echo base_url(); ?>assets/images/profile/profile_o.png" alt="user profile" class="f-left width-50px margin-right-10">
						<div class="f-left text-left margin-top-10 width-80per padding-bottom-5">
							<p><strong>Kathleen Gonzales</strong> has created a deal with Army Spy titled <strong>"TigerTak Deal"</strong></p>
							<p class="margin-top-5 gray-color font-12 italic">Monday, April 13, 2015 at 12:00PM</p>
						</div>
						<div class="clear"></div>
					</div>
				</div>		
			</div>
		</div>

		

		