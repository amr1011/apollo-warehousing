<div class="modal-container" id="generate-data-view" >
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>


<div class="main min-width-1345">
			<div class="semi-main">

				<div class="text-left  display-inline-top">

					<!-- <div class="icon-dash margin-top-30">
						<div class="icon-item light ">
							<img class="img-responsive" alt="Deals" src="../assets/images/ui/product-big.png">
						</div>
						<div class="icon-item purple">
							<img class="img-responsive" alt="Deals" src="../assets/images/ui/receiving-big.png">
						</div>
						<div class="icon-item orange">
							<img class="img-responsive" alt="Deals" src="../assets/images/ui/transfer-big.png">
						</div>
						<div class="icon-item yellow">
							<img class="img-responsive" alt="Deals" src="../assets/images/ui/withdrawal-big.png">
						</div>
						<div class="icon-item blue">
							<img class="img-responsive" alt="Deals" src="../assets/images/ui/config-big.png">
						</div>
					</div> -->

					<!-- rounded chart  -->
					<div>						
												
						<div class="rounded-chart margin-top-30" id="display-rounded-storages-record">
							
							<div class="rounded display-rounded-storages-item">
								<div class="round-graph " >
								</div>
							</div>
							<!-- <div class="rounded">
								<div class="round-graph wh2 violet" >
								</div>
							</div>
							<div class="rounded">
								<div class="round-graph wh3 blue" >
								</div>
							</div>						
							<div class="rounded">
								<div class="round-percentage design-text violet">
								</div>
							</div>
							<div class="rounded">
								<div class="round-percentage1 design-text violet">
								</div>
							</div> -->
						</div>

					<!-- 	<div class="rounded-chart margin-top-30">						
							<div class="rounded">
								<div class="round-graph wh1 green" >
								</div>
							</div>
							<div class="rounded">
								<div class="round-graph wh2 violet" >
								</div>
							</div>
							<div class="rounded">
								<div class="round-graph wh3 blue" >
								</div>
							</div>						
							<div class="rounded">
								<div class="round-percentage design-text violet">
								</div>
							</div>
							<div class="rounded">
								<div class="round-percentage1 design-text violet">
								</div>
							</div>
						</div> -->

					</div>
					
					<!-- slider report  -->
					<div class="slider-report">
						<div class="title-report">
							<p>Warehouse Movement</p>
							<p id="sliding-this-day">March 09, 2016</p>
							<div class="clear"></div>
						</div>
						
						<div class="slider-container"></div>
					</div>

					<div id="display-graphs-main">

						<!-- bar chart -->
						<div class="bar-chart f-left">
							<div id="display-graph-receiving"></div>						
							<div class="addon-plugin">						
								<div class="select large font-normal bar-drop receiving-drop-graph">
					    			<select class="transform-dd weeks-from-and-now" id="select-receiving-graph">
					    				<!-- <option value="op1" >Week 1 - January 04-10, 2016</option> -->
					    			</select>
					    		</div>
							</div>
						</div>

						<div class="bar-chart f-right">
							<div id="display-graph-withdrawal"></div>						
							<div class="addon-plugin">						
								<div class="select large font-normal bar-drop withdrawal-drop-graph">
					    			<select class="transform-dd weeks-from-and-now"  id="select-withdrawal-graph">
					    				<!-- <option value="op1" >Week 1 - January 04-10, 2016</option> -->
					    			</select>
					    		</div>
							</div>
						</div>

					</div>
					<div class="clear"></div>					
					
					<div class="margin-top-20 padding-all-15 bggray-white box-shadow-light">

						<p class="font-bold font-20">Items nearing Shelf Life</p>	

						<span id="display-main-product-record">

							<div class="box-shadow-dark padding-all-20 margin-top-10 margin-bottom-10 width-50per  display-main-product-item product-item">
								<p class="font-bold font-15 display-product-sku-name">SKU # 1234567890 - American Wheat</p>
								<div class="margin-top-5">
									<p class="f-left font-500 ">Batch Name: <span class="display-product-batch-name">234567890</span></p>
									<p class="f-right red-color font-500 italic display-product-remaining-days" >30 Days Remaining</p>								
									<div class="clear"></div>
								</div>													
							</div>															
						</span>																							
						
					</div>
				</div>					
			</div>
		</div>				
	</section>
	</div>
