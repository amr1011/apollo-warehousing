<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->library('MY_Form_validation');
		$this->load->library('Unit_converter');
        $this->form_validation->CI =& $this;
		$this->load->model('dashboard_model');
		$response = array(
			"status" => false,
			"message" => "message here",
			"data" => null
		);


	}

	public function index()
	{
		$user_session = $this->session->userdata('apollo_user');

		if($user_session === null){
			redirect(BASEURL.'login');
		}
		
		$data['title'] = 'Dashboard';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("dashboard_view", $data);
		$this->load->view("includes/footer");
	}

	public function get_products_shelf_life()
	{
		$get_all_shelf_life = $this->dashboard_model->get_products_shelf_life();
		$arr_store_remaining_life = array();

		foreach ($get_all_shelf_life as $key => $value) {

			$days_left_parameters = array(
				'date_received' => $value['date_received'],
				'product_shelf_life' => $value['product_shelf_life']
			);

			$days_left = $this->calculate_days_remaining($days_left_parameters);

			if($days_left < 31)
			{
				$arr_build_shelf_remaining = array(
						"batch_id"=>$value["batch_id"],
						"batch_name"=>$value["batch_name"],
						"product_name"=>$value["product_name"],
						"sku"=>$value["sku"],
						"product_shelf_life"=>$days_left." Days Remaining"
					);

				array_push($arr_store_remaining_life, $arr_build_shelf_remaining);

			}

		}

		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $arr_store_remaining_life;
		echo json_encode($response);
	}

	public function calculate_days_remaining($params) {
		$this->load->helper('date');
		$date = strtotime($params['date_received']);

		$this_hour = explode(".", $params['product_shelf_life']);

		$new_date = strtotime("+" .$this_hour[0]." days".$this_hour[1]." hours", $date);
		$final_date = date('y-m-d h:i:s', $new_date);
		$this_day = date('y-m-d h:i:s');
		
		$datetime1 = date_create($this_day);
		$datetime2 = date_create($final_date);
		$interval = date_diff($datetime1, $datetime2);
		$final_date = $interval->format('%a');
		return $final_date;

	}

	public function get_storage_information()
	{
		$get_storage = $this->dashboard_model->get_storage_information();
		$arr_collect_all_data = array();

		foreach($get_storage as $key => $value) {
			$get_batches = $this->dashboard_model->get_batch_storage_connection($value["storage_id"]);
			$total = 0;
			$totalTemp = 0;
			foreach ($get_batches as $batchkey => $batchvalue) {
				$sum = 0;
				$sum = $this->unit_converter->convert_measurement($batchvalue["batch_measurement_name"], $value["storage_measurement_name"], $batchvalue["batch_quantity"]);
				$total += $sum;
				$totalTemp +=$batchvalue["batch_quantity"];
			}

			$arr_gather_storage = array(
					"storage_name"=>$value["storage_name"],
					"storage_capacity"=>$value["storage_capacity"],
					"storage_measurement_name"=>$value["storage_measurement_name"],
					"storage_quantity"=>$total

				);
			array_push($arr_collect_all_data, $arr_gather_storage);

		}

		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $arr_collect_all_data;
		echo json_encode($response);
	}

	public function get_warehouse_movement()
	{
		$this_day = date("Y-m-d");
		$display_this_day = date("F d, Y");
		
		$receiving_movement = $this->get_receiving_movement($this_day);
		$withdrawal_movement = $this->get_withdrawal_movement($this_day);
		$transfer_movement = $this->get_transfer_movement($this_day);

		$arr_store_all_warehouse_movement = array(
				"this_day"=>$display_this_day,
				"receiving_info"=>$receiving_movement,
				"withdrawal_info"=>$withdrawal_movement,
				"transfer_info"=>$transfer_movement
			);

		// echo "<pre>";
		// print_r($receiving_movement);
		// echo "</pre>";
		// exit();

		

		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $arr_store_all_warehouse_movement;
		echo json_encode($response);

	}

	public function get_receiving_movement($this_day)
	{
		$receiving_movement = $this->dashboard_model->get_receiving_movement($this_day);

		foreach ($receiving_movement as $key => $value) {
			$origin = "";
			$get_vessel  = "";
			$status = "";
			$consignee_list = "";
			$product_list = "";


			if($value["status"] == "1" || $value["status"] == 1)
			{
				$status = "Completed";
			}else{
				$status = "Ongoing";
			}
			$receiving_movement[$key]["status_name"] = $status;


			if($value["vessel_log_origin"] !== null)
			{
				$get_vessel = $this->dashboard_model->get_vessel_origin($value["vessel_log_origin"]);
			}else{
				$get_vessel = $this->dashboard_model->get_vessel_origin($value["truck_log_origin"]);
			}

			$receiving_movement[$key]["vessel_name"] = $get_vessel[0]["vessel_name"];

			$consignee_info = $this->dashboard_model->get_consignee_info($value["receiving_id"]);
			$product_info = $this->dashboard_model->get_product_info($value["receiving_id"]);
			$quantity_info = $this->dashboard_model->get_quantity_info($value["receiving_id"]);


			$icount_consign = count($consignee_info);
			$icount_consign_list = 0;
			foreach ($consignee_info as $consignkey => $consignvalue) {
				$icount_consign_list++;

				if($icount_consign == $icount_consign_list)
				{
					$consignee_list .= $consignvalue["consignee_name"];
				}else{
					$consignee_list .= $consignvalue["consignee_name"].", ";
				}
				
				
			}
			$receiving_movement[$key]["consignee_info"] = $consignee_list;
			$icount_prod = count($product_info);
			$icount_prod_list = 0;
			foreach ($product_info as $prodkey => $prodvalue) {
				$icount_prod_list++;

				if($icount_prod == $icount_prod_list)
				{
					$product_list .= $prodvalue["product_name"];
				}else{
					$product_list .= $prodvalue["product_name"].", ";
				}

				
			}

			$receiving_movement[$key]["product_info"] = $product_list;
			$total = 0;
			foreach ($quantity_info as $quantitykey => $quantityvalue) {
				$sum = $this->unit_converter->convert_measurement($quantityvalue["batch_measurement_name"], "mt", $quantityvalue["batch_quantity"]);
				$total += $sum;
			}

			$receiving_movement[$key]["total_quantity"] = $total;

			
		}

		return $receiving_movement;
	}

	public function get_transfer_movement($this_day)
	{
		$transfer_movement = $this->dashboard_model->get_transfer_movement($this_day);

		foreach ($transfer_movement as $key => $value) {
			$transfer_movement[$key]["requestor"] = ucwords($value["requestor"]);
			$transfer_movement[$key]["mode_of_transfer_name"] = ucwords($value["mode_of_transfer_name"]);
			$status = "";

			if($value["status"] == "1" || $value["status"] == 1)
			{
				$status = "Completed";
			}else{
				$status = "Ongoing";
			}

			$transfer_movement[$key]["status_name"] = $status;

			$transfer_product = $this->dashboard_model->get_transfer_product_name($value["transfer_id"]);
			$product_names = "";
			$icount_prod = count($transfer_product);
			$icount_prod_list = 0;
			foreach ($transfer_product as $key => $prodvalue) {
				$icount_prod_list++;

				if($icount_prod == $icount_prod_list)
				{
					$product_names .= $prodvalue["product_name"];
				}else{
					$product_names .= $prodvalue["product_name"].", ";
				}
			}
			$transfer_movement[$key]["product_names"] = $product_names;

			$transfer_qty = $this->dashboard_model->get_transfer_product_qty($value["transfer_id"]);
			$total = 0;
			foreach ($transfer_qty as $qtykey => $qtyvalue) {
				$sum = $this->unit_converter->convert_measurement($qtyvalue["unit_of_measure_name"], "mt", $qtyvalue["transfer_qty"]);
				$total += $sum;
			}

			$transfer_movement[$key]["total_quantity"] = $total;

		}

		return $transfer_movement;
	}

	public function get_withdrawal_movement($this_day)
	{
		$get_withdraw_data = $this->dashboard_model->get_withdrawal_movement($this_day);

		if(count($get_withdraw_data) > 0)
		{
			foreach ($get_withdraw_data as $key => $value) {

				$get_atl = $this->dashboard_model->get_withdrawal_consignee_details($value["id"]);
				$alt_number = "";
				$consign_name = "";

				if(count($get_atl) > 0)
				{
					$alt_number = $get_atl[0]["atl_number"];
					$consign_name = $get_atl[0]["consignee_name"];
				}

				$get_withdraw_data[$key]["atl_number"] = $alt_number;
				$get_withdraw_data[$key]["consignee_name"] = $consign_name;

				$get_withdraw_prod = $this->dashboard_model->get_withdrawal_product($value["id"]);
				$total = 0;
				$product_names = "";
				$icount_prod = count($get_withdraw_prod);
				$icount_prod_list = 0;
				foreach ($get_withdraw_prod as $prodkey => $prodvalue) {
					$sum = $this->unit_converter->convert_measurement($prodvalue["withdraw_unit_measure_name"], "mt", $prodvalue["withdraw_qty"]);
					$total += $sum;

					$icount_prod_list++;

					if($icount_prod == $icount_prod_list)
					{
						$product_names .= $prodvalue["product_name"];
					}else{
						$product_names .= $prodvalue["product_name"].", ";
					}
				}

				$get_withdraw_data[$key]["withdaw_product_names"] = $product_names;
				$get_withdraw_data[$key]["withdaw_total_qty"] = $total;

				$status = "";
				if($value["withdrawal_status"] == "1" || $value["withdrawal_status"] == 1)
				{
					$status = "Completed";
				}else{
					$status = "Ongoing";
				}

				$get_withdraw_data[$key]["status_name"] = $status;

				$get_cdr = $this->dashboard_model->get_withdrawal_cdr_details($value["id"]);
				$cdr_number = "N/A";

				if(count($get_cdr) > 0)
				{
					$cdr_number = $get_cdr[0]["cdr_no"];
				}
				$get_withdraw_data[$key]["cdr_number"] = $cdr_number;
				

			}
		}

		return $get_withdraw_data;

	}

	public function get_all_date_graph()
	{

		$arr_get_current_receiving_graph = $this->get_current_receiving_graph();
		$arr_get_current_withdrawal_graph = $this->get_current_withdrawal_graph();


		$arr_gather_all_for_graph = array();

		


		// for 24 hours = 86400
		// for 7days to seconds = 604800
		//  for 6days to seconds = 518400

		$from_feb = strtotime( "February 01, 2016" );
		$get_now = strtotime(date("F d,Y"));
	
		$arr_collect_date = array();
		$count_week = 5;

		

		for($x=$from_feb; $x < $get_now; $x++)
		{
			// if($get_now > $get_first_week_feb)
			// {}
				$get_value = $from_feb;
				$get_start = date("F d", $from_feb);

				$from_feb += 518400;

				$get_end = date("d, Y", $from_feb);

				$build_date = "Week ".$count_week." - ".$get_start." - ".$get_end;


				$arr_build_date = array(
						"weeks"=>$build_date,
						"this_start"=>$get_value
					);

				array_push($arr_collect_date, $arr_build_date);

				$count_week++;

				$from_feb+=86400;

				$x = $from_feb;
		

		}

		$arr_collect_graphs_data = array(
				"current_receiving_graph_now"=>$arr_get_current_receiving_graph,
				"current_withdrawal_graph_now"=>$arr_get_current_withdrawal_graph,
				"weeks_graph_info"=>array_reverse($arr_collect_date)
			);


		array_push($arr_gather_all_for_graph, $arr_collect_graphs_data);


		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $arr_gather_all_for_graph;
		echo json_encode($response);
	}


	public function get_current_receiving_graph()
	{

		$day_today_start = date("l");
		$date_now_start = strtotime(date("F d,Y"));
		$monday_this = "Monday";

		// start
		while($day_today_start != $monday_this)
		{
			$date_now_start -= 86400;

			$day_today_start = date("l", $date_now_start);
		}
		$date_now_start_result = $date_now_start;

		// end 
		$day_today_end = date("l");
		$date_now_end = strtotime(date("F d,Y"));
		$sunday_this = "Sunday";

		while($day_today_end != $sunday_this)
		{
			$date_now_end += 86400;

			$day_today_end = date("l", $date_now_end);
		}
		$date_now_end_result = $date_now_end;



		// $first = new DateTime('first Monday of this month');
		// $this_start = strtotime($first->format('Y-m-d'));

		// // echo strtotime($this_start);

		// $end = new DateTime('first Sunday of this month');
		// $this_end =  strtotime($end->format('Y-m-d'));

		// echo $date_now_start_result."<br />";
		// echo $date_now_end_result;
		// exit();

		$timestamp_receiving = $this->dashboard_model->get_timestamp_receiving($date_now_start_result, $date_now_end_result);
		$monday = 0;
		$tuesday = 0;
		$wednesday = 0;
		$thursday = 0;
		$friday = 0;
		$saturday = 0;
		$sunday = 0;

		foreach ($timestamp_receiving as $key => $value) {
			$day = date("D", $value["timestams_date_created"]);
			
			$sum = $this->unit_converter->convert_measurement($value["unit_of_measure_name"], "mt", $value["receiving_quantity"]);

			if($day=="Mon")
			{
				$monday +=$sum;
			}else if($day=="Tue")
			{
				$tuesday +=$sum;
			}else if($day=="Wed")
			{
				$wednesday +=$sum;
			}else if($day=="Thu")
			{
				$thursday +=$sum;
			}else if($day=="Fri")
			{
				$friday +=$sum;
			}else if($day=="Sat")
			{
				$saturday +=$sum;
			}else if($day=="Sun")
			{
				$sunday +=$sum;
			}
		}

		$for_monday = array(
			"name"=>'M',
			"y"=>$monday,
			"drilldown"=>'Monday'
		);
		$for_tuesday = array(
			"name"=>'T',
			"y"=>$tuesday,
			"drilldown"=>'Tuesday'
		);
		$for_wed = array(
			"name"=>'W',
			"y"=>$wednesday,
			"drilldown"=>'Wednesday'
		);
		$for_thu = array(
			"name"=>'TH',
			"y"=>$thursday,
			"drilldown"=>'Thursday'
		);
		$for_fri = array(
			"name"=>'F',
			"y"=>$friday,
			"drilldown"=>'Friday'
		);
		$for_Sat = array(
			"name"=>'S',
			"y"=>$saturday,
			"drilldown"=>'Saturday'
		);
		$for_Sun = array(
			"name"=>'S',
			"y"=>$sunday,
			"drilldown"=>'Sunday'
		);

		$one_week = array();

		array_push($one_week, $for_monday);
		array_push($one_week, $for_tuesday);
		array_push($one_week, $for_wed);
		array_push($one_week, $for_thu);
		array_push($one_week, $for_fri);
		array_push($one_week, $for_Sat);
		array_push($one_week, $for_Sun);

			  
		return $one_week;
	}

	public function get_seleceted_receiving_graph()
	{

		$date_start = $this->input->post('date_start');

		$this->form_validation->set_rules("date_start", "Date Start", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{

			// start
			$date_now_start_result = $date_start;

			// end 
			$date_now_end_result = intval($date_start) + 518400;


			$timestamp_receiving = $this->dashboard_model->get_timestamp_receiving($date_now_start_result, $date_now_end_result);
			$monday = 0;
			$tuesday = 0;
			$wednesday = 0;
			$thursday = 0;
			$friday = 0;
			$saturday = 0;
			$sunday = 0;

			foreach ($timestamp_receiving as $key => $value) {
				$day = date("D", $value["timestams_date_created"]);
				
				$sum = $this->unit_converter->convert_measurement($value["unit_of_measure_name"], "mt", $value["receiving_quantity"]);

				if($day=="Mon")
				{
					$monday +=$sum;
				}else if($day=="Tue")
				{
					$tuesday +=$sum;
				}else if($day=="Wed")
				{
					$wednesday +=$sum;
				}else if($day=="Thu")
				{
					$thursday +=$sum;
				}else if($day=="Fri")
				{
					$friday +=$sum;
				}else if($day=="Sat")
				{
					$saturday +=$sum;
				}else if($day=="Sun")
				{
					$sunday +=$sum;
				}
			}

			$for_monday = array(
				"name"=>'M',
				"y"=>$monday,
				"drilldown"=>'Monday'
			);
			$for_tuesday = array(
				"name"=>'T',
				"y"=>$tuesday,
				"drilldown"=>'Tuesday'
			);
			$for_wed = array(
				"name"=>'W',
				"y"=>$wednesday,
				"drilldown"=>'Wednesday'
			);
			$for_thu = array(
				"name"=>'TH',
				"y"=>$thursday,
				"drilldown"=>'Thursday'
			);
			$for_fri = array(
				"name"=>'F',
				"y"=>$friday,
				"drilldown"=>'Friday'
			);
			$for_Sat = array(
				"name"=>'S',
				"y"=>$saturday,
				"drilldown"=>'Saturday'
			);
			$for_Sun = array(
				"name"=>'S',
				"y"=>$sunday,
				"drilldown"=>'Sunday'
			);

			$one_week = array();

			array_push($one_week, $for_monday);
			array_push($one_week, $for_tuesday);
			array_push($one_week, $for_wed);
			array_push($one_week, $for_thu);
			array_push($one_week, $for_fri);
			array_push($one_week, $for_Sat);
			array_push($one_week, $for_Sun);

				  
			// return $one_week;




				header("Content-Type: application/json");

				$response["status"] = true;
				$response["message"] = "Success";
				$response["data"] = $one_week;
				echo json_encode($response);
			}

			
	}

	public function get_seleceted_withdrawal_graph()
	{

		$date_start = $this->input->post('date_start');

		$this->form_validation->set_rules("date_start", "Date Start", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{

			// start
			$date_now_start_result = $date_start;

			// end 
			$date_now_end_result = intval($date_start) + 518400;


			$timestamp_withdrawal = $this->dashboard_model->get_timestamp_withdrawal($date_now_start_result, $date_now_end_result);
			$monday = 0;
			$tuesday = 0;
			$wednesday = 0;
			$thursday = 0;
			$friday = 0;
			$saturday = 0;
			$sunday = 0;

			foreach ($timestamp_withdrawal as $key => $value) {
				$day = date("D", $value["timestams_date_created"]);
				
				$sum = $this->unit_converter->convert_measurement($value["unit_of_measure_name"], "mt", $value["withdrawal_quantity"]);

				if($day=="Mon")
				{
					$monday +=$sum;
				}else if($day=="Tue")
				{
					$tuesday +=$sum;
				}else if($day=="Wed")
				{
					$wednesday +=$sum;
				}else if($day=="Thu")
				{
					$thursday +=$sum;
				}else if($day=="Fri")
				{
					$friday +=$sum;
				}else if($day=="Sat")
				{
					$saturday +=$sum;
				}else if($day=="Sun")
				{
					$sunday +=$sum;
				}
			}

			$for_monday = array(
				"name"=>'M',
				"y"=>$monday,
				"drilldown"=>'Monday'
			);
			$for_tuesday = array(
				"name"=>'T',
				"y"=>$tuesday,
				"drilldown"=>'Tuesday'
			);
			$for_wed = array(
				"name"=>'W',
				"y"=>$wednesday,
				"drilldown"=>'Wednesday'
			);
			$for_thu = array(
				"name"=>'TH',
				"y"=>$thursday,
				"drilldown"=>'Thursday'
			);
			$for_fri = array(
				"name"=>'F',
				"y"=>$friday,
				"drilldown"=>'Friday'
			);
			$for_Sat = array(
				"name"=>'S',
				"y"=>$saturday,
				"drilldown"=>'Saturday'
			);
			$for_Sun = array(
				"name"=>'S',
				"y"=>$sunday,
				"drilldown"=>'Sunday'
			);

			$one_week = array();

			array_push($one_week, $for_monday);
			array_push($one_week, $for_tuesday);
			array_push($one_week, $for_wed);
			array_push($one_week, $for_thu);
			array_push($one_week, $for_fri);
			array_push($one_week, $for_Sat);
			array_push($one_week, $for_Sun);
				  
			// return $one_week;




				header("Content-Type: application/json");

				$response["status"] = true;
				$response["message"] = "Success";
				$response["data"] = $one_week;
				echo json_encode($response);
			}

			
	}

	public function get_current_withdrawal_graph()
	{
		$day_today_start = date("l");
		$date_now_start = strtotime(date("F d,Y"));
		$monday_this = "Monday";

		// start
		while($day_today_start != $monday_this)
		{
			$date_now_start -= 86400;

			$day_today_start = date("l", $date_now_start);
		}
		$date_now_start_result = $date_now_start;

		// end 
		$day_today_end = date("l");
		$date_now_end = strtotime(date("F d,Y"));
		$sunday_this = "Sunday";

		while($day_today_end != $sunday_this)
		{
			$date_now_end += 86400;

			$day_today_end = date("l", $date_now_end);
		}
		$date_now_end_result = $date_now_end;


		$timestamp_withdrawal = $this->dashboard_model->get_timestamp_withdrawal($date_now_start_result, $date_now_end_result);
		$monday = 0;
		$tuesday = 0;
		$wednesday = 0;
		$thursday = 0;
		$friday = 0;
		$saturday = 0;
		$sunday = 0;

		foreach ($timestamp_withdrawal as $key => $value) {
			$day = date("D", $value["timestams_date_created"]);
			
			$sum = $this->unit_converter->convert_measurement($value["unit_of_measure_name"], "mt", $value["withdrawal_quantity"]);

			if($day=="Mon")
			{
				$monday +=$sum;
			}else if($day=="Tue")
			{
				$tuesday +=$sum;
			}else if($day=="Wed")
			{
				$wednesday +=$sum;
			}else if($day=="Thu")
			{
				$thursday +=$sum;
			}else if($day=="Fri")
			{
				$friday +=$sum;
			}else if($day=="Sat")
			{
				$saturday +=$sum;
			}else if($day=="Sun")
			{
				$sunday +=$sum;
			}
		}

		$for_monday = array(
			"name"=>'M',
			"y"=>$monday,
			"drilldown"=>'Monday'
		);
		$for_tuesday = array(
			"name"=>'T',
			"y"=>$tuesday,
			"drilldown"=>'Tuesday'
		);
		$for_wed = array(
			"name"=>'W',
			"y"=>$wednesday,
			"drilldown"=>'Wednesday'
		);
		$for_thu = array(
			"name"=>'TH',
			"y"=>$thursday,
			"drilldown"=>'Thursday'
		);
		$for_fri = array(
			"name"=>'F',
			"y"=>$friday,
			"drilldown"=>'Friday'
		);
		$for_Sat = array(
			"name"=>'S',
			"y"=>$saturday,
			"drilldown"=>'Saturday'
		);
		$for_Sun = array(
			"name"=>'S',
			"y"=>$sunday,
			"drilldown"=>'Sunday'
		);

		$one_week = array();

		array_push($one_week, $for_monday);
		array_push($one_week, $for_tuesday);
		array_push($one_week, $for_wed);
		array_push($one_week, $for_thu);
		array_push($one_week, $for_fri);
		array_push($one_week, $for_Sat);
		array_push($one_week, $for_Sun);

			  
		return $one_week;
	}





}

/* End of file Dashboards.php */
/* Location: ./application/modules/dashboards/controllers/Dashboards.php */