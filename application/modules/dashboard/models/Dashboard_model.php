<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	
	public function get_products_shelf_life()
	{
		$sql = "SELECT b.id as batch_id, b.name as batch_name, p.name as product_name, p.sku,
				p.product_shelf_life, DATE_FORMAT(rl.date_received, '%d-%b-%Y') as date_received
				FROM batch b
				LEFT JOIN storage_assignment sa ON sa.id=b.storage_assignment_id
				LEFT JOIN product_inventory_qty piq ON piq.id=sa.product_inventory_qty_id
				LEFT JOIN receiving_log rl ON rl.id=piq.product_receiving_id
				LEFT JOIN product p  ON p.id=piq.product_id
				WHERE rl.status=1 AND b.is_deleted=0 AND sa.is_deleted=0 AND piq.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_storage_information()
	{
		$sql = "SELECT s.id as storage_id, s.name as storage_name, s.capacity as storage_capacity, s.measurement_id as storage_measurement_id, uom.name as storage_measurement_name
				FROM storage s
				LEFT JOIN unit_of_measures uom ON uom.id=s.measurement_id
				ORDER BY s.id ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_batch_storage_connection($storage_id)
	{
		$sql = "SELECT b.name as batch_name, b.quantity as batch_quantity, b.unit_of_measure_id as batch_measure_id, uom.name as batch_measurement_name
				FROM batch b
				LEFT JOIN unit_of_measures uom ON uom.id=b.unit_of_measure_id
				WHERE b.storage_id='{$storage_id}' AND b.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_receiving_movement($this_day)
	{
		$sql = "SELECT rl.id as receiving_id, vrl.origin as vessel_log_origin, trl.origin as truck_log_origin, rl.status, DATE_FORMAT(rl.date_created, '%r') as date_received_time
				FROM receiving_log rl
				LEFT JOIN vessel_receiving_log vrl ON vrl.receiving_log=rl.id
				LEFT JOIN truck_receiving_log trl ON trl.receiving_log_id=rl.id
				WHERE rl.date_created LIKE '{$this_day}%' ORDER BY rl.id DESC
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_vessel_origin($vessel_log_origin)
	{
		$sql = "SELECT name as vessel_name FROM vessel_list WHERE id='{$vessel_log_origin}' AND is_deleted=0 ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_consignee_info($receiving_id)
	{
		$sql = "SELECT c.name as consignee_name
				FROM receiving_consignee_map rcm
				LEFT JOIN consignee c ON c.id=rcm.consignee_id
				WHERE rcm.receiving_log_id='{$receiving_id}'
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_info($receiving_id)
	{
		$sql = "SELECT p.name as product_name
				FROM product_inventory_qty piq
				LEFT JOIN product p ON p.id=piq.product_id
				WHERE piq.product_receiving_id='{$receiving_id}' AND piq.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_quantity_info($receiving_id)
	{
		$sql = "SELECT b.quantity as batch_quantity, b.unit_of_measure_id, uom.name as batch_measurement_name
				FROM product_inventory_qty piq
				LEFT JOIN storage_assignment sa ON sa.product_inventory_qty_id=piq.id
				LEFT JOIN batch b ON b.storage_assignment_id=sa.id
				LEFT JOIN unit_of_measures uom ON uom.id=b.unit_of_measure_id
				WHERE piq.product_receiving_id='{$receiving_id}' AND b.is_deleted=0
				";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_withdrawal_movement($this_day)
	{
		$sql = "SELECT id, status as withdrawal_status, DATE_FORMAT(date_created, '%r') as date_withdraw_time
				FROM withdrawal_log 
				WHERE date_created LIKE '{$this_day}%' AND is_deleted=0 ORDER BY id DESC
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_withdrawal_consignee_details($id)
	{
		$sql = "SELECT wcd.atl_number, c.name as consignee_name
				FROM withdrawal_consignee_details wcd
				LEFT JOIN consignee c ON c.id=wcd.consignee_id
				WHERE wcd.withdrawal_log_id='{$id}'
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_withdrawal_product($id)
	{
		$sql = "SELECT p.name as product_name, wp.qty as withdraw_qty, uom.name as withdraw_unit_measure_name
				FROM withdrawal_product wp
				LEFT JOIN product p ON p.id=wp.product_id
				LEFT JOIN unit_of_measures uom ON uom.id=wp.unit_of_measure_id
				WHERE p.is_deleted=0 AND withdrawal_log_id='{$id}'
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_withdrawal_cdr_details($id)
	{
		$sql = "SELECT cdr_no FROM withdrawal_cdr_details WHERE withdrawal_log_id='{$id}' AND is_deleted=0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfer_movement($this_day)
	{
		$sql = "SELECT tl.id as transfer_id, tl.status, DATE_FORMAT(tl.date_issued, '%r') as date_transfer_time, tr.name as requestor, mot.name as mode_of_transfer_name
				FROM transfer_log tl
				LEFT JOIN transfer_requestor tr ON tr.id=tl.requested_by
				LEFT JOIN mode_of_transfer mot ON mot.id=tl.mode_of_transfer_id
				WHERE tl.date_issued LIKE '{$this_day}%' ORDER BY tl.id DESC
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfer_product_name($transfer_id)
	{
		$sql = "SELECT p.name as product_name
				FROM transfer_product_map tpm
				LEFT JOIN product p ON p.id=tpm.product_id
				WHERE tpm.transfer_log_id='{$transfer_id}' GROUP BY p.name AND tpm.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfer_product_qty($transfer_id)
	{
		$sql = "SELECT b.quantity as transfer_qty, uom.name as unit_of_measure_name
				FROM transfer_product_map tpm
				LEFT JOIN transfer_origin t ON t.product_map_id=tpm.id
				LEFT JOIN batch b ON b.id=t.batch_id
				LEFT JOIN unit_of_measures uom ON uom.id=b.unit_of_measure_id
				WHERE tpm.transfer_log_id='{$transfer_id}' AND b.is_deleted=0 AND tpm.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_timestamp_receiving($this_start, $this_end)
	{
		$sql = "SELECT rl.id as receiving_log_id, UNIX_TIMESTAMP(rl.date_created) as timestams_date_created, b.quantity as receiving_quantity, uom.name as unit_of_measure_name
				FROM receiving_log rl
				LEFT JOIN storage_assignment sa ON sa.receiving_log_id=rl.id
				LEFT JOIN batch b ON b.storage_assignment_id=sa.id
				LEFT JOIN unit_of_measures uom ON uom.id=b.unit_of_measure_id
				WHERE ((UNIX_TIMESTAMP(rl.date_created)>='$this_start') AND (UNIX_TIMESTAMP(rl.date_created)<='$this_end')) AND b.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_timestamp_withdrawal($this_start, $this_end)
	{
		$sql = "SELECT wp.qty as withdrawal_quantity,  UNIX_TIMESTAMP(wl.date_created) as timestams_date_created, uom.name as unit_of_measure_name
				FROM withdrawal_log wl
				LEFT JOIN withdrawal_product wp ON wp.withdrawal_log_id=wl.id
				LEFT JOIN unit_of_measures uom ON uom.id=wp.unit_of_measure_id
				WHERE ((UNIX_TIMESTAMP(wl.date_created)>='$this_start') AND (UNIX_TIMESTAMP(wl.date_created)<='$this_end')) AND wl.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	

}

/* End of file Dashboard_model.php */
/* Location: ./application/modules/dashboard/models/Dashboard_model.php */