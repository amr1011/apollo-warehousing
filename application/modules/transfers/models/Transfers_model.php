<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transfers_model extends CI_Model {

	//==================================================//
	//================COMPANY TRANSFERS=================//
	//==================================================//

	public function get_all_company_transfers() {
		$sql = "SELECT *
		FROM transfer_log
		WHERE type = 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_list($transfer_log_id) {
		$sql = "SELECT *
		FROM transfer_product_map
		WHERE transfer_log_id = $transfer_log_id
		AND is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_details($product_id) {
		$sql = "SELECT *
		FROM product
		WHERE id = $product_id";
		$query = $this->db->query($sql);
		// return $query->result_array();
		return $query->row();
	}

	public function get_transfer_number() {
		$sql = "SELECT to_number
		FROM transfer_log 
		ORDER BY id 
		DESC LIMIT 1";
		$query = $this->db->query($sql);
		return $query->row("to_number");
	}

	public function get_mode_of_transfer() {
		$sql = "SELECT *
		FROM mode_of_transfer";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function add_company_transfer($transfer_details) {
		$this->db->insert('transfer_log', $transfer_details);
		return $this->db->insert_id();
	}

	public function add_transfer_note($note_details) {
		$this->db->insert('transfer_notes', $note_details);
		return $this->db->affected_rows();
	}

	public function get_all_company_transfer_notes($transfer_log_id) {
		$sql = "SELECT *
		FROM transfer_notes
		WHERE transfer_log_id = $transfer_log_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_unit_of_measures() {
		$sql = "SELECT *
		FROM unit_of_measures";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_worker_type() {
		$sql = "SELECT *
		FROM worker_type";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function complete_company_transfer($transfer_log_id, $complete_details) {
		$this->db->where('id', $transfer_log_id);
		$query = $this->db->update('transfer_log', $complete_details);
		return $this->db->affected_rows();
		// print_r($complete_details);
	}

	public function add_worker($worker_details) {
		$this->db->insert('worker_list', $worker_details);
		return $this->db->insert_id();
	}

	public function company_add_worker($worker_details)
	{
		$this->db->insert('worker_list', $worker_details);
		return $this->db->insert_id();
	}

	public function add_equipment($equipment_details) {
		$this->db->insert('equipment_list', $equipment_details);
		return $this->db->insert_id();
	}

	public function company_add_equipment($equipment_details) {
		$this->db->insert('equipment_list', $equipment_details);
		return $this->db->insert_id();
	}

	public function get_transfer_batches($transfer_log_id) {
		$sql = "SELECT *
		FROM transfer_product_map
		WHERE transfer_log_id = $transfer_log_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfer_equipments($transfer_log_id) {
		$sql = "SELECT *
		FROM equipment_list
		WHERE record_log_id = $transfer_log_id
		AND record_type = 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfer_workers($transfer_log_id) {
		$sql = "SELECT w.id, w.name as name, t.name as designation
		FROM worker_list w
		LEFT JOIN worker_type t ON  t.id = w.worker_type
		WHERE w.record_log_id = $transfer_log_id
		AND w.record_type = 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_batch($product_map_id) {
		$sql = "SELECT *
		FROM transfer_origin
		WHERE product_map_id = $product_map_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfer_documents($transfer_log_id) {
		$sql = "SELECT *
		FROM transfer_documents
		WHERE transfer_log_id = $transfer_log_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	// public function add_company_transfer_origin($transfer_origin_details) {
	// 	$query = $this->db->insert('transfer_origin', $transfer_origin_details);
	// 	return $this->db->affected_rows();
	// }

	public function add_document($data)
	{
		$this->db->insert("transfer_documents", $data);
		return $this->db->insert_id();
	}

	public function add_transfer_product_map($data) {
		$this->db->insert("transfer_product_map",$data);
		return $this->db->insert_id();
	}

	public function add_transfer_origin($data) {
		$this->db->insert("transfer_origin", $data);
		return $this->db->insert_id();
	}
	

	// public function get_product_batches($product_id) {
	// 	$sql = "SELECT b.id as batch_id,
	// 	b.name as batch_name, b.quantity as batch_quantity,
	// 	b.storage_assignment_id as storage_assignment_id,
	// 	um.name as unit_of_measure, um.id as unit_of_measure_id,
	// 	piq.qty as inventory_quantity, piq.loading_method as inventory_loading_method,
	// 	piq.bag as inventory_bag, piq.unit_of_measure_id as inventory_unit_of_measure,
	// 	l.name as location_name, l.storage_id as location_storage_id,
	// 	l.parent_id as location_parent_id, l.id as location_id,
	// 	s.storage_id as storage_id, s.name as storage_name,
	// 	s.id as storage_id_number,
	// 	s.address as storage_address, s.description as storage_description,
	// 	s.capacity as storage_capacity, v.name as vendor_name,
	// 	p.id as product_id
	// 	FROM batch b
	// 	LEFT JOIN unit_of_measures um ON um.id = b.unit_of_measure_id
	// 	LEFT JOIN storage_assignment sa ON sa.id = b.storage_assignment_id
	// 	LEFT JOIN receiving_log rl ON rl.id = sa.receiving_log_id
	// 	LEFT JOIN location l ON l.id = b.location_id
	// 	LEFT JOIN storage s ON s.id = b.storage_id
	// 	LEFT JOIN product_inventory_qty piq ON piq.id = sa.product_inventory_qty_id
	// 	LEFT JOIN vendor v ON v.id = piq.vendor_id
	// 	LEFT JOIN product p ON p.id = piq.id
	// 	WHERE p.id = $product_id
	// 	AND rl.type = 1
	// 	AND b.is_deleted = 0
	// 	AND piq.is_deleted = 0";
	// 	$query = $this->db->query($sql);
	// 	return $query->result_array();
	// }

	public function get_product_batches($product_id) {
		$sql = "SELECT b.id as batch_id,
		b.name as batch_name, b.quantity as batch_quantity,
		b.storage_assignment_id as storage_assignment_id,
		piq.qty as inventory_quantity, piq.loading_method as inventory_loading_method,
		piq.bag as inventory_bag, piq.unit_of_measure_id as inventory_unit_of_measure,
		l.name as location_name, l.storage_id as location_storage_id,
		l.parent_id as location_parent_id, l.id as location_id,
		s.storage_id as storage_id, s.name as storage_name,
		s.id as storage_id_number,
		s.address as storage_address, s.description as storage_description,
		s.capacity as storage_capacity,
		uom.name as unit_of_measure, uom.id as unit_of_measure_id
		FROM batch b
		LEFT JOIN unit_of_measures uom ON uom.id = b.unit_of_measure_id
		LEFT JOIN location l ON l.id = b.location_id
		LEFT JOIN storage s ON s.id = b.storage_id
		LEFT JOIN storage_assignment sa ON sa.id = b.storage_assignment_id
		LEFT JOIN receiving_log rl ON rl.id = sa.receiving_log_id
		LEFT JOIN product_inventory_qty piq ON piq.id = sa.product_inventory_qty_id
		LEFT JOIN product p ON p.id = piq.product_id
		WHERE p.id = $product_id
		AND rl.type = 1
		AND b.is_deleted = 0
		AND piq.is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_batches() {
		$sql = "SELECT *
		FROM batch b
		WHERE b.is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_locations() {
		$sql = "SELECT *
		FROM location";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function batch_delete($batch_id) {
		$update_details = array(
			"is_deleted" => 1
		);

		$this->db->where('id', $batch_id);
		$query = $this->db->update('batch', $update_details);
		return $this->db->affected_rows();
	}

	public function batch_transfer($batch_details) {
		$query = $this->db->insert('batch', $batch_details);
		return $this->db->insert_id();
	}

	public function get_company_product_transfers($transfer_log_id) {
		$sql = "SELECT *
		FROM transfer_product_map
		WHERE transfer_log_id = $transfer_log_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfer_origins($product_map_id) {
		$sql = "SELECT *
		FROM transfer_origin
		WHERE product_map_id = $product_map_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function company_get_transfer_details($transfer_log_id) {
		$sql = "SELECT *
		FROM transfer_log
		WHERE id = $transfer_log_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function company_get_ongoing_transfers($transfer_log_id) {
		$sql = "SELECT *
		FROM transfer_product_map
		WHERE transfer_log_id = $transfer_log_id
		AND is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function company_get_ongoing_batch($product_map_id) {
		$sql = "SELECT tn.id, tn.product_map_id, tn.batch_id, tn.storage_location,
		tn.quantity, tn.unit_of_measure, tn.status, tn.destination_container, tn.origin_container,
		b.name as batch_name,
		um.name as unit_of_measure_name
		FROM transfer_origin tn
		LEFT JOIN batch b ON b.id = tn.batch_id
		LEFT JOIN unit_of_measures um ON um.id = tn.unit_of_measure
		WHERE tn.product_map_id = $product_map_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function company_delete_transfer_batch($transfer_product_map_id) {
		$sql = "UPDATE transfer_product_map
		SET is_deleted = 1
		WHERE id = $transfer_product_map_id";
		$query = $this->db->query($sql); 
		return $this->db->affected_rows();	
	}

	public function company_edit_transfer_details($transfer_log_id, $edit_transfer_details) {
		$this->db->where('id', $transfer_log_id);
		$this->db->update('transfer_log', $edit_transfer_details);
		return $this->db->affected_rows();
	}

	// GETS ALL THE PRODUCT MAPPING FOR THE CHOSEN TRANSFER.
	public function company_get_product_map($transfer_log_id) {
		$sql = "SELECT tpm.id as map_id, tpm.product_id as map_product_id,
		tpm.transfer_log_id as map_transfer_log_id, tpm.transfer_method as map_transfer_method,
		tpm.is_deleted as map_is_deleted,
		p.id as product_id, p.name as product_name, 
		p.sku as product_sku, p.image as product_image
		FROM transfer_product_map tpm
		LEFT JOIN product p ON p.id = tpm.product_id
		WHERE tpm.transfer_log_id = $transfer_log_id
		AND tpm.is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	// GETS THE TRANSFER BATCH DETAILS. 
	public function company_get_transfer_batch($product_map_id) {
		$sql = "SELECT tn.id, tn.product_map_id, tn.batch_id, tn.storage_location,
		tn.quantity, tn.unit_of_measure, tn.status, tn.destination_container, tn.origin_container,
		b.name as batch_name,
		um.name as unit_of_measure_name
		FROM transfer_origin tn
		LEFT JOIN batch b ON b.id = tn.batch_id
		LEFT JOIN unit_of_measures um ON um.id = tn.unit_of_measure
		WHERE tn.product_map_id = $product_map_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfer_record($transfer_log_id) 
	{
		$sql = "SELECT *
		FROM transfer_log
		WHERE id = $transfer_log_id";
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function get_transfer_products($transfer_log_id) 
	{
		$sql = "SELECT tpm.id as map_id, tpm.product_id as map_product_id, 
		tpm.transfer_log_id as map_transfer_log_id, tpm.transfer_method as map_transfer_method, 
		tpm.is_deleted map_is_deleted, p.name as product_name,
		p.sku as product_sku, p.description as product_description,
		p.image as product_image
		FROM transfer_product_map tpm
		LEFT JOIN product p ON p.id = tpm.product_id
		WHERE tpm.transfer_log_id = $transfer_log_id
		AND tpm.is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfer_batch_list($product_map_id) {
		// $sql = "SELECT *
		// FROM transfer_origin
		// WHERE product_map_id = $product_map_id";

		$sql = "SELECT origin.id, origin.product_map_id, origin.batch_id,
		origin.storage_location, origin.quantity, origin.unit_of_measure,
		origin.status, origin.destination_container, origin.origin_container,
		b.name as batch_name
		FROM transfer_origin origin
		LEFT JOIN batch b ON b.id = origin.batch_id
		WHERE product_map_id = $product_map_id";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfer_notes($transfer_log_id) {
		$sql = "SELECT *
		FROM transfer_notes
		WHERE transfer_log_id = $transfer_log_id
		AND is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_workers($transfer_log_id) {
		$sql = "SELECT wl.id, wl.record_log_id, wl.name,
		wl.worker_type, wt.name as designation
		FROM worker_list wl
		LEFT JOIN worker_type wt ON wt.id = wl.worker_type
		WHERE record_log_id = $transfer_log_id
		AND record_type = 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_equipments($transfer_log_id) {
		$sql = "SELECT *
		FROM equipment_list
		WHERE record_log_id = $transfer_log_id
		AND record_type = 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function company_add_new_batch($transfer_details) {
		$this->db->insert('transfer_origin', $transfer_details);
		return $this->db->insert_id();
	}

	//==================================================//
	//============END OF COMPANY TRANSFERS==============//
	//==================================================//

	// START OF TRANSFER FOR CONSIGNEE

	public function get_documents($transfer_log_id)
	{
		$sql = "SELECT * FROM transfer_documents WHERE transfer_log_id = ".$transfer_log_id;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_requestor($where = ""){
		$query = "SELECT * FROM transfer_requestor WHERE name='{$where}' ";
		$sql = $this->db->query($query);
		return $sql->result_array();
	}

	public function add_requestor($data){
		$this->db->insert('transfer_requestor', $data);
		return $this->db->insert_id();
	}

	public function add_transfer_consignee_goods($transfers_data)
	{
		$this->db->insert('transfer_log', $transfers_data);
		return $this->db->insert_id();
	}

	public function add_notes($data){
		$this->db->insert('transfer_notes', $data);
		return $this->db->insert_id();
	}

	public function get_notes($where){
		$sql = "SELECT * FROM transfer_notes ".$where;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_consignee_products()
	{
		$sql = "SELECT piq.product_id as id, p.name as name, p.sku, p.description, p.image, piq.id as product_inventory_id 
			 	FROM product_inventory_qty piq
				LEFT JOIN receiving_log rl ON rl.id=piq.product_receiving_id
				LEFT JOIN product p ON p.id=piq.product_id
				WHERE  rl.type = 2 AND piq.is_deleted=0 AND p.is_deleted=0 GROUP BY piq.product_id";

		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function get_all_consignee_products_for_edit()
	{
		$sql = "SELECT piq.product_id as id, p.name as name, p.sku, p.description, p.image, piq.id as product_inventory_id, 
				piq.qty as product_qty, piq.loading_method, piq.unit_of_measure_id
			 	FROM product_inventory_qty piq
				LEFT JOIN receiving_log rl ON rl.id=piq.product_receiving_id
				LEFT JOIN product p ON p.id=piq.product_id
				WHERE  rl.type = 2 AND piq.is_deleted=0 AND p.is_deleted=0 GROUP BY piq.product_id";

		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function get_all_batch_consignee(){
		$sql = "SELECT b.id as batch_id,piq.product_id, b.name, s.name as storage_name,
				 b.location_id, b.quantity, b.unit_of_measure_id, uom.name as unit_of_measure_name
			 	FROM product_inventory_qty piq
				LEFT JOIN receiving_log rl ON rl.id=piq.product_receiving_id
				LEFT JOIN storage_assignment sa ON sa.product_inventory_qty_id = piq.id
				LEFT JOIN batch b ON b.storage_assignment_id = sa.id
				LEFT JOIN storage s ON s.id=b.storage_id
				LEFT JOIN unit_of_measures uom ON uom.id=b.unit_of_measure_id
				WHERE rl.type = 2 AND b.name<>'' AND b.is_deleted=0 ORDER BY b.name ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_storage_id($location_id)
	{
		$sql = "SELECT storage_id FROM location WHERE id='{$location_id}' ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_unit_measure_id($measurement)
	{
		$sql = "SELECT id FROM unit_of_measures WHERE name='{$measurement}' ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function save_new_transfer_consignee_origin($new_data)
	{
		$this->db->insert("transfer_origin", $new_data);
		return $this->db->insert_id();
	}

	public function remove_selected_batch($batch_id)
	{
		$sql = "UPDATE batch SET is_deleted=1 WHERE id='{$batch_id}' ";
		$query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function get_selected_batch($batch_id)
	{
		$sql = "SELECT storage_assignment_id, name FROM batch WHERE id='{$batch_id}' ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function replace_old_batch($collet_new_data)
	{
		$this->db->insert("batch", $collet_new_data);
		return $this->db->insert_id();
	}

	public function get_transfer_data($transfer_id)
	{
		$sql = "SELECT t.id, t.to_number, t.reason_for_transfer as reason, t.date_issued, mf.name as mode_of_transfer, tr.name as requestor,
				t.vessel_origin_id, t.mode_of_transfer_id
				FROM transfer_log t
				LEFT JOIN mode_of_transfer mf ON mf.id=t.mode_of_transfer_id
				LEFT JOIN transfer_requestor tr ON tr.id=t.requested_by
				WHERE t.type=2 AND  t.id='{$transfer_id}' AND t.status=0 ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfers_prod($transfer_id)
	{
		$sql = "SELECT p.id as product_id, p.sku, p.name as product_name, p.image, p.description, t.unit_of_measure,
				uom.name as unit_measure_name,  tpm.transfer_method
				FROM transfer_origin t
				LEFT JOIN transfer_product_map tpm ON tpm.id=t.product_map_id
				LEFT JOIN product p ON p.id=tpm.product_id
				LEFT JOIN unit_of_measures uom ON uom.id=t.unit_of_measure
				WHERE tpm.transfer_log_id='{$transfer_id}' AND tpm.is_deleted=0 GROUP BY tpm.product_id ORDER BY tpm.id DESC
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfer_origin($transfer_id, $product_id)
	{
		$sql = "SELECT t.batch_id, b.name as batch_name, t.quantity, uom.name as unit_of_measure_name, t.unit_of_measure,
				t.destination_container, t.origin_container
				FROM transfer_origin t
				LEFT JOIN batch b ON b.id=t.batch_id
				LEFT JOIN unit_of_measures uom ON uom.id=t.unit_of_measure
				LEFT JOIN transfer_product_map tpm ON tpm.id=t.product_map_id
				WHERE tpm.transfer_log_id='{$transfer_id}' AND tpm.product_id='{$product_id}' AND tpm.is_deleted=0
			";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function save_new_transfer_product_map($data)
	{
		$this->db->insert("transfer_product_map", $data);
		return $this->db->insert_id();
	}

	public function remove_old_consign_transfer_product_map($transfer_id, $product_id, $arr_set_is_deleted)
	{
		$where = "transfer_log_id='{$transfer_id}' AND product_id='{$product_id}' ";
		$this->db->where($where);
		$this->db->update("transfer_product_map", $arr_set_is_deleted);
	}

	public function get_transfer_product_map($transfer_id)
	{
		$sql = "SELECT product_id FROM transfer_product_map WHERE transfer_log_id='{$transfer_id}' AND is_deleted=0 ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function remove_transfer_consignee_old_products($transfer_id, $product_id, $arr_is_deleted)
	{
		$where = "product_id='{$product_id}' AND transfer_log_id='{$transfer_id}' ";
		$this->db->where($where);
		$this->db->update("transfer_product_map", $arr_is_deleted);
		return $this->db->affected_rows();
	}

	public function update_transfer_consignee_goods($transfer_id, $arr_update_transfer)
	{
		$this->db->where("id", $transfer_id);
		$this->db->update("transfer_log", $arr_update_transfer);
		return $this->db->affected_rows();
	}

	public function get_prod_maps_to_complete($transfer_id)
	{
		$sql = "SELECT id FROM transfer_product_map WHERE transfer_log_id='{$transfer_id}' AND is_deleted=0 ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function update_origin_consignee($id, $arr_complete)
	{
		$this->db->where("product_map_id", $id);
		$this->db->update("transfer_origin", $arr_complete);
		return $this->db->affected_rows();
	}

	public function get_worker_type($where = ""){
		$query = "SELECT * FROM worker_type WHERE name='{$where}' ";
		$sql = $this->db->query($query);
		return $sql->result_array();
	}

	public function add_worker_type($data){
		$this->db->insert('worker_type', $data);
		return $this->db->insert_id();
	}

	public function add_worker_list($arr_worker_list){
		$this->db->insert('worker_list', $arr_worker_list);
		return $this->db->insert_id();
	}


	public function add_equipment_list($arr_equipment_list){
		$this->db->insert('equipment_list', $arr_equipment_list);
		return $this->db->insert_id();
	}

	public function get_transfer_consign_complete_data($transfer_id)
	{
		$sql = "SELECT tr.name as requestor, mot.name as mode_of_transfer_name, t.to_number, t.date_issued, t.date_start, t.date_complete, t.reason_for_transfer
				FROM transfer_log t
				LEFT JOIN transfer_requestor tr ON tr.id=t.requested_by
				LEFT JOIN mode_of_transfer mot ON mot.id=t.mode_of_transfer_id
				WHERE t.id='{$transfer_id}' AND t.type=2 AND t.status=1
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_workers_consignee_complete_data($transfer_id)
	{
		$sql = "SELECT wl.name as workers_name, wt.name as workers_type
				FROM worker_list wl
				LEFT JOIN worker_type wt ON wt.id=wl.worker_type
				WHERE wl.record_log_id='{$transfer_id}' AND wl.record_type=1 GROUP BY wl.name
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_equipment_consignee_complete_data($transfer_id)
	{
		$sql = "SELECT name FROM equipment_list WHERE record_log_id='{$transfer_id}' AND record_type=1 ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_documents_consignee_complete_data($transfer_id)
	{
		$sql = "SELECT document_name, document_path, date_added
				FROM transfer_documents WHERE transfer_log_id='{$transfer_id}'
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_notes_consignee_complete_data($transfer_id)
	{
		$sql = "SELECT note, date_created, subject
				FROM transfer_notes WHERE transfer_log_id='{$transfer_id}' AND is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_consignee_complete_data($transfer_id)
	{
		$sql = "SELECT tpm.id, tpm.product_id, p.sku, p.name as product_name, p.description, tpm.transfer_method, p.image, uof.name as unit_measure_name
				FROM transfer_product_map tpm 
				LEFT JOIN transfer_origin t ON t.product_map_id=tpm.id
				LEFT JOIN product p ON p.id=tpm.product_id
				LEFT JOIN unit_of_measures uof ON uof.id=t.unit_of_measure
				WHERE tpm.transfer_log_id='{$transfer_id}' AND tpm.is_deleted=0 AND p.is_deleted=0
				GROUP BY tpm.product_id
				";
		$query = $this->db->query($sql);
		return $query->result_array();		
	}

	public function get_origin_consignee_complete_data($transfer_id, $product_id)
	{
		$sql = "SELECT uof.name as unit_of_measure_name, tr.quantity, tr.storage_location, tr.unit_of_measure, tr.batch_id,b.name as batch_name,
				tr.destination_container, tr.origin_container
				FROM transfer_product_map tpm
				LEFT JOIN transfer_origin tr ON tr.product_map_id=tpm.id
				LEFT JOIN transfer_log tl ON tl.id=tpm.transfer_log_id
				LEFT JOIN unit_of_measures uof ON uof.id=tr.unit_of_measure
				LEFT JOIN batch b ON b.id=tr.batch_id
				WHERE tpm.transfer_log_id='{$transfer_id}' AND tpm.product_id='{$product_id}' AND tl.status=1 AND tpm.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();	
	}

	public function get_all_transfer_consignee_records()
	{
		$sql = "SELECT id, to_number, date_issued, status FROM transfer_log WHERE type=2 ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_transfer_product_map_all($transfer_id)
	{
		$sql = "SELECT p.name as product_name, p.sku
				FROM transfer_product_map tpm
				LEFT JOIN product p ON p.id=tpm.product_id
				WHERE tpm.transfer_log_id='{$transfer_id}' AND tpm.is_deleted=0 AND p.is_deleted=0
				GROUP BY tpm.product_id
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}



	//=============================== END OF TRANSFER FOR CONSIGNEE =================================//
}

/* End of file transfers_model.php */
/* Location: ./application/modules/transfers/models/transfers_model.php */