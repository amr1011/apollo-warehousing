<div class="main">
	<div class="center-main">
		<div class="mod-select padding-left-3 padding-top-60">			
			<div class="select f-left width-425px ">
				<select>
					<option value="Displaying All Consignee Transfer Records"  html=""></option>
					<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
					<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
					<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
					<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
					<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
				</select>
			</div>						
		</div>
		<div class="f-right margin-top-10">	
			<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn dropdown-btn">View Filter / Search</button>
			<a href="<?php echo BASEURL; ?>transfers/create_transfer">
				<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn">Create Record</button>				
			</a>				
		</div>
		<div class="clear"></div>

		<div class="dropdown-search">
			<div class="triangle padding-right-130">
				<i class="fa fa-caret-up"></i>
			</div>
			<table>
				<tbody>
					<tr class="width-100percent">
						<td class="width-75px">Search</td>
						<td>
							<div class="select width-200px font-400">
								<select>
									<option value="items">Items</option>
								</select>
							</div>
						</td>
						<td class="width-100percent">
							<input type="text" />
						</td>
					</tr>
					<tr class="width-100percent">
						<td>Filter By:</td>
						<td>
							<div class="input-group width-200px f-left italic">
							<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar white-color">
							</span></span>
							</div>
						</td>
						<td class="search-action">
							<div class="f-left">
								
								<div class="select width-150px italic">
									<select>
										<option value="status">Status</option>
									</select>
								</div>
							</div>
							<div class="f-right margin-left-30">
								<p class="display-inline-mid">Sort By:</p>
								<div class="select width-150px display-inline-mid italic">
									<select>
										<option value="withdrawal No." >Transfer No.</option>
									</select>
								</div>
								<div class="select width-150px display-inline-mid italic">
									<select>
										<option value="ascending">Ascending</option>
										<option value="descending">Descending</option>
									</select>
								</div>
							</div>
							<div class="clear"></div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="f-left margin-top-10 margin-left-10">
				<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
				<p class="display-inline-mid font-4 font-bold">Save Search Result</p>
			</div>
			<button type="button" class="btn general-btn font-bold f-right margin-top-10 ">Search</button>
			<div class="clear"></div>
		</div>


		<div class="width-100percent bggray-white">

			<table class="tbl-4c3h margin-top-30">
				<thead>
					<tr>
						<th class="width-15percent black-color">Transfer No.</th>
						<th class="width-15percent black-color">Transfer Date</th>
						<th class="width-40percent black-color">Item Description</th>
						<th class="width-15percent black-color">Owner</th>
						<th class="width-15percent black-color">Status</th>												
					</tr>
				</thead>
			</table>
			<div class="tbl-like hover-transfer-tbl">
				<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left">
					<p class="width-15per font-400 font-14 f-none display-inline-mid text-center">1234567890</p>						
					<p class="width-15percent font-400 font-14 f-none display-inline-mid text-center">22-Oct-2015</p>
					<div class="width-40per display-inline-mid font-400 text-center">
						<ul class="list-style-square text-left text-indent-20 padding-left-20 font-14">
							<li>#12345678 - cement</li>
							<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
							<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
							<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
							<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
							<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
							<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
							<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
							<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
						</ul>
					</div>
					<div class="width-15percent font-400 display-inline-mid text-center">
						<div class="owner-img width-30per display-inline-mid ">
							<img src="<?php echo BASEURL; ?>assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
						</div>
						<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
					</div>
					<p class="width-14percent font-400 font-14 f-none display-inline-mid text-center">Ongoing</p>							
				</div>
				<div class="hover-tbl "> 
					<a href="view-transfer-complete.php display-inline-mid">
						<button class="btn general-btn margin-right-10">View Record</button>
					</a>
					<a href="" class="display-inline-mid">
						<button class="btn general-btn ">Complete Transfer</button>
					</a>	
				</div>
			</div>

			<div class="tbl-like hover-transfer-tbl">
				<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left">
					<p class="width-15per font-400 font-14 f-none display-inline-mid text-center">1234567890</p>						
					<p class="width-15percent font-400 font-14 f-none display-inline-mid text-center">22-Oct-2015</p>
					<div class="width-40per display-inline-mid font-400 text-center">
						<ul class="list-style-square text-left text-indent-20 padding-left-20 font-14">
							<li>#12345678 - cement</li>
							<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
						</ul>
					</div>
					<div class="width-15percent font-400 display-inline-mid text-center">
						<div class="owner-img width-30per display-inline-mid ">
							<img src="<?php echo BASEURL; ?>assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
						</div>
						<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
					</div>
					<p class="width-14percent font-400 font-14 f-none display-inline-mid text-center">Ongoing</p>							
				</div>
				<div class="hover-tbl "> 
					<a href="view-transfer-complete.php display-inline-mid">
						<button class="btn general-btn margin-right-10">View Record</button>
					</a>
					<a href="" class="display-inline-mid">
						<button class="btn general-btn ">Complete Transfer</button>
					</a>	
				</div>
			</div>
			<div class="tbl-like hover-transfer-tbl">
				<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left">
					<p class="width-15per font-400 font-14 f-none display-inline-mid text-center">1234567890</p>						
					<p class="width-15percent font-400 font-14 f-none display-inline-mid text-center">22-Oct-2015</p>
					<div class="width-40per display-inline-mid font-400 text-center">
						<ul class="list-style-square text-left text-indent-20 padding-left-20 font-14">
							<li>#12345678 - cement</li>
							<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
						</ul>
					</div>
					<div class="width-15percent font-400 display-inline-mid text-center">
						<div class="owner-img width-30per display-inline-mid ">
							<img src="<?php echo BASEURL; ?>assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
						</div>
						<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
					</div>
					<p class="width-14percent font-400 font-14 f-none display-inline-mid text-center">Ongoing</p>							
				</div>
				<div class="hover-tbl "> 
					<a href="view-transfer-complete.php display-inline-mid">
						<button class="btn general-btn margin-right-10">View Record</button>
					</a>
					<a href="" class="display-inline-mid">
						<button class="btn general-btn ">Complete Transfer</button>
					</a>	
				</div>
			</div>
			<div class="tbl-like hover-transfer-tbl">
				<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left">
					<p class="width-15per font-400 font-14 f-none display-inline-mid text-center">1234567890</p>						
					<p class="width-15percent font-400 font-14 f-none display-inline-mid text-center">22-Oct-2015</p>
					<div class="width-40per display-inline-mid font-400 text-center">
						<ul class="list-style-square text-left text-indent-20 padding-left-20 font-14">
							<li>#12345678 - cement</li>
							<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
						</ul>
					</div>
					<div class="width-15percent font-400 display-inline-mid text-center">
						<div class="owner-img width-30per display-inline-mid ">
							<img src="<?php echo BASEURL; ?>assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
						</div>
						<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
					</div>
					<p class="width-14percent font-400 font-14 f-none display-inline-mid text-center">Ongoing</p>							
				</div>
				<div class="hover-tbl "> 
					<a href="view-transfer-complete.php display-inline-mid">
						<button class="btn general-btn margin-right-10">View Record</button>
					</a>
					<a href="" class="display-inline-mid">
						<button class="btn general-btn ">Complete Transfer</button>
					</a>	
				</div>
			</div>
			<div class="tbl-like hover-transfer-tbl">
				<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left">
					<p class="width-15per font-400 font-14 f-none display-inline-mid text-center">1234567890</p>						
					<p class="width-15percent font-400 font-14 f-none display-inline-mid text-center">22-Oct-2015</p>
					<div class="width-40per display-inline-mid font-400 text-center">
						<ul class="list-style-square text-left text-indent-20 padding-left-20 font-14">
							<li>#12345678 - cement</li>
							<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
						</ul>
					</div>
					<div class="width-15percent font-400 display-inline-mid text-center">
						<div class="owner-img width-30per display-inline-mid ">
							<img src="<?php echo BASEURL; ?>assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
						</div>
						<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
					</div>
					<p class="width-14percent font-400 font-14 f-none display-inline-mid text-center">Ongoing</p>							
				</div>
				<div class="hover-tbl "> 
					<a href="view-transfer-complete.php display-inline-mid">
						<button class="btn general-btn margin-right-10">View Record</button>
					</a>
					<a href="" class="display-inline-mid">
						<button class="btn general-btn ">Complete Transfer</button>
					</a>	
				</div>
			</div>
		</div>



	</div>
</div>