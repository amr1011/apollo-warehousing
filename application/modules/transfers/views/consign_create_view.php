<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL; ?>transfers/consign_transfer">Transfer - Consignee Goods</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color">Create Record</li>
				</ul>
			</div>
			<div class="semi-main">
				<div class="padding-top-30 margin-bottom-30 text-right width-100percent">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color back-to-transfer" >Cancel</button>
					<button class="btn general-btn modal-trigger" modal-target="confirm-create-record">Create Record</button>
				</div>
				<div class="clear"></div>
				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0">
					<p class="font-20 font-bold black-color padding-bottom-20" id="display-transfer-number">Transfer No. 1234567890</p>
					
					<div class="width-50percent display-inline-top">
						<div class="padding-bottom-10">
							<p class="font-14 font-bold display-inline-mid width-120">Requested By: </p>
							<input type="text" class="display-inline-mid width-340 t-small" id="set-transfer-requested">
						</div>

						<div class="padding-top-10">
							<p class="font-14 font-bold display-inline-top width-120">Reason: </p>
							<textarea class="width-340" id="set-transfer-reason"></textarea>
						</div>

					</div>

					<div class="width-50percent display-inline-top">
						<div class="padding-bottom-10">
							<p class="font-14 font-bold display-inline-mid width-150">Origin Vessel:</p>
							<div class="select large ">
								<select class="transform-dd origin-vessel-dropdown">
									<!-- <option value="">Select Origin Vessel</option> -->
								</select>
							</div>
						</div>

						<div class="padding-bottom-10">
							<p class="font-14 font-bold display-inline-mid width-150">Mode Of Transfer: </p>
							<div class="select large">
								<select class="transform-dd mode-of-transfer">
									<!-- <option value="">Select Mode of Transfer</option> -->
								</select>
							</div>
						</div>
					</div>
					
				</div>

				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white padding-bottom-20 ">
					<div class="width-100percent text-left padding-all-30 no-padding-top no-padding-left margin-bottom-30 border-bottom-small border-gray">
						<p class="font-20 font-bold black-color padding-bottom-20 ">Item List</p>
						<p class="font-14 font-bold display-inline-mid padding-right-20">Item: <span class="red-color display-inline-top">*</span></p>
						<div class="select dispaly-inline-mid large setProductListSelect">
							<select class="transform-dd product-list-dropdown">
								<!-- <option value="">Select Item</option> -->
							</select>
						</div>
						<a href="#" class="display-inline-mid padding-left-20">
							<button class="btn general-btn padding-left-20 padding-right-20" id="add-selected-product">Add Item</button>
						</a>
					</div>

					<span id="displaySelectedProduct">
						<div class="border-full padding-all-20 item-display-product item-record record-item" style="display: none; margin-top: 10px; width: 100%;">
						<div class="bggray-white height-190px">
							<div class="height-100percent display-inline-mid width-230px">
								<img src="../assets/images/corn.jpg" alt="" class="height-100percent width-100percent set-product-image">
							</div>

							<div class="display-inline-top width-75percent padding-left-10">
								<div class="padding-all-15 bg-light-gray">
									<p class="font-16 font-bold f-left set-product-sku-and-name">SKU# 1234567890 - Japanese Corn</p>
									<div class="f-right width-20px margin-left-10">
										<img src="<?php echo BASEURL; ?>assets/images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor deleteItem">
									</div>
									<div class="clear"></div>
								</div>
								<!-- <div class="padding-all-15 text-left">
									<p class="f-left no-margin-all width-20percent font-bold">Vendor</p>
									<p class="f-left no-margin-all width-20percent set-product-vendor">Innova Farming</p>
									<p class="f-left no-margin-all width-20percent font-bold">Transfer Method</p>
									<div class="f-left">
										<input type="radio" class="display-inline-mid width-50px default-cursor set-product-by-bulk" name="bag-or-bulk" id="byBulk">
										<label for="byBulk" class="font-14 font-400 display-inline-mid padding-top-2 default-cursor">By Bulk</label>
									</div>
									<div class="f-left">
										<input type="radio" class="display-inline-mid width-50px default-cursor set-product-bay-bags" name="bag-or-bulk" id="byBags" >
										<label for="byBags" class="font-14 font-400 display-inline-mid padding-top-2 default-cursor">By Bags</label>
									</div>							
									<div class="clear"></div>
								</div>
								
								
								<div class="padding-all-15 bg-light-gray text-left font-0">
									<p class="display-inline-mid width-20percent font-bold">Aging Days</p>
									<p class="display-inline-mid width-20percent set-product-aging-days">10 Days</p>
									<p class="display-inline-mid width-20percent font-14 font-bold">Qty to Transfer</p>
									<input type="text" class="display-inline-mid width-70px t-small set-product-quantity" name="product-quantity">
									<div class="select font-12 display-inline-mid width-50px margin-left-5 margin-right-15 unit-remove-dropdown">
										<select class="padding-all-5 default-cursor unit-of-measure">
										</select>
									</div>
									<input type="text" class="display-inline-mid width-70px t-small" name="bag-or-bulk" >
									<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">Bags</p>
									<div class="clear"></div>
								</div> -->
								
								
								<div class="padding-all-15 text-left">
									<p class="f-left width-20percent font-bold">Description</p>
									<p class="f-left set-product-description">Very Good Corn</p>
									<div class="clear"></div>
								</div>
								<div class="padding-all-15 bg-light-gray text-left">
									<p class="f-left no-margin-all width-20percent font-bold">Transfer Method</p>
									<div class="f-left">
										<input type="radio" class="display-inline-mid width-50px default-cursor set-product-by-bulk method-select-radio" name="bag-or-bulk" value="bulk" checked="checked" id="byBulk">
										<label for="byBulk" class="font-14 font-400 display-inline-mid padding-top-2 default-cursor set-product-for-bulk">By Bulk</label>
									</div>
									<div class="f-left">
										<input type="radio" class="display-inline-mid width-50px default-cursor set-product-by-bags method-select-radio" name="bag-or-bulk" value="bag" id="byBags" >
										<label for="byBags" class="font-14 font-400 display-inline-mid padding-top-2 default-cursor set-product-for-bags">By Bags</label>
									</div>							
									<div class="clear"></div>
								</div>

								<div class="padding-all-15  text-left font-0">
									<p class="f-left width-20percent font-bold">Unit</p>

									<div class="select font-12 display-inline-mid width-50px margin-left-5 margin-right-15 unit-remove-dropdown">
										<select class="padding-all-5 default-cursor unit-of-measure">
										</select>
									</div>
									
									<div class="clear"></div>
								</div>

								
							</div>
						</div>

						<div class="padding-top-20">
							<div class="padding-top-20 padding-bottom-10">
								<p class="font-20 font-500 f-left">Item Location</p>
								<div class="f-right">
									<button class="btn general-btn  modal-trigger" modal-target="add-batch">Add Batch</button>
								</div>
								<div class="clear"></div>
							
							</div>

							<span class="display-new-batches-origin">
								<span  class="product-location display-new-locations" style="display: none;">
									<div class="f-left">
										<p class="display-inline-mid font-14 font-bold padding-right-20 padding-top-10">Batch Name:</p>
										<p class="display-inline-mid font-14 font-400 padding-top-10 display-new-batch-name"></p>
									</div>
									<p class="f-right font-14 font-bold padding-bottom-20 display-new-quantity">Quantity 50 KG</p>
									<div class="clear"></div>
									<table class="tbl-4c3h">
										<thead>
											<tr>
												<th class="width-50percent black-color">Origin</th>
												<th class="width-50percent">Destination</th>
											</tr>
										</thead>
									</table>

									
										<div class=" font-0 tbl-dark-color margin-bottom-20 position-rel" >
											<div class="width-100percent ">
												<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25 display-batch-origin">
													<!-- <p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
													<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
													<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
													<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
													<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>
													<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
													<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
													<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
													<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
													<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
													<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p> -->
												</div>

												<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25 display-batch-destination">
													<!-- <p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
													<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
													<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
													<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
													<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
													<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
													<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
													<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
													<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
													<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
													<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p> -->
												</div>
											</div>
											<div class="edit-hide" style="height: 100%; padding: 20px 0;">
												<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid edit-this-batch"  modal-target="edit-batch">Edit Batch</button>
												<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 remove-this-batch" >Remove Batch</button>
											</div>
										</div>
								</span>
							</span>
							
							

							
						

						</div>
					</div>
					</span>
					

				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title padding-top-10 font-500 font-22 black-color"> Documents</h4>
							</a>

							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="upload-documents">Upload</button>
							</div>
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-20">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>
								<span id="displayDocuments"></span>	

								<!-- <div class="table-content position-rel">
									<div class="content-show padding-all-10 tbl-dark-color">
										<div class="width-85per display-inline-mid">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">WithdrawalDocument</p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<button class="btn general-btn padding-left-30 padding-right-30">View</button>
										<button class="btn general-btn padding-left-30 padding-right-30">Remove</button>	
									</div>
								</div>	 -->							
							</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-20 black-color padding-top-10"> Notes</h4>
							</a>
							<div class="f-right">
								<button id="AddReceivingConsigneeNote" class="btn general-btn " >Add Notes</button>
							</div>
							<div class="clear"></div>
						</div>
						
					
						
						<div class="panel-collapse collapse in">
							<div id="AddConsigneeNotesDisplay" class="panel-body padding-all-20">
								
							</div>
						</div>
					</div>
				</div>

				<div class="width-100percent text-right border-top-small border-gray padding-top-10">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color back-to-transfer">Cancel</button>
					<button class="btn general-btn modal-trigger" modal-target="confirm-create-record">Create Record</button>
				</div>
		</div>
	</div>

	<!--MODALS-->

	<!--Start of Confirm Create Record MODAL-->
	<div class="modal-container" modal-id="created-record" id="created-record-consignee-modal">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Create Record</h4>				
				<!-- <div class="modal-close close-me" id="close-success-modal"></div> -->
			</div>

			<!-- content -->
			<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
				<div class="padding-all-10 bggray-7cace5">
					<p class="font-14 font-400 white-color">Transfer No. <span id="display-success-transfer-number"></span> has been created.</p>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<a href="<?php echo BASEURL ?>transfers/consignee_ongoing" id="create-new-link">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" >Show Record</button>
				</a>
			</div>
		</div>	
	</div>
	<!--End of Confirm Create Record MODAL-->

	<!--Start of Confirm Create Record MODAL-->
	<div class="modal-container" modal-id="confirm-create-record">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Create Record</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<p class="font-14 font-400">Are you sure you want to create Transfer No. <span id="confirm-transfer-number">1234567890</span>?</p>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" id="save-new-transfer-data" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" >Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Confirm Create Record MODAL-->

	

	<!--Start of Upload Document MODAL-->
	<div class="modal-container" modal-id="upload-document">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Upload Document</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">Dcoument Name:</p>
					<input type="text" class="width-231px display-inline-mid">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">File Location:</p>
					<input type="text" class="width-231px display-inline-mid">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Browse</button>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Upload Document MODAL-->

	<!--Start of Add Notes MODAL-->
	<div class="modal-container" id="show-modal-add-notes" modal-id="add-notes">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Add Notes</h4>				
				<div class="modal-close close-me"></div>
			</div>
			<form id="addReceivingConsigneeNoteForm">
				<!-- content -->
				<div class="modal-content text-left">
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
						<input type="text" id="AddReceivingConsigneeNoteSubject" datavalid="required"  class="width-380px display-inline-mid margin-left-10">
					</div>
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
						<textarea id="AddReceivingConsigneeNoteMessage" datavalid="required" class="width-380px margin-left-10 height-250px"></textarea>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="addRecievingConsigneeNotesSave" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
				</div>
			</form>
		</div>	
	</div>
	<!--End of Add Notes MODAL-->

	<!--Start of Add Batch MODAL-->
	<div class="modal-container" modal-id="add-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left" id="set-batch-title">Add Batch</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<div class="padding-bottom-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Item:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid display-batch-product-name-sku">SKU No. 1234567890 - Japanese Corn</p>
					</div>

					<div class="padding-bottom-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per margin-top-15">Batch Name: </p>
						<div class="select large trigger-batch-dropdown">
							<select class="transform-dd set-batch-name">
								
							</select>
						</div>
					</div>

					<div class="padding-bottom-10 padding-top-10">

						<p class="font-14 font-bold no-margin-left">Origin</p>

						<p class="font-14 font-500 display-inline-mid width-25per no-margin-right margin-top-15">Storage Location:</p>
						<!-- <p class="font-14 font-400 no-margin-left display-inline-mid display-batch-storage-location">Location Here</p> -->

						<!-- <p class="font-14 font-500 display-inline-mid width-25per no-margin-right margin-top-15">Storage Location:</p>
						<div class="select large">
							<select class="transform-dd">
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div> -->
						
					</div>
				</div>
				<!-- <p class="font-14 font-bold no-margin-right">Sub Location:</p> -->
				<div class="width-100percent bggray-white box-shadow-dark ">
					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-65percent" style="text-align: left;">Location Address</th>
								<th class="black-color width-20percent">Location Qty</th>
								
							</tr>
						</thead>
					</table>
					<div class="bg-light-gray font-0 text-center position-rel default-cursor padding-right ">
						<div id="location-item" class="width-65percent display-inline-mid padding-all-20" style="text-align: left;">
							<!-- <p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p> -->
							<div class="clear"></div>
						</div>
						<div class="width-20percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all " id="set-new-batch-qty" style="text-align: right;">1000 KG</p>
						</div>

					</div>
					
				</div>

				<p class="font-14 font-bold no-margin-left margin-top-20">Destination</p>
				<p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>

				<div class="bggray-white height-300px overflow-y-auto">
	                <div class="padding-all-10 height-50px padding-bottom-10 border-bottom-small border-black" id="breadCrumbsHierarchy">
	                </div>
	                <div class="font-0" id="batchDisplay">
	                </div>
	            </div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color ">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" id="confirmAddBatch">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Batch MODAL-->


	<!--Start of edit Batch MODAL-->
	<div class="modal-container" modal-id="edit-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Edit Batch</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<div class="padding-bottom-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Item:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid edit-batch-product-name-sku">SKU No. 1234567890 - Japanese Corn</p>
					</div>

					<div class="padding-bottom-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per margin-top-15">Batch Name: </p>
						<div class="select large trigger-batch-dropdown">
							<select class="transform-dd set-edit-batch-name">
								
							</select>
						</div>
					</div>

					<div class="padding-bottom-10 padding-top-10">

						<p class="font-14 font-bold no-margin-left">Origin</p>

						<p class="font-14 font-500 display-inline-mid width-25per no-margin-right margin-top-15">Storage Location:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid edit-batch-storage-location">Location Here</p>

						<!-- <p class="font-14 font-500 display-inline-mid width-25per no-margin-right margin-top-15">Storage Location:</p>
						<div class="select large">
							<select class="transform-dd">
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div> -->
						
					</div>
				</div>
				<p class="font-14 font-bold no-margin-right">Sub Location:</p>
				<div class="width-100percent bggray-white box-shadow-dark ">
					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-65percent" style="text-align: left;">Location Address</th>
								<th class="black-color width-20percent">Location Qty</th>
								
							</tr>
						</thead>
					</table>
					<div class="bg-light-gray font-0 text-center position-rel default-cursor padding-right ">
						<div id="" class="width-65percent display-inline-mid padding-all-20" style="text-align: left;">
							<!-- <p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p> -->
							<div class="clear"></div>
						</div>
						<div class="width-20percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all " id="set-edit-batch-qty" style="text-align: right;">1000 KG</p>
						</div>

					</div>
					
				</div>

				<p class="font-14 font-bold no-margin-left margin-top-20">Destination</p>
				<p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>

				<div class="bggray-white height-300px overflow-y-auto">
	                <div class="padding-all-10 height-50px padding-bottom-10 border-bottom-small border-black" id="breadCrumbsHierarchy">
	                </div>
	                <div class="font-0" id="batchDisplay">
	                </div>
	            </div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color ">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" id="confirmAddBatch">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of edit Batch MODAL-->