<div class="modal-container" id="generate-data-view" >
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>


<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL ?>transfers/consign_transfer">Transfer - Consignee Goods</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color" id="display-ongoing-transfer-number">Transfer No. 1234567890</li>
				</ul>
			</div>
			<div class="semi-main">
				<div class="f-right  margin-bottom-30">
					<button class="btn general-btn modal-trigger complete-transfer-btn" modal-target="view-complete-transfer">Complete Transfer</button>
				</div>
				<div class="clear"></div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white">
					<div class="panel-group">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">								
								<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10  active"> Transfer Information</h4>
							</a>					
							<!-- <a href="consignee-view-vessel-edit.php" class="edit-enter-product"> -->
								<button class="btn general-btn f-right width-100px transfer-edit-information">Edit</button>			

							<div class="clear"></div>
							<!-- </a> -->																	
						</div>

						<div class="panel-collapse collapse in ">
							<div class="panel-body">
								
								<div class="display-inline-mid width-50per">					
									<div class="text-left padding-bottom-10">
										<p class="f-left font-14 font-bold width-40percent">Mode Of Transfer:</p>
										<p class="f-left font-14 font-400 width-50percent" id="display-ongoing-transfer-mode">Manual Labor</p>
										<div class="clear"></div>
									</div>

									<div class="text-left padding-top-10">
										<p class="f-left font-14 font-bold width-40percent">Reason For Transfer:</p>
										<p class="f-left font-14 font-400 width-50percent" id="display-ongoing-transfer-reason">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretrium tempor. Ut eget imperdiet neque.</p>
										<div class="clear"></div>
									</div>


								</div>

								<div class="display-inline-top width-50per">
									<div class="text-left padding-bottom-10">
										<p class="f-left font-14 font-bold width-40percent">Date And Time Issued:</p>
										<p class="f-left font-14 font-400 width-50percent" id="display-ongoing-transfer-date-issued">September 10, 2015 | 08:25 AM</p>
										<div class="clear"></div>
									</div>

									<div class="text-left height-50px ">
										<p class="f-left font-14 font-bold width-40percent padding-top-5 margin-top-3">Requested By:</p>
										<div class="position-rel display-inline-mid text-center height-65percent">
											<img src="../assets/images/profile/profile_x.png" id="display-ongoing-transfer-requested-img" alt="images" class="height-100percent display-inline-mid">
											<p class="display-inline-mid font-14 font-400" id="display-ongoing-transfer-requested">Julie Mendez</p>
										</div>
										
										<div class="clear"></div>
									</div>

									<div class="text-left">
										<p class="f-left font-14 font-bold width-40percent">Status:</p>
										<p class="f-left font-14 font-400 width-50percent">Ongoing</p>
										<div class="clear"></div>
									</div>

								</div>
								<div class="width-100per margin-top-30 padding-top-10 text-left border-top-light">
								

									<div class="panel-body padding-all-10">
										<div class="padding-left-10 padding-top-10 padding-right-10 padding-bottom-10 bggray-middark font-0 gray-color">
											<p class="no-margin-all font-14 font-bold width-25percent display-inline-mid padding-left-10 ">Transfer Date / Time Start</p>
											<p class="no-margin-all font-14 width-26-6percent display-inline-mid ">Not Yet Assigned</p>
											<p class="no-margin-all font-14 font-bold width-25percent display-inline-mid ">Transfer Duration</p>
											<p class="no-margin-all font-14 width-23-4percent display-inline-mid ">Not Yet Assigned</p>
										</div>

										<div class="padding-left-10 padding-top-10 padding-right-10 padding-bottom-10 width-50percent font-0 no-padding-right gray-color">
											<p class="no-margin-all font-14 font-bold width-50percent display-inline-mid padding-left-10">Transfer Date / Time End</p>
											<p class="no-margin-all font-14 width-50percent display-inline-mid">Not Yet Assigned</p>
											
										</div>

										<div class="font-0">
											<div class="width-48percent f-left">
												<div class=" padding-bottom-10 padding-left-10 padding-top-10 padding-right-10 padding-bottom-10 bggray-middark">
													<h4 class="font-20 font-500 transfer-information display-inline-mid width-90per  gray-color">Workers List</h4>
												</div>
												<div class="padding-left-10 padding-top-10 padding-right-10 padding-bottom-10 font-0 ">
													<p class="no-margin-all font-14 font-400 italic padding-left-10 gray-color">No Workers Logged</p>
												</div>
									
											</div>

											<div class="width-48percent f-right ">
												<div class="padding-left-10 padding-top-10 padding-right-10 padding-bottom-10 bggray-middark">
													<h4 class="font-20 font-500 gray-color">Equipment Used</h4>
												</div>
												<div class="padding-left-10 padding-top-10 padding-right-10 padding-bottom-10 font-0 ">
													<p class="no-margin-all font-14 font-400 italic gray-color">No Equipment Used</p>
												</div>
											</div>
											<div class="clear"></div>
										</div>
									</div>
									<div class="clear"></div>									
								</div>
							</div>
						</div>
					</div>						
				</div>
				

				<span id="display-all-products">
					<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white display-this-item item-record record-item ">
						<div class="head panel-group text-left">
							<div class="panel-heading font-14 font-400 margin-top-5 margin-bottom-5">
								<a class="colapsed black-color " href="javascript:void(0)">
									<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10 set-product-sku-name"> SKU #1234567890 - Japanese Corn</h4>
								</a>

								<div class=" f-right width-200px text-right">
									<button type="button" class="font-12 btn btn-default font-12 display-inline-mid red-color transfer-hide-cancel">Cancel</button>		
									<button class="btn general-btn display-inline-mid min-width-100px transfer-ongoing-btn">Edit</button>						
								</div>
							</div>
							<div class="panel-collapse collapse ">
								<div class="panel-body padding-all-20">
									<div class="bggray-white border-full padding-all-20 height-190px box-shadow-dark">
										<div class="height-100percent display-inline-mid width-20per text-center">
											<img src="../assets/images/corn.jpg"  alt="" class="height-100percent width-100percent set-product-image">
										</div>

										<div class="display-inline-top width-80per padding-left-10">

											<div class="padding-all-15 bg-light-gray">
												<p class="display-inline-mid width-150px font-bold">Description</p>
												<p class="display-inline-mid set-product-description">Ang Cement na matibay</p>
												<div class="clear"></div>
											</div>
											
											<div class="padding-all-15 ">
												
												<p class="display-inline-mid width-150px font-bold">Transfer Method</p>
												<p class="display-inline-mid transfer-display-piece set-product-method">By Piece</p>

												<div class="display-inline-mid transfer-hide-piece" >
													<div class="display-inline-mid ">										
														<input type="radio" class="display-inline-mid width-20px default-cursor set-product-by-bulk" name="bag-or-bulk" id="byBulk" value="bulk">
														<label for="byBulk" class="display-inline-mid font-14 margin-top-5 default-cursor set-product-for-bulk">By Bulk</label>
													</div>

													<div class="display-inline-mid  margin-left-10">																	
														<input type="radio" class="display-inline-mid width-20px default-cursor set-product-by-bags" name="bag-or-bulk" id="byBags" value="bag" >
														<label for="byBags" class="display-inline-mid font-14 margin-top-5 default-cursor set-product-for-bags">By Bag</label>
													</div>
													<div class="clear"></div>
												</div>

												<div class="clear"></div>
											</div>
											
											
											<div class="padding-all-15">
												<p class="display-inline-mid width-125px font-bold qty-to-transfer">Unit of Measure</p>
												<p class="display-inline-mid margin-left-35 transfer-display-piece unit-to-transfer">KG</p>
												<div class="display-inline-mid margin-left-25 transfer-hide-piece">
													<div class="display-inline-mid text-left font-0">											
														
														<div class="select font-12 display-inline-mid width-50px  margin-right-15 unit-remove-dropdown">
															<select class="padding-all-5 default-cursor unit-of-measure">
															</select>
														</div>
														
													
														<div class="clear"></div>												
													</div>
												</div>
												<div class="clear"></div>
											</div>

										</div>
									</div>

									<div class="padding-top-20">
										<div class="margin-bottom-10 margin-top-10">
											<p class=" f-left no-margin-all font-20 font-500  padding-left-10">Item Location</p>
											<button class="btn general-btn f-right  modal-trigger margin-bottom-10 transfer-add-batch" modal-target="add-batch">Add Batch</button>
											<div class="clear"></div>
										</div>


										<span class="display-new-batches-origin">
											<span  class="product-location display-new-locations" >
												<div class="f-left">
													<p class="display-inline-mid font-14 font-bold padding-right-20 padding-top-10">Batch Name:</p>
													<p class="display-inline-mid font-14 font-400 padding-top-10 display-new-batch-name"></p>
												</div>
												<p class="f-right font-14 font-bold padding-bottom-20 display-new-quantity">Quantity 50 KG</p>
												<div class="clear"></div>
												<table class="tbl-4c3h">
													<thead>
														<tr>
															<th class="width-50percent black-color">Origin</th>
															<th class="width-50percent">Destination</th>
														</tr>
													</thead>
												</table>
													<div class=" font-0 tbl-dark-color margin-bottom-20 position-rel" >
														<div class="width-100percent ">
															<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25 display-batch-origin"></div>
															<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25 display-batch-destination"></div>
														</div>
														<div class="edit-hide" style="height: 100%; padding: 20px 0;">
															<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid edit-this-batch"  modal-target="edit-batch">Edit Batch</button>
															<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 remove-this-batch" >Remove Batch</button>
														</div>
													</div>
											</span>
										</span>

									</div>
								</div>
							</div>
						</div>
					</div>

				</span>

		

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-20 black-color padding-top-10"> Documents</h4>
							</a>
							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="upload-documents">Upload a Document</button>
							</div>
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-10">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>
								<span id="displayDocuments"></span>	

																
							</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<h4 class="panel-title font-500 font-20 black-color padding-top-10"> Notes</h4>
							</a>
							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="add-note">Add Notes</button>
							</div>
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-10" id="display-transfer-notes">

								<div class="border-full padding-all-10 margin-left-18  margin-top-10 display-transfer-notes-row" >
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400 display-transfer-notes-subject">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400 display-transfer-notes-date-created">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10 display-transfer-notes-message">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="javascript:void(0)" class="f-right padding-all-10 font-400">Show Less</a>
									<div class="clear"></div>
								</div>
						
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>

	<!--Start of view complete transfer modal-->
	<div class="modal-container modal-transfer" modal-id="view-complete-transfer">
		<div class="modal-body xlarge" style="margin-top: 80px;">				

			<div class="modal-head">
				<h4 class="text-left">Complete Transfer</h4>				
				<div class="modal-close close-me transfer-close"></div>
			</div>
			<!-- content -->
			<div class="modal-content text-left">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time Start :</p>
					
					<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
						<input class="form-control dp  default-cursor " id="transfer-date-start" placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon width-0"><i class="fa fa-calendar"></i></span>
					</div>
					
					<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
						<input class="form-control dp time " placeholder="Transfer Time" id="transfer-time-start" type="text">
						<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
					</div>
				
				</div>

				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time End :</p>
					
					<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
						<input class="form-control dp border-big-bl border-big-tl default-cursor " id="transfer-date-end" placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon width-0"><i class="fa fa-calendar "></i></span>
					</div>

					<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
						<input class="form-control dp time" placeholder="Transfer Time" id="transfer-time-end" type="text">
						<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
					</div>
				</div>

				<div class="width-100percent">
					<div class="width-50per f-left">
						<div class="padding-bottom-10 padding-top-10">
							<p class="font-14 font-bold f-left no-margin-all">Worker List</p>
							<a href="javascript:void(0)" id="add-worker-btn" class="display-inline-mid font-14 f-right"><i class="fa fa-user-plus font-14 padding-right-5"></i> Add Worker</a>
							
							<div class="clear"></div>
						</div>
						<div id="add-worker-list" class="bggray-white padding-top-10 padding-bottom-10 height-300px">
							<div  class="border-bottom-small border-black padding-top-10 padding-bottom-10 margin-left-10 add-some-worker">
								<input type="text" class="display-inline-mid width-45per worker-name" placeholder="Name">
								<input type="text" class="display-inline-mid width-45per no-margin-left worker-destination" placeholder="Designation">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor remove-worker">
								</div>
							</div>
						</div>
					</div>

					<div class="width-50per f-right">
						<div class="padding-bottom-10 padding-top-10">
							<p class="font-14 font-bold f-left no-margin-all">Equipment Used</p>
							<a href="javascript:void(0)" id="add-equipment-btn" class="f-right font-14"><i class="fa fa-truck font-16 padding-right-5"></i> Add Equipment</a>
							<div class="clear"></div>
						</div>
						<div id="add-equip-list" class="bggray-white padding-top-10 padding-bottom-10 height-300px">
							<div class="border-bottom-small border-black padding-top-10 padding-bottom-10 margin-left-10 add-some-equip">
								<input type="text" class="display-inline-mid width-90per equip-name" placeholder="Equipment Name">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor remove-equipment">
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<a href="javascript:void(0)" >
					<button type="button" id="save-complete-transfer-consignee" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 ">Complete Loading</button>
				</a>
			</div>
		</div>	
	</div>
	<!--End of view complete transfer modal-->

	<!--Start completed transfer modal-->
	<div class="modal-container" id="completed-transfer-consignee-modal" modal-id="completed-transfer">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Complete Transfer</h4>				
				<div class="modal-close close-me" id="close-success-modal"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
				<div class="padding-all-10 bggray-7cace5">
					<p class="font-14 font-400 white-color">Transfer Details has been saved. Transfer Record is now complete.</p>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<a href="<?php echo BASEURL; ?>transfers/consign_complete">
					<button type="button"  class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" >Show Record</button>
				</a>
			</div>
		</div>	
	</div>

	<!--End of completed transfer modal-->

	<!--Start of Upload Document MODAL-->
	<div class="modal-container" modal-id="upload-documents">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Upload Document</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">Dcoument Name:</p>
					<input type="text" class="width-231px display-inline-mid">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">File Location:</p>
					<input type="text" class="width-231px display-inline-mid">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Browse</button>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Upload Document MODAL-->

	<!--Start of Add Notes MODAL-->
	<div class="modal-container" modal-id="add-note">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Add Notes</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<form id="addTransferNoteForm">
				<!-- content -->
				<div class="modal-content text-left">
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
						<input type="text" id="addNoteSubject" datavalid="required" class="width-380px display-inline-mid margin-left-10">
					</div>
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
						<textarea id="addNoteMessage" datavalid="required" class="width-380px margin-left-10 height-250px"></textarea>
					</div>
				</div>
			</form>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" id="add-notes-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Notes MODAL-->


	<!--Start of Add Batch MODAL-->
	<div class="modal-container" modal-id="add-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left" id="set-batch-title">Add Batch</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<div class="padding-bottom-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Item:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid display-batch-product-name-sku">SKU No. 1234567890 - Japanese Corn</p>
					</div>

					<div class="padding-bottom-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per margin-top-15">Batch Name: </p>
						<div class="select large trigger-batch-dropdown">
							<select class="transform-dd set-batch-name">
								
							</select>
						</div>
					</div>

					<div class="padding-bottom-10 padding-top-10">

						<p class="font-14 font-bold no-margin-left">Origin</p>

						<p class="font-14 font-500 display-inline-mid width-25per no-margin-right margin-top-15">Storage Location:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid display-batch-storage-location">Location Here</p>

						<!-- <p class="font-14 font-500 display-inline-mid width-25per no-margin-right margin-top-15">Storage Location:</p>
						<div class="select large">
							<select class="transform-dd">
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div> -->
						
					</div>
				</div>
				<p class="font-14 font-bold no-margin-right">Sub Location:</p>
				<div class="width-100percent bggray-white box-shadow-dark ">
					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-65percent" style="text-align: left;">Location Address</th>
								<th class="black-color width-20percent">Location Qty</th>
								
							</tr>
						</thead>
					</table>
					<div class="bg-light-gray font-0 text-center position-rel default-cursor padding-right ">
						<div id="location-item" class="width-65percent display-inline-mid padding-all-20" style="text-align: left;">
							<!-- <p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p> -->
							<div class="clear"></div>
						</div>
						<div class="width-20percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all " id="set-new-batch-qty" style="text-align: right;">1000 KG</p>
						</div>

					</div>
					
				</div>

				<p class="font-14 font-bold no-margin-left margin-top-20">Destination</p>
				<p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>

				<div class="bggray-white height-300px overflow-y-auto">
	                <div class="padding-all-10 height-50px padding-bottom-10 border-bottom-small border-black" id="breadCrumbsHierarchy">
	                </div>
	                <div class="font-0" id="batchDisplay">
	                </div>
	            </div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color ">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" id="confirmAddBatch">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Batch MODAL-->


