<div class="main">
			<div class="center-main">
				<div class="mod-select padding-left-3 padding-top-60">			
					<div class="select f-left width-425px ">
						<select>
							<option value="Displaying All Consignee Transfer Records"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>						
				</div>
				<div class="f-right margin-top-10">	
					<a href="stock-create.php">
						<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn">Create Record</button>				
					</a>
					<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn dropdown-btn">View Filter / Search</button>			
				</div>
				<div class="clear"></div>

				<div class="dropdown-search">
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr class="width-100percent">
								<td class="width-75px">Search</td>
								<td>
									<div class="select width-200px font-400">
										<select>
											<option value="items">Items</option>
										</select>
									</div>
								</td>
								<td class="width-100percent">
									<input type="text" />
								</td>
							</tr>
							<tr class="width-100percent">
								<td>Filter By:</td>
								<td>
									<div class="input-group width-200px f-left italic">
									<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
									<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar white-color">
									</span></span>
									</div>
								</td>
								<td class="search-action">
									<div class="f-left">
										
										<div class="select width-150px italic">
											<select>
												<option value="status">Status</option>
											</select>
										</div>
									</div>
									<div class="f-right margin-left-30">
										<p class="display-inline-mid">Sort By:</p>
										<div class="select width-150px display-inline-mid italic">
											<select>
												<option value="withdrawal No." >Transfer No.</option>
											</select>
										</div>
										<div class="select width-150px display-inline-mid italic">
											<select>
												<option value="ascending">Ascending</option>
												<option value="descending">Descending</option>
											</select>
										</div>
									</div>
									<div class="clear"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10">
						<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
						<p class="display-inline-mid font-4 font-bold">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 ">Search</button>
					<div class="clear"></div>
				</div>


				<div class="width-100percent bggray-white">

					<table class="tbl-4c3h margin-top-30">
						<thead>
							<tr>
								<th class="width-15percent black-color no-padding-left">Transfer No.</th>
								<th class="width-15percent black-color no-padding-left">Transfer Date</th>
								<th class="width-40percent black-color no-padding-left">Item Description</th>
								<th class="width-15percent black-color no-padding-left">Owner</th>
								<th class="width-15percent black-color no-padding-left">Status</th>												
							</tr>
						</thead>
					</table>

					<div class="tbl-like hover-transfer-tbl">

						<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
							</div>
							<div class="width-40percent display-inline-mid font-400 text-center">
								<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
									<li>#12345678 - cement</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
								</ul>
							</div>
							<div class="width-15percent font-400 display-inline-mid text-center">
								<div class="owner-img width-30per display-inline-mid ">
									<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
								</div>
								<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none text-center width-100percent">Ongoing</p>							
							</div>
							
						</div>

						<div class="hover-tbl "> 
							<a href="stock-ongoing.php">
								<button class="btn general-btn margin-right-10">View Record</button>
							</a>
							<button class="btn general-btn modal-trigger" modal-target="create-complete-transfer">Complete Transfer</button>
							
						</div>

					</div>

					<div class="tbl-like hover-transfer-tbl">

						<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
							</div>
							<div class="width-40percent display-inline-mid font-400 text-center">
								<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
									<li>#12345678 - cement</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>									
								</ul>
							</div>
							<div class="width-15percent font-400 display-inline-mid text-center">
								<div class="owner-img width-30per display-inline-mid ">
									<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
								</div>
								<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none text-center width-100percent">Complete</p>							
							</div>
							
						</div>

						<div class="hover-tbl "> 
							<a href="stock-complete.php">
								<button class="btn general-btn margin-right-10">View Record</button>
							</a>	
						</div>

					</div>

					<div class="tbl-like hover-transfer-tbl">

						<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
							</div>
							<div class="width-40percent display-inline-mid font-400 text-center">
								<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
									<li>#12345678 - cement</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>									
								</ul>
							</div>
							<div class="width-15percent font-400 display-inline-mid text-center">
								<div class="owner-img width-30per display-inline-mid ">
									<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
								</div>
								<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none text-center width-100percent">Ongoing</p>							
							</div>
							
						</div>

						<div class="hover-tbl "> 
							<a href="stock-ongoing.php">
								<button class="btn general-btn margin-right-10">View Record</button>
							</a>
							<button class="btn general-btn modal-trigger " modal-target="create-complete-transfer">Complete Transfer</button>	
						</div>

					</div>

					<div class="tbl-like hover-transfer-tbl">
						<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
							</div>
							<div class="width-40percent display-inline-mid font-400 text-center">
								<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
									<li>#12345678 - cement</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>									
								</ul>
							</div>
							<div class="width-15percent font-400 display-inline-mid text-center">
								<div class="owner-img width-30per display-inline-mid ">
									<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
								</div>
								<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none text-center width-100percent">Complete</p>							
							</div>
							
						</div>
						<div class="hover-tbl "> 
							<a href="stock-complete.php">
								<button class="btn general-btn margin-right-10">View Record</button>
							</a>	
						</div>
					</div>

					<div class="tbl-like hover-transfer-tbl">
						<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
							</div>
							<div class="width-40percent display-inline-mid font-400 text-center">
								<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
									<li>#12345678 - cement</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>									
								</ul>
							</div>
							<div class="width-15percent font-400 display-inline-mid text-center">
								<div class="owner-img width-30per display-inline-mid ">
									<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
								</div>
								<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none text-center width-100percent">Ongoing</p>							
							</div>
							
						</div>
						<div class="hover-tbl "> 
							<a href="stock-ongoing.php">
								<button class="btn general-btn margin-right-10">View Record</button>
							</a>
							<div class="display-inline-mid">
								<button class="btn general-btn modal-trigger" modal-target="create-complete-transfer">Complete Transfer</button>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>

		<!--Start of view complete transfer modal-->
		<div class="modal-container modal-transfer" modal-id="create-complete-transfer">
			<div class="modal-body xlarge">				

				<div class="modal-head">
					<h4 class="text-left">Complete Transfer</h4>				
					<div class="modal-close close-me transfer-close"></div>
				</div>
				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-bottom-10">
						<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time Start :</p>
						
						<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
							<input class="form-control dp  default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon width-0"><i class="fa fa-calendar"></i></span>
						</div>

						

						<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
							<input class="form-control dp time " placeholder="Transfer Time" type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>
					</div>

					<div class="padding-bottom-10">
						<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time Start :</p>
						
						<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
							<input class="form-control dp  default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon width-0"><i class="fa fa-calendar"></i></span>
						</div>

						<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
							<input class="form-control dp time " placeholder="Transfer Time" type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>
					</div>

					<div class="width-100percent">
						<div class="width-50per f-left">
							<div class="padding-bottom-10 padding-top-10">
								<p class="font-14 font-bold f-left no-margin-all">Worker List</p>
								<a href="#"class="display-inline-mid font-14 f-right"><i class="fa fa-user-plus font-14 padding-right-5"></i> Add Worker</a>
								
								<div class="clear"></div>
							</div>
							<div class="bggray-white padding-top-10 padding-bottom-10 height-300px">
								<div class="border-bottom-small border-black padding-bottom-10 margin-left-10">
									<input type="text" value="Drew Tanaka" class="display-inline-mid width-45per">
									<input type="text" value="Bagger" class="display-inline-mid width-45per no-margin-left">
									<div class="display-inline-mid width-15px margin-left-10">
										<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
									</div>
								</div>
								<div class="border-bottom-small border-black padding-bottom-10 padding-top-10 italic margin-left-10">
									<input type="text" value="Name" class="display-inline-mid width-45per">
									<input type="text" value="Designation" class="display-inline-mid width-45per no-margin-left">
									<div class="display-inline-mid width-15px margin-left-10">
										<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
									</div>
								</div>
							</div>
						</div>

						<div class="width-50per f-right">
							<div class="padding-bottom-10 padding-top-10">
								<p class="font-14 font-bold f-left no-margin-all">Equipment Used</p>
								<a href="#"class="f-right font-14"><i class="fa fa-truck font-16 padding-right-5"></i> Add Equipment</a>
								<div class="clear"></div>
							</div>
							<div class="bggray-white padding-top-10 padding-bottom-10 height-300px">
								<div class="border-bottom-small border-black padding-bottom-10 margin-left-10">
									<input type="text" value="Manual Hopper" class="display-inline-mid width-90per">
									<div class="display-inline-mid width-15px margin-left-10">
										<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
									</div>
								</div>
								<div class="border-bottom-small border-black padding-bottom-10 padding-top-10 italic margin-left-10">
									<input type="text" value="Equipment Name" class="display-inline-mid width-90per">
									<div class="display-inline-mid width-15px margin-left-10">
										<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			
				<div class="modal-footer">
					
					<button type="button" class="font-12 btn f-left btn-primary close-me show-new-window no-margin-left">Show Record</button>
					
					<div class="f-right width-20per">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 modal-trigger close-me" modal-target="stock-completed-transfer">Confirm</button>
					</div>
					<div class="clear"></div>
				</div>
			</div>	
		</div>
		<!--End of view complete transfer modal-->

		<!--Start completed transfer modal-->
		<div class="modal-container" modal-id="stock-completed-transfer">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Complete Transfer</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Transfer Details has been saved. Transfer Record is now complete.</p>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<a href="stock-complete.php">
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" >Show Record</button>
					</a>
				</div>
			</div>	
		</div>