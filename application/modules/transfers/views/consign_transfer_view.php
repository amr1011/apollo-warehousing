<div class="modal-container" id="generate-data-view" >
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>

<div class="main">
			<div class="center-main">
				<div class="mod-select padding-left-3 padding-top-60">			
					<div class="select f-left width-425px ">
						<select class="transform-dd">
							<option value="Displaying All Consignee Transfer Records"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>						
				</div>
				<div class="f-right margin-top-10">	
					<a href="<?php echo BASEURL ?>transfers/consign_create">
						<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn">Create Record</button>				
					</a>
					<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn dropdown-btn">View Filter / Search</button>			
				</div>
				<div class="clear"></div>

				<div class="dropdown-search" >
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr>
								<td class="font-15 font-400 black-color width-15percent">Search:</td>
								<td><div class="select medium">
										<select class="transform-dd">
											<option value="search1">Items</option>
										</select>
									</div>
								</td><td class="width-75percent">
									<input type="text" class="t-small width-100per">
								</td>
							</tr>
							<tr>
								<td class="font-15 font-400 black-color">Filter By:</td>
								<td>
									<div class="input-group">
										<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Receiving Date" data-date-format="MM/DD/YYYY" type="text">
										<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
									</div>

								</td>								
								<td class="search-action">
									<div class="f-left">
										<div class="select medium">
											<select class="transform-dd">
												<option value="stats">Status</option>
											</select>
										</div>																			
									</div>
									<div class="f-right margin-left-30">
										<p class="display-inline-mid">Sort By:</p>
										<div class="select medium display-inline-mid">
											<select class="transform-dd">
												<option value="team">Team Name</option>
												<option value="member">No of Members</option>
											</select>
										</div>
										<div class="select medium display-inline-mid">
											<select class="transform-dd">
												<option value="ascending">Ascending</option>
												<option value="descending">Descending</option>
											</select>
										</div>
									</div>
									<div class="clear"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10 display-search-result">
						<i class="fa fa-2x display-inline-mid fa-caret-right"></i>
						<p class="display-inline-mid font-15 font-bold margin-left-10" >Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 margin-right-30">Search</button>
					<div class="clear"></div>
					<div class="search-result padding-all-10" >
						<div class="margin-left-50">
							<p class="font-400 font-15 black-color f-left">Who can also view this Search Result? </p>
							<div class="clear"></div>
							
							<div class="f-left">
								<div class="display-inline-mid margin-right-10">
									<input type="radio" id="me" name="search-result" class="width-20px transform-scale">
									<label for="me" class="margin-bottom-5 default-cursor font-15 black-color font-400">Only Me</label>
								</div>
								<div class="display-inline-mid margin-right-50">
									<input type="radio" id="selected" name="search-result" class="width-20px transform-scale">
									<label for="selected" class="margin-bottom-5 default-cursor font-15 black-color font-400">With Selected Members</label>
								</div>
								<div class="display-inline-mid member-selection">
									<div class="member-container">
										<p>1 Member Selected</p>
										<i class=" fa fa-caret-down fa-2x"></i>
									</div>
									<div class="popup_person_list" >
										<div class="popup_person_list_div">
											
											<div class="thumb_list_view small marked">
												<div class="thumb_list_inner">
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia Ricardo Gomez Kuala Lumpur Eklabu</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

										</div>										
									</div>
								</div>
								<div class="display-inline-mid margin-left-50">								
									<!-- dropdown here with effects  -->
									<button type="button" class="general-btn">Save Search Results</button>
								</div>
							</div>
							<div class="clear"></div>
						</div>

					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left margin-top-30">

					<div class="width-100percent bggray-white">

						<table class="tbl-4c3h margin-top-30">
							<thead>
								<tr>
									<th class="width-15percent black-color no-padding-left">Transfer No.</th>
									<th class="width-15percent black-color no-padding-left">Transfer Date</th>
									<th class="width-40percent black-color no-padding-left">Item Description</th>
									<th class="width-15percent black-color no-padding-left">Owner</th>
									<th class="width-15percent black-color no-padding-left">Status</th>												
								</tr>
							</thead>
						</table>

						<span id="display-all-record-item">
							<div  class="tbl-like hover-transfer-tbl display-per-item displayed-item">

								<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
									<div class="width-15percent display-inline-mid text-center">
										<p class="font-400 font-14 f-none width-100percent display-transfer-all-number">1234567890</p>						
									</div>
									<div class="width-15percent display-inline-mid text-center">
										<p class="font-400 font-14 f-none width-100percent display-transfer-all-date">22-Oct-2015</p>
									</div>
									<div class="width-40percent display-inline-mid font-400 text-center">
										<ul class="text-left text-indent-20 padding-left-50 font-14 display-item-container-trans">
											<li class="display-item-per-trans">#12345678 - Chinese Corn</li>
										</ul>
									</div>
									<div class="width-15percent font-400 display-inline-mid text-center">
										<div class="owner-img width-30per display-inline-mid ">
											<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent display-transfer-all-image-profile">
										</div>
										<p class="width-100px font-400 font-14 display-inline-mid f-none display-transfer-all-name">Dwayne Garcia</p>								
									</div>
									<div class="width-15percent display-inline-mid text-center">
										<p class="font-400 font-14 f-none text-center width-100percent display-transfer-all-status">Ongoing</p>							
									</div>
									
								</div>

								<div class="hover-tbl text-center" style="height: 100% !important;"> 
									<a href="javascript:void(0)" class="display-transfer-all-view-record">
										<button class="btn general-btn margin-right-10">View Record</button>
									</a>
									<button class="btn general-btn modal-trigger display-transfer-all-complete-transfer" modal-target="create-complete-transfer">Complete Transfer</button>
									
								</div>
							</div>
						</span>

						

					</div>
				</div>
			</div>
		</div>

	<!--Start of view complete transfer modal-->
	<div class="modal-container modal-transfer show-complete-transfer-modal" modal-id="view-complete-transfer">
		<div class="modal-body xlarge" style="margin-top: 80px;">				

			<div class="modal-head">
				<h4 class="text-left">Complete Transfer</h4>				
				<div class="modal-close close-me transfer-close"></div>
			</div>
			<!-- content -->
			<div class="modal-content text-left">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time Start :</p>
					
					<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
						<input class="form-control dp  default-cursor " id="transfer-date-start" placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon width-0"><i class="fa fa-calendar"></i></span>
					</div>
					
					<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
						<input class="form-control dp time " placeholder="Transfer Time" id="transfer-time-start" type="text">
						<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
					</div>
				
				</div>

				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time End :</p>
					
					<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
						<input class="form-control dp border-big-bl border-big-tl default-cursor " id="transfer-date-end" placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon width-0"><i class="fa fa-calendar "></i></span>
					</div>

					<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
						<input class="form-control dp time" placeholder="Transfer Time" id="transfer-time-end" type="text">
						<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
					</div>
				</div>

				<div class="width-100percent">
					<div class="width-50per f-left">
						<div class="padding-bottom-10 padding-top-10">
							<p class="font-14 font-bold f-left no-margin-all">Worker List</p>
							<a href="javascript:void(0)" id="add-worker-btn" class="display-inline-mid font-14 f-right"><i class="fa fa-user-plus font-14 padding-right-5"></i> Add Worker</a>
							
							<div class="clear"></div>
						</div>
						<div id="add-worker-list" class="bggray-white padding-top-10 padding-bottom-10 height-300px">
							<div  class="border-bottom-small border-black padding-top-10 padding-bottom-10 margin-left-10 add-some-worker">
								<input type="text" class="display-inline-mid width-45per worker-name" placeholder="Name">
								<input type="text" class="display-inline-mid width-45per no-margin-left worker-destination" placeholder="Designation">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor remove-worker">
								</div>
							</div>
						</div>
					</div>

					<div class="width-50per f-right">
						<div class="padding-bottom-10 padding-top-10">
							<p class="font-14 font-bold f-left no-margin-all">Equipment Used</p>
							<a href="javascript:void(0)" id="add-equipment-btn" class="f-right font-14"><i class="fa fa-truck font-16 padding-right-5"></i> Add Equipment</a>
							<div class="clear"></div>
						</div>
						<div id="add-equip-list" class="bggray-white padding-top-10 padding-bottom-10 height-300px">
							<div class="border-bottom-small border-black padding-top-10 padding-bottom-10 margin-left-10 add-some-equip">
								<input type="text" class="display-inline-mid width-90per equip-name" placeholder="Equipment Name">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor remove-equipment">
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<a href="javascript:void(0)" >
					<button type="button" id="save-complete-transfer-consignee" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 ">Complete Loading</button>
				</a>
			</div>
		</div>	
	</div>
	<!--End of view complete transfer modal-->

<!--Start completed transfer modal-->
	<div class="modal-container" id="completed-transfer-consignee-modal" modal-id="completed-transfer">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Complete Transfer</h4>				
				<div class="modal-close close-me" id="close-success-modal"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
				<div class="padding-all-10 bggray-7cace5">
					<p class="font-14 font-400 white-color">Transfer Details has been saved. Transfer Record is now complete.</p>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<a href="<?php echo BASEURL; ?>transfers/consign_complete">
					<button type="button"  class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" >Show Record</button>
				</a>
			</div>
		</div>	
	</div>

	<!--End of completed transfer modal-->