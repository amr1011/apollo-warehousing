
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL; ?>transfers/company_transfer">Transfer - Company Goods</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color">Create Record</li>
				</ul>
			</div>
			<div class="semi-main">
				<div class="padding-top-30 margin-bottom-30 text-right width-100percent">
					<a href="<?php echo BASEURL; ?>transfers/company_transfer">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					</a>
					<button class="btn general-btn modal-trigger" modal-target="confirm-create-record">Create Record</button>
				</div>
				<div class="clear"></div>
				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0">
					<p class="font-20 font-bold black-color padding-bottom-20">Transfer No. <span class="company_create_transfer_number">1234567890</span></p>
					
					<div class="width-50percent display-inline-top">
						<div class="padding-bottom-10">
							<p class="font-14 font-bold display-inline-mid width-120">Requested By: </p>
							<input id="company_create_requested_by" type="text" class="display-inline-mid width-340 t-small">
						</div>

						<div class="padding-top-10">
							<p class="font-14 font-bold display-inline-top width-120">Reason: </p>
							<textarea id="company_create_reason" class="width-340"></textarea>
						</div>

					</div>

					<div class="width-50percent display-inline-top">
						

						<div class="padding-bottom-10">
							<p class="font-14 font-bold display-inline-mid width-150">Mode Of Transfer: </p>
							<div class="select large">
								<select id="company_create_mode_of_transfer">
									<!-- <option value="Manual Labor">Manual Labor</option> -->
								</select>
								<!-- <input type="text" class="display-inline-mid width-340 t-small"> -->
							</div>
						</div>
					</div>					
				</div>

				<div id="company_create_product_details" class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white padding-bottom-20 ">
					<div class="width-100percent text-left padding-all-30 no-padding-top no-padding-left margin-bottom-30 border-bottom-small border-gray">
						<p class="font-20 font-bold black-color padding-bottom-20 ">Item List</p>
						<p class="font-14 font-bold display-inline-mid padding-right-20">Item: <span class="red-color display-inline-top">*</span></p>
						<div class="select dispaly-inline-mid large">
							<select id="company_create_product_dropdown">
								<option value="Select">Select Item</option>
							</select>
						</div>
						<!-- <a href="#" class="display-inline-mid padding-left-20"> -->
							<button id="company_create_add_product" data-id="" class="btn general-btn padding-left-20 padding-right-20">Add Item</button>
						<!-- </a> -->
					</div>
					
					<div id="product_list_div" class="border-full padding-all-20 margin-top-20 product-list-div" style="display:none;">
						<div id="company_create_product_display" class="bggray-white height-190px">
							
							<div class="add-product-image height-100percent display-inline-mid width-230px">
								<img src="../assets/images/holcim.jpg" alt="" class="height-100percent width-100percent">
							</div>

							<div class="display-inline-top width-75percent padding-left-10">
								<div class="padding-all-15 bg-light-gray">
									<p class="add_product_sku_name font-16 font-bold f-left">SKU# 1234567890 - Holcim Cement</p>
									<div class="remove_product f-right width-20px margin-left-10">
										<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
									</div>
									<div class="clear"></div>
								</div>

								<div class="padding-all-15 text-left">
									<p class="f-left width-20percent font-bold">Description</p>
									<p id="add_product_description" class="f-left">Ang Cement na matibay</p>
									<div class="clear"></div>
								</div>

								<div class="padding-all-15 text-left bg-light-gray">

									<!-- <p class="f-left no-margin-all width-20percent font-bold">Vendor</p>
									<p id="add_product_vendor" class="f-left no-margin-all width-20percent">Innova Farming</p> -->
									<p class="f-left no-margin-all width-20percent font-bold">Transfer Method</p>
									
									<div class="f-left">										
										<input type="radio" class="display-inline-mid width-50px default-cursor radio-bulk" name="bag-or-bulk">
										<label for="bulk" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>
									</div>

									<div class="f-left margin-left-10">																	
										<input type="radio" class="display-inline-mid width-50px default-cursor radio-piece" name="bag-or-bulk">
										<label for ="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Piece</label>
									</div>								
									<div class="clear"></div>
								</div>

								<div class="padding-all-15 text-left">
									<p class="f-left width-20percent font-bold">Unit of Measure: </p>
									<div class="select display-inline-mid small width-70px height-26 unit-remove-dropdown">
										<div class="frm-custom-dropdown">
											<div class="frm-custom-dropdown-txt">
												<input class="dd-txt" type="text">
											</div>

											<div class="frm-custom-icon"></div>
											
											<div class="frm-custom-dropdown-option" style="display: none;">
					 								<!-- <div class="option" data-value="1">#000000001 Cobra</div> -->
					 								<!-- <div class="option" data-value="2">#000000002 Samurai</div> -->
											</div>
										</div>
										<select class="unit_of_measure_dropdown frm-custom-dropdown-origin" style="display: none;">
				 							<!-- <option value="1">#000000001 Cobra</option> -->
											<!-- <option value="2">#000000002 Samurai</option> -->
										</select>
									</div>
									<div class="clear"></div>
								</div>
								
								
								<div class="padding-all-15 bg-light-gray text-left font-0">
									<!-- <p class="display-inline-mid width-20percent font-bold">Aging Days</p>
									<p id="add_product_aging_days" class="display-inline-mid width-20percent">10 Days</p> -->
									<!-- <p class="display-inline-mid width-25per font-14 font-bold">Qty to Transfer</p>
									
									<input type="text" class="qty-to-transfer-KG display-inline-mid width-70px height-26 t-small" name="bag-or-bulk">
									<div class="select display-inline-mid small width-70px height-26">
										<div class="frm-custom-dropdown">
											<div class="frm-custom-dropdown-txt">
												<input class="dd-txt" type="text">
											</div>

											<div class="frm-custom-icon"></div>
											
											<div class="frm-custom-dropdown-option" style="display: none;">
					 								
											</div>
										</div>
										<select class="unit_of_measure_dropdown frm-custom-dropdown-origin" style="display: none;">
				 							
										</select>
									</div>
									
									<input type="text" class="qty-to-transfer-Piece display-inline-mid width-70px height-26 t-small" name="bag-or-bulk" >
									<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">Piece</p> -->
										<p class="f-left width-20percent font-bold">Total QTY</p>
										<p class="f-left total-qty-to-transfer"></p>
										<div class="clear"></div>

									<div class="clear"></div>
								</div>
								
								<!-- <div class="padding-all-15 text-left bg-light-gray">
									
								</div> -->
								
								
							</div>

						</div>

						<div id="ui_company_create_product_location" class="padding-top-50">
							<div class="padding-top-20 padding-bottom-20">
								<p class="font-20 font-500 f-left">Item Location</p>
								<div class="f-right">
									<button class="btn_add_company_transfer_batch btn general-btn modal-trigger" modal-target="add-batch">Add Batch</button>
								</div>
								<div class="clear"></div>
								<!-- <div class="text-left padding-top-10">
									<p class="font-14 font-bold padding-right-10 display-inline-mid">Transfer Balance: </p>
									<p class="font-14 font-bold display-inline-mid"><span class="product-remaining-balance red-color vertical-baseline">80</span> KG</p>
								</div> -->
							</div>

							<!-- <p class="f-left font-14 font-bold padding-right-20">Batch Name:</p>
							<p class="f-left font-14 font-400">Sample Batch Name</p>
							<p class="f-right font-14 font-bold padding-bottom-20">Quantity 50 KG</p>
							<div class="clear"></div>
							<table class="tbl-4c3h">
								<thead>
									<tr>
										<th class="width-50percent black-color">Origin</th>
										<th class="width-50percent">Destination</th>
									</tr>
								</thead>
							</table>
							<div class="product-location font-0 tbl-dark-color margin-bottom-20 position-rel">
								<div class="width-100percent ">
									<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25">
										<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
									</div>

									<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25">
										<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
									</div>
								</div>
								<div class="edit-hide">
									<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit Batch</button>
									<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10" >Remove Batch</button>
								</div>
							</div>

							<p class="f-left font-14 font-bold padding-right-20">Batch Name:</p>
							<p class="f-left font-14 font-400">Sample Batch Name</p>
							<p class="f-right font-14 font-bold padding-bottom-20">Quantity 50 KG</p>
							<div class="clear"></div>
							<table class="tbl-4c3h">
								<thead>
									<tr>
										<th class="width-50percent black-color">Origin</th>
										<th class="width-50percent">Destination</th>
									</tr>
								</thead>
							</table>
							<div class="product-location font-0 tbl-dark-color position-rel">
								<div class="width-100percent">
									<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25">
										<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
									</div>
									<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25">
										<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
										<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
										<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
									</div>
								</div>
								<div class="edit-hide">
									<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit Batch</button>
									<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10" >Remove Batch</button>
								</div>
							</div> -->

						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400 ">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title padding-top-10 font-500 font-22 black-color"> Documents</h4>
							</a>

							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="upload-documents">Upload a Document<input type="file" name="attachment" style="display: none;"></button>
							</div>


							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-20">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>

								<div id="displayDocuments" class="table-content position-rel tbl-dark-color">
									<!-- <div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">TransferDocument </p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<button class="btn general-btn padding-left-30 padding-right-30">View File</button>
										<button class="btn general-btn padding-left-30 padding-right-30 margin-left-10">Remove File</button>	
									</div> -->
								</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-22 black-color padding-top-10"> Notes</h4>
							</a>
							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="add-notes">Add Notes</button>
							</div>
							<div class="clear"></div>
						</div>
						
					
						
						<div class="panel-collapse collapse in">
							<div id="company_create_note_list" class="panel-body padding-all-20">

								<div class="border-full padding-all-10 margin-left-18">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10  font-400">Show Less</a>
									<div class="clear"></div>
								</div>

								<div class="border-full padding-all-10 margin-left-18 margin-top-10">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10  font-400">Show More</a>
									<div class="clear"></div>
								</div>									
							</div>
						</div>
					</div>
				</div>
				<div class="width-100percent text-right border-top-small border-gray padding-top-10">
					<a href="<?php echo BASEURL; ?>transfers/company_transfer">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					</a>
					<button class="btn general-btn modal-trigger" modal-target="confirm-create-record">Create Record</button>
				</div>
		</div>
	</div>

	<!--MODALS-->

	<!--Start of Confirm Create Record MODAL-->
	<div class="modal-container modal-created-record" modal-id="created-record">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Create Record</h4>				
				<!-- <div class="modal-close close-me"></div> -->
			</div>

			<!-- content -->
			<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
				<div class="padding-all-10 bggray-7cace5">
					<p class="modal-created-record-message font-14 font-400 white-color">Transfer No. <span class="created_to_number"></span> has been created.</p>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<!-- <button type="button" class="close-modal-created-record font-12 btn btn-default font-12 display-inline-mid close-me red-color">Close</button> -->
				<button type="button" class="btn-show-record font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" >Show Record</button>
			</div>
		</div>	
	</div>
	<!--End of Confirm Create Record MODAL-->

	<!--Start of Confirm Create Record MODAL-->
	<div class="modal-container" modal-id="confirm-create-record">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Create Record</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<p class="font-14 font-400">Are you sure you want to create Transfer No. <span class="company_create_transfer_number">1234567890?</span></p>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button id="submit_add_transfer_company" type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="created-record">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Confirm Create Record MODAL-->

	



	<!--Start of Upload Document MODAL-->
	<div class="modal-container" cr8v-file-upload="modal" modal-id="upload-documents">
		<div class="modal-body small">
			<div class="modal-head">
				<h4 class="text-left">Upload Document</h4>
				<div class="modal-close close-me"></div>
			</div>
			<div class="modal-content text-left" style="position:relative;">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-125px">Document Name:</p>
					<input class="width-200px display-inline-mid document-name" type="text">
					<button class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 browse" type="button">Browse</button>
				</div>
			</div>
			<div class="modal-footer text-right">
				<button class="font-12 btn btn-default font-12 display-inline-mid close-me red-color" type="button">Cancel</button>
				<button class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 confirm-upload" type="button">Confirm</button>
			</div>
		</div>
	</div>
	<!--End of Upload Document MODAL-->

	<!--Start of Add Notes MODAL-->
	<div id="modal_company_create_add_note" class="modal-container" modal-id="add-notes">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Add Notes</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
					<input id="add_note_subject" type="text" class="width-380px display-inline-mid">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
					<textarea id="add_note_text" class="width-380px margin-left-10 height-250px"></textarea>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button id="submit_add_note" type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Notes MODAL-->

	<!--Start of Add Batch MODAL-->
	<div id="add_batch_modal" class="modal-container" modal-id="add-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Add Batch</h4>				
				<div class="close-add-batch-modal modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left">
					<div class="">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Item:</p>
						<p id="add_product_sku_name" class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Japanse Corn</p>
					</div>
					<!-- <div class="padding-top-5">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Transfer Balance:</p>
						<p id="add_product_transfer_balance" class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
					</div> -->
					<!-- <div class="padding-bottom-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Transfer Balance:</p>
						<p id="add_product_transfer_balance" class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
					</div> -->
					<!-- <div class="padding-top-5">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per ">Amount to Transfer:</p>
						<input id="add_product_amount_to_transfer" type="text" class="t-small no-margin-left">
					</div> -->
					<!-- <div class="">
						<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per">Batch Name: </p>
						<div class="select large">
							<select id="add_batch_name">
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div> -->
					<div class="padding-bottom-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per margin-top-15">Batch Name: </p>
						<div class="select large">
							<select id="add_batch_name" class="transform-dd">
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div>
					<div class="padding-top-10">
						<!-- <p class="font-14 font-500 no-margin-left">Origin</p>
						<p class="font-14 font-500 display-inline-mid width-25per margin-top-10 no-margin-right">Storage Location:</p> -->
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Origin:</p>
						<!-- <p class="batch-storage-name font-14 font-400 no-margin-left display-inline-mid ">DITO ANG STORAGE.</p> -->
						
					<!-- 	<div class="select large">
							<select id="add_batch_location">
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div> -->
						
					</div>
					<!-- <div class="padding-top-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Sub Location:</p>
					</div> -->
				</div>
				<!-- <p class="font-14 font-500">Sub Location:</p> -->
				<div id="company_create_add_batch_sub_location" class="width-100percent bggray-white box-shadow-dark">

					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-65percent">Location Address</th>
								<th class="black-color width-20percent">Location Qty</th>
								<th class="black-color width-15percent">Qty</th>
							</tr>
						</thead>
					</table>

					<div id="sub_location_batch_list">
						
					</div>

				</div>
				<p class="font-14 font-bold no-margin-left display-inline-mid width-25per margin-top-10">Storage Location:</p>
				<!-- <p class="no-margin-left font-14 font-500 margin-top-10">Storage Location:</p> -->
	            <p class="no-margin-all padding-bottom-10">Click a Location to Select It. Double-click to go to its sub-locations</p>
	            <div class="bggray-white height-300px overflow-y-auto">
	                <div class="padding-all-10 height-50px padding-bottom-10 border-bottom-small border-black" id="breadCrumbsHierarchy">
	                </div>
	                <div class="font-0" id="batchDisplay">
	                </div>
	            </div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="close-add-batch-modal font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button id="company_create_submit_add_batch" type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Batch MODAL-->

	<!-- Start of Edit Batch Modal  -->
	<div class="modal-container" modal-id="edit-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Edit Batch</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left">
					<div class="">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Item:</p>
						<p class="add_product_sku_name font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Japanse Corn</p>
					</div>
					<div class="padding-top-5">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Transfer Balance:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
					</div>
					<div class="padding-top-5">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per ">Amount to Transfer:</p>
						<input type="text" class="t-small no-margin-left">
					</div>
					<div class="">
						<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per">Batch Name: </p>
						<div class="select large">
							<select>
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div>
					<div class="padding-top-10">
						<p class="font-14 font-500 no-margin-left">Origin</p>
						<p class="font-14 font-500 display-inline-mid width-25per margin-top-10 no-margin-right">Storage Location:</p>
						<div class="select large">
							<select>
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div>
						
					</div>
				</div>
				<p class="font-14 font-500">Sub Location:</p>
				<div class="width-100percent bggray-white box-shadow-dark">

					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-75percent">Location Address</th>
								<th class="black-color width-25percent">Location Qty</th>
							</tr>
						</thead>
					</table>
					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22"></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>
				</div>

				<p class="font-14 font-400 no-margin-left margin-top-20">Destination</p>
				<p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>

				<div class="bggray-white height-300px overflow-y-auto">
					<div class="padding-all-10 padding-bottom-10 border-bottom-small border-black">
						<i class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small  padding-right-10 border-black "></i>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5">Warehouse 1</p>
						<span class="display-inline-mid padding-all-5">&gt;</span>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid">Area 1</p>
					</div>
					<div class="font-0">
						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor ">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 1</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 2</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 3</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 4</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 5</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 6</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 7</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 8</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 9</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 10</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 11</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 12</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 13</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 14</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

					</div>
				</div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!-- end of edit batch modal  -->

	<!-- start of generating view modal -->
	<div class="modal-container modal-generate-view" modal-id="generate-view">
	    <div class="modal-body small">
	        <div class="modal-head">
	            <h4 class="text-left">Message</h4>
	        </div>
	        <!-- content -->
	        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
	            <div class="padding-all-10 bggray-7cace5">
	                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
	            </div>
	        </div>
	        <div class="modal-footer text-right" style="height: 50px;">
	        </div>
	    </div>
	</div>
	<!-- end of generating view modal -->
