
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL; ?>transfers/company_transfer">Transfer - Company Goods</a></li>
					<li><span>&gt;</span></li>						
					<li id="company_complete_transfer_number" class="font-bold black-color">Transfer No. 1234567890 (Complete)</li>
				</ul>
			</div>
			<div class="semi-main">
			

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white margin-top-50">
					<div class="panel-group">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">								
								<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10  active"> Transfer Information</h4>
							</a>																			
							<div class="clear"></div>																				
						</div>

						<div class="panel-collapse collapse in ">
							<div class="panel-body">
								
								<div class="display-inline-mid width-50per">					
									<div class="text-left padding-bottom-10">
										<p class="f-left font-14 font-bold width-40percent">Mode Of Transfer:</p>
										<p id="company_complete_mode_of_transformation" class="f-left font-14 font-400 width-50percent">Manual Labor</p>
										<div class="clear"></div>
									</div>

									<div class="text-left padding-top-10">
										<p class="f-left font-14 font-bold width-40percent">Reason For Transfer:</p>
										<p id="company_complete_reason" class="f-left font-14 font-400 width-50percent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretrium tempor. Ut eget imperdiet neque.</p>
										<div class="clear"></div>
									</div>
								</div>

								<div class="display-inline-top width-50per">
									<div class="text-left padding-bottom-10">
										<p class="f-left font-14 font-bold width-40percent">Date And Time Issued:</p>
										<p id="company_complete_date_and_time" class="f-left font-14 font-400 width-50percent">September 10, 2015 | 08:25 AM</p>
										<div class="clear"></div>
									</div>

									<div class="text-left height-50px ">
										<p class="f-left font-14 font-bold width-40percent padding-top-5 margin-top-3">Requested By:</p>
										<div class="position-rel display-inline-mid text-center height-65percent">
											<img src="../assets/images/profile/profile_x.png" alt="images" class="height-100percent display-inline-mid">
											<p id="company_complete_requested_by" class="display-inline-mid font-14 font-400">Julie Mendez</p>
										</div>
										
										<div class="clear"></div>
									</div>

									<div class="text-left">
										<p class="f-left font-14 font-bold width-40percent">Status:</p>
										<p id="company_complete_status" class="f-left font-14 font-400 width-50percent">Complete</p>
										<div class="clear"></div>
									</div>

								</div>
								<div class="width-100per margin-top-30 padding-top-10 text-left border-top-light">
								

									<div class="panel-body padding-all-10">
										<div class="padding-left-10 padding-top-15 padding-right-10 padding-bottom-15 bggray-middark font-0 ">
											<p class="no-margin-all font-14 font-bold width-25percent display-inline-mid padding-left-10 ">Transfer Date / Time Start</p>
											<p id="company_complete_start_date" class="no-margin-all font-14 width-26-6percent display-inline-mid ">22-Oct-2015 10:00 AM</p>
											<p class="no-margin-all font-14 font-bold width-25percent display-inline-mid ">Transfer Duration</p>
											<p id="company_complete_time_duration" class="no-margin-all font-14 width-23-4percent display-inline-mid ">1 Day, 7 Hours</p>
										</div>

										<div class="padding-left-10 padding-top-15 padding-right-10 padding-bottom-15 width-50percent font-0 no-padding-right ">
											<p class="no-margin-all font-14 font-bold width-50percent display-inline-mid padding-left-10">Transfer Date / Time End</p>
											<p id="company_complete_end_date" class="no-margin-all font-14 width-50percent display-inline-mid">23-Oct-2015 05:00 AM</p>
											
										</div>

										<div class="font-0">
											<div id="company_complete_worker_list" class="width-48percent f-left">
												<div class=" padding-bottom-10 padding-left-10 padding-top-15 padding-right-10 padding-bottom-15">
													<h4 class="font-20 font-500 transfer-information display-inline-mid width-90per ">Workers List</h4>
												</div>
												<!-- <div class="padding-left-20 padding-top-15 padding-right-20 padding-bottom-15 font-0 bggray-middark">
													<p class="no-margin-all font-14 font-bold f-left  ">Drew Tanaka</p>
													<p class="no-margin-all font-14 font-bold f-right ">Bagger</p>
													<div class="clear"></div>
												</div>
												<div class="padding-left-20 padding-top-15 padding-right-20 padding-bottom-15 font-0">
													<p class="no-margin-all font-14 font-bold f-left  ">Luke Mejellano</p>
													<p class="no-margin-all font-14 font-bold f-right ">Bagger</p>
													<div class="clear"></div>
												</div>
												<div class="padding-left-20 padding-top-15 padding-right-20 padding-bottom-15 font-0 bggray-middark">
													<p class="no-margin-all font-14 font-bold f-left  ">Timothy James Cruz</p>
													<p class="no-margin-all font-14 font-bold f-right ">Bagger</p>
													<div class="clear"></div>
												</div> -->
									
											</div>

											<div id="company_complete_equipment_list" class="width-48percent f-right ">
												<div class="padding-left-10 padding-top-15 padding-right-10 padding-bottom-15  ">
													<h4 class="font-20 font-500 ">Equipment Used</h4>
												</div>
												<!-- <div class="padding-left-20 padding-top-15 padding-right-20 padding-bottom-15 font-0 bggray-middark">
													<p class="no-margin-all font-14 font-bold f-left  ">Crane</p>
													
													<div class="clear"></div>
												</div>
												<div class="padding-left-20 padding-top-15 padding-right-20 padding-bottom-15 font-0 ">
													<p class="no-margin-all font-14 font-bold f-left  ">ForkLift1</p>
													
													<div class="clear"></div>
												</div>
												<div class="padding-left-20 padding-top-15 padding-right-20 padding-bottom-15 font-0 bggray-middark">
													<p class="no-margin-all font-14 font-bold f-left  ">ForkLift2</p>
													
													<div class="clear"></div>
												</div> -->
											</div>
											<div class="clear"></div>
										</div>
									</div>
									<div class="clear"></div>									
								</div>
							</div>
						</div>
					</div>						
				</div>
				
				<div id="complete_product_div">
					<div class="complete-product-div-template border-top border-blue box-shadow-dark margin-bottom-20 bggray-white " style="display:none;">
						<div class="head panel-group text-left">
							<div class="panel-heading font-14 font-400 margin-top-5 margin-bottom-5">
								<a class="colapsed black-color " href="#">
									<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
									<h4 class="product-sku-name panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10 "> SKU #1234567890 - Japanese Corn</h4>
								</a>

							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body padding-all-20">
									<div class="bggray-white border-full padding-all-20 height-190px box-shadow-dark">
										<div class="product-image height-100percent display-inline-mid width-20per text-center">
											<img src="../assets/images/corn.jpg" alt="" class="height-100percent width-100percent">
										</div>

										<div class="display-inline-top width-80per padding-left-10">
											
											<div class="padding-all-15 bg-light-gray">
												<!-- <p class="f-left no-margin-all width-150px font-bold">Vendor</p>
												<p class="product-vendor f-left no-margin-all width-180px">Holcim Incorporated</p> -->
												<p class="f-left no-margin-all width-150px font-bold">Transfer Method</p>
												<p class="product-transfer-method f-left no-margin-all width-180px transfer-display-piece">By Piece</p>

												<div class="f-left transfer-hide-piece" >
													<div class="f-left ">										
														<input type="radio" class="display-inline-mid width-20px default-cursor" name="bag-or-bulk" id="bulk">
														<label for="bulk" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>
													</div>

													<div class="f-left  margin-left-10">																	
														<input type="radio" class="display-inline-mid width-20px default-cursor " name="bag-or-bulk" id="piece" checked>
														<label for="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Piece</label>
													</div>
													<div class="clear"></div>
												</div>

												<div class="clear"></div>
											</div>
											
											
											<div class="padding-all-15">
												<p class="f-left width-150px font-bold">Total QTY to transfer</p>
												<p class="total-qty-to-transfer f-left width-180px">5000 mt</p>
												<div class="clear"></div>
											</div>
											
											
											<div class="padding-all-15 bg-light-gray">
												<p class="f-left width-150px font-bold">Description</p>
												<p class="product-description f-left">Ang Cement na matibay</p>
												<div class="clear"></div>
											</div>
											

										</div>
									</div>

									<div class="product-location-div padding-top-20">
										<div class="margin-bottom-10 margin-top-10">
											<p class=" f-left no-margin-all font-20 font-500  padding-left-10">Item Location</p>
											
											<div class="clear"></div>
										</div>

										<div class="product-batch-template" style="display:none;">
											<p class="f-left font-14 font-bold padding-right-20 padding-left-10">Batch Name:</p>
											<p class="batch-name f-left font-14 font-400">CNEKIEK123456 </p>
											<p class="batch-qty f-right font-14 font-bold padding-bottom-20">Quantity 50 KG</p>
											<div class="clear"></div>
											<table class="tbl-4c3h">
												<thead>
													<tr>
														<th class="width-50percent black-color">Origin</th>
														<th class="width-50percent">Destination</th>
													</tr>
												</thead>
											</table>

											<div class="product-location-dark-template font-0 tbl-dark-color position-rel">
												<div class="width-100percent show-ongoing-content margin-bottom-20">
													<div class="batch-origin padding-all-20 width-50percent text-center display-inline-mid height-auto">
														<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
													</div>

													<div class="batch-destination padding-all-20 width-50percent text-center display-inline-mid height-auto">
														<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
													</div>

												</div>
												
											</div>

										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

		

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 padding-bottom-5 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400 ">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-20 black-color padding-top-10"> Documents</h4>
							</a>
							
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div id="complete_document_div" class="panel-body padding-all-10">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>
																
							</div>
						</div>
					</div>
				</div>

				<div id="complete_note_div" class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white padding-bottom-5">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<h4 class="panel-title font-500 font-20 black-color padding-top-10"> Notes</h4>
							</a>
						
							<div class="clear"></div>

						</div>
						<div class="view_note_list panel-collapse collapse in">
							<div class="panel-body padding-all-10">

											
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>

	<!--Start of view complete transfer modal-->
	<div class="modal-container modal-transfer" modal-id="view-complete-transfer">
		<div class="modal-body xlarge">				

			<div class="modal-head">
				<h4 class="text-left">Complete Transfer</h4>				
				<div class="modal-close close-me transfer-close"></div>
			</div>
			<!-- content -->
			<div class="modal-content text-left">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid no-margin-all">Loading Date / Time Start :</p>
					
					<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
						<input class="form-control dp  default-cursor " placeholder="Loading Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon width-0"><i class="fa fa-calendar"></i></span>
					</div>
					
					<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
						<input class="form-control dp time " placeholder="Loading Time" type="text">
						<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
					</div>
				
				</div>

				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid no-margin-all">Loading Date / Time Start :</p>
					
					<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
						<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Loading Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon width-0"><i class="fa fa-calendar "></i></span>
					</div>

					<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
						<input class="form-control dp time" placeholder="Loading Time" type="text">
						<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
					</div>
				</div>

				<div class="width-100percent">
					<div class="width-50per f-left">
						<div class="padding-bottom-10 padding-top-10">
							<p class="font-14 font-bold f-left no-margin-all">Worker List</p>
							<a href="#"class="display-inline-mid font-14 f-right"><i class="fa fa-user-plus font-14 padding-right-5"></i> Add Worker</a>
							
							<div class="clear"></div>
						</div>
						<div class="bggray-white padding-top-10 padding-bottom-10 height-300px">
							<div class="border-bottom-small border-black padding-bottom-10 margin-left-10">
								<input type="text" value="Drew Tanaka" class="display-inline-mid width-45per">
								<input type="text" value="Bagger" class="display-inline-mid width-45per no-margin-left">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
							<div class="border-bottom-small border-black padding-bottom-10 padding-top-10 italic margin-left-10">
								<input type="text" value="Name" class="display-inline-mid width-45per">
								<input type="text" value="Designation" class="display-inline-mid width-45per no-margin-left">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
						</div>
					</div>

					<div class="width-50per f-right">
						<div class="padding-bottom-10 padding-top-10">
							<p class="font-14 font-bold f-left no-margin-all">Equipment Used</p>
							<a href="#"class="f-right font-14"><i class="fa fa-truck font-16 padding-right-5"></i> Add Equipment</a>
							<div class="clear"></div>
						</div>
						<div class="bggray-white padding-top-10 padding-bottom-10 height-300px">
							<div class="border-bottom-small border-black padding-bottom-10 margin-left-10">
								<input type="text" value="Manual Hopper" class="display-inline-mid width-90per">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
							<div class="border-bottom-small border-black padding-bottom-10 padding-top-10 italic margin-left-10">
								<input type="text" value="Equipment Name" class="display-inline-mid width-90per">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20  close-me">Complete Loading</button>
			</div>
		</div>	
	</div>
	<!--End of view complete transfer modal-->

	<!--Start completed transfer modal-->
	<div class="modal-container" modal-id="completed-transfer">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Complete Transfer</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
				<div class="padding-all-10 bggray-7cace5">
					<p class="font-14 font-400 white-color">Transfer Details has been saved. Transfer Record is now complete.</p>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<a href="stock-complete.php">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" >Show Record</button>
				</a>
			</div>
		</div>	
	</div>

	<!--End of completed transfer modal-->

	<!--Start of Upload Document MODAL-->
	<div class="modal-container" modal-id="upload-documents">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Upload Document</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">Dcoument Name:</p>
					<input type="text" class="width-231px display-inline-mid">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">File Location:</p>
					<input type="text" class="width-231px display-inline-mid">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Browse</button>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Upload Document MODAL-->

	<!--Start of Add Notes MODAL-->
	<div class="modal-container" modal-id="add-note">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Add Notes</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
					<input type="text" class="width-380px display-inline-mid">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
					<textarea class="width-380px margin-left-10 height-250px"></textarea>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Notes MODAL-->


	<!--Start of Add Batch MODAL-->
	<div class="modal-container" modal-id="edit-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Edit Batch</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Item:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - HOlcim Cement</p>
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Transfer Balance:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per">Amount to Transfer:</p>
						<input type="text" class="t-small no-margin-left">
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per">Batch Name: </p>
						<div class="select large">
							<select>
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div>
					<div class="padding-bottom-10 padding-top-30">
						<p class="font-14 font-500 no-margin-left">Origin</p>
						<p class="font-14 font-500  display-inline-mid width-25per margin-left-10 margin-top-15 no-margin-right">Storage Location:</p>
						<div class="select large">
							<select>
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div>
						
					</div>
				</div>
				<p class="font-14 font-500 margin-left-10">Sub Location:</p>
				<div class="width-100percent bggray-white box-shadow-dark">
					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-75percent">Location Address</th>
								<th class="black-color width-25percent">Location Qty</th>
							</tr>
						</thead>
					</table>
					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22"></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>
				</div>

				<p class="font-14 font-500 no-margin-left margin-top-20">Destination</p>
				<p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>

				<div class="bggray-white height-300px overflow-y-auto">
					<div class="padding-all-10 padding-bottom-10 border-bottom-small border-black">
						<i class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small  padding-right-10 border-black "></i>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5">Warehouse 1</p>
						<span class="display-inline-mid padding-all-5">&gt;</span>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid">Area 1</p>
					</div>
					<div class="font-0">
						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor ">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 1</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 2</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 3</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 4</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 5</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 6</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 7</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 8</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 9</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 10</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 11</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 12</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 13</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 14</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

					</div>
				</div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Batch MODAL-->

	<div class="modal-container" modal-id="add-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Add Batch</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Item:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Holcim Cement</p>
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Transfer Balance:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per margin-top-15">Qty to Transfer:</p>
						<input type="text" class="t-small no-margin-left">
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per">Batch Name: </p>
						<div class="select large">
							<select>
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div>
					<div class="padding-top-30">
						<p class="font-14 font-500 no-margin-left">Origin</p>
						<p class="font-14 font-500 display-inline-mid width-25per margin-top-15 no-margin-right">Storage Location:</p>
						<div class="select large">
							<select>
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div>
						
					</div>
				</div>
				<p class="font-14 font-500">Sub Location:</p>
				<div class="width-100percent bggray-white box-shadow-dark">

					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-75percent">Location Address</th>
								<th class="black-color width-25percent">Location Qty</th>
							</tr>
						</thead>
					</table>
					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22"></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>
				</div>

				<p class="font-14 font-500 no-margin-left margin-top-20">Destination</p>
				<p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>

				<div class="bggray-white height-300px overflow-y-auto">
					<div class="padding-all-10 padding-bottom-10 border-bottom-small border-black">
						<i class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small  padding-right-10 border-black "></i>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5">Warehouse 1</p>
						<span class="display-inline-mid padding-all-5">&gt;</span>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid">Area 1</p>
					</div>
					<div class="font-0">
						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor ">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 1</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 2</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 3</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 4</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 5</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 6</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 7</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 8</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 9</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 10</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 11</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 12</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 13</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 14</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

					</div>
				</div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>	


	<!-- start of generating view modal -->
	<div class="modal-container modal-generate-view" modal-id="generate-view">
	    <div class="modal-body small">
	        <div class="modal-head">
	            <h4 class="text-left">Message</h4>
	        </div>
	        <!-- content -->
	        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
	            <div class="padding-all-10 bggray-7cace5">
	                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
	            </div>
	        </div>
	        <div class="modal-footer text-right" style="height: 50px;">
	        </div>
	    </div>
	</div>
	<!-- end of generating view modal -->
