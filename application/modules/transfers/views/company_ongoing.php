
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL; ?>transfers/company_transfer">Transfer - Company Goods</a></li>
					<li><span>&gt;</span></li>						
					<li class="ongoing-transfer-number font-bold black-color">Transfer No. 1234567890</li>
				</ul>
			</div>
			<div class="semi-main">
				<div class="f-right  margin-bottom-30">
					<button class="btn general-btn modal-trigger complete-transfer-btn" modal-target="view-complete-transfer">Complete Transfer</button>
				</div>
				<div class="clear"></div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white">
					<div class="panel-group">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">								
								<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10  active"> Transfer Information</h4>
							</a>					
							<a href="<?php echo BASEURL; ?>transfers/company_ongoing_edit" class="edit-enter-product">
								<button class="btn general-btn f-right width-100px transfer-edit-information">Edit</button>			
							</a>
							<div class="clear"></div>
							<!-- </a> -->																	
						</div>

						<div class="panel-collapse collapse in ">
							<div class="panel-body">
								
								<div class="display-inline-mid width-50per">					
									<div class="text-left padding-bottom-10">
										<p class="f-left font-14 font-bold width-40percent">Mode Of Transfer:</p>
										<p class="ongoing-mode-of-transfer f-left font-14 font-400 width-50percent">Manual Labor</p>
										<div class="clear"></div>
									</div>

									<div class="text-left padding-top-10">
										<p class="f-left font-14 font-bold width-40percent">Reason For Transfer:</p>
										<p class="ongoing-reason-for-transfer f-left font-14 font-400 width-50percent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretrium tempor. Ut eget imperdiet neque.</p>
										<div class="clear"></div>
									</div>


								</div>

								<div class="display-inline-top width-50per">
									<div class="text-left padding-bottom-10">
										<p class="f-left font-14 font-bold width-40percent">Date And Time Issued:</p>
										<p class="ongoing-date-issued f-left font-14 font-400 width-50percent">September 10, 2015 | 08:25 AM</p>
										<div class="clear"></div>
									</div>

									<div class="text-left height-50px ">
										<p class="f-left font-14 font-bold width-40percent padding-top-5 margin-top-3">Requested By:</p>
										<div class="position-rel display-inline-mid text-center height-65percent">
											<img src="../assets/images/profile/profile_x.png" alt="images" class="height-100percent display-inline-mid">
											<p class="ongoing-requested-by display-inline-mid font-14 font-400">Julie Mendez</p>
										</div>
										
										<div class="clear"></div>
									</div>

									<div class="text-left">
										<p class="f-left font-14 font-bold width-40percent">Status:</p>
										<p class="ongoing-status f-left font-14 font-400 width-50percent">Ongoing</p>
										<div class="clear"></div>
									</div>

								</div>
								<div class="width-100per margin-top-30 padding-top-10 text-left border-top-light">
								

									<div class="panel-body padding-all-10">
										<div class="padding-left-10 padding-top-15 padding-right-10 padding-bottom-15 bggray-middark font-0 gray-color">
											<p class="no-margin-all font-14 font-bold width-25percent display-inline-mid padding-left-10 ">Transfer Date / Time Start</p>
											<p class="ongoing-transfer-start no-margin-all font-14 width-26-6percent display-inline-mid ">Not Yet Assigned</p>
											<p class="no-margin-all font-14 font-bold width-25percent display-inline-mid ">Transfer Duration</p>
											<p class="ongoing-transfer-duration no-margin-all font-14 width-23-4percent display-inline-mid ">Not Yet Assigned</p>
										</div>

										<div class="padding-left-10 padding-top-15 padding-right-10 padding-bottom-15 width-50percent font-0 no-padding-right gray-color">
											<p class="no-margin-all font-14 font-bold width-50percent display-inline-mid padding-left-10">Transfer Date / Time Complete</p>
											<p class="ongoing-transfer-complete no-margin-all font-14 width-50percent display-inline-mid">Not Yet Assigned</p>
											
										</div>

										<div class="font-0">
											<div class="width-48percent f-left">
												<div class=" padding-bottom-10 padding-left-10 padding-top-15 padding-right-10 padding-bottom-15">
													<h4 class="font-20 font-500 transfer-information display-inline-mid width-90per  gray-color">Workers List</h4>
												</div>
												<div class="padding-left-10 padding-top-15 padding-right-10 padding-bottom-15 font-0 bggray-middark">
													<p class="ongoing-worker-list no-margin-all font-14 font-400 italic padding-left-10 gray-color">No Workers Logged</p>
												</div>
									
											</div>

											<div class="width-48percent f-right ">
												<div class="padding-left-10 padding-top-15 padding-right-10 padding-bottom-15  ">
													<h4 class="font-20 font-500 gray-color">Equipment Used</h4>
												</div>
												<div class="padding-left-10 padding-top-15 padding-right-10 padding-bottom-15 font-0 bggray-middark">
													<p class="ongoing-equipment-used no-margin-all font-14 font-400 italic gray-color">No Equipment Used</p>
												</div>
											</div>
											<div class="clear"></div>
										</div>
									</div>
									<div class="clear"></div>									
								</div>
							</div>
						</div>
					</div>						
				</div>
				
				<div id="ongoing_product_list" class='product-list'>
					<div class="ongoing-product-clone-template border-top border-blue box-shadow-dark margin-bottom-20 bggray-white " style="display:none;">
						<div class="head panel-group text-left">
							<div class="panel-heading font-14 font-400 margin-top-5 margin-bottom-5">
								<a class="colapsed black-color " href="#">
									<h4 class="product-sku-name panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10 "> SKU #1234567890 - Holcim Cement</h4>
								</a>

								<div class=" f-right width-200px text-right">
									<button type="button" class="font-12 btn btn-default font-12 display-inline-mid red-color transfer-hide-cancel">Cancel</button>		
									<button class="btn general-btn display-inline-mid min-width-100px transfer-ongoing-btn">Edit</button>						
								</div>
							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body padding-all-20">
									<div class="bggray-white border-full padding-all-20 height-210px box-shadow-dark">
										<div class="product-image height-100percent display-inline-mid width-20per text-center">
											<img src="../assets/images/holcim.jpg" alt="" class="height-100percent ">
										</div>

										<!-- ================================================== -->
										<!-- ===============PRODUCT DESCRIPTION================ -->
										<!-- ================================================== -->
										<div class="display-inline-top width-80per padding-left-10">
											
											<div class="padding-all-15 bg-light-gray">
												<p class="f-left width-150px font-bold">Description</p>
												<p class="product-description f-left">Ang Cement na matibay</p>
												<div class="clear"></div>
											</div>

											<div class="padding-all-15">
												<p class="f-left no-margin-all width-150px font-bold">Transfer Method</p>
												<p class="product-transfer-method f-left transfer-display-piece">By Piece</p>

												<div class="f-left transfer-hide-piece" >
													<div class="f-left ">										
														<input type="radio" class="display-inline-mid width-20px default-cursor radio-bulk" name="bag-or-bulk">
														<label for="bulk" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>
													</div>

													<div class="f-left  margin-left-10">																	
														<!-- <input type="radio" class="display-inline-mid width-20px default-cursor " name="bag-or-bulk" id="piece" checked>
														<label for="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Piece</label> -->
														<input type="radio" class="display-inline-mid width-20px default-cursor radio-piece" name="bag-or-bulk">
														<label for ="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Piece</label>
													</div>

													<div class="clear"></div>
												</div>

												<div class="clear"></div>
											</div>

											<!-- <div class="padding-all-15 bg-light-gray">
												
												<div class="clear"></div>
											</div> -->
											
											<div class="padding-all-15 bg-light-gray">
												<p class="f-left width-150px font-bold">Unit of measure</p>
												<p class="product-unit-of-measure f-left transfer-display-piece">Test</p>

												<div class="f-left transfer-hide-piece">
													<div class="select display-inline-mid small width-105px height-26 unit-remove-dropdown">
														<div class="frm-custom-dropdown">
															<div class="frm-custom-dropdown-txt">
																<input class="dd-txt" type="text">
															</div>

															<div class="frm-custom-icon"></div>
															
															<div class="frm-custom-dropdown-option" style="display: none;">
									 								
															</div>
														</div>
														<select class="unit_of_measure_dropdown unit-of-measure-dropdown frm-custom-dropdown-origin" style="display: none;">
								 							
														</select>
														<!-- <select class="unit_of_measure_dropdown">
															
														</select> -->
													</div>
												</div>
												<div class="clear"></div>
											</div>

											
											<div class="padding-all-15">
												<!-- <p class="f-left width-150px font-bold">Aging Days</p> -->
												<!-- <p class="product-aging-days f-left width-180px">10 Days</p> -->
												<!-- <p class="f-left width-150px font-bold">Unit of measure:</p>
												<p class="product-unit-of-measure f-left">Test</p> -->
												<p class="f-left width-150px font-bold">Total QTY</p>
												<p class="total-qty-to-transfer f-left width-180px transfer-display-piece ">100 KG (10Bags)</p>
												<div class="f-left transfer-hide-piece">
													<div class="f-left text-left font-0">											
														<!-- <input type="text" class="display-inline-mid width-70px height-26 t-small" name="bag-or-bulk" value="100"> -->
														<p class="total-qty-to-transfer font-14 font-400 display-inline-mid padding-right-10">KG</p>
														
														<!-- <input type="text" class="display-inline-mid width-70px height-26 t-small" name="bag-or-bulk" value="10"> -->
														<!-- <p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">Piece</p> -->
														
														<div class="clear"></div>												
													</div>
												</div>
												<div class="clear"></div>
											</div>
											
											
											
											

										</div>
										<!-- ================================================== -->
										<!-- =============END PRODUCT DESCRIPTION============== -->
										<!-- ================================================== -->
									</div>
									<!-- <div class="product-batch-list"> -->
										
										<div class="product-batch-list padding-top-20">
											<div class="margin-bottom-10 margin-top-10">
												<p class=" f-left no-margin-all font-20 font-500  padding-left-10">Item Location</p>
												<button class="btn general-btn f-right  modal-trigger margin-bottom-10 transfer-add-batch" modal-target="add-batch">Add Batch</button>
												<div class="clear"></div>
											</div>

											
											<div class="ongoing-batch-template" style="display:none;">
												
												<p class="f-left font-14 font-bold padding-right-20 padding-left-10">Batch Name:</p>
												<p class="ongoing-batch-name f-left font-14 font-400">CNEKIEK123456 </p>
												<p class="ongoing-quantity f-right font-14 font-bold padding-bottom-20">Quantity 50 KG</p>
												<div class="clear"></div>
												<table class="tbl-4c3h">
													<thead>
														<tr>
															<th class="width-50percent black-color">Origin</th>
															<th class="width-50percent">Destination</th>
														</tr>
													</thead>
												</table>

												<div class="transfer-prod-location font-0 tbl-dark-color position-rel">
													<div class="width-100percent show-ongoing-content margin-bottom-20">
													
														<div class="batch-origin padding-all-20 width-50percent text-center display-inline-mid height-auto">
															
														</div>

														<div class="batch-destination padding-all-20 width-50percent text-center display-inline-mid height-auto">
															
														</div>

													</div>

													<div class="ongoing-edit-hide">
														<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit Batch</button>
														<button class="btn btn-remove-batch general-btn margin-left-10 padding-right-30 padding-left-30 display-inline-mid">Remove Batch</button>
													</div>
												</div>

											</div>

										</div>

									<!-- </div> -->

								</div>
							</div>
						</div>
					</div>
				</div>

		

				<!-- <div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<h4 class="panel-title  font-500 font-20 black-color padding-top-10"> Documents</h4>
							</a>
							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="upload-documents">Upload a Document</button>
							</div>
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div id="complete_document_div" class="panel-body padding-all-10">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>

								<div class="table-content position-rel tbl-dark-color">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid padding-left-10">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">Document Name</p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">View</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn">Download</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div>

								<div class="table-content position-rel">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid padding-left-10">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">Document Name</p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">View</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn">Download</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div>
																
							</div>
						</div>
					</div>
				</div> -->

				<!-- ==================== -->
				<!-- ======UPLOAD FILE=== -->
				<!-- ==================== -->
				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400 ">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title padding-top-10 font-500 font-22 black-color"> Documents</h4>
							</a>

							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="upload-documents">Upload a Document<input type="file" name="attachment" style="display: none;"></button>
							</div>


							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-20">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>

								<div id="displayDocuments" class="table-content position-rel tbl-dark-color">
									<!-- <div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">TransferDocument </p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<button class="btn general-btn padding-left-30 padding-right-30">View File</button>
										<button class="btn general-btn padding-left-30 padding-right-30 margin-left-10">Remove File</button>	
									</div> -->
								</div>								
							</div>
						</div>
					</div>
				</div>
				<!-- ==================== -->
				<!-- ======UPLOAD FILE=== -->
				<!-- ==================== -->


				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<h4 class="panel-title font-500 font-20 black-color padding-top-10"> Notes</h4>
							</a>
							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="add-note">Add Notes</button>
							</div>
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="note-list panel-body padding-all-10 view_note_list">

								<!-- <div class="border-full padding-all-10 margin-left-18">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10 font-400">Show Less</a>
									<div class="clear"></div>
								</div>

								<div class="border-full padding-all-10 margin-left-18 margin-top-10">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10 font-400">Show More</a>
									<div class="clear"></div>
								</div> -->	

							</div>
						</div>
					</div>
				</div>
		</div>
	</div>

	<!--Start of view complete transfer modal-->
<!-- <div class="modal-container modal-transfer" modal-id="view-complete-transfer">
		<div class="modal-body xlarge">				

			<div class="modal-head">
				<h4 class="text-left">Complete Transfer</h4>				
				<div class="modal-close close-me transfer-close"></div>
			</div>

			<div class="modal-content text-left">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time Start :</p>
					
					<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
						<input class="form-control dp  default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon width-0"><i class="fa fa-calendar"></i></span>
					</div>
					
					<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
						<input class="form-control dp time " placeholder="Transfer Time" type="text">
						<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
					</div>
				
				</div>

				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time Start :</p>
					
					<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
						<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon width-0"><i class="fa fa-calendar "></i></span>
					</div>

					<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
						<input class="form-control dp time" placeholder="Transfer Time" type="text">
						<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
					</div>
				</div>

				<div class="width-100percent">
					<div class="width-50per f-left">
						<div class="padding-bottom-10 padding-top-10">
							<p class="font-14 font-bold f-left no-margin-all">Worker List</p>
							<a href="#"class="display-inline-mid font-14 f-right"><i class="fa fa-user-plus font-14 padding-right-5"></i> Add Worker</a>
							
							<div class="clear"></div>
						</div>
						<div class="bggray-white padding-top-10 padding-bottom-10 height-300px">
							<div class="border-bottom-small border-black padding-bottom-10 margin-left-10">
								<input type="text" value="Drew Tanaka" class="display-inline-mid width-45per">
								<input type="text" value="Bagger" class="display-inline-mid width-45per no-margin-left">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
							<div class="border-bottom-small border-black padding-bottom-10 padding-top-10 italic margin-left-10">
								<input type="text" value="Name" class="display-inline-mid width-45per">
								<input type="text" value="Designation" class="display-inline-mid width-45per no-margin-left">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
						</div>
					</div>

					<div class="width-50per f-right">
						<div class="padding-bottom-10 padding-top-10">
							<p class="font-14 font-bold f-left no-margin-all">Equipment Used</p>
							<a href="#"class="f-right font-14"><i class="fa fa-truck font-16 padding-right-5"></i> Add Equipment</a>
							<div class="clear"></div>
						</div>
						<div class="bggray-white padding-top-10 padding-bottom-10 height-300px">
							<div class="border-bottom-small border-black padding-bottom-10 margin-left-10">
								<input type="text" value="Manual Hopper" class="display-inline-mid width-90per">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
							<div class="border-bottom-small border-black padding-bottom-10 padding-top-10 italic margin-left-10">
								<input type="text" value="Equipment Name" class="display-inline-mid width-90per">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<a href="stock-complete.php">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20  close-me">Complete Loading</button>
				</a>
			</div>
		</div>	
	</div> -->
	<!--End of view complete transfer modal-->

	<!--Start of view complete transfer modal-->
	<div class="modal-complete-transfer modal-container modal-transfer" modal-id="view-complete-transfer">
		<div class="modal-body xlarge">				

			<div class="modal-head">
				<h4 class="text-left">Complete Transfer</h4>				
				<div class="modal-close close-me transfer-close"></div>
			</div>
			<!-- content -->
			<div class="modal-content text-left">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time Start :</p>
					
					<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
						<input id="date_start" class="form-control dp  default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon width-0"><i class="fa fa-calendar"></i></span>
					</div>
					
					<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
						<input id="time_start" class="form-control dp time " placeholder="Transfer Time" type="text">
						<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
					</div>
				
				</div>

				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time End :</p>
					
					<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
						<input id="date_end" class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon width-0"><i class="fa fa-calendar "></i></span>
					</div>

					<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
						<input id="time_end" class="form-control dp time" placeholder="Transfer Time" type="text">
						<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
					</div>
				</div>

				<div class="width-100percent">
					<div class="width-50per f-left">
						<div class="padding-bottom-10 padding-top-10">
							<p class="font-14 font-bold f-left no-margin-all">Worker List</p>
							<a id="btn_add_worker" href="#" class="display-inline-mid font-14 f-right"><i class="fa fa-user-plus font-14 padding-right-5"></i> Add Worker</a>
							
							<div class="clear"></div>
						</div>
						<div id="add_worker_list" class="bggray-white padding-top-10 padding-bottom-10 height-300px">
							<!-- <div class="border-bottom-small border-black padding-bottom-10 margin-left-10">
								<input type="text" value="Drew Tanaka" class="display-inline-mid width-45per">
								<input type="text" value="Bagger" class="display-inline-mid width-45per no-margin-left">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
							<div class="border-bottom-small border-black padding-bottom-10 padding-top-10 italic margin-left-10">
								<input type="text" value="Name" class="display-inline-mid width-45per">
								<input type="text" value="Designation" class="display-inline-mid width-45per no-margin-left">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div> -->
						</div>
					</div>

					<div class="width-50per f-right">
						<div class="padding-bottom-10 padding-top-10">
							<p class="font-14 font-bold f-left no-margin-all">Equipment Used</p>
							<a id="btn_add_equipment" href="#" class="f-right font-14"><i class="fa fa-truck font-16 padding-right-5"></i> Add Equipment</a>
							<div class="clear"></div>
						</div>
						<div id="add_equipment_list" class="bggray-white padding-top-10 padding-bottom-10 height-300px">
							<!-- <div class="border-bottom-small border-black padding-bottom-10 margin-left-10">
								<input type="text" value="Manual Hopper" class="display-inline-mid width-90per">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
							<div class="border-bottom-small border-black padding-bottom-10 padding-top-10 italic margin-left-10">
								<input type="text" value="Equipment Name" class="display-inline-mid width-90per">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div> -->
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-primary font-12 f-left no-margin-all padding-left-20 padding-right-20 show-transfer-record">Show Record</button>
				<div class="f-right width-300px">
					<button type="button" class="transfer-close font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<!-- <a href="<?php echo BASEURL; ?>transfers/company_complete"> -->
						<button type="button" class="btn-submit-complete-tranfer font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Complete Loading</button>
					<!-- </a> -->
				</div>
				<div class="clear"></div>
			</div>
		</div>	
	</div>
	<!--End of view complete transfer modal-->

	<!--Start completed transfer modal-->
	<div class="modal-container" modal-id="completed-transfer">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Complete Transfer</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
				<div class="padding-all-10 bggray-7cace5">
					<p class="font-14 font-400 white-color">Transfer Details has been saved. Transfer Record is now complete.</p>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<a href="stock-complete.php">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" >Show Record</button>
				</a>
			</div>
		</div>	
	</div>

	<!--End of completed transfer modal-->

	<!--Start of Upload Document MODAL-->
	<!-- <div class="modal-container" modal-id="upload-documents">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Upload Document</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">Dcoument Name:</p>
					<input type="text" class="width-231px display-inline-mid">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">File Location:</p>
					<input type="text" class="width-231px display-inline-mid">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Browse</button>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div> -->
	<div class="modal-container" cr8v-file-upload="modal" modal-id="upload-documents">
		<div class="modal-body small">
			<div class="modal-head">
				<h4 class="text-left">Upload Document</h4>
				<div class="modal-close close-me"></div>
			</div>
			<div class="modal-content text-left" style="position:relative;">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-125px">Document Name:</p>
					<input class="width-200px display-inline-mid document-name" type="text">
					<button class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 browse" type="button">Browse</button>
				</div>
			</div>
			<div class="modal-footer text-right">
				<button class="font-12 btn btn-default font-12 display-inline-mid close-me red-color" type="button">Cancel</button>
				<button class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 confirm-upload" type="button">Confirm</button>
			</div>
		</div>
	</div>

	<!--End of Upload Document MODAL-->

	<!--Start of Add Notes MODAL-->
	<div class="modal-add-note modal-container" modal-id="add-note">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Add Notes</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
					<input id="note_subject" type="text" class="width-380px display-inline-mid">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
					<textarea id="note_message" class="width-380px margin-left-10 height-250px"></textarea>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button id="submit_add_note" type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Notes MODAL-->


	<!--Start of Add Batch MODAL-->




	<!--Start of Add Batch MODAL-->
	<div id="add_batch_modal" class="modal-container" modal-id="add-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Add Batch</h4>				
				<div class="close-add-batch-modal modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left">
					<div class="">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Item:</p>
						<p id="add_product_sku_name" class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Japanse Corn</p>
					</div>


					<div class="padding-bottom-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per margin-top-15">Batch Name: </p>
						<div class="select large">
							<select id="add_batch_name" class="transform-dd">
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div>
					<div class="padding-top-10">
						

						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Origin:</p>
						<p class="batch-storage-name font-14 font-400 no-margin-left display-inline-mid ">Storage name.</p>
						
						
					</div>
					<div class="padding-top-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Sub Location:</p>
					</div>
				</div>

				<div id="company_create_add_batch_sub_location" class="width-100percent bggray-white box-shadow-dark">

					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-65percent">Location Address</th>
								<th class="black-color width-15percent">Qty</th>
							</tr>
						</thead>
					</table>

					<div id="sub_location_batch_list">
						<div class="location-div bg-light-gray font-0 text-center position-rel default-cursor">
							<div class="location-breadcrumb width-65percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Select a batch.  </p>
								<div class="clear">		
								</div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="batch-quantity font-14 font-400 no-margin-all "></p>
							</div>
						</div>
					</div>

				</div>
				<p class="font-14 font-bold no-margin-left display-inline-mid width-25per margin-top-10">Storage Location:</p>
	            <p class="no-margin-all padding-bottom-10">Click a Location to Select It. Double-click to go to its sub-locations</p>
	            <div class="bggray-white height-300px overflow-y-auto">
	                <div class="padding-all-10 height-50px padding-bottom-10 border-bottom-small border-black" id="breadCrumbsHierarchy">
	                </div>
	                <div class="font-0" id="batchDisplay">
	                </div>
	            </div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="close-add-batch-modal font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button id="company_create_submit_add_batch" type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Batch MODAL-->






	<!--Start of edit Batch MODAL-->
	<div class="modal-container" modal-id="edit-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Edit Batch</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left">
					<div class="">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Item:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Japanse Corn</p>
					</div>
					<div class="padding-top-5">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Transfer Balance:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
					</div>
					<div class="padding-top-5">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per ">Amount to Transfer:</p>
						<input type="text" class="t-small no-margin-left">
					</div>
					<div class="">
						<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per">Batch Name: </p>
						<div class="select large">
							<select>
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div>
					<div class="padding-top-10">
						<p class="font-14 font-500 no-margin-left">Origin</p>
						<p class="font-14 font-500 display-inline-mid width-25per margin-top-10 no-margin-right">Storage Location:</p>
						<div class="select large">
							<select>
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div>
						
					</div>
				</div>
				<p class="font-14 font-500 margin-left-10">Sub Location:</p>
				<div class="width-100percent bggray-white box-shadow-dark">
					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-75percent">Location Address</th>
								<th class="black-color width-25percent">Location Qty</th>
							</tr>
						</thead>
					</table>
					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22"></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>
				</div>

				<p class="font-14 font-500 no-margin-left margin-top-20">Destination</p>
				<p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>

				<div class="bggray-white height-300px overflow-y-auto">
					<div class="padding-all-10 padding-bottom-10 border-bottom-small border-black">
						<i class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small  padding-right-10 border-black "></i>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5">Warehouse 1</p>
						<span class="display-inline-mid padding-all-5">&gt;</span>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid">Area 1</p>
					</div>
					<div class="font-0">
						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor ">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 1</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 2</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 3</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 4</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 5</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 6</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 7</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 8</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 9</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 10</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 11</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 12</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 13</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 14</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

					</div>
				</div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Edit Batch MODAL-->
	

	<!-- start of generating view modal -->
	<div class="modal-container modal-generate-view" modal-id="generate-view">
	    <div class="modal-body small">
	        <div class="modal-head">
	            <h4 class="text-left">Message</h4>
	        </div>
	        <!-- content -->
	        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
	            <div class="padding-all-10 bggray-7cace5">
	                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
	            </div>
	        </div>
	        <div class="modal-footer text-right" style="height: 50px;">
	        </div>
	    </div>
	</div>
	<!-- end of generating view modal -->
