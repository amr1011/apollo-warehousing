
		<div class="main">
			<div class="breadcrumbs width-auto ">
				<ul>
					<li><a href="withdraw-list.php">Transfer</a></li>					
					<li><span>></span></li>
					<li><strong class="black-color">Transfer No. 1575494 (Stock Goods)</strong></li>
				</ul>
			</div>
			<div class="semi-main">						
				<!-- <div class="f-right width-400px button-content">
					<button type="button" class="general-btn display-inline-mid modal-trigger margin-right-10" modal-target="mark-void">Void Record</button>
					<button type="button" class="general-btn display-inline-mid modal-trigger" modal-target="mark-record">Mark Record as Complete</button>
					<a href="edit-product-stock.php">
						<button type="button" class="general-btn display-inline-mid margin-left-10">Edit Record</button>
					</a>
				</div> -->


				<div class="clear"></div>
				<div class="border-all-small padding-all-10 padding-left-30 margin-top-15 bggray-middark  border-blue">					
					<table class="width-100percent text-left">
						<tbody>
							<tr>
								<td class="width-200px">Transfer No.:</td>
								<td class="font-bold font-20 black-color width-300px">1575494</td>
								<td class="width-150px">Transfer Status:</td>
								<td class="font-bold font-20 black-color status-result">Complete</td>
							</tr>
							<tr>
								<td>Date Issued</td>
								<td class="font-bold font-20 black-color">September 10, 2015</td>
								<td>Date Transferred:</td>
								<td class="font-bold font-20 black-color date-trans">September 11, 2015</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- 2 accordion slip  -->
				<div class="panel-group text-left ">				
					<div class="accordion_custom border_top border-top-none bggray-light box-shadow-light">
						<div class="panel-heading border-top border-blue margin-top-15 bggray-white border-remove-radius padding-all-20 box-shadow-light">
							<a class=" collapsed " href="#">
								<h4 class="panel-title active">
									Item							
								</h4>																
							</a>					
						</div>				
						<div class="panel-collapse collapse in">
							<div class="panel-body bggray-white">
								<div class="display-inline-mid">
									<table>
										<tbody>
											<tr>
												<td rowspan="3" class="width-200 padding-left-20 padding-bottom-10">
													<img src="../assets/images/sample-company-logo-2.jpg" alt="company logo" class="width-150px">
												</td>
												<td colspan="4">
													<p class="font-20 font-bold black-color">SKU # 12345678910 - Dinorado Rice</p>
												</td>
											</tr>								
											<tr>
												<td class="width-200">
													<p class="font-15 black-color font-bold">Origin Vessel:</p>
												</td>
												<td class="width-200">
													<p>MV Sand King</p>
												</td>
												<td>
													<p class="font-bold black-color font-bold">Description</p>
												</td>
												<td rowspan="2">
													<p class="padding-left-10">Dinorado Rice that was made from the sweeat and blood of the glorious 
														farmers of the motherland
													</p>
												</td>
											</tr>
											<tr>
												<td>
													<p class="font-15 black-color font-bold">Quantity in Inventory</p>
												</td>
												<td>
													<p>1000 MT</p>
												</td>
											</tr>
										</tbody>
									</table>
								</div>			
								<hr class="margin-top-20 margin-bottom-20" />					
								<div class="border-all-small padding-all-10">
									<h3 class="font-bold black-color margin-top-10 font-20 cover-batch">1</h3>	
									<table class="tbl-4c3h margin-top-20">
										<tbody>
											<tr>
												<td class="width-170px"><p class="font-bold black-color font-15">Origin:</p></td>
												<td><p class="font-bold black-color font-15">Storage Location:</p></td>
												<td><p class="">Warehouse 1</p></td>
												<td><p class="font-bold black-color font-15">Container</p></td>
												<td><p class="">Shelf 1 > Palette 1</p></td>
												<td><p class="font-bold black-color font-15">QTY in Location</p></td>												
												<td><p>100 MT</p></td>												
											</tr>
											<tr>
												<td class="width-170px"><p class="font-bold black-color font-15">Destination:</p></td>
												<td><p class="font-bold black-color font-15">Storage Location:</p></td>
												<td><p>Warehouse 1</p></td>
												<td><p class="font-bold black-color font-15">Container</p></td>
												<td><p >Shelf 1 > Palette 1</p></td>
												<td><p class="font-bold black-color font-15">QTY in Location</p></td>												
												<td><p>100 MT</p></td>											
											</tr>
											<tr>
												<td><p class="font-bold black-color font-15">Quantity to Transfer:</p></td>
												<td colspan="6"><p>50 MT</p></td>
											</tr>	
										</tbody>
									</table>
								</div>
								<div class="border-all-small padding-all-10 margin-top-15">
									<h3 class="font-bold black-color margin-top-10 font-20 cover-batch">2</h3>	
									<table class="tbl-4c3h margin-top-20 ">
										<tbody>
											<tr>
												<td class="width-170px"><p class="font-bold black-color font-15">Origin:</p></td>
												<td><p class="font-bold black-color font-15">Storage Location:</p></td>
												<td><p class="">Warehouse 1</p></td>
												<td><p class="font-bold black-color font-15">Container</p></td>
												<td><p class="">Shelf 1 > Palette 1</p></td>
												<td><p class="font-bold black-color font-15">QTY in Location</p></td>												
												<td><p>100 MT</p></td>												
											</tr>
											<tr>
												<td class="width-170px"><p class="font-bold black-color font-15">Destination:</p></td>
												<td><p class="font-bold black-color font-15">Storage Location:</p></td>
												<td><p>Warehouse 1</p></td>
												<td><p class="font-bold black-color font-15">Container</p></td>
												<td><p >Shelf 1 > Palette 1</p></td>
												<td><p class="font-bold black-color font-15">QTY in Location</p></td>												
												<td><p>100 MT</p></td>											
											</tr>
											<tr>
												<td><p class="font-bold black-color font-15">Quantity to Transfer:</p></td>
												<td colspan="6"><p>50 MT</p></td>
											</tr>	
										</tbody>
									</table>
								</div>											
							</div>			
						</div>
					</div>
				</div>		
				<div class="panel-group text-left ">
					<div class="accordion_custom border_top border-top-none bggray-light box-shadow-light ">
						<div class="panel-heading  border-top border-blue margin-top-15 bggray-white border-remove-radius padding-all-20 box-shadow-light">
							<a class=" collapsed" href="#">
								<h4 class="panel-title active">
									Transfer Information
								</h4>
							</a>												
						</div>
						<div class="panel-collapse collapse in">								
							<div class="panel-body bggray-white">
								<!-- first content -->
								<div class="f-left width-48per">
									<table class="width-100per margin-left-5">	
										<tbody>
											<tr>
												<td><p class="font-15 f-left font-bold black-color">Requested By:</p></td>
												<td><p class="font-15 f-right">Anna Karenina</p></td>
											</tr>
											<tr>
												<td><p class="font-15 f-left font-bold black-color">Reason for Transfer:</p></td>
												<td><p class="font-15 f-right">Party Provision for CEO's Birthday</p></td>
											</tr>
											
											<tr>
												<td colspan="2"><p class="font-15 font-bold black-color">Worker List:</p></td>
											</tr>
											<tr>
												<td colspan="2"><p class="padding-all-10 border-full font-15  font-bold black-color">Payloader</p></td>
											</tr>
											<tr>
												<td colspan="2"><p class="padding-all-10 border-full font-15 font-bold black-color">Hopper</p></td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- divider -->
								<div class="divider height-250px f-left margin-left-25 margin-right-15">

								</div>
								<!-- 2nd content -->
								<div class="f-left width-48per margin-left-5">
									<p class="font-15 black-color font-bold">Worker List:</p>
									<table class="width-100per">
										<tbody>
											<tr>
												<td>
													<p class="padding-all-10 border-full font-15 font-bold black-color ">Drew Tanaka</p>
												</td>
											</tr>
											<tr>
												<td>
													<p class="padding-all-10 border-full font-15 font-bold black-color">Timothy James Cruz</p>
												</td>
											</tr>
											<tr>
												<td>
													<p class="padding-all-10 border-full font-15 font-bold black-color">Luke Mejellano</p>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="clear"></div>
															
							</div>			
						</div>
					</div>					
				</div>	

				<div class="panel-group text-left">
					<div class="accordion_custom border_top border-top-none bggray-light box-shadow-light ">
						<div class="panel-heading  border-top border-blue margin-top-15 bggray-white border-remove-radius padding-all-20 box-shadow-light">
							<a class=" collapsed" href="#">
								<h4 class="panel-title ">
									Comments and Remarks
								</h4>
							</a>												
						</div>
						<div class="panel-collapse collapse ">															
							<div class="bggray-white padding-all-20 padding-left-30 text-left">									
								<textarea class="add-border-radius-5px padding-all-5 font-14" placeholder="Type in your comments and remarks here ..."></textarea>
							</div>											
						</div>
					</div>
				</div>				
			</div>
		</div>
				
	</section>
	<!-- modal here -->
	<div class="modal-container" modal-id="mark-record">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left ">Mark Record as Complete</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content">	
				<div class="">
					<p class="font-15 font-bold black-color">Are you sure you want to mark this record as complete?</p>						
				</div>
			</div>
			<div class="modal-footer text-right margin-bottom-10">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid complete-button close-me">Mark Record as Complete</button>
			</div>
		</div>	
	</div>

	<div class="modal-container" modal-id="mark-void">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left ">Mark Record as Void</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content">	
				<div class="">
					<p class="font-15 font-bold black-color">Are you sure you want to void this record?</p>
							
				</div>
			</div>
			<div class="modal-footer text-right margin-bottom-10">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid close-me void-button">Void Record</button>
			</div>
		</div>	
	</div>
