6
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL; ?>transfers/company_transfer">Transfer - Company Goods</a></li>
					<li><span>&gt;</span></li>						
					<li class="edit-breadcrumb font-bold black-color">Edit Transfer No. 1234567890</li>
				</ul>
			</div>
			<div class="semi-main">
				<div class="padding-top-30 margin-bottom-30 text-right width-100percent">
					<a href="<?php echo BASEURL; ?>transfers/company_ongoing">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid  red-color">Cancel</button>
					</a>
					<!-- <a href="stock-ongoing.php"> -->
						<button class="btn general-btn btn-submit-edit">Save Changes</button>
					<!-- </a> -->
				</div>
				<div class="clear"></div>
				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0">
					<p class="edit-transfer-number font-20 font-bold black-color padding-bottom-20">Transfer No. 1234567890</p>
					
					<div class="width-50percent display-inline-top">
						<div class="padding-bottom-10">
							<p class="font-14 font-bold display-inline-mid width-120">Requested By: </p>
							<input id="edit_requested_by" type="text" class="display-inline-mid width-340 t-small">
						</div>

						<div class="padding-top-10">
							<p class="font-14 font-bold display-inline-top width-120">Reason: </p>
							<textarea id="edit_reason" class="width-340"></textarea>
						</div>

					</div>

					<div class="width-50percent display-inline-top">
						

						<div class="padding-bottom-10">
							<p class="font-14 font-bold display-inline-mid width-150">Mode Of Transfer: </p>
							<div class="select large">
								<select id="edit_mode_of_transfer">
									<option value="Manual Labor">Manual Labor</option>
								</select>
							</div>
						</div>
					</div>					
				</div>

				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white padding-bottom-20 ">
					<div class="width-100percent text-left padding-all-30 no-padding-top no-padding-left margin-bottom-30 border-bottom-small border-gray">
						<p class="font-20 font-bold black-color padding-bottom-20 ">Item List</p>
						<p class="font-14 font-bold display-inline-mid padding-right-20">Item: <span class="red-color display-inline-top">*</span></p>
						<div class="select dispaly-inline-mid large">
							<select id="edit_product_list_dropdown">
								<option value="Select">Select Item</option>
							</select>
						</div>
						<a href="#" class="display-inline-mid padding-left-20">
							<button class="btn-add-product btn general-btn padding-left-20 padding-right-20">Add Item</button>
						</a>
					</div>

					<div class="edit-product-master-list">
						
						
						<div class="edit-small-product-list">
						
							<div class="border-full padding-all-20 edit-small-product-template margin-top-10" style="display:none;">
								<div class="bggray-white text-left">
									<div class="small-product-image-display height-100percent display-inline-mid width-10per">
										<img src="../assets/images/holcim.jpg" alt="" class="height-100percent width-100percent">
									</div>

									<div class="display-inline-top width-90per padding-left-10">
										<div class="padding-top-15 padding-left-15 padding-bottom-15">
											<p class="small-product-sku-and-name font-16 font-bold f-left">SKU# 1234567890 - Holcim Cement</p>
											<div class="remove-product small-product-image f-right width-20px margin-left-10">
												<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
											</div>
											<div class="clear"></div>
										</div>																						
									</div>
								</div>					
							</div>

						</div>
						
						<div class="edit-product-list">
							
							<div class="edit-product-original-template border-full padding-all-20 margin-top-20" data-id="" style="display:none;">
								<div class="bggray-white height-190px">
									<div class="product-image height-100percent display-inline-mid width-230px">
										<img src="../assets/images/steel.jpg" alt="" class="height-100percent width-100percent">
									</div>

									<div class="display-inline-top width-75percent padding-left-10">
										<div class="padding-all-15 bg-light-gray">
											<p class="font-16 font-bold f-left product-sku-name">SKU# 1234567890 - Steel Bars</p>
											<div class="f-right width-20px margin-left-10 remove-product">
												<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
											</div>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 text-left">
											<p class="f-left width-20percent font-bold">Description</p>
											<p class="product-description f-left">Tough Steel</p>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 text-left bg-light-gray">
											<p class="f-left no-margin-all width-20percent font-bold">Transfer Method</p>
					
											<div class="f-left">										
												<input type="radio" class="display-inline-mid width-50px default-cursor radio-bulk" name="bag-or-bulk">
												<label for="bulk" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>
											</div>

											<div class="f-left margin-left-10">																	
												<input type="radio" class="display-inline-mid width-50px default-cursor radio-piece" name="bag-or-bulk">
												<label for ="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Piece</label>
											</div>								
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 text-left">
											<p class="f-left width-20percent font-bold">Unit of Measure: </p>
											<div class="select display-inline-mid small width-70px height-26 unit-remove-dropdown">
												<div class="frm-custom-dropdown">
													<div class="frm-custom-dropdown-txt">
														<input class="dd-txt" type="text">
													</div>

													<div class="frm-custom-icon"></div>
													
													<div class="frm-custom-dropdown-option" style="display: none;">
							 								
													</div>
												</div>
												<select class="unit_of_measure_dropdown frm-custom-dropdown-origin" style="display: none;">
						 							<!-- <option value="1">#000000001 Cobra</option> -->
													<!-- <option value="2">#000000002 Samurai</option> -->
												</select>
											</div>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 bg-light-gray text-left font-0">

											<p class="f-left width-20percent font-bold">Total QTY</p>
											<p class="f-left total-qty-to-transfer"></p>
											<div class="clear"></div>

											<div class="clear"></div>
										</div>
										
										
										<!-- <div class="padding-all-15 bg-light-gray text-left font-0">
											<p class="display-inline-mid width-20percent font-bold">Aging Days</p>
											<p class="product-aging-days display-inline-mid width-20percent">10 Days</p>
											<p class="display-inline-mid width-25per font-14 font-bold">Qty to Transfer</p>
											
											<input type="text" class="product-qty-bulk display-inline-mid width-70px height-26 t-small" name="bag-or-bulk">
											<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">KG</p>
											
											<input type="text" class="product-qty-piece display-inline-mid width-70px height-26 t-small" name="bag-or-bulk" >
											<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">Piece</p>
											
											<div class="clear"></div>
										</div>
										
										
										<div class="padding-all-15 text-left">
											<p class="f-left width-20percent font-bold">Description</p>
											<p class="product-description f-left">Tough Steel</p>
											<div class="clear"></div>
										</div> -->
										
									</div>
								</div>

								<div class="padding-top-20">
									<div class="padding-top-20 padding-bottom-20">
										<p class="font-20 font-500 f-left">Item Location</p>
										<div class="f-right">
											<button class="btn btn-add-batch general-btn modal-trigger margin-top-30" modal-target="add-batch">Add Batch</button>
										</div>
										<div class="clear"></div>
									</div>

									<div class="product-batch-display">
										
										<!-- PRODUCT BATCH TEMPLATE -->
										<div class="product-batch-template" style="display:none;">
											<p class="f-left font-14 font-bold padding-right-20">Batch Name:</p>
											<p class="batch-name f-left font-14 font-400">CNEKIEK123456</p>
											<p class="batch-quantity f-right font-14 font-bold padding-bottom-20">Quantity 50 KG</p>
											<div class="clear"></div>
											<table class="tbl-4c3h">
												<thead>
													<tr>
														<th class="width-50percent black-color">Origin</th>
														<th class="width-50percent">Destination</th>
													</tr>
												</thead>
											</table>
											<div class="batch-locations font-0 tbl-dark-color margin-bottom-20 position-rel">
												<div class="width-100percent ">
													<div class="batch-origin padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25">
														<!-- <p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p> -->
													</div>

													<div class="batch-destination padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25">
														<!-- <p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p> -->
													</div>
												</div>
												<div class="edit-hide">
													<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit Batch</button>
													<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10" >Remove Batch</button>
												</div>
											</div>
											
										</div>
										<!-- END OF PRODUCT BATCH TEMPLATE -->

									</div>
									
								</div>
							</div>

						</div>


					</div>

					
					
				</div>

				
				<div class="width-100percent text-right border-top-small border-gray padding-top-10">
					<a href="<?php echo BASEURL; ?>transfers/company_ongoing">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					</a>
					<!-- <a href="stock-ongoing.php"> -->
						<button class="btn general-btn btn-submit-edit">Save Changes</button>
					<!-- </a> -->
				</div>
		</div>
	</div>

	<!--MODALS-->


		<!--Start of Add Batch MODAL-->
	<div id="add_batch_modal" class="modal-container" modal-id="add-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Add Batch</h4>				
				<div class="close-add-batch-modal modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left">
					<div class="">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Item:</p>
						<p id="add_product_sku_name" class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Japanse Corn</p>
					</div>


					<div class="padding-bottom-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per margin-top-15">Batch Name: </p>
						<div class="select large">
							<select id="add_batch_name" class="transform-dd">
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div>
					<div class="padding-top-10">
						

						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Origin:</p>
						<p class="batch-storage-name font-14 font-400 no-margin-left display-inline-mid ">DITO ANG STORAGE.</p>
						
						
					</div>
					<div class="padding-top-10">
						<p class="font-14 font-bold no-margin-left display-inline-mid width-25per">Sub Location:</p>
					</div>
				</div>

				<div id="company_create_add_batch_sub_location" class="width-100percent bggray-white box-shadow-dark">

					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-65percent">Location Address</th>
								<th class="black-color width-15percent">Qty</th>
							</tr>
						</thead>
					</table>

					<div id="sub_location_batch_list">
						
					</div>

				</div>
				<p class="font-14 font-bold no-margin-left display-inline-mid width-25per margin-top-10">Storage Location:</p>
	            <p class="no-margin-all padding-bottom-10">Click a Location to Select It. Double-click to go to its sub-locations</p>
	            <div class="bggray-white height-300px overflow-y-auto">
	                <div class="padding-all-10 height-50px padding-bottom-10 border-bottom-small border-black" id="breadCrumbsHierarchy">
	                </div>
	                <div class="font-0" id="batchDisplay">
	                </div>
	            </div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="close-add-batch-modal font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button id="company_create_submit_add_batch" type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>



	<!-- Start of Edit Batch Modal  -->
	<div class="modal-container" modal-id="edit-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Edit Batch</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Item:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Holcim Cement</p>
					</div>
					<div class="">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Transfer Balance:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
					</div>
					<div class="">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per margin-top-15">Amount to Transfer:</p>
						<input type="text" class="t-small no-margin-left">
					</div>
					<div class="">
						<p class="font-14 font-500 no-margin-left margin-top-10 display-inline-mid width-25per">Batch Name: </p>
						<div class="select large">
							<select>
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div>
					<div class="padding-top-10">
						<p class="font-14 font-500 no-margin-left">Origin</p>
						<p class="font-14 font-500 display-inline-mid width-25per margin-top-10 no-margin-right">Storage Location:</p>
						<div class="select large">
							<select>
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div>
						
					</div>
				</div>
				<p class="font-14 font-500">Sub Location:</p>
				<div class="width-100percent bggray-white box-shadow-dark">

					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-75percent">Location Address</th>
								<th class="black-color width-25percent">Location Qty</th>
							</tr>
						</thead>
					</table>
					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22"></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>
				</div>

				<p class="font-14 font-400 no-margin-left margin-top-20">Destination</p>
				<p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>

				<div class="bggray-white height-300px overflow-y-auto">
					<div class="padding-all-10 padding-bottom-10 border-bottom-small border-black">
						<i class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small  padding-right-10 border-black "></i>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5">Warehouse 1</p>
						<span class="display-inline-mid padding-all-5">&gt;</span>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid">Area 1</p>
					</div>
					<div class="font-0">
						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor ">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 1</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 2</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 3</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 4</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 5</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 6</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 7</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 8</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 9</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 10</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 11</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 12</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 13</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 14</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

					</div>
				</div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!-- end of edit batch modal  -->

	<!-- start of generating view modal -->
	<div class="modal-container modal-generate-view" modal-id="generate-view">
	    <div class="modal-body small">
	        <div class="modal-head">
	            <h4 class="text-left">Message</h4>
	        </div>
	        <!-- content -->
	        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
	            <div class="padding-all-10 bggray-7cace5">
	                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
	            </div>
	        </div>
	        <div class="modal-footer text-right" style="height: 50px;">
	        </div>
	    </div>
	</div>
	<!-- end of generating view modal -->