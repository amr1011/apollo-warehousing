<div class="main">
			<div class="semi-main">
				<div class="f-right margin-top-30 margin-bottom-30">
					<button class="btn general-btn modal-trigger" modal-target="view-complete-transfer">Complete Transfer</button>
				</div>
				<div class="clear"></div>
				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white">
					<div class="display-inline-mid width-50per">
						<div class="text-left padding-bottom-10">
							<p class="f-left font-14 font-bold width-40percent">Origin:</p>
							<p class="f-left font-14 font-400 width-50percent">MV Sand King</p>
							<div class="clear"></div>
						</div>

						<div class="text-left padding-top-10 padding-bottom-10">
							<p class="f-left font-14 font-bold width-40percent">Mode Of Transfer:</p>
							<p class="f-left font-14 font-400 width-50percent">Manual Labor</p>
							<div class="clear"></div>
						</div>

						<div class="text-left padding-top-10">
							<p class="f-left font-14 font-bold width-40percent">Reason For Transfer:</p>
							<p class="f-left font-14 font-400 width-50percent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretrium tempor. Ut eget imperdiet neque.</p>
							<div class="clear"></div>
						</div>


					</div>

					<div class="display-inline-top width-50per">
						<div class="text-left padding-bottom-10">
							<p class="f-left font-14 font-bold width-40percent">Date And Time Issued:</p>
							<p class="f-left font-14 font-400 width-50percent">September 10, 2015 | 08:25 AM</p>
							<div class="clear"></div>
						</div>

						<div class="text-left height-50px ">
							<p class="f-left font-14 font-bold width-40percent padding-top-5 margin-top-3">Requested By:</p>
							<div class="position-rel display-inline-mid text-center height-65percent">
								<img src="../assets/images/profile/profile_x.png" alt="images" class="height-100percent display-inline-mid">
								<p class="display-inline-mid font-14 font-400">Julie Mendez</p>
							</div>
							
							<div class="clear"></div>
						</div>

						<div class="text-left">
							<p class="f-left font-14 font-bold width-40percent">Status:</p>
							<p class="f-left font-14 font-400 width-50percent">Ongoing</p>
							<div class="clear"></div>
						</div>

					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading no-padding-left font-14 font-400 margin-bottom-20">
							<a class="colapsed black-color" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title font-bold font-20 black-color"> SKU #1234567890 - Japanese Corn</h4>
							</a>
						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body no-padding-all">
								<div class="bggray-white border-full padding-all-20 height-190px box-shadow-dark">
									<div class="height-100percent display-inline-mid">
										<img src="../assets/images/corn.jpg" alt="" class="height-100percent">
									</div>

									<div class="display-inline-top width-75percent padding-left-10">
										
										<div class="padding-all-15 bggray-middark">
											<p class="f-left no-margin-all width-165px font-bold">Vendor</p>
											<p class="f-left no-margin-all width-180px">Innova Farming</p>
											<p class="f-left no-margin-all width-165px font-bold">Qty to Withdraw</p>
											<p class="f-left no-margin-all">100 KG(10Bags)</p>
											<div class="clear"></div>
										</div>
										
										
										<div class="padding-all-15">
											<p class="f-left width-165px font-bold">Aging Days</p>
											<p class="f-left width-165px">10 Days</p>
											<div class="clear"></div>
										</div>
										
										
										<div class="padding-all-15 bggray-middark">
											<p class="f-left width-165px font-bold">Loading Method</p>
											<p class="f-left">By Bags/Sack</p>
											<div class="clear"></div>
										</div>
										
									</div>
								</div>

								<div class="padding-top-20">
									<p class="no-margin-all font-20 font-500 width-100percent padding-bottom-20">Item Location</p>
									<p class="f-left font-14 font-bold padding-right-20">Batch Name:</p>
									<p class="f-left font-14 font-400">Sample Batch Name </p>
									<p class="f-right font-14 font-bold padding-bottom-20">Quantity 50 KG</p>
									<div class="clear"></div>
									<table class="tbl-4c3h">
										<thead>
											<tr>
												<th class="width-50percent black-color">Oirigin</th>
												<th class="width-50percent">Destination</th>
											</tr>
										</thead>
									</table>
									<div class="font-0 bggray-middark margin-bottom-20">
										<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto">
											<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
										</div>
										<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto">
											<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
										</div>
									</div>

									<p class="f-left font-14 font-bold padding-right-20">Batch Name:</p>
									<p class="f-left font-14 font-400">Sample Batch Name</p>
									<p class="f-right font-14 font-bold padding-bottom-20">Quantity 50 KG</p>
									<div class="clear"></div>
									<table class="tbl-4c3h">
										<thead>
											<tr>
												<th class="width-50percent black-color">Oirigin</th>
												<th class="width-50percent">Destination</th>
											</tr>
										</thead>
									</table>
									<div class="font-0 bggray-middark">
										<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto">
											<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
										</div>
										<div class="padding-all-20 width-50percent text-center display-inline-mid height-auto">
											<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
											<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
											<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading no-padding-left font-14 font-400 margin-bottom-20">
							<a class="colapsed black-color" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title font-bold font-20 black-color"> Transfer Information</h4>
							</a>
						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body no-padding-all">
								<div class="padding-all-10 bggray-middark font-0">
									<p class="no-margin-all font-14 font-bold width-25percent display-inline-mid">Loading Date / Time Start</p>
									<p class="no-margin-all font-14 width-26-6percent display-inline-mid">Not Yet Assigned</p>
									<p class="no-margin-all font-14 font-bold width-25percent display-inline-mid">Loading Duration</p>
									<p class="no-margin-all font-14 width-23-4percent display-inline-mid">Not Yet Assigned</p>
								</div>

								<div class="padding-all-10 width-50percent font-0 no-padding-right">
									<p class="no-margin-all font-14 font-bold width-50percent display-inline-mid">Loading Date / Time Start</p>
									<p class="no-margin-all font-14 width-50percent display-inline-mid">Not Yet Assigned</p>
									
								</div>

								<div class="font-0">
									<div class="width-48percent f-left">
										<div class="bggray-middark padding-bottom-10 padding-top-10">
											<h4 class="font-14 font-bold  padding-left-10 transfer-information display-inline-mid width-90per">Workers List</h4>
										</div>
										<div class=" padding-all-10 font-0">
											<p class="no-margin-all font-14 font-400 italic">No Workers Logged</p>
										</div>
							
									</div>

									<div class="width-48percent f-right ">
										<div class="bggray-middark ">
											<h4 class="font-14 font-bold padding-bottom-10 padding-top-10 padding-left-10">Equipment Used</h4>
										</div>
										<div class="padding-all-10 font-0">
											<p class="no-margin-all font-14 font-400 italic">No Equipment Used</p>
										</div>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading no-padding-left font-14 font-400 no-padding-top">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-bold font-20 black-color padding-top-10"> Documents</h4>
							</a>

							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="upload-documents">Upload a Document</button>
							</div>
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body no-padding-all">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>

								<div class="table-content position-rel">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">Document Name</p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">View</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn">Download</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div>

								<div class="table-content position-rel">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">Document Name</p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">View</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn">Download</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div>
																
							</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading no-padding-left font-14 font-400 no-padding-top">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-bold font-20 black-color"> Notes</h4>
							</a>
							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="add-note">Add Notes</button>
							</div>
							<div class="clear"></div>
						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body no-padding-all">

								<div class="border-full padding-all-10 margin-left-18">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10">Show Less</a>
									<div class="clear"></div>
								</div>

								<div class="border-full padding-all-10 margin-left-18 margin-top-10">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10">Show More</a>
									<div class="clear"></div>
								</div>							
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>

	<!--Start of view complete transfer modal-->
	<div class="modal-container" modal-id="view-complete-transfer">
		<div class="modal-body xlarge">				

			<div class="modal-head">
				<h4 class="text-left">Complete Transfer</h4>				
				<div class="modal-close close-me"></div>
			</div>
			<!-- content -->
			<div class="modal-content text-left">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid no-margin-all">Loading Date / Time Start :</p>
					
					<div class="input-group width-200px italic display-inline-mid">
						<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon border-big-tr border-big-br width-0"><i class="fa fa-calendar gray-color"></i></span>
					</div>

					<div class="input-group width-200px italic display-inline-mid ">
						<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon border-big-tr border-big-br width-0"><i class="fa fa-clock-o gray-color"></i></span>
					</div>
				</div>

				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid no-margin-all">Loading Date / Time Start :</p>
					
					<div class="input-group width-200px italic display-inline-mid">
						<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon border-big-tr border-big-br width-0"><i class="fa fa-calendar gray-color"></i></span>
					</div>

					<div class="input-group width-200px italic display-inline-mid">
						<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
						<span class="input-group-addon border-big-tr border-big-br width-0"><i class="fa fa-clock-o gray-color"></i></span>
					</div>
				</div>

				<div class="width-100percent">
					<div class="width-50per f-left">
						<div class="padding-bottom-10 padding-top-10">
							<p class="font-14 font-bold f-left no-margin-all">Worker List</p>
							<a href="#"class="f-right font-14">+ Add Worker</a>
							<div class="clear"></div>
						</div>
						<div class="bggray-white padding-top-10 padding-bottom-10 height-300px">
							<div class="border-bottom-small border-black padding-bottom-10">
								<input type="text" value="Drew Tanaka" class="display-inline-mid width-45per">
								<input type="text" value="Bagger" class="display-inline-mid width-45per no-margin-left">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
							<div class="border-bottom-small border-black padding-bottom-10 padding-top-10 italic">
								<input type="text" value="Name" class="display-inline-mid width-45per">
								<input type="text" value="Designation" class="display-inline-mid width-45per no-margin-left">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
						</div>
					</div>

					<div class="width-50per f-right">
						<div class="padding-bottom-10 padding-top-10">
							<p class="font-14 font-bold f-left no-margin-all">Equipment Used</p>
							<a href="#"class="f-right font-14">+ Add Equipment</a>
							<div class="clear"></div>
						</div>
						<div class="bggray-white padding-top-10 padding-bottom-10 height-300px">
							<div class="border-bottom-small border-black padding-bottom-10">
								<input type="text" value="Manual Hopper" class="display-inline-mid width-90per">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
							<div class="border-bottom-small border-black padding-bottom-10 padding-top-10 italic">
								<input type="text" value="Equipment Name" class="display-inline-mid width-90per">
								<div class="display-inline-mid width-15px margin-left-10">
									<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of view complete transfer modal-->

	<!--Start of Upload Document MODAL-->
	<div class="modal-container" modal-id="upload-documents">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Upload Document</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">Dcoument Name:</p>
					<input type="text" class="width-231px display-inline-mid">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">File Location:</p>
					<input type="text" class="width-231px display-inline-mid">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Browse</button>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Upload Document MODAL-->

	<!--Start of Add Notes MODAL-->
	<div class="modal-container" modal-id="add-note">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Add Notes</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
					<input type="text" class="width-380px display-inline-mid">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
					<textarea class="width-380px margin-left-10 height-250px"></textarea>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Notes MODAL-->