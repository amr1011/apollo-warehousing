	
		<div class="main">
			<div class="center-main">
				<div class="mod-select padding-left-3 padding-top-60">			
					<div class="select f-left width-425px ">
						<select>
							<option value="Displaying All Consignee Transfer Records"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>						
				</div>
				<div class="f-right margin-top-10">	
					<a href="<?php echo BASEURL; ?>transfers/company_create">
						<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn">Create Record</button>				
					</a>
					<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn dropdown-btn">View Filter / Search</button>			
				</div>
				<div class="clear"></div>

				<div class="dropdown-search">
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr class="width-100percent">
								<td class="width-75px">Search</td>
								<td>
									<div class="select width-200px font-400">
										<select>
											<option value="items">Items</option>
										</select>
									</div>
								</td>
								<td class="width-100percent">
									<input type="text" />
								</td>
							</tr>
							<tr class="width-100percent">
								<td>Filter By:</td>
								<td>
									<div class="input-group width-200px f-left italic">
									<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
									<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar white-color">
									</span></span>
									</div>
								</td>
								<td class="search-action">
									<div class="f-left">
										
										<div class="select width-150px italic">
											<select>
												<option value="status">Status</option>
											</select>
										</div>
									</div>
									<div class="f-right margin-left-30">
										<p class="display-inline-mid">Sort By:</p>
										<div class="select width-150px display-inline-mid italic">
											<select>
												<option value="withdrawal No." >Transfer No.</option>
											</select>
										</div>
										<div class="select width-150px display-inline-mid italic">
											<select>
												<option value="ascending">Ascending</option>
												<option value="descending">Descending</option>
											</select>
										</div>
									</div>
									<div class="clear"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10">
						<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
						<p class="display-inline-mid font-4 font-bold">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 ">Search</button>
					<div class="clear"></div>
				</div>

				<div id="company_transfer_list" class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left margin-top-30">

					<div class="width-100percent bggray-white">

						<table class="tbl-4c3h margin-top-30">
							<thead>
								<tr>
									<th class="width-15percent black-color no-padding-left">Transfer No.</th>
									<th class="width-15percent black-color no-padding-left">Transfer Date</th>
									<th class="width-40percent black-color no-padding-left">Item Description</th>
									<th class="width-15percent black-color no-padding-left">Owner</th>
									<th class="width-15percent black-color no-padding-left">Status</th>												
								</tr>
							</thead>
						</table>

					</div>
				</div>

			</div>
		</div>

		<!--Start of view complete transfer modal-->
		<div class="modal-complete-transfer modal-container modal-transfer" modal-id="view-complete-transfer">
			<div class="modal-body xlarge">				

				<div class="modal-head">
					<h4 class="text-left">Complete Transfer</h4>				
					<div class="modal-close close-me transfer-close"></div>
				</div>
				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-bottom-10">
						<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time Start :</p>
						
						<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
							<input id="date_start" class="form-control dp  default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon width-0"><i class="fa fa-calendar"></i></span>
						</div>
						
						<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
							<input id="time_start" class="form-control dp time " placeholder="Transfer Time" type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>
					
					</div>

					<div class="padding-bottom-10">
						<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time End :</p>
						
						<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
							<input id="date_end" class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon width-0"><i class="fa fa-calendar "></i></span>
						</div>

						<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
							<input id="time_end" class="form-control dp time" placeholder="Transfer Time" type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>
					</div>

					<div class="width-100percent">
						<div class="width-50per f-left">
							<div class="padding-bottom-10 padding-top-10">
								<p class="font-14 font-bold f-left no-margin-all">Worker List</p>
								<a id="btn_add_worker" href="#" class="display-inline-mid font-14 f-right"><i class="fa fa-user-plus font-14 padding-right-5"></i> Add Worker</a>
								
								<div class="clear"></div>
							</div>
							<div id="add_worker_list" class="bggray-white padding-top-10 padding-bottom-10 height-300px">
								<!-- <div class="border-bottom-small border-black padding-bottom-10 margin-left-10">
									<input type="text" value="Drew Tanaka" class="display-inline-mid width-45per">
									<input type="text" value="Bagger" class="display-inline-mid width-45per no-margin-left">
									<div class="display-inline-mid width-15px margin-left-10">
										<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
									</div>
								</div>
								<div class="border-bottom-small border-black padding-bottom-10 padding-top-10 italic margin-left-10">
									<input type="text" value="Name" class="display-inline-mid width-45per">
									<input type="text" value="Designation" class="display-inline-mid width-45per no-margin-left">
									<div class="display-inline-mid width-15px margin-left-10">
										<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
									</div>
								</div> -->
							</div>
						</div>

						<div class="width-50per f-right">
							<div class="padding-bottom-10 padding-top-10">
								<p class="font-14 font-bold f-left no-margin-all">Equipment Used</p>
								<a id="btn_add_equipment" href="#" class="f-right font-14"><i class="fa fa-truck font-16 padding-right-5"></i> Add Equipment</a>
								<div class="clear"></div>
							</div>
							<div id="add_equipment_list" class="bggray-white padding-top-10 padding-bottom-10 height-300px">
								<!-- <div class="border-bottom-small border-black padding-bottom-10 margin-left-10">
									<input type="text" value="Manual Hopper" class="display-inline-mid width-90per">
									<div class="display-inline-mid width-15px margin-left-10">
										<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
									</div>
								</div>
								<div class="border-bottom-small border-black padding-bottom-10 padding-top-10 italic margin-left-10">
									<input type="text" value="Equipment Name" class="display-inline-mid width-90per">
									<div class="display-inline-mid width-15px margin-left-10">
										<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
									</div>
								</div> -->
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 f-left no-margin-all padding-left-20 padding-right-20 show-transfer-record">Show Record</button>
					<div class="f-right width-300px">
						<button type="button" class="transfer-close font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<!-- <a href="<?php echo BASEURL; ?>transfers/company_complete"> -->
							<button type="button" class="btn-submit-complete-tranfer font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20  close-me">Complete Loading</button>
						<!-- </a> -->
					</div>
					<div class="clear"></div>
				</div>
			</div>	
		</div>
		<!--End of view complete transfer modal-->


		<!-- start of generating view modal -->
	<div class="modal-container modal-generate-view" modal-id="generate-view">
	    <div class="modal-body small">
	        <div class="modal-head">
	            <h4 class="text-left">Message</h4>
	        </div>
	        <!-- content -->
	        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
	            <div class="padding-all-10 bggray-7cace5">
	                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
	            </div>
	        </div>
	        <div class="modal-footer text-right" style="height: 50px;">
	        </div>
	    </div>
	</div>
	<!-- end of generating view modal -->