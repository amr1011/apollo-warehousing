<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transfers extends MX_Controller {


	public function __construct()
	{
		parent:: __construct();
		$this->load->library('MY_Form_validation');
        $this->form_validation->CI =& $this;
		$this->load->model('transfers_model');
		// $this->load->model('products/products_model');
		$response = array(
			"status" => false,
			"message" => "message here",
			"data" => null
		);
	}

	public function index()
	{

		$user_session = $this->session->userdata('apollo_user');
		if($user_session === null){
			redirect(BASEURL.'login');
		}else{
			$this->transfer_list();
		}


		// if(IS_DEVELOPMENT === true)
		// {
		// 	$user_development = array(
		// 		"id" => 10000,
		// 		"email" => "apollo@email.com",
		// 		"username" => "apollo",
		// 		"password" => "62cc2d8b4bf2d8728120d052163a77df",
		// 		"company_id" => 1,
		// 		"role_id" => 1,
		// 		"account_type" => 1,
		// 		"first_name" => "apollo",
		// 		"last_name" => "developer",
		// 		"middle_name" => ""
		// 	);

		// 	$this->session->set_userdata('apollo_user', $user_development);

		// 	redirect(BASEURL . 'transfers/transfer_list');
		// 	// $this->transfers();
		// }
		// else
		// {
		// 	/*LOGIN PROCESS*/
		// }
	}



	// New Updates

	public function stock_transfer()
	{
		$data["title"] = "TRANSFER - STOCK GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/stock_transfer_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consign_transfer()
	{
		$data["title"] = "TRANSFER - CONSIGNEE GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/consign_transfer_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consign_create()
	{
		$data["title"] = "TRANSFER - CONSIGNEE GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/consign_create_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consignee_ongoing()
	{
		$data["title"] = "TRANSFER - CONSIGNEE GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/consignee_ongoing_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consign_ongoing_edit()
	{
		$data["title"] = "TRANSFER - CONSIGNEE GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/consign_ongoing_edit_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consign_complete()
	{
		$data["title"] = "TRANSFER - CONSIGNEE GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/consign_complete_view", $data);
		$this->load->view("includes/footer.php");
	}

	// End

	public function transfer_list()
	{
		$data["title"] = "Transfer";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/transfers_list_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function view_record_stock()
	{
		$data["title"] = "Transfers";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/view_record_stock_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function create_transfer()
	{
		$data["title"] = "Create Transfer";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/create_transfer_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function view_transfer_ongoing()
	{
		$data["title"] = "Create Transfer";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/view_transfer_ongoing_view", $data);
		$this->load->view("includes/footer.php");
	}

	//==================================================//
	//=================COMPANY GOODS====================//
	//==================================================//

	// VIEWS

	public function company_transfer() 
	{
		$data["title"] = "TRANSFER - COMPANY GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/company_transfer", $data);
		$this->load->view("includes/footer.php");
	}

	public function company_create() 
	{
		$data["title"] = "TRANSFER - COMPANY GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/company_create", $data);
		$this->load->view("includes/footer.php");
	}

	public function company_ongoing() 
	{
		$data["title"] = "TRANSFER - COMPANY GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/company_ongoing", $data);
		$this->load->view("includes/footer.php");
	}

	public function company_complete() 
	{
		$data["title"] = "TRANSFER - COMPANY GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/company_complete", $data);
		$this->load->view("includes/footer.php");
	}

	public function company_ongoing_edit()
	{
		$data["title"] = "TRANSFER - COMPANY GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("transfers/company_ongoing_edit", $data);
		$this->load->view("includes/footer.php");
	}

	// METHODS

	public function get_all_company_transfers() 
	{
		$this->load->model("transfers_model");
		$result = $this->transfers_model->get_all_company_transfers();
		
		if($result > 0) {
			
			foreach ($result as $key => $row) {

				$date_issued = strtotime($row["date_issued"]);
				$date = date('M d, Y', $date_issued);
				$transfer_date = date('d-M-Y', $date_issued);
				$time = date('g:i A', $date_issued);

				//FORMATS THE DATE TO *Apr 30, 2016 | 7:30 AM*
				$result[$key]["date_issued"] = $date . " | " . $time;
				//FORMATS THE DATE TO *30-Apr-2016*
				$result[$key]["transfer_date"] = $transfer_date;

				//CALCULATES THE DAY AND HOUR INTERVAL OF DATE_START & DATE_COMPLETE
				$datetime1 = date_create($row["date_start"]);
				$datetime2 = date_create($row["date_complete"]);
				$interval = date_diff($datetime1, $datetime2);
				$result[$key]["date_interval"] = $interval->format('%a days, %h hours');

				//FORMATS THE DATE_START TO *19-Apr-2016 12:00 AM*
				$date_start = strtotime($row["date_start"]);
				$s_date = date('d-M-Y', $date_start);
				$s_time = date('g:i A', $date_start);
				$result[$key]["date_start"] = $s_date . ' ' . $s_time;

				//FORMATS THE DATE_COMPLETE TO *19-Apr-2016 12:00 AM*
				$date_complete = strtotime($row["date_complete"]);
				$c_date = date('d-M-Y', $date_complete);
				$c_time = date('g:i A', $date_complete);
				$result[$key]["date_complete"] = $c_date . ' ' . $c_time;

				$product_list = $this->transfers_model->get_product_list($row["id"]);
				foreach ($product_list as &$product) {
					$product["details"] = $this->transfers_model->get_product_details($product["product_id"]);
				}
				$result[$key]["product_list"] = $product_list;
			}

			$response = array(
				"status" => true,
				"message" => "",
				"data" => $result	
			);
		} else {
			$response = array(
				"status" => false,
				"message" => "",
				"data" => $result	
			);
		}
		header("Content-Type: application/json");
		echo json_encode($response);
	}

	public function get_product_list() 
	{
		$this->load->model('products/products_model');
		$result = $this->products_model->get_all_products();
		$response = array(
			"status" => true,
			"message" => "",
			"data" => $result
		);
		header("Content-Type: application/json");
		echo json_encode($response);
	}



	public function generate_transfer_number() 
	{
	    $this->load->model('transfers_model');
	    $result = $this->transfers_model->get_transfer_number();
	    
	    // print_r($result);
	    if($result > 0) {
	      // $new_transfer_number = $result[0]["to_number"] + 1;
	      $new_transfer_number = $result + 1;
	    } else {
	      $new_transfer_number = "000000001";
	    }

	    $response = array(
	      "status" => true,
	      "message" => "",
	      "data" => $new_transfer_number
	      // "data" => $result
	    );
	    header("Content-Type: application/json");
	    echo json_encode($response);
	}

	

	public function get_mode_of_transfer() 
	{
		$this->load->model('transfers_model');
		$result = $this->transfers_model->get_mode_of_transfer();

		$response = array(
			"status" => true,
			"message" => "",
			"data" => $result
		);
		header("Content-Type: application/json");
		echo json_encode($response);
	}

	public function add_company_transfer() 
	{
		$this->load->library('MY_Form_validation');
        $this->form_validation->CI =& $this;

		$this->form_validation->set_rules('to_number', 'TO Number', 'trim|required');
		$this->form_validation->set_rules('requested_by', 'Requested By', 'trim|required');
		$this->form_validation->set_rules('reason_for_transfer', 'Reason For Transfer', 'trim|required');
		$this->form_validation->set_rules('mode_of_transfer_id', 'Mode of Transfer', 'trim|required');
		// $this->form_validation->set_rules('date_issued', 'Date Issued', 'trim|required');
		// $this->$this->form_validation->set_rules('products', 'Products', 'trim|required');

		if ($this->form_validation->run()) {

			$products = $this->input->post('products');
			
			if($products != "null") {

				$result_count = 0;
				$notes = $this->input->post('notes') != 'null' ? $this->input->post('notes') : null;
				$upload_documents = $this->input->post('upload_documents') != 'null' ? $this->input->post('upload_documents') : null;
				
				$transfer_requestor = $this->input->post('requested_by');

				$this->load->model('transfers_model');


				$requestor_name = $transfer_requestor;
				$requestor = $this->transfers_model->get_requestor(strtolower($requestor_name));
				$requestor_id = 0;
				if(count($requestor) > 0){
					$requestor_id = $requestor[0]["id"];
				}else{
					$requestor_id = $this->transfers_model->add_requestor(array(
						"name" => strtolower($requestor_name)
					));
				}

				$transfer_details = array(
					"to_number" => $this->input->post('to_number'),
					"user_id" => $this->input->post('user_id'),
					"status" => $this->input->post('status'),
					"requested_by" => $requestor_id,
					"reason_for_transfer" => $this->input->post('reason_for_transfer'),
					"mode_of_transfer_id" => $this->input->post('mode_of_transfer_id'),
					// "transfer_method" => $this->input->post('transfer_method'),
					"date_issued" => date("Y-m-d H:i:s"),
					'type' => 1
				);


				$result = $this->transfers_model->add_company_transfer($transfer_details);
				$transfer_log_id_result = $result;
				
				if($result > 0) {
					$result_count = $result_count + 1;

					//LOOP FOR INSERTING NOTES TO DB
					if($notes != null) {
						foreach ($notes as $key => $note) 
						{ 
							$note["transfer_log_id"] = $result;
							$note["user_id"] = 1;

							$note_result = $this->transfers_model->add_transfer_note($note);
							if($note_result > 0) {
								$result_count = $result_count + 1;
							}
						}	
					}
					
					//LOOP FOR INSERTING DOCUMENTS TO DB
					if($upload_documents != null) {
						foreach ($upload_documents as $key => $upload_document) 
						{
							$upload_document["transfer_log_id"] = $result;

							$document_result = $this->transfers_model->add_document($upload_document);
							if($document_result > 0) {
								$result_count++;
							}
						}
					}

					//LOOP FOR INSERTING PRODUCTS TO DB
					foreach ($products as $key => $product) {
						$product_id = $product["id"];
						$origins = $product["origins"];
						$mode_of_transfer = $product["transfer_method"];
						$unit_of_measure = $product["unit_of_measure"];

						$transfer_product_details = array(
							"product_id" => $product_id,
							"transfer_log_id" => $result,
							"transfer_method" => $mode_of_transfer,
							"unit_of_measure_id" => $unit_of_measure
						);

						$transfer_product_result = $this->transfers_model->add_transfer_product_map($transfer_product_details);
						if($transfer_product_result > 0) {

							//LOOP FOR INSERTING PRODUCT ORIGINS TO DB.
							foreach ($origins as $key => $origin) {

								$batch_details = array(
									"name" => $origin["batch_name"],
									"storage_id" => $origin["storage_id"],
									"location_id" => $origin["location_id"],
									"quantity" => $origin["quantity"],
									"storage_assignment_id" => $origin["storage_assignment_id"],
									"unit_of_measure_id" => $origin["unit_of_measure_id"]
								);

								$delete_result = $this->transfers_model->batch_delete($origin["old_batch_id"]);
								$batch_results = $this->transfers_model->batch_transfer($batch_details);

								$origin_details = array(
									"product_map_id" => $transfer_product_result,
									"storage_location" => $origin["storage_id"],
									// "quantity" => $origin["quantity"],
									"quantity" => $origin["quantity"],
									"unit_of_measure" => $origin["unit_of_measure_id"],
									"status" => 0,
									"destination_container" => $origin["location_id"],
									"batch_id" => $batch_results,
									"origin_container" => $origin["old_location_id"]
								);

								$origin_result = $this->transfers_model->add_transfer_origin($origin_details);
							}
							$result_count++;
						}
					}
				}

				if($result_count == 0) {
					$response = array(
						"status" => false,
						"message" => "Transfer record not added.",
						"data" => ""
					);
				} else {
					$response = array(
						"status" => true,
						"message" => "Transfer record added.",
						"data" => $transfer_log_id_result
					);
				}

			} else {
				$response = array(
					"status" => false,
					"message" => "Please complete all required fields.",
					"data" => ""
				);	
			}

		} else {
			$response = array(
				"status" => false,
				"message" => "Please complete all required fields.",
				"data" => ""
			);
		}


		
		header("Content-Type: application/json");
		echo json_encode($response);
	}

	public function get_date_time() 
	{
		$this->load->helper('date');
		$datestring = "%d-%M-%Y %h:%i %a";
		$datestring2 = "%Y-%m-%d %h:%i";
		$time = time();
		$current_date_time = array(
			"to_display" => mdate($datestring, $time),
			"to_record" => mdate($datestring2, $time)
		);
		
		// echo $current_date_time;

		$response = array(
			"status" => true,
			"mesage" => "",
			"data" => $current_date_time
		);

		header("Content-Type: application/json");
		echo json_encode($response);
	}

	public function get_all_company_transfer_notes() 
	{
		// $this->load->helper('date');
		$transfer_log_id = $this->input->get('company_transfer_id');
		$this->load->model('transfers_model');
		$result = $this->transfers_model->get_all_company_transfer_notes($transfer_log_id);
		
		// DATE FORMAT
		// 25-Oct-2015 01:30 PM
		foreach ($result as $key => $note) {
			$date_created = strtotime($note["date_created"]);
			$date = date('M d, Y', $date_created);
			$time = date('g:i A', $date_created);
			$result[$key]["date_created"] = $date . " | " . $time;
		}

		if($result > 0) {
			$response = array(
				"status" => true,
				"message" => '',
				"data" => $result
			);
		} else {
			$response = array(
				"status" => false,
				"message" => '',
				"data" => $result
			);
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function complete_transfer() {
		$this->load->library('MY_Form_validation');
        $this->form_validation->CI =& $this;

        $this->form_validation->set_rules('transfer_start', 'Transfer Start', 'trim|required');
        $this->form_validation->set_rules('transfer_end', 'Transfer End', 'trim|required');
        if ($this->form_validation->run()) {
        	
        	$workers = $this->input->post('workers') != 'null' ?  $this->input->post('workers') : null;
			$equipments = $this->input->post('equipments') != 'null' ? $this->input->post('equipments') : null;
			$transfer_log_id = $this->input->post('transfer_log_id') != 'null' ? $this->input->post('transfer_log_id') : null;
			$transfer_start = $this->input->post('transfer_start') != 'null' ? $this->input->post('transfer_start') : null;
			$transfer_end = $this->input->post('transfer_end') != 'null' ? $this->input->post('transfer_end') : null;

			if($transfer_start != null && $transfer_end != null) {

				$complete_details = array(
					'date_start' => date('Y/m/d/ G:i:s', strtotime($transfer_start)),
					'date_complete' => date('Y/m/d G:i:s', strtotime($transfer_end)),
					'status' => 1
				);

				$this->load->model('transfers_model');
				$result = $this->transfers_model->complete_company_transfer($transfer_log_id, $complete_details);
				// echo $result;
				if($result > 0) {
					if($workers != null) {
						// foreach ($workers as $key => $worker) {
							// $worker_details = array(
							// 	'name' => $worker['name'],
							// 	'transfer_log_id' => $transfer_log_id,
							// 	'worker_type' => $worker['worker_type']
							// );

							$this->company_add_workers($workers, $transfer_log_id);
						// }

						// $this->company_add_workers($workers);
					}

					if($equipments != null) {
						// foreach ($equipments as $key => $equipment) {
						// 	$equipment["transfer_log_id"] = $transfer_log_id;
						// 	$this->transfers_model->add_equipment($equipment);
						// }

						$this->company_add_equipments($equipments, $transfer_log_id);
					}

					$response = array(
						'status' => true,
						'message' => 'Transfer completed.',
						'data' => $complete_details
					);
				} else {
					$response = array(
						'status' => false,
						'message' => 'Transfer not completed.',
						'data' => ""
					);
				}

			} else {

				$response = array(
					'status' => false,
					'message' => 'Please complete the required fields.',
					'data' => ""
				);

			}


        } else {
        	
        	$response = array(
				'status' => false,
				'message' => 'Please complete the required fields.',
				'data' => ""
			);

        }
		
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function company_add_workers($workers, $record_log_id) 
	{
		$this->load->model('transfers_model');

		foreach ($workers as $key => $worker) {
			$worker_details = array(
				'name' => $worker['name'],
				'record_log_id' => $record_log_id,
				'record_type' => 1,
				'worker_type' => $worker['worker_type']
			);

			$this->transfers_model->company_add_worker($worker_details);
		}
	}

	public function company_add_equipments($equipments, $record_log_id)
	{
		$this->load->model('transfers_model');

		foreach ($equipments as $key => $equipment) {
			$equipment["record_log_id"] = $record_log_id;
			$equipment["record_type"] = 1;

			$this->transfers_model->company_add_equipment($equipment);
		}
	}

	public function get_all_worker_type() {
		$this->load->model('transfers_model');
		$result = $this->transfers_model->get_all_worker_type();
		
		$response = array(
			"status" => true,
			"message" => "",
			"data" => $result
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_transfer_batches() {
		$transfer_log_id = $this->input->get('transfer_log_id');
		$this->load->model('transfers_model');
		$result = $this->transfers_model->get_transfer_batches($transfer_log_id);
		if($result) {

			foreach ($result as $key => $map) {
				$product_map_id = $map["id"];
				$result[$key]["batches"] = $this->transfers_model->get_product_batch($product_map_id);
			}

			$response = array(
				"status" => true,
				"message" => "",
				"data" => $result
			);
			header("Content-Type: application/json");
			echo json_encode($response);
		}
	}

	public function get_transfer_equipments() {
		$transfer_log_id = $this->input->get('transfer_log_id');
		$this->load->model('transfers_model');
		$result = $this->transfers_model->get_transfer_equipments($transfer_log_id);
		if($result) {
			$response = array(
				"status" => true,
				"message" => "",
				"data" => $result
			);
			header("Content-Type: application/json");
			echo json_encode($response);
		}
	}

	public function get_transfer_workers() {
		$transfer_log_id = $this->input->get('transfer_log_id');
		$this->load->model('transfers_model');
		$result = $this->transfers_model->get_transfer_workers($transfer_log_id);
		if($result) {
			$response = array(
				"status" => true,
				"message" => "",
				"data" => $result
			);
			header("Content-Type: application/json");
			echo json_encode($response);
		}
	}

	public function get_transfer_documents() {
		$transfer_log_id = $this->input->get('transfer_log_id');
		$this->load->model('transfers_model');
		$this->load->helper('date');
		$result = $this->transfers_model->get_transfer_documents($transfer_log_id);
		
		foreach ($result as $key => $document) {
			$date_issued = strtotime($document["date_added"]);
			$date = date('d-M-Y', $date_issued);
			$result[$key]["date_added"] = $date;
		}

		$response = array(
			"status" => true,
			"message" => "",
			"data" => $result
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_product_batches() {
		$this->load->model('transfers_model');
		$product_id = $this->input->get('product_id');
		$result = $this->transfers_model->get_product_batches($product_id);
		$response = array(
			"status" => true,
			"message" => "",
			"data" => $result
		);

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_all_batches() {
		$this->load->model('transfers_model');
		$result = $this->transfers_model->get_all_batches();

		$response = array(
			"status" => true,
			"message" => "",
			"data" => $result
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_all_locations() {
		$this->load->model('transfers_model');
		$result = $this->transfers_model->get_all_locations();
		
		$response = array(
			'status' => true,
			"message" => "",
			"data" => $result
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function reverse_array() {
		$the_array = $this->input->get('the_array');
		$reversed = array_reverse($the_array);
		
		$response = array(
			"type" => true,
			"message" => "",
			"data" => $reversed
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_company_product_transfers() {
		$transfer_log_id = $this->input->get('transfer_log_id');

		$this->load->model('transfers_model');
		$results = $this->transfers_model->get_company_product_transfers($transfer_log_id);

		foreach ($results as $key => $result) {
			$origin_result = $this->transfers_model->get_transfer_origins($result["id"]);
			$results[$key]["origins"] = $origin_result;
		}
		
		$response = array(
			"status" => true,
			"message" => "",
			"data" => $results
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function company_get_transfer_details() {
		$transfer_log_id = $this->input->get('transfer_log_id');
		$this->load->model('transfers_model');
		$details_result = $this->transfers_model->company_get_transfer_details($transfer_log_id);
		
		$response = array(
			'status' => true,
			'message' => '',
			'data' => $details_result
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function company_get_ongoing_transfers() {
		$transfer_log_id = $this->input->get('transfer_log_id');
		$this->load->model('transfers_model');
		$transfer_result = $this->transfers_model->company_get_ongoing_transfers($transfer_log_id);
		
		foreach ($transfer_result as $key => $transfer) {
			$product_map_id = $transfer['id'];
			$transfer_result[ $key ]['batches'] = $this->company_get_ongoing_batch($product_map_id);
		}
		$response = array(
			"status" => true,
			"message" => "",
			"data" => $transfer_result
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function company_get_ongoing_batch($product_map_id) {
		$this->load->model('transfers_model');
		$batch_result = $this->transfers_model->company_get_ongoing_batch($product_map_id);
		return $batch_result;
	}

	public function company_transfer_edit() {
		$this->load->model('transfers_model');
		$edit_details = $this->input->post('edit_details');
		$removed_products = $edit_details['removed_products'];
		$new_products = $edit_details['new_products'];
		$transfer_log_id = $edit_details['transfer_log_id'];
		$result_count = 0;

		$edit_requestor = $edit_details['requested_by'];

		$requestor_name = $edit_requestor;
				$requestor = $this->transfers_model->get_requestor(strtolower($requestor_name));
				$requestor_id = 0;
				if(count($requestor) > 0){
					$requestor_id = $requestor[0]["id"];
				}else{
					$requestor_id = $this->transfers_model->add_requestor(array(
						"name" => strtolower($requestor_name)
					));
				}

		$edit_transfer_details = array(
			'reason_for_transfer' => $edit_details['reason_for_transfer'],
			'mode_of_transfer_id' => $edit_details['mode_of_transfer'],
			'requested_by' => $requestor_name
		);

		$transfer_edit_result = $this->transfers_model->company_edit_transfer_details($transfer_log_id, $edit_transfer_details);
		if($transfer_edit_result > 0) {
			$result_count++;
		}

		//LOOP FOR THE DELETED PRODUCTS.
		if($removed_products != 'undefined') {
			// print_r($removed_products);

			foreach ($removed_products as $key => $removed) {
				$transfer_map_id = $removed['transfer_product_map_id'];
				$delete_batch_result = $this->transfers_model->company_delete_transfer_batch($transfer_map_id);
				
				// echo 'result - ' . $delete_batch_result;

				if($delete_batch_result > 0) {
					$result_count++;
				}
			}	
		}
		

		//LOOP FOR THE NEWLY ADDED PRODUCTS
		if($new_products != 'undefined') {
			foreach ($new_products as $key => $new) {
				$product_batches = $new['batches'];
				$product_map_details = array(
					'product_id' => $new['product_id'],
					'transfer_log_id' => $transfer_log_id,
					'transfer_method' => $new['transfer_method']
				);
				
				$new_product_result = $this->transfers_model->add_transfer_product_map($product_map_details);

				if($new_product_result > 0) {
					$result_count++;

					foreach ($product_batches as $key => $batch) {
						$batch_details = array(
							'product_map_id' => $new_product_result,
							'batch_id' => $batch['batch_id'],
							'storage_location' => $batch['new_storage'],
							'quantity' => $batch['quantity'],
							'unit_of_measure' => $batch['unit_of_measure'],
							'destination_container' => $batch['new_location'],
							'origin_container' => $batch['old_location']
						);
						
						$new_batch_result = $this->transfers_model->add_transfer_origin($batch_details);
						if($new_batch_result > 0) {
							$result_count++;
						}
					}
				}
			}
		}


		if($result_count > 0) {
			
			$response = array(
				'status' => true,
				'message' => 'Transfer updated.',
				'data' => ''
			);

		} else {

			$response = array(
				'status' => false,
				'message' => 'Transfer not updated.',
				'data' => ''
			);

		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function company_get_transfer_batches_list() {
		$this->load->model('transfers_model');
		$transfer_log_id = $this->input->get('transfer_log_id');

		$product_list = $this->transfers_model->company_get_product_map($transfer_log_id);
		if($product_list) {
			foreach ($product_list as $key => $product) {
				$transfer_batch_details = $this->transfers_model->company_get_transfer_batch($product['map_id']);
				$product_list[ $key ]['transfer_batch_details'] = $transfer_batch_details[0];
			}
		}

		$response = array(
			'status' => true,
			'message' => '',
			'data' => $product_list
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_transfer_record() {
		$transfer_log_id = $this->input->get('transfer_log_id');
		$this->load->model('transfers_model');
		$transfer_result = $this->transfers_model->get_transfer_record($transfer_log_id);

		$date_issued = strtotime($transfer_result["date_issued"]);
		$date = date('M d, Y', $date_issued);
		$transfer_date = date('d-M-Y', $date_issued);
		$time = date('g:i A', $date_issued);

		//FORMATS THE DATE TO *Apr 30, 2016 | 7:30 AM*
		$transfer_result["date_issued"] = $date . " | " . $time;
		//FORMATS THE DATE TO *30-Apr-2016*
		$transfer_result["transfer_date"] = $transfer_date;

		//CALCULATES THE DAY AND HOUR INTERVAL OF DATE_START & DATE_COMPLETE
		$datetime1 = date_create($transfer_result["date_start"]);
		$datetime2 = date_create($transfer_result["date_complete"]);
		$interval = date_diff($datetime1, $datetime2);
		$transfer_result["date_interval"] = $interval->format('%a days, %h hours');

		//FORMATS THE DATE_START TO *19-Apr-2016 12:00 AM*
		$date_start = strtotime($transfer_result["date_start"]);
		$s_date = date('d-M-Y', $date_start);
		$s_time = date('g:i A', $date_start);
		$transfer_result["date_start"] = $s_date . ' ' . $s_time;

		//FORMATS THE DATE_COMPLETE TO *19-Apr-2016 12:00 AM*
		$date_complete = strtotime($transfer_result["date_complete"]);
		$c_date = date('d-M-Y', $date_complete);
		$c_time = date('g:i A', $date_complete);
		$transfer_result["date_complete"] = $c_date . ' ' . $c_time;

		$transfer_result['product_list'] = $this->transfers_model->get_transfer_products($transfer_log_id);

		foreach ($transfer_result['product_list'] as $key => $product) {
			$transfer_result['product_list'][ $key ]['batches'] = $this->transfers_model->get_transfer_batch_list($product['map_id']);
		}

		$transfer_result['note_list'] = $this->get_transfer_notes($transfer_log_id);
		$transfer_result['document_list'] = $this->get_company_documents($transfer_log_id);

		$response = array(
			'status' => true,
			'message' => '',
			'data' => $transfer_result
		);

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_transfer_notes($transfer_log_id) {
		$this->load->model('transfers_model');
		$note_result = $this->transfers_model->get_transfer_notes($transfer_log_id);
		return $note_result;
	}

	public function get_company_documents($transfer_log_id) {
		$this->load->model('transfers_model');
		$document_result = $this->transfers_model->get_transfer_documents($transfer_log_id);
		return $document_result;
	}

	public function get_all_workers() {
		$transfer_log_id = $this->input->get('transfer_log_id');
		$this->load->model('transfers_model');
		$worker_result = $this->transfers_model->get_all_workers($transfer_log_id);
		
		$response = array(
			'status' => true,
			'message' => '',
			'data' => $worker_result
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_all_equipments() {
		$transfer_log_id = $this->input->get('transfer_log_id');
		$this->load->model('transfers_model');
		$equipment_result = $this->transfers_model->get_all_equipments($transfer_log_id);
		
		$response = array(
			'status' => true,
			'message' => '',
			'data' => $equipment_result
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function company_add_new_batch() {
		$batch_details = $this->input->post('batch_details');
		$this->load->model('transfers_model');
		$result = $this->transfers_model->company_add_new_batch($batch_details);
		
		if($result > 0) {
			$response = array(
				'status' => true,
				'message' => 'Transfer batch added.',
				'data' => ''
			);
		} else {
			$response = array(
				'status' => false,
				'message' => 'Transfer batch not added.',
				'data' => ''
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	// public function company_get_transfer_details()

	//==================================================//
	//==============END OF COMPANY GOODS================//
	//==================================================//

	public function get_all_unit_of_measures() {
		$this->load->model('transfers_model');
		$result = $this->transfers_model->get_all_unit_of_measures();
		$response = array(
			"status" => true,
			"message" => "",
			"data" => $result
		);
		header("Content-Type: application/json");
		echo json_encode($response);
	}

	public function upload_document(){

		$transfer_log_id = $this->input->post("transfer_log_id");
		$name = $this->input->post("name");
		$document_name = "";

		// echo 'here';
		$temporary = $this->input->post("temporary");

		if(strlen($name) > 0){
			$document_name = $name;
		}

		$response = array();
		$arr_response = array();
		$status = false;
		$message = "Fail";
		if(isset($_FILES["attachment"])){
			$name = $_FILES["attachment"]["name"];
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 20; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    
		    $file_name = $randomString . "." .$ext;

		    move_uploaded_file($_FILES["attachment"]["tmp_name"], UPLOAD_DOCUMENT_PATH.$file_name);

		    $arr_response["path"] = UPLOAD_DOCUMENT_PATH.$file_name;
		    $arr_response["location"] = BASEURL . "uploads/documents/" . $file_name;
		    $status = true;
		    $message = "Succes";

		    if(strlen($name) == 0){
				$document_name = $file_name;
			}

			$data_docs = array(
		    	"document_name" => $document_name,
		    	"document_path" => $arr_response["location"],
		    	"date_added" => date("Y-m-d H:i:s"),
		    	"transfer_log_id" => $transfer_log_id,
		    	"status" => 0,
		    	"version" => 1
		    );

			// $this->load->model('transfers_model');
		    // $docs_id = $this->transfers_model->add_document($data_docs);
		    $arr_response[0]["documents"] = array();
		    // $data_docs["upload_id"] = $docs_id;
		    $arr_response[0]["documents"][] = $data_docs;
		    unset($arr_response["location"]);
	    	unset($arr_response["path"]);

		    // if($temporary != 'true'){
		    // 	$data_docs = array(
			   //  	"document_name" => $document_name,
			   //  	"document_path" => $arr_response["location"],
			   //  	"date_added" => date("Y-m-d H:i:s"),
			   //  	"transfer_log_id" => $transfer_log_id,
			   //  	"status" => 0,
			   //  	"version" => 1
			   //  );
		    // 	$docs_id = $this->transfers_model->add_document($data_docs);
		    // 	header("Content-Type: application/json");
			   //  $arr_response[0]["documents"] = array();
			   //  $arr_response[0]["documents"][] = $data_docs;
			   //  // $arr_response[0][""]
			   //  unset($arr_response["location"]);
		    // 	unset($arr_response["path"]);
		    // 	// $arr_response = $this->get_single_company_goods($receiving_id);
		    // }else{
		    // 	$data_docs = array(
			   //  	"document_name" => $document_name,
			   //  	"date_added_formatted" =>  date("j-M-Y g:i A", strtotime( date("Y-m-d H:i:s") )),
			   //  	"document_path" => $arr_response["location"],
			   //  	"date_added" => date("Y-m-d H:i:s"),
			   //  	"status" => 0,
			   //  	"version" => 1,
			   //  	"document_id" => $docs_id
			   //  );

		    // 	header("Content-Type: application/json");
			   //  $arr_response[0]["documents"] = array();
			   //  $arr_response[0]["documents"][] = $data_docs;
			   //  unset($arr_response["location"]);
		    // 	unset($arr_response["path"]);
		    // }
		}

		header("Content-Type: application/json");
		$response["status"] = $status;
		$response["message"] = $message;
		$response["data"] = $arr_response;
		// $response["document_id"] = $docs_id;
		echo json_encode($response);
	}



	public function get_location($id, $location)
	{
		foreach ($location as $key => $value) {
			if($value["id"] == $id){
				return $location[$key];
			}
		}

		return array();
	}

	public function get_parent_location($child, $location, $refs = array())
	{
		$data = &$refs;

		$parentdata = $this->get_location($child["parent_id"], $location);

		$data[] = $child;

		if( count($parentdata) > 0 ){
			return $this->get_parent_location($parentdata, $location, $data);
		}

		if( count($parentdata) === 0 ){
			return $data;
		}


	}

	public function get_hierarchy_of_batch_location($storage_id, $location_id)
	{
		$this->load->model("Storages/storage_model");
		$location = $this->storage_model->get_location($storage_id);
		$current = $this->get_location($location_id, $location);

		$data = $this->get_parent_location($current, $location);

		$reversed = array_reverse($data);
		return $reversed;
		
	}

	// =========================== START OF TRANSFER FOR CONSIGNEE ==========================================//

	public function add_new_transfer_consignee_goods()
	{
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules('data', 'Data', 'trim|required');

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{
			$user_data = $this->session->userdata('apollo_user');

			
				$requestor_name = $data["requested_by"];
				$requestor = $this->transfers_model->get_requestor(strtolower($requestor_name));
				$requestor_id = 0;
				if(count($requestor) > 0){
					$requestor_id = $requestor[0]["id"];
				}else{
					$requestor_id = $this->transfers_model->add_requestor(array(
						"name" => strtolower($requestor_name)
					));
				}

				$date_received = date("Y-m-d H:i:s");

				$transfers_data = array(
					"requested_by"=>$requestor_id,
					"reason_for_transfer"=>$data["reason"],
					"to_number"=>$data["to_number"],
					"date_issued"=>$date_received,
					"type"=>2,
					"status"=>0,
					"user_id"=>$user_data["id"],
					"vessel_origin_id"=>$data["vessel_origin"],
					"mode_of_transfer_id"=>$data["mode_of_transfer"]
				);

				$transfer_id = $this->transfers_model->add_transfer_consignee_goods($transfers_data);

				if(count($data["document_info"]) > 0)
				{

					$this->add_consignee_documents($transfer_id, $data["document_info"]);
				}

				if(count($data["notes_info"]) > 0)
				{

					$this->save_transfer_notes($transfer_id, $data["notes_info"]);
				}

				if(count($data["product_info"]) > 0)
				{

					$this->save_new_transfer_consignee_origin($transfer_id, $data["product_info"]);
				}




				
			header("Content-Type: application/json");

			

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $transfer_id;
			echo json_encode($response);
		}
	}

	public function save_new_transfer_consignee_origin($transfer_id, $product_info)
	{
		foreach ($product_info as $key => $value) {

			foreach ($value["batch_info"] as $batchkey => $batchvalue) {

				$storageId = $this->transfers_model->get_storage_id($batchvalue["new_location_id"]);
				$unit_measure_id = $this->transfers_model->get_unit_measure_id($batchvalue["new_measurement"]);

				$this->transfers_model->remove_selected_batch($batchvalue["batch_id"]);

				$new_data = array(
					"transfer_log_id"=>$transfer_id,
					"storage_location"=>$storageId[0]["storage_id"],
					"product_id"=>$batchvalue["product_id"],
					"quantity"=>$batchvalue["new_qty"],
					"unit_of_measure"=>$unit_measure_id[0]["id"],
					"status"=>0,
					"batch_id"=>$batchvalue["batch_id"],
					"destination_container"=>$batchvalue["new_location_id"],
					"origin_container"=>$batchvalue["old_location_id"],
					"transfer_method"=>$batchvalue["transfer_method"]
					);

				$this->replace_old_batch($new_data);

				

				
			}
		}
	}

	public function replace_old_batch($new_data)
	{
		$old_batch = $this->transfers_model->get_selected_batch($new_data["batch_id"]);

		$collet_new_data = array(
			"storage_assignment_id"=>$old_batch[0]["storage_assignment_id"],
			"name"=>$old_batch[0]["name"],
			"quantity"=>$new_data["quantity"],
			"unit_of_measure_id"=>$new_data["unit_of_measure"],
			"location_id"=>$new_data["destination_container"],
			"storage_id"=>$new_data["storage_location"]
			);

		$new_batch = $this->transfers_model->replace_old_batch($collet_new_data);

		$new_data["batch_id"] = $new_batch;


		$for_product_map = array(
			"transfer_log_id"=>$new_data["transfer_log_id"],
			"product_id"=>$new_data["product_id"],
			"transfer_method"=>$new_data["transfer_method"],
		);

		$prod_map_id = $this->transfers_model->save_new_transfer_product_map($for_product_map);


		$new_trans_origin = array(
				"product_map_id"=>$prod_map_id,
				"batch_id"=>$new_data["batch_id"],
				"storage_location"=>$new_data["storage_location"],
				"quantity"=>$new_data["quantity"],
				"unit_of_measure"=>$new_data["unit_of_measure"],
				"status"=>0,
				"destination_container"=>$new_data["destination_container"],
				"origin_container"=>$new_data["origin_container"]
			);


		$this->transfers_model->save_new_transfer_consignee_origin($new_trans_origin);

	}



	public function add_consignee_documents($transfer_id, $document_info)
	{
		foreach ($document_info as $key => $value) {
			
			$document_data = array(
				"document_name" => $document_info[$key]["document_name"],
		    	"document_path" =>$document_info[$key]["document_path"],
		    	"date_added" => date("Y-m-d H:i:s"),
		    	"transfer_log_id" => $transfer_id,
		    	"status" => 0,
		    	"version" => 1
				);
		    $docs_id = $this->transfers_model->add_document($document_data);
		}
	}

	public function save_transfer_notes($transfer_id, $notes)
	{
		$user_data = $this->session->userdata('apollo_user');
		foreach ($notes as $key => $value) {
			$data = array(
				"transfer_log_id" => $transfer_id,
				"note" => $value["message"],
				"subject" => $value["subject"],
				"user_id" => $user_data["id"],
				"date_created" => $value["datetime"],
				"is_deleted" => 0
			);
			$this->transfers_model->add_notes($data);
		}
	}

	public function get_consignee_products()
	{
		$this->load->model('transfers_model');

		$result = $this->transfers_model->get_all_consignee_products();

		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $result;
		echo json_encode($response);
	}

	public function get_consignee_products_for_edit()
	{
		$this->load->model('transfers_model');

		$result = $this->transfers_model->get_all_consignee_products_for_edit();

		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $result;
		echo json_encode($response);
	}

	public function get_all_batch_consignee()
	{
		// $this->load->model('transfers_model');

		$result = $this->transfers_model->get_all_batch_consignee();

		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $result;
		echo json_encode($response);
	}

	public function upload_document_consignee(){

		$transfer_log_id = $this->input->post("transfer_log_id");
		$name = $this->input->post("name");
		$document_name = "";

		$temporary = $this->input->post("temporary");

		if(strlen($name) > 0){
			$document_name = $name;
		}

		$response = array();
		$arr_response = array();
		$status = false;
		$message = "Fail";
		if(isset($_FILES["attachment"])){
			$name = $_FILES["attachment"]["name"];
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 20; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    
		    $file_name = $randomString . "." .$ext;

		    move_uploaded_file($_FILES["attachment"]["tmp_name"], UPLOAD_DOCUMENT_PATH.$file_name);

		    $arr_response["path"] = UPLOAD_DOCUMENT_PATH.$file_name;
		    $arr_response["location"] = BASEURL . "uploads/documents/" . $file_name;
		    $status = true;
		    $message = "Succes";

		    if(strlen($name) == 0){
				$document_name = $file_name;
			}

		    if($temporary != 'true'){
		    	$data_docs = array(
			    	"document_name" => $document_name,
			    	"document_path" => $arr_response["location"],
			    	"date_added" => date("Y-m-d H:i:s"),
			    	"transfer_log_id" => $transfer_log_id,
			    	"status" => 0,
			    	"version" => 1
			    );
		    	$docs_id = $this->transfers_model->add_document($data_docs);
		    	$get_select_transfer_documents = $this->transfers_model->get_documents($transfer_log_id);

		    	foreach ($get_select_transfer_documents as $key => $value) {
					$get_time = strtotime($get_select_transfer_documents[$key]["date_added"]);
					$new_date = date('F d, Y h:i A', $get_time);
					$get_select_transfer_documents[$key]["date_added_formatted"] = $new_date;
				}


		    	header("Content-Type: application/json");
			    $arr_response[0]["documents"] = array();
			    $arr_response[0]["documents"] = $get_select_transfer_documents;
			    unset($arr_response["location"]);
		    	unset($arr_response["path"]);
		    }else{
		    	$data_docs = array(
			    	"document_name" => $document_name,
			    	"date_added_formatted" =>  date("j-M-Y g:i A", strtotime( date("Y-m-d H:i:s") )),
			    	"document_path" => $arr_response["location"],
			    	"date_added" => date("Y-m-d H:i:s"),
			    	"status" => 0,
			    	"version" => 1
			    );

		    	header("Content-Type: application/json");
			    $arr_response[0]["documents"] = array();
			    $arr_response[0]["documents"][] = $data_docs;
			    unset($arr_response["location"]);
		    	unset($arr_response["path"]);
		    }
		}

		header("Content-Type: application/json");
		$response["status"] = $status;
		$response["message"] = $message;
		$response["data"] = $arr_response;
		echo json_encode($response);
	}

	public function get_transfer_data()
	{
		$transfer_id = $this->input->post('transfer_id');

		$this->form_validation->set_rules("transfer_id", "Transfer ID", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
		else
		{
			
			$product_stored = array();
			$docs_stored = array();
			$notes_stored = array();

			$result = $this->transfers_model->get_transfer_data($transfer_id);
			$docs = $this->transfers_model->get_documents($transfer_id);
			$get_products = $this->transfers_model->get_transfers_prod($transfer_id);

			$notes = $this->transfers_model->get_notes(" WHERE transfer_log_id = ".$transfer_id." ");
			foreach($notes as $i => $val){
				$notes[$i]["date_formatted"] = date("j-M-Y g:i A", strtotime($val["date_created"]));
			}

			// array_push($notes_stored, $notes);


			foreach ($docs as $key => $value) {

				$data_docs = array(
			    	"document_name" => $value["document_name"],
			    	"date_added_formatted" =>  date("j-M-Y g:i A", strtotime( $value["date_added"] )),
			    	"document_path" => $value["document_path"],
			    	"date_added" => $value["date_added"],
			    	"status" => 0,
			    	"version" => 1
			    );
			    array_push($docs_stored, $data_docs);
			}

			


			foreach ($get_products as $key => $value) {
				$batch_info = $this->transfers_model->get_transfer_origin($transfer_id, $value["product_id"]);

				$batch_stored = array();

				foreach ($batch_info as $prodkey => $prodvalue) {
					$batch_data = array(
						"batch_id"=>$prodvalue["batch_id"],
						"batch_name"=>$prodvalue["batch_name"],
						"quantity"=>$prodvalue["quantity"],
						"unit_measure_name"=>$prodvalue["unit_of_measure_name"],
						"unit_of_measure"=>$prodvalue["unit_of_measure"],
						"new_location_id"=>$prodvalue["destination_container"],
						"old_location_id"=>$prodvalue["origin_container"],
					);
					array_push($batch_stored, $batch_data);
				}

				$prod_data = array(
					"transfer_id"=>$transfer_id,
					"id"=>$value["product_id"],
					"sku"=>$value["sku"],
					"name"=>$value["product_name"],
					"image"=>$value["image"],
					"description"=>$value["description"],
					"unit_measure_name"=>$value["unit_measure_name"],
					"unit_of_measure"=>$value["unit_of_measure"],
					"transfer_method"=>$value["transfer_method"],
					"batch_info"=>$batch_stored
				);

				array_push($product_stored, $prod_data);


			}

			$buildNewDate = strtotime($result[0]["date_issued"]);
			$new_date = date('F d, Y', $buildNewDate);
			$new_time = date('h:i A', $buildNewDate);
			$new_datetime = $new_date." | ".$new_time;

			$transfer_data = array(
				"transfer_id"=>$result[0]["id"],
				"to_number"=>$result[0]["to_number"],
				"reason"=>$result[0]["reason"],
				"date_issued"=>$new_datetime,
				"mode_of_transfer"=>$result[0]["mode_of_transfer"],
				"mode_of_transfer_id"=>$result[0]["mode_of_transfer_id"],
				"vessel_origin_id"=>$result[0]["vessel_origin_id"],
				"requestor"=>ucwords($result[0]["requestor"]),
				"product_info"=>$product_stored,
				"document_info"=>$docs_stored,
				"notes_info"=>$notes
				);


			



			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $transfer_data;
			echo json_encode($response);
		}
	}

	public function get_transfer_data_edit()
	{
		$transfer_id = $this->input->post('transfer_id');

		$this->form_validation->set_rules("transfer_id", "Transfer ID", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
		else
		{
			
			$product_stored = array();


			$result = $this->transfers_model->get_transfer_data($transfer_id);

			$get_products = $this->transfers_model->get_transfers_prod($transfer_id);



			foreach ($get_products as $key => $value) {
				$batch_info = $this->transfers_model->get_transfer_origin($transfer_id, $value["product_id"]);

				$batch_stored = array();

				

				$prod_data = array(
					"transfer_id"=>$transfer_id,
					"id"=>$value["product_id"],
					"sku"=>$value["sku"],
					"name"=>$value["product_name"],
					"image"=>$value["image"],
				);

				array_push($product_stored, $prod_data);


			}

			$buildNewDate = strtotime($result[0]["date_issued"]);
			$new_date = date('F d, Y', $buildNewDate);
			$new_time = date('h:i A', $buildNewDate);
			$new_datetime = $new_date." | ".$new_time;

			$transfer_data = array(
				"transfer_id"=>$result[0]["id"],
				"to_number"=>$result[0]["to_number"],
				"reason"=>$result[0]["reason"],
				"date_issued"=>$new_datetime,
				"mode_of_transfer"=>$result[0]["mode_of_transfer"],
				"mode_of_transfer_id"=>$result[0]["mode_of_transfer_id"],
				"vessel_origin_id"=>$result[0]["vessel_origin_id"],
				"requestor"=>$result[0]["requestor"],
				"product_info"=>$product_stored,

				);


			



			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $transfer_data;
			echo json_encode($response);
		}
	}

	public function add_selected_transfer_notes()
	{
		$note_information = json_decode($this->input->post('note_information'), true);
		$transfer_id = $this->input->post('transfer_id');
		$arr_new_notes = array();

		$this->form_validation->set_rules("transfer_id", "Transfer ID", "trim|required");
		$this->form_validation->set_rules("note_information", "Notes Information", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{

			
			$this->save_transfer_consignee_notes($note_information, $transfer_id);

			$get_select_transfer_notes = $this->transfers_model->get_notes(" WHERE transfer_log_id = ".$transfer_id." ");
			
			foreach ($get_select_transfer_notes as $key => $value) {


				$buildNewDate = strtotime($get_select_transfer_notes[$key]["date_created"]);
				$new_date = date('d-M-Y h:i A', $buildNewDate);

				$arr_build_data = array(
						"subject"=>$get_select_transfer_notes[$key]["subject"],
						"date_created"=>$new_date,
						"note"=>$get_select_transfer_notes[$key]["note"],
						"date_formatted"=>date("j-M-Y g:i A", strtotime($get_select_transfer_notes[$key]["date_created"]))
					);

				array_push($arr_new_notes, $arr_build_data);
			}

			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $arr_new_notes;
			echo json_encode($response);

				



		}
	}

	public function save_transfer_consignee_notes($notes, $transfer_id)
	{
		$user_data = $this->session->userdata('apollo_user');
		foreach ($notes as $key => $value) {
			$data = array(
				"transfer_log_id" => $transfer_id,
				"note" => $value["message"],
				"subject" => $value["subject"],
				"user_id" => $user_data["id"],
				"date_created" => $value["datetime"],
				"is_deleted" => 0
			);
			$this->transfers_model->add_notes($data);
		}
	}

	public function save_selected_product_consignee()
	{
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules("data", "Data", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
		else
		{

			// echo "<pre>";
			// print_r($data["batch_info"]);
			// echo "</pre>";
			// exit();
		
				
				$arr_set_is_deleted = array(
					"is_deleted"=>1
				);



				$result = $this->transfers_model->remove_old_consign_transfer_product_map($data["transfer_id"], $data["id"], $arr_set_is_deleted);



				foreach ($data["batch_info"] as $batchkey => $batchvalue) {
					$arr_bulid_new_prod_map = array(
							"product_id"=>$data["id"],
							"transfer_log_id"=>$data["transfer_id"],
							"transfer_method"=>$data["transfer_method"],
							"is_deleted"=>0
						);

					$prod_map_id = $this->transfers_model->save_new_transfer_product_map($arr_bulid_new_prod_map);

					$storageId = $this->transfers_model->get_storage_id($batchvalue["new_location_id"]);

					$this->transfers_model->remove_selected_batch($batchvalue["batch_id"]);
					$old_batch = $this->transfers_model->get_selected_batch($batchvalue["batch_id"]);

					$collet_new_data = array(
						"storage_assignment_id"=>$old_batch[0]["storage_assignment_id"],
						"name"=>$old_batch[0]["name"],
						"quantity"=>$data["batch_info"][$batchkey]["new_qty"],
						"unit_of_measure_id"=>$data["batch_info"][$batchkey]["new_measurement_id"],
						"location_id"=>$data["batch_info"][$batchkey]["new_location_id"],
						"storage_id"=>$storageId[0]["storage_id"],
					);

					$new_batch = $this->transfers_model->replace_old_batch($collet_new_data);

					$data["batch_info"][$batchkey]["batch_id"] = $new_batch;



					$arr_build_new_transfer_origin = array(
							"product_map_id"=>$prod_map_id,
							"storage_location"=>$storageId[0]["storage_id"],
							"quantity"=>$data["batch_info"][$batchkey]["new_qty"],
							"unit_of_measure"=>$data["batch_info"][$batchkey]["new_measurement_id"],
							"status"=>0,
							"batch_id"=>$data["batch_info"][$batchkey]["batch_id"],
							"destination_container"=>$data["batch_info"][$batchkey]["new_location_id"],
							"origin_container"=>$data["batch_info"][$batchkey]["old_location_id"],

						);

					$this->transfers_model->save_new_transfer_consignee_origin($arr_build_new_transfer_origin);


				}


			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $result;
			echo json_encode($response);
		}
	}

	public function update_transfer_consignee_product()
	{
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules("data", "Data", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
		else
		{

			$requestor_name = $data["requested_by"];
			$requestor = $this->transfers_model->get_requestor(strtolower($requestor_name));
			$requestor_id = 0;
			if(count($requestor) > 0){
				$requestor_id = $requestor[0]["id"];
			}else{
				$requestor_id = $this->transfers_model->add_requestor(array(
					"name" => strtolower($requestor_name)
				));
			}

			$arr_update_transfer = array(
				"requested_by"=>$requestor_id,
				"reason_for_transfer"=>$data["reason"],
				"vessel_origin_id"=>$data["vessel_origin"],
				"mode_of_transfer_id"=>$data["mode_of_transfer"],
				"type"=>2,
				"status"=>0,
				);

			
				$result = $this->transfers_model->update_transfer_consignee_goods($data["transfer_id"], $arr_update_transfer);

				$prod_maps_to_complete =  $this->transfers_model->get_prod_maps_to_complete($data["transfer_id"]);
				foreach ($variable as $key => $value) {
					$arr_complete = array("status"=>1);
					$this->transfers_model->update_origin_consignee($value["id"], $arr_complete);
				}



			$get_old_products = $this->transfers_model->get_transfer_product_map($data["transfer_id"]);

			if(count($data["old_products"]) > 0)
			{
				foreach ($get_old_products as $oldkey => $oldvalue) {
					$icount = 0;
					foreach ($data["old_products"] as $key => $value) {

						if($oldvalue["product_id"] == $value["product_id"])
						{
							$icount++;
						}
					}
					if($icount == 0)
					{
						$arr_is_deleted = array(
								"is_deleted"=>1
							);
						$this->transfers_model->remove_transfer_consignee_old_products($data["transfer_id"], $oldvalue["product_id"], $arr_is_deleted);
					}
				}
			}

			if(count($data["new_products"]) > 0)
			{
				$this->update_transfer_consignee_new_product($data["transfer_id"], $data["new_products"]);
			}

			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $result;
			echo json_encode($response);
		}
	}

	public function update_transfer_consignee_new_product($transfer_id, $data)
	{
		foreach ($data as $key => $value) {
			$arr_set_new_prod = array(
				"product_id"=>$value["product_id"],
				"transfer_log_id"=>$transfer_id,
				"transfer_method"=>$value["loading_method"],
				"is_deleted"=>0
				);

			$new_trans_prod_id = $this->transfers_model->save_new_transfer_product_map($arr_set_new_prod);

			$new_trans_origin = array(
					"product_map_id"=>$new_trans_prod_id,
					"quantity"=>$value["product_qty"],
					"unit_of_measure"=>$value["unit_of_measure_id"]
				);

			$this->transfers_model->add_transfer_origin($new_trans_origin);
			// $origin_result = $this->transfers_model->add_transfer_origin($origin_details);


		}
	}

	public function save_transfer_consignee_complete()
	{
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules("data", "Data", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
		else
		{
			// create start complete
			$get_start = $data["transfer_date_start"] ." ". $data["transfer_time_start"];
			$convert_start = strtotime($get_start);
			$new_data_start = date('Y-m-d G:i:s', $convert_start);

			// create end complete
			$get_end = $data["transfer_date_end"] ." ". $data["transfer_time_end"];
			$convert_end = strtotime($get_end);
			$new_data_end = date('Y-m-d G:i:s', $convert_end);

			$arr_complete_date = array(
					"date_start"=>$new_data_start,
					"date_complete"=>$new_data_end,
					"status"=>1
				);

			$this->transfers_model->update_transfer_consignee_goods($data["transfer_id"], $arr_complete_date);

			if(count($data["worker_info"]) > 0)
			{
				$this->add_worker_info($data["transfer_id"], $data["worker_info"]);
			}

			if(count($data["equipment_info"]) > 0)
			{
				$this->add_equipment_info($data["transfer_id"], $data["equipment_info"]);
			}


			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = "Successfully Completed";
			echo json_encode($response);

		}
	}

	public function add_worker_info($transfer_id, $worker_info)
	{
		foreach ($worker_info as $key => $value) {


			$worker_type_name = $value["destination"];
			$worker_type = $this->transfers_model->get_worker_type(strtolower($worker_type_name));
			$worker_type_id = 0;
			if(count($worker_type) > 0){
				$worker_type_id = $worker_type[0]["id"];
			}else{
				$worker_type_id = $this->transfers_model->add_worker_type(array(
					"name" => strtolower($worker_type_name)
				));
			}

			$arr_worker_list = array(
				"record_log_id"=>$transfer_id,
				"record_type"=>1,
				"name"=>$value["name"],
				"worker_type"=>$worker_type_id
				);

			$this->transfers_model->add_worker_list($arr_worker_list);
		}
	}

	public function add_equipment_info($transfer_id, $equipment_info)
	{
		foreach ($equipment_info as $key => $value) {

			$arr_equipment_list = array(
				"record_log_id"=>$transfer_id,
				"record_type"=>1,
				"name"=>$value["name"]
				);

			$this->transfers_model->add_equipment_list($arr_equipment_list);
		}
	}

	public function get_transfer_consignee_complete()
	{
		$transfer_id = $this->input->post('transfer_id');

		$this->form_validation->set_rules("transfer_id", "Transfer ID", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
		else
		{

			$transfer_log_data = $this->transfers_model->get_transfer_consign_complete_data($transfer_id);

			foreach ($transfer_log_data as $key => $value) {
				
				$workers_consignee_complete_data = $this->transfers_model->get_workers_consignee_complete_data($transfer_id);

				$equipment_consignee_complete_data = $this->transfers_model->get_equipment_consignee_complete_data($transfer_id);

				$documents_consignee_complete_data = $this->transfers_model->get_documents_consignee_complete_data($transfer_id);
				foreach ($documents_consignee_complete_data as $docukey => $docuvalue) {
					$documents_consignee_complete_data[$docukey]["date_added_formatted"] = date("j-M-Y g:i A", strtotime($documents_consignee_complete_data[$docukey]["date_added"]));
				}

				$notes_consignee_complete_data = $this->transfers_model->get_notes_consignee_complete_data($transfer_id);
				foreach ($notes_consignee_complete_data as $notekey => $notevalue) {
					$notes_consignee_complete_data[$notekey]["date_formatted"] = date("j-M-Y g:i A", strtotime($notes_consignee_complete_data[$notekey]["date_created"]));
				}

				$product_consignee_complete_data = $this->transfers_model->get_product_consignee_complete_data($transfer_id);
				foreach ($product_consignee_complete_data as $prodkey => $prodvalue) {
					$origin_consignee_complete_data = $this->transfers_model->get_origin_consignee_complete_data($transfer_id, $prodvalue["product_id"]);

					$product_consignee_complete_data[$prodkey]["batch_info"] = $origin_consignee_complete_data;
				}

				$buildNewDate = strtotime($value["date_issued"]);
				$new_date = date('F d, Y', $buildNewDate);
				$new_time = date('h:i A', $buildNewDate);
				$new_datetime = $new_date." | ".$new_time;

				$start_date = new DateTime($value["date_start"]);
				$since_start = $start_date->diff(new DateTime($value["date_complete"]));

				$convert_day = $since_start->d.' Days';
				$convert_hours = $since_start->h.' Hours';
				$convert_minutes = $since_start->i.' Minutes';

				$time_duration = $convert_day.",  ".$convert_hours.", ".$convert_minutes;



				$arr_collect_all = array(
						"transfer_id"=>$transfer_id,
						"to_number"=>$value["to_number"],
						"mode_of_transfer"=>$value["mode_of_transfer_name"],
						"reason"=>$value["reason_for_transfer"],
						"requestor"=>ucwords($value["requestor"]),
						"date_issued"=>$new_datetime,
						"date_start"=>date("j-M-Y g:i A", strtotime($value["date_start"])),
						"date_complete"=>date("j-M-Y g:i A", strtotime($value["date_complete"])),
						"date_duration"=>$time_duration,
						"workers_info"=>$workers_consignee_complete_data,
						"equipment_info"=>$equipment_consignee_complete_data,
						"documents_info"=>$documents_consignee_complete_data,
						"notes_info"=>$notes_consignee_complete_data,
						"product_info"=>$product_consignee_complete_data
					);



				header("Content-Type: application/json");

				$response["status"] = true;
				$response["message"] = "Success";
				$response["data"] = $arr_collect_all;
				echo json_encode($response);


			}





			
		}
	}

	public function get_all_transfer_consignee_records()
	{
		$transfer_data = $this->transfers_model->get_all_transfer_consignee_records();

		$user_data = $this->session->userdata('apollo_user');
		$arr_collet_now = array();

		foreach ($transfer_data as $key => $value) {
			$product_map = $this->transfers_model->get_transfer_product_map_all($value["id"]);

			$status = "";

			($value["status"] == 0) ? $status = "Ongoing" : $status = "Complete";

			$transfer_data_all = array(
				"transfer_id"=>$value["id"],
				"transfer_number"=>$value["to_number"],
				"transfer_date_issued"=>date("j-M-Y g:i A", strtotime($value["date_issued"])),
				"username"=>$user_data["first_name"]." ".$user_data["last_name"],
				"status"=>$status,
				"product_item"=>$product_map
				);

			array_push($arr_collet_now, $transfer_data_all);

			header("Content-Type: application/json");

			
		}

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $arr_collet_now;
		echo json_encode($response);
	}

	


	//=============================== END OF TRANSFER FOR CONSIGNEE ======================================


	public function add_note() {
		$note_details = $this->input->post('note_details');
		// print_r($note_details);
		$this->load->model('transfers_model');
		$this->transfers_model->add_notes($note_details);
	}

}

/* End of file transfers.php */
/* Location: ./application/modules/transfers/controllers/transfers.php */