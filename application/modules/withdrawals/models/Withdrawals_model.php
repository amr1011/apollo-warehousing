<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Withdrawals_model extends CI_Model {
	
	public function get_latest_receiving_number($type){
		$query = "SELECT * FROM withdrawal_log WHERE type = {$type} ORDER BY id DESC LIMIT 1";
		$sql = $this->db->query($query);
		return $sql->result_array();
	}


	/* FOR CONSIGNEE FUNCTIONS */

	public function get_receiving_consignee_record_completed()
	{
		$sql = "SELECT * FROM receiving_log WHERE type = 2 AND status = 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_consignees_from_receiving(){
		$sql = "SELECT c.* FROM receiving_consignee_map rcm 
				LEFT JOIN consignee c ON c.id = rcm.consignee_id
				LEFT JOIN receiving_log rl ON rl.id = rcm.receiving_log_id
				WHERE rl.status = 1 AND rl.type = 2
				GROUP BY c.id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_vessels(){
		$sql = "SELECT * FROM vessel_list WHERE is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_items_from_receiving(){
		$sql = "SELECT p.* FROM product_inventory_qty piq
				LEFT JOIN product p ON p.id = piq.product_id
				LEFT JOIN receiving_log rl ON piq.product_receiving_id = rl.id 
				WHERE rl.type = 2 AND rl.status = 1 AND piq.is_deleted = 0 GROUP BY piq.product_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_batch_from_product($product_id = 0){
		$sql = "SELECT b.* FROM batch b 
				LEFT JOIN storage_assignment sa ON b.storage_assignment_id = sa.id 
				LEFT JOIN receiving_log rl ON rl.id = sa.receiving_log_id 
				WHERE rl.status = 1 AND rl.type = 2 
				AND sa.product_inventory_qty_id IN (SELECT id FROM product_inventory_qty WHERE product_id = {$product_id})";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_storage_data($storage_id = 0){
		$query = "SELECT * FROM storage WHERE id = {$storage_id}";
		$sql = $this->db->query($query);
		return $sql->result_array();
	}

	public function create($data){
		$this->db->insert("withdrawal_log", $data);
		return $this->db->insert_id();
	}

	public function create_withrawal_consignee_details($data){
		$this->db->insert("withdrawal_consignee_details", $data);
		return $this->db->insert_id();
	}

	public function create_withdrawal_truck_details($data){
		$this->db->insert("withdrawal_truck_details", $data);
		return $this->db->insert_id();
	}

	public function create_withdrawal_vessel_details($data){
		$this->db->insert("withdrawal_vessel_details", $data);
		return $this->db->insert_id();
	}

	public function create_documents($data){
		$this->db->insert("withdrawal_documents", $data);
		return $this->db->insert_id();
	}

	public function create_notes($data){
		$this->db->insert("withdrawal_notes", $data);
		return $this->db->insert_id();
	}

	public function create_withrawal_consignee_swap($data){
		$this->db->insert("withdrawal_consignee_swap", $data);
		return $this->db->insert_id();
	}

	public function create_withdrawal_product($data){
		$this->db->insert("withdrawal_product", $data);
		return $this->db->insert_id();
	}

	public function update_withdrawal_product($id, $data){
		$this->db->where("id", $id);
		$this->db->update("withdrawal_product", $data);
	}

	public function get_all_consignee_withdrawal_logs($where = ' WHERE is_deleted = 0 AND type = 2 ORDER BY date_created ASC LIMIT 200'){
		$sql = "SELECT * FROM withdrawal_log ".$where;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_withdrawal_consignee_details($log_id)
	{
		$sql = "SELECT * FROM withdrawal_consignee_details WHERE withdrawal_log_id = {$log_id}";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_consignee_swap($log_id){
		$sql = "SELECT * FROM withdrawal_consignee_swap WHERE withdrawal_log_id = {$log_id} AND is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function update_withdrawal_log($id, $data){
		$this->db->where("id", $id);
		$this->db->update("withdrawal_log", $data);
	}

	public function update_consignee_details($id, $data){
		$this->db->where("id", $id);
		$this->db->update("withdrawal_consignee_details", $data);
	}

	public function update_withrawal_consignee_swap($id, $data){
		$this->db->where("id", $id);
		$this->db->update("withdrawal_consignee_swap", $data);
	}

	public function create_withdrawal_loading_information($data){
		$this->db->insert("withdrawal_loading_information", $data);
		return $this->db->insert_id();
	}

	public function update_withdrawal_loading_information($id, $data){
		$this->db->where("id", $id);
		$this->db->update("withdrawal_loading_information", $data);
	}

	public function create_withdrawal_batch($data){
		$this->db->insert("withdrawal_batch", $data);
		return $this->db->insert_id();
	}

	public function get_withdrawal_batches($withdrawal_product_id = 0){
		$sql = "SELECT * FROM withdrawal_batch WHERE withdrawal_product_id = {$withdrawal_product_id} AND is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_worker_type($where = ""){
		$sql = "SELECT * FROM worker_type {$where}";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function create_worker_type($data){
		$this->db->insert("worker_type", $data);
		return $this->db->insert_id();
	}

	public function create_worker_list($data){
		$this->db->insert("worker_list", $data);
		return $this->db->insert_id();
	}

	public function create_equipment_list($data){
		$this->db->insert("equipment_list", $data);
		return $this->db->insert_id();
	}

	public function get_worker_list($record_type, $record_log_id){
		$sql = "SELECT wl.*, wt.name AS worker_type_name FROM worker_list wl 
				LEFT JOIN worker_type wt ON wl.worker_type = wt.id
				WHERE wl.record_type = {$record_type} AND wl.record_log_id = {$record_log_id} ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_equipment_list($record_type, $record_log_id){
		$sql = "SELECT * FROM equipment_list WHERE record_type = {$record_type} AND record_log_id = {$record_log_id} ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function create_cdr_details($data){
		$this->db->insert("withdrawal_cdr_details", $data);
		return $this->db->insert_id();
	}

	public function create_cdr_seal_number($data){
		$this->db->insert("withdrawal_seal", $data);
		return $this->db->insert_id();
	}

	public function get_seal_numbers($log_id){
		$sql = "SELECT * FROM withdrawal_seal WHERE withdrawal_log_id = {$log_id} AND is_deleted = 0 ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	/* END OF CONSIGNEE FUNCTIONS */

	/* FOR COMPANY FUNCTIONS */

	public function company_get_all_withdrawals() {
		$sql = "SELECT *
			FROM withdrawal_log
			WHERE type = 1
			AND is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function company_get_all_products() {
		$sql = 'SELECT *
		FROM product
		WHERE is_deleted = 0';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_unit_of_measure() {
		$sql = 'SELECT *
		FROM unit_of_measures';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function create_company_withdrawal($withdrawal_details) {
		$this->db->insert('withdrawal_log', $withdrawal_details);
		return $this->db->insert_id();
	}

	public function create_company_details($company_details) {
		$this->db->insert('withdrawal_company_details', $company_details);
		return $this->db->insert_id();
	}

	public function get_company_withdrawal_details($withdrawal_id) {
		$sql = 'SELECT wl.id, wl.withdrawal_number, wl.user_id,
		wl.type, wl.status, wl.is_deleted, wl.date_created,
		wc.id as withdrawal_company_details_id, wc.requestor, wc.designation,
		wc.department, wc.request_for_withdrawal
		FROM withdrawal_log wl
		LEFT JOIN withdrawal_company_details wc ON wc.withdrawal_log_id = wl.id
		WHERE wl.id = "'.$withdrawal_id.'"';
		$query = $this->db->query($sql);
		return $query->row_array();
		// echo $withdrawal_id;
	}

	public function get_company_withdrawal_products($withdrawal_id) {
		$sql = "SELECT wp.id, wp.withdrawal_log_id, wp.product_id,
		wp.loading_method, wp.unit_of_measure_id, wp.qty, wp.status,
		p.name as product_name, p.sku as product_sku, p.image as product_image
		FROM withdrawal_product wp
		LEFT JOIN product p ON p.id = wp.product_id
		WHERE withdrawal_log_id = {$withdrawal_id}";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_batches($withdrawal_product_id) {
		$sql = "SELECT wb.id, wb.batch_id, wb.location_id,
		wb.qty, wb.withdrawal_product_id, wb.unit_of_measure_id,
		wb.is_deleted, b.name as batch_name
		FROM withdrawal_batch wb
		LEFT JOIN batch b ON b.id = wb.batch_id
		WHERE wb.withdrawal_product_id = $withdrawal_product_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function create_company_product_batches($batch) {
		$query = $this->db->insert('withdrawal_batch', $batch);
		return $this->db->insert_id();
	}

	public function get_company_withdrawal_notes($withdrawal_id) {
		$sql = "SELECT *
		FROM withdrawal_notes
		WHERE withdrawal_log_id = {$withdrawal_id}";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_company_withdrawal_documents($withdrawal_id) {
		$sql = "SELECT *
		FROM withdrawal_documents
		WHERE withdrawal_log_id = {$withdrawal_id}";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function save_document_details($document_details) {
		$query = $this->db->insert('withdrawal_documents', $document_details);
		return $this->db->insert_id();
	}

	public function get_company_product_batches($product_id) {
		$sql = "SELECT b.id as batch_id,
		b.name as batch_name, b.quantity as batch_quantity,
		b.storage_assignment_id as storage_assignment_id,
		um.name as unit_of_measure, um.id as unit_of_measure_id,
		piq.qty as inventory_quantity, piq.loading_method as inventory_loading_method,
		piq.bag as inventory_bag, piq.unit_of_measure_id as inventory_unit_of_measure,
		l.name as location_name, l.storage_id as location_storage_id,
		l.parent_id as location_parent_id, l.id as location_id,
		s.storage_id as storage_id, s.name as storage_name,
		s.id as storage_id_number,
		s.address as storage_address, s.description as storage_description,
		s.capacity as storage_capacity, v.name as vendor_name,
		p.id as product_id
		FROM batch b
		LEFT JOIN unit_of_measures um ON um.id = b.unit_of_measure_id
		LEFT JOIN storage_assignment sa ON sa.id = b.storage_assignment_id
		LEFT JOIN receiving_log rl ON rl.id = sa.receiving_log_id
		LEFT JOIN location l ON l.id = b.location_id
		LEFT JOIN storage s ON s.id = b.storage_id
		LEFT JOIN product_inventory_qty piq ON piq.id = sa.product_inventory_qty_id
		LEFT JOIN vendor v ON v.id = piq.vendor_id
		LEFT JOIN product p ON p.id = piq.id
		WHERE piq.product_id = $product_id
		AND rl.type = 1
		AND b.is_deleted = 0
		AND piq.is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function create_company_withdrawal_product($product_details) {
		$query = $this->db->insert('withdrawal_product', $product_details);
		return $this->db->insert_id();
	}

	public function create_company_withdrawal_note($note_details) {
		$query = $this->db->insert('withdrawal_notes', $note_details);
		return $this->db->insert_id();
	}

	public function create_company_documents($document_details) {
		$query = $this->db->insert('withdrawal_documents', $document_details);
		return $this->db->insert_id();
	}


	public function company_complete_withdrawal($withdrawal_id) {
		$update_details = array(
			'status' => 1
		);

		$this->db->where('id', $withdrawal_id);
		$this->db->update('withdrawal_log', $update_details);
		return $this->db->affected_rows();
	}

	/* END OF COMPANY FUNCTIONS */

	public function update_documents($id, $data){
		$this->db->where("id", $id);
		$this->db->update("withdrawal_documents", $data);
	}

	public function get_atl_documents($log_id){
		$sql = "SELECT * FROM withdrawal_documents WHERE withdrawal_log_id = {$log_id} AND document_name = 'ATL-FILE'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_documents($log_id){
		$sql = "SELECT * FROM withdrawal_documents WHERE withdrawal_log_id = {$log_id} ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_notes($log_id){
		$sql = "SELECT * FROM withdrawal_notes WHERE withdrawal_log_id = {$log_id} AND is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}


	public function get_cdr_details($log_id){
		$sql = "SELECT * FROM withdrawal_cdr_details WHERE withdrawal_log_id = {$log_id} AND is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_withdrawal_loading_information($log_id){
		$sql = "SELECT * FROM withdrawal_loading_information WHERE withdrawal_log_id = {$log_id} AND is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_withdrawal_product($log_id){
		$sql = "SELECT * FROM withdrawal_product WHERE withdrawal_log_id = {$log_id} AND status = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_withdrawal_product_batches($withdrawal_product_id) {
		$sql = "SELECT wb.id as id, wb.batch_id as batch_id,
		wb.location_id as location_id, wb.qty as qty,
		wb.withdrawal_product_id as withdrawal_product_id,
		wb.unit_of_measure_id as unit_of_measure_id,
		wb.is_deleted as is_deleted,
		b.name as batch_name
		FROM withdrawal_batch wb
		LEFT JOIN batch b ON b.id = wb.batch_id
		WHERE wb.withdrawal_product_id = '" . $withdrawal_product_id . "'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_trucking($log_id){
		$sql = "SELECT * FROM withdrawal_product WHERE withdrawal_log_id = {$log_id} ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_truck_details($log_id){
		$sql = "SELECT * FROM withdrawal_truck_details WHERE withdrawal_log_id = {$log_id} AND is_deleted = 0 ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_vessel_details($log_id){
		$sql = "SELECT * FROM withdrawal_vessel_details WHERE withdrawal_log_id = {$log_id} AND is_deleted = 0 ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function update_vessel_details($id, $data){
		$this->db->where("id", $id);
		$this->db->update("withdrawal_vessel_details", $data);
	}

	public function update_trucking_details($id, $data){
		$this->db->where("id", $id);
		$this->db->update("withdrawal_truck_details", $data);
	}

	public function get_location($id){
		$sql = "SELECT * FROM location WHERE id = {$id} AND is_deleted = 0 ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_locations() {
		$sql = 'SELECT *
		FROM location
		WHERE is_deleted = 0';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_products() {
		$sql = 'SELECT *
		FROM product';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function deduct_withdrawal($batch_id, $update_details) {
		$this->db->where('id', $batch_id);
		$this->db->update('batch', $update_details);
		return $this->db->affected_rows();
	}

	public function company_edit_record($id, $update_details) {
		$this->db->where('withdrawal_log_id', $id);
		$query = $this->db->update('withdrawal_company_details', $update_details);
		return $this->db->affected_rows();
	}

}

/* End of file Withdrawal_model.php\ */
/* Location: ./application/modules/withdrawals/models/Withdrawal_model.php\ */