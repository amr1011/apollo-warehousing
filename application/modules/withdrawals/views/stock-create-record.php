

		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="stock-withdrawal.php">Withdrawal - Company Goods</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color">Create Record</li>
				</ul>
			</div>
			<div class="semi-main">
				<div class="padding-top-30 margin-bottom-30 text-right width-100percent">
					<a href="stock-withdrawal.php">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					</a>
					<button class="btn general-btn modal-trigger" modal-target="confirm-create">Create Record</button>
				</div>
				<div class="clear"></div>
				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0">
					<p class="font-20 font-bold black-color padding-bottom-20">Withdrawal No. 1234567890</p>
					
					<div class="width-50percent display-inline-top">
						<div class="padding-bottom-20">
							<p class="font-14 font-bold display-inline-mid width-120">Item Requestor</p>
							<input type="text" class="display-inline-mid t-medium">
						</div>

						<div class="padding-bottom-20">
							<p class="font-14 font-bold display-inline-mid width-120">Designation: </p>
							<input type="text" class="display-inline-mid t-medium">						
						</div>

						<div class="padding-bottom-20">
							<p class="font-14 font-bold display-inline-mid width-120">Department: </p>
							<input type="text" class="display-inline-mid t-medium">					
						</div>
					</div>

					<div class="width-50percent display-inline-top">
						<p class="font-14 font-bold display-inline-top padding-right-20 margin-top-5">Request for Withdrawal:</p>
						<textarea class="t-medium"></textarea>
										
					</div>					
				</div>
				
				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white padding-bottom-20 ">
					<div class="width-100percent text-left padding-all-30 no-padding-top no-padding-left margin-bottom-30 border-bottom-small border-gray">
						<p class="font-20 font-bold black-color padding-bottom-20 ">Item List</p>
						<p class="font-14 font-bold display-inline-mid padding-right-20">Item: <span class="red-color display-inline-top">*</span></p>
						<div class="select dispaly-inline-mid large">
							<select class="transform-dd">
								<option value="Select">Select Item</option>
							</select>
						</div>
						<a href="#" class="display-inline-mid padding-left-20">
							<button class="btn general-btn padding-left-20 padding-right-20">Add Item</button>
						</a>
					</div>

					<div class="border-full padding-all-20">
						<div class="bggray-white height-190px ">
							<div class="height-100percent display-inline-mid width-230px">
								<img src="../assets/images/holcim.jpg" alt="" class="height-100percent width-100percent">
							</div>

							<div class="display-inline-top width-75percent padding-left-10">
								<div class="padding-all-15 bg-light-gray font-0 text-left">
									<p class="font-16 font-bold display-inline-mid width-50percent">SKU# 1234567890 - Holcim Cement</p>
									
									<div class="f-right width-20px margin-left-10">
										<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
									</div>
									<div class="clear"></div>
									
								</div>
								<div class="padding-all-15 text-left">
									<p class="display-inline-mid no-margin-all width-20percent font-500">Vendor</p>
									<p class="display-inline-mid font-400 no-margin-all width-20percent">Gold Ever Trading</p>
									<p class="display-inline-mid  width-20percent font-500 margin-left-13">Stock Balance</p>
									<div class="display-inline-mid margin-left-20 width-20percent">
										<p class="font-400 padding-left-20">1000 KG</p>
									</div>
								</div>

								<div class="bg-light-gray padding-all-10 text-left font-0 padding-left-15">
									<p class="display-inline-mid width-20percent font-500">Unit Price</p>
									<p class="display-inline-mid font-400 width-20percent">100.00 Php</p>
									<div class="display-inline-mid">
										<p class="display-inline-mid font-14 font-500 margin-left-20 padding-right-50">Qty to Withdraw</p>
										<input type="text" class="margin-left-10 display-inline-mid width-70px">
										<p class="font-14 font-400 display-inline-mid padding-left-5 padding-right-10">KG</p>
										<input type="text" class="display-inline-mid width-70px">
										<p class="font-14 font-400 display-inline-mid padding-left-5">Bags</p>
									</div>
								</div>
								<div class="padding-all-15 text-left">
									<p class="display-inline-mid no-margin-all width-20percent font-500">Description</p>
									<p class="display-inline-mid font-400 no-margin-all">Ang Cement na matibay</p>
									
									
								</div>
							</div>
						</div>
						<div class="assignment-display ">
									<div class="padding-top-20">
									
										<div class="margin-bottom-10 margin-top-10">
											<p class="no-margin-all font-20 font-500 f-left  padding-left-10 padding-top-5">Item Location</p>
											<div class="f-right">
												<button class="btn general-btn modal-trigger" modal-target="add-batch">Add a Batch</button>
											</div>
											<div class="clear"></div>
										</div>
										<table class="tbl-4c3h">
											<thead>
												<tr>
													<th class="width-25percent black-color no-padding-left">Batch Name</th>
													<th class="width-50percent no-padding-left">Storage Location</th>
													<th class="width-25percent black-color no-padding-left">Quantity</th>
												</tr>
											</thead>
										</table>

										<div class="transfer-prod-location font-0 bggray-white ">
											
											<!-- first - panel  -->
											<div class="storage-assign-panel position-rel">
												<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content bg-light-gray">
													<div class="width-25percent display-inline-mid text-center">
														<p class="font-14 font-400 black-color">DFD1234SD</p>
													</div>
													<div class="width-50percent text-center display-inline-mid height-auto line-height-25">
														<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
																											
													</div>
													<div class="width-25percent display-inline-mid text-center">
														<p class="font-14 font-400 black-color">50 KG</p>
													</div>
												</div>
												<div class="btn-hover-storage display-block" style="height: 55px; display: none; opacity: 0;">
													<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="">Edit Batch</button>
													<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="">Remove Batch</button>
												</div>
											</div>

											<div class="storage-assign-panel position-rel">
												<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content ">
													<div class="width-25percent display-inline-mid text-center">
														<p class="font-14 font-400 black-color">DFD1234SD</p>
													</div>
													<div class="width-50percent text-center display-inline-mid height-auto line-height-25">
														<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
																											
													</div>
													<div class="width-25percent display-inline-mid text-center">
														<p class="font-14 font-400 black-color">50 KG</p>
													</div>
												</div>
												<div class="btn-hover-storage display-block" style="height: 55px; display: none; opacity: 0;">
													<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="">Edit Batch</button>
													<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="">Remove Batch</button>
												</div>
											</div>
		
										</div>
									</div>
									<div class="border-top border-blue padding-all-10">
										<p class="font-14 font-500 black-color f-left">Total</p>
										<p class="font-14 font-500 black-color f-right margin-right-90">100 KG</p>
										<div class="clear"></div>
									</div>
								</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title padding-top-10 font-500 font-22 black-color"> Documents</h4>
							</a>

							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="upload-document">Upload a Document</button>
							</div>
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-20">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>

								<div class="table-content position-rel">
									<div class="content-show padding-all-10 tbl-dark-color">
										<div class="width-85per display-inline-mid">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">WithdrawalDocument</p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<button class="btn general-btn padding-left-30 padding-right-30">Remove</button>	
									</div>
								</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400 ">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-22 black-color padding-top-10"> Notes</h4>
							</a>
							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="add-notes">Add Notes</button>
							</div>
							<div class="clear"></div>
						</div>
						
					
						
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-20">

								<div class="border-full padding-all-10 margin-left-18">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10 font-400">Show Less</a>
									<div class="clear"></div>
								</div>

								<div class="border-full padding-all-10 margin-left-18 margin-top-10">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10 font-400">Show More</a>
									<div class="clear"></div>
								</div>									
							</div>
						</div>
					</div>
				</div>
				<div class="width-100percent text-right border-top-small border-gray padding-top-10">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button class="btn general-btn modal-trigger" modal-target="confirm-create">Create Record</button>
				</div>
		</div>
	</div>

	<!--MODALS-->

	<!--Start of Confirm Create Record MODAL-->
	<div class="modal-container" modal-id="confirm-create">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Create Record</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<p class="font-14 font-400">Are you sure you want to create Withdrawal No. 1234567890?</p>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="confirm-record">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Confirm Create Record MODAL-->

	

	<!--Start of Upload Document MODAL-->
	<div class="modal-container" modal-id="upload-document">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Upload Document</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">Dcoument Name:</p>
					<input type="text" class="width-231px display-inline-mid">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">File Location:</p>
					<input type="text" class="width-231px display-inline-mid">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Browse</button>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Upload Document MODAL-->

	<!--Start of Add Notes MODAL-->
	<div class="modal-container" modal-id="add-notes">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Add Notes</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
					<input type="text" class="width-380px display-inline-mid margin-left-10">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
					<textarea class="width-380px margin-left-10 height-250px"></textarea>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Notes MODAL-->

		<!--Start of Add Batch MODAL-->
		<div class="modal-container" modal-id="add-batch">
			<div class="modal-body  margin-top-100 margin-bottom-100">				

				<div class="modal-head">
					<h4 class="text-left">Add Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="text-left">
						
						<table class="width-100percent">
							<tbody>
								<tr>
									<td class="font-500 font-14 width-15percent">Item:</td>
									<td class="font-14 font-400">SKU No. 1234567890 - Japanese Corn</td>
								</tr>
								<tr>
									<td class="font-500 font-14 padding-top-15">Batch Name:</td>
									<td><div class="select large display-inline-mid padding-top-10">
											<select class="transform-dd">
												<option value="op1">CNEKIEK12345</option>
											</select>
										</div>
										<div class="display-inline-mid width-50percent font-400 padding-top-20 margin-left-10">									
											<p class="display-inline-mid font-14 width-150px">Amount to Withdraw:</p>
											<p class="display-inline-mid font-14">90 KG</p>
										</div>
									</td>
								</tr>
								<tr>
									<td class="font-500 font-14">Storage Location:</td>
									<td><div class="select large display-inline-mid padding-tp-10">
											<select class="transform-dd">
												<option value="loc1">Warehouse 1</option>
											</select>
										</div>
										<div class="display-inline-mid width-50percent font-400 padding-top-10 margin-left-10">
											<p class="display-inline-mid font-14 width-150px">Withdraw Balance:</p>
											<p class="display-inline-mid font-14">20 KG</p>
										</div>
									</td>								
								</tr>
							</tbody>
						</table>
					</div>
					<p class="font-14 font-500 no-margin-right display-inline-top width-15percent no-margin-left padding-left-5">Sub Location:</p>
					<div class="display-inline-top width-85per bggray-white box-shadow-dark">
						<table class="tbl-4c3h ">
							<thead>
								<tr>
									<th class="black-color width-60percent">Location Address</th>
									<th class="black-color width-20percent">Location Qty</th>
									<th class="black-color width-20percent no-padding-left">Amount</th>
								</tr>
							</thead>
						</table>
						<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
							<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
								<i class="fa fa-check font-22"></i>
							</div>
							<div class="width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="font-14 font-400 no-margin-all ">1000 KG</p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="t-small text-center super-width-100px" value="70">
							</div>
						</div>

						<div class="font-0 text-center location-check position-rel default-cursor">
							<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
								<i class="fa fa-check font-22 "></i>
							</div>
							<div class="width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="font-14 font-400 no-margin-all ">1000 KG</p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="t-small text-center super-width-100px" value="70">
							</div>
						</div>

						<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
							<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
								<i class="fa fa-check font-22 "></i>
							</div>
							<div class="width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="font-14 font-400 no-margin-all ">1000 KG</p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="t-small text-center super-width-100px" value="70">
							</div>
						</div>
					</div>

				
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
				</div>
			</div>	
		</div>
		<!--End of Add Batch MODAL-->


		<!--Start of edit Batch MODAL-->
		<div class="modal-container" modal-id="edit-batch">
			<div class="modal-body margin-top-100 margin-bottom-100">				

				<div class="modal-head">
					<h4 class="text-left">Edit Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

					<!-- content -->
				<div class="modal-content text-left">
					<div class="text-left">
						
						<table class="width-100percent">
							<tbody>
								<tr>
									<td class="font-500 font-14 width-15percent">Item:</td>
									<td class="font-14 font-400">SKU No. 1234567890 - Japanese Corn</td>
								</tr>
								<tr>
									<td class="font-500 font-14 padding-top-15">Batch Name:</td>
									<td><div class="select large display-inline-mid padding-top-10">
											<select class="transform-dd">
												<option value="op1">CNEKIEK12345</option>
											</select>
										</div>
										<div class="display-inline-mid width-50percent font-400 padding-top-20 margin-left-10">									
											<p class="display-inline-mid font-14 width-150px">Amount to Withdraw:</p>
											<p class="display-inline-mid font-14">90 KG</p>
										</div>
									</td>
								</tr>
								<tr>
									<td class="font-500 font-14">Storage Location:</td>
									<td><div class="select large display-inline-mid padding-tp-10">
											<select class="transform-dd">
												<option value="loc1">Warehouse 1</option>
											</select>
										</div>
										<div class="display-inline-mid width-50percent font-400 padding-top-10 margin-left-10">
											<p class="display-inline-mid font-14 width-150px">Withdraw Balance:</p>
											<p class="display-inline-mid font-14">20 KG</p>
										</div>
									</td>								
								</tr>
							</tbody>
						</table>
					</div>
					<p class="font-14 font-500 no-margin-right display-inline-top width-15percent no-margin-left padding-left-5">Sub Location:</p>
					<div class="display-inline-top width-85per bggray-white box-shadow-dark">
						<table class="tbl-4c3h ">
							<thead>
								<tr>
									<th class="black-color width-60percent">Location Address</th>
									<th class="black-color width-20percent">Location Qty</th>
									<th class="black-color width-20percent no-padding-left">Amount</th>
								</tr>
							</thead>
						</table>
						<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
							<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
								<i class="fa fa-check font-22"></i>
							</div>
							<div class="width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="font-14 font-400 no-margin-all ">1000 KG</p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="t-small text-center super-width-100px" value="70">
							</div>
						</div>

						<div class="font-0 text-center location-check position-rel default-cursor">
							<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
								<i class="fa fa-check font-22 "></i>
							</div>
							<div class="width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="font-14 font-400 no-margin-all ">1000 KG</p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="t-small text-center super-width-100px" value="70">
							</div>
						</div>

						<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
							<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
								<i class="fa fa-check font-22 "></i>
							</div>
							<div class="width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="font-14 font-400 no-margin-all ">1000 KG</p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="t-small text-center super-width-100px" value="70">
							</div>
						</div>
					</div>

				
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
				</div>
			</div>	
		</div>
		<!--End of edit Batch MODAL-->


		<div class="modal-container display-product-details" modal-id="confirm-record">
			<div class="modal-body small ">				

				<div class="modal-head">
					<h4 class="text-left">Mark as Complete</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Withdrawal No. 1234567890 has been created.</p>
					</div>
				</div>
			
				<div class="modal-footer text-right">	
					<a href="view-stock-goods.php">				
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Show Record</button>
					</a>
				</div>
			</div>	
		</div>


