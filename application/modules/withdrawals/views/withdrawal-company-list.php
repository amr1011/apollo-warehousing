
		<div class="main">
			<div class="center-main">
				<div class="mod-select padding-left-3 padding-top-60">			
					<div class="select f-left width-450px ">
						<select>
							<option value="Displaying All Consignee Withdrawal Records"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>						
				</div>
				<div class="f-right margin-top-10">	
					<a href="<?php echo BASEURL . 'withdrawals/withdrawal_company_create'; ?>">
						<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn">Create Record</button>				
					</a>
					<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn dropdown-btn">View Filter / Search</button>			
				</div>
				<div class="clear"></div>

				<div class="dropdown-search">
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr class="width-100percent">
								<td class="width-75px">Search</td>
								<td>
									<div class="select width-200px font-400">
										<select>
											<option value="items">Items</option>
										</select>
									</div>
								</td>
								<td class="width-100percent">
									<input type="text" />
								</td>
							</tr>
							<tr class="width-100percent">
								<td>Filter By:</td>
								<td>
									<div class="input-group width-200px f-left italic">
									<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
									<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar white-color">
									</span></span>
									</div>
								</td>
								<td class="search-action">
									<div class="f-left">
										
										<div class="select width-150px italic">
											<select>
												<option value="status">Status</option>
											</select>
										</div>
									</div>
									<div class="f-right margin-left-30">
										<p class="display-inline-mid">Sort By:</p>
										<div class="select width-150px display-inline-mid italic">
											<select>
												<option value="withdrawal No." >Withdrawal No.</option>
											</select>
										</div>
										<div class="select width-150px display-inline-mid italic">
											<select>
												<option value="ascending">Ascending</option>
												<option value="descending">Descending</option>
											</select>
										</div>
									</div>
									<div class="clear"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10">
						<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
						<p class="display-inline-mid font-4 font-bold">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 ">Search</button>
					<div class="clear"></div>
				</div>


				<div class="withdrawal-list-div width-100percent bggray-white">

					<table class="tbl-4c3h margin-top-30">
						<thead>
							<tr>
								<th class="width-15percent black-color no-padding-left">Withdrawal No.</th>
								<th class="width-15percent black-color no-padding-left">Withdrawal Date</th>
								<th class="width-40percent black-color no-padding-left">Item Description</th>
								<th class="width-15percent black-color no-padding-left">Owner</th>
								<th class="width-15percent black-color no-padding-left">Status</th>												
							</tr>
						</thead>
					</table>

					<div class="withdrawal-ongoing-template tbl-like hover-transfer-tbl" style="display:none;">

						<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
							<div class="width-15percent display-inline-mid text-center">
								<p class="withdrawal-number font-400 font-14 f-none width-100percent">1234567890</p>						
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="withdrawal-date font-400 font-14 f-none width-100percent">22-Oct-2015</p>
							</div>
							<div class="width-40percent display-inline-mid font-400 text-center">
								<ul class="withdrawal-item-list  text-left text-indent-20 padding-left-50 font-14">
									<!-- <li>#12345678 - Cement</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>	 -->								
								</ul>
							</div>
							<div class="width-15percent font-400 display-inline-mid text-center">
								<div class="owner-img width-30per display-inline-mid ">
									<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
								</div>
								<p class="withdrawal-owner width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none text-center width-100percent">Ongoing</p>							
							</div>
							
						</div>

						<div class="hover-tbl "> 
							<button class="btn-view-record btn general-btn margin-right-10">View Record</button>
							<button class="btn-complete-record btn general-btn modal-trigger" modal-target="are-you-sure">Mark as Complete</button>		
						</div>

					</div>

					<div class="withdrawal-complete-template tbl-like hover-transfer-tbl" style="display:none;">

						<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
							<div class="width-15percent display-inline-mid text-center">
								<p class="withdrawal-number font-400 font-14 f-none width-100percent">1234567890</p>						
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="withdrawal-date font-400 font-14 f-none width-100percent">22-Oct-2015</p>
							</div>
							<div class="width-40percent display-inline-mid font-400 text-center">
								<ul class="withdrawal-item-list text-left text-indent-20 padding-left-50 font-14">
									<!-- <li>#12345678 - Cement</li>
									<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>	 -->								
								</ul>
							</div>
							<div class="width-15percent font-400 display-inline-mid text-center">
								<div class="owner-img width-30per display-inline-mid ">
									<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
								</div>
								<p class="withdrawal-owner width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
							</div>
							<div class="width-15percent display-inline-mid text-center">
								<p class="font-400 font-14 f-none text-center width-100percent">Complete</p>							
							</div>
							
						</div>

						<div class="hover-tbl "> 
							<button class="btn-view-completed btn general-btn margin-right-10">View Record</button>
						</div>

					</div>

				</div>

			</div>
		</div>

		<div class="modal-container complete-product-modal" modal-id="are-you-sure">
			<div class="modal-body width-550px">				

				<div class="modal-head">
					<h4 class="text-left">Mark as Complete</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-all-10">
					<p class="font-14 font-400">Are you sure you want to mark this withdrawal record as complete?</p>
				</div>
				</div>
			
				<div class="modal-footer text-right">	
					<div class="f-left">
						<button type="button" class="btn-complete-show-record font-12 btn btn-primary font-12 display-inline-mid withdrawal-show-record">Show Record</button>					
					</div>
					<div class="f-right width-200px">
						<button type="button" class="btn-cancel-complete font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>					
						<button type="button" class="btn-confirm-complete font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 submit-btn">Confirm</button>									
					</div>
					<div class="clear"></div>
				</div>
			</div>	
		</div>

		<div class="modal-container display-product-details completed-record-modal" modal-id="product-details">
			<div class="modal-body small ">				

				<div class="modal-head">
					<h4 class="text-left">Mark as Complete</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-all-10 bggray-7cace5">
					<p class="font-14 font-400 white-color">Withdrawal No. <span class='withdrawal_number'>1234567890</span> has been completed</p>
				</div>
				</div>
			
				<div class="modal-footer text-right">	
									
						<button type="button" class="btn-show-completed-record font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Show Record</button>
					</a>
				</div>
			</div>	
		</div>

		<!-- start of generating view modal -->
		<div class="modal-container modal-generate-view" modal-id="generate-view">
		    <div class="modal-body small">
		        <div class="modal-head">
		            <h4 class="text-left">Message</h4>
		        </div>
		        <!-- content -->
		        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
		            <div class="padding-all-10 bggray-7cace5">
		                <p class="modal-message font-16 font-400 white-color">Generating view, please wait...</p>
		            </div>
		        </div>
		        <div class="modal-footer text-right" style="height: 50px;">
		        </div>
		    </div>
		</div>
		<!-- end of generating view modal -->
