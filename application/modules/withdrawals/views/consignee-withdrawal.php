
		<div class="main">
			<div class="center-main">
				<div class="mod-select padding-left-3 padding-top-60">			
					<div class="select f-left width-450px ">
						<select class="transform-dd">
							<option value="Displaying All Consignee Withdrawal Records"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>						
				</div>
				<div class="f-right margin-top-10">	
					<a href="create-consignee.php">
						<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn">Create Record</button>				
					</a>
					<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn dropdown-btn">View Filter / Search</button>			
				</div>
				<div class="clear"></div>

				<div class="dropdown-search">
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr>
								<td class="font-15 font-400 black-color width-10percent">Search:</td>
								<td><div class="select large">
										<select class="transform-dd">
											<option value="search1">Withdrawal No.</option>
											<option value="search2">ATL No.</option>
											<option value="search3">CDR No.</option>
											<option value="search4">Items</option>

										</select>
									</div>
								</td><td class="width-75percent">
									<input type="text" class="t-small width-100per">
								</td>
							</tr>
							<tr>
								<td class="font-15 font-400 black-color width-10percent">Filter By:</td>
								<td>
									<div class="input-group">
										<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Withdrawal Date" data-date-format="MM/DD/YYYY" type="text">
										<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
									</div>

								</td>								
								<td class="search-action">
									<div class="f-left">
										<div class="select small display-inline-mid">
											<select class="transform-dd">
												<option value="type1">Vessel</option>
												<option value="type2">Truck</option>
											</select>
										</div>	
										<div class="select small display-inline-mid">
											<select class="transform-dd">
												<option value="stats1">Ongoing</option>
												<option value="stats2">Complete</option>
											</select>
										</div>																		
									</div>
									<div class="f-right margin-left-30">
										<p class="display-inline-mid">Sort By:</p>
										<div class="select medium display-inline-mid">
											<select class="transform-dd">
												<option value="sort1">Withdrawal No.</option>
												<option value="sort2">ATL No.</option>
												<option value="sort3">CDR No.</option>
												<option value="sort4">Withdrawal Date</option>
												<option value="sort5">Owner</option>
												<option value="sort6">Status</option>

											</select>
										</div>
										<div class="select medium display-inline-mid">
											<select class="transform-dd">
												<option value="ascending">Ascending</option>
												<option value="descending">Descending</option>
											</select>
										</div>
									</div>
									<div class="clear"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10 display-search-result">
						<i class="fa fa-2x display-inline-mid fa-caret-right"></i>
						<p class="display-inline-mid font-15 font-bold margin-left-10">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 margin-right-30">Search</button>
					<div class="clear"></div>
					<div class="search-result padding-all-10">
						<div class="margin-left-50">
							<p class="font-400 font-15 black-color f-left">Who can also view this Search Result? </p>
							<div class="clear"></div>
							
							<div class="f-left">
								<div class="display-inline-mid margin-right-10">
									<input type="radio" id="me" name="search-result" class="width-20px transform-scale">
									<label for="me" class="margin-bottom-5 default-cursor font-15 black-color font-400">Only Me</label>
								</div>
								<div class="display-inline-mid margin-right-50">
									<input type="radio" id="selected" name="search-result" class="width-20px transform-scale">
									<label for="selected" class="margin-bottom-5 default-cursor font-15 black-color font-400">With Selected Members</label>
								</div>
								<div class="display-inline-mid member-selection">
									<div class="member-container">
										<p>1 Member Selected</p>
										<i class=" fa fa-caret-down fa-2x"></i>
									</div>
									<div class="popup_person_list">
										<div class="popup_person_list_div">
											
											<div class="thumb_list_view small marked">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia Ricardo Gomez Kuala Lumpur Eklabu</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

										</div>										
									</div>
								</div>
								<div class="display-inline-mid margin-left-50">								
									<!-- dropdown here with effects  -->
									<button type="button" class="general-btn">Save Search Results</button>
								</div>
							</div>
							<div class="clear"></div>
						</div>

					</div>
				</div>
	
				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left margin-top-30">
					<div class="width-100percent bggray-white">

						<table class="tbl-4c3h margin-top-30">
							<thead>
								<tr>
									<th class="width-10percent black-color no-padding-left">Withdrawal No.</th>
									<th class="width-10percent black-color no-padding-left">ATL No.</th>
									<th class="width-10percent black-color no-padding-left">CDR No.</th>									
									<th class="width-10percent black-color no-padding-left">Withdrawal Date</th>
									<th class="width-10percent black-color no-padding-left">Origin</th>
									<th class="width-25percent black-color no-padding-left">Items</th>
									<th class="width-15percent black-color no-padding-left">Owner</th>
									<th class="width-10percent black-color no-padding-left">Status</th>												
								</tr>
							</thead>
						</table>

						<div class="tbl-like hover-transfer-tbl">

							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">Vessel</p>						
								</div>
								<div class="width-25percent display-inline-mid font-400 text-center">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
										<li>#12345678 - Cement</li>
										<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>									
									</ul>
								</div>
								<div class="width-15percent font-400 display-inline-mid text-center">
									<div class="owner-img width-30per display-inline-mid ">
										<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none text-center width-100percent">Ongoing</p>							
								</div>
								
							</div>

							<div class="hover-tbl text-center"> 
								<a href="consignee-proceed-loading-vessel.php">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>
								<a href="consignee-create-loading-vessel.php">
									<button class="btn general-btn margin-right-10">Proceed to Loading</button>
								</a>								
																						
							</div>
						</div>

						<div class="tbl-like hover-transfer-tbl">

							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">Vessel</p>						
								</div>
								<div class="width-25percent display-inline-mid font-400 text-center">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
										<li>#12345678 - Cement</li>
										<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>									
									</ul>
								</div>
								<div class="width-15percent font-400 display-inline-mid text-center">
									<div class="owner-img width-30per display-inline-mid ">
										<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none text-center width-100percent">Ongoing</p>							
								</div>
								
							</div>

							<div class="hover-tbl text-center"> 
								<a href="consignee-view-loading-vessel.php">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>
								<a href="consignee-generatecdr-vessel.php">
									<button class="btn general-btn margin-right-10">Generate CDR</button>
								</a>	
							</div>

						</div>

						<div class="tbl-like hover-transfer-tbl">

							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">Vessel</p>						
								</div>
								<div class="width-25percent display-inline-mid font-400 text-center">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
										<li>#12345678 - Cement</li>
										<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>														
									</ul>
								</div>
								<div class="width-15percent font-400 display-inline-mid text-center">
									<div class="owner-img width-30per display-inline-mid ">
										<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none text-center width-100percent">Ongoing</p>							
								</div>
								
							</div>

							<div class="hover-tbl text-center"> 
								<a href="consignee-view-generatedcdr-vessel.php">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>
								<a href="consignee-view-loading-vessel.php">
									<button class="btn general-btn margin-right-10">Complete Loading</button>
								</a>							
														
							</div>
						</div>

						<div class="tbl-like hover-transfer-tbl">
							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">Vessel</p>						
								</div>
								<div class="width-25percent display-inline-mid font-400 text-center">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
										<li>#12345678 - Cement</li>
										<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
															
									</ul>
								</div>
								<div class="width-15percent font-400 display-inline-mid text-center">
									<div class="owner-img width-30per display-inline-mid ">
										<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none text-center width-100percent">Complete</p>							
								</div>
								
							</div>
							<div class="hover-tbl text-center"> 
								<a href="consignee-view-complete-vessel.php">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>	
							</div>
						</div>

						<div class="tbl-like hover-transfer-tbl">
							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">Truck</p>						
								</div>
								<div class="width-25percent display-inline-mid font-400 text-center">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
										<li>#12345678 - Cement</li>
										<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
																
									</ul>
								</div>
								<div class="width-15percent font-400 display-inline-mid text-center">
									<div class="owner-img width-30per display-inline-mid ">
										<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none text-center width-100percent">Ongoing</p>							
								</div>
								
							</div>
							<div class="hover-tbl text-center"> 
								<a href="consignee-proceed-loading-truck.php">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>
								<a href="consignee-create-loading-truck.php">
									<button class="btn general-btn margin-right-10">Proceed to Loading</button>
								</a>
							</div>
						</div>


						<div class="tbl-like hover-transfer-tbl">
							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">Truck</p>						
								</div>
								<div class="width-25percent display-inline-mid font-400 text-center">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
										<li>#12345678 - Cement</li>
										<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
																
									</ul>
								</div>
								<div class="width-15percent font-400 display-inline-mid text-center">
									<div class="owner-img width-30per display-inline-mid ">
										<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none text-center width-100percent">Ongoing</p>							
								</div>
								
							</div>
							<div class="hover-tbl text-center"> 
								<a href="consignee-view-loading-truck.php">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>
								<a href="consignee-generatecdr-truck.php">
									<button class="btn general-btn margin-right-10">Generate CDR</button>
								</a>
							</div>
						</div>


						<div class="tbl-like hover-transfer-tbl">
							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">Truck</p>						
								</div>
								<div class="width-25percent display-inline-mid font-400 text-center">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
										<li>#12345678 - Cement</li>
										<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
																
									</ul>
								</div>
								<div class="width-15percent font-400 display-inline-mid text-center">
									<div class="owner-img width-30per display-inline-mid ">
										<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none text-center width-100percent">Ongoing</p>							
								</div>
								
							</div>
							<div class="hover-tbl text-center"> 
								<a href="consignee-view-generatedcdr-truck.php">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>
								<a href="consignee-view-loading-truck.php">
									<button class="btn general-btn margin-right-10">Complete Loading</button>
								</a>
							</div>
						</div>

						
						<div class="tbl-like hover-transfer-tbl">
							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">1234567890</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">123456</p>						
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">22-Oct-2015</p>
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none width-100percent">Truck</p>						
								</div>
								<div class="width-25percent display-inline-mid font-400 text-center">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14">
										<li>#12345678 - Cement</li>
										<li>#12345679 - Steel(10m X 40m, 1/4 in.)</li>
																
									</ul>
								</div>
								<div class="width-15percent font-400 display-inline-mid text-center">
									<div class="owner-img width-30per display-inline-mid ">
										<img src="../assets/images/profile/profile_d.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="width-10percent display-inline-mid text-center">
									<p class="font-400 font-14 f-none text-center width-100percent">Complete</p>							
								</div>
								
							</div>
							<div class="hover-tbl text-center"> 
								<a href="consignee-view-complete-truck.php">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<div class="modal-container complete-product-modal " modal-id="are-you-sure">
			<div class="modal-body width-550px">				

				<div class="modal-head">
					<h4 class="text-left">Mark as Complete</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-all-10">
					<p class="font-14 font-400">Are you sure you want to mark this withdrawal record as complete?</p>
				</div>
				</div>
			
				<div class="modal-footer text-right">	
					<div class="f-left">
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid withdrawal-show-record">Show Record</button>					
					</div>
					<div class="f-right width-200px">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>					
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 submit-btn">Confirm</button>									
					</div>
					<div class="clear"></div>
				</div>
			</div>	
		</div>

		<div class="modal-container display-product-details" modal-id="product-details">
			<div class="modal-body small ">				

				<div class="modal-head">
					<h4 class="text-left">Mark as Complete</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Withdrawal No. 1234567890 has been completed</p>
					</div>
				</div>
			
				<div class="modal-footer text-right">	
					<a href="stock-withdrawal-complete.php">				
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Show Record</button>
					</a>
				</div>
			</div>	
		</div>

	