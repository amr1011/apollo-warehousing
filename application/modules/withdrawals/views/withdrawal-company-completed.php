
		<div class="main">

			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL . 'withdrawals/company_record_list'; ?>">Withdrawal - Company Goods</a></li>
					<li><span>&gt;</span></li>						
					<li class=" black-color bread-show font-bold" >Withdrawal No. <span class="withdrawal-number">1234567890</span> (Complete)</li>
					
				</ul>
			</div>

			<div class="semi-main">
				
				<!-- receiving accordion here  -->
				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white margin-top-50">
					<div class="panel-group">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">								
								<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10 "> Withdrawal Information</h4>
							</a>					
																						
							<div class="clear"></div>
															
						</div>

						<div class="panel-collapse collapse in ">
							<div class="panel-body">
								<div class="display-inline-mid width-50per margin-top-10">
									<div class="text-left">
										<p class="f-left font-14 font-bold width-40percent margin-top-2">Withdrawal No.:</p>
										<p class="withdrawal-number f-left font-16 font-bold width-50percent ">123456</p>
										<div class="clear"></div>
									</div>

									<div class="text-left padding-top-10">
										<p class="f-left font-14 font-bold width-40percent margin-top-2 ">Date and Time Issued:</p>
										<p class="date-time-issued f-left font-16 font-bold width-50percent ">September 10, 2015 | 08:25 AM</p>
										<div class="clear"></div>
									</div>					
								</div>

								<div class="display-inline-top width-50per margin-top-10">
															
									<div class="text-left">
										<p class="f-left font-14 font-bold  width-30percent margin-top-2 margin-left-10">Status:</p>
										<p class="f-left font-16 font-bold width-50percent">Complete</p>
										<div class="clear"></div>
									</div>

								</div>

								<div class="width-100per margin-top-30 padding-top-10 text-left border-top-light">
									<h4 class=" font-500 font-18  padding-top-10 ">Requestor Information</h4>

									<div class="display-inline-top width-100per margin-top-20  text-left ">
										<div class="padding-all-15  bg-light-gray">
											<div class="width-50percent display-inline-mid">
												<p class="font-bold	f-left width-35percent margin-left-10">Item Requestor</p>
												<p class="requestor f-left font-400 margin-left-10">Chino Cruz</p>
												<div class="clear"></div>
											</div>
											<div class="width-45percent display-inline-mid">
												<p class="font-bold	f-left width-45percent margin-left-10">Reason for Withdrawal</p>
												<p class="reason-for-withdrawal f-left width-50percent font-400">Canteen Construction</p>
												<div class="clear"></div>
											</div>
										</div>

										<div class="padding-all-15">
											<div class="width-50percent display-inline-mid">
												<p class="font-bold	f-left width-35percent margin-left-10">Designation</p>
												<p class="designation f-left font-400 margin-left-10">Supreme Commander of the Alliance</p>
												<div class="clear"></div>
											</div>											
										</div>								
										<div class="padding-all-15  bg-light-gray">
											<div class="width-50percent display-inline-mid">
												<p class="font-bold	f-left width-35percent margin-left-10">Department</p>
												<p class="department f-left font-400 margin-left-10">Housekeeping</p>
												<div class="clear"></div>
											</div>
											
										</div>																	
									</div>							
									<div class="clear"></div>									
								</div>
							</div>
						</div>
					</div>						
				</div>
						
				

				<div class="product-list">

						<div class="product-original-template border-top border-blue box-shadow-dark margin-bottom-20 bggray-white" style="display:none;">
							<div class="head panel-group text-left" >
								<div class="panel-heading font-14 font-400 margin-top-5 margin-bottom-5">
									<a class="colapsed black-color " href="#">								
										<h4 class="product-sku-name panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10 "> SKU #1234567890 - Holcim Cement</h4>
									</a>					
									<!-- <div class=" f-right width-200px text-right">
										<button type="button" class="font-12 btn btn-default font-12 display-inline-mid red-color hide-cancel">Cancel</button>		
										<button class="btn general-btn display-inline-mid min-width-100px withdraw-edit-btn">Edit</button>						
									</div> -->
									<div class="clear"></div>
								</div>
								
								<div class="panel-collapse collapse in">
									<div class="panel-body padding-all-20 " >
										<div class="bggray-white border-full padding-all-20 height-210px box-shadow-dark font-0">
											<div class="product-image height-100percent display-inline-mid width-25percent ">
												<!-- <img src="../assets/images/corn.jpg" alt="" class="height-100percent"> -->
												<!-- <img src="../assets/images/holcim.jpg" alt="" class="height-100percent width-100percent"> -->
											</div>

											<!-- ================================================== -->
											<!-- ================PRODUCT DESCRIPTION=============== -->
											<!-- ================================================== -->
											<div class="display-inline-top width-75percent padding-left-10 font-0">
											
												<div class="padding-all-15 bg-light-gray">
													<p class="f-left width-150px font-bold">Description</p>
													<p class="product-description f-left">Ang Cement na matibay</p>
													<div class="clear"></div>
												</div>

												<div class="padding-all-15">
													<p class="f-left no-margin-all width-150px font-bold">Transfer Method</p>
													<p class="product-transfer-method f-left transfer-display-piece">By Piece</p>
													<div class="clear"></div>
												</div>
												
												<div class="padding-all-15 bg-light-gray">
													<p class="f-left width-150px font-bold">Unit of measure</p>
													<p class="product-unit-of-measure f-left transfer-display-piece">Test</p>
													<div class="clear"></div>
												</div>

												
												<div class="padding-all-15">
													<p class="f-left width-150px font-bold">Total QTY</p>
													<p class="total-qty-to-transfer f-left width-180px transfer-display-piece ">100 KG (10Bags)</p>
													<div class="clear"></div>
												</div>
							
											</div>
											<!-- ================================================== -->
											<!-- =============END PRODUCT DESCRIPTION============== -->
											<!-- ================================================== -->
										</div>

										<div class="assignment-display ">
											<div class="padding-top-20">
												<div class="margin-bottom-10 margin-top-10">
													<p class="no-margin-all font-20 font-500 f-left  padding-left-10 padding-top-5">Item Location</p>
													<div class="f-right withdraw-batch">
														<button class="btn general-btn modal-trigger" modal-target="add-batch">Add a Batch</button>
													</div>
													<div class="clear"></div>
												</div>
												<table class="tbl-4c3h">
													<thead>
														<tr>
															<th class="width-25percent black-color no-padding-left">Batch Name</th>
															<th class="width-50percent no-padding-left">Storage Location</th>
															<th class="width-25percent black-color no-padding-left">Quantity</th>
														</tr>
													</thead>
												</table>

												<div class="product-batches-list transfer-prod-location font-0 bggray-white ">
													
													<!-- first - panel  -->
													<div class="batch-gray-template storage-assign-panel position-rel" style="display:none;">
														<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change">
															<div class="width-25percent display-inline-mid text-center">
																<p class="batch-withdrawal-number font-14 font-400">123468469</p>
															</div>
															<div class="withdraw-batch-breadcrumb width-50percent text-center display-inline-mid height-auto line-height-25">
																<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>													
															</div>
															<div class="width-25percent display-inline-mid text-center">
																<p class="withdraw-batch-qty font-14 font-400">50 KG</p>
															</div>
														</div>
														<!-- <div class="btn-hover-storage ">
															<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>
															<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>
														</div> -->
													</div>

													<div class="batch-white-template storage-assign-panel position-rel" style="display:none;">
														<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content ">
															<div class="width-25percent display-inline-mid text-center">
																<p class="batch-withdrawal-number font-14 font-400">123468469</p>
															</div>
															<div class="withdraw-batch-breadcrumb width-50percent text-center display-inline-mid height-auto line-height-25">
																<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>

															</div>
															<div class="width-25percent display-inline-mid text-center">
																<p class="withdraw-batch-qty font-14 font-400">50 KG</p>
															</div>
														</div>
														<!-- <div class="btn-hover-storage">
															<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>
															<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>
														</div> -->
													</div>

													
												</div>

												<div class="total-amount-storage margin-top-40">
													<p class="first-text margin-top-10">Total</p>
													<p class="total-withdrawal-qty second-text margin-right-75 margin-top-10">100 KG</p>
													<div class="clear"></div>
												</div>

											</div>
										</div>
										
										
									</div>
								</div>
							</div>
						</div>

				</div>			

				

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-20 black-color padding-top-10 padding-bottom-10"> Documents</h4>
							</a>
							<div class="f-right">
								<!-- <button class="btn general-btn modal-trigger" modal-target="upload-documents">Upload a Document</button> -->
							</div>
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-10 document-list">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>

								<div class="table-content position-rel tbl-dark-color document-template" style="display:none;">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid padding-left-10">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid document-name">RandomDocument1</p>
										</div>
										<p class=" display-inline-mid document-date">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">View</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn">Download</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div>

								<!-- <div class="table-content position-rel">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid padding-left-10">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">RandomDocument2</p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">View</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn">Download</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div>

								<div class="table-content position-rel tbl-dark-color">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid padding-left-10">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">RandomDocument3</p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">View</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn">Download</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div> -->
																
							</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400 ">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-22 black-color padding-top-10 padding-bottom-10"> Notes</h4>
							</a>
							<!-- <div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="add-notes">Add Notes</button>
							</div> -->
							<div class="clear"></div>
						</div>
						
					
						
						<div class="panel-collapse collapse in">
							<div class="note-list panel-body padding-all-20">

								<!-- <div class="border-full padding-all-10 margin-left-18">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10 font-400">Show Less</a>
									<div class="clear"></div>
								</div> -->

								<div class="note-template border-full padding-all-10 margin-left-5 margin-top-10 notes-contain" style="display:none;">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="note-subject-display f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="note-date-time-display f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="note-message-display font-14 font-400 no-padding-left padding-all-10 show-notes-message"></p>
									<a href="" class="f-right padding-all-10 font-400 show-notes-messge-length">Show More</a>
									<div class="clear"></div>
								</div>									
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

		<!-- ======================================== -->
		<!-- ==========GENERATE VIEW MODAL=========== -->
		<!-- ======================================== -->
		<div class="modal-container modal-generate-view" modal-id="generate-view">
		    <div class="modal-body small">
		        <div class="modal-head">
		            <h4 class="text-left">Message</h4>
		        </div>
		        <!-- content -->
		        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
		            <div class="padding-all-10 bggray-7cace5">
		                <p class="modal-message font-16 font-400 white-color">Generating view, please wait...</p>
		            </div>
		        </div>
		        <div class="modal-footer text-right" style="height: 50px;">
		        </div>
		    </div>
		</div>
		<!-- ======================================== -->
		<!-- =======END GENERATE VIEW MODAL========== -->
		<!-- ======================================== -->
