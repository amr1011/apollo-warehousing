
		<div class="main">

			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL . 'withdrawals/company_record_list'; ?>">Withdrawal - Company Goods</a></li>
					<li><span>&gt;</span></li>						
					<li class="black-color bread-show font-bold" >Withdrawal No. <span class="withdrawal-number">1234567890</span></li>
					
				</ul>
			</div>

			<div class="semi-main">

				<div class="f-right margin-bottom-10 width-300px text-right margin-bottom-20">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color btn-cancel-hide">Cancel</button>					
					<button class="btn-complete-withdrawal btn general-btn click-check-btn display-inline-mid ">Mark as Complete</button>					
				</div>
				<div class="clear"></div>

				<!-- receiving accordion here  -->
				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white">
					<div class="panel-group">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">								
								<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10 "> Withdrawal Information</h4>
							</a>					
							
								<button class="btn btn-edit-record general-btn f-right width-100px">Edit</button>			
							
							<div class="clear"></div>
															
						</div>

						<div class="panel-collapse collapse in ">
							<div class="panel-body">
								<div class="display-inline-mid width-50per margin-top-10">
									<div class="text-left">
										<p class="f-left font-14 font-bold width-40percent margin-top-2">Withdrawal No.:</p>
										<p class="withdrawal-number f-left font-16 font-bold width-50percent ">123456</p>
										<div class="clear"></div>
									</div>

									<div class="text-left padding-top-10">
										<p class="f-left font-14 font-bold width-40percent margin-top-2 ">Date and Time Issued:</p>
										<p class="date-time-issued f-left font-16 font-bold width-50percent ">September 10, 2015 | 08:25 AM</p>
										<div class="clear"></div>
									</div>					
								</div>

								<div class="display-inline-top width-50per margin-top-10">
															
									<div class="text-left">
										<p class="f-left font-14 font-bold  width-30percent margin-top-2 margin-left-10">Status:</p>
										<p class="f-left font-16 font-bold width-50percent">Ongoing</p>
										<div class="clear"></div>
									</div>

								</div>

								<div class="width-100per margin-top-30 padding-top-10 text-left border-top-light">
									<h4 class=" font-500 font-18  padding-top-10 ">Requestor Information</h4>

									<div class="display-inline-top width-100per margin-top-20  text-left ">
										<div class="padding-all-15  bg-light-gray">
											<div class="width-50percent display-inline-mid">
												<p class="font-bold	f-left width-35percent margin-left-10">Item Requestor</p>
												<p class="requestor f-left font-400 margin-left-10">Chino Cruz</p>
												<div class="clear"></div>
											</div>
											<div class="width-45percent display-inline-mid">
												<p class="font-bold	f-left width-45percent margin-left-10">Reason for Withdrawal</p>
												<p class="request-for-withdrawal f-left width-50percent font-400">Canteen Construction</p>
												<div class="clear"></div>
											</div>
										</div>

										<div class="padding-all-15">
											<div class="width-50percent display-inline-mid">
												<p class="font-bold	f-left width-35percent margin-left-10">Designation</p>
												<p class="designation f-left font-400 margin-left-10">Supreme Commander of the Alliance</p>
												<div class="clear"></div>
											</div>											
										</div>								
										<div class="padding-all-15  bg-light-gray">
											<div class="width-50percent display-inline-mid">
												<p class="font-bold	f-left width-35percent margin-left-10">Department</p>
												<p class="department f-left font-400 margin-left-10">Housekeeping</p>
												<div class="clear"></div>
											</div>
											
										</div>																	
									</div>							
									<div class="clear"></div>									
								</div>
							</div>
						</div>
					</div>						
				</div>
				
				<div class="product-list">

						<div class="product-original-template border-top border-blue box-shadow-dark margin-bottom-20 bggray-white " style="display:none;">
							<div class="head panel-group text-left" >
								<div class="panel-heading font-14 font-400 margin-top-5 margin-bottom-5">
									<a class="colapsed black-color " href="#">								
										<h4 class="product-sku-name panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10 "> SKU #1234567890 - Holcim Cement</h4>
									</a>					
									<div class=" f-right width-200px text-right">
										<button type="button" class="font-12 btn btn-default font-12 display-inline-mid red-color hide-cancel">Cancel</button>
										<button class="btn general-btn display-inline-mid min-width-100px withdraw-edit-btn">Edit</button>						
									</div>
									<div class="clear"></div>
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body padding-all-20 " >
										<div class="bggray-white border-full padding-all-20 height-210px box-shadow-dark font-0">
											<div class="product-image height-100percent display-inline-mid width-230px">
												<!-- <img src="../assets/images/corn.jpg" alt="" class="height-100percent"> -->
												<!-- <img src="../assets/images/holcim.jpg" alt="" class="height-100percent width-100percent"> -->
											</div>

											<!-- <div class="display-inline-top width-75percent padding-left-10 font-0">
												<div class="width-100percent ">
													
													<div class="padding-all-15 text-left bg-light-gray">
														<p class="f-left width-20percent font-bold">Description</p>
														<p class="product-description f-left">Tough Steel</p>
														<div class="clear"></div>
													</div>
													
													<div class="padding-all-15 text-left">
														<p class="f-left no-margin-all width-20percent font-bold">Transfer Method</p>
														<p class="product-transfer-method f-left">By bulk</p>
																
														<div class="clear"></div>
													</div>

													<div class="padding-all-15 text-left">
														<p class="f-left width-20percent font-bold">Unit of Measure: </p>
														<p class="product-unit-of-measure f-left">kg</p>
														
														<div class="clear"></div>
													</div>
													
												</div>
												
											</div> -->
											
											<!-- ================================================== -->
											<!-- ================PRODUCT DESCRIPTION=============== -->
											<!-- ================================================== -->
											<div class="display-inline-top width-75percent padding-left-10 font-0">
											
												<div class="padding-all-15 bg-light-gray">
													<p class="f-left width-150px font-bold">Description</p>
													<p class="product-description f-left">Ang Cement na matibay</p>
													<div class="clear"></div>
												</div>

												<div class="padding-all-15">
													<p class="f-left no-margin-all width-150px font-bold">Transfer Method</p>
													<p class="product-transfer-method f-left transfer-display-piece">By Piece</p>

													<div class="f-left transfer-hide-piece" >
														<div class="f-left ">										
															<input type="radio" class="display-inline-mid width-20px default-cursor radio-bulk" name="bag-or-bulk">
															<label for="bulk" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>
														</div>

														<div class="f-left  margin-left-10">																	
															<input type="radio" class="display-inline-mid width-20px default-cursor radio-piece" name="bag-or-bulk">
															<label for ="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Piece</label>
														</div>

														<div class="clear"></div>
													</div>

													<div class="clear"></div>
												</div>
												
												<div class="padding-all-15 bg-light-gray">
													<p class="f-left width-150px font-bold">Unit of measure</p>
													<p class="product-unit-of-measure f-left transfer-display-piece">Test</p>

													<div class="f-left transfer-hide-piece">
														<div class="select display-inline-mid small width-105px height-26 unit-remove-dropdown">
															<div class="frm-custom-dropdown">
																<div class="frm-custom-dropdown-txt">
																	<input class="dd-txt" type="text">
																</div>

																<div class="frm-custom-icon"></div>
																
																<div class="frm-custom-dropdown-option" style="display: none;">
										 								
																</div>
															</div>
															<select class="unit_of_measure_dropdown unit-of-measure-dropdown frm-custom-dropdown-origin" style="display: none;">
									 							
															</select>
															<!-- <select class="unit_of_measure_dropdown">
																
															</select> -->
														</div>
													</div>
													<div class="clear"></div>
												</div>

												
												<div class="padding-all-15">
													<p class="f-left width-150px font-bold">Total QTY</p>
													<p class="total-qty-to-transfer f-left width-180px transfer-display-piece ">100 KG (10Bags)</p>
													<div class="f-left transfer-hide-piece">
														<div class="f-left text-left font-0">											
															<p class="total-qty-to-transfer font-14 font-400 display-inline-mid padding-right-10">KG</p>
															<div class="clear"></div>												
														</div>
													</div>
													<div class="clear"></div>
												</div>
							
											</div>
											<!-- ================================================== -->
											<!-- =============END PRODUCT DESCRIPTION============== -->
											<!-- ================================================== -->


										</div>

										<div class="assignment-display ">
											<div class="padding-top-20">
												<div class="margin-bottom-10 margin-top-10">
													<p class="no-margin-all font-20 font-500 f-left  padding-left-10 padding-top-5">Item Location</p>
													<div class="f-right withdraw-batch">
														<button class="btn general-btn modal-trigger" modal-target="add-batch">Add a Batch</button>
													</div>
													<div class="clear"></div>
												</div>
												<table class="tbl-4c3h">
													<thead>
														<tr>
															<th class="width-25percent black-color no-padding-left">Batch Name</th>
															<th class="width-50percent no-padding-left">Storage Location</th>
															<th class="width-25percent black-color no-padding-left">Quantity</th>
														</tr>
													</thead>
												</table>

												<div class="product-batches-list transfer-prod-location font-0 bggray-white ">
													
													<!-- first - panel  -->
													<div class="batch-gray-template storage-assign-panel position-rel" style="display:none;">
														<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change">
															<div class="width-25percent display-inline-mid text-center">
																<p class="batch-withdrawal-number font-14 font-400">123468469</p>
															</div>
															<div class="withdraw-batch-breadcrumb width-50percent text-center display-inline-mid height-auto line-height-25">
																<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>													
															</div>
															<div class="width-25percent display-inline-mid text-center">
																<p class="withdraw-batch-qty font-14 font-400">50 KG</p>
															</div>
														</div>
														<div class="btn-hover-storage hover-btns" style="height: 100% !important;">
															<button class="btn btn-edit-batch general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>
															<button class="btn btn-remove-batch general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>
														</div>
													</div>

													<div class="batch-white-template storage-assign-panel position-rel" style="display:none;">
														<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content ">
															<div class="width-25percent display-inline-mid text-center">
																<p class="batch-withdrawal-number font-14 font-400">123468469</p>
															</div>
															<div class="withdraw-batch-breadcrumb width-50percent text-center display-inline-mid height-auto line-height-25">
																<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>

															</div>
															<div class="width-25percent display-inline-mid text-center">
																<p class="withdraw-batch-qty font-14 font-400">50 KG</p>
															</div>
														</div>
														<div class="hover-btns btn-hover-storage">
															<button class="btn btn-edit-batch general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>
															<button class="btn btn-remove-batch general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>
														</div>
													</div>

													
												</div>

												<div class="total-amount-storage margin-top-40">
													<p class="first-text margin-top-10">Total</p>
													<p class="total-withdrawal-qty second-text margin-right-75 margin-top-10">100 KG</p>
													<div class="clear"></div>
												</div>

											</div>
										</div>
										
										
									</div>
								</div>
							</div>
						</div>

				</div>
			

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-20 black-color padding-top-10"> Documents</h4>
							</a>
							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="upload-documents">Upload</button>
							</div>
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-10 document-list">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>

								<div class="table-content position-rel tbl-dark-color document-template" style="display:none;">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid padding-left-10">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid document-name">RandomDocument1</p>
										</div>
										<p class=" display-inline-mid document-date">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="javascript:void(0)" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30 view-document">View</button>
										</a>
										<a href="javascript:void(0)" class="display-inline-mid download-document">
											<button class="btn general-btn ">Download</button>
										</a>
										<a href="javascript:void(0)" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div>

								<div id="displayDocuments" class="table-content position-rel tbl-dark-color">
									<!-- <div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">TransferDocument </p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<button class="btn general-btn padding-left-30 padding-right-30">View File</button>
										<button class="btn general-btn padding-left-30 padding-right-30 margin-left-10">Remove File</button>	
									</div> -->
								</div>	

								<!-- <div class="table-content position-rel">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid padding-left-10">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">RandomDocument2</p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">View</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn">Download</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div>

								<div class="table-content position-rel tbl-dark-color">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid padding-left-10">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">RandomDocument3</p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">View</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn">Download</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div> -->
																
							</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400 ">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-22 black-color padding-top-10"> Notes</h4>
							</a>
							<div class="f-right">
								<button class="btn general-btn modal-trigger btn-add-note" modal-target="add-notes">Add Notes</button>
							</div>
							<div class="clear"></div>
						</div>
						
					
						
						<div class="panel-collapse collapse in">
							<div class="note-list panel-body padding-all-20">

								<!-- <div class="border-full padding-all-10 margin-left-18">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10 font-400">Show Less</a>
									<div class="clear"></div>
								</div> -->

								<div class="note-template border-full padding-all-10 margin-left-5 margin-top-10 notes-contain" style="display:none;">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="note-subject-display f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="note-date-time-display f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="note-message-display font-14 font-400 no-padding-left padding-all-10 show-notes-message"> </p>
									<a href="" class="f-right padding-all-10 font-400 show-notes-messge-length">Show More</a>
									<div class="clear"></div>
								</div>									
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--Start of Upload Document MODAL-->
		<div class="modal-container" cr8v-file-upload="modal" modal-id="upload-documents">
			<div class="modal-body small">
				<div class="modal-head">
					<h4 class="text-left">Upload Document</h4>
					<div class="modal-close close-me"></div>
				</div>
				<div class="modal-content text-left" style="position:relative;">
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all display-inline-mid width-125px">Document Name:</p>
						<input class="width-200px display-inline-mid document-name" type="text">
						<button class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 browse" type="button">Browse</button>
					</div>
				</div>
				<div class="modal-footer text-right">
					<button class="font-12 btn btn-default font-12 display-inline-mid close-me red-color modal-close" type="button">Cancel</button>
					<button class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 confirm-upload" type="button">Confirm</button>
				</div>
			</div>
		</div>
		<!--End of Upload Document MODAL-->

		<!--Start of Add Notes MODAL-->
		<div class="modal-container note-modal" modal-id="add-notes">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Add Notes</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
						<input type="text" class="note-subject width-380px display-inline-mid margin-left-10">
					</div>
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
						<textarea class="note-message width-380px margin-left-10 height-250px"></textarea>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="close-note font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="submit-note font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
				</div>
			</div>	
		</div>
		<!--End of Add Notes MODAL-->
			
		<!-- Start of Remove Batch  -->
		<div class="modal-container" modal-id="remove-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Remove Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all">Are you sure you want to remove Batch 1234567890</p>
						
					</div>					
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Remove</button>
				</div>
			</div>	
		</div>

			<!--Start of edit Batch MODAL-->
		<div class="modal-container" modal-id="edit-batch">
			<div class="modal-body margin-top-100 margin-bottom-100">				

				<div class="modal-head">
					<h4 class="text-left">Edit Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

					<!-- content -->
				<div class="modal-content text-left">
					<div class="text-left">
						
						<table class="width-100percent">
							<tbody>
								<tr>
									<td class="font-500 font-14 width-15percent">Item:</td>
									<td class="font-14 font-400">SKU No. 1234567890 - Japanese Corn</td>
								</tr>
								<tr>
									<td class="font-500 font-14 padding-top-15">Batch Name:</td>
									<td><div class="select large display-inline-mid padding-top-10">
											<select>
												<option value="op1">CNEKIEK12345</option>
											</select>
										</div>
										<div class="display-inline-mid width-50percent font-400 padding-top-20 margin-left-10">									
											<p class="display-inline-mid font-14 width-150px">Amount to Withdraw:</p>
											<p class="display-inline-mid font-14">90 KG</p>
										</div>
									</td>
								</tr>
								<tr>
									<td class="font-500 font-14">Storage Location:</td>
									<td><div class="select large display-inline-mid padding-tp-10">
											<select>
												<option value="loc1">Warehouse 1</option>
											</select>
										</div>
										<div class="display-inline-mid width-50percent font-400 padding-top-10 margin-left-10">
											<p class="display-inline-mid font-14 width-150px">Withdraw Balance:</p>
											<p class="display-inline-mid font-14">20 KG</p>
										</div>
									</td>								
								</tr>
							</tbody>
						</table>
					</div>
					<p class="font-14 font-500 no-margin-right display-inline-top width-15percent no-margin-left padding-left-5">Sub Location:</p>
					<div class="display-inline-top width-85per bggray-white box-shadow-dark">
						<table class="tbl-4c3h ">
							<thead>
								<tr>
									<th class="black-color width-60percent">Location Address</th>
									<th class="black-color width-20percent">Location Qty</th>
									<th class="black-color width-20percent no-padding-left">Amount</th>
								</tr>
							</thead>
						</table>
						<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
							<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
								<i class="fa fa-check font-22"></i>
							</div>
							<div class="width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="font-14 font-400 no-margin-all ">1000 KG</p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="t-small text-center super-width-100px" value="70">
							</div>
						</div>

						<div class="font-0 text-center location-check position-rel default-cursor">
							<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
								<i class="fa fa-check font-22 "></i>
							</div>
							<div class="width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="font-14 font-400 no-margin-all ">1000 KG</p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="t-small text-center super-width-100px" value="70">
							</div>
						</div>

						<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
							<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
								<i class="fa fa-check font-22 "></i>
							</div>
							<div class="width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="font-14 font-400 no-margin-all ">1000 KG</p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="t-small text-center super-width-100px" value="70">
							</div>
						</div>
					</div>

				
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
				</div>
			</div>	
		</div>
		<!--End of edit Batch MODAL-->
		
		<!--Start of Add Batch MODAL-->
		<div class="modal-container" modal-id="add-batch">
			<div class="modal-body  margin-top-100 margin-bottom-100">				

				<div class="modal-head">
					<h4 class="text-left">Add Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="text-left">
						
						<table class="width-100percent">
							<tbody>
								<tr>
									<td class="font-500 font-14 width-15percent">Item:</td>
									<td class="font-14 font-400">SKU No. 1234567890 - Japanese Corn</td>
								</tr>
								<tr>
									<td class="font-500 font-14 padding-top-15">Batch Name:</td>
									<td><div class="select large display-inline-mid padding-top-10">
											<select>
												<option value="op1">CNEKIEK12345</option>
											</select>
										</div>
										<div class="display-inline-mid width-50percent font-400 padding-top-20 margin-left-10">									
											<p class="display-inline-mid font-14 width-150px">Amount to Withdraw:</p>
											<p class="display-inline-mid font-14">90 KG</p>
										</div>
									</td>
								</tr>
								<tr>
									<td class="font-500 font-14">Storage Location:</td>
									<td><div class="select large display-inline-mid padding-tp-10">
											<select>
												<option value="loc1">Warehouse 1</option>
											</select>
										</div>
										<div class="display-inline-mid width-50percent font-400 padding-top-10 margin-left-10">
											<p class="display-inline-mid font-14 width-150px">Withdraw Balance:</p>
											<p class="display-inline-mid font-14">20 KG</p>
										</div>
									</td>								
								</tr>
							</tbody>
						</table>
					</div>
					<p class="font-14 font-500 no-margin-right display-inline-top width-15percent no-margin-left padding-left-5">Sub Location:</p>
					<div class="display-inline-top width-85per bggray-white box-shadow-dark">
						<table class="tbl-4c3h ">
							<thead>
								<tr>
									<th class="black-color width-60percent">Location Address</th>
									<th class="black-color width-20percent">Location Qty</th>
									<th class="black-color width-20percent no-padding-left">Amount</th>
								</tr>
							</thead>
						</table>
						<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
							<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
								<i class="fa fa-check font-22"></i>
							</div>
							<div class="width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="font-14 font-400 no-margin-all ">1000 KG</p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="t-small text-center super-width-100px" value="70">
							</div>
						</div>

						<div class="font-0 text-center location-check position-rel default-cursor">
							<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
								<i class="fa fa-check font-22 "></i>
							</div>
							<div class="width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="font-14 font-400 no-margin-all ">1000 KG</p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="t-small text-center super-width-100px" value="70">
							</div>
						</div>

						<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
							<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
								<i class="fa fa-check font-22 "></i>
							</div>
							<div class="width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
								<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="font-14 font-400 no-margin-all ">1000 KG</p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="t-small text-center super-width-100px" value="70">
							</div>
						</div>
					</div>

				
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
				</div>
			</div>	
		</div>
		<!--End of Add Batch MODAL-->
	
		<!-- Are you sure product details -->
		<div class="modal-container complete-product-modal" modal-id="are-you-sure">
			<div class="modal-body width-550px">				

				<div class="modal-head">
					<h4 class="text-left">Mark as Complete</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-all-10">
					<p class="font-14 font-400">Are you sure you want to mark this withdrawal record as complete?</p>
				</div>
				</div>
			
				<div class="modal-footer text-right">	
					<div class="f-right width-200px">
						<button type="button" class="btn-cancel-complete font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>					
						<button type="button" class="btn-confirm-complete font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 submit-btn">Confirm</button>									
					</div>
					<div class="clear"></div>
				</div>
			</div>	
		</div>

		<!-- start of complete product details  -->

		<div class="modal-container display-product-details completed-record-modal" modal-id="product-details">
			<div class="modal-body small ">				

				<div class="modal-head">
					<h4 class="text-left">Mark as Complete</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-all-10 bggray-7cace5">
					<p class="complete-message font-14 font-400 white-color">Withdrawal No. <span class='withdrawal_number'>1234567890</span> has been completed</p>
				</div>
				</div>
			
				<div class="modal-footer text-right">	
									
						<!-- <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Show Record</button> -->
					<!-- </a> -->
				</div>
			</div>	
		</div>


		<!-- ================================================== -->
		<!-- ===================BATCH MODAL==================== -->
		<!-- ================================================== -->
		<div class="modal-container batch-modal" modal-id="edit-batch">
			<div class="modal-body margin-top-100 margin-bottom-100">				

				<div class="modal-head">
					<h4 class="text-left">Edit Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<div class="modal-content text-left">
					<div class="text-left">
						
						<table class="width-100percent">
							<tbody>
								<tr>
									<td class="font-500 font-14 width-15percent">Item:</td>
									<td class="modal-product-sku-name font-14 font-400">SKU No. 1234567890 - Japanese Corn</td>
								</tr>
								<tr>
									<td class="font-500 font-14 padding-top-15">Batch Name:</td>
									<td><div class="select large display-inline-mid padding-top-10">
											<select class='modal-batch-dropdown'>
												
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td class="font-500 font-14">Storage Location:</td>
									<td class="modal-storage-name font-14">Storage name</td>				
								</tr>
							</tbody>
						</table>
					</div>
					<p class="font-14 font-500 no-margin-right width-15percent no-margin-left padding-left-5">Sub Location:</p>
					<div class=" width-100per bggray-white box-shadow-dark">
						
						<table class="tbl-4c3h ">
							<thead>
								<tr>
									<th class="black-color width-60percent">Location Address</th>
									<th class="black-color width-20percent">Quantity</th>
									<th class="black-color width-20percent no-padding-left">Amount</th>
								</tr>
							</thead>
						</table>

						<div class="modal-sub-location bg-light-gray font-0 text-center location-check position-rel default-cursor">
							<div class="modal-sub-location-template width-60percent display-inline-mid padding-all-20">
								<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Batch Location </p>
								<div class="clear"></div>
							</div>
							<div class="width-20percent display-inline-mid">
								<p class="batch-available-qty font-14 font-400 no-margin-all "></p>
							</div>
							<div class="width-20percent display-inline-mid">
								<input type="text" class="batch-withdraw-amount t-small text-center super-width-100px">
							</div>
						</div>

					</div>

				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid modal-close close-me red-color">Cancel</button>
					<button type="button" class="btn-submit-add-batch font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
				</div>
			</div>	
		</div>
		<!-- ================================================== -->
		<!-- =================END BATCH MODAL================== -->
		<!-- ================================================== -->


		<!-- ======================================== -->
		<!-- ==========GENERATE VIEW MODAL=========== -->
		<!-- ======================================== -->
		<div class="modal-container modal-generate-view" modal-id="generate-view">
		    <div class="modal-body small">
		        <div class="modal-head">
		            <h4 class="text-left">Message</h4>
		        </div>
		        <!-- content -->
		        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
		            <div class="padding-all-10 bggray-7cace5">
		                <p class="modal-message font-16 font-400 white-color">Generating view, please wait...</p>
		            </div>
		        </div>
		        <div class="modal-footer text-right" style="height: 50px;">
		        </div>
		    </div>
		</div>
		<!-- ======================================== -->
		<!-- =======END GENERATE VIEW MODAL========== -->
		<!-- ======================================== -->
