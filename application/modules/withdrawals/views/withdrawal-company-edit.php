
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="stock-withdrawal.php">Withdrawal - Company Goods</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color">Edit Record No. <span class="withdrawal-number">1234567890</span></li>
				</ul>
			</div>
			<div class="semi-main">

				<div class="padding-top-30 margin-bottom-30 text-right width-100percent">
					<a href="<?php echo BASEURL . '/withdrawals/withdrawal_company_ongoing';  ?>">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					</a>
					<!-- <a href="stock-withdrawal.php"> -->
						<button class="btn-submit-edit-record btn general-btn">Save Changes</button>
					<!-- </a> -->
				</div>
				<div class="clear"></div>


				<!-- ====================================== -->
				<!-- ==========WITHDRAWAL DETAILS========== -->
				<!-- ====================================== -->
				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0 input-div">
					<p class="font-20 font-bold black-color padding-bottom-20">Withdrawal No. <span class="withdrawal-number">1234567890</span></p>

					<div class="width-45percent display-inline-top">
						<div class="">
							<p class="font-14 font-bold display-inline-mid width-120">Item Requestor: </p>
							<input type="text" class="requestor display-inline-mid t-medium margin-left-20 edit-input">
						</div>	
						<div class="margin-top-20">
							<p class="font-14 font-bold display-inline-mid width-120">Designation: </p>
							<input type="text" class="designation display-inline-mid t-medium margin-left-20 edit-input">
						</div>	
						
						<div class="margin-top-20">
							<p class="font-14 font-bold display-inline-mid width-120">Department: </p>
							<input type="text" class="department display-inline-mid t-medium margin-left-20 edit-input">
						</div>	
															
					</div>

					<div class="width-50percent display-inline-top">
						<div class="">
							<p class="font-14 font-bold display-inline-top width-150 margin-top-5">Reason for Withdrawal: </p>
							<textarea class="reason-for-withdrawal display-inline-mid width-65per margin-left-20 edit-input"></textarea>
						</div>

						
					</div>
				</div>
				<!-- ====================================== -->
				<!-- ========END WITHDRAWAL DETAILS======== -->
				<!-- ====================================== -->


				<!-- ====================================== -->
				<!-- ============PRODUCT DIV=============== -->
				<!-- ====================================== -->
				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white padding-bottom-20 ">
					<div class="width-100percent text-left padding-all-30 no-padding-top no-padding-left margin-bottom-30 border-bottom-small border-gray">
						<p class="font-20 font-bold black-color padding-bottom-20 ">Item List</p>
						<p class="font-14 font-bold display-inline-mid padding-right-20">Item: <span class="red-color display-inline-top">*</span></p>
						<div class="select dispaly-inline-mid large">
							<select class="product-list-dropdown">
								<option value="">Select Item</option>
							</select>
						</div>
						<a href="#" class="display-inline-mid padding-left-20">
							<button class="btn btn-add-item general-btn padding-left-20 padding-right-20">Add Item</button>
						</a>
					</div>

					<div class="all-product-list">
							
						<div class="old-product-list">

							<div class="old-product-template border-full padding-all-20 margin-top-10" style="display:none;">
								<div class="bggray-white text-left">
									<div class="old-product-image height-100percent display-inline-mid width-10per">
										<img src="../assets/images/corn.jpg" alt="" class="height-100percent width-100percent">
									</div>

									<div class="display-inline-top width-90per padding-left-10">
										<div class="padding-top-15 padding-left-15 padding-bottom-15">
											<p class="old-product-sku-name font-16 font-bold f-left">SKU# 1234567890 - Steel Rebars</p>
											<div class="btn-remove-old-product f-right width-20px margin-left-10">
												<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
											</div>
											<div class="clear"></div>
										</div>																						
									</div>
								</div>					
							</div>

						</div>

						<div class="product-list">
								
							<div class="product-original-template border-full padding-all-20 margin-top-20" data-id="" style="display:none;">
								<div class="bggray-white height-190px">
									<div class="product-image height-100percent display-inline-mid width-230px">
										<img src="../assets/images/steel.jpg" alt="" class="height-100percent width-100percent">
									</div>

									<div class="display-inline-top width-75percent padding-left-10">
										<div class="padding-all-15 bg-light-gray">
											<p class="font-16 font-bold f-left product-sku-name">SKU# 1234567890 - Steel Bars</p>
											<div class="f-right width-20px margin-left-10 remove-product">
												<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
											</div>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 text-left">
											<p class="f-left width-20percent font-bold">Description</p>
											<p class="product-description f-left">Tough Steel</p>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 text-left bg-light-gray">
											<p class="f-left no-margin-all width-20percent font-bold">Transfer Method</p>
					
											<div class="f-left">										
												<input type="radio" class="display-inline-mid width-50px default-cursor radio-bulk" name="bag-or-bulk">
												<label for="bulk" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>
											</div>

											<div class="f-left margin-left-10">																	
												<input type="radio" class="display-inline-mid width-50px default-cursor radio-piece" name="bag-or-bulk">
												<label for ="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Piece</label>
											</div>								
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 text-left">
											<p class="f-left width-20percent font-bold">Unit of Measure: </p>
											<div class="select display-inline-mid small width-70px height-26 unit-remove-dropdown">
												<div class="frm-custom-dropdown">
													<div class="frm-custom-dropdown-txt">
														<input class="dd-txt" type="text">
													</div>

													<div class="frm-custom-icon"></div>
													
													<div class="frm-custom-dropdown-option" style="display: none;">
							 								
													</div>
												</div>
												<select class="unit-of-measure-dropdown frm-custom-dropdown-origin" style="display: none;">
						 							<!-- <option value="1">#000000001 Cobra</option> -->
													<!-- <option value="2">#000000002 Samurai</option> -->
												</select>
											</div>
											<div class="clear"></div>
										</div>
												
									</div>
								</div>

								<div class="padding-top-20">
									<div class="padding-top-20 padding-bottom-20">
										<p class="font-20 font-500 f-left">Item Location</p>
										<div class="f-right">
											<button class="btn btn-add-batch general-btn modal-trigger margin-top-30 disabled" modal-target="add-batch">Add Batch</button>
										</div>
										<div class="clear"></div>
									</div>

									<div class="product-batch-display">
										
										<div class="assignment-display ">
											
											<div class="padding-top-20">
												
												<table class="tbl-4c3h">
													<thead>
														<tr>
															<th class="width-25percent black-color no-padding-left">Batch Name</th>
															<th class="width-50percent no-padding-left">Storage Location</th>
															<th class="width-25percent black-color no-padding-left">Quantity</th>
														</tr>
													</thead>
												</table>

												<div class="product-batches-list font-0 bggray-white ">
													
													<!-- first - panel  -->
													<div class="storage-assign-panel position-rel batch-gray-template batch-template" style="display:none;">
														<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change">
															<div class="width-25percent display-inline-mid text-center">
																<p class="font-14 font-400 batch-withdrawal-number">123468469</p>
															</div>
															<div class="withdraw-batch-breadcrumb width-50percent text-center display-inline-mid height-auto line-height-25">
																<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>													
															</div>
															<div class="width-25percent display-inline-mid text-center">
																<p class="withdraw-batch-qty font-14 font-400">50 KG</p>
															</div>
														</div>
														<div class="btn-hover-storage display-block" style="height: 55px; display: none; opacity: 0;">
															<button class="btn btn-batch-edit general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>
															<button class="btn btn-batch-remove general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>
														</div>
													</div>

													<div class="storage-assign-panel position-rel batch-white-template batch-template" style="display:none;">
														<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content ">
															<div class="width-25percent display-inline-mid text-center">
																<p class="batch-withdrawal-number font-14 font-400">123468469</p>
															</div>
															<div class="withdraw-batch-breadcrumb width-50percent text-center display-inline-mid height-auto line-height-25">
																<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>

															</div>
															<div class="width-25percent display-inline-mid text-center">
																<p class="withdraw-batch-qty font-14 font-400">50 KG</p>
															</div>
														</div>
														<div class="btn-hover-storage display-block" style="height: 80px; display: none; opacity: 0;">
															<button class="btn-batch-edit btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>
															<button class="btn-batch-remove btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>
														</div>
													</div>

													
												</div>
												<div class="total-amount-storage margin-top-40">
													<p class="first-text margin-top-10">Total</p>
													<p class="total-withdrawal-qty second-text margin-right-75 margin-top-10"></p>
													<div class="clear"></div>
												</div>
											</div>
										</div>
										
										<!-- <div class="product-batch-template" style="display:none;">
											<p class="f-left font-14 font-bold padding-right-20">Batch Name:</p>
											<p class="batch-name f-left font-14 font-400">CNEKIEK123456</p>
											<p class="batch-quantity f-right font-14 font-bold padding-bottom-20">Quantity 50 KG</p>
											<div class="clear"></div>
											<table class="tbl-4c3h">
												<thead>
													<tr>
														<th class="width-50percent black-color">Origin</th>
														<th class="width-50percent">Destination</th>
													</tr>
												</thead>
											</table>
											<div class="batch-locations font-0 tbl-dark-color margin-bottom-20 position-rel">
												<div class="width-100percent ">
													<div class="batch-origin padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25">
														 
													</div>

													<div class="batch-destination padding-all-20 width-50percent text-center display-inline-mid height-auto line-height-25">
														 
													</div>
												</div>
												<div class="edit-hide">
													<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit Batch</button>
													<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10" >Remove Batch</button>
												</div>
											</div>
											
										</div> -->

									</div>
									
								</div>
							</div>

						</div>

					</div>

					

				</div>
				<!-- ====================================== -->
				<!-- ==========END PRODUCT DIV============= -->
				<!-- ====================================== -->


				<!-- ====================================== -->
				<!-- ============DOCUMENT DIV============== -->
				<!-- ====================================== -->
				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-20 black-color padding-top-10"> Documents</h4>
							</a>
							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="upload-documents">Upload</button>
							</div>
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-10 document-list">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>

								<div class="table-content position-rel tbl-dark-color document-template" style="display:none;">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid padding-left-10">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid document-name">RandomDocument1</p>
										</div>
										<p class=" display-inline-mid document-date">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="javascript:void(0)" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30 view-document">View</button>
										</a>
										<a href="javascript:void(0)" class="display-inline-mid download-document">
											<button class="btn general-btn ">Download</button>
										</a>
										<a href="javascript:void(0)" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div>

								<div id="displayDocuments" class="table-content position-rel tbl-dark-color">
									<!-- <div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">TransferDocument </p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<button class="btn general-btn padding-left-30 padding-right-30">View File</button>
										<button class="btn general-btn padding-left-30 padding-right-30 margin-left-10">Remove File</button>	
									</div> -->
								</div>	

								<!-- <div class="table-content position-rel">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid padding-left-10">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">RandomDocument2</p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">View</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn">Download</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div>

								<div class="table-content position-rel tbl-dark-color">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid padding-left-10">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">RandomDocument3</p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">View</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn">Download</button>
										</a>
										<a href="#" class="display-inline-mid">
											<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
										</a>
									</div>
								</div> -->
																
							</div>
						</div>
					</div>
				</div>
				<!-- ====================================== -->
				<!-- ==========END DOCUMENT DIV============ -->
				<!-- ====================================== -->


				<!-- ====================================== -->
				<!-- ==============NOTE DIV================ -->
				<!-- ====================================== -->
				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400 ">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-22 black-color padding-top-10"> Notes</h4>
							</a>
							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="add-notes">Add Notes</button>
							</div>
							<div class="clear"></div>
						</div>
						
					
						
						<div class="panel-collapse collapse in">
							<div class="note-list panel-body padding-all-20">

								<!-- <div class="border-full padding-all-10 margin-left-18">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10 font-400">Show Less</a>
									<div class="clear"></div>
								</div> -->

								<div class="note-template border-full padding-all-10 margin-left-5 margin-top-10 notes-contain" style="display:none;">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="note-subject-display f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="note-date-time-display f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="note-message-display font-14 font-400 no-padding-left padding-all-10 show-notes-message"></p>
									<a href="" class="f-right padding-all-10 font-400 show-notes-messge-length">Show More</a>
									<div class="clear"></div>
								</div>									
							</div>
						</div>
					</div>
				</div>
				<!-- ====================================== -->
				<!-- ============END NOTE DIV============== -->
				<!-- ====================================== -->
				
				<div class="width-100percent text-right border-top-small border-gray padding-top-20 margin-top-20">
					<a href="<?php echo BASEURL . '/withdrawals/withdrawal_company_ongoing';  ?>">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					</a>
					<!-- <a href="stock-withdrawal.php"> -->
						<button class="btn-submit-edit-record btn general-btn">Save Changes</button>
					<!-- </a> -->
				</div>
			

			<div class="modal-container batch-modal" modal-id="edit-batch">
				<div class="modal-body margin-top-100 margin-bottom-100">				

					<div class="modal-head">
						<h4 class="text-left">Edit Batch</h4>				
						<div class="modal-close close-me"></div>
					</div>

						<!-- content -->
					<div class="modal-content text-left">
						<div class="text-left">
							
							<table class="width-100percent">
								<tbody>
									<tr>
										<td class="font-500 font-14 width-15percent">Item:</td>
										<td class="modal-product-sku-name font-14 font-400">SKU No. 1234567890 - Japanese Corn</td>
									</tr>
									<tr>
										<td class="font-500 font-14 padding-top-15">Batch Name:</td>
										<td><div class="select large display-inline-mid padding-top-10">
												<select class='modal-batch-dropdown'>
													
												</select>
											</div>
											<!-- <div class="display-inline-mid width-50percent font-400 padding-top-20 margin-left-10">									
												<p class="display-inline-mid font-14 width-150px">Amount to Withdraw:</p>
												<p class="display-inline-mid font-14">90 KG</p>
											</div> -->
										</td>
									</tr>
									<tr>
										<td class="font-500 font-14">Storage Location:</td>
										<td class="modal-storage-name font-14">Storage name</td>
										<!-- <td><div class="select large display-inline-mid padding-tp-10">
												<select>
													<option value="loc1">Warehouse 1</option>
												</select>
											</div>
											<div class="display-inline-mid width-50percent font-400 padding-top-10 margin-left-10">
												<p class="display-inline-mid font-14 width-150px">Withdraw Balance:</p>
												<p class="display-inline-mid font-14">20 KG</p>
											</div>
										</td>		 -->						
									</tr>
									<!-- <tr>
										<td class="font-500 font-14">Withdraw Balance:</td>
										<td class="font-14">2000 MT</td>
									</tr> -->
								</tbody>
							</table>
						</div>
						<p class="font-14 font-500 no-margin-right width-15percent no-margin-left padding-left-5">Sub Location:</p>
						<div class=" width-100per bggray-white box-shadow-dark">
							<table class="tbl-4c3h ">
								<thead>
									<tr>
										<th class="black-color width-60percent">Location Address</th>
										<th class="black-color width-20percent">Quantity</th>
										<th class="black-color width-20percent no-padding-left">Amount</th>
									</tr>
								</thead>
							</table>
							<div class="modal-sub-location bg-light-gray font-0 text-center location-check position-rel default-cursor">
								<!-- <div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
									<i class="fa fa-check font-22"></i>
								</div> -->
								<div class="modal-sub-location-template width-60percent display-inline-mid padding-all-20">
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Batch Location </p>
									<div class="clear"></div>
								</div>
								<div class="width-20percent display-inline-mid">
									<p class="batch-available-qty font-14 font-400 no-margin-all "></p>
								</div>
								<div class="width-20percent display-inline-mid">
									<input type="text" class="batch-withdraw-amount t-small text-center super-width-100px">
								</div>
							</div>

							<!-- <div class="font-0 text-center location-check position-rel default-cursor">
								<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
									<i class="fa fa-check font-22 "></i>
								</div>
								<div class="width-60percent display-inline-mid padding-all-20">
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
									<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
									<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
									<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
									<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
									<div class="clear"></div>
								</div>
								<div class="width-20percent display-inline-mid">
									<p class="font-14 font-400 no-margin-all ">1000 KG</p>
								</div>
								<div class="width-20percent display-inline-mid">
									<input type="text" class="t-small text-center super-width-100px" value="70">
								</div>
							</div>

							<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
								<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
									<i class="fa fa-check font-22 "></i>
								</div>
								<div class="width-60percent display-inline-mid padding-all-20">
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
									<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
									<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
									<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
									<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
									<div class="clear"></div>
								</div>
								<div class="width-20percent display-inline-mid">
									<p class="font-14 font-400 no-margin-all ">1000 KG</p>
								</div>
								<div class="width-20percent display-inline-mid">
									<input type="text" class="t-small text-center super-width-100px" value="70">
								</div>
							</div> -->
						</div>

					
					</div>
				
					<div class="modal-footer text-right">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid modal-close close-me red-color">Cancel</button>
						<button type="button" class="btn-submit-add-batch font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
					</div>
				</div>	
			</div>


			<!-- ====================================== -->
			<!-- ===========START NOTE MODAL=========== -->
			<!-- ====================================== -->
			<div class="modal-container note-modal" modal-id="add-notes">
				<div class="modal-body small">				

					<div class="modal-head">
						<h4 class="text-left">Add Notes</h4>				
						<div class="modal-close close-me"></div>
					</div>

					<!-- content -->
					<div class="modal-content text-left">
						<div class="text-left padding-bottom-10">
							<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
							<input type="text" class="note-subject width-380px display-inline-mid margin-left-10">
						</div>
						<div class="text-left padding-bottom-10">
							<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
							<textarea class="note-message width-380px margin-left-10 height-250px"></textarea>
						</div>
					</div>
				
					<div class="modal-footer text-right">
						<button type="button" class="close-note font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<button type="button" class="submit-note font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
					</div>
				</div>	
			</div>
			<!-- ====================================== -->
			<!-- =====-======END NOTE MODAL============ -->
			<!-- ====================================== -->


			<!-- ====================================== -->
			<!-- ==========CONFIRM EDIT MODAL========== -->
			<!-- ====================================== -->
			<div class="modal-container confirm-edit-record" modal-id="confirm-create-record">
				<div class="modal-body small">				

					<div class="modal-head">
						<h4 class="text-left">Edit Record</h4>				
						<div class="modal-close close-me btn-cancel-edit-record"></div>
					</div>

					<!-- content -->
					<div class="modal-content text-left">
						<p class="font-14 font-400">Are you sure you want to edit Withdrawal No. <span class="withdrawal-number-display">1234567890</span>?</p>
					</div>
				
					<div class="modal-footer text-right">
						<button type="button" class="btn-cancel-edit-record font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<button type="button" class="btn-confirm-edit-record font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="created-record">Confirm</button>
					</div>
				</div>	
			</div>
			<!-- ====================================== -->
			<!-- =======END CONFIRM EDIT MODAL========= -->
			<!-- ====================================== -->


			<!-- ====================================== -->
			<!-- ========RECORD UPDATED MODAL========== -->
			<!-- ====================================== -->
			<div class="modal-container modal-updated-record" modal-id="created-record">
				<div class="modal-body small">				

					<div class="modal-head">
						<h4 class="text-left">Edit Record</h4>				
						<div class="modal-close close-me close-modal"></div>
					</div>

					<!-- content -->
					<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
						<div class="padding-all-10 bggray-7cace5">
							<p class="font-14 font-400 white-color">Withdrawal No. <span class="withdrawal-number-display">1234567890</span> has been updated. Redirecting...</p>
						</div>
					</div>
				
					<div class="modal-footer text-right">
						<!-- <a href="stock-withdrawal-ongoing.php"> -->
							<!-- <button type="button" class="btn-show-created-record font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" >Show Record</button> -->
						<!-- </a> -->
					</div>
				</div>	
			</div>
			<!-- ====================================== -->
			<!-- ======END RECORD UPDATED MODAL======== -->
			<!-- ====================================== -->


			<!-- ======================================== -->
			<!-- ==========GENERATE VIEW MODAL=========== -->
			<!-- ======================================== -->
			<div class="modal-container modal-generate-view" modal-id="generate-view">
			    <div class="modal-body small">
			        <div class="modal-head">
			            <h4 class="text-left">Message</h4>
			        </div>
			        <!-- content -->
			        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
			            <div class="padding-all-10 bggray-7cace5">
			                <p class="modal-message font-16 font-400 white-color">Generating view, please wait...</p>
			            </div>
			        </div>
			        <div class="modal-footer text-right" style="height: 50px;">
			        </div>
			    </div>
			</div>
			<!-- ======================================== -->
			<!-- =======END GENERATE VIEW MODAL========== -->
			<!-- ======================================== -->
				
		</div>
	</div>
