<div class="main">
	<div class="breadcrumbs no-margin-left padding-left-50">
		<ul>
			<li>
				<a href="<?php echo BASEURL; ?>withdrawals/consignee_record_list">Withdrawal - Consignee Goods</a>
			</li>
			<li>
				<span>&gt;</span>
			</li>
			<li class="black-color bread-show font-bold withdrawal-no">
			</li>
		</ul>
	</div>
	<div class="semi-main">
		<div class="f-right margin-bottom-10 width-300px text-right margin-bottom-20" id="buttonHolder">
		</div>
		<div class="clear"></div><!-- receiving accordion here  -->
		<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white consignee-details-main">
			<div class="panel-group">
				<div class="panel-heading font-14 font-400">
					<a class="colapsed black-color f-left" href="#">
						<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10"> Withdrawal Information  </h4>
					</a>
					<a href="<?php echo BASEURL; ?>withdrawals/edit_consignee_record">
						<button class="btn general-btn f-right width-100px edit-consignee-data">Edit</button>	
					</a>
					<div class="clear"></div>
				</div>
				<div class="panel-collapse collapse in">
					<div class="panel-body">
						<div class="display-inline-mid width-50per margin-top-10">
							<div class="text-left">
								<p class="f-left font-14 font-bold width-40percent margin-top-2">
									ATL No.:
								</p>
								<p class="f-left font-16 font-bold width-50percent atl-no">
									
								</p>
								<div class="clear"></div>
							</div>
							<div class="text-left padding-top-10">
								<p class="f-left font-14 font-bold width-40percent margin-top-2">
									Consignee:
								</p>
								<p class="f-left font-16 font-bold width-50percent consignee">
									
								</p>
								<div class="clear"></div>
							</div>
							<div class="text-left padding-top-10">
								<p class="f-left font-14 font-bold width-40percent margin-top-2">
									Origin:
								</p>
								<p class="f-left font-16 font-bold width-50percent origin">
									
								</p>
								<div class="clear"></div>
							</div>
						</div>
						<div class="display-inline-top width-50per margin-top-10">
							<div class="text-left">
								<p class="f-left font-14 font-bold width-40percent margin-top-2 margin-left-10">
									Mode of Delivery:
								</p>
								<p class="f-left font-16 font-bold width-50percent mode-of-delivery">
									
								</p>
								<div class="clear"></div>
							</div>
							<div class="text-left padding-top-10">
								<p class="f-left font-14 font-bold width-40percent margin-top-2 margin-left-10">
									Date and Time Issued:
								</p>
								<p class="f-left font-16 font-bold width-50percent date-time">
									
								</p>
								<div class="clear"></div>
							</div>
							<div class="text-left padding-top-10">
								<p class="f-left font-14 font-bold  width-40percent margin-top-2 margin-left-10">Status:</p>
								<p class="f-left font-16 font-bold width-50percent record-status"> </p>
								<div class="clear"></div>
							</div>
						</div>
						<div class="width-100per margin-top-30 padding-top-10 text-left border-top-light">
							<h4 class=" font-500 font-18 padding-top-10 padding-bottom-10">
								Delivery Information
							</h4>
							<div class="font-0">
								<div class="width-50per text-left f-left">
									<h4 class="font-16 font-500 gray-color margin-top-10">
										CDR details
									</h4>
									<div class="margin-top-10 gray-color">
										<div class="bg-light-gray padding-all-10">
											<p class="font-14 font-bold display-inline-mid width-50percent">
												CDR No.
											</p>
											<p class="font-14 font-400 display-inline-mid width-50percent cdr-no">
												Not Yet Assigned
											</p>
										</div>
										<div class="bggray-white padding-all-10">
											<p class="font-14 font-bold display-inline-mid width-50percent">
												TARE In
											</p>
											<p class="font-14 font-400 display-inline-mid width-50percent tare-in">
												Not Yet Assigned
											</p>
										</div>
										<div class="bg-light-gray padding-all-10">
											<p class="font-14 font-bold display-inline-mid width-50percent">
												TARE Out
											</p>
											<p class="font-14 font-400 display-inline-mid width-50percent tare-out">
												Not Yet Assigned
											</p>
										</div>
										<div class="bggray-white padding-all-10">
											<p class="font-14 font-bold display-inline-mid width-50percent">
												Gate Pass Ctrl No.
											</p>
											<p class="font-14 font-400 display-inline-mid width-50percent gate-pass-ctrl-no">
												Not Yet Assigned
											</p>
										</div>
										<div class="bg-light-gray padding-all-10">
											<p class="font-14 font-bold display-inline-mid width-50percent">
												Scale Ticket No.
											</p>
											<p class="font-14 font-400 display-inline-mid width-50percent scale-ticket-no">
												Not Yet Assigned
											</p>
										</div>
										<div class="bggray-white padding-all-10">
											<p class="font-14 font-bold display-inline-mid width-50percent">
												Seal No.
											</p>
											<p class="font-14 font-400 display-inline-mid width-50percent seal-no">
												Not Yet Assigned
											</p>
										</div>
									</div>
								</div>
								<div class="width-50per f-right">
									<h4 class="font-16 font-500 black-color margin-top-10 trucking-vessel-title">
										
									</h4>
									<div class="margin-top-10 trucking-vessel-details-container">
										<div class="bg-light-gray padding-all-10 template">
											<p class="font-14 font-bold display-inline-mid black-color width-50percent key">
												
											</p>
											<p class="font-14 font-400 display-inline-mid black-color width-50percent value">
												
											</p>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0 display-none consignee-details-clone">
			<div class="display-inline-mid width-50per margin-top-10">
				<div class="text-left">
					<p class="f-left font-14 font-bold width-30percent margin-top-2">ATL No.:</p>
					<p class="f-left font-16 font-bold width-50percent atl-no"> </p>
					<div class="clear"></div>
				</div>

				<div class="text-left padding-top-10">
					<p class="f-left font-14 font-bold width-30percent margin-top-2 ">Consignee:</p>
					<p class="f-left font-16 font-bold width-50percent consignee"> </p>
					<div class="clear"></div>
				</div>

				<div class="text-left padding-top-10">
					<p class="f-left font-14 font-bold width-30percent margin-top-2 origin">Origin:</p>
					<p class="f-left font-16 font-bold width-50percent origin"> </p>
					<div class="clear"></div>
				</div>					
			</div>

			<div class="display-inline-top width-50per margin-top-10">						
				<div class="text-left">
					<p class="f-left font-14 font-bold  width-40percent margin-top-2 margin-left-10">Mode of Delivery:</p>
					<p class="f-left font-16 font-bold width-50percent mode-of-delivery"> </p>
					<div class="clear"></div>
				</div>

				<div class="text-left padding-top-10">
					<p class="f-left font-14 font-bold  width-40percent margin-top-2 margin-left-10">Date and Time Issued:</p>
					<p class="f-left font-16 font-bold width-50percent date-time"> </p>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0 cdr-container display-none">
			<p class="font-20 font-bold black-color padding-bottom-20">CDR Details</p>
			
			<div class="width-50percent display-inline-top">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid width-150px">CDR No.:</p>
					<input type="text" class="display-inline-mid t-medium width-230px" id="cdrNumber" placeholder="CDR no.">
				</div>

				<div class="padding-bottom-20">
					<p class="font-14 font-bold display-inline-top width-150px">Gate Pass Ctrl No.: </p>
					<div class="display-inline-mid width-340">
						<input type="text" class="display-inline-mid t-medium width-230px" id="cdrGatePassCrtlno" placeholder="Gate pass control no.">
						<div class="padding-top-10">
							<input type="checkbox" class="no-margin-all display-inline-mid default-cursor" id="cdrCargoSwap">
							<label class="font-14 font-bold width-120 display-inline-mid margin-left-10 default-cursor" >Cargo Swap</label>
						</div>
						<div class="padding-top-10">
							<div class="select large">
								<select id="cdrSelectConsignee"></select>
							</div>
						</div>

					</div>						
				</div>
			</div>

			<div class="width-50percent display-inline-top">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-top width-150 margin-top-5">Draft Weight (IN):</p>
					<input type="text" class="display-inline-mid t-medium width-230px" id="cdrWeightIn" placeholder="Draft weight (in)">
				</div>	
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-top width-150 margin-top-5 ">Draft Weight (Out):</p>
					<input type="text" class="display-inline-mid t-medium width-230px" id="cdrWeightOut" placeholder="Draft weight (out)">
				</div>		
				<span id="sealContainer">
					<div class="padding-bottom-10 seal seal-template">
						<p class="font-14 font-bold display-inline-top width-150 margin-top-5 ">Seal No.:</p>
						<input type="text" class="display-inline-mid t-medium width-230px seal-number" placeholder="Seal No.">
					</div>
				</span>
				<div class="f-right  padding-right-100 margin-right-10">
					<a href="javascript:void(0)" id="cdrAddSeal" class="font-14 font-400 f-right">+ Add Seal No</a>
					<div class="clear"></div>
				</div>	
			</div>					
		</div>
		
		<span id="itemContainer">

			<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white item-template template">
				<div class="head panel-group text-left">
					<div class="panel-heading font-14 font-400 margin-top-5 margin-bottom-5">
						<a class="colapsed black-color" href="#">
							<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10 item-sku-name"> </h4>
						</a>				
						<div class=" f-right width-200px text-right">
							<button type="button" class="font-12 btn btn-default font-12 display-inline-mid red-color stock-cancel" style="display: none;">Cancel</button>
							<button class="btn general-btn display-inline-mid min-width-100px stock-edit-save-changes" style="display: none;">Save Changes</button>
							<button class="btn general-btn display-inline-mid min-width-100px stock-edit">Edit</button>						
						</div>
						<div class="clear"></div>
					</div>
					<div class="panel-collapse collapse">
						<div class="panel-body padding-all-20 ">
							<div class="bggray-white border-full padding-all-20 box-shadow-dark font-0" style="min-height:190px;">
								<div class="height-100percent display-inline-mid width-25percent ">
									<img src="" alt="" class="height-100percent item-image" style="width:190px;">
								</div>

								<div class="display-inline-top width-75percent padding-left-10 font-0">
									<div class="width-100percent ">
										
										<div class="padding-all-15 bg-light-gray">
											<p class="display-inline-mid no-margin-all width-20percent font-bold">Loading Method</p>
											<p class="display-inline-mid no-margin-all width-50percent font-400 font-14 bagsack loading-method " style=""> </p>
											 
											<div class="radio-bagsandbulks display-inline-mid width-25percent loading-method-radios" style="display: none;">
												<div class="display-inline-mid">										
													<input type="radio" class="display-inline-mid width-20px default-cursor bagbulk" value="bulk" >
													<label for="bulk" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>
												</div>

												<div class="display-inline-mid margin-left-10">																	
													<input type="radio" class="display-inline-mid width-20px default-cursor bagbulk" value="bag" >
													<label for="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bags</label>
												</div>
											</div>																	

										</div>

										<div class="container-bulkbags padding-all-15">
											<p class="display-inline-mid no-margin-all width-20percent font-bold">Unit Of Measure</p>
											<p class="display-inline-mid no-margin-all width-25percent font-400 unit-of-measure-label"> </p>
											<div class="select small select-uom" style="display:none;">
												<select class="unit-of-measure"></select>
											</div>
										</div>

										<div class="container-bulkbags padding-all-15 bg-light-gray">
											<p class="display-inline-mid no-margin-all width-20percent font-bold">Qty to Withdraw</p>
											<p class="display-inline-mid no-margin-all width-25percent font-400 qty-to-withdraw-label"></p>
											<p class="display-inline-mid no-margin-all width-25percent font-400 qty-to-withdraw-input-container" style="display:none;">
												<input type="text" class="t-small  display-inline-mid  qty-to-withdraw-input" />
											</p>
										</div>

										<div class="container-bulkbags padding-all-15">
											<p class="display-inline-mid no-margin-all width-20percent font-bold">Description</p>
											<p class="display-inline-mid width-70percent item-description"></p>
										</div>

																					
									</div>										
								</div>
							</div>

							<div class="assignment-display ">
								<div class="padding-top-20">
								
									<div class="margin-bottom-10 margin-top-10">
										<p class="no-margin-all font-20 font-500 f-left  padding-left-10 padding-top-5">Item Location</p>
										<div class="f-right withdraw-batch" style="display: none;">
											<button class="btn general-btn modal-trigger" modal-target="add-batch">Add a Batch</button>
										</div>
										<div class="clear"></div>
									</div>
									<table class="tbl-4c3h">
										<thead>
											<tr>
												<th class="width-25percent gray-color no-padding-left">Batch Name</th>
												<th class="width-50percent gray-color no-padding-left">Storage Location</th>
												<th class="width-25percent gray-color no-padding-left">Quantity</th>
											</tr>
										</thead>
									</table>

									<div class="transfer-prod-location font-0 bggray-white ">
										
										<!-- first - panel  -->
										<div class="storage-assign-panel position-rel not-yet-assigned template">
											<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content bg-light-gray">
												<div class="width-25percent display-inline-mid text-center">
													
												</div>
												<div class="width-50percent text-center display-inline-mid height-auto line-height-25">
													<p class="font-14 font-400 gray-color">Not Yet Assigned</p>										
												</div>
												<div class="width-25percent display-inline-mid text-center">
													
												</div>
											</div>
											<div class="btn-hover-storage " style="height: 47px; display: none; opacity: 0;">
												<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>
												<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>
											</div>
										</div>

										<div class="storage-assign-panel position-rel product-batch-item template">
											<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content bg-light-gray">
												<div class="width-25percent display-inline-mid text-center">
													<p class="font-14 font-400 black-color batch-name"></p>
												</div>
												<div class="width-50percent text-center display-inline-mid height-auto line-height-25 location">
																														
												</div>
												<div class="width-25percent display-inline-mid text-center">
													<p class="font-14 font-400 black-color quantity"></p>
												</div>
											</div>
											<div class="btn-hover-storage" style="height: 100%;">
												<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>
												<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>
											</div>
										</div>

	
									</div>
								</div>
							</div>
							
							
						</div>
					</div>
				</div>
			</div>


		</span>
		
		<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white min-height-61px loading-information-display">
			<div class="head panel-group text-left">
				<div class="panel-heading font-14 font-400">
					<a class="colapsed black-color f-left" href="#">
						<h4 class="panel-title  font-500 font-20 black-color padding-top-10 active"> Loading Information</h4>
					</a>
					<div class="f-right width-200px">
						<button type="button" class="font-12 btn btn-default font-12 f-left close-me red-color vessel-cancel-loading-info" style="display: none;">Cancel</button>
						<button class="btn general-btn f-right width-100px vessel-edit-loading-info display-none">Edit</button>
						<button class="btn general-btn f-right width-100px display-none loading-info-save" style="display: none;">Save</button>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="panel-collapse collapse in">
					<div class="panel-body padding-all-10 paddding gray-color loading-information-gray">
						<div class="padding-all-10 font-0 bg-light-gray position-rel ">

							<p class="font-14 font-bold display-inline-mid width-25percent loading-label">Loading Date/Time Start</p>
							<p class="font-14 font-400 display-inline-mid width-27percent not-yet-assigned loading-date-time-start">Not Yet Assigned</p>

							<div class="width-27percent display-inline-mid assigned-value with-width font-0 position-abs" style="display: none;">
								<div class="input-group width-50% italic f-left fixed-datepicker">
									<input class="form-control dp width-100px default-cursor" data-date-format="MM/DD/YYYY" type="text" id='loadingStartDate'>
									<span class="input-group-addon width-0 "><i class="fa fa-calendar"></i></span>
								</div>
								<div class="input-group width-50% italic f-right fixed-timepicker">
									<input class="form-control dp width-100px default-cursor time" type="text" id='loadingStartTime'>
									<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
								</div>
								<div class="clear"></div>
							</div>


							<p class="font-14 font-bold display-inline-mid width-23percent handling">Handling Method</p>
							<p class="font-14 font-400 display-inline-mid width-25percent not-yet-assigned handling-method-display">Not Yet Assigned</p>

							<div class="select assigned-value large" style="display: none;">
								<select id="loadingHandlingMethod">
									<option value="auto-bagging machine">Auto-Bagging Machine</option>
								</select>
							</div>
						</div>

						<div class="padding-all-10 font-0 bggray-white paddding">

							<p class="font-14 font-bold display-inline-mid width-25percent">Loading Date/Time End</p>
							<p class="font-14 font-400 display-inline-mid width-27percent loading-date-time-end">Not Yet Assigned</p>
							<p class="font-14 font-bold display-inline-mid width-23percent loading-area">Loading Area</p>
							<p class="font-14 font-400 display-inline-mid width-25percent not-yet-assigned loading-area-display">Not Yet Assigned</p>
							<div class="select medium assigned-value loading-area" style="display: none;">
								<select   id="loadingInfoLoadingArea">
								</select>
							</div>
						</div>

						<div class="padding-all-10 font-0 bg-light-gray">

							<p class="font-14 font-bold display-inline-mid width-25percent">Duration</p>
							<p class="font-14 font-400 display-inline-mid width-25percent duration-display">Not Yet Assigned</p>

						</div>

						<div class="font-0">
							<div class="width-50per f-left gray-color worker-list-display">
								<p class="no-margin-all padding-all-10 font-14 font-bold">Worker list</p>
								<div class="padding-all-10 font-0 bg-light-gray no-workers-logged">
									<p class="font-14 font-400 italic">No Workers Logged</p>
								</div>
								<div class="padding-all-10 font-0  worker-list-template template">
									<p class="font-14 font-400 black-color width-60percent display-inline-mid name"> </p>
									<p class="font-14 font-400 black-color width-40percent display-inline-mid designation"> </p>
								</div>
							</div>
							
							<div class="width-50per f-right gray-color equipment-display">
								<p class="no-margin-all padding-all-10 font-14 font-bold">Equidpment Used</p>
								<div class="padding-all-10 font-0 bg-light-gray no-equipment-used">
									<p class="font-14 font-400 italic">No Equipment Used</p>
								</div>
								<div class="padding-all-10 font-0  equipment-template template">
									<p class="font-14 font-400 black-color display-inline-mid name"> </p>
								</div>
							</div>


						</div>

						
														
					</div>
				</div>
			</div>
		</div>
			
		<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white min-height-61px loading-information-edit display-none">
			<div class="head panel-group text-left">
				<div class="panel-heading font-14 font-400">
					<a class="colapsed black-color" href="#">
						<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
						<h4 class="panel-title  font-500 font-20 black-color padding-top-10 active"> Loading Information</h4>
					</a>
				</div>

				<div class="panel-collapse collapse in">
					<div class="panel-body padding-all-10 text-left">
						<div class="padding-all-10 font-0 bg-light-gray">
							<p class="font-14 font-bold black-color display-inline-mid width-20percent">Est Loading Date/Time Start</p>
							<div class="input-group width-15percent italic display-inline-mid fixed-datepicker">
								<input class="form-control dp width-100px default-cursor" id="estDate" data-date-format="MM/DD/YYYY" type="text">
								<span class="input-group-addon width-0 "><i class="fa fa-calendar"></i></span>
							</div>
							<div class="input-group width-20percent italic display-inline-mid fixed-timepicker">
								<input class="form-control dp time width-100px default-cursor" id="estTime" type="text">
								<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
							</div>
							<p class="font-14 font-bold black-color display-inline-mid width-20percent">Handling Method</p>
							<div class="select large">
								<select id="handlingMethod">
									<option value="auto-bagging machine">Auto-Bagging Machine</option>
								</select>
							</div>
						</div>

						<div class="padding-all-10 font-0 bggray-white">
							<p class="font-14 font-bold black-color display-inline-mid width-20percent">Loading Area</p>
							<div class="select large">
								<select id="selectLoadingArea"></select>
							</div>

						</div>							
					</div>
				</div>
			</div>
		</div>

		<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
			<div class="head panel-group text-left">
				<div class="panel-heading font-14 font-400">
					<a class="colapsed black-color f-left" href="#">
						<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
						<h4 class="panel-title padding-top-10 font-500 font-22 black-color active"> Documents</h4>
					</a>

					<div class="f-right">
						<button class="btn general-btn modal-trigger" id="btnUploadDocs">Upload a Document</button>
					</div>
					<div class="clear"></div>

				</div>
				<div class="panel-collapse collapse in">
					<div class="panel-body padding-all-20">
						<table class="tbl-4c3h">
							<thead>
								<tr>
									<th class="black-color">Document Name</th>
									<th class="black-color">Date</th>
								</tr>
							</thead>
						</table>

						<span id="displayDocuments"></span>

					</div>
				</div>
			</div>
		</div>
		
		<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
			<div class="head panel-group text-left">
				<div class="panel-heading font-14 font-400 ">
					<a class="colapsed black-color f-left" href="#">
						<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
						<h4 class="panel-title  font-500 font-22 black-color padding-top-10 active"> Notes</h4>
					</a>
					<div class="f-right">
						<button class="btn general-btn modal-trigger" id="btnAddNotes">Add Notes</button>
					</div>
					<div class="clear"></div>
				</div>
				
			
				
				<div class="panel-collapse collapse in">
					<div class="panel-body padding-all-20" id="noteContainer">					
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<!--Start of Upload Document MODAL-->
<div class="modal-container" modal-id="upload-documents">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">
				Upload Document
			</h4>
			<div class="modal-close close-me"></div>
		</div><!-- content -->
		<div class="modal-content text-left">
			<div class="text-left padding-bottom-10">
				<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">
					Dcoument Name:
				</p>
				<input type="text" class="width-231px display-inline-mid">
			</div>
			<div class="text-left padding-bottom-10">
				<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">
					File Location:
				</p>
				<input type="text" class="width-231px display-inline-mid"> <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Browse</button>
			</div>
		</div>
		<div class="modal-footer text-right">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button> <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
		</div>
	</div>
</div>
<!--End of Upload Document MODAL-->

<!--Start of Add Notes MODAL-->
<div class="modal-container" modal-id="add-note">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">
				Add Notes
			</h4>
			<div class="modal-close close-me"></div>
		</div><!-- content -->
		<div class="modal-content text-left">
			<div class="text-left padding-bottom-10">
				<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">
					Subject:
				</p><input type="text" class="width-380px display-inline-mid margin-left-10">
			</div>
			<div class="text-left padding-bottom-10">
				<p class="font-14 font-400 no-margin-all display-inline-top width-60px">
					Message:
				</p>
				<textarea class="width-380px margin-left-10 height-250px"></textarea>
			</div>
		</div>
		<div class="modal-footer text-right">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button> <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
		</div>
	</div>
</div>
<!--End of Add Notes MODAL-->

<!-- Start of Remove Batch  -->
<div class="modal-container" modal-id="remove-batch">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">
				Remove Batch
			</h4>
			<div class="modal-close close-me"></div>
		</div><!-- content -->
		<div class="modal-content text-left">
			<div class="text-left padding-bottom-10">
				<p class="font-14 font-400 no-margin-all message">
				</p>
			</div>
		</div>
		<div class="modal-footer text-right">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 confirm">Remove</button>
		</div>
	</div>
</div>

<!--Start of edit Batch MODAL-->
<div class="modal-container" modal-id="edit-batch">
	<div class="modal-body margin-top-100 margin-bottom-100">
		<div class="modal-head">
			<h4 class="text-left">
				Edit Batch
			</h4>
			<div class="modal-close close-me"></div>
		</div><!-- content -->
		<div class="modal-content text-left">
			<div class="text-left">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td class="font-500 font-14 width-15percent">
								Item:
							</td>
							<td class="font-14 font-400">
								SKU No. 1234567890 - Japanese Corn
							</td>
						</tr>
						<tr>
							<td class="font-500 font-14 padding-top-15">
								Batch Name:
							</td>
							<td>
								<div class="select large display-inline-mid padding-top-10">
									<select class="transform-dd">
										<option value="op1">
											CNEKIEK12345
										</option>
									</select>
								</div>
								<div class="display-inline-mid width-50percent font-400 padding-top-20 margin-left-10">
									<p class="display-inline-mid font-14 width-150px">
										Amount to Withdraw:
									</p>
									<p class="display-inline-mid font-14">
										90 KG
									</p>
								</div>
							</td>
						</tr>
						<tr>
							<td class="font-500 font-14">
								Storage Location:
							</td>
							<td>
								<div class="select large display-inline-mid padding-tp-10">
									<select class="transform-dd">
										<option value="loc1">
											Warehouse 1
										</option>
									</select>
								</div>
								<div class="display-inline-mid width-50percent font-400 padding-top-10 margin-left-10">
									<p class="display-inline-mid font-14 width-150px">
										Withdraw Balance:
									</p>
									<p class="display-inline-mid font-14">
										20 KG
									</p>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<p class="font-14 font-500 no-margin-right display-inline-top width-15percent no-margin-left padding-left-5">
				Sub Location:
			</p>
			<div class="display-inline-top width-85per bggray-white box-shadow-dark">
				<table class="tbl-4c3h">
					<thead>
						<tr>
							<th class="black-color width-60percent">
								Location Address
							</th>
							<th class="black-color width-20percent">
								Location Qty
							</th>
							<th class="black-color width-20percent no-padding-left">
								Amount
							</th>
						</tr>
					</thead>
				</table>
				<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
					<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15"></div>
					<div class="width-60percent display-inline-mid padding-all-20">
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Area 1
						</p><i class="display-inline-mid font-14 padding-right-10">&gt;</i>
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Storage 1
						</p><i class="display-inline-mid font-14 padding-right-10">&gt;</i>
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Shelf
						</p><i class="display-inline-mid font-14 padding-right-10">&gt;</i>
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Rack 1
						</p><i class="display-inline-mid font-14 padding-right-10">&gt;</i>
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Pallete 1
						</p>
						<div class="clear"></div>
					</div>
					<div class="width-20percent display-inline-mid">
						<p class="font-14 font-400 no-margin-all">
							1000 KG
						</p>
					</div>
					<div class="width-20percent display-inline-mid">
						<input type="text" class="t-small text-center super-width-100px" value="70">
					</div>
				</div>
				<div class="font-0 text-center location-check position-rel default-cursor">
					<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15"></div>
					<div class="width-60percent display-inline-mid padding-all-20">
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Area 1
						</p><i class="display-inline-mid font-14 padding-right-10">&gt;</i>
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Storage 1
						</p><i class="display-inline-mid font-14 padding-right-10">&gt;</i>
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Shelf
						</p><i class="display-inline-mid font-14 padding-right-10">&gt;</i>
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Rack 1
						</p><i class="display-inline-mid font-14 padding-right-10">&gt;</i>
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Pallete 1
						</p>
						<div class="clear"></div>
					</div>
					<div class="width-20percent display-inline-mid">
						<p class="font-14 font-400 no-margin-all">
							1000 KG
						</p>
					</div>
					<div class="width-20percent display-inline-mid">
						<input type="text" class="t-small text-center super-width-100px" value="70">
					</div>
				</div>
				<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
					<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15"></div>
					<div class="width-60percent display-inline-mid padding-all-20">
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Area 1
						</p><i class="display-inline-mid font-14 padding-right-10">&gt;</i>
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Storage 1
						</p><i class="display-inline-mid font-14 padding-right-10">&gt;</i>
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Shelf
						</p><i class="display-inline-mid font-14 padding-right-10">&gt;</i>
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Rack 1
						</p><i class="display-inline-mid font-14 padding-right-10">&gt;</i>
						<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10">
							Pallete 1
						</p>
						<div class="clear"></div>
					</div>
					<div class="width-20percent display-inline-mid">
						<p class="font-14 font-400 no-margin-all">
							1000 KG
						</p>
					</div>
					<div class="width-20percent display-inline-mid">
						<input type="text" class="t-small text-center super-width-100px" value="70">
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer text-right">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button> <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
		</div>
	</div>
</div>
<!--End of edit Batch MODAL-->


<!-- Are you sure product details -->
<div class="modal-container complete-product-modal" modal-id="are-you-sure">
	<div class="modal-body width-550px">
		<div class="modal-head">
			<h4 class="text-left">
				Mark as Complete
			</h4>
			<div class="modal-close close-me"></div>
		</div><!-- content -->
		<div class="modal-content text-left">
			<div class="padding-all-10">
				<p class="font-14 font-400">
					Are you sure you want to mark this withdrawal record as complete?
				</p>
			</div>
		</div>
		<div class="modal-footer text-right">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button> <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 submit-btn">Confirm</button>
		</div>
	</div>
</div>

<!-- start of complete product details  -->
<div class="modal-container display-product-details" modal-id="product-details">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">
				Mark as Complete
			</h4>
			<div class="modal-close close-me"></div>
		</div><!-- content -->
		<div class="modal-content text-left">
			<div class="padding-all-10 bggray-7cace5">
				<p class="font-14 font-400 white-color">
					Withdrawal No. 1234567890 has been completed
				</p>
			</div>
		</div>
		<div class="modal-footer text-right">
			<a href="stock-withdrawal-complete.php"><button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20"><a href="stock-withdrawal-complete.php">Show Record</a></button></a>
		</div>
	</div>
</div>

<div class="modal-container" modal-id="create-ATL">
	<div class="modal-body large" style="width: 790px !important;">				

		<div class="modal-head">
			<h4 class="text-left to-hide-print">Create ATL</h4>				
			<div class="modal-close close-me"></div>
		</div>
	
		<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent  "  >
			<p class="font-14 font-400 black-color no-margin-all to-hide-print">Please confirm if the following ATL details are correct:</p>
			<div class="bggray-white padding-all-10 margin-top-20 printable" style="width:750px !important;">
 
				<table style="width: 700px;">
					<tbody>
						<tr>
							<td style="text-align:center; font-size:18px; font-weight:bold; font-family: Arial;">
								AUTHORITY TO LOAD
							</td>
						</tr>
					</tbody>
				</table>
				<table style="width: 700px;">
					<tbody>
						<tr>
							<td style="text-align:right; font-family: Arial;" class="atl-number">
								
							</td>
						</tr>
					</tbody>
				</table>
				<table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px;width: 120px;">
								Deliver to/Consignee:
							</td>
							<td style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 180px; text-align:center;" class="consignee-name">
								
							</td>
							<td style="padding: 0px; min-width: 0px; width: 15px;" >
								Date:
							</td>
							<td style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 130px; text-align:center;" class="date">
								
							</td>
							<td style="padding: 0px; min-width: 0px; width: 45px;">
								Time-in:
							</td>
							<td style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 70px; text-align:center;" class="time-in">
								
							</td>
							<td style="padding: 0px; min-width: 0px; width: 52px;">
								Time-out:
							</td>
							<td style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 70px; text-align:center;" class="time-out">
								
							</td>
						</tr>
					</tbody>
				</table>
				<table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px;width: 40px;">
								Vessel:
							</td>
							<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 230px; text-align:center;" class="vessel-name">
								
							</td>
							<td style="padding: 0px;min-width: 0px;width: 66px;">
								Commodity:
							</td>
							<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 230px; text-align:center;" class="commodity">
								
							</td>
							<td style="padding: 0px; min-width: 0px; width: 45px;">
								Balance:
							</td>
							<td style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 75px; text-align:center;" class="balance">
								
							</td>
						</tr>
					</tbody>
				</table>
				<table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px;width: 105px;">
								No. of Bags / Bulk:
							</td>
							<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 200px; text-align:center;" class="no-bags-bulk">
								
							</td>
							<td style="padding: 0px;min-width: 0px;width: 80px;">
								Accomodation:
							</td>
							<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 180px; text-align:center;" class="accomodation">
								
							</td>
							<td style="padding: 0px;min-width: 0px;width: 70px;">
								Aging Days:
							</td>
							<td style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 60px; text-align:center;" class="aging-days">
								
							</td>
						</tr>
					</tbody>
				</table>
				<table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px; width: 100px;">
								Trucking / Barging:
							</td>
							<td class="trucking-barging" style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 200px; text-align:center;" >
								
							</td>
							<td style="padding: 0px;min-width: 0px; width: 160px;">
								Truck Plate No. / Barge Name:
							</td>
							<td class="truck-plate-no-barge-name" style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 200px; text-align:center;" >
								
							</td>
						</tr>
					</tbody>
				</table>
				<table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px; width: 100px;">
								Truck / Barge Patron:
							</td>
							<td class="trucking-barging-patron" style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 200px; text-align:center;" >
								
							</td>
							<td style="padding: 0px;min-width: 0px; width: 40px;">
								License:
							</td>
							<td class="license" style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 250px; text-align:center;" >
								
							</td>
						</tr>
					</tbody>
				</table>

				<div style="height:30px;"></div>

				<div style="width:700px; border:1px solid #999; padding:10px;">
					<table style="font-size: 12px; font-family: Arial; width: 660px;">
						<tbody>
							<tr>
								<td style="text-align:center; font-size:15px; font-weight:bold;">
									Loading Details
								</td>
							</tr>
						</tbody>
					</table>
					<table style="font-size: 12px;font-family: Arial;width: 660px;margin-top: 20px;">
						<tbody style="">
							<tr style="">
								<td style="padding: 0px;min-width: 0px;width: 80px;">
									Storage Area:
								</td>
								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 130px; text-align:center;" class="storage-area">
									
								</td>
								<td style="padding: 0px;min-width: 0px;width: 80px;">
									Loading Type:
								</td>
								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" class="loading-type">
									
								</td>
								<td style="padding: 0px;min-width: 0px;width: 170px;">
									Est. Loading Date and Time:
								</td>
								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" class="est-loading-date-time">
									
								</td>
							</tr>
						</tbody>
					</table>
					<table style="font-size: 12px;font-family: Arial;width: 660px;margin-top: 20px;">
						<tbody style="">
							<tr style="">
								<td style="padding: 0px;min-width: 0px;width: 60px;">
									Loading Start:
								</td>
								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" class="loading-start">
									
								</td>
								<td style="padding: 0px;min-width: 0px;width: 60px;">
									Loading End:
								</td>
								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" class="loading-end">
									
								</td>
								<td style="padding: 0px;min-width: 0px;width: 80px;">
									Equipment Used:
								</td>
								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" class="equipment-used">
									
								</td>
							</tr>
						</tbody>
					</table>
					<table style="font-size: 12px;font-family: Arial;width: 660px;margin-top: 20px;">
						<tbody style="">
							<tr style="">
								<td style="padding: 0px;min-width: 0px;width: 70px;">
									Verified by:
								</td>
								
								<td style="width:10px;"> </td>

								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" >
									
								</td>

								<td style="width:50px;"> </td>

								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" >
									
								</td>

								<td style="width:100px;"> </td>
							</tr>
						</tbody>
					</table>
					<table style="font-size: 12px;font-family: Arial;width: 660px;">
						<tbody style="">
							<tr style="">
								<td style="padding: 0px;min-width: 0px;width: 70px;">

								</td>
								
								<td style="width:10px;"> </td>

								<td style="padding: 0px;min-width: 0px;width: 100px; text-align:center; " >
									Cargo Checker
								</td>

								<td style="width:50px;"> </td>

								<td style=" padding: 0px;min-width: 0px;width: 100px; text-align:center;" >
									Equipment Operator
								</td>

								<td style="width:100px;"> </td>
							</tr>
						</tbody>
					</table>
				</div>

				<div style="height:40px;"> </div>

				<table style="font-size: 12px;font-family: Arial;width: 660px;margin-top: 20px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px;width: 70px;">
								Issued by:
							</td>
							
							<td style="width:10px;"> </td>

							<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px;" >
								
							</td>

							<td style="width:50px;"> </td>

							<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px;" >
								
							</td>

							<td style="width:100px;"> </td>
						</tr>
					</tbody>
				</table>

				<table style="font-size: 12px;font-family: Arial;width: 660px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px;width: 70px;">

							</td>
							
							<td style="width:10px;"> </td>

							<td style="padding: 0px;min-width: 0px;width: 100px; text-align:center;" >
								Posting Clerk
							</td>

							<td style="width:50px;"> </td>

							<td style=" padding: 0px;min-width: 0px;width: 100px; text-align:center;" >
								Operations Supervisor
							</td>

							<td style="width:100px;"> </td>
						</tr>
					</tbody>
				</table>

			</div>
		</div>
	
		<div class="modal-footer text-right to-hide-print">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 confirm-atl">Confirm</button>
		</div>
	</div>	
</div>


<div class="modal-container" modal-id="generate-view">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>

<div class="modal-container " modal-id="add-notes">
	<div class="modal-body small">				

		<div class="modal-head">
			<h4 class="text-left">Add Notes</h4>				
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content text-left">
			<div class="text-left padding-bottom-10">
				<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
				<input type="text" class="width-380px display-inline-mid" id="noteSubject">
			</div>
			<div class="text-left padding-bottom-10">
				<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
				<textarea class="width-380px margin-left-10 height-250px" id="noteMessage"></textarea>
			</div>
		</div>
	
		<div class="modal-footer text-right">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" id="confirmNote">Confirm</button>
		</div>
	</div>	
</div>


<div class="modal-container" modal-id="add-batch">
	<div class="modal-body  margin-top-100 margin-bottom-100">				

		<div class="modal-head">
			<h4 class="text-left">Add Batch</h4>				
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content text-left">
			<div class="text-left">
				
				<table class="width-100percent">
					<tbody>
						<tr>
							<td class="font-500 font-14 width-15percent">Item:</td>
							<td class="font-14 font-400 sku-name"></td>
						</tr>
						<tr>
							<td class="font-500 font-14 padding-top-15">Batch Name:</td>
							<td>
								<div class="select large display-inline-mid padding-top-10">
									<select id="selectBatch"></select>
								</div>
								<div class="display-inline-mid width-50percent font-400 padding-top-20 margin-left-10">									
									<p class="display-inline-mid font-14 width-150px">Amount to Withdraw:</p>
									<p class="display-inline-mid font-14 amount-to-withdraw"> </p>
								</div>
							</td>
						</tr>
						<tr>
							<td class="font-500 font-14">Storage Location:</td>
							<td>
								<div class="select large display-inline-mid padding-tp-10">
									<select id="selectLocation"></select>
								</div>
								<div class="display-inline-mid width-50percent font-400 padding-top-10 margin-left-10">
									<p class="display-inline-mid font-14 width-150px">Withdraw Balance:</p>
									<p class="display-inline-mid font-14 withdrawal-balance"> </p>
								</div>
							</td>								
						</tr>
					</tbody>
				</table>
			</div>
			<p class="font-14 font-500 no-margin-right display-inline-top width-15percent no-margin-left padding-left-5">Sub Location:</p>
			<div class="display-inline-top width-85per bggray-white box-shadow-dark">
				<table class="tbl-4c3h ">
					<thead>
						<tr>
							<th class="black-color width-60percent">Location Address</th>
							<th class="black-color width-20percent">Location Qty</th>
							<th class="black-color width-20percent no-padding-left">Amount</th>
						</tr>
					</thead>
				</table>
				<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
					<div class="width-60percent display-inline-mid padding-all-20 location-container">

					</div>
					<div class="width-20percent display-inline-mid">
						<p class="font-14 font-400 no-margin-all qty-container"> </p>
					</div>
					<div class="width-20percent display-inline-mid">
						<input type="text" class="t-small text-center super-width-100px withdraw-qty" />
					</div>
				</div>

			</div>

		
		</div>
	
		<div class="modal-footer text-right">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 confirm">Confirm</button>
		</div>
	</div>	
</div>

<div class="modal-container modal-transfer" modal-id="complete-loading">
	<div class="modal-body xlarge">				

		<div class="modal-head">
			<h4 class="text-left">Complete Loading</h4>				
			<div class="modal-close close-me transfer-close"></div>
		</div>
		<!-- content -->
		<div class="modal-content text-left">
			<div class="padding-bottom-10">
				<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time Start :</p>
				
				<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
					<input class="form-control dp  default-cursor " placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text" id="transferDateStart">
					<span class="input-group-addon width-0"><i class="fa fa-calendar"></i></span>
				</div>
				
				<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
					<input class="form-control dp time " placeholder="Transfer Time" type="text" id="transferTimeStart">
					<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
				</div>
			
			</div>

			<div class="padding-bottom-10">
				<p class="font-14 font-bold display-inline-mid no-margin-all">Transfer Date / Time End :</p>
				
				<div class="input-group width-200px italic display-inline-mid fixed-datepicker margin-left-10">
					<input class="form-control dp border-big-bl border-big-tl default-cursor" placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text" id="transferDateEnd">
					<span class="input-group-addon width-0"><i class="fa fa-calendar "></i></span>
				</div>

				<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
					<input class="form-control dp time" placeholder="Transfer Time" type="text" id="transferTimeEnd">
					<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
				</div>
			</div>

			<div class="width-100percent">
				<div class="width-50per f-left">
					<div class="padding-bottom-10 padding-top-10">
						<p class="font-14 font-bold f-left no-margin-all">Worker List</p>
						<a href="javascript:void(0)" class="display-inline-mid font-14 f-right" id="addWorkerModal">
							<i class="fa fa-user-plus font-14 padding-right-5"></i> 
							Add Worker
						</a>
						
						<div class="clear"></div>
					</div>
					<div class="bggray-white padding-top-10 padding-bottom-10 height-300px worker-list">
						<div class="border-bottom-small border-black padding-bottom-10 margin-left-10 list-item template">
							<input type="text" class="display-inline-mid width-45per name" placeholder="Name">
							<input type="text" class="display-inline-mid width-45per no-margin-left designation" placeholder="Designation">
							<div class="display-inline-mid width-15px margin-left-10">
								<img src="../assets/images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor remove">
							</div>
						</div>
					</div>
				</div>

				<div class="width-50per f-right">
					<div class="padding-bottom-10 padding-top-10">
						<p class="font-14 font-bold f-left no-margin-all">Equipment Used</p>
						<a href="javascript:void(0)" class="f-right font-14" id="addEquipmentModal">
							<i class="fa fa-truck font-16 padding-right-5"></i> 
							Add Equipment
						</a>
						<div class="clear"></div>
					</div>
					<div class="bggray-white padding-top-10 padding-bottom-10 height-300px equipment-list">
						<div class="border-bottom-small border-black padding-bottom-10 margin-left-10 list-item template">
							<input type="text" class="display-inline-mid width-90per equipment-name" placeholder="Equipment Name">
							<div class="display-inline-mid width-15px margin-left-10">
								<img src="../assets/images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor remove">
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	
		<div class="modal-footer text-right">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20  modal-trigger complete-loading-confirmation">Complete Loading</button>
		</div>
	</div>	
</div>

<div class="modal-container display-product-details" modal-id="complete-loading-confirmation">
	<div class="modal-body small ">				

		<div class="modal-head">
			<h4 class="text-left">Complete Loading</h4>				
			<div class="modal-close close-me"></div>
		</div>

		<div class="modal-content text-left">
			<div class="padding-all-10 bggray-7cace5">
			<p class="font-14 font-400 white-color">Loading Details Complete. Please view the record to generate CDR</p>
		</div>
		</div>
	
		<div class="modal-footer text-right">	
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 show-record">Show Record</button>
		</div>
	</div>	
</div>

<div class="modal-container" modal-id="getting-data-record">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color message">Getting information for update of record, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>


<div class="modal-container" modal-id="generate-cdr">
	<div class="modal-body large">				

		<div class="modal-head">
			<h4 class="text-left">Generate CDR</h4>				
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
			<p class="font-14 font-400 black-color no-margin-all">Please confirm if the following CDR details are correct:</p>
			
			<!-- PRINTABLE -->
			<div class="bggray-white padding-all-10 margin-top-20 printable">
			    <table style="width: 700px;">
			        <tbody>
			            <tr>
			                <td style="text-align:center; font-size:18px; font-weight:bold; font-family: Arial;">
			                    CARGO DELIVERY RECEIPT
			                </td>
			            </tr>
			        </tbody>
			    </table>
			    <table style="width: 700px;">
			        <tbody>
			            <tr>
			                <td class="cdr-number" style="text-align:right; font-family: Arial;">
			                    
			                </td>
			            </tr>
			        </tbody>
			    </table>
			    <table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
			        <tbody style="">
			            <tr style="">
			                <td style="padding: 0px;min-width: 0px;width: 100px;">
			                    Deliver to/Consignee:
			                </td>
			                <td class="consignee-name" style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 180px;text-align:center;">
			                    
			                </td>
			                <td style="padding: 0px; min-width: 0px; width: 15px;">
			                    Date:
			                </td>
			                <td class="date" style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 130px; text-align:center;">
			                	
			                </td>
			                <td style="padding: 0px;min-width: 0px;width: 46px;">
			                    Time-out:
			                </td>
			                <td class="time-out" style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 75px;text-align:center;">
			                	
			                </td>
			            </tr>
			        </tbody>
			    </table>
			    <table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
			        <tbody style="">
			            <tr style="">
			                <td style="padding: 0px;min-width: 0px;width: 45px;">
			                    Vessel:
			                </td>
			                <td class="vessel" style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 270px;text-align:center;">
			                	
			                </td>
			                <td style="padding: 0px;min-width: 0px;width: 69px;">
			                    Commodity:
			                </td>
			                <td class="commodity" style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 317px;text-align:center;">
			                	
			                </td>
			            </tr>
			        </tbody>
			    </table>
			    <table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
			        <tbody style="">
			            <tr style="">
			                <td style="padding: 0px;min-width: 0px;width: 84px;">
			                    Storage Area:
			                </td>
			                <td class="storage-area" style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 180px; text-align:center;">
			                	
			                </td>
			                <td style="padding: 0px;min-width: 0px;width: 88px;">
			                    Loading Type:
			                </td>
			                <td class="loading-type" style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 130px; text-align:center;">
			                	
			                </td>
			                <td style="padding: 0px;min-width: 0px;">
			                    Loading Date:
			                </td>
			                <td class="loading-date" style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 130px; text-align:center;">
			                	
			                </td>
			            </tr>
			        </tbody>
			    </table>
			    <div style="width:711px; border:1px solid #aaa; padding: 0px !important; margin: 0px !important; margin-top: 35px !important; margin-right: 0px !important;">
			        <table style="font-size: 12px;font-family: Arial;width: 710px;margin: 0px;padding: 0px;">
			            <tbody>
			                <tr>
			                    <td style="border-right: 1px solid #D7D7D7;height: 20px;padding: 0px;margin: 0px;">
			                        <table style="width: 290px; margin: 0px; padding: 0px; border-bottom: 1px solid #ddd;">
			                            <tbody>
			                                <tr>
			                                    <td style="text-align:center;font-size: 13px;font-weight:bold;font-family: Arial;padding: 10px;">
			                                        Loading details
			                                    </td>
			                                </tr>
			                            </tbody>
			                        </table>
			                        <table style="width: 290px;margin: 0px;padding: 0px;height: 140px;">
			                            <tbody>
			                                <tr>
			                                    <td style="text-align:center;font-size:12px;font-weight:bold;font-family: Arial;padding: 0px;">
			                                        <table style=" width: 290px; margin: 0px; padding: 0px; height: 165px;">
			                                            <tbody>
			                                                <tr>
			                                                    <td style="width: 50px;padding: 0px;margin: 0px;border-right: 1px solid #DDD;"></td>
			                                                    <td style=" padding: 0px; margin: 0px;">
			                                                        <table style=" width: 209px; margin: 0px; padding: 0px; height: 140px;">
			                                                            <tbody>
			                                                                <tr style=" height: 35px; text-align: center; font-size: 13px; border-bottom: 1px solid #ddd;">
			                                                                    <td>
			                                                                        Weight
			                                                                    </td>
			                                                                </tr>
			                                                                <tr>
			                                                                    <td style=" padding: 0px; margin: 0px;">
			                                                                        <table style=" padding: 0px; margin: 0px; height: 104px; width: 209px; font-size: 12px; font-weight: initial;">
			                                                                            <tbody>
			                                                                                <tr>
			                                                                                    <td style=" width: 50px; min-width: 0px; text-align: right; vertical-align: bottom;">
			                                                                                        TARE In:
			                                                                                    </td>
			                                                                                    <td class="tare-in" style="border-bottom: 1px solid #999;width: 90px;min-width: 0px;text-align: center;vertical-align: bottom;">
			                                                                                    	
			                                                                                    </td>
			                                                                                    <td style=" width: 25px; min-width: 0px;"></td>
			                                                                                </tr>
			                                                                                <tr>
			                                                                                    <td style=" width: 10px; min-width: 0px; text-align: right; vertical-align: bottom;">
			                                                                                        Gross:
			                                                                                    </td>
			                                                                                    <td class="gross" style="border-bottom: 1px solid #999;width: 90px;min-width: 0px;text-align: center;vertical-align: bottom;">
			                                                                                    	
			                                                                                    </td>
			                                                                                    <td style=" width: 25px; min-width: 0px;"></td>
			                                                                                </tr>
			                                                                                <tr>
			                                                                                    <td style=" width: 10px; min-width: 0px; text-align: right; vertical-align: bottom;">
			                                                                                        Net:
			                                                                                    </td>
			                                                                                    <td class="net" style="border-bottom: 1px solid #999;width: 90px;min-width:0px;text-align: center;vertical-align: bottom;">
			                                                                                        
			                                                                                    </td>
			                                                                                    <td style=" width: 25px; min-width: 0px;"></td>
			                                                                                </tr>
			                                                                            </tbody>
			                                                                        </table>
			                                                                    </td>
			                                                                </tr>
			                                                            </tbody>
			                                                        </table>
			                                                    </td>
			                                                </tr>
			                                            </tbody>
			                                        </table>
			                                    </td>
			                                </tr>
			                            </tbody>
			                        </table>
			                    </td>
			                    <td style="height: 20px;padding: 0px;margin: 0px;">
			                        <table style="width: 418px; margin: 0px; padding: 0px; border-bottom: 1px solid #ddd;">
			                            <tbody>
			                                <tr>
			                                    <td style="text-align:center;font-size: 13px;font-weight:bold;font-family: Arial;padding: 10px;">
			                                        Particulars
			                                    </td>
			                                </tr>
			                            </tbody>
			                        </table>
			                        <table style="width: 417px;margin: 0px;padding: 0px;height: 165px;">
			                            <tbody>
			                                <tr>
			                                    <td style="text-align:center;font-size:12px;font-weight:bold;font-family: Arial;padding: 10px;"></td>
			                                </tr>
			                            </tbody>
			                        </table>
			                    </td>
			                </tr>
			            </tbody>
			        </table>
			    </div>
			    <table style="padding:0px;margin:0px;font-size: 12px;font-family: Arial;width: 712px;margin-top: 30px;">
			        <tbody>
			            <tr>
			                <td style="width:400px;padding: 10px;margin: 0px;">
			                    <table style=" padding: 0px; margin:0px; font-size: 12px; font-family: Arial;">
			                        <tbody>
			                            <tr style=" height: 35px;">
			                                <td style=" text-align: right; vertical-align: bottom;">
			                                    Vessel Name:
			                                </td>
			                                <td class="vessel-name" style="border-bottom: 1px solid #989898;width: 250px;vertical-align: bottom;text-align: center;">
			                                	
			                                </td>
			                            </tr>
			                            <tr style=" height: 35px;">
			                                <td style=" text-align: right; vertical-align: bottom;">
			                                    Captain:
			                                </td>
			                                <td class="captain" style="border-bottom: 1px solid #989898;width: 250px;vertical-align: bottom;text-align: center;">
			                                	
			                                </td>
			                            </tr>
			                            <tr style=" height: 35px;">
			                                <td style=" text-align: right; vertical-align: bottom;">
			                                    Issued By:
			                                </td>
			                                <td class="issued-by" style="border-bottom: 1px solid #989898;width: 250px;vertical-align: bottom;text-align: center;">
			                                	
			                                </td>
			                            </tr>
			                        </tbody>
			                    </table>
			                </td>
			                <td style="width: 312px;border: 1px solid #A5A5A5;padding: 0px;">
			                    <table style=" padding: 0px; margin: 0px; height: 125px; width: 312px;">
			                        <tbody>
			                            <tr style=" border-bottom: 1px solid #999;">
			                                <td style=" text-align: center; height: 60px; vertical-align: top; font-size: 14px;">
			                                    Received the above cargo in good condition
			                                </td>
			                            </tr>
			                            <tr style="">
			                                <td style=" height: 70px; text-align: center; font-size: 14px; padding: 0px 30px;">
			                                    Consignee's Representative Customer signature over printed name
			                                </td>
			                            </tr>
			                        </tbody>
			                    </table>
			                </td>
			            </tr>
			        </tbody>
			    </table>
			</div>
			<!--  PRINTABLE -->

  

			
		</div>
	
		<div class="modal-footer text-right">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 modal-trigger confirm">Confirm</button>
		</div>
	</div>	
</div>


<div class="modal-container display-product-details" modal-id="confirm-generatecdr">
	<div class="modal-body small ">				

		<div class="modal-head">
			<h4 class="text-left">Generate CDR</h4>				
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content text-left">
			<div class="padding-all-10 bggray-7cace5">
				<p class="font-14 font-400 white-color">Cargo Delivery Receipt has been generated.</p>
			</div>
		</div>
	
		<div class="modal-footer text-right">	
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 show-record">Show Record</button>
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 print-cdr">Print CDR</button>
		</div>
	</div>	
</div>