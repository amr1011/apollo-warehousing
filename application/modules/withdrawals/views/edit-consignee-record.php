<div class="main">
	<div class="breadcrumbs no-margin-left padding-left-50">			
		<ul>
			<li>
				<a href="<?php echo BASEURL; ?>withdrawals/consignee_record_list">Withdrawal - Consignee Goods</a>
			</li>
			<li><span>&gt;</span></li>						
			<li>
				<a href="<?php echo BASEURL; ?>withdrawals/view_consignee_record" class="anchor-withdrawal-no">Withdrawal No.  0</a>
			</li>
			<li><span>&gt;</span></li>						
			<li class="font-bold black-color">Edit Details</li>
		</ul>
	</div>
 
	<div class="semi-main">
		<div class="padding-top-30 margin-bottom-30 text-right width-100percent">
			<a href="<?php echo BASEURL; ?>withdrawals/view_consignee_record">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color" >Cancel </button>
			</a>
			<button class="btn general-btn modal-trigger" modal-target="save-changes">Save Changes</button>
		</div>
		<div class="clear"></div>
		<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0">
			<p class="font-20 font-bold black-color padding-bottom-20 withdrawal-number" data-withdrawal-number="">
				Withdrawal No. 123123213
			</p>
			<div class="width-50percent display-inline-top">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid width-120"> ATL No.: </p>
					<input type="text" class="display-inline-mid t-medium width-230px" id="inputWithdrawalATLno">
				</div>
				<div class="padding-bottom-20">
					<p class="font-14 font-bold display-inline-top width-120">
						Consignee:
					</p>
					<div class="display-inline-mid width-340">
						<div class="select large">
							<select class="transform-dd" id="selectWithdrawalConsignee1"> </select>
						</div>
						<div class="padding-top-10">
							<input type="checkbox" class="no-margin-all display-inline-mid default-cursor" id="cargoSwap">
							<label class="font-14 font-bold width-120 display-inline-mid margin-left-10 default-cursor" for="cargo-swap">Cargo Swap</label>
						</div>
						<div class="padding-top-10">
							<div class="select large">
								<select class="transform-dd" id="selectWithdrawalConsignee2">
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="width-50percent display-inline-top">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-top width-150 margin-top-5">
						Origin Vessel:
					</p>
					<div class="select large">
						<select class="transform-dd" id="selectWithdrawalVessel">
						</select>
					</div>
				</div>
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-top width-150 margin-top-5">
						Mode of Delivery:
					</p>
					<div class="delivery-method select large">
						<select class="transform-dd" id="selectModeDelivery">
							<option value="truck"> Truck </option>
							<option value="vessel"> Vessel </option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0 trucking">
			<p class="font-20 font-bold black-color padding-bottom-20">
				Trucking Details
			</p>
			<div class="width-50percent display-inline-top">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid width-120"> Trucking:</p>
					<input type="text" class="display-inline-mid t-medium width-230px" id="inputTrucking" >
				</div>
				<div class="padding-bottom-20">
					<p class="font-14 font-bold display-inline-top width-120"> Plate No.: </p>
					<input type="text" class="display-inline-mid t-medium width-230px" id="inputTruckingPlateNo" >
				</div>
			</div>
			<div class="width-50percent display-inline-top">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-top width-150 margin-top-5"> Driver Name: </p>
					<input type="text" class="display-inline-mid t-medium width-230px" id="inputTruckingDriverName" >
				</div>
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-top width-150 margin-top-5"> License No.: </p>
					<input type="text" class="display-inline-mid t-medium width-230px" id="inputTruckingLicenseNo" >
				</div>
			</div>
		</div>
		<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0 vessel display-none">
			<p class="font-20 font-bold black-color padding-bottom-20">
				Vessel Details
			</p>
			<div class="width-50percent display-inline-top">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-mid width-150px">
						Vessel Name:
					</p>
					<div class="delivery-method select large">
						<select class="transform-dd" id="selectVesselName"> </select>
					</div>
				</div>
				<div class="padding-bottom-20">
					<p class="font-14 font-bold display-inline-top width-150px"> Destination </p>
					<input type="text" class="display-inline-mid t-medium width-230px" id="inputVesselDestination">
				</div>
			</div>
			<div class="width-50percent display-inline-top">
				<div class="padding-bottom-10">
					<p class="font-14 font-bold display-inline-top width-150 margin-top-5"> Vessel Captain </p>
					<input type="text" class="display-inline-mid t-medium width-230px" id="inputVesselCaptain">
				</div>
			</div>
		</div>
		<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white padding-bottom-20 ">
			<div class="width-100percent text-left padding-all-30 no-padding-top no-padding-left margin-bottom-30 border-bottom-small border-gray">
				<p class="font-20 font-bold black-color padding-bottom-20 ">Item List</p>
				<p class="font-14 font-bold display-inline-mid padding-right-20">Item: <span class="red-color display-inline-top">*</span></p>
				<div class="select dispaly-inline-mid large">
					<select class="transform-dd" id="selectItem">
					</select>
				</div>
				<button class="btn general-btn padding-left-20 padding-right-20" id="btnAddItem">Add Item</button>

			</div>
			
			<span id="existingItemContainer">
				<div class="border-full padding-all-20 text-left font-0 margin-bottom-20 template item-template">
					<div class="width-10percent display-inline-mid text-left">
						<img src="" alt="" class="height-100percent width-100percent item-image">
					</div>
					<div class="width-90percent display-inline-mid">
						<p class="font-16 font-bold f-left padding-left-20 item-sku-name"></p>
						<div class="f-right width-20px margin-right-15">
							<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor remove-item">
						</div>
					</div>
				</div>
			</span>

			<span id="itemContainer">
				<div class="border-full padding-all-20 item-template template">
					<div class="bggray-white  ">
						<div class="height-100percent display-inline-mid width-230px">
							<img src="../assets/images/corn.jpg" alt="" class="height-100percent width-100percent item-image">
						</div>

						<div class="display-inline-top width-75percent padding-left-10">
							<div class="padding-all-15 bg-light-gray font-0 text-left">
								<p class="font-16 font-bold display-inline-mid width-50percent item-sku-name"> </p>

								<div class="display-inline-mid width-50percent">
									<div class="f-right width-20px margin-left-10">
										<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor item-remove">
									</div>
									<div class="clear"></div>
								</div>
							</div>

							<div class="padding-all-15 text-left">
								<p class="display-inline-mid  width-20percent font-bold">Description:</p>
								<div class="display-inline-mid width-70percent margin-left-20 item-description">
								</div>
							</div>
							
							
							<div class="padding-all-15 bg-light-gray text-left font-0">
								<p class="display-inline-mid width-25per font-14 font-bold ">Loading Method</p>
								
								<input type="radio" class="display-inline-mid width-30px  bagbulk"  name="" value="bag">
								<label for="bags" class="font-14 font-400 display-inline-mid  padding-right-10">By Bags</label>
								<input type="radio" class="display-inline-mid width-30px bagbulk"  name="" value="bulk">
								<label for="bulk" class="font-14 font-400 display-inline-mid  padding-right-10">By Bulk</label>
							</div>

							<div class="padding-all-15 text-left font-0">
								<p class="display-inline-mid width-25per font-14 font-bold ">Unit Of Measure</p>
								<div class="select small">
									<select class="item-unit-of-measure"> </select>
								</div>
							</div>

							<div class="padding-all-15 bg-light-gray text-left font-0">
								<p class="display-inline-mid width-25per font-14 font-bold ">Qty To Withdraw</p>
								<input type="text" class="display-inline-mid t-small width-100px qty-to-withdraw"  />
							</div>

						</div>
					</div>
				</div>
			</span>
			
		</div>
		  
	</div>
</div>



<div class="modal-container" cr8v-file-upload="modal">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">
				Upload Document
			</h4>
			<div class="modal-close close-me"></div>
		</div>
		<div class="modal-content text-left" style="position:relative;">
			<div class="text-left padding-bottom-10">
				<p class="font-14 font-400 no-margin-all display-inline-mid width-125px">
					Document Name:
				</p><input type="text" class="width-200px display-inline-mid document-name"><button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 browse">Browse</button>
			</div>
		</div>
		<div class="modal-footer text-right">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button><button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 confirm-upload">Confirm</button>
		</div>
	</div>
</div>


<div class="modal-container " modal-id="add-notes">
	<div class="modal-body small">				

		<div class="modal-head">
			<h4 class="text-left">Add Notes</h4>				
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content text-left">
			<div class="text-left padding-bottom-10">
				<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
				<input type="text" class="width-380px display-inline-mid" id="noteSubject">
			</div>
			<div class="text-left padding-bottom-10">
				<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
				<textarea class="width-380px margin-left-10 height-250px" id="noteMessage"></textarea>
			</div>
		</div>
	
		<div class="modal-footer text-right">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" id="confirmNote">Confirm</button>
		</div>
	</div>	
</div>



<div class="modal-container" modal-id="create-ATL">
	<div class="modal-body large" style="width: 790px !important;">				

		<div class="modal-head">
			<h4 class="text-left to-hide-print">Create ATL</h4>				
			<div class="modal-close close-me"></div>
		</div>
	
		<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent  "  >
			<p class="font-14 font-400 black-color no-margin-all to-hide-print">Please confirm if the following ATL details are correct:</p>
			<div class="bggray-white padding-all-10 margin-top-20 printable" style="width:750px !important;">
 
				<table style="width: 700px;">
					<tbody>
						<tr>
							<td style="text-align:center; font-size:18px; font-weight:bold; font-family: Arial;">
								AUTHORITY TO LOAD
							</td>
						</tr>
					</tbody>
				</table>
				<table style="width: 700px;">
					<tbody>
						<tr>
							<td style="text-align:right; font-family: Arial;" class="atl-number">
								
							</td>
						</tr>
					</tbody>
				</table>
				<table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px;width: 120px;">
								Deliver to/Consignee:
							</td>
							<td style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 180px; text-align:center;" class="consignee-name">
								
							</td>
							<td style="padding: 0px; min-width: 0px; width: 15px;" >
								Date:
							</td>
							<td style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 130px; text-align:center;" class="date">
								
							</td>
							<td style="padding: 0px; min-width: 0px; width: 45px;">
								Time-in:
							</td>
							<td style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 70px; text-align:center;">
								
							</td>
							<td style="padding: 0px; min-width: 0px; width: 52px;">
								Time-out:
							</td>
							<td style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 70px; text-align:center;">
								
							</td>
						</tr>
					</tbody>
				</table>
				<table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px;width: 40px;">
								Vessel:
							</td>
							<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 230px; text-align:center;" class="vessel-name">
								
							</td>
							<td style="padding: 0px;min-width: 0px;width: 66px;">
								Commodity:
							</td>
							<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 230px; text-align:center;" class="commodity">
								
							</td>
							<td style="padding: 0px; min-width: 0px; width: 45px;">
								Balance:
							</td>
							<td style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 75px; text-align:center;">
								
							</td>
						</tr>
					</tbody>
				</table>
				<table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px;width: 105px;">
								No. of Bags / Bulk:
							</td>
							<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 200px; text-align:center;" class="no-bags-bulk">
								
							</td>
							<td style="padding: 0px;min-width: 0px;width: 80px;">
								Accomodation:
							</td>
							<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 180px; text-align:center;">
								
							</td>
							<td style="padding: 0px;min-width: 0px;width: 70px;">
								Aging Days:
							</td>
							<td style="border-bottom: 1px solid #444; padding: 0px; min-width: 0px; width: 60px; text-align:center;">
								
							</td>
						</tr>
					</tbody>
				</table>
				<table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px; width: 100px;">
								Trucking / Barging:
							</td>
							<td class="trucking-barging" style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 200px; text-align:center;">
								
							</td>
							<td style="padding: 0px;min-width: 0px; width: 160px;">
								Truck Plate No. / Barge Name:
							</td>
							<td class="truck-plate-no-barge-name" style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 200px; text-align:center;">
								
							</td>
						</tr>
					</tbody>
				</table>
				<table style="font-size: 12px; font-family: Arial; width: 700px; margin-top: 30px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px; width: 100px;">
								Truck / Barge Patron:
							</td>
							<td class="trucking-barging-patron" style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 200px; text-align:center;">
								
							</td>
							<td style="padding: 0px;min-width: 0px; width: 40px;">
								License:
							</td>
							<td class="license" style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 250px; text-align:center;">
								
							</td>
						</tr>
					</tbody>
				</table>

				<div style="height:30px;"></div>

				<div style="width:700px; border:1px solid #999; padding:10px;">
					<table style="font-size: 12px; font-family: Arial; width: 660px;">
						<tbody>
							<tr>
								<td style="text-align:center; font-size:15px; font-weight:bold;">
									Loading Details
								</td>
							</tr>
						</tbody>
					</table>
					<table style="font-size: 12px;font-family: Arial;width: 660px;margin-top: 20px;">
						<tbody style="">
							<tr style="">
								<td style="padding: 0px;min-width: 0px;width: 80px;">
									Storage Area:
								</td>
								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 130px; text-align:center;" class="storage-area">
									
								</td>
								<td style="padding: 0px;min-width: 0px;width: 80px;">
									Loading Type:
								</td>
								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" class="loading-type">
									
								</td>
								<td style="padding: 0px;min-width: 0px;width: 170px;">
									Est. Loading Date and Time:
								</td>
								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" class="est-loading-date-time">
									
								</td>
							</tr>
						</tbody>
					</table>
					<table style="font-size: 12px;font-family: Arial;width: 660px;margin-top: 20px;">
						<tbody style="">
							<tr style="">
								<td style="padding: 0px;min-width: 0px;width: 60px;">
									Loading Start:
								</td>
								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" class="loading-start">
									
								</td>
								<td style="padding: 0px;min-width: 0px;width: 60px;">
									Loading End:
								</td>
								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" class="loading-end">
									
								</td>
								<td style="padding: 0px;min-width: 0px;width: 80px;">
									Equipment Used:
								</td>
								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" class="equipment-used">
									
								</td>
							</tr>
						</tbody>
					</table>
					<table style="font-size: 12px;font-family: Arial;width: 660px;margin-top: 20px;">
						<tbody style="">
							<tr style="">
								<td style="padding: 0px;min-width: 0px;width: 70px;">
									Verified by:
								</td>
								
								<td style="width:10px;"> </td>

								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" >
									
								</td>

								<td style="width:50px;"> </td>

								<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px; text-align:center;" >
									
								</td>

								<td style="width:100px;"> </td>
							</tr>
						</tbody>
					</table>
					<table style="font-size: 12px;font-family: Arial;width: 660px;">
						<tbody style="">
							<tr style="">
								<td style="padding: 0px;min-width: 0px;width: 70px;">

								</td>
								
								<td style="width:10px;"> </td>

								<td style="padding: 0px;min-width: 0px;width: 100px; text-align:center; " >
									Cargo Checker
								</td>

								<td style="width:50px;"> </td>

								<td style=" padding: 0px;min-width: 0px;width: 100px; text-align:center;" >
									Equipment Operator
								</td>

								<td style="width:100px;"> </td>
							</tr>
						</tbody>
					</table>
				</div>

				<div style="height:40px;"> </div>

				<table style="font-size: 12px;font-family: Arial;width: 660px;margin-top: 20px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px;width: 70px;">
								Issued by:
							</td>
							
							<td style="width:10px;"> </td>

							<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px;" >
								
							</td>

							<td style="width:50px;"> </td>

							<td style="border-bottom: 1px solid #444;padding: 0px;min-width: 0px;width: 100px;" >
								
							</td>

							<td style="width:100px;"> </td>
						</tr>
					</tbody>
				</table>

				<table style="font-size: 12px;font-family: Arial;width: 660px;">
					<tbody style="">
						<tr style="">
							<td style="padding: 0px;min-width: 0px;width: 70px;">

							</td>
							
							<td style="width:10px;"> </td>

							<td style="padding: 0px;min-width: 0px;width: 100px; text-align:center;" >
								Posting Clerk
							</td>

							<td style="width:50px;"> </td>

							<td style=" padding: 0px;min-width: 0px;width: 100px; text-align:center;" >
								Operations Supervisor
							</td>

							<td style="width:100px;"> </td>
						</tr>
					</tbody>
				</table>

			</div>
		</div>
	
		<div class="modal-footer text-right to-hide-print">
			<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 confirm-atl">Confirm</button>
		</div>
	</div>	
</div>


<div class="modal-container display-product-details" modal-id="confirm-ATL">
	<div class="modal-body small ">				

		<div class="modal-head">
			<h4 class="text-left">Create ATL</h4>				
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content text-left">
			<div class="padding-all-10 bggray-7cace5">
				<p class="font-14 font-400 white-color message"></p>
			</div>
		</div>
	
		<div class="modal-footer text-right">		
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 show-record">Show Record</button>
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 print-atl">Print ATL</button>
		</div>
	</div>	
</div>

<div class="modal-container" modal-id="generate-view">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>

<div class="modal-container display-product-details" modal-id="remove-existing-item">
	<div class="modal-body small ">				

		<div class="modal-head">
			<h4 class="text-left">Remove Item</h4>				
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content text-left">
			<div class="padding-all-10  ">
				<p class="font-14 font-400  message">
					Are you sure you want to remove this item?
				</p>
			</div>
		</div>
	
		<div class="modal-footer text-right">		
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">Cancel</button>
			<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 confirm">Confirm</button>
		</div>
	</div>	
</div>

<div class="modal-container" modal-id="updating-record">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Updating record, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>

<div class="modal-container" modal-id="getting-data-record">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Getting information for update of record, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>
