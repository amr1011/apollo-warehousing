<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."third_party/dompdf/autoload.inc.php";

use Dompdf\Dompdf;
use Dompdf\Options;

class Withdrawals extends MX_Controller {

	private $response = array(
    	"status" => false,
    	"message" => "",
    	"data" => null
    );

	public function __construct()
	{
		parent:: __construct();
		$this->load->library('MY_Form_validation');
        $this->form_validation->CI =& $this;
        $this->load->model("withdrawals_model");
	}

	/* FOR CONSIGNEE FUNCTIONS */

 	public function consignee_record_list()
	{
		$data['title'] = 'WITHDRAWAL - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("withdrawal-consignee-list", $data);
		$this->load->view("includes/footer.php");
	}

	public function create_consignee_record(){
		$data['withdrawal_number'] = $this->generate_withdrawal_number(2);

		$data['title'] = 'WITHDRAWAL - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("create-consignee-record", $data);
		$this->load->view("includes/footer.php");
	}

	public function view_consignee_record(){
		$data['title'] = 'WITHDRAWAL - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("view-consignee-record", $data);
		$this->load->view("includes/footer.php");
	}

	public function edit_consignee_record(){
		$data['title'] = 'WITHDRAWAL - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("edit-consignee-record", $data);
		$this->load->view("includes/footer.php");
	}

	public function transform_to_receiving_number($receiving_num = 1)
	{
		$length = 10;
		$return = "0";
		$len = strlen($receiving_num);
		$int_val = intval($receiving_num);
		$return_data = $int_val;

		$len = $length - $len;

		for($i = 1; $i < $len; $i++){
			$return = "0".$return;
		}

		$return = $return.$return_data;

		return $return;
	}

	public function generate_withdrawal_number($type = 0){
		$post_type = $this->input->post('post_type');
		if(isset($post_type)) {
			$type = $post_type;
		}

		$length = 9;
		$zero = 0;
		$return = "0";
		$receiving_num = "0";
		if($type > 0){
			$record = $this->withdrawals_model->get_latest_receiving_number($type);
			if(count($record) > 0){	
				$receiving_num = $record[0]["withdrawal_number"];
			}
			$len = strlen($receiving_num);
			

			$int_val = intval($receiving_num);
			$return_data = $int_val + 1;

			$len = $length - $len;

			for($i = 1; $i < $len; $i++){
				$return = "0".$return;
			}

			$return = $return.$return_data;

			// return $return;
			if(isset($post_type)) {
				$response = array(
					'status' => true,
					'message' => '',
					'data' => $return
				);

				header('Content-Type: application/json');
				echo json_encode($response);
			} else {
				return $return;
			}
		}
	}

	public function get_withdrawal_consignee_record($param_record_id = 0)
	{
		$record_id = $this->input->post("record_id");
		$where = " WHERE is_deleted = 0 AND type = 2 ORDER BY date_created DESC LIMIT 200";
		if($record_id){
			$where = "WHERE id = {$record_id} LIMIT 1";
		}
		if($param_record_id > 0){
			$record_id = $param_record_id;
			$where = "WHERE id = {$record_id} LIMIT 1";
		}

		//MODELS
		$this->load->model('vessel_list/vessel_model');
		$this->load->model('consignee/consignee_model');
		$this->load->model('settings/loadingarea_model');
		$this->load->model('products/products_model');
		$this->load->model('receiving/receiving_model');
		$vessel_model = $this->vessel_model;
		$consignee_model = $this->consignee_model;
		$loading_area_model = $this->loadingarea_model;
		$products_model = $this->products_model;
		$receiving_model = $this->receiving_model;
		$model = $this->withdrawals_model;

		$logs = $model->get_all_consignee_withdrawal_logs($where);

		foreach ($logs as $i => &$log) {
			$log_id = $log["id"];
			$consignee_details = $model->get_withdrawal_consignee_details($log_id);
			
			$withdrawal_number = $this->transform_to_receiving_number($log["withdrawal_number"]);
			$log["withdrawal_number"] = $withdrawal_number;
			$log["date_created_formats"] = array(
				"1" =>  date("M d, Y | g:i a", strtotime($log["date_created"])),
				"2" =>  date("M d, Y", strtotime($log["date_created"])),
				"3" => date("j-M-Y g:i A", strtotime($log["date_created"])),
				"4" => date("j-M-Y", strtotime($log["date_created"]))
			);

			//RECORD DETAILS
			if(count($consignee_details) > 0){
				$cons_data = $consignee_details[0];
				$vessel = $vessel_model->get_single_vessel($cons_data["origin_vessel_id"]);

				$cons_data["origin_vessel"] = $vessel[0];

				if($cons_data["mode_of_delivery"] == "truck"){
					$truck_details = $model->get_truck_details($log_id);
					$cons_data["truck_details"] = $truck_details[0];
				}else if($cons_data["mode_of_delivery"] == "vessel"){
					$vessel_details = $model->get_vessel_details($log_id);
					$ves_det = $vessel_details[0];

					$vessel_data = $vessel_model->get_single_vessel($ves_det["vessel_id"]);
					$ves_det["vessel_data"] = $vessel_data[0];
					$cons_data["vessel_details"] = $ves_det;
				}

				$consigneedetails = $consignee_model->get_selected_consignee($cons_data["consignee_id"]);
				if(count($consigneedetails) > 0){
					$consigneedetails = $consigneedetails[0];
				}
				$cons_data["consignee"] = $consigneedetails;

				$consignee_details = $cons_data;
			}
			$log["consignee_details"] = $consignee_details;

			//CONSIGNEE SWAP
			$consignee_swap_data = $model->get_consignee_swap($log_id);
			if(count($consignee_swap_data) > 0){
				$swap_data = $consignee_swap_data[0];

				$consignee_from = $consignee_model->get_selected_consignee($swap_data["consignee_id_from"]);
				$consignee_to = $consignee_model->get_selected_consignee($swap_data["consignee_id_to"]);

				$swap_data["consignee_from"] = $consignee_from[0];
				$swap_data["consignee_to"] = $consignee_to[0];

				$consignee_swap_data = $swap_data;
			}
			$log["consignee_swap_data"] = $consignee_swap_data;

			//DOCS
			$log["documents"] = $model->get_documents($log_id);
			foreach ($log["documents"] as $doc_i => $doc_v) {
				$log["documents"][$doc_i]["date_added_formatted"] = date("j-M-Y g:i A", strtotime( $doc_v["date_added"] ));
			}

			//NOTES
			$log["notes"] = $model->get_notes($log_id);
			foreach ($log["notes"] as $note_i => $note_val) {
				$log["notes"][$note_i]["date_formats"] = array(
					"1" =>  date("M d, Y | g:i a", strtotime($note_val["date_created"])),
					"2" =>  date("M d, Y", strtotime($note_val["date_created"])),
					"3" => date("j-M-Y g:i A", strtotime($note_val["date_created"])),
					"4" => date("j-M-Y", strtotime($note_val["date_created"]))
				);
			}

			//CDR DETAILS
			$cdr = $model->get_cdr_details($log_id);
			if(count($cdr) > 0){
				$c = $cdr[0];
				$c["consignee"] = $consignee_model->get_selected_consignee($c["consignee_id"]);

				$c["seal_numbers"] = $model->get_seal_numbers($log_id);

				$cdr = $c;
			}
			$log["cdr_details"] = $cdr;

			//LOADING INFO
			$loading = $model->get_withdrawal_loading_information($log_id);
			if(count($loading) > 0){
				$load = $loading[0];
				$area = $loading_area_model->gel_loading_area($load["loading_area_id"]);
				$load["loading_area"] = $area[0];

				$load["est_date_time_formats"] = array(
					"1" =>  date("M d, Y | g:i a", strtotime($load["est_date_time"])),
					"2" =>  date("M d, Y", strtotime($load["est_date_time"])),
					"3" => date("j-M-Y g:i A", strtotime($load["est_date_time"])),
					"4" => date("j-M-Y", strtotime($load["est_date_time"])),
					"5" => date("g:i A", strtotime($load["est_date_time"])),
					"6" => date("m/d/Y", strtotime($load["est_date_time"])),
					"7" => date("h:m:s", strtotime($load["est_date_time"]))
				);

				if($load["datetime_end"] !== null){
					$load["datetime_end_formats"] = array(
						"1" =>  date("M d, Y | g:i a", strtotime($load["datetime_end"])),
						"2" =>  date("M d, Y", strtotime($load["datetime_end"])),
						"3" => date("j-M-Y g:i A", strtotime($load["datetime_end"])),
						"4" => date("j-M-Y", strtotime($load["datetime_end"])),
						"5" => date("g:i A", strtotime($load["datetime_end"]))
					);
				}

				if($load["datetime_start"] !== null){
					$load["datetime_start_formats"] = array(
						"1" =>  date("M d, Y | g:i a", strtotime($load["datetime_start"])),
						"2" =>  date("M d, Y", strtotime($load["datetime_start"])),
						"3" => date("j-M-Y g:i A", strtotime($load["datetime_start"])),
						"4" => date("j-M-Y", strtotime($load["datetime_start"])),
						"5" => date("g:i A", strtotime($load["datetime_start"]))
					);
				}

				$loading = $load;
			}
			$log["loading_information"] = $loading;

			//PRODUCTS
			$products = $model->get_withdrawal_product($log_id);
			if(count($products) > 0){
				foreach ($products as $i_prod => &$val_prod) {
					$prod_id = $val_prod["product_id"];
					$details = $products_model->get_product(" WHERE id = {$prod_id}");
					$uom_id = $val_prod["unit_of_measure_id"];
					$uom = $products_model->get_unit_of_measure(" WHERE id = {$uom_id}");
					$details[0]["unit_of_measure"] = $uom[0];
					$val_prod["details"] = $details[0];

					$batches = $this->withdrawals_model->get_withdrawal_batches($val_prod["id"]);
					if(count($batches) > 0){
						foreach ($batches as $i_batch => &$batch) {
							
							$uom_data = $products_model->get_unit_of_measure(" WHERE id = {$batch["unit_of_measure_id"]}");
							if(count($uom_data) > 0){
								$uom_data = $uom_data[0];
							}
							$batch["unit_of_measure"] = $uom_data;

							$location_data = $model->get_location($batch["location_id"]);
							if(count($location_data) > 0){
								$location_data = $location_data[0];
								$batch["location_hierarchy"] = $this->get_hierarchy_of_batch_location($location_data["storage_id"], $location_data["id"]);
							}
							$batch["location_data"] = $location_data;

							//$receiving_model
							$batch_data = $receiving_model->get_batch("WHERE id = {$batch["batch_id"]}");
							if(count($batch_data) > 0){
								$batch_data = $batch_data[0];
							}
							$batch["batch_data"] = $batch_data;
						}
					}

					$val_prod["batches"] = $batches;
				}
			}
			$log["products"] = $products;

			//WORKER LIST
			$workers = $model->get_worker_list(2, $log_id);
			$log["workers"] = $workers;

			//EQUIPMENT
			$equipments = $model->get_equipment_list(2, $log_id);
			$log["equipments"] = $equipments;

			//OWNER
			$user_data = array();
			if(IS_DEVELOPMENT === true){
				$user_data = $this->session->userdata('apollo_user');
			}else{
				//GET USER'S DATA
			}
			$log["owner"] = $user_data;
		}


		if($param_record_id > 0){
			return $logs;
		}else{
			$this->response["status"] = true;
			$this->response["data"] = $logs;

			header("Content-Type: application/json");
			echo json_encode($this->response);
			
		}

	}

	public function get_consignee_with_receiving(){
		 
		$consignees = $this->withdrawals_model->get_consignees_from_receiving();

		$this->response["status"] = true;
		$this->response["data"] = $consignees;

		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function get_vessels(){
		$consignees = $this->withdrawals_model->get_vessels();

		$this->response["status"] = true;
		$this->response["data"] = $consignees;

		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function get_items_from_receiving()
	{
		$items = $this->withdrawals_model->get_items_from_receiving();

		$this->response["status"] = true;
		$this->response["data"] = $items;

		header("Content-Type: application/json");
		echo json_encode($this->response);

	}

	public function get_location($id, $location)
	{
		foreach ($location as $key => $value) {
			if($value["id"] == $id){
				return $location[$key];
			}
		}

		return array();
	}

	public function get_parent_location($child, $location, $refs = array())
	{
		$data = &$refs;

		$parentdata = $this->get_location($child["parent_id"], $location);

		$data[] = $child;

		if( count($parentdata) > 0 ){
			return $this->get_parent_location($parentdata, $location, $data);
		}

		if( count($parentdata) === 0 ){
			return $data;
		}


	}

	public function get_hierarchy_of_batch_location($storage_id, $location_id)
	{
		$this->load->model("Storages/storage_model");
		$location = $this->storage_model->get_location($storage_id);
		$current = $this->get_location($location_id, $location);

		$data = $this->get_parent_location($current, $location);

		$reversed = array_reverse($data);
		return $reversed;
		
	}

	public function get_batches_from_product(){
		$product_id = $this->input->post("product_id");

		$batches = $this->withdrawals_model->get_batch_from_product($product_id);
		foreach ($batches as $key => &$batch) {
			$storage_data = $this->withdrawals_model->get_storage_data($batch["storage_id"]);
			if(count($storage_data) > 0){
				$batch["storage_data"] = $storage_data[0];
			}

			$hierarchy = $this->get_hierarchy_of_batch_location($batch["storage_id"], $batch["location_id"]);
			if(count($hierarchy) > 0){
				$batch["location_hierarchy"] = $hierarchy;
			}
			
		}

		$this->response["status"] = true;
		$this->response["data"] = $batches;

		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function upload_document(){

		$withdrawal_id = $this->input->post("withdrawal_id");
		$name = $this->input->post("name");
		$document_name = "";

		$temporary = $this->input->post("temporary");

		if(strlen($name) > 0){
			$document_name = $name;
		}

		$response = array();
		$arr_response = array();
		$status = false;
		$message = "Fail";
		if(isset($_FILES["attachment"])){
			$name = $_FILES["attachment"]["name"];
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 20; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    
		    $file_name = $randomString . "." .$ext;

		    move_uploaded_file($_FILES["attachment"]["tmp_name"], UPLOAD_DOCUMENT_PATH.$file_name);

		    $arr_response["path"] = UPLOAD_DOCUMENT_PATH.$file_name;
		    $arr_response["location"] = BASEURL . "uploads/documents/" . $file_name;
		    $status = true;
		    $message = "Succes";

		    if(strlen($name) == 0){
				$document_name = $file_name;
			}

		    if($temporary != 'true'){
			    $data_docs = array(
					"withdrawal_log_id" => $withdrawal_id,
					"document_name" => $document_name,
					"document_path" => $arr_response["location"],
					"date_added" => date("Y-m-d H:i:s")
				);
				$this->withdrawals_model->create_documents($data_docs);

				$data_docs["date_added_formatted"] = date("j-M-Y g:i A", strtotime( $data_docs["date_added"] ));

			    header("Content-Type: application/json");
			    $arr_response[0]["documents"] = array();
			    $arr_response[0]["documents"][] = $data_docs;
			    unset($arr_response["location"]);
		    	unset($arr_response["path"]);
		    }else{
		    	$data_docs = array(
		    		"withdrawal_log_id" => $withdrawal_id,
			    	"document_name" => $document_name,
			    	"date_added_formatted" =>  date("j-M-Y g:i A", strtotime( date("Y-m-d H:i:s") )),
			    	"document_path" => $arr_response["location"],
			    	"date_added" => date("Y-m-d H:i:s"),
			    	"status" => 0,
			    	"version" => 1
			    );

		    	header("Content-Type: application/json");
			    $arr_response[0]["documents"] = array();
			    $arr_response[0]["documents"][] = $data_docs;
			    unset($arr_response["location"]);
		    	unset($arr_response["path"]);
		    }
		}

		header("Content-Type: application/json");
		$response["status"] = $status;
		$response["message"] = $message;
		$response["data"] = $arr_response;
		echo json_encode($response);
	}

	public function validation_record_data(){
		$save_data = $this->input->post("save_data");
		$data = json_decode($save_data, TRUE);

		if($data === null AND json_last_error() === JSON_ERROR_NONE)
		{
			$this->form_validation->set_message("record_data", "Format not valid");
			return false;
		}
	}

	public function save_consignee_record(){
		$save_data = $this->input->post("save_data");
		$data = json_decode($save_data, TRUE);

		$this->form_validation->set_rules("save_data", "Record data", "required|trim|callback_validation_record_data");
 		
		$user_data = $this->session->userdata('apollo_user');

		if($this->form_validation->run() === FALSE)
		{
			$this->response["status"] = false;
			$this->response["message"] = validation_errors();
			$this->response["data"] = $this->form_validation->error_array();
		}else{

			$type = 2;
			$mode_of_delivery = $data["mode_of_delivery"];
			$withdrawal_number = intval($data["withdrawal_number"]);
			$user_data = array();
			if(IS_DEVELOPMENT === true){
				$user_data = $this->session->userdata('apollo_user');
			}else{
				//GET USER'S DATA
			}

			$withrawal_log_data = array(
				"user_id" => $user_data["id"],
				"withdrawal_number" => $withdrawal_number,
				"type" => $type,
				"date_created" => date("Y-m-d H:i:s")
			);

			$withdrawal_log_id = $this->withdrawals_model->create($withrawal_log_data);

			$cargo_swap = $data["cargo_swap"];
			$cargo_swap_int = 0;
			if($cargo_swap){
				$cargo_swap_int = 1;
			}

			$withrawal_consignee_data = array(
				"withdrawal_log_id" => $withdrawal_log_id,
				"atl_number" => $data["atl_number"],
				"cargo_swap" => $cargo_swap_int,
				"mode_of_delivery" => $mode_of_delivery,
				"consignee_id" => $data["consignee"]["id"],
				"origin_vessel_id" => $data["origin_vessel"]["id"]
			);

			$this->withdrawals_model->create_withrawal_consignee_details($withrawal_consignee_data);

			if($cargo_swap_int == 1){
				$consignee_swap_data = array(
					"withdrawal_log_id" => $withdrawal_log_id,
					"consignee_id_from" => $data["consignee_from"]["id"],
					"consignee_id_to" => $data["consignee_to"]["id"]
				);
				$this->withdrawals_model->create_withrawal_consignee_swap($consignee_swap_data);
			}

			if($mode_of_delivery == "truck"){
				$delivery_details = $data["mode_of_delivery_details"];
				$withdrawal_truck_data = array(
					"withdrawal_log_id" => $withdrawal_log_id,
					"trucking" => $delivery_details["trucking"],
					"plate_no" => $delivery_details["plate_no"],
					"driver_name" => $delivery_details["driver_name"],
					"license_no" => $delivery_details["license"]
				);
				$this->withdrawals_model->create_withdrawal_truck_details($withdrawal_truck_data);
			}else{
				$delivery_details = $data["mode_of_delivery_details"];
				$withdrawal_vessel_data = array(
					"withdrawal_log_id" => $withdrawal_log_id,
					"vessel_id" => $delivery_details["vessel_id"],
					"destination" => $delivery_details["destination"],
					"captain" => $delivery_details["captain"]
				);
				$this->withdrawals_model->create_withdrawal_vessel_details($withdrawal_vessel_data);
			}

			//products
			foreach ($data["items"] as $k => $v) {
				$prod_data = array(
					"withdrawal_log_id" => $withdrawal_log_id,
					"product_id" => $v["id"],
					"loading_method" => $v["loading_method"],
					"unit_of_measure_id" => $v["qty_to_withdraw_uom"],
					"qty" => $v["qty_to_withdraw"]
				);
				$this->withdrawals_model->create_withdrawal_product($prod_data);
			}

			//documents

			foreach ($data["documents"] as $k => $v) {
				$doc_data = array(
					"withdrawal_log_id" => $withdrawal_log_id,
					"document_name" => $v["document_name"],
					"document_path" => $v["document_path"],
					"date_added" => date("Y-m-d H:i:s")
				);
				$this->withdrawals_model->create_documents($doc_data);
			}

			//notes
			foreach ($data["notes"] as $k => $v) {
				$note_data = array(
					"withdrawal_log_id" => $withdrawal_log_id,
					"note" => $v["message"],
					"subject" => $v["subject"],
					"user_id" => $user_data["id"],
					"date_created" => $v["datetime"]
				);
				$this->withdrawals_model->create_notes($note_data);
			}


			$data["withdrawal_log_id"] = $withdrawal_log_id;

			$this->response["status"] = true;
			$this->response["data"] = $data;
		}


		

		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function save_new_notes(){
		$withdrawal_id = $this->input->post("withdrawal_id");
		$subject = $this->input->post("subject");
		$note = $this->input->post("note");

		$user_data = array();
		if(IS_DEVELOPMENT === true){
			$user_data = $this->session->userdata('apollo_user');
		}else{
			//GET USER'S DATA
		}

		$note_data = array(
			"withdrawal_log_id" => $withdrawal_id,
			"note" => $note,
			"subject" => $subject,
			"user_id" => $user_data["id"],
			"date_created" => date("Y-m-d H:i:s")
		);
		$this->withdrawals_model->create_notes($note_data);

		$note_data["date_formats"] = array(
			"1" =>  date("M d, Y | g:i a", strtotime($note_data["date_created"])),
			"2" =>  date("M d, Y", strtotime($note_data["date_created"])),
			"3" => date("j-M-Y g:i A", strtotime($note_data["date_created"])),
			"4" => date("j-M-Y", strtotime($note_data["date_created"]))
		);

		$this->response["status"] = true;
		$this->response["data"] = $note_data;

		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function save_atl_to_pdf(){
		$html = $this->input->post("html");
		$withdrawal_log_id = $this->input->post("withdrawal_id");

		$dompdf = new Dompdf();
		$dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 20; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }

        $file_name = $randomString.'.pdf';
        $dir_path = UPLOAD_DOCUMENT_PATH.$file_name;
        $url_path = BASEURL . "uploads/documents/" . $file_name;

        file_put_contents($dir_path, $output);

		$arr_response["path"] = UPLOAD_DOCUMENT_PATH.$file_name;
		$arr_response["location"] = BASEURL . "uploads/documents/" . $file_name;

		$doc_data = array(
			"withdrawal_log_id" => $withdrawal_log_id,
			"document_name" => 'ATL-FILE',
			"document_path" => $url_path,
			"date_added" => date("Y-m-d H:i:s")
		);
		$this->withdrawals_model->create_documents($doc_data);

		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function update_atl_pdf(){
		$html = $this->input->post("html");
		$withdrawal_log_id = $this->input->post("withdrawal_id");
		$atl_docs = $this->withdrawals_model->get_atl_documents($withdrawal_log_id);
		$atl_docs = $atl_docs[0];

		$dompdf = new Dompdf();
		$dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 20; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }

        $file_name = $randomString.'.pdf';
        $dir_path = UPLOAD_DOCUMENT_PATH.$file_name;
        $url_path = BASEURL . "uploads/documents/" . $file_name;

        file_put_contents($dir_path, $output);

		$arr_response["path"] = UPLOAD_DOCUMENT_PATH.$file_name;
		$arr_response["location"] = BASEURL . "uploads/documents/" . $file_name;

		$atl_docs["document_path"] = $url_path;

		$this->withdrawals_model->update_documents($atl_docs["id"], $atl_docs);

		$this->response["status"] = true;
		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function update_item(){
		$record_item_id = $this->input->post("record_item_id");
		$unit_of_measure_id = $this->input->post("unit_of_measure_id");
		$loading_method = $this->input->post("loading_method");
		$quantity = $this->input->post("quantity");

		$prod_data = array(
			"loading_method" => $loading_method,
			"unit_of_measure_id" => $unit_of_measure_id,
			"qty" => $quantity
		);
		$this->withdrawals_model->update_withdrawal_product($record_item_id, $prod_data);

		$this->response["status"] = true;
		$this->response["data"] = $prod_data;
		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function update_consignee_record(){
		$save_data = $this->input->post("save_data");
		$data = json_decode($save_data, TRUE);


		$this->form_validation->set_rules("save_data", "Record data", "required|trim|callback_validation_record_data");
 
		if($this->form_validation->run() === FALSE)
		{
			$this->response["status"] = false;
			$this->response["message"] = validation_errors();
			$this->response["data"] = $this->form_validation->error_array();
		}else{
			$withdrawal_log_id = $data["withdrawal_log_id"];
			$withdrawal_record = $this->get_withdrawal_consignee_record($withdrawal_log_id);
			if(count($withdrawal_record) > 0){
				$withdrawal_record = $withdrawal_record[0];

				$cargo_swap = false;
				if($data["cargo_swap"]){
					$cargo_swap = true;
				}

				$consignee_id = $data["consignee"]["id"];


				// CONSIGNEE SWAP ACTIONS
				if(count($withdrawal_record["consignee_swap_data"]) > 0){
					$consignee_swap_id = $withdrawal_record["consignee_swap_data"]["id"];
					if($cargo_swap){
						// update cargo swap data
						$consignee_swap_data = array(
							"withdrawal_log_id" => $withdrawal_log_id,
							"consignee_id_from" => $data["consignee_from"]["id"],
							"consignee_id_to" => $data["consignee_to"]["id"]
						);
						$this->withdrawals_model->update_withrawal_consignee_swap($consignee_swap_id, $consignee_swap_data);
					}else{
						// delete cargo swap data
						$consignee_swap_data = array("is_deleted" => 1);
						$this->withdrawals_model->update_withrawal_consignee_swap($consignee_swap_id, $consignee_swap_data);
					}
				}else{
					if($cargo_swap){
						//save cargo swap
						$consignee_swap_data = array(
							"withdrawal_log_id" => $withdrawal_log_id,
							"consignee_id_from" => $data["consignee_from"]["id"],
							"consignee_id_to" => $data["consignee_to"]["id"]
						);
						$this->withdrawals_model->create_withrawal_consignee_swap($consignee_swap_data);
					}
				}


				// withdrawal_consignee_details
				$withdrawal_consignee_details = $withdrawal_record["consignee_details"];
				$withdrawal_consignee_details_id = $withdrawal_record["consignee_details"]["id"];
				$withdrawal_consignee_details_update_data = array(
					"atl_number" => $data["atl_number"],
					"cargo_swap" => $cargo_swap,
					"mode_of_delivery" => $data["mode_of_delivery"],
					"consignee_id" => $consignee_id,
					"origin_vessel_id" => $data["origin_vessel"]["id"]
				);
				$this->withdrawals_model->update_consignee_details($withdrawal_consignee_details_id, $withdrawal_consignee_details_update_data);


				//mode_of_delivery
				$mode_of_delivery_details = $data["mode_of_delivery_details"];
				$vessel_details = $this->withdrawals_model->get_vessel_details($withdrawal_log_id);
				$truck_details = $this->withdrawals_model->get_truck_details($withdrawal_log_id);

				if($data["mode_of_delivery"] == "truck"){
					//IS TRUCKING
					if(count($truck_details) > 0){
						$truck_details_id = $truck_details[0]["id"];
						$truck_details_data = array(
							"trucking" => $mode_of_delivery_details["trucking"],
							"plate_no" => $mode_of_delivery_details["plate_no"],
							"driver_name" => $mode_of_delivery_details["driver_name"],
							"license_no" => $mode_of_delivery_details["license"]
						);
						$this->withdrawals_model->update_trucking_details($truck_details_id, $truck_details_data);
					}else{
						$withdrawal_truck_data = array(
							"withdrawal_log_id" => $withdrawal_log_id,
							"trucking" => $mode_of_delivery_details["trucking"],
							"plate_no" => $mode_of_delivery_details["plate_no"],
							"driver_name" => $mode_of_delivery_details["driver_name"],
							"license_no" => $mode_of_delivery_details["license"]
						);
						$this->withdrawals_model->create_withdrawal_truck_details($withdrawal_truck_data);
					}

					if(count($vessel_details) > 0){
						$vessel_details_id = $vessel_details[0]["id"];
						$vessel_details_data = array("is_deleted" => 1);
						$this->withdrawals_model->update_vessel_details($vessel_details_id, $vessel_details_data);
					}
				}else{
					// IS VESSEL
					if(count($vessel_details) > 0){
						$vessel_details_id = $vessel_details[0]["id"];
						$vessel_details_data = array(
							"vessel_id" => $mode_of_delivery_details["vessel_id"],
							"destination" => $mode_of_delivery_details["destination"],
							"captain" => $mode_of_delivery_details["captain"]
						);
						$this->withdrawals_model->update_vessel_details($vessel_details_id, $vessel_details_data);
					}else{
						$withdrawal_vessel_data = array(
							"withdrawal_log_id" => $withdrawal_log_id,
							"vessel_id" => $mode_of_delivery_details["vessel_id"],
							"destination" => $mode_of_delivery_details["destination"],
							"captain" => $mode_of_delivery_details["captain"]
						);
						$this->withdrawals_model->create_withdrawal_vessel_details($withdrawal_vessel_data);
					}

					if(count($truck_details) > 0){
						$truck_details_id = $truck_details[0]["id"];
						$truck_details_data = array("is_deleted" => 1);
						$this->withdrawals_model->update_trucking_details($truck_details_id, $truck_details_data);
					}
				}

				// REMOVED PRODUCTS
				$removed_products = $data["removed_items"];
				foreach ($removed_products as $i => $item) {
					$record_id = $item["record"]["id"];
					$removed_product_data = array("status" => 1);
					$this->withdrawals_model->update_withdrawal_product($record_id, $removed_product_data);
				}

				// NEW PRODUCTS
				$new_products = $data["items"];
				foreach ($new_products as $k => $v) {
					$prod_data = array(
						"withdrawal_log_id" => $withdrawal_log_id,
						"product_id" => $v["id"],
						"loading_method" => $v["loading_method"],
						"unit_of_measure_id" => $v["qty_to_withdraw_uom"],
						"qty" => $v["qty_to_withdraw"]
					);
					$this->withdrawals_model->create_withdrawal_product($prod_data);
				}

			}


			$this->response["status"] = true;
			$this->response["data"] = $withdrawal_record;
		}

		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function proceed_to_loading(){
		$withdrawal_id = $this->input->post("withdrawal_id");
		$data = array(
			"status" => 2
		);
		$this->withdrawals_model->update_withdrawal_log($withdrawal_id, $data);

		$this->response["status"] = true;
		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function cancel_proceed_to_loading(){
		$withdrawal_id = $this->input->post("withdrawal_id");
		$data = array(
			"status" => 0
		);
		$this->withdrawals_model->update_withdrawal_log($withdrawal_id, $data);

		$this->response["status"] = true;
		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function get_loading_area(){
		$this->load->model("settings/loadingarea_model");
		$loading_area = $this->loadingarea_model->gel_all();

		$this->response["status"] = true;
		$this->response["data"] = $loading_area;
		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function save_loading_details(){
		$withdrawal_id = $this->input->post('withdrawal_id');
		$items = $this->input->post('items');
		$est_date = $this->input->post('est_date');
		$est_time = $this->input->post('est_time');
		$loading_area_id = $this->input->post('loading_area');
		$handling_method = $this->input->post('handling_method');
		$items_data = json_decode($items, true);

		$loading_information_data = array(
			"withdrawal_log_id" => $withdrawal_id,
			"loading_area_id" => $loading_area_id,
			"handling_method" => $handling_method,
			"est_date_time" => $est_date." ".$est_time
		);
		$this->withdrawals_model->create_withdrawal_loading_information($loading_information_data);

		foreach ($items_data as $item_i => $item) {
			$batches = $item["batches"];
			foreach ($batches as $batch_i => $batch) {
				$data_batch = array(
					"batch_id" => $batch["batch"]["id"],
					"location_id" => $batch["location"]["id"],
					"qty" => $batch["qty"],
					"withdrawal_product_id" => $item["record_id"],
					"unit_of_measure_id" => $batch["uom_id"]
				);
				$this->withdrawals_model->create_withdrawal_batch($data_batch);
			}
		}

		$data_update = array(
			"status" => 3
		);
		$this->withdrawals_model->update_withdrawal_log($withdrawal_id, $data_update);


		$this->response["status"] = true;
		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function update_loading_information(){
		$withdrawal_id = $this->input->post('withdrawal_id');
		$est_date = $this->input->post('est_date');
		$est_time = $this->input->post('est_time');
		$handling_method = $this->input->post('handling_method');
		$loading_area_id = $this->input->post('loading_area_id');

		$loading = $this->withdrawals_model->get_withdrawal_loading_information($withdrawal_id);

		$loading_information_data = array(
			"loading_area_id" => $loading_area_id,
			"handling_method" => $handling_method,
			"est_date_time" => $est_date." ".$est_time
		);
		$this->withdrawals_model->update_withdrawal_loading_information($loading[0]["id"], $loading_information_data);

		$this->response["status"] = true;
		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function complete_loading(){
		$withdrawal_id = $this->input->post('withdrawal_id');
		$workers_string = $this->input->post('workers');
		$equipments_string = $this->input->post('equipments');
		$workers = json_decode($workers_string, true);
		$equipments = json_decode($equipments_string, true);
		$date_start = $this->input->post('date_start');
		$time_start = $this->input->post('time_start');
		$date_end = $this->input->post('date_end');
		$time_end = $this->input->post('time_end');
		$duration = $this->input->post('duration');

		foreach ($workers as $key => $value) {
			$name = strtolower($value["name"]);
			$designation = strtolower($value["designation"]);
			$worker_type = $this->withdrawals_model->get_worker_type("WHERE name = '{$designation}'");
			$worker_type_id = 0;
			if(count($worker_type) == 0){
				$worker_type_data = array(
					"name" => $designation
				);
				$worker_type_id = $this->withdrawals_model->create_worker_type($worker_type_data);
			}else{
				$worker_type_id = $worker_type[0]["id"];
			}

			$worker_list_data = array(
				"record_type" => 2,
				"record_log_id" => $withdrawal_id,
				"name" => $name,
				"worker_type" => $worker_type_id
			);
			$this->withdrawals_model->create_worker_list($worker_list_data);
		}

		foreach ($equipments as $key => $value) {
			$name = strtolower($value["equipment"]);
			$equipment_data = array(
				"record_type" => 2,
				"record_log_id" => $withdrawal_id,
				"name" => $name
			);
			$this->withdrawals_model->create_equipment_list($equipment_data);
		}


		$loading = $this->withdrawals_model->get_withdrawal_loading_information($withdrawal_id);

		$loading_information_data = array(
			"datetime_start" => $date_start." ".$time_start,
			"datetime_end" => $date_end." ".$time_end,
			"duration" => $duration
		);
		$this->withdrawals_model->update_withdrawal_loading_information($loading[0]["id"], $loading_information_data);


		$data_update = array(
			"status" => 4
		);
		$this->withdrawals_model->update_withdrawal_log($withdrawal_id, $data_update);

		$this->response["status"] = true;
		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function create_cdr_pdf(){
		$html = $this->input->post("html");
		$withdrawal_log_id = $this->input->post("withdrawal_id");

		$dompdf = new Dompdf();
		$dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 20; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }

        $file_name = $randomString.'.pdf';
        $dir_path = UPLOAD_DOCUMENT_PATH.$file_name;
        $url_path = BASEURL . "uploads/documents/" . $file_name;

        file_put_contents($dir_path, $output);

		$arr_response["path"] = UPLOAD_DOCUMENT_PATH.$file_name;
		$arr_response["location"] = BASEURL . "uploads/documents/" . $file_name;

		$doc_data = array(
			"withdrawal_log_id" => $withdrawal_log_id,
			"document_name" => 'CDR-FILE',
			"document_path" => $url_path,
			"date_added" => date("Y-m-d H:i:s")
		);
		$this->withdrawals_model->create_documents($doc_data);

		$this->response["status"] = true;
		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	public function complete_cdr(){
		
		$withdrawal_id = $this->input->post('withdrawal_id');
		$save_data_string = $this->input->post('save_data');
		$save_data = json_decode($save_data_string, true);

		$cdr_no = $save_data["cdr_no"];
		$gate_pass_ctrl_no = $save_data["gate_pass_ctrl_no"];
		$cargo_swap = $save_data["cargo_swap"];
		$consignee_id = 0;
		$weight_in = $save_data["weight_in"];
		$weight_out = $save_data["weight_out"];
		$seals = $save_data["seals"];

		$gate_pass_swap = 0;
		if($cargo_swap){
			$gate_pass_swap = 1;
			$consignee_id = $save_data["consignee_id"];
		}

		$cdr_data = array(
			"withdrawal_log_id" => $withdrawal_id,
			"cdr_no" => $cdr_no,
			"gate_pass_ctrl_no" => $gate_pass_ctrl_no,
			"gate_pass_swap" => $gate_pass_swap,
			"consignee_id" => $consignee_id,
			"weight_in" => $weight_in,
			"weight_out" => $weight_out,
			"date_completed" => date("Y-m-d g:i:s")
		);


		$this->withdrawals_model->create_cdr_details($cdr_data);

		foreach ($seals as $key => $value) {
			$seal_data = array(
				"withdrawal_log_id" => $withdrawal_id,
				"number" => $value
			);

			$this->withdrawals_model->create_cdr_seal_number($seal_data);
		}

		$data_update = array(
			"status" => 1
		);
		$this->withdrawals_model->update_withdrawal_log($withdrawal_id, $data_update);


		$this->response["status"] = true;
		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	/* END OF CONSIGNEE FUNCTIONS */

	/*  COMPANY FUNCTIONS */

	public function company_record_list(){
		$data['title'] = 'WITHDRAWAL - COMPANY GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("withdrawal-company-list", $data);
		$this->load->view("includes/footer.php");
	}

	public function withdrawal_company_create() {
		$data['title'] = 'WITHDRAWAL - COMPANY GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("withdrawal-company-create", $data);
		$this->load->view("includes/footer.php");
	}

	public function withdrawal_company_ongoing() {
		$data['title'] = 'WITHDRAWAL - COMPANY GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("withdrawal-company-ongoing", $data);
		$this->load->view("includes/footer.php");
	}

	public function withdrawal_company_completed() {
		$data['title'] = 'WITHDRAWAL - COMPANY GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("withdrawal-company-completed", $data);
		$this->load->view("includes/footer.php");
	}

	public function withdrawal_company_edit() {
		$data['title'] = 'WITHDRAWAL - COMPANY GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("withdrawal-company-edit", $data);
		$this->load->view("includes/footer.php");
	}

	//==================================================//
	//================COMPANY FUNCTION==================//
	//==================================================//

	public function company_get_all_withdrawals() {
		$withdrawal_result = $this->withdrawals_model->company_get_all_withdrawals();

		if(count($withdrawal_result) > 0) {
			foreach ($withdrawal_result as $key => $withdrawal) {
				$withdrawal_id = $withdrawal['id'];
				$withdrawal_products = $this->get_company_withdrawal_products($withdrawal_id);
				$withdrawal_result[ $key ]['products'] = $withdrawal_products;
			}

			$response = array(
				'status' => true,
				'message' => '',
				'data' => $withdrawal_result
			);	
		} else {

			$response = array(
				'status' => false,
				'message' => 'No records found.',
				'data' => ''
			);
		}
		
		// if(count($withdrawal_result) > 0) {
			
		// } else {
			
		// }

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function company_get_all_products() {
		$product_result = $this->withdrawals_model->company_get_all_products();

		if(count($product_result) > 0) {
			$response = array(
				'status' => true,
				'message' => '',
				'data' => $product_result
			);
		} else {
			$reponse = array(
				'status' => false,
				'message' => 'No Products found.',
				'data' => ''
			);
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function create_company_withdrawal() {
		// $withdrawal_documents = $this->input->post('documents');

		$withdrawal_log_details = array(
			'withdrawal_number' => $this->input->post('withdrawal_number'),
			'user_id' => $this->input->post('user_id'),
			'type' => $this->input->post('type'),
			'date_created' => date('Y-m-d H:i:s')
		);
		$create_result = $this->withdrawals_model->create_company_withdrawal($withdrawal_log_details);

		$withdrawal_products = $this->input->post('products');
		$this->create_company_withdrawal_product($create_result, $withdrawal_products);

		$withdrawal_notes = $this->input->post('notes');
		if(count($withdrawal_notes) > 0) {
			$this->create_company_notes($create_result, $withdrawal_notes);	
		}

		$withdrawal_documents = $this->input->post('documents');
		if(count($withdrawal_documents) > 0) {
			$this->create_company_documents($create_result, $withdrawal_documents);
		}
		
		$withdrawal_company_details = array(
			'withdrawal_log_id' => $create_result,
			'requestor' => $this->input->post('requestor'),
			'designation' => $this->input->post('designation'),
			'department' => $this->input->post('department'),
			'request_for_withdrawal' => $this->input->post('request_for_withdrawal')
		);
		$company_result = $this->withdrawals_model->create_company_details($withdrawal_company_details);

		$response = array(
			'status' => true,
			'message' => 'Withdrawal record created.',
			'data' => $create_result
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function create_company_withdrawal_product($withdrawal_log_id, $products) {
		foreach ($products as $key => $product) {
			$product['withdrawal_log_id'] = $withdrawal_log_id;
			$product_batches = $product['product_batches'];
			unset($product['product_batches']);
			$product_result = $this->withdrawals_model->create_company_withdrawal_product($product);

			foreach ($product_batches as $key => $batch) {
				$this->create_company_product_batches($product_result, $batch);
			}
		}
	}

	public function create_company_product_batches($withdrawal_product_id, $batch) {
		$batch['withdrawal_product_id'] = $withdrawal_product_id;
		$this->withdrawals_model->create_company_product_batches($batch);
	}

	public function create_company_notes($withdrawal_log_id ,$notes) {
		foreach ($notes as $key => $note) {
			$note['withdrawal_log_id'] = $withdrawal_log_id;
			$note['user_id'] = 1; //TEMPORARY USER.
			$this->withdrawals_model->create_company_withdrawal_note($note);
		}
	}

	public function create_company_documents($withdrawal_log_id, $documents) {
		foreach($documents as $key => $document) {
			$document['withdrawal_log_id'] = $withdrawal_log_id;
			$this->withdrawals_model->create_company_documents($document);
		}
	}

	public function get_company_withdrawal_details() {
		$withdrawal_id = $this->input->get('id');
		$withdrawal_result = $this->withdrawals_model->get_company_withdrawal_details($withdrawal_id);

		$date_created = strtotime($withdrawal_result["date_created"]);
		$date = date('M d, Y', $date_created);
		$time = date('g:i A', $date_created);

		//FORMATS THE DATE TO *Apr 30, 2016 | 7:30 AM*
		$withdrawal_result["display_date"] = $date . " | " . $time;
		$withdrawal_result["products"] = $this->get_company_withdrawal_products($withdrawal_id);
		$withdrawal_result["notes"] = $this->get_company_withdrawal_notes($withdrawal_id);
		$withdrawal_result["documents"] = $this->get_company_withdrawal_documents($withdrawal_id);

		$response = array(
			'status' => true,
			'message' => '',
			'data' => $withdrawal_result
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_company_product_list() {
		$withdrawal_id = $this->input->get('withdrawal_id');
		$product_list = $this->get_company_withdrawal_products($withdrawal_id);

		$response = array(
			'status' => true,
			'message' => '',
			'data' => $product_list
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_company_withdrawal_documents($withdrawal_id) {
		$document_list = $this->withdrawals_model->get_company_withdrawal_documents($withdrawal_id);
		return $document_list;
	}

	public function get_company_withdrawal_products($withdrawal_id) {
		$withdrawal_products = $this->withdrawals_model->get_company_withdrawal_products($withdrawal_id);
		foreach ($withdrawal_products as $key => $product) {
			$withdrawal_products[ $key ]['batches'] = $this->get_product_batches($withdrawal_id);
		}

		return $withdrawal_products;
	}

	public function get_company_withdrawal_products_and_batches() {
		$withdrawal_log_id = $this->input->get('withdrawal_log_id');
		$withdrawal_products = $this->withdrawals_model->get_company_withdrawal_products($withdrawal_log_id);
		foreach ($withdrawal_products as $key => $product) {
			$withdrawal_products[ $key ]['batches'] = $this->get_withdrawal_product_batches($product['id']);
		}

		$response = array(
			"status" => true,
			"message" => "",
			"data" => $withdrawal_products
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_withdrawal_product_batches($withdrawal_product_id) {
		$batches = $this->withdrawals_model->get_withdrawal_product_batches($withdrawal_product_id);

		return $batches;
	}

	public function get_product_batches($withdrawal_id) {
		$batches = $this->withdrawals_model->get_product_batches($withdrawal_id);
		return $batches;
	}

	public function get_company_withdrawal_notes($withdrawal_id) {
		$withdrawal_notes = $this->withdrawals_model->get_company_withdrawal_notes($withdrawal_id);
		return $withdrawal_notes;
	}

	public function get_company_product_batches() {
		$product_id = $this->input->get('product_id');
		$result = $this->withdrawals_model->get_company_product_batches($product_id);
		$response = array(
			"status" => true,
			"message" => "",
			"data" => $result
		);

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function save_document_details() {
		$response = array(
			"status" => false,
			"message" => '',
			"data" => ''
		);

		$document_details = array(
			'withdrawal_log_id' => $this->input->post('withdrawal_log_id'),
			'document_name' => $this->input->post('document_name'),
			'document_path' => $this->input->post('document_path'),
			'date_added' => $this->input->post('date_added')
		);

		$result = $this->withdrawals_model->save_document_details($document_details);
		if($result > 0) {
			$response['status'] = true;
			$response['message'] = 'Document has been recorded.';
			$response['data'] = $result;
		} else {
			$response['message'] = 'Document not recorded.';
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function company_complete_withdrawal() {
		$withdrawal_id = $this->input->post('withdrawal_id');
		$result = $this->withdrawals_model->company_complete_withdrawal($withdrawal_id);

		if($result > 0) {
			$response = array(
				'status' => true,
				'message' => 'Withdrawal completed.',
				'data' => ''
			);
		} else {
			$response = array(
				'status' => false,
				'message' => 'Withdrawal not completed.',
				'data' => ''
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);	
	}

	public function company_edit_record() {
		$error_count = 0;
		$input_status = $this->input->post('input_status');

		$this->form_validation->set_rules('withdrawal_log_id', 'Withdrawal ID', 'trim|required');
		$this->form_validation->set_rules('requestor', 'Requestor', 'trim|required');
		$this->form_validation->set_rules('department', 'Department', 'trim|required');
		$this->form_validation->set_rules('reason', 'Reason for withdrawal', 'trim|required');
		$this->form_validation->set_rules('designation', 'Reason for withdrawal', 'trim|required');

		if ($this->form_validation->run()) {
			$withdrawal_log_id = $this->input->post('withdrawal_log_id');
			$new_products = $this->input->post('new_products');
			$deleted_products = $this->input->post('deleted_products');
			$update_data = array(
				'requestor' => $this->input->post('requestor'),
				'department' => $this->input->post('department'),
				'request_for_withdrawal' => $this->input->post('reason'),
				'designation' => $this->input->post('designation')
			);

			// var_dump($withdrawal_log_id);
			// var_dump($update_data);
			if($input_status == 'changed') {
				// print_r($update_data);
				// print_r($withdrawal_log_id);
				$log_result = $this->withdrawals_model->company_edit_record($withdrawal_log_id, $update_data);
				
				if(!$log_result) {
					$error_count = $error_count + 1;
				}
				// $error_count = $log_result > 0 ? $error_count = $error_count + 0 : $error_count = $error_count + 1; 
				// var_dump($error_count);
			}

			if($new_products != 'null') {
				foreach ($new_products as $key => $product) {
					$product_details = array(
						'withdrawal_log_id' => $withdrawal_log_id,
						'product_id' => $product['product_id'],
						'loading_method' => $product['loading_method'],
						'unit_of_measure_id' => $product['unit_of_measure_id'],
						'qty' => $product['qty'],
						'status' => $product['status']
					);

					$product_result = $this->withdrawals_model->create_withdrawal_product($product_details);
					$product_batches = $product['product_batches'];
					
					foreach ($product_batches as $key => $batch) {
						$batch['withdrawal_product_id'] = $product_result;
						$batch_result = $this->withdrawals_model->create_company_product_batches($batch);
					}
				}
			}
			

			if($error_count == 0) {
				$this->response["status"] = true;
				$this->response["message"] = 'Withdrawal updated.';
				$this->response["data"] = $update_data;
			} else {
				$this->response["status"] = false;
				$this->response["message"] = 'Withdrawal not updated.';
			}
			
		} else {

			$this->response["status"] = false;
			$this->response["message"] = 'Withdrawal not updated.';

		}

		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	//==================================================//
	//==============END COMPANY FUNCTION================//
	//==================================================//

	public function get_all_unit_of_measure() {
		$measure_result = $this->withdrawals_model->get_all_unit_of_measure();
		
		$response = array(
			'status' => true,
			'message' => '',
			'data' => $measure_result
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_all_locations() {
		$location_result = $this->withdrawals_model->get_all_locations();

		$response = array(
			'status' => true,
			'message' => '',
			'data' => $location_result
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_current_date_time() {
		$this->load->helper('date');
		$datestring = "%d-%M-%Y %h:%i %a";
		$datestring2 = "%Y-%m-%d %h:%i";
		$time = time();
		$current_date_time = array(
			"to_display" => mdate($datestring, $time),
			"to_record" => mdate($datestring2, $time)
		);
		
		// echo $current_date_time;

		$response = array(
			"status" => true,
			"mesage" => "",
			"data" => $current_date_time
		);

		header("Content-Type: application/json");
		echo json_encode($response);
	}

	public function get_all_products() {
		$products = $this->withdrawals_model->get_all_products();

		$response = array(
			"status" => true,
			"mesage" => "",
			"data" => $products
		);
		header("Content-Type: application/json");
		echo json_encode($response);
	}

	public function add_note() {
		$withdrawal_number = $this->input->post('id');
		$note = array(
			'subject' => $this->input->post('subject'),
			'note' => $this->input->post('note'),
			'date_created' => $this->input->post('date_created'),
			'withdrawal_log_id' => $this->input->post('withdrawal_log_id'),
			'user_id' => $this->input->post('user_id')
		);

		$note_result = $this->withdrawals_model->create_company_withdrawal_note($note);
		if($note_result > 0) {
			$this->response["status"] = true;
			$this->response["message"] = 'Note has been added.';
		} else {
			$this->response["status"] = false;
			$this->response["message"] = 'Note not added.';
		}

		header("Content-Type: application/json");
		echo json_encode($this->response);
	}

	/* END OF COMPANY FUNCTIONS */

}

/* End of file withdrawals.php */
/* Location: ./application/controllers/withdrawals.php */