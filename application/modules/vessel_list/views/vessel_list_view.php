<div class="main">
			<div class="semi-main">
				<div class="mod-select padding-left-3 padding-top-60">			
					<div class="select f-left width-350px ">
						<select>
							<option value="Displaying All Vessels"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>						
				</div>
				<div class="f-right margin-top-10">	
					<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn modal-trigger" modal-target="add-new-vessel">Add New Vessel</button>				
					<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn dropdown-btn">View Filter / Search</button>					
				</div>
				<div class="clear"></div>

				<div class="dropdown-search">
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr>
								<td>Search</td>
								<td>
									<div class="select medium">
										<select>
											<option value="items">Items</option>
										</select>
									</div>
								</td>
								<td class="width-75percent">
									<input type="text" class="t-small width-100per" />
								</td>
							</tr>
							<tr>
								<td>Filter By:</td>
								<td>
									<div class="select medium">
										<select>
											<option value="calender">Calendar Here</option>
										</select>
									</div>
								</td>
								<td class="search-action">
									<div class="f-left">
										<div class="select semi-medium">
											<select>
												<option value="origin">Origin</option>
											</select>
										</div>
										<div class="select semi-medium">
											<select>
												<option value="status">Status</option>
											</select>
										</div>
									</div>
									<div class="f-right ">
										<p class="display-inline-mid">Sort By:</p>
										<div class="select semi-medium display-inline-mid">
											<select>
												<option value="withdrawal No.">Withdrawal No.</option>
											</select>
										</div>
										<div class="select semi-medium display-inline-mid">
											<select>
												<option value="ascending">Ascending</option>
												<option value="descending">Descending</option>
											</select>
										</div>
									</div>
									<div class="clear"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10">
						<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
						<p class="display-inline-mid font-15 font-bold">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-400 f-right margin-top-10 margin-right-30">Search</button>
					<div class="clear"></div>

				</div>

				

				<div class="first-container width-100per margin-top-20 margin-bottom-20">
					<div class="width-100percent text-left font-0" id="vesselContainer">
						

					</div>
				</div>

			</div>
		</div>

		<!--start modal add vessel -->
		<div class="modal-container" modal-id="add-new-vessel">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Add Vessel</h4>				
					<div class="modal-close close-me"></div>
				</div>
				<form id="AddVesselForm">
					<!-- content -->
					<div class="modal-content text-left add-vessel">	
						<div class="text-center margin-auto position-rel default-cursor " id="uploadHolder" style="width:130px; height: 130px;">

						</div>
						
						<div class="width-100per margin-top-50">
							<p class="font-14 font-400 no-margin-all display-inline-mid width-125px" id="vesselname">Vessel Name:</p>
							<input type="text" class="width-300px  display-inline-mid" datavalid="required" labelinput="Vessel Name" name ="name" id="vesselName">
							<p class="font-14 font-400 no-margin-all display-inline-mid width-125px margin-top-10">Alias:</p>
							<input type="text" class="width-300px  display-inline-mid margin-top-10" labelinput="Vessel Alias" name="alias" id="vesselalias">
							<p class="font-14 font-400 no-margin-all display-inline-mid width-125px margin-top-10">Group:</p>
							<input type="text" class="width-300px  display-inline-mid margin-top-10" labelinput="Group" name="group" id="vesselgroup">
						</div>
					</div>
				
					<div class="modal-footer text-right">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<button type="button" id="add-vessel" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Add Vessel</button>
					</div>
				</form>

			</div>	
		</div>
		<!--end of modal add vessel-->


			
		<!--start of modal edit vessel -->
		<div class="modal-container" modal-id="edit-vessel">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Edit Vesel</h4>				
					<div class="modal-close close-me"></div>
				</div>
				<form id="EditVesselForm">
				<!-- content -->
				<div class="modal-content text-left">	
					<div class="text-center margin-auto position-rel default-cursor " id="edituploadHolder" style="width:130px; height: 130px;">

					</div>
					
					<div class="width-100per margin-top-50">
						<p class="font-20 font-500 no-margin-all display-inline-mid width-125px">Vessel Name:</p>
						<input type="text" class="width-300px  display-inline-mid"  id="editVesselName" datavalid="required">
						<p class="font-20 font-500 no-margin-all display-inline-mid width-125px margin-top-10">Alias:</p>
						<input type="text" class="width-300px  display-inline-mid margin-top-10" id="editVesselAlias">
						<p class="font-20 font-500 no-margin-all display-inline-mid width-125px margin-top-10">Group:</p>
						<input type="text" class="width-300px  display-inline-mid margin-top-10" labelinput="Group" name="groupedit" id="editVesselGroup">

					</div>
				</div>
				</form>
			
				<div class="  modal-footer margin-bottom-10">
					<i class="fa fa-trash-o font-22 icon-hover default-cursor f-left padding-top-5" id="delete-vessel"></i>
					<div class="f-right width-200px">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid" id="update-vessel">Save Changes</button>
					</div>
					<div class="clear"></div>
				</div>

			</div>	
		</div>
		<!--end of modal edit vessel -->
