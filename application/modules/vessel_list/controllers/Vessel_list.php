<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vessel_list extends MX_Controller {

	public function index()
	{
		$user_session = $this->session->userdata('apollo_user');
		if($user_session === null){
			redirect(BASEURL.'login');
		}else{
			
			$this->home();
		}
		
		// if(IS_DEVELOPMENT === true)
		// {
		// 	$user_development = array(
		// 		"id" => 10000,
		// 		"email" => "apollo@email.com",
		// 		"username" => "apollo",
		// 		"password" => "62cc2d8b4bf2d8728120d052163a77df",
		// 		"company_id" => 1,
		// 		"role_id" => 1,
		// 		"account_type" => 1,
		// 		"first_name" => "apollo",
		// 		"last_name" => "developer",
		// 		"middle_name" => ""
		// 	);

		// 	$this->session->set_userdata('apollo_user', $user_development);

			// redirect(BASEURL . 'vessel_list/');
			// $this->home();
		// }
		// else
		// {
		// 	/*LOGIN PROCESS*/
		// }

			


	}

	public function home()
	{
		$data["title"] = "Vessel List";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("vessel_list/vessel_list_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function upload($paramReturn = false) 
	{
		$response = array();
		$arr_response = array();

		$status = false;
		$message = "Fail";

		if(isset($_FILES["attachment"])){
			$name = $_FILES["attachment"]["name"];
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 20; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    
		    $file_name = $randomString . "." .$ext;

		    move_uploaded_file($_FILES["attachment"]["tmp_name"], UPLOAD_IMAGE_PATH.$file_name);

		    $arr_response["path"] = UPLOAD_IMAGE_PATH.$file_name;
		    $arr_response["location"] = BASEURL . "uploads/images/" . $file_name;
		    $status = true;
		    $message = "Succes";
		}

		if($paramReturn === true){
			return array(
				"status" => true,
				"data" => $arr_response
			);
		}else{
			$response["status"] = $status;
			$response["message"] = $message;
			$response["data"] = $arr_response;
			echo json_encode($response);
		}
	}

	public function get_all()
	{
		$this->load->model("vessel_model");
		$vessels = array();

		$vessels = $this->vessel_model->gel_all();

		foreach ($vessels as $key => $value) {

			$vessels[$key]["date_formatted"] = date("M. d, Y", strtotime($value["date_created"]));
		}

		$response = array(
			"status" => true,
			"message" => "",
			"data" => $vessels
		);

		header("Content-Type:application/json;");
		echo json_encode($response);

	}

	public function add() 
	{
		$image_response = $this->upload(true);

		/*OPTIONAL*/
		$image = $this->input->post('image');
		$alias = $this->input->post('alias');
		$group = $this->input->post('group');

		if($image_response["status"]){
			$image = $image_response["data"]["location"];
		}

		if(strlen($image) == 0 && $image == null){
			$image = DEFAULT_PROD_IMG;
		}
		
		/*REQUIRED*/
		$name = $this->input->post('name');

		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[160]|regex_match[/[A-Za-z0-9\'\"]+$/]');
		
		if($this->form_validation->run())
		{
			$this->load->model('vessel_model');

			$vessel_data = array(
				'image' => $image,
				'alias' => $alias,
				'name' => $name,
				'vessel_group' => $group
			);

			$product_id = $this->vessel_model->add_vessel($vessel_data);

			$response["status"] = true;
			$response["message"] = "success";
			$response["data"] = array();
			header("Content-Type:application/json;");
			echo json_encode($response);
		}
		else
		{
			$response["status"] = false;
			$response["message"] = "failed";
			$response["data"] = $this->form_validation->error_array();
			header("Content-Type:application/json;");
			echo json_encode($response);
		}
	}

	public function delete() {
		$rows_changed = 0;
		$vessel_id = $this->input->post('id');
		
		$this->load->model('vessel_model');	
		echo $this->vessel_model->delete($vessel_id);
	}
	

	public function update() {
		$rows_changed = 0;
		$vessel_id = $this->input->post('id');
		/*OPTIONAL*/
		$image = $this->input->post('image');
		$alias = $this->input->post('alias');
		$group = $this->input->post('groupedit');
		/*REQUIRED*/
		$name = $this->input->post('name');

		$this->form_validation->set_rules('name', 'Name', 'required|trim|max_length[160]|regex_match[/[A-Za-z0-9\'\"]+$/]');
		
		if($this->form_validation->run())
		{
			$this->load->model('vessel_model');

			$vessel_data = array(
				'image' => $image,
				'alias' => $alias,
				'name' => $name,
				'vessel_group' => $group
			);

			$this->vessel_model->update($vessel_id,$vessel_data);
		}
		else
		{
			$response["status"] = false;
			$response["message"] = "failed";
			$response["data"] = $this->form_validation->error_array();
			header("Content-Type:application/json;");
			echo json_encode($response);
		}
	}


}

/* End of file Vessel_list.php */
/* Location: ./application/modules/vessel_list/controllers/Vessel_list.php */