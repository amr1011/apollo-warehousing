<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vessel_model extends CI_Model 
{	
	
	public function gel_all()
	{
		$sql = "SELECT *
		FROM vessel_list WHERE is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function add_vessel($vessel) 
	{
		$this->db->insert('vessel_list', $vessel);
		return $this->db->insert_id();
	}


	public function get_single_vessel($id)
	{
		$sql = "SELECT *
		FROM vessel_list WHERE  id = $id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_vessel_name()
	{
		$sql = "SELECT id, name FROM vessel_list WHERE is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function delete($vessel_id) {
		// echo $product_id;
		$delete_details = array(
			"is_deleted" => 1
		);
		$this->db->where("id", $vessel_id);
		$this->db->update("vessel_list", $delete_details);
		// print_r($this->db->last_query());
		return $this->db->affected_rows();
	}

	public function update($vessel_id, $vessel_data) {
		$this->db->where("id", $vessel_id);
		$this->db->update("vessel_list", $vessel_data);
		return $this->db->affected_rows();
	}

}