<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Storage_model extends CI_Model {

	public function add_storage($storage_details)
	{
		$this->db->insert('storage', $storage_details);
		return $this->db->insert_id();
	}

	public function add_location($arr_data)
	{
		$this->db->insert("location", $arr_data);
		return $this->db->insert_id();
	}

	public function get_storages($select_query, $query_conditions)
	{
		// print_r($select_query . $query_conditions);
		$sql = $select_query . $query_conditions;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_location($storage_id)
	{
		$sql = "SELECT * FROM location WHERE storage_id = " . $storage_id . " AND is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function update_location($query, $arr_data)
	{
		$this->db->where($query);
		$this->db->update("location", $arr_data);
		return $this->db->affected_rows();
	}

	public function add_storage_hierarchy($data){
		$this->db->insert("storage_hierarchy", $data);
		return $this->db->insert_id();
	}

	public function get_hierarchy($and_where = " "){
		$sql = "SELECT * FROM storage_hierarchy WHERE is_deleted = 0  ".$and_where;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function add($storage_details) {
		$this->db->insert('storage', $storage_details);
		return $this->db->insert_id();
	}

	public function get_storage_type() {
		$sql = "SELECT * FROM storage_type";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function update_storage_hierarchy($id, $data){
		$this->db->where("id", $id);
		$this->db->update("storage_hierarchy", $data);
	}

	public function get_all_storages() {
		$sql = "SELECT s.id, s.name, s.is_active, s.capacity_measurement, s.high_setpoint, s.low_setpoint,
		s.company_id, s.address, s.storage_type, s.storage_id,
		s.image, s.description, s.capacity, st.name as type_of_storage,
		u.name as measure_name, u.id as measurement_id
		FROM storage s
		LEFT JOIN storage_type st ON st.id = s.storage_type
		LEFT JOIN unit_of_measures u ON u.id = s.measurement_id
		WHERE s.is_active = 1
		ORDER BY s.id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function edit($storage_id, $storage_details) {
		$this->db->where('id', $storage_id);
		$query = $this->db->update("storage", $storage_details);
		return $this->db->affected_rows();
	}

	public function get_all_unit_of_measures() {
		$sql = "SELECT * FROM unit_of_measures";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function add_storage_category_map($storage_id, $category_type_id) {
		$map_details = array(
			"storage_id" => $storage_id,
			"category_type_id" => $category_type_id
		);
		$this->db->insert('storage_category_map', $map_details);
	}

	public function delete_storage_category_map($storage_id) {
		$this->db->where("storage_id", $storage_id);
		$this->db->delete("storage_category_map");
	}

	public function get_all_storage_category($storage_id) {
		$sql = "SELECT c.id AS category_id, c.name, m.id AS map_id
		FROM storage_category_map m
		LEFT JOIN category_type c
		ON m.category_type_id = c.id
		WHERE m.storage_id = $storage_id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function delete($storage_id) {
		$update_data = array("is_active" => 0);

		$this->db->where('id', $storage_id);
		$this->db->update("storage", $update_data);
		return $this->db->affected_rows();
	}

	public function delete_storage_hierarchy_map($storage_id)
	{
		$this->db->where("storage_id", $storage_id);
		$this->db->update("location", array("is_deleted" => 1));
	}

	public function add_document($data)
	{
		$this->db->insert("transfer_documents", $data);
		return $this->db->insert_id();
	}
}
