<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Storages extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->library('MY_Form_validation');
        $this->form_validation->CI =& $this;
		$this->load->model('storage_model');

		$response = array(
			"status" => false,
			"message"=>"",
			"data"=>array()
			);
	}

	public function upload() 
	{
		$response = array();

		$status = false;
		$message = "Fail";
		if(isset($_FILES["attachment"])){
			$name = $_FILES["attachment"]["name"];
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 20; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    
		    $file_name = $randomString . "." .$ext;

		    move_uploaded_file($_FILES["attachment"]["tmp_name"], UPLOAD_IMAGE_PATH.$file_name);

		    $arr_response["path"] = UPLOAD_IMAGE_PATH.$file_name;
		    $arr_response["location"] = BASEURL . "uploads/images/" . $file_name;
		    $status = true;
		    $message = "Succes";
		}

		$response["status"] = $status;
		$response["message"] = $message;
		$response["data"] = $arr_response;
		echo json_encode($response);
	}

	//==================================================//
	//===================ADD STORAGES===================//
	//==================================================//
	public function validate_locations($locations = array())
	{
		if(count($locations) > 0){
			$arr_locations = $locations;
		}else{
			$locations = $this->input->post('locations');
			$arr_locations = json_decode($locations, true);
		}

		if(gettype($arr_locations) == "string"){
			$arr_locations = json_decode($arr_locations, true);
		}
		
		if($arr_locations == null)
		{
			$this->form_validation->set_message("validate_locations", "Location Format is invalid");
			return false;
		}else if(count($arr_locations) > 0){
			foreach ($arr_locations as $key => $value) {
				if(isset($value['locations']) && count($value['locations']) > 0){
					$this->validate_locations($value['locations']);
				}else{
					if( !isset($value['name']) && !isset($value['type']) && !isset($value['is_deleted']) ){
						$this->form_validation->set_message("validate_locations", "Location Format is invalid");
						return false;
					}else{
						return true;
					}
				}
			}
		}
	}

	private function _save_recursive_locations($storage_id, $arr_locations, $parent_id = 0)
	{
		foreach ($arr_locations as $key => $value) {
			$location_data = array(
				"name" => $value['name'],
				"type" => $value['type'],
				"storage_id" => $storage_id,
				"parent_id" => $parent_id
			);
			$id = $this->storage_model->add_location($location_data);

			if(isset($value['locations']) && count($value['locations']) > 0){
				$locations = $value["locations"];
				if(gettype($value["locations"]) == "string"){
					$locations = json_decode($value["locations"], true);
				}			
				$this->_save_recursive_locations($storage_id, $value['locations'], $id);	
			}
		}
	}

	public function edit() {
		$this->form_validation->set_rules('storage_id', 'Storage ID', 'trim|required');
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('address', 'Address', 'required|trim');
		$this->form_validation->set_rules('storage_type', 'Storage Type', 'required|trim|integer');
		$this->form_validation->set_rules('locations', 'Locations', 'required|trim|callback_validate_locations');
		$this->form_validation->set_rules('maximum_storage', 'Maximum Storage', 'trim|required');
		$this->form_validation->set_rules('high_setpoint', 'High Setpoint', 'trim|required');
		$this->form_validation->set_rules('low_setpoint', 'Low Setpoint', 'trim|required');
		$this->form_validation->set_rules('capacity_measure', 'Capacity Measure', 'trim|required');
		$storage_id = $this->input->post('id');

		$locations = $this->input->post('locations');
		$category_details = json_decode($this->input->post("categories"), true);
		$arr_locations = json_decode($locations, true);
		$image = $this->input->post('image');
		$response = array();

		if(!isset($image)){
			$image = DEFAULT_PROD_IMG;
		}

		$hierarchy_id = $this->input->post("hierarchy_id");

		$storage_details = array(
			"storage_id" => $this->input->post('storage_id'),
			"name" => $this->input->post('name'),
			"address" => $this->input->post('address'),
			"storage_type" => $this->input->post('storage_type'),
			"description" => $this->input->post('description'),
			"image" => $image,
			"capacity" => $this->input->post('maximum_storage'),
			"low_setpoint" => $this->input->post('low_setpoint'),
			"high_setpoint" => $this->input->post('high_setpoint'),
			"capacity_measurement" => $this->input->post('capacity_measure'),
			"measurement_id" => $this->input->post('measurement_id'),
			"hierarchy_id" => $hierarchy_id
		);


		if ($this->form_validation->run()) {
			
			$this->storage_model->edit($storage_id, $storage_details);
			$category_details = json_decode($this->input->post("categories"), true);
			$this->storage_model->delete_storage_category_map($storage_id);
			$locations = $this->input->post('locations');
			$arr_locations = json_decode($locations, true);

			foreach ($category_details as $key => $category) {
				$this->storage_model->add_storage_category_map($storage_id, $category["value"]);
			}

			$storage_list = $this->storage_model->get_storages("SELECT * FROM storage WHERE id = ".$storage_id, "");

			$this->storage_model->delete_storage_hierarchy_map($storage_id);
			$this->_save_recursive_locations($storage_id, $arr_locations, 0);

			foreach ($storage_list as $key => $storage) {
				$location = $this->storage_model->get_location($storage["id"]);
				$storage_list[$key]["location"] = $location;

				$hierarchy = array();
				if(count($location) > 0){
					$hierarchy = $this->storage_model->get_hierarchy(" AND id = ".$storage["hierarchy_id"]);
					if(count($hierarchy) > 0){
						$hierarchy = $hierarchy[0];
					}
				}

				$storage_list[$key]["storage_hierarchy"] = $hierarchy;

				$categories = $this->storage_model->get_all_storage_category($storage["id"]);
				$storage_list[$key]["storage_category"] = $categories;
			}

			header("Content-Type: application/json");
			$response["status"] = true;
			$response["message"] = "";
			$response["data"] = $storage_list[0];
			echo json_encode($response);
		} else {
			// echo validation_errors();
			$response["status"] = false;
			$response["message"] = "Error!";
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
	}

	public function add_storages()
	{
		//==============CHECKS IF REQUIREMENTS FOR VALUES===============//
		$this->form_validation->set_rules('storage_id', 'Storage ID', 'trim|required');
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('company_id', 'Company ID', 'required|trim|integer');
		$this->form_validation->set_rules('address', 'Address', 'required|trim');
		$this->form_validation->set_rules('storage_type', 'Storage Type', 'required|trim|integer');
		$this->form_validation->set_rules('locations', 'Locations', 'required|trim|callback_validate_locations');
		$this->form_validation->set_rules('maximum_storage', 'Maximum Storage', 'trim|required');
		$this->form_validation->set_rules('high_setpoint', 'High Setpoint', 'trim|required');
		$this->form_validation->set_rules('low_setpoint', 'Low Setpoint', 'trim|required');
		$this->form_validation->set_rules('capacity_measure', 'Capacity Measure', 'trim|required');

		$locations = $this->input->post('locations');
		$category_details = json_decode($this->input->post("categories"), true);
		$arr_locations = json_decode($locations, true);
		$image = $this->input->post('image');
		$response = array();

		if(!isset($image)){
			$image = DEFAULT_PROD_IMG;
		}

		$hierarchy_id = $this->input->post("hierarchy_id");

		$storage_details = array(
			"storage_id" => $this->input->post('storage_id'),
			"name" => $this->input->post('name'),
			"address" => $this->input->post('address'),
			"company_id" => $this->input->post('company_id'),
			"storage_type" => $this->input->post('storage_type'),
			"description" => $this->input->post('description'),
			"image" => $image,
			"capacity" => $this->input->post('maximum_storage'),
			"low_setpoint" => $this->input->post('low_setpoint'),
			"high_setpoint" => $this->input->post('high_setpoint'),
			"capacity_measurement" => $this->input->post('capacity_measure'),
			"measurement_id" => $this->input->post('measurement_id'),
			"hierarchy_id" => $hierarchy_id
		);

		if ($this->form_validation->run()) {
			if($storage_details["company_id"] > 0)
			{
				
				$storage_id = $this->storage_model->add($storage_details);
				$this->_save_recursive_locations($storage_id, $arr_locations, 0);

				foreach ($category_details as $key => $cat) {
					$this->storage_model->add_storage_category_map($storage_id, $cat["value"]);
				}

				header("Content-Type: application/json;");
				$response["status"] = true;
				$response["message"] = "";
				$response["data"] = array();
				echo json_encode($response);
			}
			else
			{
				$response["message"] = "Please make sure all inputs are valid.";
				echo json_encode($response);
			}
		} else {
			// echo validation_errors();
			$response["status"] = false;
			$response["message"] = "Error!";
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
	}

	public function add($storage_details_params = array()) {
		$storage_details = array(
			// "storage_id" => $this->input->post('storage_id'),
			"name" => $this->input->post('name'),
			"address" => $this->input->post('address'),
			"company_id" => $this->input->post('company_id'),
			"storage_type" => $this->input->post('storage_type'),
			"description" => $this->input->post('description'),
			"image" => $this->input->post('image'),
			"capacity" => $this->input->post('capacity'),
			"measurement_id" => $this->input->post('measurement_id')
		);


		if(count($storage_details_params) > 0){
			$storage_details = $storage_details_params;
		}

		/*$category_details = array(
			"solid" => $this->input->post('solid'),
			"liquid" => $this->input->post('liquid'),
			"gas" => $this->input->post('gas')
		);*/

		$category_details = json_decode($this->input->post("categories"), true);

		// $insert_id = $this->storage_model->add($storage_details);
		
		if($insert_id > 0) {

			foreach ($category_details as $key => $value) {
				$this->storage_model->add_storage_category_map($insert_id, $value["value"]);
			}

			/*foreach ($category_details as $key => $category) {
				if($key == "solid") {
					if($category == "true") {
						$this->storage_model->add_storage_category_map($insert_id, 1);
					}
				}

				if($key == "liquid") {
					if($category == "true") {
						$this->storage_model->add_storage_category_map($insert_id, 2);
					}
				}

				if($key == "gas") {
					if($category == "true") {
						$this->storage_model->add_storage_category_map($insert_id, 3);
					}
				}
			}*/	
		}

		if(count($storage_details_params) > 0){
			return $insert_id;
		}else{
			echo $insert_id;
		}

	}

	//=====VALIDATE THE FIELDS FOR LOCATION ARRAY=======//
	private function _validate_locations_array($location)
	{
		$errors = 0;

		foreach ($location as $row) {
			if(strlen($row) == 0 || $row === null)
			{
				$errors++;
			}
		}
		if($errors == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//==================================================//
	//================END OF ADD STORAGES===============//
	//==================================================//


	// 
	// This is to be add for Location controller 
	// 

	public function add_location($data_param = array())
	{
		$arr_data = array();
		$result = 0;
		$error = "";
		
		// Check if the data_param have a value

		if(count($data_param) > 0)
		{
			$data = $data_param;
		}
		else
		{
			$data = json_decode($this->input->post('data'), true);
		}

		if($data !== null)
		{
			foreach ($data as $key => $value) 
			{
				if( ($data[$key]["name"] != "") AND
					($data[$key]["type"] != "") AND
					($data[$key]["storage_id"] != "") AND
					($data[$key]["parent_id"] != "")
				)
				{
					$arr_data = array(
						"name"=>$data[$key]["name"],
						"type"=>$data[$key]["type"],
						"storage_id"=>$data[$key]["storage_id"],
						"parent_id"=>$data[$key]["parent_id"],
						"is_deleted"=>0
					);

					$result = $this->storage_model->add_location($arr_data);

					$response["status"] = true;
					$response["message"] = "Successfully Added";
					$response["data"] = array("location_id"=>$result);
					echo json_encode($response);
				}
				else
				{
					$error = "<p>Please complete the location information</p>";

					$response["status"] = false;
					$response["message"] = $error;
					$response["data"] = array("error"=>"Please complete the location information");
					echo json_encode($response);

					return false;
				}			
			}
		}
		else
		{
			return false;
		}

	}

	// End of Location
	// Start of Update
	public function update_location()
	{
		$qualifiers = json_decode($this->input->post("qualifiers"), true);
		$data = json_decode($this->input->post("data"), true);

		$this->form_validation->set_rules("qualifiers", "Qualifiers", "callback_update_qualifiers_location_validation");
		$this->form_validation->set_rules("data", "Data", "callback_update_data_location_validation");

		$query = "";
		$arr_data = array();

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
		else
		{
			$new_val = "";
			$counter = 0;
			foreach ($qualifiers as $key => $value) {
				$data_val = $qualifiers[$key]["value"];

				if(gettype($data_val) == "string")
				{
					$new_val = "'".$data_val."'";
				}
				else if(gettype($data_val) == "integer")
				{
					$new_val = intval($data_val);
				}
				else if(gettype($data_val) == "double")
				{
					$new_val = doubleval($data_val);
				}

				if($counter == 0)
				{
					$query = $qualifiers[$key]["field"].$qualifiers[$key]["operation"].$new_val;
				}
				else
				{
					$query .= " AND ".$qualifiers[$key]["field"].$qualifiers[$key]["operation"].$new_val;
				}
			}

			if($data !== null AND json_last_error() === JSON_ERROR_NONE)
			{	
				foreach($data as $key=>$value)
				{
					if(isset($data[$key]["name"]))
					{
						$arr_data["name"] = $data[$key]["name"];
					}
					if(isset($data[$key]["type"]))
					{
						$arr_data["type"] = $data[$key]["type"];
					}
					if(isset($data[$key]["storage_id"]))
					{
						$arr_data["storage_id"] = $data[$key]["storage_id"];
					}
					if(isset($data[$key]["parent_id"]))
					{
						$arr_data["parent_id"] = $data[$key]["parent_id"];
					}
					if(isset($data[$key]["is_deleted"]))
					{
						$arr_data["is_deleted"] = $data[$key]["is_deleted"];
					}
				}
			}

			$result = $this->storage_model->update_location($query, $arr_data);

			$response["status"] = true;
			$response["message"] = "Successfully Updated";
			$response["data"] = array("affected_rows"=>$result);
			echo json_encode($response);
		}
	}

	public function update_qualifiers_location_validation()
	{
		$qualifiers = json_decode($this->input->post("qualifiers"), true);

		if($qualifiers !== null AND json_last_error() === JSON_ERROR_NONE)
		{
			foreach ($qualifiers as $key => $value) {

				if($qualifiers[$key]["field"] == "")
				{
					$this->form_validation->set_message("update_location_validation", "Field is Required");
					return false;
				}

				if($qualifiers[$key]["operation"] == "")
				{
					$this->form_validation->set_message("update_location_validation", "Operation is Required");
					return false;
				}

				if($qualifiers[$key]["value"] == "")
				{
					$this->form_validation->set_message("update_location_validation", "Value is Required");
					return false;
				}
			}
		}
	}

	public function update_data_location_validation()
	{
		$data = json_decode($this->input->post("data"), true);

		if($data !== null AND json_last_error() === JSON_ERROR_NONE)
		{
			foreach ($data as $key => $value) {

				if(isset($data[$key]["name"]) AND trim($data[$key]["name"]) == "")
				{
					$this->form_validation->set_message("update_data_location_validation", "Location Name must not empty");
					return false;
				}

				if(isset($data[$key]["type"]) AND trim($data[$key]["type"]) == "")
				{
					$this->form_validation->set_message("update_data_location_validation", "Location Type must not empty");
					return false;
				}

				if(isset($data[$key]["storage_id"]) AND trim($data[$key]["storage_id"]) == "")
				{
					$this->form_validation->set_message("update_data_location_validation", "Location Storage ID must not empty");
					return false;
				}

				if(isset($data[$key]["parent_id"]) AND trim($data[$key]["parent_id"]) == "")
				{
					$this->form_validation->set_message("update_data_location_validation", "Location Parent ID must not empty");
					return false;
				}

			}
		}
	}

	// end of Update

	//==================================================//
	//===================GET STORAGES===================//
	//==================================================//
	public function get_storages()
	{
		$this->form_validation->set_rules('id', 'Storage ID', 'trim|integer');
		$this->form_validation->set_rules('name', 'Name', 'trim');
		$this->form_validation->set_rules('offset', 'Offset', 'trim|integer');
		$this->form_validation->set_rules('limit', 'Limit', 'trim|integer');
		$this->form_validation->set_rules('return_complete', 'Return Complete', 'trim|integer');

		if ($this->form_validation->run()) {
			if(!empty($this->input->post('return_complete')) && $this->input->post('return_complete') === null) {
				$return_complete = 0;
			} else {
				$return_complete = $this->input->post('return_complete');
			}

			if($return_complete == 0) {
				$parameters = array(
					'id' => ($this->input->post('id') !== null) ? $this->input->post('id') : false,
					'name' => ($this->input->post('name') !== null) ? $this->input->post('name') : false,
					'offset' => ($this->input->post('offset') !== null) ? $this->input->post('offset') : false,
					'limit' => ($this->input->post('limit') !== null) ? $this->input->post('limit') : false
				);	
			} else {
				$parameters = array(
					's.id' => ($this->input->post('id') !== null) ? $this->input->post('id') : false,
					's.name' => ($this->input->post('name') !== null) ? $this->input->post('name') : false,
					'offset' => ($this->input->post('offset') !== null) ? $this->input->post('offset') : false,
					'limit' => ($this->input->post('limit') !== null) ? $this->input->post('limit') : false
				);
			}

			

			//SETS THE RETURN COMPLETE VALUE TO A VARIABLE AND UNSETS THE ONE IN THE ARRAY.
		

			//REMOVES THE EMPTY ROW FROM THE ARRAY.
			foreach ($parameters as $key => $parameter) {
				if(empty($parameter) || $parameter === null) {
					if($key == "return_complete") {
						$parameters["return_complete"] = 0;
					} else {
						unset($parameters[$key]);
					}
				}
			}

			$query_select = $this->_build_query_select($return_complete);
			$query_conditions = $this->_build_query_conditions($parameters);
			$storages_result = $this->storage_model->get_storages($query_select, $query_conditions);
			$locations = $this->storage_model->get_location($parameters["s.id"]);
			$hierarchy_locations = array();

			foreach ($locations as $key => $location) {
				# code...
			}


			// print_r($storages_result);
		} else {
			$reponse["message"] = "Error!";
			$reponse["data"] = $this->form_validation->error_array();
			echo json_encode($response);
			// echo validation_errors();
		}
	}

	public function get_all_storages() {
		$storage_list = $this->storage_model->get_all_storages();

		foreach ($storage_list as $key => $storage) {
			$location = $this->storage_model->get_location($storage["id"]);
			$storage_list[$key]["location"] = $location;

			$hierarchy = array();
			if(count($location) > 0){
				$hierarchy = $this->storage_model->get_hierarchy(" AND id = ".$storage["hierarchy_id"]);
				if(count($hierarchy) > 0){
					$hierarchy = $hierarchy[0];
				}
			}

			$storage_list[$key]["storage_hierarchy"] = $hierarchy;

			$categories = $this->storage_model->get_all_storage_category($storage["id"]);
			$storage_list[$key]["storage_category"] = $categories;
		}

		$result = array(
			"status" => true,
			"message" => "",
			"data" => $storage_list
		);

		header("Content-Type: application/json");
		echo json_encode($storage_list);
	}

	public function get_storage_type() {
		$storage_types = $this->storage_model->get_storage_type();

		$result = array(
			"status" => true,
			"message" => "",
			"data" => $storage_types
		);

		header("Content-Type: application/json");
		echo json_encode($result);
	}

	public function get_all_unit_of_measures() {
		$unit_of_measures = $this->storage_model->get_all_unit_of_measures();

		$result = array(
			"status" => true,
			"message" => "",
			"data" => $unit_of_measures
		);

		header("Content-Type: application/json");
		echo json_encode($result);
	}

	private function _build_query_conditions($parameters)
	{
		$counter = 0;
		$query_conditions = "";
		foreach ($parameters as $key => $parameter) {
			if($counter == 0)
			{
				if($key != "offset" 
					&& $key != "limit" 
					&& !empty($parameter) 
					&& $parameter !== null
					&& $parameter != "return_complete")
				{
					$query_conditions .= " WHERE " . $key . "=" . $parameter;	
				}		
			}
			else
			{
				if($key != "offset" 
					&& $key != "limit" 
					&& !empty($parameter) 
					&& $parameter !== null
					&& $parameter != "return_complete")
				{
					$query_conditions .= " AND " . $key . "=" . $parameter;
				}
			}
			if($key == "offset") {
				$query_conditions .= " OFFSET " . $parameter;
			}
			if($key == "limit") {
				$query_conditions .= " LIMIT " . $parameter;
			}
			$counter++;
		}
		// print_r($get_query);
		return $query_conditions;
	}

	private function _build_query_select($return_complete)
	{
		$select_query = "";
		if($return_complete == 0) {
			$select_query .= "SELECT * FROM storage";
		} else {
			$select_query .= "SELECT *
			FROM storage s
			LEFT JOIN location l ON l.storage_id = s.id";
		}
		return $select_query;
	}

	public function get_all_storage_category() {
		$storage_id = $this->input->get('storage_id');
		$results = $this->storage_model->get_all_storage_category($storage_id);
		header("Content-Type : application/json");
		echo json_encode($results);
	}
	//==================================================//
	//==============END OF GET STORAGES=================//
	//==================================================//

	public function hierarchy_data_validation()
	{
		$hierarchy = $this->input->post("hierarchy");
		$hierarchy = json_decode($hierarchy);
		if($hierarchy == null AND json_last_error() === JSON_ERROR_NONE){
			$this->form_validation->set_message("hierarchy_data_validation", "Hierarchy has invalid format");
			return false;
		}else{
			return true;
		}
	}

	public function add_storage_hierarchy()
	{
		$name = $this->input->post("name");
		$hierarchy = $this->input->post("hierarchy");
		$hierarchy = json_decode($hierarchy, true);

		$response = array(
			"status" => false,
			"message" => "",
			"data" => array()
		);

		$this->form_validation->set_rules("name", "Hierarchy Name", "required|trim");
		//$this->form_validation->set_rules("data", "Hierarchy Data", "callback_hierarchy_data_validation");
		
		if($this->form_validation->run()) {
			$this->load->model('storage_model');
			$parent_id = 0;

			foreach ($hierarchy as $key => $value) {
				$data = array(
					"main_name" => $name,
					"hierarchy_name" => $value,
					"parent_id" => $parent_id,
					"date_created" => date("Y-m-d H:i:s")
				);
				$hierarchy_id = $this->storage_model->add_storage_hierarchy($data);
				$parent_id = $hierarchy_id;
			}

			$response["status"] = true;
		}

		header("Content-type: application/json");
		echo json_encode($response);
	}

	private function _get_children_hierarchy($basedata, $data, $parent_id)
	{
		$response = array();
		foreach ($basedata as $key => $data) {
			if($parent_id == $data["parent_id"]){
				$children = array();
				$children = $this->_get_children_hierarchy($basedata, $data, $data['id']);
				$data["children"] = $children;
				$response[] = $data;
			}
		}

		return $response;


	}

	public function get_hierarchy()
	{
		$arr_hierarchy = array();
		$arr_parent_hierarchy = array();
		$hierarchy = $this->storage_model->get_hierarchy();

		foreach ($hierarchy as $key => $hier) {
			if($hier["parent_id"] == 0){
				$hier["children"] = $this->_get_children_hierarchy($hierarchy, $hier, $hier['id']);
				$arr_hierarchy[] = $hier;
			}
		}

		$response = array(
			"status" => true,
			"message" => "",
			"data" => $arr_hierarchy
		);

		header("Content-type: application/json");
		echo json_encode($response);
	}

	public function update_storage_hierarchy(){
		$id = $this->input->post("hierarchy_id");
		$name = $this->input->post("name");
		$hierarchy = $this->input->post("hierarchy");
		$hierarchy = json_decode($hierarchy, true);
		$deleted_hierarchy = $this->input->post("deleted_hierarchy");
		$deleted_hierarchy = json_decode($deleted_hierarchy, true);

		$response = array(
			"status" => false,
			"message" => "",
			"data" => array()
		);

		$this->form_validation->set_rules("name", "Hierarchy Name", "required|trim");
		$this->form_validation->set_rules("data", "Hierarchy Data", "callback_hierarchy_data_validation");

		if($this->form_validation->run()) {
			$parent_id = 0;

			foreach ($deleted_hierarchy as $key => $value) {
				$data = array(
					"main_name" => $name,
					"is_deleted" => 1
				);
				$hierarchy_id = $this->storage_model->update_storage_hierarchy(intval($value['id']), $data);
			}

			foreach ($hierarchy as $key => $value) {
				if(isset($value['id'])){
					if(intval($value['id']) == 0){
						$data = array(
							"main_name" => $name,
							"hierarchy_name" => $value['name'],
							"parent_id" => $parent_id,
							"date_created" => date("Y-m-d H:i:s")
						);
						$hierarchy_id = $this->storage_model->add_storage_hierarchy($data);
						$parent_id = $hierarchy_id;
					}else{
						$data = array(
							"main_name" => $name,
							"hierarchy_name" => $value['name']
						);
						$hierarchy_id = $this->storage_model->update_storage_hierarchy(intval($value['id']), $data);
						$parent_id = $hierarchy_id;
					}
				}else if(isset($value['parent_id'])){
					if($value['parent_id'] == "refer"){
						$data = array(
							"main_name" => $name,
							"hierarchy_name" => $value['name'],
							"parent_id" => $parent_id,
							"date_created" => date("Y-m-d H:i:s")
						);
						$hierarchy_id = $this->storage_model->add_storage_hierarchy($data);
						$parent_id = $hierarchy_id;
					}else if(intval($value['parent_id']) > 0){
						$data = array(
							"main_name" => $name,
							"hierarchy_name" => $value['name'],
							"parent_id" => intval($value['parent_id']),
							"date_created" => date("Y-m-d H:i:s")
						);
						$hierarchy_id = $this->storage_model->add_storage_hierarchy($data);
						$parent_id = $hierarchy_id;
					}
				}

				
			}

			$response["status"] = true;
		}

		header("Content-type: application/json");
		echo json_encode($response);
	}

	private function _delete_children_hierarchy($basedata, $data, $parent_id)
	{
		foreach ($basedata as $key => $d) {
			if($parent_id == $d["parent_id"]){
				$this->_delete_children_hierarchy($basedata, $d, $d['id']);
				$data = array(
					"is_deleted" => 1
				);
				$this->storage_model->update_storage_hierarchy(intval($d['id']), $data);
			}
		}
	}

	public function delete_storage_hierarchy()
	{
		$main_hierarchy_id = $this->input->post('main_hierarchy');
		$this->load->model("storage_model");
		$response = array(
			"status" => true,
			"message" => "",
			"data" => array()
		);

		$arr_hierarchy = array();
		$arr_parent_hierarchy = array();
		$hierarchy = $this->storage_model->get_hierarchy();

		foreach ($hierarchy as $key => $hier) {
			if( $hier["id"] == $main_hierarchy_id){
				$data = array(
					"is_deleted" => 1
				);
				$this->storage_model->update_storage_hierarchy(intval($hier['id']), $data);
				$this->_delete_children_hierarchy($hierarchy, $hier, $hier['id']);
			}
		}


		header("Content-type: application/json");
		echo json_encode($response);
	}

	public function delete() {
		$storage_id = $this->input->post('storage_id');
		$result = $this->storage_model->delete($storage_id);
		echo $result;
	}

}

/* End of file Storages.php */
/* Location: ./application/modules/storages/controllers/Storages.php */