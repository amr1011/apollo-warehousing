<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends MX_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->library('MY_Form_validation');
		$this->load->library('Unit_converter');
        $this->form_validation->CI =& $this;
		$this->load->model('inventory_model');

		// $value = $this->unit_converter->convert_measurement("bag", "kg", 20);
 		// print_r($value);
		// exit();

		$response = array(
			"status" => false,
			"message" => "message here",
			"data" => null
		);


	}

	public function transform_to_receiving_number($receiving_num = 1)
	{
		$length = 10;
		$return = "0";
		$len = strlen($receiving_num);
		$int_val = intval($receiving_num);
		$return_data = $int_val;

		$len = $length - $len;

		for($i = 1; $i < $len; $i++){
			$return = "0".$return;
		}

		$return = $return.$return_data;

		return $return;
	}

	public function get_all_products_inventory()
	{
		$get_products = $this->inventory_model->get_all_products_inventory();
		$arr_stored_data = array();

		foreach ($get_products as $key => $value) {
			$get_inventory_details = $this->inventory_model->get_product_inventory_details($value["product_id"]);
			$total = 0;
			$sum = 0;

			// var_dump($get_inventory_details);
			// exit();

			foreach ($get_inventory_details as $detailskey => $detailsvalue) {

				$sum = $this->unit_converter->convert_measurement($detailsvalue["unit_measure_name"], "kg", $detailsvalue["quantity_received"]);

				if($detailsvalue["method"] == "add")
				{
					$total += $sum;
				}
				else
				{
					$total = $total - $sum;
				}
			}

			$get_batches = $this->inventory_model->get_all_products_batches_inventory($value["product_id"]);

			$arr_collect_data= array(
				"product_id"=>$value["product_id"],
				"product_name"=>$value["name"],
				"sku"=>$value["sku"],
				"image"=>$value["image"],
				"total_quantity"=>$total,
				"total_batches"=>$get_batches[0]["counted_batches"]
				);
			array_push($arr_stored_data, $arr_collect_data);

			

		}

		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $arr_stored_data;
		echo json_encode($response);

	}

	public function get_product_inventory_records()
	{

		$product_id = $this->input->get('product_id');
		$product_details = $this->get_product_details($product_id);
		$product_batches = $this->get_product_batches($product_id);
		$product_transfers = $this->get_product_transfers($product_id);
		$product_receiving = $this->get_product_receiving($product_id);
		$product_total_qty = $this->get_product_total_inventory($product_id);
		$product_details['total_quantity'] = $product_total_qty;
		$product_inventory = $this->get_product_inventory($product_id);
		$product_withdrawal = $this->get_product_withdrawals($product_id);
		$product_consignee = $this->get_product_consignee($product_id);
		$batch_adjusted = $this->get_batch_adjust_quantity($product_id);
		$batch_return = $this->get_batch_return_quantity($product_id);
		$batch_remove = $this->get_batch_remove_quantity($product_id);
		$batch_update_status = $this->get_batch_update_status($product_id);
		$batch_split = $this->get_batch_split($product_id);
		$batch_repack =  $this->get_batch_repack($product_id);

		if(count($product_transfers) > 0)
		{
			foreach ($product_transfers as $key => $value) {

				$user_data = $this->session->userdata('apollo_user');

				$product_transfers[$key]["user_name"] = $user_data["first_name"]." ".$user_data["last_name"];
			}
		}

		
		if(count($product_receiving) > 0)
		{
			foreach ($product_receiving as $key => $value) {
				$origin = "";
				$user_data = $this->session->userdata('apollo_user');

				$vessel = $this->inventory_model->check_if_vessel($value["receiving_id"]);
				$truck = $this->inventory_model->check_if_truck($value["receiving_id"]);


				// print_r($value["receiving_id"]);
				// exit();

				if(count($vessel) > 0)
				{
					$origin = $vessel[0]["vessel_name"];
				}
				if(count($truck) > 0)
				{
					$origin =$truck[0]["vessel_name"];
				}

				// if($vessel !== null)
				// {
				// 	print_r($vessel);
				// 	exit();
				// 	$origin = $vessel[0]["vessel_name"];
				// }else if($truck !== null){
				// 	print_r($truck);
				// 	exit();
				// 	$origin =$truck[0]["vessel_name"];
				// }

				$product_receiving[$key]["receiving_origin"] = $origin;

				$get_receiving_batches = $this->inventory_model->get_receiving_batches($value["receiving_id"], $value["product_id"]);

				$product_receiving[$key]["batch_info"] = $get_receiving_batches;

				$product_receiving[$key]["user_name"] = $user_data["first_name"]." ".$user_data["last_name"];

			}
		}

		
		foreach ($product_batches as $key => $batch) {
			$days_left_parameters = array(
				'date_received' => $batch['receiving_date_received'],
				'product_shelf_life' => $product_details['product_shelf_life']
			);

			$days_left = $this->calculate_days_remaining($days_left_parameters);
			$product_batches[ $key ]['days_left'] = $days_left;


			if($batch["product_inventory_loading_method"] == "Bags")
			{
				$product_batches[$key]['batch_quantity'] = $this->unit_converter->convert_measurement("bag", $batch["unit_of_measure_name"], $batch["product_inventory_bag"]);
			}

			$sum = 0;

			$sum = $this->unit_converter->convert_measurement($batch["unit_of_measure_name"], "kg", $product_batches[$key]['batch_quantity']);
			$sum = number_format($sum, 2);

			$product_batches[ $key ]['converted_qty_kg'] = $sum;
		}
		
		// BUILDS THE ARRAY FOR THE PRODUCT ACTIVITY.
		$product_activity = array(
			'transfers' => $product_transfers,
			'product_receiving' => $product_receiving,
			'product_withdrawal' => $product_withdrawal,
			'batch_adjusment' =>$batch_adjusted,
			'batch_retun' =>$batch_return,
			"batch_remove"=>$batch_remove,
			'batch_update_status'=>$batch_update_status,
			'batch_split'=>$batch_split,
			'batch_repack'=>$batch_repack
		);
		
		// BUILDS THE ARRAY FOR THE PRODUCT INVENTORY RECORD (MAIN ARRAY).
		$product_invetory_record = array(
			'product' => $product_details,
			'product_batches' => $product_batches,
			'product_activities' => $product_activity,
			'product_inventory' => $product_inventory,
			'consignee_assignments'=>$product_consignee
			
		);

		$response = array(
			'status' => true,
			'message' => '',
			'data' => $product_invetory_record
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_product_details($product_id) 
	{
		$product_details = $this->inventory_model->get_product_details($product_id);
		return $product_details;
	}

	public function get_product_batches($product_id) 
	{
		$product_batches = $this->inventory_model->get_product_batches($product_id);
		// echo "<pre>";
		// print_r($product_batches);
		// echo "</pre>";
		// exit();
		foreach ($product_batches as $key => $value) {
			$product_batch_repack = $this->inventory_model->get_product_batches_repack($value["batch_id"]);

			if(count($product_batch_repack) > 0)
			{
				$product_batches[$key]["repack_batch_qty"] = $product_batch_repack[0]["quantity"];
			}else{
				$product_batches[$key]["repack_batch_qty"] = "";
			}

			$vessel_origin = "";

			if($value["vessel_origin_id"] !== null)
			{
				$vessels_orig= $this->inventory_model->get_vessel_name($value["vessel_origin_id"]);
				$vessel_origin = $vessels_orig[0]["vessel_name"];
				
			}else if($value["truck_origin_id"] !== null){
				$vessels_orig = $this->inventory_model->get_vessel_name($value["truck_origin_id"]);
				$vessel_origin = $vessels_orig[0]["vessel_name"];
			}else{
				$vessel_origin = "";
			}
		
			$product_batches[$key]["vessel_name"] = $vessel_origin;


			
		}
		return $product_batches;
	}

	public function get_product_transfers($product_id) 
	{
		$product_transfers = $this->inventory_model->get_product_transfers($product_id);

		return $product_transfers;
	}

	public function get_product_receiving($product_id) 
	{
		$product_receiving = $this->inventory_model->get_product_receiving($product_id);
		return $product_receiving;
	}

	public function get_product_inventory($product_id) 
	{
		$product_inventory = $this->inventory_model->get_product_inventory_details($product_id);
		return $product_inventory;
	}

	public function get_all_storages()
	{
		$storage_details = $this->inventory_model->get_all_storages();
		// return $storage_details;

		$response = array(
			'status' => true,
			'message' => '',
			'data' => $storage_details
		);
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function get_product_total_inventory($product_id) 
	{
		$get_inventory_details = $this->inventory_model->get_product_inventory_details($product_id);
		$total = 0;
		$sum = 0;


		foreach ($get_inventory_details as $detailskey => $detailsvalue) {

			$sum = $this->unit_converter->convert_measurement($detailsvalue["unit_measure_name"], "kg", $detailsvalue["quantity_received"]);

			if($detailsvalue["method"] == "add")
			{
				$total += $sum;
			}
			else
			{
				$total = $total - $sum;
			}
		}

		return number_format($total,2);
	}

	public function get_product_withdrawals($product_id) {
		$product_withdrawals = $this->inventory_model->get_product_withdrawals($product_id);

		$user_data = $this->session->userdata('apollo_user');

		foreach ($product_withdrawals as $key => $value) {
			$mode_travel = "";

			if($value["this_vessel"] !== null)
			{
				$mode_travel = "Vessel";
			}else if($value["this_truck"] !==null)
			{
				$mode_travel = "Truck";
			}

			$vessel_origin = $this->inventory_model->get_vessel_name($value["origin_vessel_id"]);

			$batch_info = $this->inventory_model->get_batch_info($value["withdrawal_prod_id"]);

			$product_withdrawals[$key]["batch_info"] = $batch_info;

			$product_withdrawals[$key]["mode_travel"] = $mode_travel;

			$product_withdrawals[$key]["vessel_origin"] = $vessel_origin[0]["vessel_name"];

			$product_withdrawals[$key]["user_name"] = $user_data["first_name"]." ".$user_data["last_name"];

		}

		return $product_withdrawals;

		// echo "<pre>";
		// print_r($product_withdrawals);
		// echo "</pre>";
		// exit();
		
	}

	// public function calculate_days_remaining($params) {
	// 	$this->load->helper('date');
	// 	$date = strtotime($params['date_received']);
	// 	$new_date = strtotime("+" . $params['product_shelf_life'] . " day", $date);
	// 	$final_date = date('y-m-d h:i:s', $new_date);
		
	// 	$datetime1 = date_create($params['date_received']);
	// 	$datetime2 = date_create($final_date);
	// 	$interval = date_diff($datetime1, $datetime2);
	// 	$final_date = $interval->format('%a days, %h hours');
	// 	return $final_date;
	// }

	public function calculate_days_remaining($params) {
		$this->load->helper('date');
		$date = strtotime($params['date_received']);

		$this_hour = explode(".", $params['product_shelf_life']);

		$new_date = strtotime("+" .$this_hour[0]." days".$this_hour[1]." hours", $date);
		$final_date = date('y-m-d h:i:s', $new_date);
		$this_day = date('y-m-d h:i:s');
		
		$datetime1 = date_create($this_day);
		$datetime2 = date_create($final_date);
		$interval = date_diff($datetime1, $datetime2);
		$final_date = $interval->format('%a days, %h hours');
		return $final_date;

	}




	public function get_product_consignee($product_id)
	{
		$product_consignee = $this->inventory_model->get_product_consignee($product_id);
		$total = 0;
		$sum = 0;
		$arr_collect_consignee = array();
		$arr_store_new_consignee = array();

		foreach ($product_consignee as $key => $value) {

			$sum = $this->unit_converter->convert_measurement($value["unit_of_measure_name"], "kg", $value["distribution_qty"]);

			// $total += $sum;
			$vessel_name = "";

			if($value["vessel_origin"] !== null)
			{
				$vessel_name = $this->inventory_model->get_vessel_name($value["vessel_origin"]);
			}else if($value["truck_origin"] !==null)
			{
				$vessel_name = $this->inventory_model->get_vessel_name($value["truck_origin"]);
			}

			$arr_gather_consign = array(
					"consignee_id"=>$value["consignee_id"],
					"consignee_name"=>$value["consignee_name"],
					"vessel_id"=>$vessel_name[0]["vessel_id"],
					"vessel_name"=>$vessel_name[0]["vessel_name"],
					"consignee_quantity"=>$sum
				);

			array_push($arr_collect_consignee, $arr_gather_consign);
			
		}

		foreach ($arr_collect_consignee as $key => $value) {
			$arr_vessel_data = array();
			$arr_quantity_data = array();

			if(count($arr_store_new_consignee) > 0)
			{

				$icount = 0;
				foreach ($arr_store_new_consignee as $newkey => $newvalue) {
					$arr_new_vessels = array();
					$arr_new_quantities = array();
					$arr_new_consignee = array();

					if($arr_store_new_consignee[$newkey]["consignee_id"] == $value["consignee_id"])
					{

						$icount++;
						foreach ($arr_store_new_consignee[$newkey]["vessel_info"] as $vesselkey => $vesselvalue) {

							if($arr_store_new_consignee[$newkey]["vessel_info"][$vesselkey]["vessel_id"] != $value["vessel_id"])
							{
								$new_vessel_merge = array(
										"vessel_id"=>$value["vessel_id"],
										"vessel_name"=>$value["vessel_name"]
									);

								array_push($arr_store_new_consignee[$newkey]["vessel_info"], $new_vessel_merge);

								$new_quantity_merge = array(
										"batch_quantity"=>$value["consignee_quantity"]
									);
								array_push($arr_store_new_consignee[$newkey]["consignee_quantity_info"], $new_quantity_merge);
							}
						}
					}
					
				}

				if($icount == 0)
				{
					$arr_new_vessels_new = array(
								"vessel_id"=>$value["vessel_id"],
								"vessel_name"=>$value["vessel_name"],
							);
						array_push($arr_new_vessels, $arr_new_vessels_new);

						$arr_new_quantity = array(
							"batch_quantity"=>$value["consignee_quantity"]
						);

						array_push($arr_new_quantities, $arr_new_quantity);


						$arr_new_consignee = array(
							"consignee_id"=>$value["consignee_id"],
							"consignee_name"=>$value["consignee_name"],
							"vessel_info"=>$arr_new_vessels,
							"consignee_quantity_info"=>$arr_new_quantities
						);


						array_push($arr_store_new_consignee, $arr_new_consignee);
				}

			}else{
				$arr_store_vessel = array(
						"vessel_id"=>$arr_collect_consignee[$key]["vessel_id"],
						"vessel_name"=>$arr_collect_consignee[$key]["vessel_name"]
				);
				array_push($arr_vessel_data, $arr_store_vessel);

				$arr_store_quantity = array(
						"batch_quantity"=>$value["consignee_quantity"]
					);

				array_push($arr_quantity_data, $arr_store_quantity);
				


				$array_gather_new_consign = array(
						"consignee_id"=>$value["consignee_id"],
						"consignee_name"=>$value["consignee_name"],
						"vessel_info"=>$arr_vessel_data,
						"consignee_quantity_info"=>$arr_quantity_data
					);
				array_push($arr_store_new_consignee, $array_gather_new_consign);


			}
		}


		return $arr_store_new_consignee;

	}

	public function save_update_batch_status()
	{
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules("data", "Data", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{

			$this->inventory_model->update_batch_status(array("status"=>$data["status"]), $data["batch_id"]);



			$this->inventory_model->update_batch_status_map_deleted($data["batch_id"], array("is_deleted"=>1));

			$arr_new_batch_status = array(
				"batch_id"=>$data["batch_id"],
				"reason"=>$data["reason"],
				"status"=>$data["status"],
				"date_created"=>date("Y-m-d H:i:s")
				);

			$result = $this->inventory_model->add_batch_status_map($arr_new_batch_status);

			$user_data = $this->session->userdata('apollo_user');

			$status = "";
			if($data["status"] == "1")
			{
				$status = "Quality Assurance";
			}else if($data["status"] == "2")
			{
				$status = "Restricted";
			}

			$arr_create_status_batch = array(
					"batch_id"=>$data["batch_id"],
					"batch_name"=>$data["batch_name"],
					"reason"=>$data["reason"],
					"status"=>$status,
					"user_name"=>ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]),
					"date_created"=>date('F d,Y h:i A')
				);


			header("Content-Type: application/json");

				$response["status"] = true;
				$response["message"] = "Success";
				$response["data"] = $arr_create_status_batch;
				echo json_encode($response);
		}
	}

	public function get_all_storage_location_info()
	{
		$get_storage = $this->inventory_model->get_storages();
		$arr_storage_data = array();
		$storage_total_quantity = 0;

		foreach ($get_storage as $key => $value) {
			$arr_collect_storege_product = array();
			$get_products = $this->inventory_model->get_all_storages_products($value["id"]);
			
			foreach ($get_products as $prodkey => $prodvalue) {
				$get_prod_batches = $this->inventory_model->get_all_storages_products_batches($prodvalue["product_id"], $value["id"]);
				$arr_collect_bacthes_product = array();
				$prodTotal = 0;

				foreach ($get_prod_batches as $batcheskey => $batchesvalue) {
					$sum = $this->unit_converter->convert_measurement($batchesvalue["unit_measure_name"], "kg", $batchesvalue["batch_quantity"]);

					$prodTotal += $sum;


					$days_left_parameters = array(
						'date_received' => $batchesvalue['receiving_date_received'],
						'product_shelf_life' => $prodvalue['product_shelf_life']
					);

					$days_left = $this->calculate_days_remaining($days_left_parameters);

					$product_batch_repack = $this->inventory_model->get_product_batches_repack($batchesvalue["batch_id"]);
					$repack_qty = "";
					if(count($product_batch_repack) > 0)
					{
						$repack_qty = $product_batch_repack[0]["quantity"];
					}else{
						$repack_qty = "";
					}


					$arr_gather_batches = array(
							"batch_id"=>$batchesvalue["batch_id"],
							"batch_name"=>$batchesvalue["batch_name"],
							"location_id"=>$batchesvalue["location_id"],
							"status"=>$batchesvalue["status"],
							"date_received"=>$batchesvalue["receiving_date_received"],
							"unit_of_measure_id"=>$batchesvalue["unit_of_measure_id"],
							"batch_unit_of_measure"=>$batchesvalue["unit_of_measure_id"],
							"unit_of_measure_name"=>$batchesvalue["unit_measure_name"],
							"repack_batch_qty"=>$repack_qty,
							"days_left"=>$days_left,
							"batch_quantity"=>$sum,
							"converted_qty_kg"=>number_format($sum, 2)
						);

					array_push($arr_collect_bacthes_product, $arr_gather_batches);
				}

				$storage_total_quantity +=$prodTotal;

				$arr_gather_storege_product = array(
						"product_id"=>$prodvalue["product_id"],
						"product_name"=>$prodvalue["product_name"],
						"sku"=>$prodvalue["sku"],
						"total_prod_quantity"=>$prodTotal,
						"batch_info"=>$arr_collect_bacthes_product
					);




				array_push($arr_collect_storege_product, $arr_gather_storege_product);

			}

			$storage_sum = $this->unit_converter->convert_measurement(strtolower($value["unit_measure_name"]), "kg", $value["capacity"]);


			$arr_gather_storage = array(
					"storage_id"=>$value["id"],
					"storage_name"=>$value["name"],
					"storage_capacity"=>$storage_sum,
					"storage_total_quantity"=>$storage_total_quantity,
					"product_info"=>$arr_collect_storege_product
				);

			array_push($arr_storage_data, $arr_gather_storage);

		}


		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $arr_storage_data;
		echo json_encode($response);
		

	}

	public function save_remove_batch()
	{
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules("data", "Data", "trim|required");

		if($this->form_validation->run() === FALSE)
		{

			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);

		}else{


			$result = $this->inventory_model->save_remove_batch_reason(array("batch_id"=>$data["batch_id"], "reason"=>$data["reason"], "date_created"=>date("Y-m-d H:i:s")));

			$this->inventory_model->save_remove_batch($data["batch_id"], array("is_deleted"=>1));
			$user_data = $this->session->userdata('apollo_user');
			$arr_create_remove_batch = array(
					"batch_id"=>$data["batch_id"],
					"batch_name"=>$data["batch_name"],
					"reason"=>$data["reason"],
					"user_name"=>ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]),
					"date_created"=>date('F d,Y h:i A')
				);


			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $arr_create_remove_batch;
			echo json_encode($response);

		}
	}

	public function save_batch_adjustment_quatity()
	{
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules("data", "Data", "trim|required");

		if($this->form_validation->run() === FALSE)
		{

			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);

		}else{

			$sum = 0;

			$sum = $this->unit_converter->convert_measurement("kg", strtolower($data["measurement_name"]), $data["total"]);

			$arr_new_adjustment = array(
					"batch_id"=>$data["batch_id"],
					"method"=>$data["method"],
					"adjustment_quantity"=>$data["adjusted_qty"],
					"old_qty"=>$data["old_qty"],
					"new_qty"=>$data["total"],
					"reason"=>$data["reason"],
					"date_created"=>date("Y-m-d H:i:s")
				);

			$result = $this->inventory_model->save_batch_adjustment_quatity($arr_new_adjustment);

			$array_batch_adjusted_qty = array(
					"quantity"=>$sum
				);

			$this->inventory_model->adjust_batch_quantity($data["batch_id"], $array_batch_adjusted_qty);

			$user_data = $this->session->userdata('apollo_user');
			$arr_create_new_batch_adjusted = array(
					"batch_id"=>$data["batch_id"],
					"batch_name"=>$data["batch_name"],
					"old_qty"=>number_format($data["old_qty"], 2),
					"new_qty"=> number_format($data["total"], 2),
					"reason"=>$data["reason"],
					"user_name"=>ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]),
					"date_created"=>date('F d,Y h:i A')
				);



			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $arr_create_new_batch_adjusted;
			echo json_encode($response);
		}
	}

	public function get_batch_adjust_quantity($product_id)
	{
		$get_batch = $this->inventory_model->get_batch_adjust_quantity($product_id);
		$user_data = $this->session->userdata('apollo_user');
		foreach ($get_batch as $key => $value) {
			$get_batch[$key]["new_qty"] = number_format($get_batch[$key]["new_qty"], 2);
			$get_batch[$key]["old_qty"] = number_format($get_batch[$key]["old_qty"], 2);
			$get_batch[$key]["user_name"] = ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]);
		}

		return $get_batch;
	}

	public function get_batch_return_quantity($product_id)
	{
		$get_batch_return = $this->inventory_model->get_batch_return_quantity($product_id);
		$user_data = $this->session->userdata('apollo_user');
		foreach ($get_batch_return as $key => $value) {
			$get_batch_return[$key]["returning_qty"] = number_format($get_batch_return[$key]["returning_qty"], 2);
			$get_batch_return[$key]["user_name"] = ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]);
		}

		return $get_batch_return;
			
	}

	public function get_batch_remove_quantity($product_id)
	{
		$get_batch_remove = $this->inventory_model->get_batch_remove_quantity($product_id);
		$user_data = $this->session->userdata('apollo_user');
		foreach ($get_batch_remove as $key => $value) {
			$get_batch_remove[$key]["user_name"] = ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]);
		}

		return $get_batch_remove;

	}

	public function get_batch_update_status($product_id)
	{
		$get_batch_update_status = $this->inventory_model->get_batch_update_status($product_id);
		$user_data = $this->session->userdata('apollo_user');
		foreach ($get_batch_update_status as $key => $value) {

			if($value["status"] == "1")
			{
				$get_batch_update_status[$key]["status"] = "Quality Assurance";
			}else if($value["status"] == "2")
			{
				$get_batch_update_status[$key]["status"] = "Restricted";
			}

			$get_batch_update_status[$key]["user_name"] = ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]);
		}

		return $get_batch_update_status;
	}

	// public function get_batch_split($product_id)
	// {

	// 	$get_split_dates =  $this->inventory_model->get_batch_split_dates();
	// 	$get_batch_split_record = $this->inventory_model->get_batch_split($product_id);
	// 	$user_data = $this->session->userdata('apollo_user');
	// 	$build_split_batch = array();
	// 	foreach ($get_batch_split_record as $key => $value) {
	// 		$get_batch_split_record[$key]["user_name"] = ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]);

	// 	}

	// 	return $get_batch_split_record;
	// }

	public function get_batch_split($product_id)
	{
		$get_split_dates =  $this->inventory_model->get_batch_split_dates($product_id);
		$build_split_batch = array();
		$user_data = $this->session->userdata('apollo_user');
		foreach ($get_split_dates as $key => $value) {
		
			$get_batch_split_record = $this->inventory_model->get_batch_split($product_id, $value["date_created"]);
			
			$arr_get_splits = array(
					"batch_old_id"=>$value["batch_id_from"],
					"batch_name"=>$value["batch_name"],
					"date_created"=>$value["date_created_formatted"],
					"unit_of_measure"=>$value["unit_of_measure_name"],
					"user_name"=>ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]),
					"batch_split_info"=>$get_batch_split_record
				);
			array_push($build_split_batch, $arr_get_splits);

		}

		return $build_split_batch;

	}

	public function get_batch_repack($product_id)
	{
		$get_repack_batch = $this->inventory_model->get_batch_repack($product_id);
		$user_data = $this->session->userdata('apollo_user');

		foreach ($get_repack_batch as $key => $value) {
			$get_repack_batch[$key]["user_name"] = ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]);
		}

		return $get_repack_batch;
	}



	public function save_returning_batch()
	{
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules("data", "Data", "trim|required");

		if($this->form_validation->run() === FALSE)
		{

			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);

		}else{

			$arr_build_return_batch = array(
					"batch_id"=>$data["batch_id"],
					"returning_qty"=>$data["return_quantity"],
					"reason"=>$data["reason"],
					"date_created"=>date("Y-m-d H:i:s")
				);
			$result = $this->inventory_model->save_returning_batch($arr_build_return_batch);

			$sum = 0;

			$sum = $this->unit_converter->convert_measurement("kg", strtolower($data["unit_of_measure_name"]), $data["total"]);

			$array_batch_adjusted_qty = array(
					"quantity"=>$sum
				);

			$this->inventory_model->adjust_batch_quantity($data["batch_id"], $array_batch_adjusted_qty);

			$product_invt_info = $this->inventory_model->get_product_invt_info($data["batch_id"]);

			$batch_quantity_convert = $this->unit_converter->convert_measurement("kg", strtolower($data["unit_of_measure_name"]), $data["return_quantity"]);

			foreach ($product_invt_info as $key => $value) {
				$arr_subtract_product_invt = array(
						"quantity_received"=>$batch_quantity_convert,
						"method"=>"subtract",
						"unit_of_measure_id"=>$data["unit_of_measure_id"],
						"date_created"=>date("Y-m-d H:i:s"),
						"product_id"=>$value["product_id"]

					);

				$this->inventory_model->add_product_inventory($arr_subtract_product_invt);


			}

			$user_data = $this->session->userdata('apollo_user');
			$arr_create_return_batch = array(
					"batch_id"=>$data["batch_id"],
					"batch_name"=>$data["batch_name"],
					"returning_qty"=>number_format($data["return_quantity"], 2),
					"reason"=>$data["reason"],
					"user_name"=>ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]),
					"date_created"=>date('F d,Y h:i A')
				);



			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $arr_create_return_batch;
			echo json_encode($response);
		}
	}

	public function save_split_batch()
	{
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules("data", "Data", "trim|required");

		if($this->form_validation->run() === FALSE)
		{

			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);

		}else{
			$batch_data = $this->inventory_model->get_batch_selected($data["batch_old_id"]);
			$quantity = floatval($batch_data[0]["quantity"]);

			foreach ($data["batch_info"] as $key => $value) {
				$quantity -= floatval($value["batch_quantity"]);
				$get_date = "";

				$arr_new_batch = array(
						"name"=>$value["batch_name"],
						"quantity"=>$value["batch_quantity"],
						"storage_assignment_id"=>$batch_data[0]["storage_assignment_id"],
						"unit_of_measure_id"=>$data["unit_of_measure"],
						"location_id"=>$batch_data[0]["location_id"],
						"storage_id"=>$batch_data[0]["storage_id"],
						"status"=>$batch_data[0]["status"]
					);

				$new_batch_id = $this->inventory_model->add_batch($arr_new_batch);
				$get_date = date("Y-m-d H:i:s");

				$arr_batch_split_map = array(
						"batch_id_from"=>$data["batch_old_id"],
						"batch_id_new"=>$new_batch_id,
						"quantity"=>$value["batch_quantity"],
						"unit_of_measure_id"=>$data["unit_of_measure"],
						"date_created"=>$get_date
					);
				$this->inventory_model->add_batch_split_map($arr_batch_split_map);

				$data["batch_info"][$key]["unit_of_measure_name"] = $data["unit_of_measure_name"];
			}

			if($quantity > 0)
			{
				$update_data = array(
						"quantity"=>$quantity
					);
				$this->inventory_model->update_batch($data["batch_old_id"], $update_data);
			}else{
				$update_data = array(
						"is_deleted"=>"0"
					);
				$this->inventory_model->update_batch($data["batch_old_id"], $update_data);
			}

			$user_data = $this->session->userdata('apollo_user');
			$the_date = strtotime($get_date);
			$arr_create_split_batch = array(
					"batch_old_id"=>$data["batch_old_id"],
					"batch_name"=>$data["batch_old_name"],
					"unit_of_measure_name"=>$data["unit_of_measure_name"],
					"user_name"=>ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]),
					"date_created"=>date('F d,Y h:i A', $the_date),
					"batch_split_info"=>$data["batch_info"]
				);




			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $arr_create_split_batch;
			echo json_encode($response);
		}
	}

	public function save_repack_batch()
	{
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules("data", "Data", "trim|required");

		if($this->form_validation->run() === FALSE)
		{

			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);

		}else{
			$get_date = date("Y-m-d H:i:s");
			$the_date = strtotime($get_date);

			$arr_new_repack_batch = array(
					"batch_id"=>$data["batch_id"],
					"quantity"=>$data["quantity"],
					"old_quantity"=>$data["repack_batch_qty"],
					"date_created"=>$get_date
				);

			$this->inventory_model->add_repack_batch($arr_new_repack_batch);

			$user_data = $this->session->userdata('apollo_user');
			$arr_create_repack_batch = array(
					"batch_id"=>$data["batch_id"],
					"batch_name"=>$data["batch_name"],
					"quantity"=>$data["quantity"],
					"old_quantity"=>$data["repack_batch_qty"],
					"user_name"=>ucwords($user_data["first_name"])." ".ucwords($user_data["last_name"]),
					"date_created"=>date('F d,Y h:i A', $the_date)
				);


			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $arr_create_repack_batch;
			echo json_encode($response);
		}
	}









}
