<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory_model extends CI_Model {


	public function add_product_inventory($data){
		$this->db->insert("product_inventory", $data);
		return $this->db->insert_id();
	}

	public function get_all_products_inventory()
	{
		$sql = "SELECT pi.product_id, p.name, p.sku, p.image
				FROM product_inventory pi 
				LEFT JOIN product p ON p.id=pi.product_id
				GROUP BY pi.product_id ORDER BY pi.id DESC
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_inventory_details($product_id)
	{
		$sql = "SELECT pi.unit_of_measure_id, pi.quantity_received, pi.date_created, pi.method, uom.name as unit_measure_name
				FROM product_inventory pi
				LEFT JOIN unit_of_measures uom ON uom.id=pi.unit_of_measure_id
				WHERE pi.product_id='{$product_id}'
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_products_batches_inventory($product_id)
	{
		$sql = "SELECT count(b.id) as counted_batches
				FROM  product_inventory_qty piq
				LEFT JOIN storage_assignment sa ON sa.product_inventory_qty_id=piq.id
				LEFT JOIN batch b ON b.storage_assignment_id=sa.id
				WHERE piq.product_id='{$product_id}' AND piq.is_deleted=0 AND sa.is_deleted=0 AND b.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_details($product_id) 
	{
		$sql = "SELECT p.id, p.name, p.sku,
			p.product_category, p.description, p.image,
			p.product_shelf_life, p.threshold_min, p.threshold_maximum,
			p.unit_price, p.is_active,
			c.name as category_name
			FROM product p
			LEFT JOIN category c ON c.id = p.product_category
			WHERE p.id = '{$product_id}' AND p.is_deleted=0 AND c.is_deleted=0";
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function get_product_batches($product_id) {
		$sql = "SELECT piq.id as product_invetory_id, piq.qty as product_inventory_qty,
			piq.loading_method as product_inventory_loading_method,
			piq.bag as product_inventory_bag,
			piq.product_receiving_id as product_inventory_receiving,
			sa.id as storage_assignment_id, sa.receiving_log_id as receiving_log_id,
			sa.product_inventory_qty_id as storage_assignment_product_inventory, sa.total as storage_assignment_total,
			b.id as batch_id, b.storage_assignment_id as batch_storage_assignment, b.name as batch_name,
			b.quantity as batch_quantity, b.unit_of_measure_id as batch_unit_of_measure,
			b.location_id as batch_location, b.storage_id as batch_storage, st.name as storage_name,
			uom.name as unit_of_measure_name, b.status as batch_status,
			vrl.origin as vessel_origin_id, trl.origin as truck_origin_id,
			DATE_FORMAT(rl.date_received, '%d-%b-%Y') as receiving_date_received
			FROM batch b
			LEFT JOIN storage_assignment sa ON sa.id = b.storage_assignment_id
			LEFT JOIN product_inventory_qty piq ON piq.id = sa.product_inventory_qty_id
			LEFT JOIN unit_of_measures uom ON uom.id =  b.unit_of_measure_id
			LEFT JOIN receiving_log rl ON rl.id = piq.product_receiving_id
			LEFT JOIN storage s ON s.id=b.storage_id
			LEFT JOIN storage_type st ON st.id=s.storage_type
			LEFT JOIN vessel_receiving_log vrl ON vrl.receiving_log=rl.id
			LEFT JOIN truck_receiving_log trl ON trl.receiving_log_id=rl.id
			WHERE piq.product_id = '{$product_id}' AND rl.status=1
			AND b.is_deleted = 0 AND piq.is_deleted=0 AND sa.is_deleted=0 ORDER BY rl.id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_transfers($product_id) {
		$sql = "SELECT tpm.id as transfer_product_map_id, tpm.product_id as transfer_product_map_product,
			tpm.transfer_log_id as transfer_product_map_log, tpm.transfer_method as transfer_product_map_trasfer_method,
			tpm.is_deleted as tranfer_product_map_is_deleted,
			tl.id as log_id, tl.type as log_type, tl.user_id as log_user,
			DATE_FORMAT(tl.date_issued, '%M %d, %Y %r') as log_issued_date, tl.date_start as log_date_start,
			tl.date_complete as log_date_complete, tl.qty_to_transfer as log_qty_to_transfer,
			tl.status as log_status, tl.requested_by as log_requested_by,
			tl.reason_for_transfer as log_reason_for_transfer, tl.mode_of_transfer_id as log_mode_of_transfer,
			tn.destination_container, tn.origin_container, vl.name as vessel_name, b.name as batch_name, tl.to_number
			FROM transfer_product_map tpm
			LEFT JOIN transfer_log tl ON tl.id = tpm.transfer_log_id
			LEFT JOIN transfer_origin tn ON tn.product_map_id=tpm.id
			LEFT JOIN vessel_list vl ON vl.id=tl.vessel_origin_id
			LEFT JOIN batch b ON b.id=tn.batch_id
			WHERE tpm.product_id = '{$product_id}' AND tl.status=1
			AND tpm.is_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_receiving($product_id)
	{
		$sql = "SELECT sa.id as storage_assignment_id, sa.receiving_log_id as storage_assignment_receiving_log,
			sa.product_inventory_qty_id as storage_assignment_inventory_qty, sa.total as storage_assignment_total,
			r.id as receiving_id, r.receiving_number as receiving_number,
			r.purchase_order as receiving_purchase_order, DATE_FORMAT(r.date_received, '%d-%b-%Y') as receiving_date_received,
			r.status as receiving_status, r.type as receiving_type,
			r.received_by as receiving_received_by, r.invoice_id as receiving_invoice,
			r.officer_id as receiving_officer, r.courier_id as receiving_courier,
			DATE_FORMAT(r.date_created, '%M %d, %Y %r') as receiving_date_created,
			piq.id as product_inventory_qty_id, piq.product_id
			FROM storage_assignment sa
			LEFT JOIN receiving_log r ON r.id = sa.receiving_log_id
			LEFT JOIN product_inventory_qty piq ON piq.id = sa.product_inventory_qty_id
			WHERE piq.is_deleted = 0
			AND piq.product_id = '{$product_id}' AND r.status=1 ORDER BY r.id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_withdrawals($product_id)
	{
		$sql = "SELECT wp.id as withdrawal_prod_id, wl.withdrawal_number, wcd.atl_number, 
				DATE_FORMAT(wl.date_created, '%M %d, %Y %r') as withdrawal_date_created,
				wdrd.cdr_no, c.name as consignee_name, wcd.origin_vessel_id, wtd.id as this_truck, wvd.id as this_vessel
				FROM withdrawal_log wl
				LEFT JOIN withdrawal_product wp ON wp.withdrawal_log_id=wl.id
				LEFT JOIN withdrawal_consignee_details wcd ON wcd.withdrawal_log_id=wp.withdrawal_log_id
				LEFT JOIN withdrawal_cdr_details wdrd ON wdrd.withdrawal_log_id=wp.withdrawal_log_id
				LEFT JOIN consignee c ON c.id=wcd.consignee_id
				LEFT JOIN withdrawal_truck_details wtd ON wtd.withdrawal_log_id=wp.withdrawal_log_id
				LEFT JOIN withdrawal_vessel_details wvd ON wvd.withdrawal_log_id=wp.withdrawal_log_id
				WHERE wp.product_id='{$product_id}' AND wl.status=1 AND wl.is_deleted=0 AND wdrd.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_batch_info($withdrawal_prod_id)
	{
		$sql = "SELECT b.name as batch_name
				FROM withdrawal_batch wb
				LEFT JOIN batch b ON b.id=wb.batch_id
				WHERE wb.withdrawal_product_id='{$withdrawal_prod_id}' AND b.is_deleted=0 AND wb.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}


	public function get_all_storages()
	{
		$sql = "SELECT s.id, s.name, st.name as storage_type_name, s.capacity, uom.name as measurement_name
				FROM storage s
				LEFT JOIN storage_type st ON st.id=s.storage_type
				LEFT JOIN unit_of_measures uom ON uom.id=s.measurement_id
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function check_if_vessel($receiving_id)
	{
		$sql = "SELECT vl.name as vessel_name
				FROM vessel_receiving_log vrl
				LEFT JOIN vessel_list vl ON vl.id=vrl.origin
				WHERE vrl.receiving_log='{$receiving_id}';
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function check_if_truck($receiving_id)
	{
		$sql = "SELECT vl.name as vessel_name
				FROM truck_receiving_log trl
				LEFT JOIN vessel_list vl ON vl.id=trl.origin
				WHERE trl.receiving_log_id='{$receiving_id}';
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_receiving_batches($receiving_id, $product_id)
	{
		$sql = "SELECT b.name as batch_name
				FROM storage_assignment sa 
				LEFT JOIN batch b ON b.storage_assignment_id=sa.id
				LEFT JOIN product_inventory_qty piq ON piq.id=sa.product_inventory_qty_id
				WHERE sa.receiving_log_id='{$receiving_id}' AND piq.product_id='{$product_id}' AND sa.is_deleted=0 AND b.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_consignee($product_id)
	{
		$sql = "SELECT c.id as consignee_id, c.name as consignee_name, trl.origin as truck_origin, vcl.origin as vessel_origin, uom.name as unit_of_measure_name, rcm.distribution_qty
				FROM receiving_consignee_map rcm
				LEFT JOIN consignee c ON c.id=rcm.consignee_id
				LEFT JOIN receiving_log rl ON rl.id=rcm.receiving_log_id
				LEFT JOIN truck_receiving_log trl ON trl.receiving_log_id=rcm.receiving_log_id
				LEFT JOIN vessel_receiving_log vcl ON vcl.receiving_log=rcm.receiving_log_id
				LEFT JOIN unit_of_measures uom ON uom.id=rcm.unit_of_measure_id
				WHERE rl.type=2 AND rcm.product_id='{$product_id}' AND rl.status=1 ORDER BY rl.id DESC
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_vessel_name($id)
	{
		$sql = "SELECT name as vessel_name, id as vessel_id
				FROM vessel_list WHERE id='{$id}' AND is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function update_batch_status($status_data, $batch_id)
	{
		$this->db->where("id", $batch_id);
		$this->db->update("batch", $status_data);
		return $this->db->affected_rows();
	}

	public function add_batch_status_map($data)
	{
		$this->db->insert("batch_status_map", $data);
		return $this->db->insert_id();
	}

	public function update_batch_status_map_deleted($batch_id, $deleted_data)
	{
		$this->db->where("batch_id", $batch_id);
		$this->db->update("batch_status_map", $deleted_data);
	}

	public function get_all_storages_products($storage_id)
	{
		$sql = "SELECT p.id as product_id,  p.name as product_name, p.sku, p.product_shelf_life
				FROM batch b
				LEFT JOIN storage_assignment sa ON sa.id=b.storage_assignment_id
				LEFT JOIN product_inventory_qty piq ON piq.id=sa.product_inventory_qty_id
				LEFT JOIN receiving_log rl ON rl.id=sa.receiving_log_id
				LEFT JOIN product p ON p.id=piq.product_id
				WHERE b.storage_id='{$storage_id}' AND rl.status=1 AND b.is_deleted=0 AND sa.is_deleted=0 AND piq.is_deleted=0
				AND p.is_deleted=0 GROUP BY p.id ORDER BY p.id ASC
				";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_storages_products_batches($product_id, $storage_id)
	{
		$sql = "SELECT b.id as batch_id, b.name as batch_name, b.quantity as batch_quantity, b.unit_of_measure_id, b.location_id, 
				uom.name as unit_measure_name, b.status, DATE_FORMAT(rl.date_created, '%d-%b-%Y') as receiving_date_received
				FROM product_inventory_qty piq
				LEFT JOIN storage_assignment sa ON sa.product_inventory_qty_id=piq.id
				LEFT JOIN receiving_log rl ON rl.id=sa.receiving_log_id
				LEFT JOIN batch b ON b.storage_assignment_id=sa.id
				LEFT JOIN unit_of_measures uom ON uom.id=b.unit_of_measure_id
				WHERE piq.product_id='{$product_id}' AND b.storage_id='{$storage_id}' AND piq.is_deleted=0 AND sa.is_deleted=0 AND rl.status=1 AND b.is_deleted=0
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_storages()
	{
		$sql = "SELECT s.id, s.name, s.measurement_id, uom.name as unit_measure_name, s.capacity
		FROM storage s
		LEFT JOIN unit_of_measures uom ON uom.id=s.measurement_id
		";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function save_remove_batch($batch_id, $data)
	{
		$this->db->where("id", $batch_id);
		$this->db->update("batch", $data);
	}

	public function save_remove_batch_reason($data)
	{
		$this->db->insert("batch_remove_map", $data);
		return $this->db->insert_id();
	}

	public function save_batch_adjustment_quatity($arr_new_adjustment)
	{
		$this->db->insert("batch_adjust_map", $arr_new_adjustment);
		return $this->db->insert_id();
	}

	public function adjust_batch_quantity($batch_id, $array_batch_adjusted_qty)
	{
		$this->db->where("id", $batch_id);
		$this->db->update("batch", $array_batch_adjusted_qty);
	}

	public function save_returning_batch($data)
	{
		$this->db->insert("batch_return_map", $data);
		return $this->db->insert_id();
	}

	public function get_product_invt_info($batch_id)
	{
		$sql = "SELECT piq.product_id
				FROM batch b
				LEFT JOIN storage_assignment sa ON sa.id=b.storage_assignment_id
				LEFT JOIN product_inventory_qty piq ON piq.id=sa.product_inventory_qty_id
				WHERE b.id='{$batch_id}' AND b.is_deleted=0 AND piq.is_deleted=0
				GROUP BY piq.product_id
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_batch_adjust_quantity($product_id)
	{
		$sql = "SELECT bam.batch_id, bam.new_qty, bam.old_qty, bam.reason, DATE_FORMAT(bam.date_created, '%M %d, %Y %r') as date_created, b.name as batch_name
				FROM batch_adjust_map bam
				LEFT JOIN batch b ON b.id=bam.batch_id
				LEFT JOIN storage_assignment sa ON sa.id=storage_assignment_id
				LEFT JOIN product_inventory_qty piq ON piq.id=sa.product_inventory_qty_id
				WHERE piq.product_id='{$product_id}' AND b.is_deleted=0 AND sa.is_deleted=0 ORDER BY bam.id DESC
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_batch_return_quantity($product_id)
	{
		$sql = "SELECT brm.batch_id, brm.returning_qty, brm.reason, DATE_FORMAT(brm.date_created, '%M %d, %Y %r') as date_created, b.name as batch_name
				FROM batch_return_map brm
				LEFT JOIN batch b ON b.id=brm.batch_id
				LEFT JOIN storage_assignment sa ON sa.id=storage_assignment_id
				LEFT JOIN product_inventory_qty piq ON piq.id=sa.product_inventory_qty_id
				WHERE piq.product_id='{$product_id}' AND b.is_deleted=0 AND sa.is_deleted=0 ORDER BY brm.id DESC
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_batch_remove_quantity($product_id)
	{
		$sql = "SELECT  brm.batch_id, brm.reason, DATE_FORMAT(brm.date_created, '%M %d, %Y %r') as date_created, b.name as batch_name
				FROM batch_remove_map brm
				LEFT JOIN batch b ON b.id=brm.batch_id
				LEFT JOIN storage_assignment sa ON sa.id=storage_assignment_id
				LEFT JOIN product_inventory_qty piq ON piq.id=sa.product_inventory_qty_id
				WHERE piq.product_id='{$product_id}' ORDER BY brm.id DESC
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_batch_update_status($product_id)
	{
		$sql = "SELECT bsm.batch_id, bsm.reason, DATE_FORMAT(bsm.date_created, '%M %d, %Y %r') as date_created, b.name as batch_name, bsm.status
				FROM batch_status_map bsm
				LEFT JOIN batch b ON b.id=bsm.batch_id
				LEFT JOIN storage_assignment sa ON sa.id=storage_assignment_id
				LEFT JOIN product_inventory_qty piq ON piq.id=sa.product_inventory_qty_id
				WHERE piq.product_id='{$product_id}'  AND b.is_deleted=0 AND sa.is_deleted=0 AND bsm.is_deleted=0 ORDER BY bsm.id DESC
				";	
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_batch_selected($batch_id)
	{
		$sql = "SELECT name, storage_assignment_id, quantity, location_id, storage_id, status
				FROM batch WHERE id='{$batch_id}' AND is_deleted=0;
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function add_batch($arr_new_batch)
	{
		$this->db->insert("batch", $arr_new_batch);
		return $this->db->insert_id();
	}

	public function add_batch_split_map($arr_batch_split_map)
	{
		$this->db->insert("batch_split_map", $arr_batch_split_map);
		return $this->db->insert_id();
	}

	public function update_batch($batch_id, $update_data)
	{
		$this->db->where("id", $batch_id);
		$this->db->update("batch", $update_data);
	}

	public function get_batch_split($product_id, $date_created)
	{
		$sql = "SELECT bsm.batch_id_new as batch_id, bsm.quantity, b.name as batch_name, 
				uof.name as unit_of_measure_name
				FROM batch_split_map bsm
				LEFT JOIN batch b ON b.id=bsm.batch_id_new
				LEFT JOIN storage_assignment sa ON sa.id=b.storage_assignment_id
				LEFT JOIN product_inventory_qty piq ON piq.id=sa.product_inventory_qty_id
				LEFT JOIN unit_of_measures uof ON uof.id=bsm.unit_of_measure_id
				WHERE piq.product_id='{$product_id}' AND bsm.date_created='{$date_created}' AND b.is_deleted=0 AND sa.is_deleted=0  ORDER BY bsm.id DESC
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_batch_split_dates($product_id)
	{
		$sql = "SELECT bsm.batch_id_from, bsm.date_created, DATE_FORMAT(bsm.date_created, '%M %d, %Y %r') as date_created_formatted, b.name as batch_name, uof.name as unit_of_measure_name
				FROM batch_split_map bsm
				LEFT JOIN batch b ON b.id=bsm.batch_id_from
				LEFT JOIN storage_assignment sa ON sa.id=b.storage_assignment_id
				LEFT JOIN product_inventory_qty piq ON piq.id=sa.product_inventory_qty_id
				LEFT JOIN unit_of_measures uof ON uof.id=bsm.unit_of_measure_id
				WHERE piq.product_id='{$product_id}' AND b.is_deleted=0  AND sa.is_deleted=0 GROUP BY bsm.date_created ORDER BY bsm.id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function add_repack_batch($data)
	{
		$this->db->insert("batch_repack_map", $data);
		return $this->db->insert_id();
	}

	public function get_batch_repack($product_id)
	{
		$sql = "SELECT brm.batch_id, b.name as batch_name, brm.quantity, DATE_FORMAT(brm.date_created, '%M %d, %Y %r') as date_created, brm.old_quantity
				FROM batch_repack_map brm
				LEFT JOIN batch b ON b.id=brm.batch_id
				LEFT JOIN storage_assignment sa ON sa.id=b.storage_assignment_id
				LEFT JOIN product_inventory_qty piq ON piq.id=sa.product_inventory_qty_id
				WHERE piq.product_id='{$product_id}' AND b.is_deleted=0 AND sa.is_deleted=0  ORDER BY brm.id DESC
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_batches_repack($batch_id)
	{
		$sql = "SELECT quantity
				FROM batch_repack_map brm
				WHERE brm.batch_id='{$batch_id}' ORDER BY id DESC LIMIT 1
				";
		$query = $this->db->query($sql);
		return $query->result_array();
	}



}