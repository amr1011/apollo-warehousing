<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model 
{	
	public function get_all_products() 
	{
		$sql = "SELECT p.id, p.name, p.sku,
		p.product_category, p.description, p.image,
		p.product_shelf_life, p.threshold_min, p.threshold_maximum,
		p.unit_price, p.is_active, p.is_deleted, cat.name as category_name
		FROM product p
		LEFT JOIN category cat ON cat.id = p.product_category";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_sku() 
	{
		$sql = "SELECT sku 
		FROM product
		ORDER BY id DESC
		LIMIT 1";

		$sql = $this->db->query($sql);
		return $sql->result_array();

		
		// $sql = "SELECT sku 
		// FROM product, base_product
		// WHERE sku = '" . $sku . "'";
		// $sql = $this->db->query($sql);
		// return $sql->result_array();
	}

	public function get_all_product_category() {
		$query = $this->db->get('category');
		return $query->result_array();
	}

	public function add_product($product) 
	{
		$this->db->insert('product', $product);
		return $this->db->insert_id();
	}

	public function add_product_composition($composition_details) 
	{
		$this->db->insert('product_composition', $composition_details);
		return $this->db->affected_rows();
	}

	// public function update($update_conditions, $update_details)
	// {
	// 	$this->db->where($update_conditions);
	// 	$this->db->update('product', $update_details);
	// 	return $this->db->affected_rows();
	// }

	public function update($product_id, $update_details) {
		$this->db->where("id", $product_id);
		$this->db->update("product", $update_details);
		return $this->db->affected_rows();
	}

	public function delete_product_composition($product_id) {
		$this->db->where("product_id", $product_id);
		$this->db->delete("product_composition");
		return $this->db->affected_rows();
	}

	public function update_product_composition($product_composition) {
		$this->db->insert("product_composition", $product_composition);
		return $this->db->affected_rows();
	}
	
	/*public function get_product($arr_params) 
	{
		$sql = "SELECT p.id, p.threshold_min AS min_threshold, 
		p.threshold_maximum AS max_threshold, 
		p.description, p.unit_price, 
		p.product_shelf_life AS per_material_shelf_life,
		p.product_category AS category_id, 
		bc.name AS base_name, bc.id AS base_id
		FROM product p
		LEFT JOIN product_composition pc ON pc.product_id = p.id
		LEFT JOIN base_product bc ON bc.id = pc.base_product_id";
		$sql .= $arr_params;
		$query = $this->db->query($sql);

		return $query->result_array();
	}*/

	public function get_base_product($id)
	{
		$sql = "SELECT * FROM base_product WHERE id = ".$id;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product_composition($product_id)
	{
		$sql = "SELECT * FROM product_composition WHERE product_id = ".$product_id;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_product($where = "", $order = "", $limit = "", $offset = "")
	{
		$sql = "SELECT * FROM product ".$where." ".$order." ".$limit." ".$offset;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_expiry($product_category)
	{
		$sql = "SELECT MIN(product_shelf_life)
		FROM product
		WHERE product_category = $product_category";
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function add_base_product($base_product_details) 
	{
		$this->db->insert('base_product', $base_product_details);
		$id = $this->db->insert_id();
		return $id;
	}

	public function add_product_inventory($product_invetory_details) 
	{
		$this->db->insert('product_inventory', $product_invetory_details);
		return $this->db->affected_rows();
	}

	public function add_product_inventory_qty($inventory_qty_details) 
	{
		$this->db->insert('product_inventory_qty', $inventory_qty_details);
		return $this->db->affected_rows();
	}

	public function archive($product_id) 
	{
		$data = array('is_deleted' => 1);
		$this->db->where('id', $product_id);
		$this->db->update('product', $data);
		return $this->db->affected_rows();
	}

	public function search_categories($search_field, $search_keyword, $sorting, $sort_field)
	{
		$sql = "SELECT * FROM category WHERE {$search_field} LIKE '%{$search_keyword}%' ORDER BY {$sort_field} {$sorting} ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function delete($product_id, $delete_details) {
		$this->db->where("id", $product_id);
		$this->db->update("product", $delete_details);
		return $this->db->affected_rows();
	} 

	public function get_unit_of_measure($where = "")
	{
		$sql = "SELECT * FROM unit_of_measures ".$where;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function set_product_is_active($id , $is_active) {
		$update_details = array(
			"is_active" => $is_active
		);
		$this->db->where("id", $id);
		$this->db->update("product", $update_details);
		return $this->db->affected_rows();
	}

}