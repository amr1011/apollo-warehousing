<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MX_Controller 
{
	private $aliases = array();

	private $unit_of_measure_convert = "";

	public function __construct()
	{
		parent:: __construct();
		$this->load->library('MY_Form_validation');
    	$this->load->library('Unit_converter');
        $this->form_validation->CI =& $this;

  		// $value = $this->unit_converter->convert_measurement("bag", "kg", 20);
 		// print_r($value);
		// exit();


        $this->aliases = array(
			"itemID" => "id",
	        "itemName" => "name",
	        "itemCategory" => "product_category",
	        "itemShelfLife" => "product_shelf_life",
	        "minThreshold" => "threshold_min",
	        "maxThreshold" => "threshold_max",
	        "itemPrice" => "unit_price",
	        "itemDescription" => "description"
		);

	}



	//==================================================//
	//=================PRODUCTS VIEWS===================//
	//==================================================//
	public function test() {
		$this->load->model('products_model');
		$result = $this->products_model->get_all_products();

		
		echo json_encode($result);
	}

	public function view_test() 
	{
		$data["title"] = "Product Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("test_view", $data);
		$this->load->view("includes/footer.php");
	}
	public function index()
	{

		$user_session = $this->session->userdata('apollo_user');
		if($user_session === null){
			redirect(BASEURL.'login');
		}else{
			$this->product_management();
		}


		// if(IS_DEVELOPMENT === true)
		// {
		// 	$user_development = array(
		// 		"id" => 10000,
		// 		"email" => "apollo@email.com",
		// 		"username" => "apollo",
		// 		"password" => "62cc2d8b4bf2d8728120d052163a77df",
		// 		"company_id" => 1,
		// 		"role_id" => 1,
		// 		"account_type" => 1,
		// 		"first_name" => "apollo",
		// 		"last_name" => "developer",
		// 		"middle_name" => ""
		// 	);

		// 	$this->session->set_userdata('apollo_user', $user_development);

			
		// }
		// else
		// {
			/*LOGIN PROCESS*/
		//}
	}

	public function product_management() 
	{
		$data["title"] = "Item Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("products/product_management_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function product_inventory()
	{
		$data["title"] = "Product Inventory";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("products/product_inventory_1_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function add_product()
	{
		$data["title"] = "Product Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("products/add_product_view", $data);
		$this->load->view("includes/footer.php");
	}


	public function edit_product()
	{
		$data["title"] = "Product Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("products/edit_product_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function view_product()
	{
		$data["title"] = "Product Management";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("products/view_product_empty_view", $data);
		$this->load->view("includes/footer.php");	
	}

	public function add_product_empty() 
	{
		$data["title"] = "Product Inventory";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("products/add_product_empty_view", $data);
		$this->load->view("includes/footer.php");	
	}

	public function stock_inventory()
	{
		$data["title"] = "INVENTORY";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("products/stock_inventory_view", $data);
		$this->load->view("includes/footer.php");	

	}

	public function stock_product_activity()
	{
		$data["title"] = "Stock Item Activity";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("products/stock_product_activity_view", $data);
		$this->load->view("includes/footer.php");	
	}

	public function stock_storage_location()
	{
		$data["title"] = "INVENTORY - STOCK GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("products/stock_storage_location_view", $data);
		$this->load->view("includes/footer.php");	
	}

	public function consignee_inventory()
	{
		$data["title"] = "INVENTORY - CONSIGNEE GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("products/consignee_inventory_view", $data);
		$this->load->view("includes/footer.php");	
	}

	public function consignee_product_activity()
	{
		$data["title"] = "INVENTORY - CONSIGNEE GOODS";
		$this->load->view("includes/header", $data);
		$this->load->view("includes/side_nav");
		$this->load->view("includes/top_nav");
		$this->load->view("products/consignee_product_activity_view", $data);
		$this->load->view("includes/footer.php");	
	}

	//==================================================//
	//==============END OF PRODUCTS VIEWS===============//
	//==================================================//

	//==================================================//
	//=================ADD PRODUCTS=====================//
	//==================================================//

	public function product_composition_validation()
	{
		$product_composition = $this->input->post('product_composition');
		$arr_composition = json_decode($product_composition, true);
		$this->form_validation->set_message('product_composition_validation', 'Product Composition');
		if(count($arr_composition) > 0 && $arr_composition !== null)
		{
			$error = 0;
			foreach ($arr_composition as $value) {
				if( !isset($value['sku']) ){
					$error++;
				}
			}

			if($error)
			{
				return false;
			}
			else
			{
				return true;
			}

		}
		else
		{
			return true;
		}
	}

	public function add() 
	{
		/*OPTIONAL*/
		$image = $this->input->post('image');
		$description = $this->input->post('description');
		$product_composition = $this->input->post('product_composition');
		$arr_composition = json_decode($product_composition, true);

		/*REQUIRED*/
		$product_name = $this->input->post('product_name');
		$item_category = $this->input->post('item_category');
		$min_threshold = $this->input->post('min_threshold');
		$max_threshold = $this->input->post('max_threshold');
		$product_shelf_life  = $this->input->post('product_shelf_life');
		$unit_price = $this->input->post('unit_price');
		$sku = $this->input->post('sku');
		$guarantee = $this->input->post('guarantee');
		$unit_measure = $this->input->post('unit_measure');


		$this->form_validation->set_rules('product_name', 'Product Name', 'required|trim|max_length[160]|regex_match[/[A-Za-z0-9\'\"]+$/]');
		$this->form_validation->set_rules('item_category', 'Item Category', 'required|integer|max_length[4]');
		$this->form_validation->set_rules('min_threshold', 'Min Threshold', 'required|numeric|max_length[10]');
		$this->form_validation->set_rules('max_threshold', 'Max Threshold', 'required|numeric|max_length[10]');
		$this->form_validation->set_rules('product_shelf_life', 'Product Shelf Life', 'required|numeric|max_length[10]');
		$this->form_validation->set_rules('unit_price', 'Unit Price', 'required|numeric|max_length[10]');
		$this->form_validation->set_rules('unit_price', 'Unit Price', 'required|numeric|max_length[10]');
		$this->form_validation->set_rules('guarantee', 'Guarantee Percent', 'required');
		$this->form_validation->set_rules('unit_measure', 'Unit of Measure', 'required');

		$this->form_validation->set_rules('image', 'Image', 'trim');
		$this->form_validation->set_rules('description', 'Description', 'trim');
		$this->form_validation->set_rules('product_composition', 'Product Composition', 'trim');
		
		if(strlen($image) == 0){
			$image = DEFAULT_PROD_IMG;
		}
		if(!isset($description)){
			$description = "";
		}
 
		if($this->form_validation->run())
		{
			$this->load->model('products_model');

			$product_data = array(
				'sku' => $sku,
				'name' => $product_name,
				'product_category' => $item_category,
				'description' => $description,
				'image' => $image,
				'product_shelf_life' => $product_shelf_life,
				'threshold_min' => $min_threshold,
				'threshold_maximum' => $max_threshold,
				'unit_price' => $unit_price,
				'guarantee' => $guarantee,
				'unit_of_measure_id' => $unit_measure
			);

			$product_id = $this->products_model->add_product($product_data);
			if($product_id > 0)
			{
				// if(count($arr_composition) > 0){
				// 	foreach ($arr_composition as $key => $composition) {
				// 		$where = " WHERE sku = ".$composition['sku'];
				// 		$base_product = $this->products_model->get_product($where);

				// 		$base_product_data = array(
				// 			"sku" => $composition['sku'],
				// 			"name" => $base_product[0]['name']
				// 		);
				// 		$base_product_id = $this->products_model->add_base_product($base_product_data);

				// 		$composition_data = array(
				// 			"product_id" => $product_id,
				// 			"base_product_id" => $base_product_id
				// 		);
				// 		$composition_result = $this->products_model->add_product_composition($composition_data);
				// 	}

				// 	echo $composition_result;
				// }
				if(count($arr_composition > 0)) {
					foreach ($arr_composition as $key => $composition) {
						$composition_data = array(
							// "product_id" => $composition["product_id"],
							"product_id" => $product_id,
							"base_product_id" => $composition["base_product_id"],
							"quantity" => $composition["quantity"]
						);
						$composition_result = $this->products_model->add_product_composition($composition_data);
					}
				}

				$response = array(
					"status" => true,
					"message" => "success",
					"data" => $product_id
				);
				echo json_encode($response);
			} 
			else 
			{
				echo "false";
			}
		}
		else
		{
			$response["status"] = false;
			$response["message"] = "failed";
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
	}

	public function upload($paramCalled = false) 
	{
		$response = array();
		$arr_response = array();
		$status = false;
		$message = "Fail";
		if(isset($_FILES["attachment"])){
			$name = $_FILES["attachment"]["name"];
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 20; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    
		    $file_name = $randomString . "." .$ext;

		    move_uploaded_file($_FILES["attachment"]["tmp_name"], UPLOAD_IMAGE_PATH.$file_name);

		    $arr_response["path"] = UPLOAD_IMAGE_PATH.$file_name;
		    $arr_response["location"] = BASEURL . "uploads/images/" . $file_name;
		    $status = true;
		    $message = "Succes";
		}else{
			$arr_response = array("status" => "no image");
		}

		if($paramCalled){
			return $arr_response;
		}else{
			$response["status"] = $status;
			$response["message"] = $message;
			$response["data"] = $arr_response;
			echo json_encode($response);
		}
	}

	//==================================================//
	//===============END OF ADD PRODUCTS================//
	//==================================================//

	public function search()
	{
		$this->load->model('products_model');
		$this->load->model('categories/categories_model');

		$key = $this->input->post("key");
		$limit = $this->input->post("limit");
		$sort_field = $this->input->post("sort_field");
		$sorting = $this->input->post("sorting");

		$this->form_validation->set_rules('key', 'Search Value', 'required|trim');
		if(!isset($limit)){
			$limit = 30;
		}
		if(!isset($sort_field)){
			$sort_field = "id";
		}
		if(!isset($sorting)){
			$sorting = "ASC";
		}

		$where = " WHERE name LIKE '%{$key}%'";
		$sorting = " ORDER BY {$sort_field} {$sorting} ";
		$limit = " LIMIT ".$limit;

		$arr_products = array();
		$arr_products = $this->products_model->get_product($where, $sorting, $limit);

		foreach ($arr_products as $key => $prod)
		{
			$product_id = $prod["id"];
			$compositions = $this->products_model->get_product_composition($product_id);
			foreach ($compositions as $i => $composition) {
				$where = " WHERE id = ".$composition["base_product_id"];
				$product = $this->products_model->get_product($where);
				$product[0]["composition_quantity"] = $composition["quantity"];
				$compositions[$i] = $product[0];
			}

			$cat_sql_where = " WHERE c.id = ".$prod['product_category'];
			$category = $this->categories_model->get($cat_sql_where, "");
			$arr_products[$key]["category"] = $category[0];

			$arr_products[$key]["product_composition"] = $compositions;
		}

		header("Content-Type: application/json");
		$response["status"] = true;
		$response["message"] = "success";
		$response["data"] = $arr_products;
		echo json_encode($response);

	}

	public function get() 
	{
		$this->load->model('products_model');
		$this->load->model('categories/categories_model');

		$arr_params = array(
			'id' => ($this->input->post('id') !== null) ? $this->input->post('id') : false,
			'name' => ($this->input->post('name') !== null) ? $this->input->post('name') : false,
			'sku' => ($this->input->post('sku') !== null) ? $this->input->post('sku') : false,
			'product_category' => ($this->input->post('category') !== null) ? $this->input->post('category') : false,
			'offset' => ($this->input->post('offset') !== null) ? $this->input->post('offset') : false,
			'limit' => ($this->input->post('limit') !== null) ? $this->input->post('limit') : false,
			'sorting' => ($this->input->post('sorting') !== null) ? $this->input->post('sorting') : false,
			'sort_field' => ($this->input->post('sort_field') !== null) ? $this->input->post('sort_field') : false,
		);

		$counter = 0;
		$sql_where = "";
		$sql_limit = "";
		$sql_order = "";
		$sql_offset = "";

		foreach ($arr_params as $key => $param)
		{
			if($counter == 0)
			{
				if($key != 'offset' && $key != 'limit' && $key != 'sorting' && $key != 'sort_field'){
					if($param != false){
						$sql_where .= " WHERE ".$key." = '".$param."'";
						$counter++;
					}
				}
			}
			else if($key != 'offset' && $key != 'limit' && $key != 'sorting' && $key != 'sort_field')
			{
				if($param != false){
					$sql_where .= " AND ".$key." = '".$param."'";
				}
			}
		}

		if($arr_params['sort_field'] !== false && $arr_params['sorting'] !== false)
		{
			$sort = strtoupper($arr_params['sorting']);
			if($sort != 'ASC' && $sort != 'DESC'){
				$sort = 'ASC';
			}
			$sql_order .= " ORDER BY ".$arr_params['sort_field']." ".$sort;
		}

		if($arr_params['limit'] !== false){
			$sql_limit .= " LIMIT ".$arr_params['limit'];
			if($arr_params['offset'] !== false){
				$sql_offset .= " OFFSET ".$arr_params['offset'];
			}
		}


		$arr_products = $this->products_model->get_product($sql_where, $sql_order, $sql_limit, $sql_offset);
		$counter = 0;

		foreach ($arr_products as $key => $product)
		{
			// Get composition of each product
			$composition_data = $this->products_model->get_product_composition($product['id']);
			if($composition_data > 0) {
				foreach ($composition_data as $value) {
					// Get base product
					$item = $this->products_model->get_base_product($value['base_product_id']);
					if(count($item) > 0){
						$arr_products[$key]['product_composition'][] = $item[0];
					}else{
						$arr_products[$key]['product_composition'][] = array();
					}
						
				}	
			}

			// Get category data
			$cat_sql_where = " WHERE c.id = ".$product['product_category'];
			$category = $this->categories_model->get($cat_sql_where, "");
			//Modification of category fields
			unset($arr_products[$key]['product_category']);
			$arr_products[$key]['category_id'] = $category[0]['id'];
			$arr_products[$key]['category_name'] = $category[0]['name'];
			// Add here batch inventory after inventory module build
			$arr_products[$key]['batch_inventory'] =  array();
			
		}

		$response["status"] = true;
		$response["message"] = "success";
		$response["data"] = $arr_products;
		echo json_encode($response);
	}

	public function get_all_products() {
		$this->load->model('products_model');
		$this->load->model('categories/categories_model');
		$user_data = $this->session->userdata('apollo_user');
		$categories = $this->categories_model->get("WHERE c.company_id = ".$user_data["company_id"], "", "");

		foreach ($categories as $i => $cat) {
			$products = $this->products_model->get_product(" WHERE product_category = {$cat["category_id"]}  AND is_deleted = 0", $order = "", $limit = " LIMIT 50", $offset = "");
			$saved_products = array();
			foreach ($products as $key => $prod) {

				$product_id = $prod["id"];
				$compositions = $this->products_model->get_product_composition($product_id);
				
				foreach ($compositions as $x => $composition) {
					$where = " WHERE id = ".$composition["base_product_id"];
					$product = $this->products_model->get_product($where);
					$product[0]["composition_quantity"] = $composition["quantity"];
					$compositions[$x] = $product[0];
				}

				$prod["product_composition"] = $compositions;

				$saved_products[] = $prod;
			}

			$categories[$i]["products"] = $saved_products;
			$saved_products = array();
		}

		// $result = $this->products_model->get_all_products();

		

		header("Content-Type: application/json");
		$response = array(
			"status" => true,
			"message" => "",
			"data" => $categories
		);
		echo json_encode($response);
	}

	public function get_one_product($product_id = 0) {

		$this->load->model('products_model');
		$this->load->model('categories/categories_model');
		$user_data = $this->session->userdata('apollo_user');
		$categories = $this->categories_model->get("WHERE c.company_id = ".$user_data["company_id"], "", "");

		$product = $this->products_model->get_product(" WHERE id = {$product_id} AND is_deleted = 0", $order = "", $limit = " LIMIT 1", $offset = "");

		if(count($product) > 0){

			$compositions = $this->products_model->get_product_composition($product_id);

			foreach ($compositions as $x => $composition) {
				$where = " WHERE id = ".$composition["base_product_id"];
				$product_compo = $this->products_model->get_product($where);
				$product_compo[0]["composition_quantity"] = $composition["quantity"];
				$compositions[$x] = $product_compo[0];
			}


			$product[0]["product_composition"] = $compositions;
		}
		

		return $product;
	}

	public function get_all_product_category() {
		$this->load->model('products_model');
		$all_product_category = $this->products_model->get_all_product_category();
		
		$response = array(
			"status" => true,
			"message" => "",
			"data" => $all_product_category
		);
		header("Content-Type: application/json");
		echo json_encode($response);
	}

	public function get_expiry() 
	{
		if(!empty($this->input->post('item_category'))
			AND !empty(trim($this->input->post('item_category')))
			AND is_numeric($this->input->post('item_category')))
		{
			$this->load->model('products_model');
			$expiry_dates = $this->products_model->get_expiry($this->input->post('item_category'));
			print_r($expiry_dates);
		}
		else
		{
			$response["status"] = error;
			$response["message"] = "input error";
			$response["data"] = null;
			echo json_encode($response);
		}
	}

	public function update_qualifiers_validation()
	{
		$aliases = $this->aliases;

		$object_qualifiers = $this->input->post('qualifiers');
		$arr_qualifiers = json_decode($object_qualifiers, true);

		$this->form_validation->set_message('update_qualifiers_validation', 'Qualifiers Data Invalid');
		if(count($arr_qualifiers) > 0 && $arr_qualifiers !== null)
		{
			$error = 0;
			foreach ($arr_qualifiers as $key => $qualifier) {
				if( !isset($qualifier['field']) AND !isset($qualifier['operation']) AND !isset($qualifier['value']) )
				{
					$error++;
				}else{
					if( !isset( $aliases[ $qualifier["field"] ] ) 
						AND !isset( $qualifier["operation"] )
						AND !isset( $qualifier["value"]) )
					{
						$error++;
					}
				}
			}

			if($error > 0){
				return false;
			}else{
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	public function update_data_validation()
	{
		$aliases = $this->aliases;
		$object_data = $this->input->post('data');
		$arr_data = json_decode($object_data, true);

		$this->form_validation->set_message('update_data_validation', 'Data Format Invalid');
		
		if(count($arr_data) === 0 AND $arr_data === null)
		{
			return false;
		}
		else if(count($arr_data) == count($arr_data, COUNT_RECURSIVE))
		{
			//NOT MULTIDIMENSIONAL
			if(count($arr_data) > 0 AND $arr_data !== null)
			{
				$error = 0;
				foreach ($arr_data as $key => $data) 
				{
					if(!isset( $aliases[ $key ]))
					{
						$error++;
					}
				}

				if($error > 0){
					return false;
				}else{
					return true;
				}
			}
		}
		else if(count($arr_data) != count($arr_data, COUNT_RECURSIVE))
		{
			if(count($arr_data) > 0 AND $arr_data !== null)
			{
				$error = 0;
				foreach ($arr_data as $key => $data) 
				{
					foreach ($data as $i => $v) {
						if(!isset( $aliases[ $i ]))
						{
							$error++;
						}
					}
				}

				if($error > 0){
					return false;
				}else{
					return true;
				}
			}
		}
	}

	// public function update() 
	// {
	// 	$aliases = $this->aliases;

	// 	$object_qualifiers = $this->input->post('qualifiers');
	// 	$object_data = $this->input->post('data');
	// 	$arr_qualifiers = json_decode($object_qualifiers, true);
	// 	$arr_data = json_decode($object_data, true);
	// 	print_r($arr_data);
	// 	$conditions = "";

	// 	$this->form_validation->set_rules('qualifiers', 'Qualifiers', 'required|trim|callback_update_qualifiers_validation');
	// 	$this->form_validation->set_rules('data', 'Data', 'required|trim|callback_update_data_validation');

	// 	if($this->form_validation->run())
	// 	{
	// 		if(count($arr_qualifiers) > 0 AND $arr_qualifiers !== null)
	// 		{
	// 			foreach ($arr_qualifiers as $key => $qualifier) {

	// 				if(isset( $aliases[ $qualifier["field"] ] ) 
	// 					AND isset( $qualifier["operation"] )
	// 					AND isset( $qualifier["value"]) )
	// 				{
	// 					$field = $aliases[ $qualifier["field"] ];
	// 					$operation = $qualifier["operation"];
	// 					$value = $qualifier["value"];

	// 					if($key == 0)
	// 					{
	// 						$conditions .= " " . $field . $operation . "'" . $value . "'";
	// 					}
	// 					else
	// 					{
	// 						$conditions .= " AND " . $field . $operation . "'" . $value . "'";
	// 					}
	// 				}
	// 			}
	// 		}

	// 		$update_data = array();

	// 		if(count($arr_data) == count($arr_data, COUNT_RECURSIVE))
	// 		{
	// 			//NOT MULTIDIMENSIONAL
	// 			if(count($arr_data) > 0 AND $arr_data !== null)
	// 			{
	// 				foreach ($arr_data as $key => $data) 
	// 				{
	// 					if(isset( $aliases[ $key ]))
	// 					{
	// 						$field = $aliases[ $key ];
	// 	                    $update_data[$field] = $data;
	// 					}
	// 				}
	// 			}
	// 		}
	// 		else
	// 		{
	// 			if(count($arr_data) > 0 AND $arr_data !== null)
	// 			{
	// 				foreach ($arr_data as $key => $data) 
	// 				{
	// 					foreach ($data as $i => $v) {
	// 						if(isset( $aliases[ $i ]))
	// 						{
	// 							$field = $aliases[ $i ];
	// 		                    $update_data[$field] = $v;
	// 						}
	// 					}
	// 				}
	// 			}
	// 		}

	// 		$this->load->model('products_model');
	// 		return $this->products_model->update($conditions, $update_data);
	// 	}
	// 	else
	// 	{
	// 		// echo validation_errors();
	// 		$response["status"] = false;
	// 		$response["message"] = "Update has failed.";
	// 		$response["data"] = $this->form_validation->error_array();
	// 		echo json_encode($response);

	// 	}
	// }

	public function update() {

		$imagepath = $this->upload(true);


		$rows_changed = 0;
		$product_id = $this->input->post('itemID');
		$product_composition = $this->input->post('productComposition');
		$arr_productComposition = json_decode($product_composition, true);
		$update_data = array(
			"name" => $this->input->post('itemName'),
			"product_category" => $this->input->post('itemCategory'),
			"description" => $this->input->post('itemDescription'),
			"threshold_min" => $this->input->post('minThreshold'),
			"threshold_maximum" => $this->input->post('maxThreshold'),
			"product_shelf_life" => $this->input->post('itemShelfLife'),
			"unit_price" => $this->input->post('itemPrice'),
			"guarantee" => $this->input->post('guarantee'),
			"unit_of_measure_id" => $this->input->post('unit_measure')
		);

		if(!array_key_exists("status", $imagepath)){
			$update_data["image"] = $imagepath["location"];
		}

		$this->load->model('Products_model');	
		$this->Products_model->delete_product_composition($product_id);
		
		foreach ($arr_productComposition as $key => $product_composition) {
			$arr_product = array(
				"product_id" => $product_id,
				"base_product_id" => $product_composition["base_product_id"]
			);
			$rows_changed = $rows_changed + $this->Products_model->update_product_composition($arr_product);
		}
		$rows_changed = $rows_changed + $this->Products_model->update($product_id, $update_data);
		
		header("Content-Type: application/json");
		
		if($rows_changed > 0) {

			$product_data = $this->get_one_product($this->input->post('itemID'));

			$response["status"] = true;
			$response["message"] = "Product updated.";
			$response["data"] = $rows_changed;
			$response["product_data"] = $product_data[0];
			echo json_encode($response);
		} else {
			$response["status"] = false;
			$response["message"] = "Product not updated.";
			$response["data"] = $rows_changed;
			echo json_encode($response);
		}
	}

	public function delete() {
		$rows_changed = 0;
		$product_id = $this->input->post('id');
		$delete_details = array(
			"is_deleted" => 1
		);
		// echo $product_id;

		$this->load->model('products_model');	
		$result = $this->products_model->delete($product_id, $delete_details);
		if($result > 0) {
			$response = array(
				"status" => true,
				"message" => "Product deleted.",
				"data" => ""
			);
 		} else {
 			$response = array(
				"status" => false,
				"message" => "Product not deleted.",
				"data" => ""
			);
 		}

 		header("Content-Type: application/json");
 		echo json_encode($response);
		
		// foreach ($arr_productComposition as $key => $product_composition) {
		// 	$arr_product = array(
		// 		"product_id" => $product_id,
		// 		"base_product_id" => $product_composition["base_product_id"]
		// 	);
		// 	$rows_changed = $rows_changed + $this->Products_model->update_product_composition($arr_product);
		// }
		// $rows_changed = $rows_changed + $this->Products_model->update($product_id, $update_data);
		// echo $rows_changed;
	}
	

	private function _generate_sku() 
	{
		$this->load->model('products_model');
		$sku = $this->products_model->get_sku();
		$sku_value = intval($sku[0]["sku"]);
		if($sku_value > 0) 
		{
			$sku_value = $sku_value + 1;
		} else {
			$sku_value = 1;
		}

		$new_sku = "";
		$sku_length = strlen($sku_value);
		$limit = 9 - $sku_length;
		for($i = 1; $i <= $limit; $i++){
			$new_sku .= 0;
		}
		$new_sku = $new_sku.$sku_value;
		
		$where_query = "WHERE sku = '".$new_sku."'";
		$existing_sku = $this->products_model->get_product($where_query);
		if(count($existing_sku) > 0) {
			$this->_generate_sku();
		} else {
			return $new_sku;
		}
	}

	public function generate_sku() 
	{
		$this->load->model('products_model');
		$sku = $this->products_model->get_sku();
		
		if(!empty($sku) && $sku != "") {
			// echo "di empty";
			$sku_value = intval($sku[0]["sku"]);
			if($sku_value > 0) 
			{
				$sku_value = $sku_value + 1;
			} else {
				$sku_value = 1;
			}
		} else {
			// echo "empty";
			$sku_value = 1;
		}

		$new_sku = "";
		$sku_length = strlen($sku_value);
		$limit = 9 - $sku_length;
		for($i = 1; $i <= $limit; $i++){
			$new_sku .= 0;
		}
		$new_sku = $new_sku.$sku_value;
		
		$where_query = "WHERE sku = '".$new_sku."'";
		$existing_sku = $this->products_model->get_product($where_query);
		if(count($existing_sku) > 0) {
			$this->_generate_sku();
		} else {
			// echo $new_sku;
			$response = array(
				"status" => true,
				"message" => "",
				"data" => $new_sku
			);

			header("Content-Type: application/json");
			echo json_encode($response);
		}
	}

	public function archive() 
	{
		if(!empty($this->input->post('product_id'))
			AND !empty(trim($this->input->post('product_id')))
			AND is_numeric($this->input->post('product_id')))
		{
			$this->load->model('products_model');
			$result = $this->products_model->archive($this->input->post('product_id'));
			if($result > 0) 
			{
				$response["status"] = true;
				$response["message"] = "Succesfully deleted!";
				$response["data"] = null;
				echo json_encode($response);
			}
			else
			{
				$response["status"] = false;
				$response["message"] = "Delete has failed!";
				$response["data"] = null;
				echo json_encode($response);
			}
		}
		else
		{
			$response["status"] = false;
			$response["message"] = "Input has an error!";
			$response["data"] = null;
			echo json_encode($response);
		}
	}

	public function scheduler()
	{
		$arr_params = array(
			'date_check' => $this->input->post('date_check'),
			'days_threshold' => $this->input->post('days_threshold')
		);

		// $date_check = $this->input->post('date_check');
		// $this->load->model('products_model');
		// $products = $this->products_model->get_all_products;
		// foreach ($products as $product) 
		// {
		// 	$arr_expiration_days = array(
		// 		'item_id' => $product["id"],
		// 		'item_name' => $product["name"],
		// 		'days_left' => $product["days_threshold"] - $date_check
		// 	);
		// }
		$days_left = $arr_params["days_threshold"] - $arr_params["date_check"];
		echo $days_left . ' Day/s left.';
	}

	public function get_product_group()
	{
		$search_field  = $this->input->post('search_field');
		$search_keyword = $this->input->post('search_keyword');
		$sorting  = $this->input->post('sorting');
		$sort_field = $this->input->post('sort_field');

		$this->form_validation->set_rules('search_field', 'Search Field', 'required|trim');
		$this->form_validation->set_rules('search_keyword', 'Search Keyword', 'required|trim|min_length[3]|regex_match[/[A-Za-z0-9\'\"]+$/]');
		$this->form_validation->set_rules('sorting', 'Sorting', 'alpha_numeric');
		$this->form_validation->set_rules('sort_field', 'Sort Field', 'alpha_numeric');

		$sorting = strtoupper($sorting);

		if(!isset($sorting) || ($sorting != 'ASC' && $sorting != 'DESC') ){
			$sorting = "ASC";
		}

		if(!isset($sort_field) || $sort_field == ''){
			$sort_field = "name";
		}

		if($this->form_validation->run() === FALSE)
		{
			echo validation_errors();
		}
		else
		{
			$this->load->model('products_model');
			$categories = $this->products_model->search_categories($search_field, $search_keyword, $sorting, $sort_field);
			$result = array();
			foreach ($categories as $key => $category) {
				$products = $this->products_model->get_product(" WHERE p.product_category = ".$category['id']);
				$result[] = array(
					"category_name" => $category['name'],
					"category_id" => $category['id'],
					"products" => $products
				);
			}

			// echo json_encode($result);
			$response["status"] = true;
			$response["message"] = null;
			$response["data"] = $result;
			echo json_encode($response);
		}
	}

	public function get_unit_of_measure(){
		
		$this->load->model('products_model');
		$measures = $this->products_model->get_unit_of_measure();


		header("Content-Type: application/json");
		$response = array(
			"status" => true,
			"message" => "",
			"data" => $measures
		);
		echo json_encode($response);

	}

	public function test_ajax() {
		$test = array(
			"name" => "Dru Moncatar",
			"tester" => "tester"
		);
		header("Content-Type: application/json");
		echo json_encode($test);
	}

	public function test_add() {
		$data = array(
			"image" => $this->input->post('image'),
			"product_name" => $this->input->post('product_name'),
			"item_category" => $this->input->post('item_category'),
			"min_threshold" => $this->input->post('min_threshold'),
			"max_threshold" => $this->input->post('max_threshold'),
			"product_shelf_life" => $this->input->post('product_shelf_life'),
			"unit_price" => $this->input->post('unit_price'),
			"description" => $this->input->post('description'),
			"product_composition" => $this->input->post('product_composition')
		);
		header("Content-Type: application/json");
		echo json_encode($data);
	}

	public function set_product_is_active() {
		$product_id = $this->input->post('id');
		$is_active = $this->input->post('is_active');

		$this->load->model("products_model");
		$result = $this->products_model->set_product_is_active($product_id, $is_active);
		
		if($result > 0) {

			if($is_active == 1) {
				$response = array(
					"status" => true,
					"message" => "Product activated.",
					"data" => ""
				);
			} else {
				$response = array(
					"status" => true,
					"message" => "Product deactivated.",
					"data" => ""
				);	
			}
			
		} else {
			$response = array(
				"status" => false,
				"message" => "Product not updated.",
				"data" => ""
			);
		}

		header("Content-Type: application/json");
		echo json_encode($response);
	}
}