<div class="modal-container" id="generate-data-view" >
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>


<div class="main min-width-1200">
			<div class="center-main">	
				<div class="mod-select">			
					<div class="select f-left width-280px margin-top-20 margin-left-5">
						<select class="transform-dd">
							<option value="Displaying All Products"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Item</td></tr><tr><td>Keywords:</td><td>American</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>Date: 01-Mar-2016 - 10-Mar-2016; Status: all / All Categories</td></tr><tr><td>Sort By:</td><td>Receiving No.,; Ascending</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Item</td></tr><tr><td>Keywords:</td><td>American</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>Date: 01-Mar-2016 - 10-Mar-2016; Status: all / All Categories</td></tr><tr><td>Sort By:</td><td>Receiving No.,; Ascending</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Item</td></tr><tr><td>Keywords:</td><td>American</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>Date: 01-Mar-2016 - 10-Mar-2016; Status: all / All Categories</td></tr><tr><td>Sort By:</td><td>Receiving No.,; Ascending</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Item</td></tr><tr><td>Keywords:</td><td>American</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>Date: 01-Mar-2016 - 10-Mar-2016; Status: all / All Categories</td></tr><tr><td>Sort By:</td><td>Receiving No.,; Ascending</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Item</td></tr><tr><td>Keywords:</td><td>American</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>Date: 01-Mar-2016 - 10-Mar-2016; Status: all / All Categories</td></tr><tr><td>Sort By:</td><td>Receiving No.,; Ascending</td></tr></tbody></table></div>"></option>
						</select>
					</div>
					<div class="clear"></div>
				</div>

				<div class=" margin-top-20 f-left">
					<p class="display-inline-mid margin-right-10 margin-left-5 font-15 font-bold">Display Inventory By:</p>
					<div class="wh-select display-inline-mid">					
						<div class="select stocks medium">
							<select class="transform-dd">
								<option value="sto">Storage Location</option>
								<option value="prod">Item</option>						
							</select>
						</div>
					</div>
				</div>
				<button class="f-right general-btn margin-right-5 dropdown-btn margin-top-20"> View Filter / Search </button>
				<div class="clear"></div>

				<div class="dropdown-search margin-left-5 margin-right-5 ">
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr>
								<td>Search</td>
								<td>
									<div class="select">
										<select class="transform-dd">
											<option value="Product Name">Item Name</option>
										</select>
									</div>
								</td>
								<td>
									<input type="text" class="t-small width-100per" />
								</td>
							</tr>
							<tr>
								<td>Sort By</td>
								<td>
									<div class="select italic">
										<select class="transform-dd">
											<option value="Product Name">Item Name</option>
										</select>
									</div>
								</td>
								<td>
									<div class="select italic">
										<select class="transform-dd">
											<option value="Product Name">Ascending</option>
										</select>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10">
						<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
						<p class="display-inline-mid font-15 font-bold">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 margin-right-30 padding-left-20 padding-right-20">Search</button>
					<div class="clear"></div>
					<div class="save-result-container">
					</div>
				</div>


				<!--first head panel group-->
				<span id="display-storage-record">

					<div class="panel-group margin-top-20 display-storage-item storage-item">
						<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							<!-- step 1 -->
							<div class="panel-heading panel-heading">
								<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">							
									<i class="fa fa-caret-down margin-right-5"></i>
									<span class="display-storage-name">Warehouse 1</span>
								</a>					
								<p class="f-right font-15 font-500  "><span class="font-green vertical-baseline display-storage-quantity">12600</span>
									/<span class="display-storage-capacity">50000</span> KG in Storage</p>
								<div class="clear"></div>
							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body bggray-white">
								<!--first panel-->
									<span class="display-product-record">
										<div class="panel-group margin-top-20 display-product-item product-item">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading trigger-collapse">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														<span class="display-product-sku-name">SKU #1234567890 - Japanese Corn</span>
													</a>
													<p class="f-right font-15 font-500 ">Total Quantity: <span class="font-green vertical-baseline display-product-total-quantity">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center  width-14percent">Batch Name.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center width-14percent ">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center width-30percent">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center width-14percent">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center width-14percent">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center width-14percent">Status</th>												
																</tr>
															</thead>
														</table>
														<span class="display-batch-record">

															<div class="tbl-like text-left display-batch-item batch-item batches-displayed">
																<div class="first-tbl mngt-tbl-content height-auto min-height-50px font-0">
																	<div class="display-inline-mid width-14percent text-center">
																		<p class="font-400 f-none font-14 width-100percent display-batch-name">12345678</p>
																	</div>
																	<div class="display-inline-mid width-14percent text-center">
																		<p class="font-400 f-none font-14 width-100percent display-batch-date">01-Mar-2016</p>
																	</div>
																	<div class="display-inline-mid width-30percent text-left display-batch-location">
																		<!-- <p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																		<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																		<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																		<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																		<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																		<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																		<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																		<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																		<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p> -->
																							
																	</div>
																	<div class="display-inline-mid width-14percent text-center">
																		<p class="font-400 f-none font-14 width-100percent display-batch-quantity">250 Bags</p>
																	</div>
																	<div class="display-inline-mid width-14percent text-center">
																		<p class="font-400 f-none font-14 width-100percent display-batch-life">300 days</p>
																	</div>
																	<div class="display-inline-mid width-14percent text-center">
																		<p class="font-400 f-none font-14 width-100percent display-batch-status">Unrestricted</p>
																	</div>
																</div>
																<div class="hover-tbl text-center">
																	
																		<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																	
																		<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																	
																		<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																	
																		<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																	
																		<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																		<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
																	
																</div>
															</div>
														</span>

														

													</div>
												</div>
											</div>
										</div>
									</span>
									
								<!--end of first panel-->

								</div>
							</div>
						</div>
					</div>
				</span>
				<!--end head panel group-->

				

				

				




			</div>
		</div>
		</section>

		<!--modal start update status -->
		<div class="modal-container" id="update-status-modal" modal-id="update-status">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Update Status</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-bottom-20">
						<p class="font-14 font-400 display-inline-mid margin-top-5 no-margin-all padding-right-20">New Status:</p>
						<div class="select  display-inline-mid width-250px">
							<select class="medium select-batch-status">
								<option value="1">Quality Assurance</option>
								<option value="2">Restricted</option>
							</select>
						</div>
					</div>
					<div>
						<p class="font-14 font-400 display-inline-mid margin-top-5 no-margin-all padding-right-20 padding-bottom-20">Please provide a reason for status change:</p>
						<textarea id="batch-update-reason" class="height-150px"></textarea>

					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="update-status-modal-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20  modal-trigger" modal-target="update-status-confirmation">Confirm</button>
				</div>
			</div>	
		</div>
		<!--modal end update status -->

		<!--modal start update status confirmation -->
		<div class="modal-container" modal-id="update-status-confirmation">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Update Status</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Batch Name <span id="display-batch-name-new-status">12345678</span> status changed to <span id="display-batch-new-status">Quality Assurance</span></p>
					</div>
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end update status confirmation -->

		<!--modal start of adjust quantity -->
		<div class="modal-container" id="adjust-quantity-modal" modal-id="adjust-quantity">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Adjust Quantity</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-bottom-20">
						<p class="font-14 font-400 display-inline-mid width-25percent no-margin-all">Batch Name:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-all" id="display-adjust-batch-name">12345678</p>
					</div>

					<div class="padding-bottom-10">
						<p class="font-14 font-400 display-inline-mid width-25percent no-margin-all">Current Quantity:</p>
						<p class="font-14 font-400 display-inline-mid width-25percent no-margin-all" id="display-adjust-batch-quantity">300 Bags</p>
						<p class="font-14 font-400 display-inline-mid italic font-green no-margin-all padding-left-35"><span id="display-adjust-batch-state"></span></p>
					</div>
					<div class="font-0">
						<p class="font-14 font-400 display-inline-top width-25percent no-margin-all padding-top-15">Adjustment:</p>
						<div class="display-inline-mid width-75percent">
							<div class="width-100percent padding-top-6">
								<div class="display-inline-mid width-40percent">
									<input type="radio" class="width-20px display-inline-mid no-margin-all transform-scale default-cursor" id="increaseBy" name="increaseAndDecrease" checked>
									<label for="increaseBy" class="font-14 font-400 display-inline-mid default-cursor">Increase By</label>
								</div>
								<div class="display-inline-mid">
									<input type="text" class="width-100px display-inline-mid" id="text-increase">
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-left-20 ">Bags</p>
								</div>
							</div>
							<div class="width-100percent padding-top-10">
								<div class="display-inline-mid width-40percent">
									<input type="radio" class="width-20px display-inline-mid no-margin-all transform-scale default-cursor" id="decreaseBy" name="increaseAndDecrease">
									<label for="decreaseBy" class="font-14 font-400 display-inline-mid default-cursor">Decrease By</label>
								</div>
								<div class="display-inline-mid">
									<input type="text" class="width-100px display-inline-mid" id="text-decrease">
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-left-20">Bags</p>
								</div>
							</div>
						</div>
					</div>
					<div class="padding-top-20">
						<p class="font-14 font-400 no-margin-all padding-bottom-10">Please provide a reason for batch adjustment:</p>
						<textarea class=" height-100px" id="display-adjust-batch-reason"></textarea>
					</div>
					
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="adjust-quantity-modal-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 ">Adjust Quantity</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of adjust quantity-->

		<!--modal start of adjust quantity password -->
		<div class="modal-container" modal-id="adjust-quantity-password">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Adjust Quantity</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<p class="font-14 font-400 no-margin-hor">Please input your password to proceed:</p>
					<p class="font-14 font-400 display-inline-mid width-20percent no-margin-all">Password:</p>
					<input type="password" class="t-medium" id="set-search-user-password">
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="adjust-quantity-search-password" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 " >Enter</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of adjust quantity password-->

		<!--modal start adjust quantity confirmation -->
		<div class="modal-container" modal-id="adjust-quantity-confirmation">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Adjust Quantity</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Batch Name <span id="display-adjustment-batch-name">12345678</span> quantity has been adjusted.</p>
						<p class="font-14 font-400 white-color ">New Quantity: <span id="display-new-adjusted-qty">300 Bags</span></p>
						<p class="font-14 font-400 white-color">Previous Quantity: <span id="display-old-adjusted-qty">300 Bags</span></p>

					</div>
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end adjust quantity confirmation -->

		<!--modal start remove batch -->
		<div class="modal-container" id='remove-batch-modal' modal-id="remove-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Remove Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div>
						<p class="font-14 font-400 display-inline-mid margin-top-5 no-margin-all padding-right-20 padding-bottom-20">Please provide a reason for batch removal:</p>
						<textarea class="height-150px" id="remove-batch-textarea"></textarea>

					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="remove-batch-modal-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20  modal-trigger" modal-target="remove-batch-confirmation">Remove Batch</button>
				</div>
			</div>	
		</div>
		<!--modal end remove batch -->

		<!--modal start remove batch confirmation -->
		<div class="modal-container" modal-id="remove-batch-confirmation">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Adjust Quantity</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Batch No. <span id="remove-batch-confirmation-caption">12345678</span> has been removed.</p>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end remove batch confirmation -->

		<!--modal start of repack batch -->
		<div class="modal-container" id="repack-batch-modal" modal-id="repack-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Repack Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-hor">Batch Name:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor" id="display-repack-batch-name">12345678</p>
					</div>
					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-hor">Current Quantity:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor" id="display-repack-batch-quantity">300 Bags</p>
					</div>

					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-all">Repacking Quantity:</p>
						<input type="text" class="width-100px display-inline-mid" id="set-repack-batch-quantity">
						<div class="select medium select-dropdown">
						<select>
							<option value="Bags">Bags</option>
						</select>
						</div>
					</div>

				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="repack-batch-modal-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" >Repack Batch</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of repack batch-->

		<!--modal start repack batch confirmation -->
		<div class="modal-container" modal-id="repack-batch-confirmation">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Repack Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>
				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Batch Name <span id="display-success-repack-batch-name">12345678</span> quantity has been repacked.</p>
						<p class="font-14 font-400 white-color ">New Quantity: <span id="display-success-repack-batch-new-qty">30</span> Bags</p>
						<p class="font-14 font-400 white-color">Previous Quantity: <span id="display-success-repack-batch-old-qty">30</span> Bags</p>

					</div>
				</div>
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end repack batch confirmation -->

		<!--modal start of return batch -->
		<div class="modal-container" id="return-batch-modal" modal-id="return-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Return Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-hor">Batch Name:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor" id="display-return-batch-name">12345678</p>
					</div>
					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-hor">Current Quantity:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor" id="display-return-batch-quantity">300 Bags</p>
					</div>

					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-all">Returning Quantity:</p>
						<input type="text" class="width-100px display-inline-mid" id="display-return-batch-quantity-entry">
						<p class="font-14 font-400 display-inline-mid no-margin-ver">KG</p>
					</div>
					<div class="padding-top-20">
						<p class="font-14 font-400 no-margin-hor">Please provide a reason for batch returns:</p>
						<textarea class="height-150px" id="display-return-batch-reason"></textarea>
					</div>

				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="return-batch-modal-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" >Return Batch</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of return batch-->

		<!--modal start return batch confirmation -->
		<div class="modal-container" modal-id="return-batch-confirmation">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Return Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">A part of Batch Name <span id="display-success-return-batch-name">12345678</span> has been returned.</p>
						<p class="font-14 font-400 white-color ">Quantity Returned: <span id="display-success-return-batch-returned-quantity">50 Bags</span></p>
					</div>
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end return batch confirmation -->


		<!--modal start of split batch -->
		<div class="modal-container" id="split-batch-modal" modal-id="split-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Split Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div>
						<p class="font-14 font-bold black-color display-inline-mid no-margin-hor width-30percent">Batch Name:</p>
						<p class="font-14 font-bold black-color display-inline-mid no-margin-hor" id="display-split-batch-name">12345678</p>
					</div>
					<div>
						<p class="font-14 font-400 black-color display-inline-mid no-margin-hor width-30percent">Current Quantity:</p>
						<p class="font-14 font-400 black-color display-inline-mid no-margin-hor" id="display-split-batch-quantity">300 Bags</p>
					</div>

					<div class="padding-bottom-10">
						<p class="font-14 font-400 black-color display-inline-mid no-margin-all f-left">Batch Splits</p>
						<a href="javascript:void(0)" class="font-14 font-400 blue-color f-right" id="split-batch-add">
							+Add Batch Split
						</a>
						<div class="clear"></div>
					</div>

					<span id="split-add-batch-record">
						<div class="padding-all-5 bggray-white margin-bottom-10 split-add-batch-item">
							<p class="font-14 font-400 black-color no-margin-ver display-inline-mid insert-number">1. </p>
							<input type="text" class="width-400px display-inline-mid set-split-batch-name-input" placeholder="Batch Name">
							<div class="padding-top-10 padding-bottom-10 padding-left-25">
								<p class="font-14 font-400 black-color no-margin-ver display-inline-mid">Quantity: </p>
								<input type="text" class="t-small display-inline-mid margin-right-7 set-split-qunatity-input">
								<div class="select medium select-dropdown">
									<select class="unit-of-measure">
										<!-- <option value="Bags">Bags</option> -->
									</select>
								</div>
							</div>
						</div>
					</span>

					<!-- <div class="padding-all-5 bggray-white margin-bottom-10">
						<p class="font-14 font-400 black-color no-margin-ver display-inline-mid">2. </p>
						<input type="text" class="width-400px display-inline-mid" placeholder="Batch Name">
						<div class="padding-top-10 padding-bottom-10 padding-left-25">
							<p class="font-14 font-400 black-color no-margin-ver display-inline-mid">Quantity: </p>
							<input type="text" class="t-small display-inline-mid margin-right-7">
							<div class="select medium">
								<select>
									<option value="Bags">Bags</option>
								</select>
							</div>
						</div>
					</div>

					<div class="padding-all-5 bggray-white margin-bottom-10">
						<p class="font-14 font-400 black-color no-margin-ver display-inline-mid">3. </p>
						<input type="text" class="width-400px display-inline-mid" placeholder="Batch Name">
						<div class="padding-top-10 padding-bottom-10 padding-left-25">
							<p class="font-14 font-400 black-color no-margin-ver display-inline-mid">Quantity: </p>
							<input type="text" class="t-small display-inline-mid margin-right-7">
							<div class="select medium">
								<select>
									<option value="Bags">Bags</option>
								</select>
							</div>
						</div>
					</div> -->
				</div>
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="split-batch-modal-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 " >Split Batch</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of split batch-->

	<!--modal start split batch confirmation -->
		<div class="modal-container" modal-id="split-batch-confirmation">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Split Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<p class="font-14 font-400 no-margin-hor black-color">Batch Name <span id="display-split-success-batch-name">12345678</span> has been split into:</p>
					<table class="tbl-4c3h">
						<thead>
							<tr>
								<th class="no-padding-left black-color">Batch Name</th>
								<th class="no-padding-left black-color">Quantity</th>
							<tr>
						</thead>
						<tbody class="text-center" id="display-split-success-record">
							<tr class="display-split-success-item">
								<td class="font-14 font-400 no-margin-hor black-color display-split-success-batch-name-new">12345678A</td>
								<td class="font-14 font-400 no-margin-hor black-color display-split-success-batch-quantity">200 Bags</td>
							</tr>
							<!-- <tr>
								<td class="font-14 font-400 no-margin-hor black-color">12345678A</td>
								<td class="font-14 font-400 no-margin-hor black-color">100 Bags</td>
							</tr> -->
							
						</tbody>
					</table>
				</div>

				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="">OK</button>
				</div>
			</div>	
		</div>
		<!--- modal ending split batch confirmation-->