<div class="main min-width-1200">
			<div class="center-main">	
				<div class="mod-select">			
					<div class="select f-left width-280px margin-top-20 margin-left-5">
						<select>
							<option value="Displaying All Products"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class=" margin-top-20 f-left">
					<p class="display-inline-mid margin-right-10 margin-left-5 font-15 font-bold">Display Inventory By:</p>
					<div class="wh-select display-inline-mid">					
						<div class="select consignee medium">
							<select>
								<option value="prod">Item</option>
								<option value="sto">Storage Location</option>							
							</select>
						</div>
					</div>
				</div>
				<div class="clear"></div>


				<div class="width-100percent">
					<div class="f-left margin-top-15 margin-left-5">
						<p class="display-inline-mid font-15 font-bold"> Browse By:</p>
						<i class="fa fa-list display-inline-mid margin-left-10 font-20 icon-grid default-cursor listing"></i>
						<i class="fa fa-th-large display-inline-mid margin-left-10 font-20 icon-table default-cursor pictures"></i>
					</div>
					<button class="f-right general-btn margin-right-5 dropdown-btn"> View Filter / Search </button>
					<div class="clear"></div>
				</div>
				
				<!-- search button  -->
				<div class="dropdown-search margin-left-5 margin-right-5 ">
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr>
								<td>Search</td>
								<td>
									<div class="select">
										<select>
											<option value="Product Name">Item Name</option>
										</select>
									</div>
								</td>
								<td>
									<input type="text" class="t-small width-100per" />
								</td>
							</tr>
							<tr>
								<td>Sort By</td>
								<td>
									<div class="select italic">
										<select>
											<option value="Product Name">Item Name</option>
										</select>
									</div>
								</td>
								<td>
									<div class="select italic">
										<select>
											<option value="Product Name">Ascending</option>
										</select>
									</div>
								</td>
							</tr>
							
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10">
						<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
						<p class="display-inline-mid font-15 font-bold">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 margin-right-30 padding-left-20 padding-right-20">Search</button>
					<div class="clear"></div>
					<div class="save-result-container">
						

					</div>

				</div>

				<div class="first-content margin-top-20 text-left font-0">
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/consignee_product_activity" class="black-color height-200px ">
							<div class="width-100percent fix-image">
								<img src="../assets/images/australian-wheat.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Australian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">66 Batches (7952 KG)</p>
						</a>
					</div>

					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/consignee_product_activity" class="black-color height-200px">
							<div class="width-100percent fix-image">
								<img src="../assets/images/ukrainian wheat.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Ukrainian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">96 Batches (8274 KG)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/consignee_product_activity" class="black-color height-200px">
							<div class="width-100percent  fix-image">
								<img src="../assets/images/brazilian-wheat.jpg" class="width-100percent  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Brazilian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">49 Batches (1750 KG)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/consignee_product_activity" class="black-color height-200px">
							<div class="width-100percent fix-image ">
								<img src="../assets/images/romanian-wheat.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Romanian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">67 Batches (174 KG)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/consignee_product_activity" class="black-color height-200px ">
							<div class="width-100percent fix-image ">
								<img src="../assets/images/indian-wheat.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Indian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">28 Batches (7084)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/consignee_product_activity" class="black-color height-200px">
							<div class="width-100percent fix-image  ">
								<img src="../assets/images/black-sea-wheat.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Black Sea Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">66 Batches (7952 KG)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/consignee_product_activity" class="black-color height-200px">
							<div class="width-100percent fix-image">
								<img src="../assets/images/bulgarian-wheat.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Bulgarian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">96 Batches (8274 KG)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/consignee_product_activity" class="black-color height-200px">
							<div class="width-100percent fix-image ">
								<img src="../assets/images/us-soya.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>United State Soya</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">49 Batches (1750 KG)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/consignee_product_activity" class="black-color height-200px">
							<div class="width-100percent fix-image ">
								<img src="../assets/images/argentina-soya.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Argentina Soya</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">67 Batches (174 KG)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/consignee_product_activity" class="black-color height-200px">
							<div class="width-100percent fix-image">
								<img src="../assets/images/argentina-corn.jpg" class="width-100per "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Argentina Corn</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">28 Batches (7084 KG)</p>
						</a>
					</div>				
				</div>
				<div class="second-content">
					<table class="tbl-4c3h margin-top-20">
						<thead>
							<tr>
								<th class="black-color width-25percent no-padding-left">Item Name</th>
								<th class="black-color width-25percent no-padding-left">SKU Number</th>
								<th class="black-color width-25percent no-padding-left">No. of Batches</th>
								<th class="black-color width-25percent no-padding-left">Total Quantity</th>
																			
							</tr>
						</thead>
					</table>
					<div class="tbl-like font-0">
						<div class="first-tbl no-padding-all mngt-tbl-content text-left">
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">Australian Wheat</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">983122340</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">66</p>
							</div> 
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">7952 KG</p>
							</div>
							
							
						</div>
						<div class="hover-tbl">
							<a href="consignee-product-activity.php">
								<button class="btn general-btn">View Item Details</button>
							</a>
						</div>
					</div>
					<div class="tbl-like font-0 text-center">
						<div class="first-tbl no-padding-all mngt-tbl-content text-left">
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14 ">Australian Wheat</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">983122340</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">66</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">7952 KG</p>
							</div>										
						</div>
						<div class="hover-tbl">									
							<a href="consignee-product-activity.php">
								<button class="btn general-btn">View Item Details</button>
							</a>											
						</div>
					</div>
					<div class="tbl-like font-0 ">
						<div class="first-tbl no-padding-all mngt-tbl-content text-left">
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">Chinese Wheat</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">983122340</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">66</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">7952 KG</p>
							</div>
						</div>
						<div class="hover-tbl">
							<a href="consignee-product-activity.php">
								<button class="btn general-btn">View Item Details</button>
							</a>
						</div>
					</div>
					<div class="tbl-like font-0">
						<div class="first-tbl no-padding-all mngt-tbl-content text-left">
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">Italian Wheat</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">983122340</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">66</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">7952 KG</p>
							</div>
						</div>
						<div class="hover-tbl">
							<a href="consignee-product-activity.php">
								<button class="btn general-btn">View Item Details</button>
							</a>
						</div>
					</div>																	
				</div>

			</div>
		</div>
				
	</section>