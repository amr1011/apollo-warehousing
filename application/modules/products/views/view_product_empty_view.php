	
			<div class="main min-width-1024">
				<div class="breadcrumbs no-margin-left padding-left-50">
					<ul>
						<li><a href="<?php echo BASEURL . "products/product_management" ?>" class=" font-400 font-15 black-color">Item Management</a></li>
						<li><span>></span></li>
						<li><strong class="black-color font-bold"><span id="view_product_breadcrumb">Australian Wheat</span></strong></li>
					</ul>
				</div>
				<div class="semi-main">
					<div class="f-right">
						<a href="<?php echo BASEURL; ?>products/edit_product">
							<button id="view_product_edit_button" type="button" class="general-btn ">Edit Item Details</button>						
						</a>
					</div>
					<div class="clear"></div>
					<div class="border-top border-blue bggray-white box-shadow-light margin-top-20 padding-all-20">
						<!-- <img src="../assets/images/australian-wheat.jpg" class="width-100per"> -->
						<!-- <img class="img-responsive f-left width-30per margin-top-10" src="../assets/images/australian-wheat.jpg"> -->
						<span id="image_view"></span>
						<div class="f-left width-70per margin-left-20">
							<table class="tbl-4c3h width-100per">
								<tbody>
									<tr>
										<td colspan="4" class="text-left">
											<p class="font-bold font-20 black-color">SKU # <span id="view_product_sku">1234567890</span> - <span id="view_product_name">Australian Wheat</span></p>
										</td>
									</tr>
									<tr>
										<td class="text-left"> 
											<p class="font-bold black-color">Item Category</p>
										</td>
										<td class="text-left">
											<p id="view_product_category" class="black-color"></p>
										</td>
										<td class="text-left">
											<p class="font-bold black-color">Quantity Threshold</p>
										</td>
										<td class="text-left">
											<p id="view_product_quantity_threshold" class="black-color"> </p>
										</td>
									</tr>
									<tr>
										<td class="text-left"> 
											<p class="font-bold black-color">Item Shelf Life:</p>
										</td>
										<td class="text-left">
											<p id="view_product_shelf_life" class="black-color">0 Days</p>
										</td>
										<td class="">
											<p class="font-bold black-color">Minimum</p>
										</td>
										<td class="text-left">
											<p id="view_product_minimum" class="black-color"> </p>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Unit Price:</p>
										</td>
										<td class="text-left">
											<p id="view_product_price" class="black-color"> </p>
										</td>
										<td>
											<p class="font-bold black-color">Maximum</p>
										</td>
										<td class="text-left">
											<p id="view_product_maximum"  class="black-color"> </p>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Percent Guarantee</p>
										</td>
										<td colspan="3" class="text-left">
											<p id="view_product_guarantee" class="font-14 black-color"> </p>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Description</p>
										</td>
										<td colspan="3" class="text-left">
											<p id="view_product_description" class="font-14 black-color"> </p>
										</td>
									</tr>						
							</table>
						</div>
						<div class="clear"></div> 				
					
					</div>
					
					<div class="border-top border-blue bggray-white box-shadow-light margin-top-20 padding-all-20">
						<div class="f-left">
							<p class="font-20 font-bold black-color">Item Status</p>
						</div>

						<div class="f-right margin-right-80 button-activated">
							<div class="toggleSwitch ">
                                <input type="checkbox" id="switch1" name="switch1" class="switch">
                                <label for="switch1">f</label>
                            </div>
						</div>
						<div class="clear"></div>
					</div>

					<div class="border-top border-blue bggray-white box-shadow-light margin-top-20 padding-all-20">
						<div class="f-left">
							<p class="font-20 font-bold black-color">Bill of Materials</p>
						</div>

						<div class="f-right">
							<p class="font-20 font-bold black-color"><span id="num_material">0</span> Material</p>
						</div>
						<div class="clear"></div>

						<div class="materials-container width-100per  margin-top-10 text-left">

							

						</div>
					</div>
				</div>
			</div>					
		</section>
