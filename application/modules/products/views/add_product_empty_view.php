	
			<div class="main min-width-1024">
				<div class="breadcrumbs no-margin-left padding-left-50">
					<ul>
						<li><a href="product-manage.php" class="font-15 font-400 black-color">Item Management</a></li>
						<li><span>></span></li>
						<li><a href="view-product-empty.php" class="font-15 font-400 black-color">Australian Wheat</a></li>
						<li><span>></span></li>
						<li class="font-bold black-color">Edit Item Details</li>

					</ul>
				</div>
				<div class="semi-main">
					<div class="f-left margin-top-15">	
						<i class="fa fa-trash-o default-cursor font-22 gray-color"></i>
					</div>
					<div class="f-right width-220">
						<a href="<?php echo BASEURL; ?>products/product_management">
							<button type="button" class="btn-default ">Cancel</button>
						</a>
						<button type="button" id="submit_edit_product" class="general-btn ">Edit Item</button>						
					</div>
					<div class="clear"></div>
					<div class="border-top border-blue bggray-white box-shadow-light margin-top-20 padding-all-20">

						<div class="f-left margin-left-30 margin-right-10">
							<div class="upload-square default-cursor">
								<i class="fa fa-camera"></i>
								<p class="title-cam">Upload Photo</p>
							</div>
						</div>

 						
						<div class="f-left margin-left-30 margin-right-10">
							
							<table>
								<tbody>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Item SKU:</p>
										</td>
										<td class="text-left">
											<input type="text" id="itemSku" class="t-small  end-width-200px margin-left-10" />
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Item Name:</p>
										</td>
										<td class="text-left">
											<input type="text" id="itemName" class="t-small end-width-200px margin-left-10"  />
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Item Category</p>
										</td>
										<td class="text-left">
											<div class="select with-modal add-category margin-left-10 width-200">
												<select class="" id="itemCategory">
													<option value="0">Select Category</option>
													<option value="1">Grain</option>
													<option value="2">Computers</option>
													<option value="3">Cars</option>
													<option value="add-cat">Add New Category</option>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td colspan="2" class="text-left">
											<p class="font-bold black-color margin-top-10">Quantity Threshold</p>
										</td>
									</tr>
									<tr>
										<td>
											<p class="margin-top-10">Min:</p>
										</td>
										<td class="text-left text-inside-item">
											<div class=" margin-left-10">
												<input type="text" id="minThreshold" class="t-small text-right" />
												<p>Items</p>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<p class="margin-top-10 padding-left-3">Max:</p>
										</td>
										<td class="text-left text-inside-item">
											<div class="margin-left-10">
												<input type="text" id="maxThreshold" class="t-small width-200 text-right" />
												<p>Items</p>
											</div>
										</td>										
								</tbody>
							</table>											
						</div>

						<div class="f-left margin-left-30">
							<table>
								<tbody>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color margin-top-10">
												Item Shelf Life:
											</p>
										</td>
										<td class="text-inside-item width-200">
											<div class="text-left margin-left-10">
												<input type="text" id="itemShelfLife" class="t-small width-200 text-right" />
												<p>Days</p>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color margin-top-10">
												Unit Price:
											</p>
										</td>
										<td class="text-inside-item">
											<div class="text-left margin-left-10">
												<input type="text" id="itemPrice" class="t-small width-200 text-right" />
												<p>Php</p>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color margin-bottom-75">Description:</p>

										</td>
										<td rowspan="2" class="text-left ">
											<textarea id="itemDescription" class="margin-left-10 margin-top-10"></textarea>
										</td>
									</tr>
								</tbody>
							</table>
						</div>

						<div class="clear"></div>	
					
					</div>
					
					<div class="border-top border-blue bggray-white box-shadow-light margin-top-20 padding-all-20">
						<div class="f-left">
							<p class="font-20 font-bold black-color text-left">Composite Item?</p>
							<p class="text-left margin-top-10 margin-left-5 remove-composite empty-composite">Click and drag your selected items from the Item List to the Bill of Materials on the right.</p>
						</div>

						<div class="f-right margin-right-80 button-activated composite-activated ">
							<div class="toggleSwitch yes-no add-composite-switch ">
                                <input type="checkbox" id="switch1" name="switch1" class="switch">
                                <label for="switch1">f</label>
                            </div>
						</div>
						<div class="clear"></div>

						<div class="margin-top-10 composite-product text-left composite-add-product empty-composite">
							<div class="display-inline-top width-45per margin-top-20 margin-right-40">
								<p class="font-15 black-color font-bold text-left">Item List</p>

								<div class="item-container margin-top-20">
									<div class="search-item">
										<input type="text" class="t-large" placeholder="Select Item Here"/>
									</div>


									<div id="composite_product_list_UI" class="item-list ">

									</div>

								</div>
							</div>
							<div class="display-inline-top width-45per margin-top-20">
								<p class="font-15 black-color font-bold text-left">Bill of Materials</p>

								<div class="item-container margin-top-20 item-drag">	

									<div class="item-list drag-list ">
										<div class="drag-item">
											<p class="font-15 font-400 black-color">Please Drag items from the item list</p>
										</div>									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>					
		</section>

		<div class="modal-container composite-product-modal" modal-id="compo-product">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left ">Composite Item</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content">	
					<p class="font-15 black-color ">Are you sure you want to proceed? </p>
					<p class="font-15 black-color ">Disabling this will erase all the items assigned to the list.</p>						
				</div>
				<div class="modal-footer text-right margin-bottom-10">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid close-me yes-btn">Confirm</button>
				</div>
			</div>	
		</div>

		<div class="modal-container add-cat-mdl" modal-id="add-cat">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left ">Add New Category</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content">		
					<div>
						<p class="font-bold black-color font-15 margin-top-10 display-inline-mid">Category Name:</p>
						<input type="text" class="t-small width-200 display-inline-mid" />		
					</div>
					<div>
						<p class="font-bold black-color font-15 display-inline-top margin-top-10">Category Type:</p>
						<div class="display-inline-top margin-left-10">
							<div class="margin-top-10">
								<input type="checkbox" id="isSolid">
								<label for="isSolid" class="default-cursor">Solid</label>
							</div>
							<div class="margin-top-10">
								<input type="checkbox" id="isLiquid">
								<label for="isLiquid" class="default-cursor">Liquid</label>
							</div>
							<div class="margin-top-10">
								<input type="checkbox" id="isGas">
								<label for="isGas" class="default-cursor">Gas</label>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer text-right margin-bottom-10">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid close-me ">Add Category</button>
				</div>
			</div>	
		</div>
