
		<div class="main min-width-1024">
			<div class="breadcrumbs no-margin-left padding-left-50">				
			</div>
			<div class="semi-main ">


				<!-- modified dropdown -->
				<div class="mod-select ">			
					<div class="select f-left width-350px ">
						<select>
							<option value="Displaying All Active Product"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>						
				</div>
				<div class="clear"></div>
				<!-- buttons -->
				<div class="f-left margin-top-20">
					<p class="display-inline-mid font-15 font-bold"> Browse By:</p>
					<i class="fa fa-list display-inline-mid margin-left-10 font-20 icon-grid default-cursor"></i>
					<i class="fa fa-th-large display-inline-mid margin-left-10 font-20 icon-table default-cursor"></i>
				</div>
				<div class="f-right margin-top-10">	
					<a href="<?php echo BASEURL; ?>products/add_product">
						<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn">Add Item</button>
					</a>							
					<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn dropdown-btn">View Filter / Search</button>					
				</div>
				<div class="clear"></div>

				<div class="dropdown-search">
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr>
								<td>Search</td>
								<td>
									<div class="select">
										<select>
											<option value="items">Items</option>
										</select>
									</div>
								</td>
								<td>
									<input type="text" class="t-small width-100per" />
								</td>
							</tr>
							<tr>
								<td>Filter By:</td>
								<td>
									<div class="select">
										<select>
											<option value="calender">Calendar Here</option>
										</select>
									</div>
								</td>
								<td class="search-action">
									<div class="f-left">
										<div class="select width-150px">
											<select>
												<option value="origin">Origin</option>
											</select>
										</div>
										<div class="select width-150px">
											<select>
												<option value="status">Status</option>
											</select>
										</div>
									</div>
									<div class="f-right margin-left-30">
										<p class="display-inline-mid">Sort By:</p>
										<div class="select width-150px display-inline-mid">
											<select>
												<option value="withdrawal No.">Withdrawal No.</option>
											</select>
										</div>
										<div class="select width-150px display-inline-mid">
											<select>
												<option value="ascending">Ascending</option>
												<option value="descending">Descending</option>
											</select>
										</div>
									</div>
									<div class="clear"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10">
						<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
						<p class="display-inline-mid font-15 font-bold">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 margin-right-30">Search</button>
					<div class="clear"></div>
					<div class="save-result-container">
						

					</div>

				</div>

				<div id="product_management_div_UI" class="grid grid-container">

					 
				</div>

				<div id="listDiplayContainer" class="table tbl-container">

				</div>
						
			</div>
		</div>
	</section>
