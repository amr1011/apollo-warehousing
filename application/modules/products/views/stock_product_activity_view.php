<div class="modal-container" id="generate-data-view" >
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>

<div class="main min-width-1200">
			<div class="breadcrumbs no-margin-left padding-left-50">
				<ul>
					<li><a href="<?php echo BASEURL ?>products/stock_inventory">Item Inventory</a></li>
					<li><span>></span></li>
					<li><strong class="black-color" id="display-product-name">Austrailian Wheat</strong></li>
				</ul>
			</div>
			<div class="center-main">	
				<div class="border-top border-blue bggray-white box-shadow-light margin-top-20 padding-all-20">
					<div class="display-inline-top width-20per">
					<img class="img-responsive " src="" style="height: 127px; width: 191px;" id="display-product-image">
					</div>
 					<div class="display-inline-mid width-80per margin-left-10 text-left">
 					<div class="bg-light-gray padding-all-10">
 						<p class="black-color font-20 font-bold f-left" id="display-product-name-sku">SKU # 1234567810 - Holcim Cement</p>
 						<div class="f-right padding-top-3">
							<p class="black-color font-16 font-bold padding-right-10 f-left">Total Inventory</p>
							<p class="black-color font-16 font-500 f-left"><span class="font-bold blue-color vertical-baseline" id="display-product-total-inventory">100000</span></p>
							<div class="clear"></div>
 						</div>
 						<div class="clear"></div>
 					</div>
 					<div class="bggray-white padding-all-15 padding-left-10">
 						<p class="black-color font-14 font-bold display-inline-mid width-220px">Item Category</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-35per" id="display-product-category">Corn</p>
 						<p class="black-color font-14 font-bold display-inline-mid width-15percent">Unit Price</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-15percent" id="display-product-unit-price">13,000 Php</p>
 					</div>

 					<div class="bg-light-gray padding-all-15 padding-left-10">
 						<p class="black-color font-14 font-bold display-inline-mid width-220px">Quantity Threshold</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-35per"></p>
 						<p class="black-color font-14 font-bold display-inline-mid width-15percent" >Item Shelf Life</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-15percent" id="display-product-shelf-life">1200 Days</p>
 					</div>

 					<div class="bggray-white padding-all-15 padding-left-10">
 						<p class="black-color font-14 font-bold display-inline-mid width-220px padding-left-100">Minimum</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-35per" id="display-product-quantity-minimum">1000 MT</p>
 						<p class="black-color font-14 font-bold display-inline-mid width-15percent">Description</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-15percent italic" id="display-product-description">Ang Cement na matibay</p>
 					</div>

 					<div class="bg-light-gray padding-all-15 padding-left-10">
 						<p class="black-color font-14 font-bold display-inline-mid width-220px padding-left-100	">Maximum</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-35per" id="display-product-quantity-maximum">15000 MT</p>
 					</div>
					</div>
				</div>

				<div class="tab-container font-0 text-left margin-top-30">
					<div class="batch-list display-inline-mid padding-all-20 text-left bggray-white width-30percent margin-right-10 border-top border-blue">
						<input type="radio" name="tab" id="tab-batch" checked="checked" style="display: none;">
						<label for="tab-batch" class="default-cursor font-20 font-bold">Batch List</label>
					</div>

					<div id="consign-assignment-tab" class="consign-assignment display-inline-mid padding-all-20 margin-right-7 text-left bggray-white width-33percent no-margin-left">
						<input type="radio" name="tab" id="tab-consignee" style="display: none;">
						<label for="tab-consignee" class="default-cursor font-20 font-bold">Consignee Assignments</label>
					</div>

					<div class="product-act display-inline-mid padding-all-20 text-left bggray-white width-30percent ">
						<input type="radio" name="tab" id="tab-product" style="display: none;">
						<label for="tab-product" class="default-cursor font-20 font-bold">Item Activity</label>
					</div>
					
					
					<div class="tabs padding-all-10 bggray-white" id="tab-bat">
						<!--first panel-->
						<span id="display-product-storages">
							<div class="panel-group margin-top-20 display-selected-storage selected-storage">
								<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
									<div class="panel-heading panel-heading">
										<a class="collapsed active black-color default-cursor f-left font-20 font-bold trigger-collapse" href="javascript:void(0)">	
											<i class="fa fa-caret-down margin-right-5"></i>
											<span class="display-storage-name">Stock Room 1</span>
										</a>
										<div class="clear"></div>
									</div>
									<div class="panel-collapse collapse in" >
										<div class="panel-body bggray-white font-0">
											<table class="tbl-4c3h margin-top-20 text-left">
												<thead>
													<tr>
														<th class="black-color font-14 font-500 no-padding-left text-center  width-14percent">Batch Name.</th>
														<th class="black-color font-14 font-500 no-padding-left text-center width-14percent ">Batch Date</th>
														<th class="black-color font-14 font-500 no-padding-left text-center width-30percent">Location</th>
														<th class="black-color font-14 font-500 no-padding-left text-center width-14percent">Quantity</th>
														<th class="black-color font-14 font-500 no-padding-left text-center width-14percent">Remaining Shelf Life</th>
														<th class="black-color font-14 font-500 no-padding-left text-center width-14percent">Status</th>												
													</tr>
												</thead>
											</table>
											<span class="display-batches-item">
												<div class="tbl-like text-left display-batches-here batches-displayed">
													<div class="first-tbl mngt-tbl-content height-auto min-height-50px font-0">
														<div class="display-inline-mid width-14percent text-center">
															<p class="font-400 f-none font-14 width-100percent display-batch-name"></p>
														</div>
														<div class="display-inline-mid width-14percent text-center">
															<p class="font-400 f-none font-14 width-100percent display-batch-date"></p>
														</div>
														<div class="display-inline-mid width-30percent text-left display-batch-location">		
														</div>
														<div class="display-inline-mid width-14percent text-center">
															<p class="font-400 f-none font-14 width-100percent display-batch-quantity"></p>
														</div>
														<div class="display-inline-mid width-14percent text-center">
															<p class="font-400 f-none font-14 width-100percent display-batch-shelf-life"></p>
														</div>
														<div class="display-inline-mid width-14percent text-center">
															<p class="font-400 f-none font-14 width-100percent green-color display-batch-status"></p>
														</div>
													</div>
													<div class="hover-tbl text-center" >
														
														<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
														
														<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
														
														<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
														
														<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
														
														<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>
														
														<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
													</div>
												</div>
											</span>
										</div>
									</div>
								</div>
							</div>

						</span>
							<!--end of first panel-->
				</div>

				<!--Product Activity Panel Group-->
				<div class="tabs padding-all-10 bggray-white" id="tab-prod">
					

					<!-- start Receiving-->
					<div class="panel-group" id="display-main-receiving-record">
						<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							<!-- step 1 -->
							<div class="panel-heading panel-heading">
								<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
									<i class="fa fa-caret-down margin-right-5"></i>
									Receiving
								</a>
								<div class="clear"></div>
							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body bggray-white font-0">
								
										<!--start product activity-->
										<div class="padding-bottom-20 bggray-white box-shadow-light">
										<!-- sample 1 -->
											<span class="display-receiving-record">
												<div class="padding-all-10 dashboard-icon display-receiving-items receiving-items">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-receiving-date-created">September 02, 2015 06:00PM</p>
													
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="" alt="user profile-pic" class="width-50px display-inline-mid display-receiving-user-pic">
															<p class="display-inline-mid margin-left-10 display-receiving-name"><span class="display-receiving-user">Jules Ignacio</span> has received a batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Receiving No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">PO No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch Name</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Receiving Date</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Origin</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-receiving-num">1234657890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-receiving-po">123456</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-receiving-batch">
																			<!-- <p>1234567890</p>
																			<p>1234567891</p> -->
																		</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-receiving-date-received">01-Mar-2015</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-receiving-origin">Vessel</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</span>
											
										</div>
										<!--end product activity-->
								</div>
							</div>
						</div>
					</div>
					<!-- end Receiving-->

					<!-- start Withdrawals-->
					<div class="panel-group" id="display-main-withdrawals-record">
						<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							<!-- step 1 -->
							<div class="panel-heading panel-heading">
								<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
									<i class="fa fa-caret-down margin-right-5"></i>
									Withdrawals
								</a>
								<div class="clear"></div>
							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body bggray-white font-0">
								
										<!--start product activity-->
										<div class="padding-bottom-20 bggray-white box-shadow-light">
										<!-- sample 1 -->
											<span class="display-withdrawal-record">
												<div class="padding-all-10 dashboard-icon display-withdrawal-items ">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-withdrawal-date">September 02, 2015 06:00PM</p>
													
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="" alt="user profile-pic" class="width-50px display-inline-mid display-withdrawal-user-pic">
															<p class="display-inline-mid margin-left-10"><span class="display-withdrawal-user">Jules Ignacio</span> has withdrawn a batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Withdrawal No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">ATL No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">CDR No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch Name</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Origin</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Consignee</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Mode of Travel</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-withdrawal-num">1234657890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-withdrawal-atl">123456</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-withdrawal-cdr">123456</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-withdrawal-batch-name">1234567890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-withdrawal-origin">Vessel</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-withdrawal-consignee">Alpha Foods Inc.</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-withdrawal-mode-travel">Vessel</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</span>
										
										</div>
										<!--end product activity-->
								</div>
							</div>
						</div>
					</div>

					<!-- Ending Withdrawals-->

					<!-- start Transfer-->
					<div class="panel-group" id="display-main-transfer-record">
						<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							<!-- step 1 -->
							<div class="panel-heading panel-heading">
								<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
									<i class="fa fa-caret-down margin-right-5"></i>
									Transfer
								</a>
								<div class="clear"></div>
							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body bggray-white font-0">
								
										<!--start product activity-->
										<div class="padding-bottom-20 bggray-white box-shadow-light">
											<!-- sample 1 -->
											<span class="display-transfer-record">
												<div class="padding-all-10 dashboard-icon display-transfer-item transfer-displayed">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-transfer-date-issued">September 02, 2015 06:00PM</p>
													
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="" alt="user profile-pic" class="width-50px display-inline-mid display-transfer-user-pic">
															<p class="display-inline-mid margin-left-10"><span class="display-transfer-user">Jules Ignacio</span> has transferred a batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Transfer No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch Name</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Origin</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Destination</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Origin</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-transfer-num" style="width: 15%;">1234657890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-transfer-batch-name" style="width: 15%;">1234657890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left max-width-300px text-left line-height-25 display-transfer-batch-origin" style="width: 30%;">
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
																		</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-transfer-batch-destination" style="width: 30%;">1234567890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-transfer-origin">Vessel</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</span>
											
										</div>
										<!--end product activity-->
								</div>
							</div>
						</div>
					</div>

					<!-- ending Transfer-->


					<!-- start Adjustment-->
					<div class="panel-group" id="display-main-adjustment-div">
						<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
		
							<div class="panel-heading panel-heading">
								<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
									<i class="fa fa-caret-down margin-right-5"></i>
									Adjustment
								</a>
								<div class="clear"></div>
							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body bggray-white font-0">
								
										
										<div class="padding-bottom-20 bggray-white box-shadow-light">
											<span id="display-main-adjustment-record">
												<div class="padding-all-10 dashboard-icon display-main-adjustment-item ">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-adjust-batch-date-created">September 02, 2015 06:00PM</p>
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid display-adjust-batch-user-pic">
															<p class="display-inline-mid margin-left-10 "><span class='display-adjust-batch-user'>Jules Ignacio</span> has adjusted a batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch Name</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Previous Qty</th>
																		<th class=" font-14 font-bold black-color no-padding-left">New QTY</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Reason</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-adjust-batch-name">12345677</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-adjust-batch-prev-qty">100 Bags</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-adjust-batch-new-qty">300 Bag</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-adjust-batch-reason">Nam dapibus nisl vitae elit fringilla rutrum. Aenean Solicitudin, erat a elementum.</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</span>

										</div>
									
								</div>
							</div>
						</div>
					</div> 
					<!-- ending Adjustment-->

					<!-- start Batch Repack-->
					<div class="panel-group" id="display-main-repack-div">
						<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							
							<div class="panel-heading panel-heading">
								<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
									<i class="fa fa-caret-down margin-right-5"></i>
									Batch Repack
								</a>
								<div class="clear"></div>
							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body bggray-white font-0">
								
										
										<div class="padding-bottom-20 bggray-white box-shadow-light">
											<span id="display-main-repack-record">

												<div class="padding-all-10 dashboard-icon display-main-repack-item">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-repack-batch-date-created">September 02, 2015 06:00PM</p>
													
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid display-repack-batch-user-pic">
															<p class="display-inline-mid margin-left-10"><span class="display-repack-batch-user">Jules Ignacio</span> has repacked Batch Name <span class="display-repack-batch-name">13245678</span></p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Previous Quantity</th>
																		<th class=" font-14 font-bold black-color no-padding-left">New Quantity</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-repack-batch-old-qty">300 Bags</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left display-repack-batch-new-qty">30 Crates</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</span>
										</div>
										
								</div>
							</div>
						</div>
					</div> 
					<!-- End Batch Repack-->

					<!-- Start Batch Return-->
					<div class="panel-group" id="display-main-return-div">
						<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							<div class="panel-heading panel-heading">
								<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
									<i class="fa fa-caret-down margin-right-5"></i>
									Batch Returns
								</a>
								<div class="clear"></div>
							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body bggray-white font-0">
										<div class="padding-bottom-20 bggray-white box-shadow-light">
											<span id="display-main-return-record">
												<div class="padding-all-10 dashboard-icon display-main-return-item">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-return-batch-date-created">September 02, 2015 06:00PM</p>
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid display-return-batch-user-pic">
															<p class="display-inline-mid margin-left-10"><span class="display-return-batch-user">Jules Ignacio</span> has returned a part of Batch Name <span class="display-return-batch-name">13245678</span></p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Quantity to Return</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Reason</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-return-batch-quantity">300 Bags</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-return-batch-reason">Vestibulum rutrum quam vitae fringilla tincidunt. Suspendisse nec tortor urna. Ut laoreet sodales nisi, quis iaculis nulla iaculis vitae. Dinec sagittis faucibus lacus eget blandit.</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</span>
										</div>
								</div>
							</div>
						</div>
					</div> 
					<!-- End Batch Return-->

					<!-- Start Split Batch-->
					<div class="panel-group" id="display-main-split-div">
						<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							<div class="panel-heading panel-heading">
								<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
									<i class="fa fa-caret-down margin-right-5"></i>
									Batch Splits
								</a>
								<div class="clear"></div>
							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body bggray-white font-0">
										<div class="padding-bottom-20 bggray-white box-shadow-light">
											<span id="display-main-split-record">
												<div class="padding-all-10 dashboard-icon display-main-split-item">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-split-batch-date-created">September 02, 2015 06:00PM</p>
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid display-split-batch-user-pic">
															<p class="display-inline-mid margin-left-10"><span class="display-split-batch-user">Jules Ignacio</span> has split a part of Batch Name <span class="display-split-batch-name">13245678</span></p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch Name</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Quantity</th>
																	</tr>
																</thead>
																<tbody class="display-split-batch-new-batches">
																	<tr class="display-split-batch-new-batches-item">
																		<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-split-batch-name">300 Bags</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-split-batch-quality">Vestibulum rutrum quam vitae fringilla tincidunt.</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</span>
										</div>
								</div>
							</div>
						</div>
					</div> 
					<!-- End Split Batch-->

					<!-- Start Batch Remove-->
					<div class="panel-group" id="display-main-remove-div">
						<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							<div class="panel-heading panel-heading">
								<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
									<i class="fa fa-caret-down margin-right-5"></i>
									Batch Remove
								</a>
								<div class="clear"></div>
							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body bggray-white font-0">
										<div class="padding-bottom-20 bggray-white box-shadow-light">
											<span id="display-main-remove-record">
												<div class="padding-all-10 dashboard-icon display-main-remove-item">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-remove-batch-date-created">September 02, 2015 06:00PM</p>
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid display-remove-batch-user-pic">
															<p class="display-inline-mid margin-left-10"><span class="display-remove-batch-user">Jules Ignacio</span> has removed a Batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch Name</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Reason</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-remove-batch-name">300 Bags</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-remove-batch-reason">Vestibulum rutrum quam vitae fringilla tincidunt. Suspendisse nec tortor urna. Ut laoreet sodales nisi, quis iaculis nulla iaculis vitae. Dinec sagittis faucibus lacus eget blandit.</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</span>
										</div>
								</div>
							</div>
						</div>
					</div> 
					<!-- End Batch Remove-->

					<!-- Start Batch Update Status-->
					<div class="panel-group" id="display-main-status-div">
						<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							<div class="panel-heading panel-heading">
								<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
									<i class="fa fa-caret-down margin-right-5"></i>
									Batch Update Status
								</a>
								<div class="clear"></div>
							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body bggray-white font-0">
										<div class="padding-bottom-20 bggray-white box-shadow-light">
											<span id="display-main-status-record">
												<div class="padding-all-10 dashboard-icon display-main-status-item">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10 display-status-batch-date-created">September 02, 2015 06:00PM</p>
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid display-status-batch-user-pic">
															<p class="display-inline-mid margin-left-10"><span class="display-status-batch-user">Jules Ignacio</span> has removed a Batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch Name</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Status</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Reason</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-20percent display-status-batch-name">300 Bags</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-30percent display-status-batch-status">asdasd</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-50percent display-status-batch-reason">asdasd</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</span>
										</div>
								</div>
							</div>
						</div>
					</div> 
					<!-- End Batch Update Status-->

				</div>

				<div class=" box-shadow-dark" id="tab-con">
					<div class="tabs padding-all-10 bggray-white " >
						<table class="tbl-4c3h">
							<thead>
								<tr>
									<th class=" font-14 font-bold black-color no-padding-left width-33percent">Consignee Name</th>
									<th class=" font-14 font-bold black-color no-padding-left ">Vessel</th>
									<th class=" font-14 font-bold black-color no-padding-left ">Quantity Assigned</th>
								</tr>
							</thead>
							<tbody class="text-center" id="display-consignee-records">
							
								<tr class="display-consignee-items">
									<td class=" font-14 font-400 black-color no-padding-left display-consignee-name">Alphass Foods Inc.</td>
									<td class=" font-14 font-400 black-color no-padding-left display-consignee-vessel">MV ssChino</td>
									<td class=" font-14 font-400 black-color no-padding-left display-consignee-quantity">25000 MT</td>
								</tr>

							</tbody>
						</table>
					</div>	
				</div>

			</div>
		</div>
		</section>
		<!--modal start update status -->
		<div class="modal-container" id="update-status-modal" modal-id="update-status">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Update Status</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-bottom-20">
						<p class="font-14 font-400 display-inline-mid margin-top-5 no-margin-all padding-right-20">New Status:</p>
						<div class="select  display-inline-mid width-250px">
							<select class="medium select-batch-status">
								<option value="1">Quality Assurance</option>
								<option value="2">Restricted</option>
							</select>
						</div>
					</div>
					<div>
						<p class="font-14 font-400 display-inline-mid margin-top-5 no-margin-all padding-right-20 padding-bottom-20">Please provide a reason for status change:</p>
						<textarea id="batch-update-reason" class="height-150px"></textarea>

					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="update-status-modal-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20  modal-trigger" modal-target="update-status-confirmation">Confirm</button>
				</div>
			</div>	
		</div>
		<!--modal end update status -->

		<!--modal start update status confirmation -->
		<div class="modal-container" modal-id="update-status-confirmation">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Update Status</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Batch Name <span id="display-batch-name-new-status">12345678</span> status changed to <span id="display-batch-new-status">Quality Assurance</span></p>
					</div>
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end update status confirmation -->

		<!--modal start of adjust quantity -->
		<div class="modal-container" id="adjust-quantity-modal" modal-id="adjust-quantity">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Adjust Quantity</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-bottom-20">
						<p class="font-14 font-400 display-inline-mid width-25percent no-margin-all">Batch Name:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-all" id="display-adjust-batch-name">12345678</p>
					</div>

					<div class="padding-bottom-10">
						<p class="font-14 font-400 display-inline-mid width-25percent no-margin-all">Current Quantity:</p>
						<p class="font-14 font-400 display-inline-mid width-25percent no-margin-all" id="display-adjust-batch-quantity">300 KG</p>
						<p class="font-14 font-400 display-inline-mid italic no-margin-all padding-left-35"><span id="display-adjust-batch-state" class=""></span></p>
					</div>
					<div class="font-0">
						<p class="font-14 font-400 display-inline-top width-25percent no-margin-all padding-top-15">Adjustment:</p>
						<div class="display-inline-mid width-75percent">
							<div class="width-100percent padding-top-6">
								<div class="display-inline-mid width-40percent">
									<input type="radio" class="width-20px display-inline-mid no-margin-all transform-scale default-cursor" id="increaseBy" name="increaseAndDecrease" checked>
									<label for="increaseBy" class="font-14 font-400 display-inline-mid default-cursor">Increase By</label>
								</div>
								<div class="display-inline-mid">
									<input type="text" class="width-100px display-inline-mid" id="text-increase">
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-left-20 ">KG</p>
								</div>
							</div>
							<div class="width-100percent padding-top-10">
								<div class="display-inline-mid width-40percent">
									<input type="radio" class="width-20px display-inline-mid no-margin-all transform-scale default-cursor" id="decreaseBy" name="increaseAndDecrease">
									<label for="decreaseBy" class="font-14 font-400 display-inline-mid default-cursor">Decrease By</label>
								</div>
								<div class="display-inline-mid">
									<input type="text" class="width-100px display-inline-mid" id="text-decrease">
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-left-20">KG</p>
								</div>
							</div>
						</div>
					</div>
					<div class="padding-top-20">
						<p class="font-14 font-400 no-margin-all padding-bottom-10">Please provide a reason for batch adjustment:</p>
						<textarea class="height-100px" id="display-adjust-batch-reason"></textarea>
					</div>
					
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="adjust-quantity-modal-save"  class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Adjust Quantity</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of adjust quantity-->

		<!--modal start of adjust quantity password -->
		<div class="modal-container" modal-id="adjust-quantity-password">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Adjust Quantity</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<p class="font-14 font-400 no-margin-hor">Please input your password to proceed:</p>
					<p class="font-14 font-400 display-inline-mid width-20percent no-margin-all">Password:</p>
					<input type="password" class="t-medium" id="set-search-user-password">
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="adjust-quantity-search-password" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 " >Enter</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of adjust quantity password-->

		<!--modal start adjust quantity confirmation -->
		<div class="modal-container" modal-id="adjust-quantity-confirmation">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Adjust Quantity</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Batch Name <span id="display-adjustment-batch-name">12345678</span> quantity has been adjusted.</p>
						<p class="font-14 font-400 white-color">New Quantity: <span id="display-new-adjusted-qty">300 Bags</span></p>
						<p class="font-14 font-400 white-color">Previous Quantity: <span id="display-old-adjusted-qty">300 Bags</span></p>

					</div>
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end adjust quantity confirmation -->

		<!--modal start remove batch -->
		<div class="modal-container" id='remove-batch-modal' modal-id="remove-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Remove Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div>
						<p class="font-14 font-400 display-inline-mid margin-top-5 no-margin-all padding-right-20 padding-bottom-20">Please provide a reason for batch removal:</p>
						<textarea class="height-150px" id="remove-batch-textarea"></textarea>

					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="remove-batch-modal-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 modal-trigger" modal-target="remove-batch-confirmation">Remove Batch</button>
				</div>
			</div>	
		</div>
		<!--modal end remove batch -->

		<!--modal start remove batch confirmation -->
		<div class="modal-container" modal-id="remove-batch-confirmation">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Remove Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Batch Name <span id="remove-batch-confirmation-caption">12345678</span> has been removed.</p>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end remove batch confirmation -->

		<!--modal start of repack batch -->
		<div class="modal-container" id="repack-batch-modal" modal-id="repack-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Repack Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-hor">Batch Name:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor" id="display-repack-batch-name">12345678</p>
					</div>
					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-hor">Current Quantity:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor" id="display-repack-batch-quantity">300 Bags</p>
					</div>

					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-all">Repacking Quantity:</p>
						<input type="text" class="width-100px display-inline-mid" id="set-repack-batch-quantity">
						<div class="select medium select-dropdown">
						<select>
							<option value="Bags">Bags</option>
						</select>
						</div>
					</div>

				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="repack-batch-modal-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" >Repack Batch</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of repack batch-->

		<!--modal start repack batch confirmation -->
		<div class="modal-container" modal-id="repack-batch-confirmation">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Repack Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>
				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Batch Name <span id="display-success-repack-batch-name">12345678</span> quantity has been repacked.</p>
						<p class="font-14 font-400 white-color ">New Quantity: <span id="display-success-repack-batch-new-qty">30</span> Bags</p>
						<p class="font-14 font-400 white-color">Previous Quantity: <span id="display-success-repack-batch-old-qty">30</span> Bags</p>

					</div>
				</div>
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end repack batch confirmation -->

		<!--modal start of return batch -->
		<div class="modal-container" id="return-batch-modal" modal-id="return-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Return Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-hor">Batch Name:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor" id="display-return-batch-name">12345678</p>
					</div>
					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-hor">Current Quantity:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor" id="display-return-batch-quantity">300 Bags</p>
					</div>

					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-all">Returning Quantity:</p>
						<input type="text" class="width-100px display-inline-mid" id="display-return-batch-quantity-entry">
						<p class="font-14 font-400 display-inline-mid no-margin-ver">KG</p>
					</div>
					<div class="padding-top-20">
						<p class="font-14 font-400 no-margin-hor">Please provide a reason for batch returns:</p>
						<textarea class="height-150px" id="display-return-batch-reason"></textarea>
					</div>

				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="return-batch-modal-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 ">Return Batch</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of return batch-->

		<!--modal start return batch confirmation -->
		<div class="modal-container" modal-id="return-batch-confirmation">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Return Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">A part of Batch Name <span id="display-success-return-batch-name">12345678</span> has been returned.</p>
						<p class="font-14 font-400 white-color">Quantity Returned: <span id="display-success-return-batch-returned-quantity">50 Bags</span></p>
					</div>
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end return batch confirmation -->


		<!--modal start of split batch -->
		<div class="modal-container" id="split-batch-modal" modal-id="split-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Split Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div>
						<p class="font-14 font-bold black-color display-inline-mid no-margin-hor width-30percent">Batch Name:</p>
						<p class="font-14 font-bold black-color display-inline-mid no-margin-hor" id="display-split-batch-name">12345678</p>
					</div>
					<div>
						<p class="font-14 font-400 black-color display-inline-mid no-margin-hor width-30percent">Current Quantity:</p>
						<p class="font-14 font-400 black-color display-inline-mid no-margin-hor" id="display-split-batch-quantity">300 Bags</p>
					</div>

					<div class="padding-bottom-10">
						<p class="font-14 font-400 black-color display-inline-mid no-margin-all f-left">Batch Splits</p>
						<a href="javascript:void(0)" class="font-14 font-400 blue-color f-right" id="split-batch-add">
							+Add Batch Split
						</a>
						<div class="clear"></div>
					</div>
					<span id="split-add-batch-record">
						<div class="padding-all-5 bggray-white margin-bottom-10 split-add-batch-item">
							<p class="font-14 font-400 black-color no-margin-ver display-inline-mid insert-number">1. </p>
							<input type="text" class="width-400px display-inline-mid set-split-batch-name-input" placeholder="Batch Name">
							<div class="padding-top-10 padding-bottom-10 padding-left-25">
								<p class="font-14 font-400 black-color no-margin-ver display-inline-mid">Quantity: </p>
								<input type="text" class="t-small display-inline-mid margin-right-7 set-split-qunatity-input">
								<div class="select medium select-dropdown">
									<select class="unit-of-measure">
										<!-- <option value="Bags">Bags</option> -->
									</select>
								</div>
							</div>
						</div>
					</span>

				</div>
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="split-batch-modal-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 " >Split Batch</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of split batch-->

		<!--modal start split batch confirmation -->
		<div class="modal-container" modal-id="split-batch-confirmation">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Split Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<p class="font-14 font-400 no-margin-hor black-color">Batch Name <span id="display-split-success-batch-name">12345678</span> has been split into:</p>
					<table class="tbl-4c3h">
						<thead>
							<tr>
								<th class="no-padding-left black-color">Batch Name</th>
								<th class="no-padding-left black-color">Quantity</th>
							<tr>
						</thead>
						<tbody class="text-center" id="display-split-success-record">
							<tr class="display-split-success-item">
								<td class="font-14 font-400 no-margin-hor black-color display-split-success-batch-name-new">12345678A</td>
								<td class="font-14 font-400 no-margin-hor black-color display-split-success-batch-quantity">200 Bags</td>
							</tr>
							<!-- <tr>
								<td class="font-14 font-400 no-margin-hor black-color">12345678A</td>
								<td class="font-14 font-400 no-margin-hor black-color">100 Bags</td>
							</tr> -->
							
						</tbody>
					</table>
				</div>

				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="">OK</button>
				</div>
			</div>	
		</div>
		<!--- modal ending split batch confirmation-->