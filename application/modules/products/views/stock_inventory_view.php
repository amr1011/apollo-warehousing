<div class="modal-container" id="generate-data-view" >
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>

<div class="main min-width-1200">
			<div class="center-main">
				<div class="mod-select">			
					<div class="select f-left width-280px margin-top-20 margin-left-5">
						<select class="transform-dd">
							<option value="Displaying All Products"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class=" margin-top-20 f-left">
					<p class="display-inline-mid margin-right-10 margin-left-5 font-15 font-bold">Display Inventory By:</p>
					<div class="wh-select display-inline-mid">					
						<div class="select stocks medium item-">
							<select class="transform-dd select-prod-or-location">
								<option value="prod">Item</option>
								<option value="sto">Storage Location</option>							
							</select>
						</div>
					</div>
				</div>
				<div class="clear"></div>


				<div class="width-100percent">
					<div class="f-left margin-top-15 margin-left-5">
						<p class="display-inline-mid font-15 font-bold"> Browse By:</p>
						<i class="fa fa-list display-inline-mid margin-left-10 font-20 icon-grid default-cursor listing"></i>
						<i class="fa fa-th-large display-inline-mid margin-left-10 font-20 icon-table default-cursor pictures"></i>
					</div>
					<button class="f-right general-btn margin-right-5 dropdown-btn"> View Filter / Search </button>
					<div class="clear"></div>
				</div>
				
				<!-- search button  -->
				<div class="dropdown-search" >
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr>
								<td class="font-15 font-400 black-color width-15percent">Search:</td>
								<td><div class="select medium">
										<select class="transform-dd">
											<option value="search1">Item Name</option>
											<option value="search2">SKU</option>
										</select>
									</div>
								</td><td class="width-75percent">
									<input type="text" class="t-small width-100per">
								</td>
							</tr>
							<tr>
								<td class="font-15 font-400 black-color">Sort By:</td>
								<td>
									<div class="select medium">
										<select class="transform-dd">
											<option value="product1">Item Name</option>
											<option value="product2">Item SKU</option>
											<option value="product3">Batches</option>
											<option value="product4">Total Quantity</option>

										</select>
									</div>
								</td>								
								<td class="search-action">
									<div class="f-left">
										<div class="select medium display-inline-mid">
											<select class="transform-dd">
												<option value="ascending">Ascending</option>
												<option value="descending">Descending</option>
											</select>
										</div>																		
									</div>
									
									
								</td>
							</tr>
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10 display-search-result">
						<i class="fa fa-2x display-inline-mid fa-caret-right"></i>
						<p class="display-inline-mid font-15 font-bold margin-left-10">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 margin-right-30">Search</button>
					<div class="clear"></div>
					<div class="search-result padding-all-10">
						<div class="margin-left-50">
							<p class="font-400 font-15 black-color f-left">Who can also view this Search Result? </p>
							<div class="clear"></div>
							
							<div class="f-left">
								<div class="display-inline-mid margin-right-10">
									<input type="radio" id="me" name="search-result" class="width-20px transform-scale">
									<label for="me" class="margin-bottom-5 default-cursor font-15 black-color font-400">Only Me</label>
								</div>
								<div class="display-inline-mid margin-right-50">
									<input type="radio" id="selected" name="search-result" class="width-20px transform-scale">
									<label for="selected" class="margin-bottom-5 default-cursor font-15 black-color font-400">With Selected Members</label>
								</div>
								<div class="display-inline-mid member-selection">
									<div class="member-container">
										<p>1 Member Selected</p>
										<i class=" fa fa-caret-down fa-2x"></i>
									</div>
									<div class="popup_person_list" >
										<div class="popup_person_list_div">
											
											<div class="thumb_list_view small marked">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia Ricardo Gomez Kuala Lumpur Eklabu</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

										</div>										
									</div>
								</div>
								<div class="display-inline-mid margin-left-50">								
									<!-- dropdown here with effects  -->
									<button type="button" class="general-btn">Save Search Results</button>
								</div>
							</div>
							<div class="clear"></div>
						</div>

					</div>
				</div>
				<div class="first-content margin-top-20 text-left font-0">

					<span id="grid-product-views">
						<div class="grid-display-item selected-grid-item record-item display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center" style="display: inline-block;">
							<a href="javascript:void(0)" class="black-color height-200px view-product-details">
								<div class="width-100percent fix-image">
									<img src="" class="width-100per  display-grid-image"/>
								</div>
								<p class="margin-top-20 font-20"><strong class="display-grid-name">Australian Wheat</strong></p>
								<hr/>
								<p class="margin-top-10"><strong>SKU#</strong> <span  class="display-grid-sku">122213321321</span></p>
								<p class="margin-top-10" ><span class="display-grid-batches">66</span> Batches (<span class="display-grid-total-quantity">7952</span> KG)</p>
							</a>
						</div>
					</span>
					



				
				</div>
				<div class="second-content padding-all-20 border-top border-blue box-shadow-dark margin-top-20">
					<table class="tbl-4c3h margin-top-20">
						<thead>
							<tr>
								<th class="black-color width-25percent no-padding-left">Item Name</th>
								<th class="black-color width-25percent no-padding-left">SKU Number</th>
								<th class="black-color width-25percent no-padding-left">No. of Batches</th>
								<th class="black-color width-25percent no-padding-left">Total Quantity</th>
																			
							</tr>
						</thead>
					</table>
					<span id="list-product-views">
						<div class="tbl-like font-0 list-display-item selected-list-item record-item">
							<div class="first-tbl no-padding-all mngt-tbl-content text-left">
								<div class="display-inline-mid width-25percent text-center">
									<p class="font-400 f-none width-100percent font-14 display-list-name">Australian Wheat</p>
								</div>
								<div class="display-inline-mid width-25percent text-center">
									<p class="font-400 f-none width-100percent font-14 display-list-sku">983122340</p>
								</div>
								<div class="display-inline-mid width-25percent text-center">
									<p class="font-400 f-none width-100percent font-14 display-list-batches">66</p>
								</div> 
								<div class="display-inline-mid width-25percent text-center">
									<p class="font-400 f-none width-100percent font-14 display-list-total-quantity">7952 KG</p>
								</div>
								
								
							</div>
							<div class="hover-tbl">
								<a href="javascript:void(0)" class="view-product-details">
									<button class="btn general-btn">View Item Details</button>
								</a>
							</div>
						</div>
					</span>
					
					<!-- <div class="tbl-like font-0">
						<div class="first-tbl no-padding-all mngt-tbl-content text-left">
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14 ">Australian Wheat</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">983122340</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">66</p>
							</div>
							<div class="display-inline-mid width-25percent text-center">
								<p class="font-400 f-none width-100percent font-14">7952 KG</p>
							</div>										
						</div>
						<div class="hover-tbl">									
							<a href="<?php echo BASEURL ?>products/stock_product_activity">
								<button class="btn general-btn">View Product Details</button>
							</a>											
						</div>
					</div> -->

																
				</div>

			</div>
		</div>
				
	</section>