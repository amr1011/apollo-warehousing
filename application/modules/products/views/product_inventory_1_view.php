
		<div class="main min-width-1200">
			<div class="center-main">	
				<div class="mod-select">			
					<div class="select f-left width-280px margin-top-20">
						<select>
							<option value="Displaying All Products"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="margin-top-20 f-left">
					<p class="display-inline-mid margin-right-10">Search By:</p>
					<div class="wh-select display-inline-mid">					
						<div class="select">
							<select>
								<option value="prod">Item</option>
								<option value="sto">Storage</option>							
							</select>
						</div>
					</div>
				</div>
				<button class="f-right margin-top-20 search-button general-btn"><i class="fa fa-search"></i> Search <i class="fa fa-caret-down"></i></button>
				<div class="clear"></div>

				<!-- search button  -->
				<div class="bggray-middark margin-top-10 margin-bottom-10 margin-right-20 search-button-display">				
					<table>
						<tbody>
							<tr>
								<td class="width-200 padding-right-10 text-right">Search:</td>
								<td>
									<div class="select">
										<select>
											<option value="name" selected>Name</option>
										</select>
									</div>
								</td>
								<td colspan="3" class="padding-left-10 padding-right-10">
									<input type="text" class="t-large width-100per">
								</td>
							</tr>
							<tr>
								<td class="width-200 padding-right-10 text-right">Filter By:</td>
								<td class="padding-right-10 padding-left-10">
									<div class="select">
										<select>
											<option value="all locations" selected>Date Created</option>
										</select>
									</div>
								</td>
								<td class="padding-right-10 padding-left-10">
									<div class="select">
										<select>
											<option value="all locations" selected>All Categories</option>
										</select>
									</div>
								</td>
								<td class=" padding-right-10 text-right">Sort By:</td>
								<td class="padding-right-10 padding-left-10">
									<div class="select">
										<select>
											<option value="all locations" selected>SKU No.</option>
										</select>
									</div>
								</td>
								<td class="padding-right-10 padding-left-10">
									<div class="select">
										<select>
											<option value="all locations" selected>Ascending</option>
										</select>
									</div>
								</td>
							</tr>
							
						</tbody>
					</table>
					<div class="panel-group mod-accordion">
						<div class="accordion_custom border-top-none">
							<div class="panel-heading width-1000px">
								<a class="collapsed" href="#">
									<h4 class="panel-title active f-left">
										<i class="fa fa-caret-right f-left margin-right-10 caret-click"></i>
										<p class="f-left save-click">Save Search Results</p>
										<div class="clear"></div>
									</h4>								
									<button class="general-btn f-right">Search</button>								
									<div class="clear"></div>
								</a>
							</div>
							<div class="panel-collapse collapse margin-left-10">
								<div class="panel-body no-padding-all">
									<div class="margin-left-30">
										<p class="font-bold font-15 black-color f-left">Who can also view this Search Result? </p>
										<div class="clear"></div>
										
										<div class="f-left">
											<div class="display-inline-mid margin-right-10">
												<input type="radio" id="me" name="search-result" class="width-20px">
												<label for="me" class="margin-bottom-5 default-cursor font-15 black-color font-bold">Only Me</label>
											</div>
											<div class="display-inline-mid margin-right-50">
												<input type="radio" id="selected" name="search-result" class="width-20px">
												<label for="selected" class="margin-bottom-5 default-cursor font-15 black-color font-bold">With Selected Members</label>
											</div>
											<div class="display-inline-mid">
												<div class="select width-300px margin-right-30">
													<select>
														<option value="op1">User Selection UI Here</option>
													</select>
												</div>
												<button type="button" class="general-btn">Save Search Results</button>
											</div>
										</div>
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>

				<div class="margin-top-20 text-left font-0">
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/view_product" class="black-color height-200px ">
							<div class="width-100percent fix-image">
								<img src="../assets/images/australian-wheat.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Australian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">200 Items (45 Batches)</p>
						</a>
					</div>

					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/view_product" class="black-color height-200px">
							<div class="width-100percent fix-image">
								<img src="../assets/images/ukrainian wheat.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Ukrainian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">200 Items (45 Batches)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/view_product" class="black-color height-200px">
							<div class="width-100percent  fix-image">
								<img src="../assets/images/brazilian-wheat.jpg" class="width-100percent  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Brazilian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">200 Items (45 Batches)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/view_product" class="black-color height-200px">
							<div class="width-100percent fix-image ">
								<img src="../assets/images/romanian-wheat.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Romanian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">200 Items (45 Batches)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/view_product" class="black-color height-200px ">
							<div class="width-100percent fix-image ">
								<img src="../assets/images/indian-wheat.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Indian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">200 Items (45 Batches)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/view_product" class="black-color height-200px">
							<div class="width-100percent fix-image  ">
								<img src="../assets/images/black-sea-wheat.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Black Sea Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">200 Items (45 Batches)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/view_product" class="black-color height-200px">
							<div class="width-100percent fix-image">
								<img src="../assets/images/bulgarian-wheat.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Bulgarian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">200 Items (45 Batches)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/view_product" class="black-color height-200px">
							<div class="width-100percent fix-image ">
								<img src="../assets/images/us-soya.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>United State Soya</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">200 Items (45 Batches)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/view_product" class="black-color height-200px">
							<div class="width-100percent fix-image ">
								<img src="../assets/images/argentina-soya.jpg" class="width-100per  "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Argentina Soya</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">200 Items (45 Batches)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/view_product" class="black-color height-200px">
							<div class="width-100percent fix-image">
								<img src="../assets/images/argentina-corn.jpg" class="width-100per "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Argentina Corn</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">200 Items (45 Batches)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/view_product" class="black-color height-200px">
							<div class="width-100percent  fix-image">
								<img src="../assets/images/indonesian-corn.jpg" class="width-100per "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Indonesian Wheat</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">200 Items (45 Batches)</p>
						</a>
					</div>
					<div class="display-inline-top default-cursor margin-bottom-10 margin-right-5 margin-left-5 padding-all-10 min-width-300 bggray-white box-shadow-light border-top border-blue width-20per text-center">
						<a href="<?php echo BASEURL; ?>products/view_product" class="black-color height-200px">
							<div class="width-100percent  fix-image">
								<img src="../assets/images/thai-corn.jpg" class="width-100per "/>
							</div>
							<p class="margin-top-20 font-20"><strong>Thailand Corn</strong></p>
							<hr/>
							<p class="margin-top-10"><strong>SKU#</strong> 122213321321</p>
							<p class="margin-top-10">200 Items (45 Batches)</p>
						</a>
					</div> 				
				</div>
			</div>
		</div>
				
	</section>
