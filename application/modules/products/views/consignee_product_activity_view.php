<div class="main min-width-1200">
			<div class="breadcrumbs no-margin-left padding-left-50">
				<ul>
					<li><a href="consignee-inventory.php">Item Inventory</a></li>
					<li><span>></span></li>
					<li><strong class="black-color">Austrailian Wheat</strong></li>
				</ul>
			</div>
			<div class="center-main">	
				<div class="border-top border-blue bggray-white box-shadow-light margin-top-20 padding-all-20">
					<div class="display-inline-top width-20per">
						<img class="img-responsive " src="../assets/images/australian-wheat.jpg">
					</div>
 					<div class="display-inline-mid width-80per margin-left-10 text-left">
 					<div class="bg-light-gray padding-all-10">
 						<p class="black-color font-20 font-bold f-left">SKU # 1234567810 - Holcim Cement</p>
 						<div class="f-right padding-top-3">
							<p class="black-color font-16 font-bold padding-right-10 f-left">Total Inventory</p>
							<p class="black-color font-16 font-500 f-left"><span class="font-bold blue-color vertical-baseline">100000</span> MT</p>
							<div class="clear"></div>
 						</div>
 						<div class="clear"></div>
 					</div>
 					<div class="bggray-white padding-all-15 padding-left-10">
 						<p class="black-color font-14 font-bold display-inline-mid width-20per">Item Category</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-35per">Corn</p>
 						<p class="black-color font-14 font-bold display-inline-mid width-15percent">Unit Price</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-15percent">13,000 Php</p>
 					</div>

 					<div class="bg-light-gray padding-all-15 padding-left-10">
 						<p class="black-color font-14 font-bold display-inline-mid width-20per">Quantity Threshold</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-35per"></p>
 						<p class="black-color font-14 font-bold display-inline-mid width-15percent">Item Shelf Life</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-15percent">1200 Days</p>
 					</div>

 					<div class="bggray-white padding-all-15 padding-left-10">
 						<p class="black-color font-14 font-bold display-inline-mid width-20per padding-left-120">Minimum</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-35per">1000 MT</p>
 						<p class="black-color font-14 font-bold display-inline-mid width-15percent">Description</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-15percent italic">Ang Cement na matibay</p>
 					</div>

 					<div class="bg-light-gray padding-all-15 padding-left-10">
 						<p class="black-color font-14 font-bold display-inline-mid width-20per padding-left-120	">Maximum</p>
 						<p class="black-color font-14 font-400 display-inline-mid width-35per">15000 MT</p>
 					</div>
					</div>
				</div>

				<div class="tab-container font-0 text-left margin-top-30">
					<div class="batch-list display-inline-mid padding-all-20 text-left bggray-white width-33percent margin-right-7 border-top border-blue no-margin-left">
						<input type="radio" name="tab" id="tab-batch" checked="checked">
						<label for="tab-batch" class="default-cursor font-20 font-bold">Batch List</label>
					</div>

					<div class="consign-assignment display-inline-mid padding-all-20 margin-right-7 text-left bggray-white width-33percent no-margin-left">
						<input type="radio" name="tab" id="tab-consignee">
						<label for="tab-consignee" class="default-cursor font-20 font-bold">Consignee Assignments</label>
					</div>

					<div class="product-act display-inline-mid padding-all-20 text-left bggray-white width-33percent no-margin-left">
						<input type="radio" name="tab" id="tab-product">
						<label for="tab-product" class="default-cursor font-20 font-bold">Item Activity</label>
					</div>
					
					<div class="tabs padding-all-10 bggray-white" id="tab-bat">

						<!--1first panel-->
						<div class="panel-group margin-top-20">
							<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							<!-- step 1 -->
								<div class="panel-heading panel-heading">
									<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">							
										<i class="fa fa-caret-down margin-right-5"></i>
										Warehouse 1
									</a>					
									<p class="f-right font-15 font-500  "><span class="font-green vertical-baseline">12600</span>/50000 KG in Storage</p>
									<div class="clear"></div>
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white">
									<!--first panel-->
										<div class="panel-group margin-top-20">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														SKU #1234567890 - Japanese Corn
													</a>
													<p class="f-right font-15 font-500  ">Total Quantity: <span class="font-green vertical-baseline ">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left height-auto min-height-50px">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5   width-10percent " rowspan="2">Batch No.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Vessel</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent" rowspan="2">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent tbl-dark-color " colspan="3">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>
																</tr>
																<tr>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>
																</tr>
															</thead>
														</table>
														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV CHINO</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">50 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG<br>(14pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV NORMANDY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">150 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DEADMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">250 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent"></p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV BIGMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DUTCHMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">400 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV SILANTRY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														
													

													</div>
												</div>
											</div>
										</div>
									<!--end of first panel-->

									<!--second panel-->
									<div class="panel-group margin-top-20">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														SKU #1234567890 - Italian Wheat
													</a>
													<p class="f-right font-15 font-500  ">Total Quantity: <span class="font-green vertical-baseline ">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5   width-10percent " rowspan="2">Batch No.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Vessel</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent" rowspan="2">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent tbl-dark-color " colspan="3">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>
																</tr>
																<tr>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>
																</tr>
															</thead>
														</table>
														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV CHINO</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">50 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG<br>(14pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV NORMANDY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">150 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DEADMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">250 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent"></p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV BIGMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DUTCHMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">400 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV SILANTRY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														
													

													</div>
												</div>
											</div>
										</div>

									<!--end of second panel-->

									<!--third panel-->
									<div class="panel-group margin-top-20">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														SKU #1234567890 - Chinese Wheat
													</a>
													<p class="f-right font-15 font-500  ">Total Quantity: <span class="font-green vertical-baseline ">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5   width-10percent " rowspan="2">Batch No.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Vessel</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent" rowspan="2">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent tbl-dark-color " colspan="3">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>
																</tr>
																<tr>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>
																</tr>
															</thead>
														</table>
														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV CHINO</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">50 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG<br>(14pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV NORMANDY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">150 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DEADMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">250 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent"></p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV BIGMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DUTCHMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">400 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV SILANTRY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														
													

													</div>
												</div>
											</div>
										</div>
									
									<!--end of third panel-->
									</div>
								</div>
							</div>
						</div>
						<!--1end of first panel-->

						<!--2second panel-->
						<div class="panel-group margin-top-20">
							<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							<!-- step 1 -->
								<div class="panel-heading panel-heading">
									<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">							
										<i class="fa fa-caret-down margin-right-5"></i>
										Warehouse 2
									</a>					
									<p class="f-right font-15 font-500  "><span class="font-green vertical-baseline">12600</span>/50000 KG in Storage</p>
									<div class="clear"></div>
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white">
									<!--first panel-->
										<div class="panel-group margin-top-20">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														SKU #1234567890 - Japanese Corn
													</a>
													<p class="f-right font-15 font-500  ">Total Quantity: <span class="font-green vertical-baseline ">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left height-auto min-height-50px">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5   width-10percent " rowspan="2">Batch No.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Vessel</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent" rowspan="2">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent tbl-dark-color " colspan="3">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>
																</tr>
																<tr>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>
																</tr>
															</thead>
														</table>
														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV CHINO</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">50 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG<br>(14pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV NORMANDY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">150 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DEADMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">250 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent"></p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV BIGMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DUTCHMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">400 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV SILANTRY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														
													

													</div>
												</div>
											</div>
										</div>
									<!--end of first panel-->

									<!--second panel-->
									<div class="panel-group margin-top-20">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														SKU #1234567890 - Italian Wheat
													</a>
													<p class="f-right font-15 font-500  ">Total Quantity: <span class="font-green vertical-baseline ">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5   width-10percent " rowspan="2">Batch No.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Vessel</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent" rowspan="2">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent tbl-dark-color " colspan="3">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>
																</tr>
																<tr>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>
																</tr>
															</thead>
														</table>
														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV CHINO</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">50 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG<br>(14pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV NORMANDY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">150 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DEADMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">250 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent"></p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV BIGMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DUTCHMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">400 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV SILANTRY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														
													

													</div>
												</div>
											</div>
										</div>

									<!--end of second panel-->

									<!--third panel-->
									<div class="panel-group margin-top-20">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														SKU #1234567890 - Chinese Wheat
													</a>
													<p class="f-right font-15 font-500  ">Total Quantity: <span class="font-green vertical-baseline ">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5   width-10percent " rowspan="2">Batch No.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Vessel</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent" rowspan="2">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent tbl-dark-color " colspan="3">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>
																</tr>
																<tr>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>
																</tr>
															</thead>
														</table>
														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV CHINO</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">50 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG<br>(14pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV NORMANDY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">150 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DEADMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">250 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent"></p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV BIGMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DUTCHMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">400 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV SILANTRY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														
													

													</div>
												</div>
											</div>
										</div>
									
									<!--end of third panel-->
									</div>
								</div>
							</div>
						</div>
						<!--2end of second panel-->

						<!--3third panel-->
						<div class="panel-group margin-top-20">
							<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							<!-- step 1 -->
								<div class="panel-heading panel-heading">
									<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">							
										<i class="fa fa-caret-down margin-right-5"></i>
										Silo 1
									</a>					
									<p class="f-right font-15 font-500  "><span class="font-green vertical-baseline">12600</span>/50000 KG in Storage</p>
									<div class="clear"></div>
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white">
									<!--first panel-->
										<div class="panel-group margin-top-20">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														SKU #1234567890 - Japanese Corn
													</a>
													<p class="f-right font-15 font-500  ">Total Quantity: <span class="font-green vertical-baseline ">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left height-auto min-height-50px">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5   width-10percent " rowspan="2">Batch No.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Vessel</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent" rowspan="2">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent tbl-dark-color " colspan="3">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>
																</tr>
																<tr>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>
																</tr>
															</thead>
														</table>
														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV CHINO</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">50 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG<br>(14pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV NORMANDY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">150 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DEADMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">250 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent"></p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV BIGMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DUTCHMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">400 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV SILANTRY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														
													

													</div>
												</div>
											</div>
										</div>
									<!--end of first panel-->

									<!--second panel-->
									<div class="panel-group margin-top-20">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														SKU #1234567890 - Italian Wheat
													</a>
													<p class="f-right font-15 font-500  ">Total Quantity: <span class="font-green vertical-baseline ">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5   width-10percent " rowspan="2">Batch No.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Vessel</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent" rowspan="2">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent tbl-dark-color " colspan="3">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>
																</tr>
																<tr>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>
																</tr>
															</thead>
														</table>
														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV CHINO</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">50 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG<br>(14pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV NORMANDY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">150 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DEADMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">250 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent"></p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV BIGMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DUTCHMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">400 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV SILANTRY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														
													

													</div>
												</div>
											</div>
										</div>

									<!--end of second panel-->

									<!--third panel-->
									<div class="panel-group margin-top-20">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														SKU #1234567890 - Chinese Wheat
													</a>
													<p class="f-right font-15 font-500  ">Total Quantity: <span class="font-green vertical-baseline ">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5   width-10percent " rowspan="2">Batch No.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Vessel</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent" rowspan="2">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent tbl-dark-color " colspan="3">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>
																</tr>
																<tr>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>
																</tr>
															</thead>
														</table>
														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV CHINO</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">50 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG<br>(14pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV NORMANDY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">150 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DEADMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">250 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent"></p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV BIGMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DUTCHMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">400 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV SILANTRY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														
													

													</div>
												</div>
											</div>
										</div>
									
									<!--end of third panel-->
									</div>
								</div>
							</div>
						</div>
						<!--3end of third panel-->

						<!--4start of 4th panel-->
						<div class="panel-group margin-top-20">
							<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
							<!-- step 1 -->
								<div class="panel-heading panel-heading">
									<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">							
										<i class="fa fa-caret-down margin-right-5"></i>
										Silo 2
									</a>					
									<p class="f-right font-15 font-500  "><span class="font-green vertical-baseline">12600</span>/50000 KG in Storage</p>
									<div class="clear"></div>
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white">
									<!--first panel-->
										<div class="panel-group margin-top-20">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														SKU #1234567890 - Japanese Corn
													</a>
													<p class="f-right font-15 font-500  ">Total Quantity: <span class="font-green vertical-baseline ">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left height-auto min-height-50px">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5   width-10percent " rowspan="2">Batch No.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Vessel</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent" rowspan="2">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent tbl-dark-color " colspan="3">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>
																</tr>
																<tr>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>
																</tr>
															</thead>
														</table>
														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV CHINO</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">50 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG<br>(14pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV NORMANDY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">150 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DEADMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">250 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent"></p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV BIGMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DUTCHMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">400 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV SILANTRY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														
													

													</div>
												</div>
											</div>
										</div>
									<!--end of first panel-->

									<!--second panel-->
									<div class="panel-group margin-top-20">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														SKU #1234567890 - Italian Wheat
													</a>
													<p class="f-right font-15 font-500  ">Total Quantity: <span class="font-green vertical-baseline ">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5   width-10percent " rowspan="2">Batch No.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Vessel</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent" rowspan="2">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent tbl-dark-color " colspan="3">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>
																</tr>
																<tr>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>
																</tr>
															</thead>
														</table>
														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV CHINO</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">50 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG<br>(14pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV NORMANDY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">150 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DEADMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">250 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent"></p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV BIGMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DUTCHMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">400 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV SILANTRY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														
													

													</div>
												</div>
											</div>
										</div>

									<!--end of second panel-->

									<!--third panel-->
									<div class="panel-group margin-top-20">
											<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
												<!-- step 1 -->
												<div class="panel-heading panel-heading">
													<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
														<i class="fa fa-caret-down margin-right-5"></i>
														SKU #1234567890 - Chinese Wheat
													</a>
													<p class="f-right font-15 font-500  ">Total Quantity: <span class="font-green vertical-baseline ">4200 </span> KG</p>
													<div class="clear"></div>
												</div>
												<div class="panel-collapse collapse in">
													<div class="panel-body bggray-white font-0">
														<table class="tbl-4c3h margin-top-20 text-left">
															<thead>
																<tr>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5   width-10percent " rowspan="2">Batch No.</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Batch Date</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Vessel</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent" rowspan="2">Location</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-20percent tbl-dark-color " colspan="3">Quantity</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Remaining Shelf Life</th>
																	<th class="black-color font-14 font-500 no-padding-left text-center padding-top-5 padding-bottom-5  width-10percent" rowspan="2">Status</th>
																</tr>
																<tr>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-light-gray">Bulk</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 color-lighter-gray">Bag</th>
																	<th class="black-color font-12 font-500 no-padding-left text-center border-top-none width-10percent padding-top-5 padding-bottom-5 acc-dark-color">Total</th>
																</tr>
															</thead>
														</table>
														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV CHINO</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">50 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG<br>(14pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV NORMANDY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">150 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DEADMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">250 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent"></p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV BIGMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV DUTCHMAN</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">400 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														<div class="tbl-like text-left">
															<div class="first-tbl no-padding-all mngt-tbl-content height-auto min-height-50px font-0">
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">12345678</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">01-Mar-2016</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">MV SILANTRY</p>
																</div>
																<div class="display-inline-mid width-20percent text-left  padding-top-5">
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Area 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Storage 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Shelf 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Rack 1</p>
																	<i class="fa fa-caret-right font-15 display-inline-mid padding-right-20 padding-left-20"></i>
																	<p class="font-14 font-400 f-none display-inline-mid width-initial">Palette 1</p>
																						
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">100 KG<br>(2pcs)</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">700 KG</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent">300 days</p>
																</div>
																<div class="display-inline-mid width-10percent text-center">
																	<p class="font-400 f-none font-14 width-100percent green-color">Unrestricted</p>
																</div>
															</div>
															<div class="hover-tbl text-center">
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="split-batch">Split Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="repack-batch">Repack Batch</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="adjust-quantity">Adjust Quantity</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="update-status">Update Status</button>
																
																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="remove-batch">Remove Batch</button>

																	<button class="btn general-btn margin-right-20 modal-trigger" modal-target="return-batch">Return Batch</button>
															</div>
														</div>

														
													

													</div>
												</div>
											</div>
										</div>
									
									<!--end of third panel-->
									</div>
								</div>
							</div>
						</div>
						<!--4end of 4th panel-->
					</div>

					<div class="tabs padding-all-10 bggray-white" id="tab-con">
						<table class="tbl-4c3h">
							<thead>
								<tr>
									<th class=" font-14 font-bold black-color no-padding-left">Consignee Name</th>
									<th class=" font-14 font-bold black-color no-padding-left">Quantity Assigned</th>
								</tr>
							</thead>
							<tbody class="text-center">
								<tr>
									<td class=" font-14 font-400 black-color no-padding-left">Alpha Foods Inc.</td>
									<td class=" font-14 font-400 black-color no-padding-left">25000 MT</td>
								</tr>
								<tr>
									<td class=" font-14 font-400 black-color no-padding-left">Beta Feeds Corporation</td>
									<td class=" font-14 font-400 black-color no-padding-left">25000 MT</td>
								</tr>
								<tr>
									<td class=" font-14 font-400 black-color no-padding-left">Charlie Chicken Corporation</td>
									<td class=" font-14 font-400 black-color no-padding-left">25000 MT</td>
								</tr>
								<tr>
									<td class=" font-14 font-400 black-color no-padding-left">Delta Ducks Inc.</td>
									<td class=" font-14 font-400 black-color no-padding-left">25000 MT</td>
								</tr>
							</tbody>
						</table>
					</div>	

					<div class="tabs padding-all-10 bggray-white" id="tab-prod">
						<!-- start Adjustment-->
						<div class="panel-group">
							<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
								<!-- step 1 -->
								<div class="panel-heading panel-heading">
									<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
										<i class="fa fa-caret-down margin-right-5"></i>
										Adjustment
									</a>
									<div class="clear"></div>
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white font-0">
											<!--start product activity-->
											<div class="padding-bottom-20 bggray-white box-shadow-light">
											<!-- sample 1 -->
												<div class="padding-all-10 dashboard-icon">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10">September 02, 2015 06:00PM</p>
													
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid">
															<p class="display-inline-mid margin-left-10">Jules Ignacio has adjusted a batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Previous Qty</th>
																		<th class=" font-14 font-bold black-color no-padding-left">New QTY</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Reason</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left">12345677</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">600 KG</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">700 KG</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">Nam dapibus nisl vitae elit fringilla rutrum. Aenean Solicitudin, erat a elementum.</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											<!-- sample 2-->
												<div class="padding-all-10 dashboard-icon">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10">September 01 , 2015 06:00 PM</p>
													
													<div class="margin-left-50">	
														<div class=" to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid">
															<p class="display-inline-mid margin-left-10">Jules Ignacio has adjusted a batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Previous Qty</th>
																		<th class=" font-14 font-bold black-color no-padding-left">New QTY</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Reason</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left">12345677</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">600 KG</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">700 KG</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">Nam dapibus nisl vitae elit fringilla rutrum. Aenean Solicitudin, erat a elementum.</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</div>
											<!--end product activity-->
									</div>
								</div>
							</div>
						</div>
						<!-- ending Adjustment-->

						<!-- start Receiving-->
						<div class="panel-group">
							<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
								<!-- step 1 -->
								<div class="panel-heading panel-heading">
									<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
										<i class="fa fa-caret-down margin-right-5"></i>
										Receiving
									</a>
									<div class="clear"></div>
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white font-0">
									
											<!--start product activity-->
											<div class="padding-bottom-20 bggray-white box-shadow-light">
											<!-- sample 1 -->
												<div class="padding-all-10 dashboard-icon">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10">September 02, 2015 06:00PM</p>
													
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid">
															<p class="display-inline-mid margin-left-10">Jules Ignacio has received a batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Receiving No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">PO No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Receiving Date</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Origin</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Items</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left">1234657890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">123456</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">
																			<p>1234567890</p>
																			<p>1234567891</p>
																		</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">01-Mar-2015</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">Vessel</td>
																		<td>
																			<ul class="padding-left-10">
																				<li class="font-14 font-400 black-color list-style-square padding-left-20 ">#12345678 - Dinorado Rice</li>
																				<li class="font-14 font-400 black-color list-style-square padding-left-20 ">#12345679 - Brazilian Wheat</li>
																			</ul>
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											<!-- sample 2-->
												<div class="padding-all-10 dashboard-icon">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10">September 01 , 2015 06:00 PM</p>
													
													<div class="margin-left-50">	
														<div class=" to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid">
															<p class="display-inline-mid margin-left-10">Jules Ignacio has received a batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Receiving No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">PO No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Receiving Date</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Origin</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Items</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left">1234657890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">123456</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">
																			<p>1234567890</p>
																			<p>1234567891</p>
																		</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">01-Mar-2015</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">Vessel</td>
																		<td>
																			<ul class="padding-left-10">
																				<li class="font-14 font-400 black-color list-style-square padding-left-20 ">#12345678 - Dinorado Rice</li>
																				<li class="font-14 font-400 black-color list-style-square padding-left-20 ">#12345679 - Brazilian Wheat</li>
																			</ul>
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</div>
											<!--end product activity-->
									</div>
								</div>
							</div>
						</div>
						<!-- end Receiving-->

						<!-- start Withdrawals-->
						<div class="panel-group">
							<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
								<!-- step 1 -->
								<div class="panel-heading panel-heading">
									<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
										<i class="fa fa-caret-down margin-right-5"></i>
										Withdrawals
									</a>
									<div class="clear"></div>
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white font-0">
									
											<!--start product activity-->
											<div class="padding-bottom-20 bggray-white box-shadow-light">
											<!-- sample 1 -->
												<div class="padding-all-10 dashboard-icon">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10">September 02, 2015 06:00PM</p>
													
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid">
															<p class="display-inline-mid margin-left-10">Jules Ignacio has withdrawn a batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Withdrawal No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">ATL No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">CDR No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Withdrawal Date</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Origin</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Consignee</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Mode of Travel</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left">1234657890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">123456</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">123456</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">01-Mar-2016</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">Vessel</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">Alpha Foods Inc.</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">Vessel</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											<!-- sample 2-->
												<div class="padding-all-10 dashboard-icon">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10">September 01 , 2015 06:00 PM</p>
													
													<div class="margin-left-50">	
														<div class=" to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid">
															<p class="display-inline-mid margin-left-10">Jules Ignacio has withdrawn a batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Withdrawal No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">ATL No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">CDR No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Withdrawal Date</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Origin</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Consignee</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Mode of Travel</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left">1234657890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">123456</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">123456</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">01-Mar-2016</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">Vessel</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">Alpha Foods Inc.</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">Vessel</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</div>
											<!--end product activity-->
									</div>
								</div>
							</div>
						</div>

						<!-- Ending Withdrawals-->

						<!-- start Transfer-->
						<div class="panel-group">
							<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
								<!-- step 1 -->
								<div class="panel-heading panel-heading">
									<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
										<i class="fa fa-caret-down margin-right-5"></i>
										Transfer
									</a>
									<div class="clear"></div>
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white font-0">
									
											<!--start product activity-->
											<div class="padding-bottom-20 bggray-white box-shadow-light">
											<!-- sample 1 -->
												<div class="padding-all-10 dashboard-icon">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10">September 02, 2015 06:00PM</p>
													
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid">
															<p class="display-inline-mid margin-left-10">Jules Ignacio has transferred a batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Transfer No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Origin</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Destination</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Reason</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left">1234657890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">1234657890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left max-width-300px text-left line-height-25">
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
																		</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">1234567890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left max-width-200px">In hac habitasse platea disctumst. Vivamus adipiscing fermentum quam volutpat aliquam.</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											<!-- sample 2-->
												<div class="padding-all-10 dashboard-icon">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10">September 01 , 2015 06:00 PM</p>
													
													<div class="margin-left-50">	
														<div class=" to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid">
															<p class="display-inline-mid margin-left-10">Jules Ignacio has transferred a batch</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Transfer No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Batch No.</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Origin</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Destination</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Reason</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left">1234657890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">1234657890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left max-width-300px text-left line-height-25">
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 9</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Area 54</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 10</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Rack 1</p>
																			<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
																			<p class="font-14 font-400 display-inline-mid padding-right-10">Palette 1</p>
																		</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">1234567890</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left max-width-200px">In hac habitasse platea disctumst. Vivamus adipiscing fermentum quam volutpat aliquam.</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</div>
											<!--end product activity-->
									</div>
								</div>
							</div>
						</div>

						<!-- ending Transfer-->

						<!-- start Batch Repack-->
						<div class="panel-group">
							<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
								<!-- step 1 -->
								<div class="panel-heading panel-heading">
									<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
										<i class="fa fa-caret-down margin-right-5"></i>
										Batch Repack
									</a>
									<div class="clear"></div>
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white font-0">
									
											<!--start product activity-->
											<div class="padding-bottom-20 bggray-white box-shadow-light">
											<!-- sample 1 -->
												<div class="padding-all-10 dashboard-icon">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10">September 02, 2015 06:00PM</p>
													
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid">
															<p class="display-inline-mid margin-left-10">Jules Ignacio has repacked Batch No. 13245678</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Previous Quantity</th>
																		<th class=" font-14 font-bold black-color no-padding-left">New Quantity</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left">600 KG</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">650 KG</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											<!-- sample 2-->
												<div class="padding-all-10 dashboard-icon">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10">September 01 , 2015 06:00 PM</p>
													
													<div class="margin-left-50">	
														<div class=" to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid">
															<p class="display-inline-mid margin-left-10">Jules Ignacio has repacked Batch No. 13245678</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Previous Quantity</th>
																		<th class=" font-14 font-bold black-color no-padding-left">New Quantity</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left">600 KG</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left">650 KG</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</div>
											<!--end product activity-->
									</div>
								</div>
							</div>
						</div>
						<!-- End Batch Repack-->

						<!-- Start Batch Return-->
						<div class="panel-group">
							<div class="border_top border-top-none box-shadow-dark padding-all-10 bggray-white">
								<!-- step 1 -->
								<div class="panel-heading panel-heading">
									<a class="collapsed active black-color default-cursor f-left font-20 font-bold" href="#">	
										<i class="fa fa-caret-down margin-right-5"></i>
										Batch Returns
									</a>
									<div class="clear"></div>
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white font-0">
									
											<!--start product activity-->
											<div class="padding-bottom-20 bggray-white box-shadow-light">
											<!-- sample 1 -->
												<div class="padding-all-10 dashboard-icon">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10">September 02, 2015 06:00PM</p>
													
													<div class="margin-left-50 side-border">	
														<div class="stock-product-act to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid">
															<p class="display-inline-mid margin-left-10">Jules Ignacio has returned a part of Batch No. 13245678</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Quantity to Return</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Bags to Return</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Reason</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-33percent">300 Bags</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-33percent">50 KG (1 pc)</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-33percent">Vestibulum rutrum quam vitae fringilla tincidunt. Suspendisse nec tortor urna. Ut laoreet sodales nisi, quis iaculis nulla iaculis vitae. Dinec sagittis faucibus lacus eget blandit.</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg top-9px"><i class="fa fa-newspaper-o"></i></div>
												</div>
											<!-- sample 2-->
												<div class="padding-all-10 dashboard-icon">
													<p class="margin-left-50 font-14 font-500 margin-bottom-10">September 01 , 2015 06:00 PM</p>
													
													<div class="margin-left-50">	
														<div class=" to-do box-shadow-dark padding-all-10 font-14 font-400">
															<img src="../assets/images/profile/profile_j.png" alt="user profile-pic" class="width-50px display-inline-mid">
															<p class="display-inline-mid margin-left-10">Jules Ignacio has returned a part of Batch No. 13245678</p>
															<table class="tbl-4c3h margin-top-20">
																<thead>
																	<tr>
																		<th class=" font-14 font-bold black-color no-padding-left">Quantity to Return</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Bags to Return</th>
																		<th class=" font-14 font-bold black-color no-padding-left">Reason</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-33percent">300 Bags</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-33percent">50 KG (1 pc)</td>
																		<td class="font-14 font-400 black-color text-center no-padding-left width-33percent">Vestibulum rutrum quam vitae fringilla tincidunt. Suspendisse nec tortor urna. Ut laoreet sodales nisi, quis iaculis nulla iaculis vitae. Dinec sagittis faucibus lacus eget blandit.</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="side-icon light-blue-bg"><i class="fa fa-newspaper-o"></i></div>
												</div>
											</div>
											<!--end product activity-->
									</div>
								</div>
							</div>
						</div>
					<!-- End Batch Return-->
					</div>

				</div>
			</div>

		</section>
		
		<!--modal start update status -->
		<div class="modal-container" modal-id="update-status">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Update Status</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-bottom-20">
						<p class="font-14 font-400 display-inline-mid margin-top-5 no-margin-all padding-right-20">New Status:</p>
						<div class="select  display-inline-mid width-250px">
							<select class="medium">
								<option value="Quality Assurance">Quality Assurance</option>
							</select>
						</div>
					</div>
					<div>
						<p class="font-14 font-400 display-inline-mid margin-top-5 no-margin-all padding-right-20 padding-bottom-20">Please provide a reason for status change:</p>
						<textarea class="height-150px"></textarea>

					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="update-status-confirmation">Confirm</button>
				</div>
			</div>	
		</div>
		<!--modal end update status -->

		<!--modal start update status confirmation -->
		<div class="modal-container" modal-id="update-status-confirmation">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Update Status</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Batch No. 12345678 status changed from Unrestricted to Quality Assurance</p>
					</div>
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end update status confirmation -->

		<!--modal start of adjust quantity -->
		<div class="modal-container" modal-id="adjust-quantity">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Adjust Quantity</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-bottom-20">
						<p class="font-14 font-400 display-inline-mid width-25percent no-margin-all">Batch No.:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-all">12345678</p>
					</div>

					<div class="padding-bottom-10">
						<p class="font-14 font-400 display-inline-mid width-25percent no-margin-all">Current Quantity:</p>
						<p class="font-14 font-400 display-inline-mid width-25percent no-margin-all">300 Bags</p>
						<p class="font-14 font-400 display-inline-mid italic font-green no-margin-all padding-left-35">+200 Bags</p>
					</div>
					<div class="font-0">
						<p class="font-14 font-400 display-inline-top width-25percent no-margin-all padding-top-15">Adjustment:</p>
						<div class="display-inline-mid width-75percent">
							<div class="width-100percent padding-top-6">
								<div class="display-inline-mid width-40percent">
									<input type="radio" class="width-20px display-inline-mid no-margin-all transform-scale" id="increaseBy" name="increaseAndDecrease" checked>
									<label for="increaseBy" class="font-14 font-400 display-inline-mid">Increase By</label>
								</div>
								<div class="display-inline-mid">
									<input type="text" class="width-100px display-inline-mid" id="text-increase">
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-left-20">Bags</p>
								</div>
							</div>
							<div class="width-100percent padding-top-10">
								<div class="display-inline-mid width-40percent">
									<input type="radio" class="width-20px display-inline-mid no-margin-all transform-scale" id="decreaseBy" name="increaseAndDecrease">
									<label for="decreaseBy" class="font-14 font-400 display-inline-mid">Decrease By</label>
								</div>
								<div class="display-inline-mid padding-bottom-20">
									<input type="text" class="width-100px display-inline-mid" id="text-decrease">
									<p class="font-14 font-400 display-inline-mid no-margin-all padding-left-20">Bags</p>
								</div>
							</div>
						</div>
					</div>
					<div>
						<p class="font-14 font-400 no-margin-all padding-bottom-10">Please provide a reason for batch adjustment:</p>
						<textarea class=" height-100px"></textarea>
					</div>
					
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="adjust-quantity-password">Adjust Quantity</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of adjust quantity-->

		<!--modal start of adjust quantity password -->
		<div class="modal-container" modal-id="adjust-quantity-password">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Adjust Quantity</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<p class="font-14 font-400 no-margin-hor">Please input your password to proceed:</p>
					<p class="font-14 font-400 display-inline-mid width-20percent no-margin-all">Password:</p>
					<input type="password" class="t-medium">
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="adjust-quantity-confirmation">Enter</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of adjust quantity password-->

		<!--modal start adjust quantity confirmation -->
		<div class="modal-container" modal-id="adjust-quantity-confirmation">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Adjust Quantity</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Batch No. 12345678 quantity has been adjusted.</p>
						<p class="font-14 font-400 white-color padding-left-50">New Quantity: 300 Bags</p>
						<p class="font-14 font-400 white-color padding-left-50">Previous Quantity: 500 Bags</p>

					</div>
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end adjust quantity confirmation -->

		<!--modal start remove batch -->
		<div class="modal-container" modal-id="remove-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Remove Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div>
						<p class="font-14 font-400 display-inline-mid margin-top-5 no-margin-all padding-right-20 padding-bottom-20">Please provide a reason for batch removal:</p>
						<textarea class="height-150px"></textarea>

					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="remove-batch-confirmation">Remove Batch</button>
				</div>
			</div>	
		</div>
		<!--modal end remove batch -->

		<!--modal start remove batch confirmation -->
		<div class="modal-container" modal-id="remove-batch-confirmation">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Adjust Quantity</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Batch No. 12345678 has been removed.</p>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end remove batch confirmation -->

		<!--modal start of repack batch -->
		<div class="modal-container" modal-id="repack-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Repack Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-hor">Batch No.:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor">12345678</p>
					</div>
					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-hor">Current Quantity:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor">300 Bags</p>
					</div>

					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-all">Repacking Quantity:</p>
						<input type="text" class="width-100px display-inline-mid">
						<div class="select medium">
						<select>
							<option value="Bags">Bags</option>
						</select>
						</div>
					</div>

				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="repack-batch-confirmation">Repack Batch</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of repack batch-->

		<!--modal start repack batch confirmation -->
		<div class="modal-container" modal-id="repack-batch-confirmation">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Repack Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>
				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">Batch No. 12345678 quantity has been repacked.</p>
						<p class="font-14 font-400 white-color padding-left-50">New Quantity: 30 Bags</p>
						<p class="font-14 font-400 white-color padding-left-50">Previous Quantity: 300 Bags</p>

					</div>
				</div>
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end repack batch confirmation -->

		<!--modal start of return batch -->
		<div class="modal-container" modal-id="return-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Return Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-hor">Batch No.:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor">12345678</p>
					</div>
					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-hor">Current Quantity:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor">300 Bags</p>
					</div>

					<div>
						<p class="font-14 font-400 display-inline-mid width-30percent no-margin-all">Returning Quantity:</p>
						<input type="text" class="width-100px display-inline-mid">
						<p class="font-14 font-400 display-inline-mid no-margin-ver">Bags</p>
					</div>
					<div class="padding-top-20">
						<p class="font-14 font-400 no-margin-hor">Please provide a reason for batch returns:</p>
						<textarea class="height-150px"></textarea>
					</div>

				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="return-batch-confirmation">Return Batch</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of return batch-->

		<!--modal start return batch confirmation -->
		<div class="modal-container" modal-id="return-batch-confirmation">
			<div class="modal-body small">				
				<div class="modal-head">
					<h4 class="text-left">Return Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div class="padding-all-10 bggray-7cace5">
						<p class="font-14 font-400 white-color">A part of Batch No. 12345678 has been returned.</p>
						<p class="font-14 font-400 white-color padding-left-50">Quantity Returned: 50 Bags</p>
					</div>
				</div>

			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me">OK</button>
				</div>
			</div>	
		</div>
		<!--modal end return batch confirmation -->


		<!--modal start of split batch -->
		<div class="modal-container" modal-id="split-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Split Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<div>
						<p class="font-14 font-400 display-inline-mid no-margin-hor width-30percent">Batch No.:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor">12345678</p>
					</div>
					<div>
						<p class="font-14 font-400 display-inline-mid no-margin-hor width-30percent">Current Quantity:</p>
						<p class="font-14 font-400 display-inline-mid no-margin-hor">300 Bags</p>
					</div>

					<div class="padding-bottom-10">
						<p class="font-14 font-400 display-inline-mid no-margin-all f-left">Batch Splits</p>
						<a href="#" class="font-14 font-400 f-right">
							+Add Batch Split
						</a>
						<div class="clear"></div>
					</div>

					<div class="padding-all-5 bggray-white margin-bottom-10">
						<p class="font-14 font-400 no-margin-ver display-inline-mid">1. </p>
						<input type="text" class="width-400px display-inline-mid" placeholder="Batch Name">
						<div class="padding-top-10 padding-bottom-10 padding-left-25">
							<p class="font-14 font-400 no-margin-ver display-inline-mid">Quantity: </p>
							<input type="text" class="t-small display-inline-mid margin-right-7">
							<div class="select medium">
								<select>
									<option value="Bags">Bags</option>
								</select>
							</div>
						</div>
					</div>

					<div class="padding-all-5 bggray-white margin-bottom-10">
						<p class="font-14 font-400 no-margin-ver display-inline-mid">2. </p>
						<input type="text" class="width-400px display-inline-mid" placeholder="Batch Name">
						<div class="padding-top-10 padding-bottom-10 padding-left-25">
							<p class="font-14 font-400 no-margin-ver display-inline-mid">Quantity: </p>
							<input type="text" class="t-small display-inline-mid margin-right-7">
							<div class="select medium">
								<select>
									<option value="Bags">Bags</option>
								</select>
							</div>
						</div>
					</div>

					<div class="padding-all-5 bggray-white margin-bottom-10">
						<p class="font-14 font-400 no-margin-ver display-inline-mid">3. </p>
						<input type="text" class="width-400px display-inline-mid" placeholder="Batch Name">
						<div class="padding-top-10 padding-bottom-10 padding-left-25">
							<p class="font-14 font-400 no-margin-ver display-inline-mid">Quantity: </p>
							<input type="text" class="t-small display-inline-mid margin-right-7">
							<div class="select medium">
								<select>
									<option value="Bags">Bags</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="split-batch-confirmation">Split Batch</button>
				</div>
			</div>	
		</div>
		<!--- modal ending of split batch-->

		<!--modal start split batch confirmation -->
		<div class="modal-container" modal-id="split-batch-confirmation">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Split Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
					<p class="font-14 font-400">Batch No. 12345678 has been split into:</p>
					<table class="tbl-4c3h">
						<thead>
							<tr>
								<th class="no-padding-left">Batch No.</th>
								<th class="no-padding-left">Quantity</th>
							<tr>
						</thead>
						<tbody class="text-center">
							<tr>
								<td>12345678A</td>
								<td>200 Bags</td>
							</tr>
							<tr>
								<td>12345678A</td>
								<td>100 Bags</td>
							</tr>
							
						</tbody>
					</table>
				</div>

				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="">OK</button>
				</div>
			</div>	
		</div>
		<!--- modal ending split batch confirmation-->