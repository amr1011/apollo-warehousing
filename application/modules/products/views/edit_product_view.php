	
			<div class="main min-width-1024">
				<div class="breadcrumbs no-margin-left padding-left-50">
					<ul>
						<li><a href="<?php echo BASEURL . "products/product_management"; ?>" class="font-15 font-400 black-color">Item Management</a></li>
						<li><span>></span></li>
						<li><a href="<?php echo BASEURL . "products/view_product"; ?>" class="font-15 font-400 black-color product-name-link"></a></li>
						<li><span>></span></li>
						<li class="font-bold black-color">Edit Item Details</li>

					</ul>
				</div>
				<div class="semi-main">
					<div class="f-left margin-top-10">	

						<i id="deleteProduct" class="fa fa-trash-o default-cursor font-20 gray-color modal-trigger" modal-target="trash-modal"></i>

						<!-- <i class="fa fa-trash-o default-cursor font-20 gray-color modal-trigger delete-button" modal-target="trash-modal"></i> -->

					</div>
					<div class="f-right width-220">
						<a href="<?php echo BASEURL; ?>products/view_product">
							<button type="button" class="btn-default ">Cancel</button>
						</a>
						<button id="submit_edit_product" type="button" class="general-btn ">Save Changes</button>
											
					</div>
					<div class="clear"></div>
					<div class="border-top border-blue bggray-white box-shadow-light margin-top-20 padding-all-20">

						<div class="f-left margin-left-30 margin-right-10">
							<div id="upload_photo">
								
							</div>
						</div>
 
						<div class="f-left margin-left-30 margin-right-10">
							<table>
								<tbody>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Item SKU:</p>
										</td>
										<td class="text-left">
											<input type="text" id="itemSku" class="t-small end-width-200px margin-left-10" />
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Item Name:</p>
										</td>
										<td class="text-left">
											<input type="hidden" id="itemID" value="">
											<input type="text" id="itemName" class="t-small end-width-200px margin-left-10"  />
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Item Category</p>
										</td>
										<td class="text-left">
											<div class="select with-modal add-category margin-left-10 width-200">
												<select id="itemCategory" class="">
													<option value="1">Grain</option>
													<option value="2">Computers</option>
													<option value="3">Cars</option>
													<option value="4">Add New Category</option>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Unit of Measure:</p>
										</td>
										<td class="text-left">
											<div class="select with-modal add-category margin-left-10 medium " id="unit-measure-drop-down">
												<select id="unit-of-measure" class="" >
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td colspan="2" class="text-left">
											<p class="font-bold black-color margin-top-10">Quantity Threshold</p>
										</td>
									</tr>
									<tr>
										<td>
											<p class="margin-top-10">Min:</p>
										</td>
										<td class="text-left text-inside-item">
											<div class=" margin-left-10">
												<input type="text" id="minThreshold" class="t-small width-200" />
												<p>Items</p>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<p class="margin-top-10">Max:</p>
										</td>
										<td class="text-left text-inside-item">
											<div class="margin-left-10">
												<input type="text" id="maxThreshold" class="t-small width-200" />
												<p>Items</p>
											</div>
										</td>										
								</tbody>
							</table>											
						</div>

						<div class="f-left margin-left-30">
							<table>
								<tbody>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color margin-top-10">
												Item Shelf Life:
											</p>
										</td>
										<td class="text-inside-item width-200">
											<div class="text-left margin-left-10">
												<input type="text" id="itemShelfLife" class="t-small width-200" />
												<p>Days</p>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color margin-top-10">
												Item Price:
											</p>
										</td>
										<td class="text-inside-item">
											<div class="text-left margin-left-10">
												<input type="text" id="itemPrice" class="t-small width-200 " />
												<p>Php</p>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color margin-top-10">
												Percent Guarantee:
											</p>
										</td>
										<td class="text-inside-item">
											<div class="text-left margin-left-10">
												<input type="text" id="itemGuarantee" class="t-small width-200 " />
												<p>%</p>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color margin-bottom-50">Description:</p>

										</td>
										<td rowspan="2" class="text-left ">
											<textarea id="itemDescription" class="margin-left-10 margin-top-10"></textarea>
										</td>
									</tr>
								</tbody>
							</table>
						</div>	
						<div class="clear"></div>	
					
					</div>
					
					<div class="border-top border-blue bggray-white box-shadow-light margin-top-20 padding-all-20">
						<div class="f-left">
							<p class="font-20 font-bold black-color text-left">Composite Item?</p>
							<p class="text-left margin-top-10 margin-left-5">Click and drag your selected items from the Item List to the Bill
								of Materials on the right.</p>
						</div>

						<div class="f-right margin-right-80 button-activated composite-activated">
							<div class="toggleSwitch yes-no add-composite-switch">
                                <input type="checkbox" id="switch1" name="switch1" class="switch">
                                <label for="switch1">f</label>
                            </div>
						</div>
						<div class="clear"></div>

						<div class="margin-top-10 composite-product text-left composite-add-product">

							<div class="display-inline-top width-45per margin-top-20 margin-right-40">
								<p class="font-15 black-color font-bold text-left">Item List</p>

								<div class="item-container margin-top-20">
									<div class="search-item">
										<input type="text" class="t-large" placeholder="Select Item Here"/>
									</div>


									<div id="composite_product_list_UI" class="item-list ">
										
									</div>

								</div>
							</div>
							<div class="display-inline-top width-45per margin-top-20">
								<p class="font-15 black-color font-bold text-left">Bill of Materials</p>

								<div class="item-container margin-top-20 item-drag">						
									<div class="item-list drag-list">
										
									</div>

								</div>
							</div>
						</div>
					</div>


				</div>
			</div>					
		</section>

		<div class="modal-container add-cat-mdl" modal-id="add-cat">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left ">Add New Category</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content">		
					<div>
						<p class="font-bold black-color font-15 margin-top-10 display-inline-mid">Category Name:</p>
						<input type="text" class="t-small width-200 display-inline-mid" />		
					</div>
					<div>
						<p class="font-bold black-color font-15 display-inline-top margin-top-10">Category Type:</p>
						<div class="display-inline-top margin-left-10">
							<div class="margin-top-10">
								<input type="checkbox" id="isSolid">
								<label for="isSolid" class="default-cursor">Solid</label>
							</div>
							<div class="margin-top-10">
								<input type="checkbox" id="isLiquid">
								<label for="isLiquid" class="default-cursor">Liquid</label>
							</div>
							<div class="margin-top-10">
								<input type="checkbox" id="isGas">
								<label for="isGas" class="default-cursor">Gas</label>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer text-right margin-bottom-10">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid close-me ">Add Category</button>
				</div>
			</div>	
		</div>

		<div class="modal-container composite-product-modal" modal-id="compo-product">
			<div class="modal-body small">				
			
				<div class="modal-head">
					<h4 class="text-left ">Composite Item</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content">	
					<p class="font-15 black-color ">Are you sure you want to proceed? </p>
					<p class="font-15 black-color ">Disabling this will erase all the items assigned to the list.</p>						
				</div>
				<div class="modal-footer text-right margin-bottom-10">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid close-me yes-btn">Confirm</button>
				</div>
			</div>	
		</div>


	<!--	<div class="modal-container composite-product-modal" modal-id="trash-modal">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left ">Delete Information</h4>				
					<div class="modal-close close-me"></div>
				</div>

				
				<div class="modal-content">	
					<p class="font-15 black-color ">Are you sure you want to Delete?</p>
					
				</div>
				<div class="modal-footer text-right margin-bottom-10">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="delete_product" class="font-12 btn btn-primary font-12 display-inline-mid close-me yes-btn">Confirm</button>
				</div>
			</div>	
		</div>-->