
		<div class="main min-width-1024">
			<div class="breadcrumbs no-margin-left padding-left-50">
				<ul>
					<li><a href="product-inventory-1.php">Item Inventory</a></li>
					<li><span>></span></li>
					<li><strong class="black-color">Austrailian Wheat</strong></li>
				</ul>
			</div>
			<div class="semi-main">
				<div class="text-right">					
					<button class="margin-left-5 margin-right-5 display-inline-mid general-btn modal-trigger" modal-target="remove-batches">View Removed Batches</button>
					<button class="margin-left-5 margin-right-5 display-inline-mid general-btn">View Adjustment Records</button>
					<a href="view-product-1.php">
						<button class="margin-left-5 margin-right-5 display-inline-mid general-btn">View Item Activity</button>
					</a>
				</div>
				<div class="clear"></div>

				<div class="border-top border-blue bggray-white box-shadow-light margin-top-20 padding-all-20">
					<img class="img-responsive display-inline-top width-20per margin-top-10" src="../assets/images/australian-wheat.jpg">
 					<table class="tbl-4c3h display-inline-mid width-80per margin-left-10">
 						<tbody class="text-left">
 							<tr>
 								<td colspan="2">
 									<p class="font-20 black-color font-bold">Australian Wheat</p>
 								</td>
 								<td colspan="2" class="text-right width-300 ">
 									<p class="font-20 margin-right-10 black-color font-bold">SKU# 123456790123</p>
 								</td>
 							</tr>
 							<tr>
 								<td class="font-15 font-bold black-color width-250px">
 									Item Category
 								</td>
 								<td class="width-200px">
 									Technological
 								</td>
 								<td class="font-15 font-bold black-color">
 									Unit Price
 								</td>
 								<td class="f-right margin-right-10">
 									30,000 PHP
 								</td>
 							</tr>
 							<tr>
 								<td colspan="2" class="font-15 font-bold black-color">Quality Threshold:</td>
 								<td class="font-15 font-bold black-color">Vendor</td>
 								<td class="f-right margin-right-10">Computerran Inc.</td>
 							<tr>
 								<td class="font-15 font-bold black-color padding-left-30">Minimum</td>
 								<td>800 Items</td>
 								<td colspan="2" class="font-15 black-color font-bold">Item Shelf Life:</td>
 							</tr> 							
 							<tr>
 								<td class="font-15 black-color font-bold padding-left-30">Maximum</td>
 								<td>1200 Items</td>
 								<td class="font-15 black-color font-bold padding-left-30">Per Individual:</td>
 								<td class="f-right margin-right-10">800 Days</td>
 							</tr>
 							<tr>
 								<td colspan="2" class="font-bold font-15 black-color">Description</td>
 								<td class="font-bold font-15 black-color padding-left-30">Per Batch:</td>
 								<td class="f-right margin-right-10">1200 Days</td>
 							</tr>
 							<tr>
 								<td colspan="4" class="padding-left-30">Dinorado Rice</td>
 							</tr>
 						</tbody>
 					</table>
				</div>

				<hr/>

				<div class="padding-all-20  bggray-white box-shadow-light text-left">
					<h2>Inventory List for #12313265465 - "Australian Wheat"</h2>
					<div class="f-right margin-top-20">
						<button type="button" class="general-btn">Transfer Item</button>
						<button type="button" class="general-btn margin-left-10">Withdraw Items</button>						
						<button type="button" class="general-btn margin-left-10 modal-trigger" modal-target="repackage">Repackage Items</button>
						<button type="button" class="general-btn margin-left-10">Remove Items</button>
						<button type="button" class="general-btn margin-left-10">Return Items</button>
					</div>
					<div class="clear"></div>
					<div class="padding-all-10">
						<h3>Pre-Packed</h3>
						<div class="panel-group">
							<div class="border_top border-top-none box-shadow-dark">
								<!-- step 1 -->
								<div class="panel-heading panel-heading border-top border-blue margin-top-20">
									<a class="collapsed active black-color default-cursor font-20" href="#">							
										<i class="fa fa-caret-down margin-right-5"></i>
										Warehouse 1	
									</a>					
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white">
										<table class="tbl-4c3h no-margin-top margin-left-20 text-center">
											<thead>
												<tr>
													<th class="width-20px "><input type="checkbox" name="check-all"/></th>
													<th class="width-100px padding-left-15">Batch No.</th>
													<th class="width-50px">QTY</th>
													<th>Container Location</th>
													<th>Status</th>
													<th>Expiry Date</th>
													<th class="width-300">Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><input type="checkbox" name="chk-palette-1"/></td>
													<td class="padding-left-15">12399</td>
													<td>100</td>
													<td>Palette 1</td>
													<td>Unrestricted</td>
													<td>10/12/2016</td>
													<td><a href="#"><div class="width-100per">Recount</a> | <a href="#" class="modal-trigger" modal-target="split-batch">Split Batch</a> | <a href="#" class="modal-trigger" modal-target="ustatus">Update Status</a></div></td>
												</tr>
												<tr>
													<td><input type="checkbox" name="chk-palette-1"/></td>
													<td class="padding-left-15">12399</td>
													<td>100</td>
													<td>Palette 1</td>
													<td>Unrestricted</td>
													<td>10/12/2016</td>
													<td><a href="#"><div class="width-100per">Recount</a> | <a href="#" class="modal-trigger" modal-target="split-batch">Split Batch</a> | <a href="#" class="modal-trigger" modal-target="ustatus">Update Status</a></div></td>
												</tr>
												<tr>
													<td><input type="checkbox" name="chk-palette-1"/></td>
													<td class="padding-left-15">12399</td>
													<td>100</td>
													<td>Palette 1</td>
													<td>Unrestricted</td>
													<td>10/12/2016</td>
													<td><a href="#"><div class="width-100per">Recount</a> | <a href="#" class="modal-trigger" modal-target="split-batch">Split Batch</a> | <a href="#" class="modal-trigger" modal-target="ustatus">Update Status</a></div></td>
												</tr>
											</tbody>
										</table>														
									</div>
								</div>
							</div>
						</div>
					</div>

					<h2>Item Breakdown of #123465798789 - "Australian Wheat"</h2>
					<div class="padding-all-10">
						<h3>#14856313 - "Piglet Starter"</h3>
						<div class="panel-group">
							<div class="border_top border-top-none box-shadow-dark">
								<!-- step 1 -->
								<div class="panel-heading panel-heading border-top border-blue margin-top-20">
									<a class="collapsed active black-color default-cursor font-20" href="#">							
										<i class="fa fa-caret-down margin-right-5"></i>
										Warehouse 1	
									</a>					
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white">
										<table class="tbl-4c3h no-margin-top margin-left-20 text-center">
											<thead>
												<tr>
													<th class="width-20px "><input type="checkbox" name="check-all"/></th>
													<th class="width-100px padding-left-15">Batch No.</th>
													<th class="width-50px">QTY</th>
													<th>Container Location</th>
													<th>Status</th>
													<th>Expiry Date</th>
													<th class="width-300">Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><input type="checkbox" name="chk-palette-1"/></td>
													<td class="padding-left-15">12399</td>
													<td>100</td>
													<td>Palette 1</td>
													<td>Unrestricted</td>
													<td>10/12/2016</td>
													<td><a href="#"><div class="width-100per">Recount</a> | <a href="#" class="modal-trigger" modal-target="split-batch">Split Batch</a> | <a href="#" class="modal-trigger" modal-target="ustatus">Update Status</a></div></td>
												</tr>
												<tr>
													<td><input type="checkbox" name="chk-palette-1"/></td>
													<td class="padding-left-15">12399</td>
													<td>100</td>
													<td>Palette 1</td>
													<td>Unrestricted</td>
													<td>10/12/2016</td>
													<td><a href="#"><div class="width-100per">Recount</a> | <a href="#" class="modal-trigger" modal-target="split-batch">Split Batch</a> | <a href="#" class="modal-trigger" modal-target="ustatus">Update Status</a></div></td>
												</tr>
												<tr>
													<td><input type="checkbox" name="chk-palette-1"/></td>
													<td class="padding-left-15">12399</td>
													<td>100</td>
													<td>Palette 1</td>
													<td>Unrestricted</td>
													<td>10/12/2016</td>
													<td><a href="#"><div class="width-100per">Recount</a> | <a href="#" class="modal-trigger" modal-target="split-batch">Split Batch</a> | <a href="#" class="modal-trigger" modal-target="ustatus">Update Status</a></div></td>
												</tr>
											</tbody>
										</table>														
									</div>
								</div>
							</div>
							<div class="border_top border-top-none box-shadow-dark">
								<!-- step 1 -->
								<div class="panel-heading panel-heading border-top border-blue margin-top-20">
									<a class="collapsed active black-color default-cursor font-20" href="#">							
										<i class="fa fa-caret-down margin-right-5"></i>
										Warehouse 2	
									</a>					
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white">
										<table class="tbl-4c3h no-margin-top margin-left-20 text-center">
											<thead>
												<tr>
													<th class="width-20px "><input type="checkbox" name="check-all"/></th>
													<th class="width-100px padding-left-15">Batch No.</th>
													<th class="width-50px">QTY</th>
													<th>Container Location</th>
													<th>Status</th>
													<th>Expiry Date</th>
													<th class="width-300">Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><input type="checkbox" name="chk-palette-1"/></td>
													<td class="padding-left-15">12399</td>
													<td>100</td>
													<td>Palette 1</td>
													<td>Unrestricted</td>
													<td>10/12/2016</td>
													<td><a href="#"><div class="width-100per">Recount</a> | <a href="#" class="modal-trigger" modal-target="split-batch">Split Batch</a> | <a href="#" class="modal-trigger" modal-target="ustatus">Update Status</a></div></td>
												</tr>
												<tr>
													<td><input type="checkbox" name="chk-palette-1"/></td>
													<td class="padding-left-15">12399</td>
													<td>100</td>
													<td>Palette 1</td>
													<td>Unrestricted</td>
													<td>10/12/2016</td>
													<td><a href="#"><div class="width-100per">Recount</a> | <a href="#" class="modal-trigger" modal-target="split-batch">Split Batch</a> | <a href="#" class="modal-trigger" modal-target="ustatus">Update Status</a></div></td>
												</tr>
												<tr>
													<td><input type="checkbox" name="chk-palette-1"/></td>
													<td class="padding-left-15">12399</td>
													<td>100</td>
													<td>Palette 1</td>
													<td>Unrestricted</td>
													<td>10/12/2016</td>
													<td><a href="#"><div class="width-100per">Recount</a> | <a href="#" class="modal-trigger" modal-target="split-batch">Split Batch</a> | <a href="#" class="modal-trigger" modal-target="ustatus">Update Status</a></div></td>
												</tr>
											</tbody>
										</table>														
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="padding-all-10">
						<h3>#14856313 - "Hog Grower"</h3>
						<div class="panel-group">
							<div class="border_top border-top-none box-shadow-dark">
								<!-- step 1 -->
								<div class="panel-heading panel-heading border-top border-blue margin-top-20">
									<a class="collapsed active black-color default-cursor font-20" href="#">							
										<i class="fa fa-caret-down margin-right-5"></i>
										Warehouse 1	
									</a>					
								</div>
								<div class="panel-collapse collapse in">
									<div class="panel-body bggray-white">
										<table class="tbl-4c3h no-margin-top margin-left-20 text-center">
											<thead>
												<tr>
													<th class="width-20px "><input type="checkbox" name="check-all"/></th>
													<th class="width-100px padding-left-15">Batch No.</th>
													<th class="width-50px">QTY</th>
													<th>Container Location</th>
													<th>Status</th>
													<th>Expiry Date</th>
													<th class="width-300">Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><input type="checkbox" name="chk-palette-1"/></td>
													<td class="padding-left-15">12399</td>
													<td>100</td>
													<td>Palette 1</td>
													<td>Unrestricted</td>
													<td>10/12/2016</td>
													<td><a href="#"><div class="width-100per">Recount</a> | <a href="#" class="modal-trigger" modal-target="split-batch">Split Batch</a> | <a href="#" class="modal-trigger" modal-target="ustatus">Update Status</a></div></td>
												</tr>
												<tr>
													<td><input type="checkbox" name="chk-palette-1"/></td>
													<td class="padding-left-15">12399</td>
													<td>100</td>
													<td>Palette 1</td>
													<td>Unrestricted</td>
													<td>10/12/2016</td>
													<td><a href="#"><div class="width-100per">Recount</a> | <a href="#" class="modal-trigger" modal-target="split-batch">Split Batch</a> | <a href="#" class="modal-trigger" modal-target="ustatus">Update Status</a></div></td>
												</tr>
												<tr>
													<td><input type="checkbox" name="chk-palette-1"/></td>
													<td class="padding-left-15">12399</td>
													<td>100</td>
													<td>Palette 1</td>
													<td>Unrestricted</td>
													<td>10/12/2016</td>
													<td><a href="#"><div class="width-100per">Recount</a> | <a href="#" class="modal-trigger" modal-target="split-batch">Split Batch</a> | <a href="#" class="modal-trigger" modal-target="ustatus">Update Status</a></div></td>
												</tr>
											</tbody>
										</table>														
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>		
			</div>
		</div>
	</section>
	<!-- modal loss report -->


	<!-- modal loss report -->
	<div class="modal-container" modal-id="repackage">
		<div class="modal-body medium width-800px">				

			<div class="modal-head">
				<h4 class="text-left">Repackage Batches of Australian Wheat</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content">
				<p class="font-15 font-bold black-color">You have selected the following batches to Re-package</p>
				<table class="tbl-4c3h">
					<thead>
						<tr>
							<th class="no-padding-left">Batch No.</th>
							<th class="width-100 no-padding-left">QTY</th>
							<th class="no-padding-left">Location</th>
							<th class="no-padding-left">Expiry Date</th>
							<th class="no-padding-left">Old Container</th>
							<th class="no-padding-left">New Storage Container</th>
							<th class="no-padding-left">QTY</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="text-center">123</td>
							<td class="text-center">10</td>
							<td>Warehouse 1</td>
							<td>10/08/15</td>
							<td>Palette 1</td>
							<td>
								<div class="select width-150px">
									<select>
										<option value="storage1">Select Storage Container</option>
									</select>
								</div>
							</td>
							<td><input type="text" class="t-small width-80" /></td>
						</tr>	
						<tr>
							<td class="text-center">1234</td>
							<td class="text-center">10</td>
							<td>Warehouse 1</td>
							<td>10/08/15</td>
							<td>Rack 1</td>
							<td>
								<div class="select width-150px">
									<select>
										<option value="storage1">Select Storage Container</option>
									</select>
								</div>
							</td>
							<td><input type="text" class="t-small width-80" /></td>
						</tr>
						<tr>
							<td class="text-center">12345</td>
							<td class="text-center">10</td>
							<td>Warehouse 1</td>
							<td>10/08/15</td>
							<td>Bin 1</td>
							<td>
								<div class="select width-150px">
									<select>
										<option value="storage1">Select Storage Container</option>
									</select>
								</div>
							</td>
							<td><input type="text" class="t-small width-80" /></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default display-inline-mid close-me">Cancel</button>
				<button type="button" class="font-12 btn general-btn display-inline-mid">Return Item Batches</button>
			</div>
		</div>
	</div>

	<div class="modal-container" modal-id="ustatus">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Item Inventory</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content">
				<p class="display-inline-mid  font-15 padding-top-10">Update Item Status</p>
				<div class="select display-inline-mid">
					<select>

						<option value="qa">Quality Assurance</option>
						<option value="scap">Scrapping</option>
						<option value="unres">Unrestricted</option>
					</select>
				</div>				
			</div>
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default display-inline-mid close-me">Cancel</button>
				<button type="button" class="font-12 btn general-btn display-inline-mid">Save Changes</button>
			</div>
		</div>
	</div>

	<div class="modal-container" modal-id="remove-batches">
		<div class="modal-body medium">				

			<div class="modal-head">
				<h4 class="text-left">Remove Batches</h4>				
				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content">
				<p class="font-15 font-bold black-color">You have selected the following batches for Renewal:</p>
				<table class="tbl-4c3h">
					<thead>
						<tr>
							<th>Item Name</th>
							<th>Batch No.</th>
							<th>QTY</th>
							<th>Storage Location</th>
							<th>Storage Container</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Dinorado Rice</td>
							<td>123</td>
							<td>100</td>
							<td>Warehouse 1</td>
							<td>Palette 1</td>
							<td>Unrestricted</td>
						</tr>
					</tbody>
				</table>
				<p class="font-15 font-bold black-color">Reason for Removal:</p>
				<textarea rows="50"></textarea>
			</div>
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default display-inline-mid close-me">Cancel</button>
				<button type="button" class="font-12 btn general-btn display-inline-mid">Remove Items</button>
			</div>
		</div>
	</div>

	<!-- remove batches -->
	<div class="modal-container" modal-id="split-batch">
		<div class="modal-body medium">				

			<div class="modal-head">
				<h4 class="text-left">Split Batches of Australian Wheat</h4>				
				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content">
				<p class="font-15 font-bold black-color">You have selected the following batch for Splitting:</p>
				<table class="tbl-4c3h">
					<thead>
						<tr>
							<th class="text-left">Batch No.</th>
							<th class="text-left">QTY</th>							
							<th class="text-left">Storage Location</th>
							<th class="text-left">Storage Container</th>
							<th class="text-left">Status</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>123</td>
							<td>100</td>							
							<td>Warehouse 1</td>
							<td>Palette 1</td>
							<td>Unrestricted</td>
						</tr>
					</tbody>
				</table>
				<p class="font-15 font-bold black-color">Batch Split:</p>
				<table class="tbl-4c3h">
					<thead>
						<tr>
							<th class="text-left">Batch No.</th>
							<th class="text-left">QTY</th>							
							<th class="text-left">Storage Location</th>
							<th class="text-left">Storage Container</th>
							<th class="text-left">Status</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="text" class="t-small width-80" placeholder="123" /></td>
							<td><input type="text" class="t-small width-80" placeholder="100" /></td>
							<td>
								<div class="select width-100">
									<select>
										<option value="op1">WareHouse 1</option>
									</select>
								</div>
							</td>
							<td>
								<div class="select width-100">
									<select>
										<option value="opA">Palette 1</option>
									</select>
								</div>
							</td>
							<td>
								<div class="select width-100">
									<select>
										<option value="opB">Unrestricted</option>
									</select>
								</div>
							</td>							
						</tr>
					</tbody>
				</table>
				<a href="#" class="f-right font-15 font-bold">+ Add Another Batch</a>
				<div class="clear"></div>
							
			</div>
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default display-inline-mid close-me">Cancel</button>
				<button type="button" class="font-12 btn general-btn display-inline-mid">Split Item Batch</button>
			</div>
		</div>
	</div>