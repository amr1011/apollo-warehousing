<div class="main min-width-1024">
				<div class="breadcrumbs no-margin-left padding-left-50">
					<ul>
						<li><a href="<?php echo BASEURL . "products/product_management" ?>" class="font-15 black-color font-400">Item Management</a></li>
						
						<li><span>></span></li>
						<li class="font-bold black-color">Add Item</li>

					</ul>
				</div>
				<div class="semi-main" id="add_product_form">
					<div class="f-right width-220">
						<a href="<?php echo BASEURL . "products/product_management" ?>">
							<button type="button" class="btn-default ">Cancel</button>
						</a>
						<button id="submit_add_product" type="button" class="general-btn ">Add Item</button>						
					</div>
					<div class="clear"></div>
					<div class="border-top border-blue bggray-white box-shadow-light margin-top-20 padding-all-20">

						<div class="f-left margin-left-30 margin-right-10">
							<div id="upload_photo">
								
							</div>
						</div>
 						
						<form id="addProductForm">

						<div class="f-left margin-left-30 margin-right-10">
							<table>
								<tbody>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Item SKU:</p>
										</td>
										<td class="text-left">
											<input type="text" id="itemSku" class="t-small  end-width-200px margin-left-10" disabled />
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Item Name:</p>
										</td>
										<td class="text-left">
											<input type="text" datavalid="required" labelinput="Item Name" id="itemName" class="t-small end-width-200px margin-left-10"  />
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Item Category:</p>
										</td>
										<td class="text-left">
											<div class="select with-modal add-category margin-left-10 medium">
												<select id="itemCategory" class="" datavalid="required numeric" labelinput="Item Category">
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color">Unit of Measure:</p>
										</td>
										<td class="text-left">
											<div class="select with-modal add-category margin-left-10 medium " id="unit-measure-drop-down">
												<select id="unit-of-measure" class="" >
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td colspan="2" class="text-left">
											<p class="font-bold black-color margin-top-10">Quantity Threshold</p>
										</td>
									</tr>
									<tr>
										<td>
											<p class="margin-top-10">Min:</p>
										</td>
										<td class="text-left text-inside-item">
											<div class=" margin-left-10">
												<input id="minThreshold" datavalid="required" labelinput="Min Threshold" type="text" class="t-small text-right" />
												<p>Items</p>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<p class="margin-top-10 padding-left-3">Max:</p>
										</td>
										<td class="text-left text-inside-item">
											<div class="margin-left-10">
												<input type="text" id="maxThreshold" datavalid="required" labelinput="Max Threshold" class="t-small width-200 text-right" />
												<p>Items</p>
											</div>
										</td>										
								</tbody>
							</table>											
						</div>

						<div class="f-left margin-left-30">
							<table>
								<tbody>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color margin-top-10">
												Item Shelf Life:
											</p>
										</td>
										<td class="text-inside-item width-200">
											<div class="text-left margin-left-10">
												<input type="text" id="itemShelfLife" datavalid="required numeric" labelinput="Item Shelf Life" class="t-small width-200 text-right" />
												<p>Days</p>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color margin-top-10">
												Item Price:
											</p>
										</td>
										<td class="text-inside-item">
											<div class="text-left margin-left-10">
												<input type="text" id="itemPrice" datavalid="required" labelinput="Price" class="t-small width-200 text-right" />
												<p>Php</p>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color margin-top-10">
												Percent Guarantee:
											</p>
										</td>
										<td class="text-inside-item">
											<div class="text-left margin-left-10">
												<input type="text" id="itemGuarantee" datavalid="required" labelinput="Guarantee" class="t-small width-200 text-right" />
												<p>%</p>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<p class="font-bold black-color margin-bottom-75">Description:</p>

										</td>
										<td rowspan="2" class="text-left ">
											<textarea id="itemDescription" datavalid="alpha_numberic" labelinput="Description" class="margin-left-10 margin-top-10"></textarea>
										</td>
									</tr>
								</tbody>
							</table>
						</div>

						</form>

						<div class="clear"></div>	
					
					</div>
					
					<div class="border-top border-blue bggray-white box-shadow-light margin-top-20 padding-all-20">
						<div class="f-left">
							<p class="font-20 font-bold black-color text-left">Composite Item?</p>
							<p class="text-left margin-top-10 margin-left-5 remove-composite add-product-content">Click and drag your selected items from the Item List to the Bill of Materials on the right.</p>
						</div>

						<div class="f-right margin-right-80 button-activated composite-activated">
							<div class="toggleSwitch yes-no add-composite-switch ">
                                <input type="checkbox" id="switch1" name="switch1" class="switch" >
                                <label for="switch1">f</label>
                            </div>
						</div>
						<div class="clear"></div>

						<div class="margin-top-10 composite-product text-left composite-add-product add-product-result">

							<div class="display-inline-top width-45per margin-top-20 margin-right-40">
								<p class="font-15 black-color font-bold text-left">Item List</p>

								<div class="item-container margin-top-20">
									<div class="search-item">
										<input type="text" class="t-large" placeholder="Select Item Here"/>
									</div>


									<div id="composite_product_list_UI" class="item-list ">
										

									</div>

								</div>
							</div>
							<div class="display-inline-top width-45per margin-top-20">
								<p class="font-15 black-color font-bold text-left">Bill of Materials</p>

								<div class="item-container margin-top-20 item-drag">	

									<div class="item-list drag-list ">
										<div class="drag-item">
											<p class="font-15 font-400 black-color">Please Drag items from the item list</p>
										</div>									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>					
		</section>

		<div class="modal-container composite-product-modal" modal-id="compo-product">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left ">Composite Item</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content">	
					<p class="font-15 black-color ">Are you sure you want to proceed? </p>
					<p class="font-15 black-color ">Disabling this will erase all the items assigned to the list.</p>						
				</div>
				<div class="modal-footer text-right margin-bottom-10">
					<button id="cancel_close_composite_product" type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button id="confirm_close_composite_product" type="button" class="font-12 btn btn-primary font-12 display-inline-mid close-me yes-btn">Confirm</button>
				</div>
			</div>	
		</div>

		<!--start modal category -->
		<div class="modal-container" modal-id="add-new-category">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Add New Category</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<form id="formCategory">
						<p class="display-inline-mid no-margin-all font-300 font-14">Category Name :</p>
						<input type="text" datavalid="required" labelinput="Category Name" class="display-inline-mid width-280px" id="categoryName">

						<div class="width-100per  padding-top-20">
							<p class="display-inline-bottom no-margin-all font-14 display-inline-top font-300">Category Type :</p>

							<div class="display-inline-mid padding-left-15" id="modalAddNewCategoryTypeContainer">
								<div class="padding-bottom-10 ">
									<input type="checkbox" name="category_type[]" value="1"  id="solid" class="default-cursor">
									<label for ="solid" class="default-cursor font-300 font-14" >Solid</label>
								</div>
								<div class="padding-bottom-10 default-cursor">
									<input type="checkbox" name="category_type[]" value="2" class="default-cursor" id="liquid">
									<label for ="liquid" class="default-cursor font-300 font-14" >Liquid</label>
								</div>
								<div class="padding-bottom-10 default-cursor">
									<input type="checkbox" name="category_type[]" value="3" class="default-cursor" id="gas">
									<label for ="gas" class="default-cursor font-300 font-14" >Gas</label>
								</div>
							</div>

						</div>
					</form>	
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="saveCategory" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Add Category</button>
				</div>
			</div>	
		</div>
		<!--end of modal category-->
