<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receiving_model extends CI_Model {

	//==================================================//
	//=================COMPANY RECEIVING================//
	//==================================================//
	
	public function add_receiving_log_info($receiving_details)
	{
		$this->db->insert('receiving_log', $receiving_details);
		return $this->db->insert_id();
	}

	public function edit($query, $update_data)
	{	

		$this->db->where($query);
		$this->db->update("receiving_log", $update_data);
		return $this->db->affected_rows();
	}

	public function add_receiving_consignee_map_info($data)
	{
		$this->db->insert('receiving_consignee_map', $data);
		if($this->db->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function add_product_inventory_qty($data)
	{
		$this->db->insert('product_inventory_qty', $data);
		if($this->db->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function update_product_inventory_qty($id, $data)
	{
		$this->db->where("id", $id);
		$this->db->update('product_inventory_qty', $data);
	}

	//==================================================//
	//=============END OF COMPANY RECEIVING=============//
	//==================================================//


	public function get_receiving_records($select = "*", $where = "", $sort_field = "id", $sort = "ASC", $offset = "", $limit = "")
	{
		$sql = "SELECT {$select} FROM receiving_log r LEFT JOIN receiving_consignee_map rc ON r.id = rc.receiving_log_id LEFT JOIN product p ON rc.product_id = p.id {$where} ORDER BY {$sort_field} {$sort} {$limit} {$offset}";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_company_goods() {
		$sql = "SELECT *
		FROM receiving_log WHERE type = 1 ORDER BY date_created DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function search_company_goods() {
		$sql = "SELECT rl.id,rl.purchase_order,rl.receiving_number,rl.date_received,rl.received_by,rl.invoice_id,rl.officer_id,
						rn.receiving_log_id,rn.note, rn.user_id,rn.date_created
			    FROM receiving_log rl
				LEFT JOIN receiving_notes rn ON  rn.receiving_log_id = rl.id
			    WHERE rl.type = 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_single_company_goods($where="") {
		$sql = "SELECT *
		FROM receiving_log {$where} ORDER BY date_created DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_latest_receiving_number($type){
		$query = "SELECT * FROM receiving_log WHERE type = {$type} ORDER BY id DESC LIMIT 1";
		$sql = $this->db->query($query);
		return $sql->result_array();
	}

	// ==========================  CONSIGNEE GOODS =============================//

	public function get_all_products()
	{
		$sql = "SELECT * FROM product WHERE is_deleted=0 LIMIT 30";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function add_consignee_receiving_log($set_receiving_log_data)
	{
		$this->db->insert('receiving_log', $set_receiving_log_data);
		return $this->db->insert_id();
	}

	public function update_consignee_receiving_log($receiving_id, $set_receiving_log_data)
	{
		$this->db->where("id", $receiving_id);
		$this->db->update('receiving_log', $set_receiving_log_data);
		return $this->db->affected_rows();
	}

	public function add_receiving_vessel_log($create_vessels_log)
	{
		$this->db->insert('vessel_receiving_log', $create_vessels_log);
		return $this->db->insert_id();
	}

	public function add_receiving_truck_log($create_truck_log)
	{
		$this->db->insert('truck_receiving_log', $create_truck_log);
		return $this->db->insert_id();
	}

	public function save_receiving_consignee_map($create_consignee_map)
	{
		$this->db->insert('receiving_consignee_map', $create_consignee_map);
		return $this->db->insert_id();
	}

	public function display_all_receiving_consignee_goods($id)
	{
		$sql = "SELECT r.id, r.receiving_number, r.purchase_order, r.date_created, r.status, vrl.berthing_time FROM receiving_log r
				LEFT JOIN vessel_receiving_log vrl ON vrl.receiving_log=r.id
				WHERE received_by='{$id}' AND type=2
				ORDER BY receiving_number DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function check_if_vessel($receiving_id)
	{
		$sql = "SELECT id FROM vessel_receiving_log WHERE receiving_log='{$receiving_id}'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	public function check_if_truck($receiving_id)
	{
		$sql = "SELECT id FROM truck_receiving_log WHERE receiving_log_id='{$receiving_id}'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	public function get_all_products_data($receiving_id)
	{
		$sql = "SELECT p.name, p.sku FROM product_inventory_qty piq
				LEFT JOIN product p ON p.id = piq.product_id
				WHERE piq.product_receiving_id='{$receiving_id}' AND piq.is_deleted =0
				GROUP BY piq.product_id ORDER BY piq.id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function get_receiving_log($where = ""){
		$sql = "SELECT * FROM receiving_log {$where}";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_receiving_log_complete($receiving_id)
	{
		$sql = "SELECT *, v.name as vessel_name, r.id as receiving_id, r.date_created as date_created, uom.name as bill_measure FROM receiving_log r
				LEFT JOIN vessel_receiving_log vl ON vl.receiving_log=r.id
				LEFT JOIN vessel_list v ON v.id=vl.origin
				LEFT JOIN unit_of_measures uom ON uom.id=vl.unit_of_measure_id
				WHERE r.id='{$receiving_id}' AND r.type = 2 AND r.status=1";
		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function get_selected_current_receiving_log($receiving_id)
	{
		$sql = "SELECT *, v.name as vessel_name, r.id as receiving_id, r.date_created as date_created, uom.name as bill_measure FROM receiving_log r
				LEFT JOIN vessel_receiving_log vl ON vl.receiving_log=r.id
				LEFT JOIN vessel_list v ON v.id=vl.origin
				LEFT JOIN unit_of_measures uom ON uom.id=vl.unit_of_measure_id
				WHERE r.id='{$receiving_id}' AND r.type = 2 AND (r.status=0 OR r.status=2)";
		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function get_selected_current_receiving_log_1($receiving_number)
	{
		$sql = "SELECT *, v.name as vessel_name, r.id as receiving_id, r.date_created as date_created FROM receiving_log r
				LEFT JOIN vessel_receiving_log vl ON vl.receiving_log=r.id
				LEFT JOIN vessel_list v ON v.id=vl.origin
				WHERE r.id='{$receiving_id}' AND r.type = 2 AND (r.status=0 OR r.status=2)";
		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function get_receiving_id($receiving_number)
	{
		$sql = "SELECT id FROM receiving_log WHERE receiving_number='{$receiving_number}' AND type=2 ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_selected_current_receiving_log_truck($receiving_id)
	{
		$sql = "SELECT *, v.name as vessel_name, r.id as receiving_id, r.date_created as date_created FROM receiving_log r
				LEFT JOIN truck_receiving_log vl ON vl.receiving_log_id=r.id
				LEFT JOIN vessel_list v ON v.id=vl.origin
				WHERE r.id='{$receiving_id}' AND r.type = 2 AND (r.status=0 OR r.status=2)";
		$query = $this->db->query($sql);
		return $query->result_array();

	}
	
	public function get_complete_receiving_log_truck($receiving_id)
	{
		$sql = "SELECT *, v.name as vessel_name, r.id as receiving_id, r.date_created as date_created FROM receiving_log r
				LEFT JOIN truck_receiving_log vl ON vl.receiving_log_id=r.id
				LEFT JOIN vessel_list v ON v.id=vl.origin
				WHERE r.id='{$receiving_id}' AND r.type = 2 AND r.status=1";
		$query = $this->db->query($sql);
		return $query->result_array();

	}
	

	public function get_selected_product_inventory($receiving_id)
	{
		$sql = "SELECT p.id as product_id, p.name, p.sku, p.image, piq.id as product_inventory_id ,piq.unit_of_measure_id, piq.qty, piq.loading_method, piq.bag, v.name as vendor_name, uof.name as measures_name,
				uof.id as unit_of_measure_id FROM product_inventory_qty piq
				LEFT JOIN product p ON p.id = piq.product_id
				LEFT JOIN vendor v ON v.id=piq.vendor_id
				LEFT JOIN unit_of_measures uof ON uof.id=piq.unit_of_measure_id
				WHERE piq.product_receiving_id='{$receiving_id}' AND piq.is_deleted =0
				GROUP BY piq.product_id ORDER BY piq.id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_selected_consignee_map($product_id, $receiving_id)
	{
		$sql = "SELECT rcm.distribution_qty, rcm.bag, rcm.consignee_id, rcm.unit_of_measure_id, c.name, uof.name as measures_name FROM receiving_consignee_map rcm
				LEFT JOIN consignee c ON c.id=rcm.consignee_id
				LEFT JOIN unit_of_measures uof ON uof.id=rcm.unit_of_measure_id
				WHERE rcm.receiving_log_id='{$receiving_id}' AND rcm.product_id='{$product_id}'";

		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function get_select_receiving_notes($receiving_id)
	{
		$sql = "SELECT subject, note, date_created FROM receiving_notes
				WHERE receiving_log_id='{$receiving_id}' AND is_deleted=0 ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function delete_receiving_vessel_log($receiving_id)
	{
		$this->db->where("receiving_log", $receiving_id);
		$this->db->delete("vessel_receiving_log");
	}

	public function delete_receiving_truck_log($receiving_id)
	{
		$this->db->where("receiving_log_id", $receiving_id);
		$this->db->delete("truck_receiving_log");
	}

	public function delete_receiving_consignee_map($receiving_id)
	{
		$this->db->where("receiving_log_id", $receiving_id);
		$this->db->delete("receiving_consignee_map");
	}

	public function receiving_consignee_complete_vessel_log($receiving_id, $save_data)
	{
		$this->db->where("receiving_log", $receiving_id);
		$this->db->update("vessel_receiving_log", $save_data);
		return $this->db->affected_rows();
	}

	public function receiving_consignee_complete_receiving_log($receiving_id, $date_received)
	{
		$sql = "UPDATE receiving_log SET status=1, date_received='{$date_received}' WHERE id='{$receiving_id}'";
		$query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function  remove_product_consignee_map($receiving_id, $product_id)
	{
		$sql = "DELETE FROM receiving_consignee_map WHERE receiving_log_id='{$receiving_id}' AND product_id='{$product_id}' ";
		$query = $this->db->query($sql);
	}

	public function add_product_consignee_map($set_add_product_consignee_map)
	{
		$query = $this->db->insert("receiving_consignee_map", $set_add_product_consignee_map);
		return $this->db->insert_id();
	}

	public function update_product_consignee_map($receiving_id, $product_id, $consignee_id, $set_update_product_consignee_map)
	{
		$where = "receiving_log_id='{$receiving_id}' AND product_id='{$product_id}' AND consignee_id='{$consignee_id}'";
		$this->db->where($where);
		$query = $this->db->update("receiving_consignee_map", $set_update_product_consignee_map);
		return $this->db->affected_rows();
	}

	public function update_receiving_consignee_product_item($product_inventory_id, $set_consignee_product_inventry)
	{
		$this->db->where("id", $product_inventory_id);
		$query = $this->db->update("product_inventory_qty", $set_consignee_product_inventry);
		return $this->db->affected_rows();
	}

	public function check_discrepancy($prod_inv_qty_id)
	{
		$sql = "SELECT product_inventory_qty_id FROM discrepancy WHERE product_inventory_qty_id='{$prod_inv_qty_id}' ";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	public function update_discrepancy_record($prod_inv_qty_id, $discrepancy_data)
	{
		$this->db->where("product_inventory_qty_id", $prod_inv_qty_id);
		$this->db->update("discrepancy", $discrepancy_data);
	}

	public function save_consignee_ongoing_truck_tare($receiving_id, $data)
	{
		$this->db->where("receiving_log_id", $receiving_id);
		$this->db->update("truck_receiving_log", $data);
		return $this->db->affected_rows();
	}

	public function update_receiving_logs_for_truck($receiving_id, $update_truck)
	{
		$this->db->where("id", $receiving_id);
		$this->db->update("receiving_log", $update_truck);
	}

	public function change_status_receiving_log($receiving_id)
	{
		$sql = "UPDATE receiving_log SET status=2 WHERE id='{$receiving_id}' AND type=2 ";
		$this->db->query($sql);
	}

	public function check_receiving_vessel_log($receiving_id)
	{
		$sql = "SELECT receiving_log FROM vessel_receiving_log WHERE receiving_log='{$receiving_id}' ";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	public function update_receiving_vessel_log($receiving_id, $update_vessels_log)
	{
		$this->db->where("receiving_log", $receiving_id);
		$this->db->update("vessel_receiving_log", $update_vessels_log);
	}

	public function check_receiving_truck_log($receiving_id)
	{
		$sql = "SELECT receiving_log_id FROM truck_receiving_log WHERE receiving_log_id='{$receiving_id}' ";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	public function update_receiving_truck_log($receiving_id, $update_truck_log)
	{
		$this->db->where("receiving_log_id", $receiving_id);
		$this->db->update("truck_receiving_log", $update_truck_log);
	}

	public function remove_product_inventory($product_inventory_id)
	{
		$sql = "UPDATE product_inventory_qty SET is_deleted=1 WHERE id='{$product_inventory_id}' ";
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function check_receiving_item($receiving_id, $product_id)
	{
		$sql = "SELECT id FROM product_inventory_qty WHERE product_receiving_id='{$receiving_id}' AND product_id='{$product_id}' AND is_deleted=0 ";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	public function update_receiving_item($receiving_id, $roduct_id, $update_consignee_product_inventry)
	{
		$where = "product_receiving_id='{$receiving_id}' AND product_id='{$product_id}' ";
		$this->db->where($where);
		$this->db->update("product_inventory_qty", $update_consignee_product_inventry);
	}

	public function collect_for_inventory($receiving_id)
	{
		$sql = "SELECT piq.product_id, d.received_quantity, piq.unit_of_measure_id FROM product_inventory_qty piq
				LEFT JOIN discrepancy d ON d.product_inventory_qty_id=piq.id
				WHERE piq.product_receiving_id='{$receiving_id}' AND piq.is_deleted=0 AND d.is_deleted=0 ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function save_to_product_inventory($data_intv)
	{
		$this->db->insert("product_inventory", $data_intv);
		return $this->db->insert_id();
	}


	// ==========================  END CONSIGNEE GOODS =============================//

	public function add_notes($data){
		$this->db->insert('receiving_notes', $data);
		return $this->db->insert_id();
	}

	public function add_courier($data){
		$this->db->insert('courier', $data);
		return $this->db->insert_id();
	}

	public function add_invoice($data){
		$this->db->insert('invoice', $data);
		return $this->db->insert_id();
	}

	public function update_invoice($id, $data){
		$this->db->where("id", $id);
		$this->db->update('invoice', $data);
	}

	public function add_officer($data){
		$this->db->insert('officer', $data);
		return $this->db->insert_id();
	}

	public function add_vendor($data){
		$this->db->insert('vendor', $data);
		return $this->db->insert_id();
	}

	public function get_vendor($where = ""){
		$query = "SELECT * FROM vendor WHERE name='{$where}' ";
		$sql = $this->db->query($query);
		return $sql->result_array();
	}

	public function add_receiving_item($data)
	{
		$this->db->insert('product_inventory_qty', $data);
		return $this->db->insert_id();
	}

	public function delete_receiving_item($receiving_id)
	{
		$sql = "DELETE FROM product_inventory_qty WHERE product_receiving_id='{$receiving_id}' ";
		$query = $this->db->query($sql);
	}

	public function get_receiving_company_goods_items($receiving_log_id = 0, $and_where = "")
	{
		$sql = "SELECT 
					piq.id as id, piq.qty as quantity, piq.product_id as product_id, piq.loading_method as loading_method, piq.vendor_id as vendor_id, piq.unit_of_measure_id as unit_of_measure_id,
					p.name as product_name, p.image as product_image, p.sku as product_sku, p.product_category as product_category_id, p.description as product_description, p.product_shelf_life as product_shelf_life,
					p.threshold_maximum as product_threshold_maximum, p.threshold_min as product_threshold_min, p.unit_price as product_unit_price, p.is_active as product_is_active,
					uom.name as product_unit_of_measure, v.name as vendor_name, c.name as product_category_name
			 	FROM product_inventory_qty piq
				LEFT JOIN product p ON piq.product_id = p.id
				LEFT JOIN category c ON p.product_category = c.id
				LEFT JOIN unit_of_measures uom ON piq.unit_of_measure_id = uom.id
				LEFT JOIN vendor v ON piq.vendor_id = v.id
				WHERE piq.product_receiving_id = {$receiving_log_id} {$and_where}
			 	";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_notes($where){
		$sql = "SELECT * FROM receiving_notes ".$where;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_invoice($where){
		$sql = "SELECT * FROM invoice ".$where;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_officer($where){
		$sql = "SELECT * FROM officer ".$where;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_courier($where)
	{
		$sql = "SELECT * FROM courier ".$where;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function add_document($data)
	{
		$this->db->insert("inventory_documents", $data);
		return $this->db->insert_id();
	}

	public function get_documents($receiving_id)
	{
		$sql = "SELECT * FROM inventory_documents WHERE receiving_log_id = ".$receiving_id;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_storage_assignment($where)
	{
		$sql = "SELECT * FROM storage_assignment ".$where;
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function check_storage_assignment($receiving_id, $prod_inv_qty_id)
	{
		$sql = "SELECT id FROM storage_assignment WHERE receiving_log_id='{$receiving_id}' AND product_inventory_qty_id='{$prod_inv_qty_id}'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	public function create_storage_assignment($data){
		$this->db->insert("storage_assignment", $data);
		return $this->db->insert_id();
	}

	public function update_storage_assignment($receiving_id, $prod_inv_qty_id, $store_assign_data)
	{
		$where = " receiving_log_id='{$receiving_id}' AND product_inventory_qty_id ='{$prod_inv_qty_id}' ";
		$this->db->where($where);
		$this->db->update("storage_assignment", $store_assign_data);
	}

	public function get_storage_assignment_id($receiving_id, $prod_inv_qty_id)
	{
		$sql = "SELECT id FROM storage_assignment WHERE receiving_log_id='{$receiving_id}' AND product_inventory_qty_id='{$prod_inv_qty_id}' AND is_deleted = 0 LIMIT 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function delete_batch($store_assign_id)
	{
		$this->db->where("storage_assignment_id", $store_assign_id);
		$this->db->update("batch", array("is_deleted" => 1));
	}

	public function get_batch($where){
		$sql = "SELECT * FROM batch ".$where;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function create_batch($data){
		$this->db->insert("batch", $data);
		return $this->db->insert_id();
	}

	public function create_product_inventory($data){
		$this->db->insert("product_inventory", $data);
		return $this->db->insert_id();
	}

	public function create_discrepancy_record($data)
	{
		$this->db->insert("discrepancy", $data);
		return $this->db->insert_id();
	}

	public function get_discrepancy($where)
	{
		$sql = "SELECT * FROM discrepancy ".$where;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	// $query = "SELECT *, 
	// 			(SELECT sum(quantity) FROM temp_cart WHERE temp_cart.shirt_size='small') as total_small_quantity,
	// 			(SELECT sum(quantity) FROM temp_cart WHERE temp_cart.shirt_size='medium') as total_medium_quantity,
	// 			(SELECT sum(quantity) FROM temp_cart WHERE temp_cart.shirt_size='large') as total_large_quantity,
	// 			FROM temp_cart
	// 			LEFT JOIN shirts ON shirts.shirt_id = temp_cart.shirt_id
	// 			WHERE temp_cart.temp_id = '$user_id'
	// 			GROUP BY temp_cart.shirt_id
	// 			ORDER BY temp_cart.temp_id DESC
	// 			";
	// $result = $this->db->query($query);

}

/* End of file Receiving_model.php */
/* Location: ./application/modules/receiving/models/Receiving_model.php */