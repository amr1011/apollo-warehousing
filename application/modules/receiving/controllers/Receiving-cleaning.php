<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receiving extends MX_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->library('MY_Form_validation');
        $this->form_validation->CI =& $this;
		$this->load->model('receiving_model');
		$this->load->model('products/products_model');
		$response = array(
			"status" => false,
			"message" => "message here",
			"data" => null
		);
	}

	//==================================================//
	//==================RECEIVING VIEWS=================//
	//==================================================//

	public function generate_receiving_number($type = 0)
	{
		$length = 10;
		$zero = 0;
		$return = "0";
		$receiving_num = "0";
		if($type > 0){
			$receiving_record = $this->receiving_model->get_latest_receiving_number($type);
			if(count($receiving_record) > 0){	
				$receiving_num = $receiving_record[0]["receiving_number"];
			}
			$len = strlen($receiving_num);
			

			$int_val = intval($receiving_num);
			$return_data = $int_val + 1;

			$len = $length - $len;

			for($i = 1; $i < $len; $i++){
				$return = "0".$return;
			}

			$return = $return.$return_data;

			return $return;
		}
	}

	public function transform_to_receiving_number($receiving_num = 1)
	{
		$length = 10;
		$return = "0";
		$len = strlen($receiving_num);
		$int_val = intval($receiving_num);
		$return_data = $int_val;

		$len = $length - $len;

		for($i = 1; $i < $len; $i++){
			$return = "0".$return;
		}

		$return = $return.$return_data;

		return $return;
	}

	public function index(){
		$this->company_goods();	
	}

	public function company_goods()
	{
		$data['title'] = 'RECEIVING - COMPANY GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("stock_receiving_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function stock_view_edit() {
		$data['title'] = 'RECEIVING - COMPANY GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("stock_view_edit", $data);
		$this->load->view("includes/footer.php");
	}

	public function consignee_receiving()
	{
		$data['title'] = 'RECEIVING - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_receiving_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consignee_create()
	{
		$data['title'] = 'RECEIVING - CONSIGNEE GOODS';
		$data['receiving_number'] = $this->generate_receiving_number(2);
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_create_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consignee_edit_vessel()
	{
		$data['title'] = 'RECEIVING - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_edit_vessel_view", $data);
		$this->load->view("includes/footer.php");
	}
	public function consignee_complete_vessel()
	{
		$data['title'] = 'RECEIVING - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_complete_vessel_view", $data);
		$this->load->view("includes/footer.php");
	}

	
	public function consignee_ongoing()
	{
		$data['title'] = 'RECEIVING - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_ongoing_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consignee_ongoing_vessel()
	{
		$data['title'] = 'RECEIVING - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_ongoing_vessel_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consignee_ongoing_vessel_1()
	{
		$data['title'] = 'RECEIVING - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_ongoing_vessel_1_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consignee_ongoing_truck()
	{
		$data['title'] = 'RECEIVING - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_ongoing_truck_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consignee_ongoing_truck_1()
	{
		$data['title'] = 'RECEIVING - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_ongoing_truck_1_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consignee_edit_truck()
	{
		$data['title'] = 'RECEIVING - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_edit_truck_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function consignee_complete_truck()
	{
		$data['title'] = 'RECEIVING - CONSIGNEE GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_complete_truck_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function view_company_goods() {
		$data['title'] = 'RECEIVING - COMPANY GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("stock_view_ongoing", $data);
		$this->load->view("includes/footer.php");
	}

	public function stock_view_complete() {
		$data['title'] = 'RECEIVING - COMPANY GOODS';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("stock_view_complete", $data);
		$this->load->view("includes/footer.php");
	}

	public function stock_create() {
		$data['title'] = 'RECEIVING - COMPANY GOODS';
		$data['receiving_number'] = $this->generate_receiving_number(1);
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("stock_create", $data);
		$this->load->view("includes/footer.php");
	}

	public function add_notes_to_receiving()
	{
		$notes = json_decode($this->input->post("notes"), true);
		$receiving_id = $this->input->post("receiving_id");

		$response = array(
			"status" => false,
			"message" => "",
			"data" => array()
		);

		$this->form_validation->set_rules('receiving_id', 'Receiving id', 'trim|required');
		$this->form_validation->set_rules('notes', 'Notes', 'trim');
		if ($this->form_validation->run()) {
			$this->save_receiving_notes($notes, $receiving_id);
			$response["status"];
		}else{
			$response["message"] = $this->form_validation->error_array();
		}

		header("Content-Type: application/json");
		echo json_encode($response);
	}

	public function save_receiving_notes($notes, $receiving_id)
	{
		$user_data = $this->session->userdata('apollo_user');
		foreach ($notes as $key => $value) {
			$data = array(
				"receiving_log_id" => $receiving_id,
				"note" => $value["message"],
				"subject" => $value["subject"],
				"user_id" => $user_data["id"],
				"date_created" => $value["datetime"],
				"is_deleted" => 0
			);
			$this->receiving_model->add_notes($data);
		}
	}

	public function save_receiving_items($items, $receiving_id)
	{
		$user_data = $this->session->userdata('apollo_user');
		$this->load->model('Products/products_model');
		$measure_piece_data = $this->products_model->get_unit_of_measure(" WHERE name = 'pc' ");

		foreach ($items as $key => $value) {
			$vendor_name = $value["item_vendor_name"];
			$vendor = $this->receiving_model->get_vendor(strtolower($vendor_name));
			$vendor_id = 0;
			if(count($vendor) > 0){
				$vendor_id = $vendor[0]["id"];
			}else{
				$vendor_id = $this->receiving_model->add_vendor(array(
					"name" => strtolower($vendor_name)
				));
			}

			if($value["item_loading_method"] == "piece")
			{
				$data_piece = array(
					"qty" => $value["item_qty_by_piece"],
					"product_receiving_id" => $receiving_id,
					"product_id" => $value["item_id"],
					"loading_method" => $value["item_loading_method"],
					"vendor_id" => $vendor_id,
					"unit_of_measure_id" => $measure_piece_data[0]["id"]
				);
				$this->receiving_model->add_receiving_item($data_piece);
			}
			else if($value["item_loading_method"] == "bulk")
			{
				$data_measure = array(
					"qty" => $value["item_qty_unit_of_measure"],
					"product_receiving_id" => $receiving_id,
					"product_id" => $value["item_id"],
					"loading_method" => $value["item_loading_method"],
					"vendor_id" => $vendor_id,
					"unit_of_measure_id" => $value["item_unit_of_measure"]
				);

				$this->receiving_model->add_receiving_item($data_measure);
			}
		}
	}

	public function add_company_receiving() {
		$company_receiving_data = array(
			"receiving_number" => $this->input->post('receiving_number'),
			"po_number" => $this->input->post('po_number'),
			"officer" => $this->input->post('officer'),
			"courier" => $this->input->post('courier'),
			"invoice_number" => $this->input->post('invoice_number'),
			"invoice_date" => $this->input->post('invoice_date'),
			"notes" => $this->input->post('notes'),
			"items" => $this->input->post('items')
		);

		$this->form_validation->set_rules('receiving_number', 'Receiving number', 'trim|required');
		$this->form_validation->set_rules('po_number', 'Purchase Order', 'trim|required');
		$this->form_validation->set_rules('officer', 'Purchase Officer', 'trim|required');
		$this->form_validation->set_rules('courier', 'Courier Name', 'trim|required');
		$this->form_validation->set_rules('invoice_number', 'Invoice Number', 'trim|required');
		$this->form_validation->set_rules('invoice_date', 'Invoice Date', 'trim|required');
		$this->form_validation->set_rules('notes', 'Notes', 'trim');
		$this->form_validation->set_rules('items', 'Items', 'trim|required');
		$this->form_validation->set_rules('documents', 'Items', 'trim|required');

		if ($this->form_validation->run()) {
			$user_data = $this->session->userdata('apollo_user');

			$courier_id = $this->receiving_model->add_courier(array(
				"name" => $company_receiving_data['courier']
			));

			$invoice_id = $this->receiving_model->add_invoice(array(
				"number" => $company_receiving_data['invoice_number'],
				"date" => $company_receiving_data["invoice_date"]." ".date("H:i:s")
			));

			$officer_id = $this->receiving_model->add_officer(array(
				"name" => $company_receiving_data['officer']
			));

			$data = array(
				"receiving_number" => $company_receiving_data["receiving_number"],
				"purchase_order" => $company_receiving_data["po_number"],
				"date_received" => date("Y-m-d H:i:s"),
				"status" => 0,
				"type" => 1,
				// "received_by" => $user_data["id"],
				"received_by" => 1, //TEMPORARY
				"invoice_id" => $invoice_id,
				"officer_id" => $officer_id,
				"courier_id" => $courier_id,
				"date_created" => date("Y-m-d H:i:s")
			);

			$receiving_id = $this->receiving_model->add_receiving_log_info($data);

			$items = json_decode($company_receiving_data["items"], true);
			if(count($items) > 0){
				$this->save_receiving_items($items, $receiving_id);
			}

			$notes = json_decode($company_receiving_data["notes"], true);
			if(count($notes) > 0){
				$this->save_receiving_notes($notes, $receiving_id);
			}

			$documents_data = $this->input->post("documents");
			$documents = json_decode($documents_data, true);
			if(count($documents) > 0){
				foreach($documents as $doc_k => $doc_v) {
					unset($documents[$doc_k]["date_added_formatted"]);
					$documents[$doc_k]["receiving_log_id"] = $receiving_id;
			    	$docs_id = $this->receiving_model->add_document($documents[$doc_k]);
				}				
			}
			
			$response = array(
				"status" => true,
				"message" => "Company good added.",
				"data" => $receiving_id
			);
			header("Content-Type: application/json");
			echo json_encode($response);

		} else {

			$response = array(
				"status" => false,
				"message" => "Please fill all required fields.",
				"data" => $this->form_validation->error_array()
			);
			header("Content-Type: application/json");
			echo json_encode($response);

		}
	}

	//==================================================//
	//=============END OF RECEIVING VIEWS===============//
	//==================================================//

	//==================================================//
	//=================PUBLIC FUNCTIONS=================//
	//==================================================//

	public function add()
	{

		$receiving_details = array(
				"receiving_number" => $this->input->post('receiving_number'),
				"purchase_order" => $this->input->post('po_number'),
				"status" => $this->input->post('status'),
				"type" => $this->input->post('type'),
				"received_by" => $this->input->post('user_id')
		);
		$items = $this->input->post('items');
		$decoded_items = json_decode($items, true);

		$this->form_validation->set_rules('po_number', 'PO Number', 'required|trim');
		$this->form_validation->set_rules('receiving_number', 'Receiving Number', 'required|trim|numeric');
		$this->form_validation->set_rules('type', 'Type', 'required|trim|integer');
		$this->form_validation->set_rules('user_id', 'User ID', 'required|trim|integer');
		$this->form_validation->set_rules('status', 'Status', 'required|trim|integer');
		$this->form_validation->set_rules('items', 'Items', 'required|trim');

		if ($this->form_validation->run()) 
		{
			$receiving_log_id = $this->receiving_model->add_receiving_log_info($receiving_details);
			
			foreach ($decoded_items as $item) {
				$where = "WHERE sku = '". $item["sku"] . "'";
				$product_details = $this->products_model->get_product($where);
				$product_id = $product_details[0]["id"];
				
				$arr_item = array(
					"qty" => $item["quantity_to_received"],
					"unit_of_measure" => 1,
					"product_inventory_id" => 0, //TO FOLLOW
					"product_receiving_id" => $receiving_log_id,
					"file_icon" => "sample.jpg",
					"product_id" => $product_id
				);
				
				$item_errors = $this->_validate_received_items($arr_item);		
				if($item_errors)
				{
					$arr_consignee = $item["consignee_distribution"];
					$this->receiving_model->add_product_inventory_qty($arr_item);					
					
					foreach ($arr_consignee as $consignee) {
						$arr_consignee = array(
							"receiving_log_id" => $receiving_log_id,
							"consignee_id" => $consignee["consignee_id"],
							"distribution_qty" => $consignee["qty"],
							"unit_of_measure_id" => 1
						);

						$consignee_errors = $this->_validate_received_consignee($arr_consignee);
						if($consignee_errors)
						{
							$this->receiving_model->add_receiving_consignee_map_info($arr_consignee);	
						}
						else
						{
							$response["message"] = "Consignee has errors!";
							echo json_encode($response);
						}					
					}
				}
				else
				{
					$response["message"] = "Items has errors!";
					echo json_encode($response);
				}
			}
		} 
		else 
		{
			$response["message"] = "Failed.";
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}

		if(isset($receiving_log_id))
		{
			$response["status"] = true;
			$response["message"] = "Success!";
			$response["data"] = $receiving_log_id;
		};
	}

	private function _validate_received_items($arr_item)
	{
		$errors = 0;
		foreach ($arr_item as $item) {
			if($item === null && $item == "")
			{
				$errors++;
			}
		}
		if($errors === 0) {
			return true;
		}
		else
		{
			return false;
		}
	}

	private function _validate_received_consignee($arr_consignee)
	{
		$errors = 0;
		foreach ($arr_consignee as $consignee) {
			if($consignee === null && $consignee == "")
			{
				$errors++;
			}
		}
		if($errors === 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function edit()
	{
		// $this->form_validation->set_error_delimiters('', '');

		$qualifiers = json_decode($this->input->post('qualifiers'), true);
		$update_data =json_decode($this->input->post('update_data'), true);
		$arr_data = array();

		$this->form_validation->set_rules("qualifiers", "Qualifiers", "callback_edit_validate_qualifiers");
		$this->form_validation->set_rules("update_data", "Data",  "callback_edit_validate_data");
 
		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
		else
		{
			$counter = 0;
			$query = "";

			if(count($qualifiers) > 0)
			{
				foreach($qualifiers as $key=>$value)
				{
					$data_val = $qualifiers[$key]["value"];
					$new_val = "";
					
					if(gettype($data_val) == "string")
					{
						$new_val = "'".$data_val."'";
					}
					else if(gettype($data_val) == "integer")
					{
						$new_val = intval($data_val);
					}
					else if(gettype($data_val) == "double")
					{
						$new_val = doubleval($data_val);
					}

					if($counter == 0)
					{
						$query = $qualifiers[$key]["field"].$qualifiers[$key]["operation"].$new_val;
					}
					else
					{
						$query .= ' AND '.$qualifiers[$key]["field"].$qualifiers[$key]["operation"].$new_val;
					}

					$counter++;
				}		
			}
			
			if($update_data !== null AND json_last_error() === JSON_ERROR_NONE)
			{

				// echo $update_data[$key]["receiving_number"];

				foreach ($update_data as $key => $value) {

					if(isset($update_data[$key]["receiving_number"]))
					{
						$arr_data["receiving_number"] = $update_data[$key]["receiving_number"];
					}

					if(isset($update_data[$key]["purchase_order"]))
					{
						$arr_data["purchase_order"] = $update_data[$key]["purchase_order"];
					}
					if(isset($update_data[$key]["date_received"]))
					{
						$arr_data["date_received"] = date("Y-m-d H:i:s", $update_data[$key]["date_received"]);
					}
					if(isset($update_data[$key]["status"]))
					{
						$arr_data["status"] = $update_data[$key]["status"];
					}
					if(isset($update_data[$key]["type"]))
					{
						$arr_data["type"] = $update_data[$key]["type"];
					}
					if(isset($update_data[$key]["comments"]))
					{
						$arr_data["comments"] = $update_data[$key]["comments"];
					}
					if(isset($update_data[$key]["received_by"]))
					{
						$arr_data["received_by"] = $update_data[$key]["received_by"];
					}

				}	
			}
			$result = $this->receiving_model->edit($query, $arr_data);

			$response["status"] = true;
			$response["message"] = "Successfully Updated";
			$response["data"] = array("affected_rows"=>$result);
			echo json_encode($response);
		}
	}	

	public function edit_validate_qualifiers()
	{
		$qualifiers = json_decode($this->input->post('qualifiers'), true);

		if($qualifiers !== null AND json_last_error() === JSON_ERROR_NONE)
		{
			foreach($qualifiers as $key=>$value)
			{
				if($qualifiers[$key]["field"] == "")
				{
					$this->form_validation->set_message("validate_qualifiers", "Field is Required");
					return false;
				}

				if($qualifiers[$key]["operation"] == "")
				{
					$this->form_validation->set_message("validate_qualifiers", "Operation is Required");
					return false;
				}

				if($qualifiers[$key]["value"] == "")
				{
					$this->form_validation->set_message("validate_qualifiers", "Value is Required");
					return false;
				}
			}
		}
	}

	public function edit_validate_data()
	{
		$update_data = json_decode($this->input->post('update_data'), true);
	
		if($update_data !== null AND json_last_error() === JSON_ERROR_NONE)
		{
			foreach ($update_data as $key => $value) {
				// echo $update_data[$key]["receiving_number"];

				if(isset($update_data[$key]["receiving_number"]) AND trim($update_data[$key]["receiving_number"]) == "")
				{
					$this->form_validation->set_message("edit_validate_data", "Receiving must not empty");
					return false;
				}
				if(isset($update_data[$key]["purchase_order"]) AND trim($update_data[$key]["purchase_order"]) == "")
				{
					$this->form_validation->set_message("edit_validate_data", "Purchase Order must not empty");
					return false;
				}
				if(isset($update_data[$key]["date_received"]) AND trim($update_data[$key]["date_received"]) == "")
				{
					$this->form_validation->set_message("edit_validate_data", "Date Received must not empty");
					return false;
				}
				if(isset($update_data[$key]["status"]) AND trim($update_data[$key]["status"]) == "")
				{
					$this->form_validation->set_message("edit_validate_data", "Status must not empty");
					return false;
				}
				if(isset($update_data[$key]["type"]) AND trim($update_data[$key]["type"]) == "")
				{
					$this->form_validation->set_message("edit_validate_data", "Type must not empty");
					return false;
				}
				if(isset($update_data[$key]["comments"]) AND trim($update_data[$key]["comments"]) == "")
				{
					$this->form_validation->set_message("edit_validate_data", "Comments must not empty");
					return false;
				}
				if(isset($update_data[$key]["received_by"]) AND trim($update_data[$key]["received_by"]) == "")
				{
					$this->form_validation->set_message("edit_validate_data", "Received By must not empty");
					return false;
				}

			}
		}
		else
		{
			$this->form_validation->set_message("edit_validate_data", "Set of Data is Required");
			return false;
		}
	}

	public function get_receiving_items_validation()
	{
		$items = $this->input->post('items');
		$arr_items = json_decode($items, true);

		if(count($arr_items) > 0)
		{
			$errors = 0;
			foreach ($$arr_items as $key => $value) {
				if(!isset($value['sku'])){
					$errors++;
				}
			}

			if($errors == 0){
				$this->form_validation->set_message('get_receiving_items_validation', 'Items format invalid add SKU');
				return false;
			}else{
				return true;
			}
		}
		else
		{
			$this->form_validation->set_message('get_receiving_items_validation', 'Items format invalid add SKU');
			return false;
		}
	}

	public function get()
	{
		$receiving_no = $this->input->post('receiving_no');
		$po_number = $this->input->post('po_number');
		$type = $this->input->post('type');
		$date_received = $this->input->post('date_received');
		$items = $this->input->post('items');
		$arr_items = json_decode($items, true);
		$received_by = $this->input->post('received_by');
		$status = $this->input->post('status');
		$offset = $this->input->post('offset');
		$limit = $this->input->post('limit');
		$sorting = $this->input->post('sorting');
		$sort_field = $this->input->post('sort_field');

		$this->form_validation->set_rules('receiving_no', 'Receiving Number', 'trim|max_length[160]|alpha_numeric');
		$this->form_validation->set_rules('po_number', 'Purchase Order Number', 'trim|max_length[20]|alpha_numeric');
		$this->form_validation->set_rules('type', 'Type', 'trim|numeric');
		$this->form_validation->set_rules('date_received', 'Date Received', 'trim');
		$this->form_validation->set_rules('received_by', 'Received By', 'trim|alpha_numeric');
		$this->form_validation->set_rules('status', 'Status', 'trim|numeric');
		$this->form_validation->set_rules('offset', 'Offset ', 'trim|numeric');
		$this->form_validation->set_rules('items', 'Items', 'callback_get_receiving_items_validation');

		$arr_where_query = array();

		if($receiving_no !== null)
		{
			$arr_where_query[] = " r.receiving_number = '".$receiving_no."'";
		}

		if($po_number !== null)
		{
			$arr_where_query[] = " r.purchase_order = '".$po_number."'";
		}

		if($type !== null)
		{
			$arr_where_query[] = " r.type = ".$type;
		}

		if($date_received !== null)
		{
			$arr_where_query[] = " r.date_received = ".$date_received;
		}

		if($date_received !== null)
		{
			$arr_where_query[] = " r.date_received = ".$date_received;
		}

		if($status !== null)
		{
			$arr_where_query[] = " r.status = ".$status;
		}

		$this->load->model('receiving_model');

		$counter = 0;
		$select = "r.id AS id, r.receiving_number AS receiving_number, r.purchase_order AS purchase_order, r.date_received AS date_received, r.status AS status, r.type AS type, r.received_by AS received_by";
		$where = "";
		if(count($arr_where_query) > 0){
			foreach ($arr_where_query as $key => $val) {
				if($counter > 0){
					$where .= "AND ".$val;
				}else{
					$where .= "WHERE ".$val;
				}
				$counter++;
			}
		}

		$items = $this->input->post('items');
		$arr_items = json_decode($items, true);
		$counter = 0;

		if(count($arr_items) > 0){
			foreach ($arr_items as $key => $value) {
				if($counter > 0){
					$where .= "AND p.sku = '".$value['sku']."'";
				}else{
					$where .= "WHERE p.sku = '".$value['sku']."'";
				}
				$counter++;
			}
		}
		$receiving_records = $this->receiving_model->get_receiving_records($select, $where);
		$response["status"] = true;
		$response["data"] = $receiving_records;
		echo json_encode($response);
	}

	public function get_location($id, $location)
	{
		foreach ($location as $key => $value) {
			if($value["id"] == $id){
				return $location[$key];
			}
		}

		return array();
	}

	public function get_parent_location($child, $location, $refs = array())
	{
		$data = &$refs;

		$parentdata = $this->get_location($child["parent_id"], $location);

		$data[] = $child;

		if( count($parentdata) > 0 ){
			return $this->get_parent_location($parentdata, $location, $data);
		}

		if( count($parentdata) === 0 ){
			return $data;
		}


	}

	public function get_hierarchy_of_batch_location($storage_id, $location_id)
	{
		$this->load->model("Storages/storage_model");
		$location = $this->storage_model->get_location($storage_id);
		$current = $this->get_location($location_id, $location);

		$data = $this->get_parent_location($current, $location);

		$reversed = array_reverse($data);
		return $reversed;
		
	}

	public function get_single_company_goods($receiving_id_param = 0){
		if($receiving_id_param == 0){
			$receiving_id = $this->input->post("receiving_id");
			$receiving_num = $this->input->post("receiving_number");

			$this->form_validation->set_rules('receiving_id', 'Receiving Id', 'trim|alpha_numeric');
			$this->form_validation->set_rules('receiving_number', 'Receiving Number', 'trim|alpha_numeric');
		}else{
			$receiving_id = $receiving_id_param;
		}


		if($this->form_validation->run() === FALSE && $receiving_id_param == 0)
		{
			header("Content-Type: application/json");
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{

			if(strlen($receiving_id) == 0 AND strlen($receiving_num) == 0 && $receiving_id_param == 0){
				header("Content-Type: application/json");
				$response["status"] = false;
				$response["message"] = validation_errors();
				$response["data"] = $this->form_validation->error_array();
				echo json_encode($response);
			}else{
				$this->load->model("receiving_model");
				$this->load->model("Users/users_model");
				$where = "";

				if(strlen($receiving_id) > 0){
					$where = " WHERE id = ".$receiving_id;
				}else if(strlen($receiving_num) > 0){
					$receiving_num = intval($receiving_num);
					$where = " WHERE receiving_number = ".$receiving_num;
				}

				$result = $this->receiving_model->get_single_company_goods($where);

				foreach ($result as $key => $value) {
					/* NOTES */
					$notes = $this->receiving_model->get_notes(" WHERE receiving_log_id = ".$value['id']." ");
					foreach($notes as $i => $val){
						$notes[$i]["date_formatted"] = date("j-M-Y g:i A", strtotime($val["date_created"]));
					}
					$result[$key]["notes"] = $notes;
					/* NOTES */

					/* ITEMS */
					$items = $this->receiving_model->get_receiving_company_goods_items($value['id'], "AND piq.is_deleted = 0");
					foreach ($items as $ik => $iv) {
						$storage_assignment = $this->receiving_model->get_storage_assignment(" WHERE receiving_log_id = ".$value['id']." AND product_inventory_qty_id = ".$iv['id']." AND is_deleted = 0 ");
						foreach ($storage_assignment as $sa_k => $sa_v) {
							$batch = $this->receiving_model->get_batch(" WHERE storage_assignment_id = ".$sa_v['id']." AND is_deleted = 0 ");
							foreach ($batch as $bk => $bv) {
								$batch[$bk]["location"] = $this->get_hierarchy_of_batch_location($bv["storage_id"], $bv["location_id"]);
							}
							$storage_assignment[$sa_k]["batch"] = $batch;
						}
						$items[$ik]["storage_assignment"] = $storage_assignment;

						$discrepancy = $this->receiving_model->get_discrepancy(" WHERE product_inventory_qty_id = ".$iv['id']);
						if( (count($discrepancy) > 0) ){
							$items[$ik]["discrepancy"] =  $discrepancy[0];
						}else{
							$items[$ik]["discrepancy"] = array();
						}
					}
					$result[$key]["items"] = $items;
					/* ITEMS */

					/* OWNER AND RECEIVER */
					$receiver = array();
					if(IS_DEVELOPMENT === true){
						$result[$key]["owner"] = $this->session->userdata('apollo_user');
					}else{
						$result[$key]["owner"] = $this->users_model->get_user($value['received_by']);
					}
					$result[$key]["receiving_number"] = $this->transform_to_receiving_number($value['receiving_number']);
					$result[$key]["receiver"] = $receiver;
					/* OWNER AND RECEIVER */
					
					/* INVOICE */
					$invoice = $this->receiving_model->get_invoice("WHERE id = ".$value['invoice_id']);
					$result[$key]["invoice_number"] = $invoice[0]['number'];
					$result[$key]["invoice_date"] = $invoice[0]['date'];
					$result[$key]["invoice_date_formatted"] = date("M. d, Y | g:i a", strtotime($invoice[0]['date']));
					$result[$key]["invoice_date_formatted_2"] = date("m/d/Y", strtotime($invoice[0]['date']));
					/* INVOICE */

					/* OFFICER */
					$officer = $this->receiving_model->get_officer("WHERE id = ".$value['officer_id']);
					$result[$key]["officer"] = $officer[0]['name'];
					/* OFFICER */

					/* COURIER */
					$courier = $this->receiving_model->get_courier("WHERE id = ".$value['courier_id']);
					$result[$key]["courier"] = $courier[0]['name'];
					/* COURIER */

					if(strlen($value["date_received"]) > 0){
						$result[$key]["date_received_formatted_no_time"] = date("j-M-Y", strtotime($value["date_received"]));
						$result[$key]["date_received_formatted"] = date("j-M-Y g:i a", strtotime($value["date_received"]));
					}else{
						$result[$key]["date_received_formatted_no_time"] =  "none";
						$result[$key]["date_received_formatted"] = "none";
					}

					$result[$key]["date_created_formatted"] = date("M d, Y | g:i a", strtotime($value["date_created"]));
					$docs = $this->receiving_model->get_documents($value['id']);
					foreach ($docs as $dk => $dv) {
						$docs[$dk]["date_added_formatted"] = date("j-M-Y g:i A", strtotime($dv["date_added"]));
					}
					$result[$key]["documents"] = $docs;
					
				}

				$response = array(
					"status" => true,
					"message" => "",
					"data" => $result
				);

				if($receiving_id_param > 0){
					return $result;
				}else{
					header("Content-Type: application/json");
					echo json_encode($response);
				}
			}
		}

		
	}


	public function get_all_company_goods() {
		$this->load->model("receiving_model");
		$this->load->model("Users/users_model");
		$result = $this->receiving_model->get_all_company_goods();

		foreach ($result as $key => $value) {
			/* NOTES */
			$notes = $this->receiving_model->get_notes(" WHERE receiving_log_id = ".$value['id']." ");
			foreach($notes as $i => $val){
				$notes[$i]["date_formatted"] = date("j-M-Y g:i A", strtotime($val["date_created"]));
			}
			$result[$key]["notes"] = $notes;
			/* NOTES */

			/* ITEMS */
			$items = $this->receiving_model->get_receiving_company_goods_items($value['id'], "AND piq.is_deleted = 0");
			foreach ($items as $ik => $iv) {
				$storage_assignment = $this->receiving_model->get_storage_assignment(" WHERE receiving_log_id = ".$value['id']." AND product_inventory_qty_id = ".$iv['id']." AND is_deleted = 0 ");
				foreach ($storage_assignment as $sa_k => $sa_v) {
					$batch = $this->receiving_model->get_batch(" WHERE storage_assignment_id = ".$sa_v['id']." AND is_deleted = 0");
					foreach ($batch as $bk => $bv) {
						$batch[$bk]["location"] = $this->get_hierarchy_of_batch_location($bv["storage_id"], $bv["location_id"]);
					}
					$storage_assignment[$sa_k]["batch"] = $batch;
				}
				$items[$ik]["storage_assignment"] = $storage_assignment;

				$discrepancy = $this->receiving_model->get_discrepancy(" WHERE product_inventory_qty_id = ".$iv['id']);
				if( (count($discrepancy) > 0) ){
					$items[$ik]["discrepancy"] =  $discrepancy[0];
				}else{
					$items[$ik]["discrepancy"] = array();
				}
			}
			$result[$key]["items"] = $items;
			/* ITEMS */

			/* OWNER AND RECEIVER */
			$receiver = array();
			if(IS_DEVELOPMENT === true){
				$result[$key]["owner"] = $this->session->userdata('apollo_user');
			}else{
				$result[$key]["owner"] = $this->users_model->get_user($value['received_by']);
			}
			$result[$key]["receiving_number"] = $this->transform_to_receiving_number($value['receiving_number']);
			$result[$key]["receiver"] = $receiver;
			/* OWNER AND RECEIVER */
			
			/* INVOICE */
			$invoice = $this->receiving_model->get_invoice("WHERE id = ".$value['invoice_id']);
			$result[$key]["invoice_number"] = $invoice[0]['number'];
			$result[$key]["invoice_date"] = $invoice[0]['date'];
			$result[$key]["invoice_date_formatted"] = date("M. d, Y | g:i a", strtotime($invoice[0]['date']));
			/* INVOICE */

			/* OFFICER */
			$officer = $this->receiving_model->get_officer("WHERE id = ".$value['officer_id']);
			$result[$key]["officer"] = $officer[0]['name'];
			/* OFFICER */

			/* COURIER */
			$courier = $this->receiving_model->get_courier("WHERE id = ".$value['courier_id']);
			$result[$key]["courier"] = $courier[0]['name'];
			/* COURIER */

			if(strlen($value["date_received"]) > 0){
				$result[$key]["date_received_formatted_no_time"] = date("j-M-Y", strtotime($value["date_received"]));
				$result[$key]["date_received_formatted"] = date("j-M-Y g:i a", strtotime($value["date_received"]));
			}else{
				$result[$key]["date_received_formatted_no_time"] =  "none";
				$result[$key]["date_received_formatted"] = "none";
			}

			$result[$key]["date_created_formatted"] = date("M. d, Y | g:i a", strtotime($value["date_created"]));
			$docs = $this->receiving_model->get_documents($value['id']);
			foreach ($docs as $dk => $dv) {
				$docs[$dk]["date_added_formatted"] = date("j-M-Y g:i A", strtotime($dv["date_added"]));
			}
			$result[$key]["documents"] = $docs;
			
		}

		$response = array(
			"status" => true,
			"message" => "",
			"data" => $result
		);

		header("Content-Type: application/json");
		echo json_encode($response);
	}


	// ==========================  CONSIGNEE GOODS =============================//

	public function get_all_products()
	{
		$result = $this->receiving_model->get_all_products();

		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $result;
		echo json_encode($response);
	}

	public function add_receiving_consignee_goods()
	{
		$consignee_goods_data = array(
			"receiving_number"=>$this->input->post('receiving_number'),
			"purchase_order"=>$this->input->post('purchase_order'),
			"mode_of_delivery"=>$this->input->post('mode_of_delivery'),
			"product_data"=>$this->input->post('product_data'),
			"note_data"=>$this->input->post('note_data'),
			"delivery_mode_data"=>$this->input->post('delivery_mode_data'),
			"consignee_items"=>$this->input->post('consignee_items'),
			"uploaded_documents"=>$this->input->post('uploaded_documents')
			); 

		$this->form_validation->set_rules("receiving_number", "Receiving Number", "trim|required|integer");
		$this->form_validation->set_rules("purchase_order", "Purchase Order", "trim|required");
		$this->form_validation->set_rules("mode_of_delivery", "Mode Of Delivery", "trim|required");
		$this->form_validation->set_rules("product_data", "Products Data", "trim|required");
		// $this->form_validation->set_rules("note_data", "Notes Data", "trim|required");
		$this->form_validation->set_rules("delivery_mode_data", "Delivery Mode Data", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);

		}else{
			$user_data = $this->session->userdata('apollo_user');

			$set_receiving_log_data = array(
				"receiving_number"=>$consignee_goods_data["receiving_number"],
				"purchase_order"=>$consignee_goods_data["purchase_order"],
				"status"=>0,
				"type"=>2,
				"date_created"=> date("Y-m-d H:i:s"),
				"received_by"=>$user_data["id"]
			);

			$receiving_id = $this->receiving_model->add_consignee_receiving_log($set_receiving_log_data);

			$product_inventory_data = json_decode($consignee_goods_data["product_data"], true);

			if(count($product_inventory_data) > 0)
			{
				$this->add_receiving_consignee_product_inventory($receiving_id, $product_inventory_data);
			}

			$notes = json_decode($consignee_goods_data["note_data"], true);
			if(count($notes) > 0)
			{
				$this->save_receiving_notes($notes, $receiving_id);
			}

			$vessels = json_decode($consignee_goods_data["delivery_mode_data"], true);
			if(count($vessels) > 0)
			{
				$this->save_receiving_vessels($vessels, $receiving_id);
			}

			$consignee_items = json_decode($consignee_goods_data["consignee_items"], true);
			if(count($consignee_items) > 0)
			{
				$this->save_receiving_consignee_map($consignee_items, $receiving_id);
			}

			$uploaded_documents = json_decode($consignee_goods_data["uploaded_documents"], true);
			if(count($uploaded_documents) > 0)
			{
				$this->save_receiving_uploaded_documents($uploaded_documents, $receiving_id);
			}

			$response["status"] = True;
			$response["message"] = "Successfully Added";
			$response["data"] = $receiving_id;
			echo json_encode($response);


		}

	}

	public function update_receiving_consignee_goods()
	{
		$consignee_goods_data = array(
			"receiving_id"=>$this->input->post('receiving_id'),
			"purchase_order"=>$this->input->post('purchase_order'),
			"mode_of_delivery"=>$this->input->post('mode_of_delivery'),
			"product_data"=>$this->input->post('product_data'),
			"delivery_mode_data"=>$this->input->post('delivery_mode_data'),
			"consignee_items"=>$this->input->post('consignee_items')
			); 

		$this->form_validation->set_rules("receiving_id", "Receiving ID", "trim|required|integer");
		$this->form_validation->set_rules("purchase_order", "Purchase Order", "trim|required");
		$this->form_validation->set_rules("mode_of_delivery", "Mode Of Delivery", "trim|required");
		$this->form_validation->set_rules("product_data", "Products Data", "trim|required");
		$this->form_validation->set_rules("delivery_mode_data", "Delivery Mode Data", "trim|required");
		$this->form_validation->set_rules("consignee_items", "Consignee Items", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{
			$user_data = $this->session->userdata('apollo_user');
			$receiving_id = $consignee_goods_data["receiving_id"];

			$set_receiving_log_data = array(
				"purchase_order"=>$consignee_goods_data["purchase_order"],
				"received_by"=>$user_data["id"]
			);

			$this->receiving_model->update_consignee_receiving_log($consignee_goods_data["receiving_id"], $set_receiving_log_data);

			$product_inventory_data = json_decode($consignee_goods_data["product_data"], true);

			if(count($product_inventory_data) > 0)
			{
				$this->edit_receiving_consignee_product_inventory($receiving_id, $product_inventory_data);
			}

			$vessels = json_decode($consignee_goods_data["delivery_mode_data"], true);
			if(count($vessels) > 0)
			{
				$this->update_receiving_vessels($vessels, $receiving_id);
			}

			$consignee_items = json_decode($consignee_goods_data["consignee_items"], true);
			if(count($consignee_items) > 0)
			{
				$this->update_receiving_consignee_map($consignee_items, $receiving_id);
			}
			
		}
	}


	public function save_receiving_consignee_map($consignee_items, $receiving_id)
	{
		foreach ($consignee_items as $key => $value) {

			$create_consignee_map = array(
					"receiving_log_id"=>$receiving_id,
					"product_id"=>$value["product_id"],
					"consignee_id"=>$value["consignee_id"],
					"distribution_qty"=>$value["distribution_qty"],
					"unit_of_measure_id"=>$value["unit_of_measure_id"],
					"bag"=>$value["bag"]

				);

				$this->receiving_model->save_receiving_consignee_map($create_consignee_map);

			
		}
	}

	public function update_receiving_consignee_map($consignee_items, $receiving_id)
	{
		$this->receiving_model->delete_receiving_consignee_map($receiving_id);

		foreach ($consignee_items as $key => $value) {

			$create_consignee_map = array(
					"receiving_log_id"=>$receiving_id,
					"product_id"=>$value["product_id"],
					"consignee_id"=>$value["consignee_id"],
					"distribution_qty"=>$value["distribution_qty"],
					"unit_of_measure_id"=>$value["unit_of_measure_id"],
					"bag"=>$value["bag"]

				);

				$this->receiving_model->save_receiving_consignee_map($create_consignee_map);

			
		}
	}

	public function save_receiving_vessels($vessels, $receiving_id)
	{
		
		if($vessels["modeType"] == "Vessel")
		{
			$create_date = $vessels["berthingDate"] . $vessels["transferTime"];
			$buildNewDate = strtotime($create_date);
			$new_date = date('Y-m-d G:i:s', $buildNewDate);

			$create_vessels_log = array(
				"origin"=>$vessels["vesselName"],
				"vessel_type"=>$vessels["vesselType"],
				"captain"=>$vessels["vesselCaptain"],
				"discharge_type"=>$vessels["dischargeType"],
				"hatches"=>$vessels["hatches"],
				"bill_of_landing_volume"=>$vessels["loadingVolume"],
				"berthing_time"=>$new_date,
				"receiving_log"=>$receiving_id
				);

			$this->receiving_model->add_receiving_vessel_log($create_vessels_log);
		}else if($vessels["modeType"] == "Truck"){
			$create_truck_log = array(
				"origin"=>$vessels["vesselOrigin"],
				"driver_name"=>$vessels["driverName"],
				"license_name"=>$vessels["licenseNum"],
				"plate_number"=>$vessels["plateNumber"],
				"trucking"=>$vessels["trucking"],
				"receiving_log_id"=>$receiving_id
				);

			$this->receiving_model->add_receiving_truck_log($create_truck_log);
		}
		
	}

	public function update_receiving_vessels($vessels, $receiving_id)
	{
		
		if($vessels["modeType"] == "Vessel")
		{


			$check_vessel = $this->receiving_model->check_receiving_vessel_log($receiving_id);

			if($check_vessel == 0){
				$create_date = $vessels["berthingDate"] ." ". $vessels["transferTime"];
				$buildNewDate = strtotime($create_date);
				$new_date = date('Y-m-d G:i:s', $buildNewDate);

				$create_vessels_log = array(
					"origin"=>$vessels["vesselName"],
					"vessel_type"=>$vessels["vesselType"],
					"captain"=>$vessels["vesselCaptain"],
					"discharge_type"=>$vessels["dischargeType"],
					"hatches"=>$vessels["hatches"],
					"bill_of_landing_volume"=>$vessels["loadingVolume"],
					"berthing_time"=>$new_date,
					"receiving_log"=>$receiving_id
					);

				$this->receiving_model->add_receiving_vessel_log($create_vessels_log);
			}else{
				$create_date = $vessels["berthingDate"] ." ". $vessels["transferTime"];
				$buildNewDate = strtotime($create_date);
				$new_date = date('Y-m-d G:i:s', $buildNewDate);

				$update_vessels_log = array(
					"origin"=>$vessels["vesselName"],
					"vessel_type"=>$vessels["vesselType"],
					"captain"=>$vessels["vesselCaptain"],
					"discharge_type"=>$vessels["dischargeType"],
					"hatches"=>$vessels["hatches"],
					"bill_of_landing_volume"=>$vessels["loadingVolume"],
					"berthing_time"=>$new_date
					);

				$this->receiving_model->update_receiving_vessel_log($receiving_id, $update_vessels_log);
			}

			
		}else if($vessels["modeType"] == "Truck"){

			$check_truck = $this->receiving_model->check_receiving_truck_log($receiving_id);

			if($check_truck == 0){

				$create_truck_log = array(
					"origin"=>$vessels["vessel_name"],
					"driver_name"=>$vessels["driver_name"],
					"license_name"=>$vessels["license_name"],
					"plate_number"=>$vessels["plate_number"],
					"trucking"=>$vessels["trucking"],
					"receiving_log_id"=>$receiving_id
					);

				$this->receiving_model->add_receiving_truck_log($create_truck_log);
			}else{
				$update_truck_log = array(
					"origin"=>$vessels["vessel_name"],
					"driver_name"=>$vessels["driver_name"],
					"license_name"=>$vessels["license_name"],
					"plate_number"=>$vessels["plate_number"],
					"trucking"=>$vessels["trucking"]
					);

				$this->receiving_model->update_receiving_truck_log($receiving_id, $update_truck_log);
			}

			
		}
		
	}

	public function add_receiving_consignee_product_inventory($receiving_id, $product_inventory_data)
	{
		foreach ($product_inventory_data as $key => $value) {

			$vendor_name = $value["vendor"];
			$vendor = $this->receiving_model->get_vendor(strtolower($vendor_name));
			$vendor_id = 0;
			if(count($vendor) > 0){
				$vendor_id = $vendor[0]["id"];
			}else{
				$vendor_id = $this->receiving_model->add_vendor(array(
					"name" => strtolower($vendor_name)
				));
			}

				$set_consignee_product_inventry = array(
					"qty"=>$value["qty_to_receive"],
					"product_receiving_id"=>$receiving_id,
					"product_id"=>$value["product_id"],
					"loading_method"=>$value["loading_method"],
					"vendor_id"=>$vendor_id,
					"unit_of_measure_id"=>$value["unit_of_measure"],
					"loading_method"=>$value["loading_method"],
					"bag"=>$value["bag"]
				);

				$this->receiving_model->add_receiving_item($set_consignee_product_inventry);

		}
	}

	public function remove_product_inventory()
	{
		$product_inventory_id = $this->input->post('product_inventory_id');

		$this->form_validation->set_rules('product_inventory_id', 'Product Inventory ID', 'trim|required');

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{

			$result = $this->receiving_model->remove_product_inventory($product_inventory_id);

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $result;
			echo json_encode($response);
		}

	}

	public function edit_receiving_consignee_product_inventory($receiving_id, $product_inventory_data)
	{

		// $this->receiving_model->delete_receiving_item($receiving_id);

		foreach ($product_inventory_data as $key => $value) {

			$vendor_name = $value["vendor"];
			$vendor = $this->receiving_model->get_vendor(strtolower($vendor_name));
			$vendor_id = 0;
			if(count($vendor) > 0){
				$vendor_id = $vendor[0]["id"];
			}else{
				$vendor_id = $this->receiving_model->add_vendor(array(
					"name" => strtolower($vendor_name)
				));
			}

			$check_product = $this->receiving_model->check_receiving_item($receiving_id, $value["product_id"]);

			if($check_product == 0)
			{
				$set_consignee_product_inventry = array(
					"qty"=>$value["qty_to_receive"],
					"product_receiving_id"=>$receiving_id,
					"product_id"=>$value["product_id"],
					"loading_method"=>$value["loading_method"],
					"vendor_id"=>$vendor_id,
					"unit_of_measure_id"=>$value["unit_of_measure_id"],
					"loading_method"=>$value["loading_method"],
					"bag"=>$value["bag"]
				);

				$this->receiving_model->add_receiving_item($set_consignee_product_inventry);
			}else{
				$update_consignee_product_inventry = array(
					"qty"=>$value["qty_to_receive"],
					"loading_method"=>$value["loading_method"],
					"vendor_id"=>$vendor_id,
					"unit_of_measure_id"=>$value["unit_of_measure_id"],
					"loading_method"=>$value["loading_method"],
					"bag"=>$value["bag"]
				);

				$this->receiving_model->update_receiving_item($receiving_id, $value["product_id"], $update_consignee_product_inventry);
			}


			

				

		}
	}

	public function save_complete_receiving_record_vessel()
	{
		$data = $this->input->post('data');

		$this->form_validation->set_rules("data", "Data", "trim|required");

		if($this->form_validation->run() === FALSE)
		{

		}else{
			
			$arr_data = json_decode($this->input->post('data'), true);
			// create departure time
			$get_departure = $arr_data["berthing_date"] ." ". $arr_data["berthing_time"];
			$convert_date = strtotime($get_departure);
			$new_berthing_time = date('Y-m-d G:i:s', $convert_date);

			// create start unloading
			$get_start = $arr_data["start_unloading_date"] ." ". $arr_data["start_unloading_time"];
			$convert_start = strtotime($get_start);
			$new_start_unloading = date('Y-m-d G:i:s', $convert_start); 

			// create end unloading
			$get_end = $arr_data["end_unloading_date"] ." ". $arr_data["end_unloading_time"];
			$convert_end = strtotime($get_end);
			$new_endunloading = date('Y-m-d G:i:s', $convert_end); 

			// create duration date 
			$get_duration = $arr_data["duration_date"] ." ". $arr_data["duration_time"];
			$convert_duration = strtotime($get_duration);
			$new_duration = date('Y-m-d G:i:s', $convert_duration); 

			$save_data = array(
					"start_unloading"=>$new_start_unloading,
					"end_unloading"=>$new_endunloading,
					"unloading_duration"=>$arr_data["unloading_duration"],
					"departure_time"=>$new_duration,
					"berthing_time"=>$new_berthing_time
				);

			$date_received = date("Y-m-d H:i:s");

			$this->receiving_model->receiving_consignee_complete_vessel_log($arr_data["receiving_id"], $save_data);
			$this->receiving_model->receiving_consignee_complete_receiving_log($arr_data["receiving_id"], $date_received);

			$get_for_inventory = $this->receiving_model->collect_for_inventory($arr_data["receiving_id"]);

			foreach ($get_for_inventory as $key => $value) {
				$data_intv = array(
					"quantity_received"=>$value["received_quantity"],
					"unit_of_measure_id"=>$value["unit_of_measure_id"],
					"date_created"=>$date_received,
					"status"=>0,
					"product_id"=>$value["product_id"],
					"last_update_date"=>$date_received
					);

				$this->receiving_model->save_to_product_inventory($data_intv);
			}


		}
	}

	public function save_receiving_uploaded_documents($uploaded_documents, $receiving_id)
	{
		foreach ($uploaded_documents as $key => $value) {
			$uploaded_data = array(
				"receiving_log_id"=>$receiving_id,
				"document_name"=>$uploaded_documents[$key]["document_name"],
				"document_path"=>$uploaded_documents[$key]["document_path"],
				"date_added"=>$uploaded_documents[$key]["date_added"],
				"status"=>$uploaded_documents[$key]["status"],
				"version"=>$uploaded_documents[$key]["version"]
				);

			$this->receiving_model->add_document($uploaded_data);
		}
	}



	public function get_vessel_name()
	{
		$this->load->model('Vessel_list/vessel_model');

		$result = $this->vessel_model->get_vessel_name();
		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $result;
		echo json_encode($response);
	}



	public function display_all_receiving_consignee_goods()
	{
		$this->load->model("Users/users_model");
		$user_data = $this->session->userdata('apollo_user');
		$origin = "";
		$status = "";
		$statusId = 0;

		$arr_collect_al = array();
		
		$get_receiving_data = $this->receiving_model->display_all_receiving_consignee_goods($user_data["id"]);

		foreach ($get_receiving_data as $key => $value) {

			$get_origin_vessel = $this->receiving_model->check_if_vessel($get_receiving_data[$key]["id"]);
			$get_origin_truck = $this->receiving_model->check_if_truck($get_receiving_data[$key]["id"]);

			if($get_origin_vessel != 0){
				$origin = "Vessel";
			}else if($get_origin_truck != 0){
				$origin = "Truck";
			}


			$departure_time_date="";
			$departure_time_set = "";
			if($origin == "Vessel")
			{
				
				$departure_time = strtotime($get_receiving_data[$key]["berthing_time"]);
				$new_departure_time = date('F d, Y h:i A', $departure_time);
				$departure_time_date =  date('m/d/Y', $departure_time);
				$departure_time_set =  date('h:i A', $departure_time);	
			}

			

			
			

			if($get_receiving_data[$key]["status"] == 0)
			{
				$status = "Ongoing";
				$statusId = 0;
			}elseif($get_receiving_data[$key]["status"] == 1){
				$status = "Complete";
				$statusId = 1;
			}elseif($get_receiving_data[$key]["status"] == 2){
				$status = "Ongoing";
				$statusId = 2;
			}




			$date_created = date("j-M-Y g:i A", strtotime($get_receiving_data[$key]["date_created"]));
			$recieving_number = $this->transform_to_receiving_number($get_receiving_data[$key]['receiving_number']);


			$gether_all_products_data = $this->receiving_model->get_all_products_data($get_receiving_data[$key]["id"]);
			if(IS_DEVELOPMENT === true){
				$get_receiving_data[$key]["owner"] = $user_data["first_name"]." ".$user_data["last_name"];
			}else{
				// $result[$key]["owner"] = $this->users_model->get_user($get_receiving_data[$key]["receiving_number"]);
			}

			$result = array(
				"receiving_id"=>$get_receiving_data[$key]["id"],
				"receiving_number"=>$recieving_number,
				"purchase_order"=>$get_receiving_data[$key]["purchase_order"],
				"issued_date"=>$date_created,
				"origin"=>$origin,
				"prducts"=>$gether_all_products_data,
				"owner"=>$get_receiving_data[$key]["owner"],
				"status"=>$status,
				"status_id"=>$statusId,
				"departure_date"=>$departure_time_date,
				"departure_time"=>$departure_time_set
				);
			array_push($arr_collect_al, $result);


		}

		header("Content-Type: application/json");

		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = $arr_collect_al;
		echo json_encode($response);

	
	}

	public function update_to_compelete_record()
	{
		$receiving_id = $this->input->post('receiving_id');

		$this->form_validation->set_rules("receiving_id", "Receiving ID", "required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{

			$this->receiving_model->change_status_receiving_log($receiving_id);

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = "Successfully Updated";
			echo json_encode($response);

		}
	}

	public function get_current_selected_receiving_consignee_vessel()
	{
		$receiving_id = $this->input->post('receiving_id');

		$this->form_validation->set_rules("receiving_id", "Receiving ID", "trim|required");

		if($this->form_validation->run() === FALSE){
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{


			$get_select_receiving_log = $this->receiving_model->get_selected_current_receiving_log($receiving_id);
			$get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);
			$get_select_receiving_documents = $this->receiving_model->get_documents($receiving_id);
			$status = "";
			$arr_collect_product_inventory = array();
			$arr_collect_data = array();
			$arr_collect_product = array();
			$statusId = 0;

			// print_r($get_select_receiving_log);
			// exit();

			$buildNewDate = strtotime($get_select_receiving_log[0]["date_created"]);
			$new_date = date('F d, Y', $buildNewDate);
			$new_time = date('h:i A', $buildNewDate);
			$new_datetime = $new_date." | ".$new_time;

			$departure_time = strtotime($get_select_receiving_log[0]["berthing_time"]);
			$new_departure_time = date('F d, Y h:i A', $departure_time);
			$departure_time_date =  date('m/d/Y', $departure_time);
			$departure_time_set =  date('h:i A', $departure_time);

			foreach ($get_select_receiving_documents as $key => $value) {
				$get_time = strtotime($get_select_receiving_documents[$key]["date_added"]);
				$new_date = date('F d, Y h:i A', $get_time);
				$get_select_receiving_documents[$key]["date_added_formatted"] = $new_date;
			}


			// foreach ($get_select_receiving_log as $key => $value) {

				


				if($get_select_receiving_log[0]["status"] == 0)
				{
					$status = "Ongoing";
					$statusId = 0;
				}elseif($get_select_receiving_log[0]["status"] == 1){
					$status = "Complete";
					$statusId = 1;
				}elseif($get_select_receiving_log[0]["status"] == 2){
					$status = "Ongoing";
					$statusId = 2;
				}

				// $get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);
				$get_selected_product_inventory = $this->receiving_model->get_selected_product_inventory($receiving_id);
				foreach ($get_selected_product_inventory as $key_consignee => $value_consignee) {
					$get_selected_consignee_map = $this->receiving_model->get_selected_consignee_map($get_selected_product_inventory[$key_consignee]["product_id"], $receiving_id);
					
					$arr_collect_product = array(
							"product_id"=>$get_selected_product_inventory[$key_consignee]["product_id"],
							"product_inventory_id"=>$get_selected_product_inventory[$key_consignee]["product_inventory_id"],
							"product_name"=>$get_selected_product_inventory[$key_consignee]["name"],
							"sku"=>$get_selected_product_inventory[$key_consignee]["sku"],
							"product_image"=>$get_selected_product_inventory[$key_consignee]["image"],
							"qty"=>$get_selected_product_inventory[$key_consignee]["qty"],
							"loading_method"=>$get_selected_product_inventory[$key_consignee]["loading_method"],
							"vendor_name"=>$get_selected_product_inventory[$key_consignee]["vendor_name"],
							"measures_name"=>$get_selected_product_inventory[$key_consignee]["measures_name"],
							"unit_of_measure_id"=>$get_selected_product_inventory[$key_consignee]["unit_of_measure_id"],
							"bag"=>$get_selected_product_inventory[$key_consignee]["bag"],
							"consignee_info"=>$get_selected_consignee_map
						);



					$storage_assignment = $this->receiving_model->get_storage_assignment(" WHERE receiving_log_id = ".$receiving_id." AND product_inventory_qty_id = ".$get_selected_product_inventory[$key_consignee]["product_inventory_id"]." ");
					foreach ($storage_assignment as $sa_k => $sa_v) {
						$batch = $this->receiving_model->get_batch(" WHERE storage_assignment_id = ".$sa_v['id']." AND is_deleted = 0 ");
						foreach ($batch as $bk => $bv) {
							$batch[$bk]["location"] = $this->get_hierarchy_of_batch_location($bv["storage_id"], $bv["location_id"]);
						}

						if( (count($batch) > 0) ){
							$storage_assignment[$sa_k]["batch"] = $batch;
						}
						
					}

					if( (count($storage_assignment) > 0) ){
						$arr_collect_product["storage_assignment"] = $storage_assignment;
					}
					


					$discrepancy = $this->receiving_model->get_discrepancy(" WHERE product_inventory_qty_id = ".$get_selected_product_inventory[$key_consignee]["product_inventory_id"]);
					if( (count($discrepancy) > 0) ){
						$arr_collect_product["discrepancy"] =  $discrepancy[0];
					}


					array_push($arr_collect_product_inventory, $arr_collect_product);

				}
				$receiving_number = $this->transform_to_receiving_number($get_select_receiving_log[0]["receiving_number"]);

				$arr_collect_data = array(
					"receiving_id"=>$get_select_receiving_log[0]["receiving_id"],
					"receiving_number"=>$receiving_number,
					"origin"=>"Vessel",
					"purchase_order"=>$get_select_receiving_log[0]["purchase_order"],
					"date_issued"=>$new_datetime,
					"status"=>$status,
					"status_id"=>$statusId,
					"vessel_name"=>$get_select_receiving_log[0]["vessel_name"],
					"vessel_type"=>$get_select_receiving_log[0]["vessel_type"],
					"vessel_captain"=>$get_select_receiving_log[0]["captain"],
					"hatches"=>$get_select_receiving_log[0]["hatches"],
					"discharge_type"=>$get_select_receiving_log[0]["discharge_type"],
					"bill_of_landing_volume"=>$get_select_receiving_log[0]["bill_of_landing_volume"],
					"bill_measure"=>$get_select_receiving_log[0]["bill_measure"],
					"berthing_time"=>$new_departure_time,
					"berthing_time_set"=>$departure_time_set,
					"berthing_time_date"=>$departure_time_date,
					"product_info"=>$arr_collect_product_inventory,
					"receiving_notes"=>$get_select_receiving_notes,
					"receiving_documents"=>$get_select_receiving_documents


				);

				// echo "<pre style='color:white;'>";
				// print_r($arr_collect_data);
				// echo "</pre>";
				// exit();

				header("Content-Type: application/json");

				$response["status"] = true;
				$response["message"] = "Success";
				$response["data"] = $arr_collect_data;
				echo json_encode($response);
			// }

			

		}
	}



	public function get_current_selected_receiving_consignee_vessel_1()
	{
		$receiving_number = $this->input->post('receiving_number');

		$this->form_validation->set_rules("receiving_number", "Receiving Number", "trim|required");

		if($this->form_validation->run() === FALSE){
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{

			$receiving_result = $this->receiving_model->get_receiving_id($receiving_number);

			$receiving_id = $receiving_result[0]["id"];

			$get_select_receiving_log = $this->receiving_model->get_selected_current_receiving_log($receiving_id);
			$get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);
			$get_select_receiving_documents = $this->receiving_model->get_documents($receiving_id);
			$status = "";
			$arr_collect_product_inventory = array();
			$arr_collect_data = array();
			$arr_collect_product = array();
			$statusId = 0;

			foreach ($get_select_receiving_documents as $key => $value) {
				$get_time = strtotime($get_select_receiving_documents[$key]["date_added"]);
				$new_date = date('F d, Y h:i A', $get_time);
				$get_select_receiving_documents[$key]["date_added_formatted"] = $new_date;
			}


			


			// foreach ($get_select_receiving_log as $key => $value) {

				$buildNewDate = strtotime($get_select_receiving_log[0]["date_created"]);
				$new_date = date('F d, Y', $buildNewDate);
				$new_time = date('h:i A', $buildNewDate);
				$new_datetime = $new_date." | ".$new_time;

				$departure_time = strtotime($get_select_receiving_log[0]["berthing_time"]);
				$new_departure_time = date('F d, Y h:i A', $departure_time);
				$departure_time_date =  date('m/d/Y', $departure_time);
				$departure_time_set =  date('h:i A', $departure_time);

				
				if($get_select_receiving_log[0]["status"] == 0)
				{
					$status = "Ongoing";
					$statusId = 0;
				}elseif($get_select_receiving_log[0]["status"] == 1){
					$status = "Complete";
					$statusId = 1;
				}elseif($get_select_receiving_log[0]["status"] == 2){
					$status = "Ongoing";
					$statusId = 2;
				}


				// $get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);
				$get_selected_product_inventory = $this->receiving_model->get_selected_product_inventory($receiving_id);
				foreach ($get_selected_product_inventory as $key_consignee => $value_consignee) {
					$get_selected_consignee_map = $this->receiving_model->get_selected_consignee_map($get_selected_product_inventory[$key_consignee]["product_id"], $receiving_id);
					
					
					
					$arr_collect_product = array(
							"product_id"=>$get_selected_product_inventory[$key_consignee]["product_id"],
							"product_inventory_id"=>$get_selected_product_inventory[$key_consignee]["product_inventory_id"],
							"product_name"=>$get_selected_product_inventory[$key_consignee]["name"],
							"sku"=>$get_selected_product_inventory[$key_consignee]["sku"],
							"product_image"=>$get_selected_product_inventory[$key_consignee]["image"],
							"qty"=>$get_selected_product_inventory[$key_consignee]["qty"],
							"loading_method"=>$get_selected_product_inventory[$key_consignee]["loading_method"],
							"vendor_name"=>$get_selected_product_inventory[$key_consignee]["vendor_name"],
							"measures_name"=>$get_selected_product_inventory[$key_consignee]["measures_name"],
							"unit_of_measure_id"=>$get_selected_product_inventory[$key_consignee]["unit_of_measure_id"],
							"bag"=>$get_selected_product_inventory[$key_consignee]["bag"],
							"consignee_info"=>$get_selected_consignee_map
						);


					$storage_assignment = $this->receiving_model->get_storage_assignment(" WHERE receiving_log_id = ".$receiving_id." AND product_inventory_qty_id = ".$get_selected_product_inventory[$key_consignee]["product_inventory_id"]." ");
					foreach ($storage_assignment as $sa_k => $sa_v) {
						$batch = $this->receiving_model->get_batch(" WHERE storage_assignment_id = ".$sa_v['id']." AND is_deleted = 0 ");
						foreach ($batch as $bk => $bv) {
							$batch[$bk]["location"] = $this->get_hierarchy_of_batch_location($bv["storage_id"], $bv["location_id"]);
						}

						if( (count($batch) > 0) ){
							$storage_assignment[$sa_k]["batch"] = $batch;
						}
						
					}

					if( (count($storage_assignment) > 0) ){
						$arr_collect_product["storage_assignment"] = $storage_assignment;
					}
					


					$discrepancy = $this->receiving_model->get_discrepancy(" WHERE product_inventory_qty_id = ".$get_selected_product_inventory[$key_consignee]["product_inventory_id"]);
					if( (count($discrepancy) > 0) ){
						$arr_collect_product["discrepancy"] =  $discrepancy[0];
					}



					array_push($arr_collect_product_inventory, $arr_collect_product);

				}







				$receiving_number = $this->transform_to_receiving_number($get_select_receiving_log[0]["receiving_number"]);

				$arr_collect_data = array(
					"receiving_id"=>$get_select_receiving_log[0]["receiving_id"],
					"receiving_number"=>$receiving_number,
					"purchase_order"=>$get_select_receiving_log[0]["purchase_order"],
					"date_issued"=>$new_datetime,
					"status"=>$status,
					"status_id"=>$statusId,
					"vessel_name"=>$get_select_receiving_log[0]["vessel_name"],
					"vessel_type"=>$get_select_receiving_log[0]["vessel_type"],
					"vessel_captain"=>$get_select_receiving_log[0]["captain"],
					"hatches"=>$get_select_receiving_log[0]["hatches"],
					"discharge_type"=>$get_select_receiving_log[0]["discharge_type"],
					"bill_of_landing_volume"=>$get_select_receiving_log[0]["bill_of_landing_volume"],
					"berthing_time"=>$new_departure_time,
					"berthing_time_set"=>$departure_time_set,
					"berthing_time_date"=>$departure_time_date,
					"product_info"=>$arr_collect_product_inventory,
					"receiving_notes"=>$get_select_receiving_notes,
					"receiving_documents"=>$get_select_receiving_documents


				);


			// }

			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $arr_collect_data;
			echo json_encode($response);

			

			


		}
	}

	public function get_receiving_consignee_vessel_complete()
	{
		$receiving_id = $this->input->post('receiving_id');

		$this->form_validation->set_rules("receiving_id", "Receiving ID", "trim|required");

		if($this->form_validation->run() === FALSE){
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{
			
			$get_select_receiving_log = $this->receiving_model->get_receiving_log_complete($receiving_id);
			$get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);
			$get_select_receiving_documents = $this->receiving_model->get_documents($receiving_id);
			$status = "";
			$arr_collect_product_inventory = array();
			$arr_collect_data = array();
			$arr_collect_product = array();
			$statusId = 0;

			// print_r($get_select_receiving_log);
			// exit();

			$buildNewDate = strtotime($get_select_receiving_log[0]["date_created"]);
			$new_date = date('F d, Y', $buildNewDate);
			$new_time = date('h:i A', $buildNewDate);
			$new_datetime = $new_date." | ".$new_time;

			$departure_time = strtotime($get_select_receiving_log[0]["berthing_time"]);
			$new_departure_time = date('F d, Y h:i A', $departure_time);
			$departure_time_date =  date('m/d/Y', $departure_time);
			$departure_time_set =  date('h:i A', $departure_time);


			// For Finished Process
				// $departure_time_complete = strtotime($get_select_receiving_log[0]["berthing_time"]);
				// $new_departure_time_complete = date('d-M-Y h:i A', $departure_time_complete);

				$new_berthing_time_complete = "";
				$new_start_unloading_complete = "";
				$new_end_unloading_complete = "";
				$new_departure_time_complete = "";
				$new_unloading_duration_complete = "";

				if($get_select_receiving_log[0]["berthing_time"] != "0000-00-00 00:00:00")
				{
					$berthing_time_complete = strtotime($get_select_receiving_log[0]["berthing_time"]);
					$new_berthing_time_complete = date('d-M-Y h:i A', $berthing_time_complete);
				}

				if($get_select_receiving_log[0]["start_unloading"] != "0000-00-00 00:00:00")
				{
					$start_unloading_complete = strtotime($get_select_receiving_log[0]["start_unloading"]);
					$new_start_unloading_complete = date('d-M-Y h:i A', $start_unloading_complete);
				}

				if($get_select_receiving_log[0]["end_unloading"] != "0000-00-00 00:00:00")
				{
					$end_unloading_complete = strtotime($get_select_receiving_log[0]["end_unloading"]);
					$new_end_unloading_complete = date('d-M-Y h:i A', $end_unloading_complete);
				}

				if($get_select_receiving_log[0]["departure_time"] != "0000-00-00 00:00:00")
				{
					$departure_time_complete = strtotime($get_select_receiving_log[0]["departure_time"]);
					$new_departure_time_complete = date('d-M-Y h:i A', $departure_time_complete);
				}

				if($get_select_receiving_log[0]["unloading_duration"] != 0)
				{
					
					$new_unloading_duration_complete = $get_select_receiving_log[0]["unloading_duration"];
				}
				
			// end




			foreach ($get_select_receiving_documents as $key => $value) {
				$get_time = strtotime($get_select_receiving_documents[$key]["date_added"]);
				$new_date = date('F d, Y h:i A', $get_time);
				$get_select_receiving_documents[$key]["date_added_formatted"] = $new_date;
			}


			foreach ($get_select_receiving_log as $key => $value) {

				


				if($get_select_receiving_log[0]["status"] == 0)
				{
					$status = "Ongoing";
					$statusId = 0;
				}elseif($get_select_receiving_log[0]["status"] == 1){
					$status = "Complete";
					$statusId = 1;
				}elseif($get_select_receiving_log[0]["status"] == 2){
					$status = "Ongoing";
					$statusId = 2;
				}

				// $get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);
				$get_selected_product_inventory = $this->receiving_model->get_selected_product_inventory($receiving_id);
				foreach ($get_selected_product_inventory as $key_consignee => $value_consignee) {
					$get_selected_consignee_map = $this->receiving_model->get_selected_consignee_map($get_selected_product_inventory[$key_consignee]["product_id"], $receiving_id);
					
					$arr_collect_product = array(
							"product_id"=>$get_selected_product_inventory[$key_consignee]["product_id"],
							"product_inventory_id"=>$get_selected_product_inventory[$key_consignee]["product_inventory_id"],
							"product_name"=>$get_selected_product_inventory[$key_consignee]["name"],
							"sku"=>$get_selected_product_inventory[$key_consignee]["sku"],
							"product_image"=>$get_selected_product_inventory[$key_consignee]["image"],
							"qty"=>$get_selected_product_inventory[$key_consignee]["qty"],
							"loading_method"=>$get_selected_product_inventory[$key_consignee]["loading_method"],
							"vendor_name"=>$get_selected_product_inventory[$key_consignee]["vendor_name"],
							"measures_name"=>$get_selected_product_inventory[$key_consignee]["measures_name"],
							"unit_of_measure_id"=>$get_selected_product_inventory[$key_consignee]["unit_of_measure_id"],
							"bag"=>$get_selected_product_inventory[$key_consignee]["bag"],
							"consignee_info"=>$get_selected_consignee_map
						);



					$storage_assignment = $this->receiving_model->get_storage_assignment(" WHERE receiving_log_id = ".$receiving_id." AND product_inventory_qty_id = ".$get_selected_product_inventory[$key_consignee]["product_inventory_id"]." ");
					foreach ($storage_assignment as $sa_k => $sa_v) {
						$batch = $this->receiving_model->get_batch(" WHERE storage_assignment_id = ".$sa_v['id']." ");
						foreach ($batch as $bk => $bv) {
							$batch[$bk]["location"] = $this->get_hierarchy_of_batch_location($bv["storage_id"], $bv["location_id"]);
						}

						if( (count($batch) > 0) ){
							$storage_assignment[$sa_k]["batch"] = $batch;
						}
						
					}

					if( (count($storage_assignment) > 0) ){
						$arr_collect_product["storage_assignment"] = $storage_assignment;
					}
					


					$discrepancy = $this->receiving_model->get_discrepancy(" WHERE product_inventory_qty_id = ".$get_selected_product_inventory[$key_consignee]["product_inventory_id"]);
					if( (count($discrepancy) > 0) ){
						$arr_collect_product["discrepancy"] =  $discrepancy[0];
					}


					array_push($arr_collect_product_inventory, $arr_collect_product);

				}
				$receiving_number = $this->transform_to_receiving_number($get_select_receiving_log[0]["receiving_number"]);

				$arr_collect_data = array(
					"receiving_id"=>$get_select_receiving_log[0]["receiving_id"],
					"receiving_number"=>$receiving_number,
					"purchase_order"=>$get_select_receiving_log[0]["purchase_order"],
					"date_issued"=>$new_datetime,
					"status"=>$status,
					"status_id"=>$statusId,
					"vessel_name"=>$get_select_receiving_log[0]["vessel_name"],
					"vessel_type"=>$get_select_receiving_log[0]["vessel_type"],
					"vessel_captain"=>$get_select_receiving_log[0]["captain"],
					"hatches"=>$get_select_receiving_log[0]["hatches"],
					"discharge_type"=>$get_select_receiving_log[0]["discharge_type"],
					"bill_of_landing_volume"=>$get_select_receiving_log[0]["bill_of_landing_volume"],
					"berthing_time"=>$new_departure_time,
					"berthing_time_set"=>$departure_time_set,
					"berthing_time_date"=>$departure_time_date,
					"complete_berthing_time"=>$new_berthing_time_complete,
					"complete_start_unloading"=>$new_start_unloading_complete,
					"complete_end_unloading"=>$new_end_unloading_complete,
					"complete_departure_time"=>$new_departure_time_complete,
					"complete_unloading_duration"=>$new_unloading_duration_complete,
					"product_info"=>$arr_collect_product_inventory,
					"receiving_notes"=>$get_select_receiving_notes,
					"receiving_documents"=>$get_select_receiving_documents

				);
			}

			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $arr_collect_data;
			echo json_encode($response);

		}
	}
	

	public function get_current_selected_receiving_consignee_truck_1()
	{
		$receiving_number = $this->input->post('receiving_number');

		$this->form_validation->set_rules("receiving_number", "Receiving Number", "trim|required");

		if($this->form_validation->run() === FALSE){
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{

			$receiving_result = $this->receiving_model->get_receiving_id($receiving_number);

			$receiving_id = $receiving_result[0]["id"];			

			$get_select_receiving_log = $this->receiving_model->get_selected_current_receiving_log_truck($receiving_id);
			$get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);
			$get_select_receiving_documents = $this->receiving_model->get_documents($receiving_id);
			$status = "";
			$arr_collect_product_inventory = array();
			$arr_collect_data = array();
			$statusId = 0;

			foreach ($get_select_receiving_documents as $key => $value) {
				$get_time = strtotime($get_select_receiving_documents[$key]["date_added"]);
				$new_date = date('F d, Y h:i A', $get_time);
				$get_select_receiving_documents[$key]["date_added_formatted"] = $new_date;
			}


			foreach ($get_select_receiving_log as $key => $value) {

				$buildNewDate = strtotime($get_select_receiving_log[0]["date_created"]);
				$new_date = date('F d, Y', $buildNewDate);
				$new_time = date('h:i A', $buildNewDate);
				$new_datetime = $new_date." | ".$new_time;


				if($get_select_receiving_log[0]["status"] == 0)
				{
					$status = "Ongoing";
					$statusId = 0;
				}elseif($get_select_receiving_log[0]["status"] == 1){
					$status = "Complete";
					$statusId = 1;
				}elseif($get_select_receiving_log[0]["status"] == 2){
					$status = "Ongoing";
					$statusId = 2;
				}



				$get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);
				$get_selected_product_inventory = $this->receiving_model->get_selected_product_inventory($receiving_id);
				foreach ($get_selected_product_inventory as $key_consignee => $value_consignee) {
					$get_selected_consignee_map = $this->receiving_model->get_selected_consignee_map($get_selected_product_inventory[$key_consignee]["product_id"], $receiving_id);
					
					$arr_collect_product = array(
							"product_id"=>$get_selected_product_inventory[$key_consignee]["product_id"],
							"product_inventory_id"=>$get_selected_product_inventory[$key_consignee]["product_inventory_id"],
							"product_name"=>$get_selected_product_inventory[$key_consignee]["name"],
							"sku"=>$get_selected_product_inventory[$key_consignee]["sku"],
							"product_image"=>$get_selected_product_inventory[$key_consignee]["image"],
							"qty"=>$get_selected_product_inventory[$key_consignee]["qty"],
							"loading_method"=>$get_selected_product_inventory[$key_consignee]["loading_method"],
							"vendor_name"=>$get_selected_product_inventory[$key_consignee]["vendor_name"],
							"measures_name"=>$get_selected_product_inventory[$key_consignee]["measures_name"],
							"unit_of_measure_id"=>$get_selected_product_inventory[$key_consignee]["unit_of_measure_id"],
							"bag"=>$get_selected_product_inventory[$key_consignee]["bag"],
							"consignee_info"=>$get_selected_consignee_map
						);

					$storage_assignment = $this->receiving_model->get_storage_assignment(" WHERE receiving_log_id = ".$receiving_id." AND product_inventory_qty_id = ".$get_selected_product_inventory[$key_consignee]["product_inventory_id"]." ");
					foreach ($storage_assignment as $sa_k => $sa_v) {
						$batch = $this->receiving_model->get_batch(" WHERE storage_assignment_id = ".$sa_v['id']." ");
						foreach ($batch as $bk => $bv) {
							$batch[$bk]["location"] = $this->get_hierarchy_of_batch_location($bv["storage_id"], $bv["location_id"]);
						}

						if( (count($batch) > 0) ){
							$storage_assignment[$sa_k]["batch"] = $batch;
						}
						
					}

					if( (count($storage_assignment) > 0) ){
						$arr_collect_product["storage_assignment"] = $storage_assignment;
					}
					


					$discrepancy = $this->receiving_model->get_discrepancy(" WHERE product_inventory_qty_id = ".$get_selected_product_inventory[$key_consignee]["product_inventory_id"]);
					if( (count($discrepancy) > 0) ){
						$arr_collect_product["discrepancy"] =  $discrepancy[0];
					}

					array_push($arr_collect_product_inventory, $arr_collect_product);

				}
				$receiving_number = $this->transform_to_receiving_number($get_select_receiving_log[0]["receiving_number"]);
				$arr_collect_data = array(
					"receiving_id"=>$get_select_receiving_log[0]["receiving_id"],
					"receiving_number"=>$receiving_number,
					"purchase_order"=>$get_select_receiving_log[0]["purchase_order"],
					"date_issued"=>$new_datetime,
					"status"=>$status,
					"status_id"=>$statusId,
					"vessel_name"=>$get_select_receiving_log[0]["vessel_name"],
					"trucking"=>$get_select_receiving_log[0]["trucking"],
					"driver_name"=>$get_select_receiving_log[0]["driver_name"],
					"license_name"=>$get_select_receiving_log[0]["license_name"],
					"plate_number"=>$get_select_receiving_log[0]["plate_number"],
					"product_info"=>$arr_collect_product_inventory,
					"receiving_notes"=>$get_select_receiving_notes,
					"receiving_documents"=>$get_select_receiving_documents


				);
			}

			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $arr_collect_data;
			echo json_encode($response);

			


		}
	}

	public function get_current_selected_receiving_consignee_truck()
	{
		$receiving_id = $this->input->post('receiving_id');

		$this->form_validation->set_rules("receiving_id", "Receiving ID", "trim|required");

		if($this->form_validation->run() === FALSE){
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{
			$get_select_receiving_log = $this->receiving_model->get_selected_current_receiving_log_truck($receiving_id);
			$get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);
			$get_select_receiving_documents = $this->receiving_model->get_documents($receiving_id);
			$status = "";
			$arr_collect_product_inventory = array();
			$arr_collect_data = array();
			$statusId = 0;

			foreach ($get_select_receiving_documents as $key => $value) {
				$get_time = strtotime($get_select_receiving_documents[$key]["date_added"]);
				$new_date = date('F d, Y h:i A', $get_time);
				$get_select_receiving_documents[$key]["date_added_formatted"] = $new_date;
			}

			foreach ($get_select_receiving_log as $key => $value) {

				$buildNewDate = strtotime($get_select_receiving_log[0]["date_created"]);
				$new_date = date('F d, Y', $buildNewDate);
				$new_time = date('h:i A', $buildNewDate);
				$new_datetime = $new_date." | ".$new_time;


				if($get_select_receiving_log[0]["status"] == 0)
				{
					$status = "Ongoing";
					$statusId = 0;
				}elseif($get_select_receiving_log[0]["status"] == 1){
					$status = "Complete";
					$statusId = 1;
				}elseif($get_select_receiving_log[0]["status"] == 2){
					$status = "Ongoing";
					$statusId = 2;
				}

				$get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);
				$get_selected_product_inventory = $this->receiving_model->get_selected_product_inventory($receiving_id);
				foreach ($get_selected_product_inventory as $key_consignee => $value_consignee) {
					$get_selected_consignee_map = $this->receiving_model->get_selected_consignee_map($get_selected_product_inventory[$key_consignee]["product_id"], $receiving_id);
					
					$arr_collect_product = array(
							"product_id"=>$get_selected_product_inventory[$key_consignee]["product_id"],
							"product_inventory_id"=>$get_selected_product_inventory[$key_consignee]["product_inventory_id"],
							"product_name"=>$get_selected_product_inventory[$key_consignee]["name"],
							"sku"=>$get_selected_product_inventory[$key_consignee]["sku"],
							"product_image"=>$get_selected_product_inventory[$key_consignee]["image"],
							"qty"=>$get_selected_product_inventory[$key_consignee]["qty"],
							"loading_method"=>$get_selected_product_inventory[$key_consignee]["loading_method"],
							"vendor_name"=>$get_selected_product_inventory[$key_consignee]["vendor_name"],
							"measures_name"=>$get_selected_product_inventory[$key_consignee]["measures_name"],
							"unit_of_measure_id"=>$get_selected_product_inventory[$key_consignee]["unit_of_measure_id"],
							"bag"=>$get_selected_product_inventory[$key_consignee]["bag"],
							"consignee_info"=>$get_selected_consignee_map
						);

					$storage_assignment = $this->receiving_model->get_storage_assignment(" WHERE receiving_log_id = ".$receiving_id." AND product_inventory_qty_id = ".$get_selected_product_inventory[$key_consignee]["product_inventory_id"]." ");
					foreach ($storage_assignment as $sa_k => $sa_v) {
						$batch = $this->receiving_model->get_batch(" WHERE storage_assignment_id = ".$sa_v['id']." ");
						foreach ($batch as $bk => $bv) {
							$batch[$bk]["location"] = $this->get_hierarchy_of_batch_location($bv["storage_id"], $bv["location_id"]);
						}

						if( (count($batch) > 0) ){
							$storage_assignment[$sa_k]["batch"] = $batch;
						}
						
					}

					if( (count($storage_assignment) > 0) ){
						$arr_collect_product["storage_assignment"] = $storage_assignment;
					}
					


					$discrepancy = $this->receiving_model->get_discrepancy(" WHERE product_inventory_qty_id = ".$get_selected_product_inventory[$key_consignee]["product_inventory_id"]);
					if( (count($discrepancy) > 0) ){
						$arr_collect_product["discrepancy"] =  $discrepancy[0];
					}

					array_push($arr_collect_product_inventory, $arr_collect_product);

				}
				$receiving_number = $this->transform_to_receiving_number($get_select_receiving_log[0]["receiving_number"]);
				$arr_collect_data = array(
					"receiving_id"=>$get_select_receiving_log[0]["receiving_id"],
					"receiving_number"=>$receiving_number,
					"purchase_order"=>$get_select_receiving_log[0]["purchase_order"],
					"origin"=>"Truck",
					"date_issued"=>$new_datetime,
					"status"=>$status,
					"status_id"=>$statusId,
					"vessel_name"=>$get_select_receiving_log[0]["vessel_name"],
					"trucking"=>$get_select_receiving_log[0]["trucking"],
					"driver_name"=>$get_select_receiving_log[0]["driver_name"],
					"license_name"=>$get_select_receiving_log[0]["license_name"],
					"plate_number"=>$get_select_receiving_log[0]["plate_number"],
					"product_info"=>$arr_collect_product_inventory,
					"receiving_notes"=>$get_select_receiving_notes,
					"receiving_documents"=>$get_select_receiving_documents


				);
			}

			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $arr_collect_data;
			echo json_encode($response);

			

			


		}
	}

	public function get_receiving_consignee_truck_complete()
	{
		$receiving_id = $this->input->post('receiving_id');

		$this->form_validation->set_rules("receiving_id", "Receiving ID", "trim|required");

		if($this->form_validation->run() === FALSE){
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{
			$get_select_receiving_log = $this->receiving_model->get_complete_receiving_log_truck($receiving_id);
			$get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);
			$get_select_receiving_documents = $this->receiving_model->get_documents($receiving_id);
			$status = "";
			$arr_collect_product_inventory = array();
			$arr_collect_data = array();
			$statusId = 0;

			foreach ($get_select_receiving_documents as $key => $value) {
				$get_time = strtotime($get_select_receiving_documents[$key]["date_added"]);
				$new_date = date('F d, Y h:i A', $get_time);
				$get_select_receiving_documents[$key]["date_added_formatted"] = $new_date;
			}

			foreach ($get_select_receiving_log as $key => $value) {

				$buildNewDate = strtotime($get_select_receiving_log[0]["date_created"]);
				$new_date = date('F d, Y', $buildNewDate);
				$new_time = date('h:i A', $buildNewDate);
				$new_datetime = $new_date." | ".$new_time;


				if($get_select_receiving_log[0]["status"] == 0)
				{
					$status = "Ongoing";
					$statusId = 0;
				}elseif($get_select_receiving_log[0]["status"] == 1){
					$status = "Complete";
					$statusId = 1;
				}elseif($get_select_receiving_log[0]["status"] == 2){
					$status = "Ongoing";
					$statusId = 2;
				}

				$get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);
				$get_selected_product_inventory = $this->receiving_model->get_selected_product_inventory($receiving_id);
				foreach ($get_selected_product_inventory as $key_consignee => $value_consignee) {
					$get_selected_consignee_map = $this->receiving_model->get_selected_consignee_map($get_selected_product_inventory[$key_consignee]["product_id"], $receiving_id);
					
					$arr_collect_product = array(
							"product_id"=>$get_selected_product_inventory[$key_consignee]["product_id"],
							"product_inventory_id"=>$get_selected_product_inventory[$key_consignee]["product_inventory_id"],
							"product_name"=>$get_selected_product_inventory[$key_consignee]["name"],
							"sku"=>$get_selected_product_inventory[$key_consignee]["sku"],
							"product_image"=>$get_selected_product_inventory[$key_consignee]["image"],
							"qty"=>$get_selected_product_inventory[$key_consignee]["qty"],
							"loading_method"=>$get_selected_product_inventory[$key_consignee]["loading_method"],
							"vendor_name"=>$get_selected_product_inventory[$key_consignee]["vendor_name"],
							"measures_name"=>$get_selected_product_inventory[$key_consignee]["measures_name"],
							"unit_of_measure_id"=>$get_selected_product_inventory[$key_consignee]["unit_of_measure_id"],
							"bag"=>$get_selected_product_inventory[$key_consignee]["bag"],
							"consignee_info"=>$get_selected_consignee_map
						);

					$storage_assignment = $this->receiving_model->get_storage_assignment(" WHERE receiving_log_id = ".$receiving_id." AND product_inventory_qty_id = ".$get_selected_product_inventory[$key_consignee]["product_inventory_id"]." ");
					foreach ($storage_assignment as $sa_k => $sa_v) {
						$batch = $this->receiving_model->get_batch(" WHERE storage_assignment_id = ".$sa_v['id']." ");
						foreach ($batch as $bk => $bv) {
							$batch[$bk]["location"] = $this->get_hierarchy_of_batch_location($bv["storage_id"], $bv["location_id"]);
						}

						if( (count($batch) > 0) ){
							$storage_assignment[$sa_k]["batch"] = $batch;
						}
						
					}

					if( (count($storage_assignment) > 0) ){
						$arr_collect_product["storage_assignment"] = $storage_assignment;
					}
					


					$discrepancy = $this->receiving_model->get_discrepancy(" WHERE product_inventory_qty_id = ".$get_selected_product_inventory[$key_consignee]["product_inventory_id"]);
					if( (count($discrepancy) > 0) ){
						$arr_collect_product["discrepancy"] =  $discrepancy[0];
					}

					array_push($arr_collect_product_inventory, $arr_collect_product);

				}
				$receiving_number = $this->transform_to_receiving_number($get_select_receiving_log[0]["receiving_number"]);
				$arr_collect_data = array(
					"receiving_id"=>$get_select_receiving_log[0]["receiving_id"],
					"receiving_number"=>$receiving_number,
					"purchase_order"=>$get_select_receiving_log[0]["purchase_order"],
					"date_issued"=>$new_datetime,
					"status"=>$status,
					"status_id"=>$statusId,
					"vessel_name"=>$get_select_receiving_log[0]["vessel_name"],
					"trucking"=>$get_select_receiving_log[0]["trucking"],
					"driver_name"=>$get_select_receiving_log[0]["driver_name"],
					"license_name"=>$get_select_receiving_log[0]["license_name"],
					"plate_number"=>$get_select_receiving_log[0]["plate_number"],
					"tare_in"=>$get_select_receiving_log[0]["tare_in"],
					"tare_out"=>$get_select_receiving_log[0]["tare_out"],
					"net_weight"=>$get_select_receiving_log[0]["net_weight"],
					"product_info"=>$arr_collect_product_inventory,
					"receiving_notes"=>$get_select_receiving_notes,
					"receiving_documents"=>$get_select_receiving_documents


				);
			}

			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $arr_collect_data;
			echo json_encode($response);

			

			


		}
	}


	public function save_update_product_per_item()
	{
		$receiving_id = $this->input->post('receiving_id');
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules("receiving_id", "receiving ID", "required");
		$this->form_validation->set_rules("data", "Data", "required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{


			$this->update_consignee_product_inventory($data);

			if(count($data["consignee_info"]) > 0)
			{
				$result_check = $this->receiving_model->remove_product_consignee_map($receiving_id, $data["product_id"]);
				foreach ($data["consignee_info"] as $key => $value) {

					

						$set_add_product_consignee_map = array(
							"receiving_log_id"=>$receiving_id,
							"consignee_id"=>$value["consignee_id"],
							"product_id"=>$data["product_id"],
							"distribution_qty"=>$value["distribution_qty"],
							"bag"=>$value["bag"],
							"unit_of_measure_id"=>$value["unit_of_measure_id"]
						);

						$this->receiving_model->add_product_consignee_map($set_add_product_consignee_map);

					
				}

				
 			}



			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = "Successfully Updated";
			echo json_encode($response);
		}

	}

	public function update_consignee_product_inventory($data)
	{
		// foreach ($data as $key => $value) {
		
			$product_inventory_id = $data["product_inventory_id"];


				$set_consignee_product_inventry = array(
					"qty"=>$data["qty"],
					"loading_method"=>$data["loading_method"],
					"unit_of_measure_id"=>$data["unit_of_measure_id"],
					"bag"=>$data["bag"]
				);

				$this->receiving_model->update_receiving_consignee_product_item($product_inventory_id, $set_consignee_product_inventry);
		// }
	}

	public function add_selected_receiving_notes()
	{
		$note_information = json_decode($this->input->post('note_information'), true);
		$arr_new_notes = array();

		$this->form_validation->set_rules("note_information", "Notes Information", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{

			$receiving_id =$note_information[0]["receiving_id"];
			$this->save_receiving_notes($note_information, $receiving_id);

			$get_select_receiving_notes = $this->receiving_model->get_select_receiving_notes($receiving_id);

			foreach ($get_select_receiving_notes as $key => $value) {


				$buildNewDate = strtotime($get_select_receiving_notes[$key]["date_created"]);
				$new_date = date('d-M-Y h:i A', $buildNewDate);

				$arr_build_data = array(
						"subject"=>$get_select_receiving_notes[$key]["subject"],
						"date_created"=>$new_date,
						"note"=>$get_select_receiving_notes[$key]["note"]
					);

				array_push($arr_new_notes, $arr_build_data);
			}

			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $arr_new_notes;
			echo json_encode($response);

				



		}
	}

	public function save_consignee_ongoing_truck_tare()
	{
		$receiving_id = $this->input->post('receiving_id');
		$data = json_decode($this->input->post('data'), true);

		$this->form_validation->set_rules("receiving_id", "Receiving ID", "required");
		$this->form_validation->set_rules("data", "Consignee Truck Data", "trim|required");

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{

			$date_received = date("Y-m-d H:i:s");
			$update_truck = array(
				"date_received"=>$date_received,
				"status"=>1
				);

			$this->receiving_model->update_receiving_logs_for_truck($receiving_id, $update_truck);

			$result = $this->receiving_model->save_consignee_ongoing_truck_tare($receiving_id, $data);

			$get_for_inventory = $this->receiving_model->collect_for_inventory($receiving_id);

			foreach ($get_for_inventory as $key => $value) {
				$data_intv = array(
					"quantity_received"=>$value["received_quantity"],
					"unit_of_measure_id"=>$value["unit_of_measure_id"],
					"date_created"=>$date_received,
					"status"=>0,
					"product_id"=>$value["product_id"],
					"last_update_date"=>$date_received
					);

				$this->receiving_model->save_to_product_inventory($data_intv);
			}


			header("Content-Type: application/json");

			$response["status"] = true;
			$response["message"] = "Success";
			$response["data"] = $result;
			echo json_encode($response);
		}
	}






	// ==========================  END CONSIGNEE GOODS =============================//

	public function get_current_date_time()
	{
		header("Content-Type: application/json");
		$date = date("Y-m-d H:i:s");
		$response["status"] = true;
		$response["message"] = "Success";
		$response["data"] = array(
			"date" => $date,
			"date_formatted" => date("j-M-Y g:i A", strtotime($date)),
			"date_no_time" => date("j-M-Y", strtotime($date)),
			"date_formatted_2" => date("M j, Y", strtotime($date))
		);
		echo json_encode($response);
	}


	public function upload_document(){

		$receiving_id = $this->input->post("receiving_id");
		$name = $this->input->post("name");
		$document_name = "";

		$temporary = $this->input->post("temporary");

		if(strlen($name) > 0){
			$document_name = $name;
		}

		$response = array();
		$arr_response = array();
		$status = false;
		$message = "Fail";
		if(isset($_FILES["attachment"])){
			$name = $_FILES["attachment"]["name"];
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 20; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    
		    $file_name = $randomString . "." .$ext;

		    move_uploaded_file($_FILES["attachment"]["tmp_name"], UPLOAD_DOCUMENT_PATH.$file_name);

		    $arr_response["path"] = UPLOAD_DOCUMENT_PATH.$file_name;
		    $arr_response["location"] = BASEURL . "uploads/documents/" . $file_name;
		    $status = true;
		    $message = "Succes";

		    if(strlen($name) == 0){
				$document_name = $file_name;
			}

		    if($temporary != 'true'){
		    	$data_docs = array(
			    	"document_name" => $document_name,
			    	"document_path" => $arr_response["location"],
			    	"date_added" => date("Y-m-d H:i:s"),
			    	"receiving_log_id" => $receiving_id,
			    	"status" => 0,
			    	"version" => 1
			    );
		    	$docs_id = $this->receiving_model->add_document($data_docs);
		    	$arr_response = $this->get_single_company_goods($receiving_id);
		    }else{
		    	$data_docs = array(
			    	"document_name" => $document_name,
			    	"date_added_formatted" =>  date("j-M-Y g:i A", strtotime( date("Y-m-d H:i:s") )),
			    	"document_path" => $arr_response["location"],
			    	"date_added" => date("Y-m-d H:i:s"),
			    	"status" => 0,
			    	"version" => 1
			    );

		    	header("Content-Type: application/json");
			    $arr_response[0]["documents"] = array();
			    $arr_response[0]["documents"][] = $data_docs;
			    unset($arr_response["location"]);
		    	unset($arr_response["path"]);
		    }
		}

		header("Content-Type: application/json");
		$response["status"] = $status;
		$response["message"] = $message;
		$response["data"] = $arr_response;
		echo json_encode($response);
	}

	public function upload_document_consignee(){

		$receiving_id = $this->input->post("receiving_id");
		$name = $this->input->post("name");
		$document_name = "";

		$temporary = $this->input->post("temporary");

		if(strlen($name) > 0){
			$document_name = $name;
		}

		$response = array();
		$arr_response = array();
		$status = false;
		$message = "Fail";
		if(isset($_FILES["attachment"])){
			$name = $_FILES["attachment"]["name"];
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 20; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    
		    $file_name = $randomString . "." .$ext;

		    move_uploaded_file($_FILES["attachment"]["tmp_name"], UPLOAD_DOCUMENT_PATH.$file_name);

		    $arr_response["path"] = UPLOAD_DOCUMENT_PATH.$file_name;
		    $arr_response["location"] = BASEURL . "uploads/documents/" . $file_name;
		    $status = true;
		    $message = "Succes";

		    if(strlen($name) == 0){
				$document_name = $file_name;
			}

		    if($temporary != 'true'){
		    	$data_docs = array(
			    	"document_name" => $document_name,
			    	"document_path" => $arr_response["location"],
			    	"date_added" => date("Y-m-d H:i:s"),
			    	"receiving_log_id" => $receiving_id,
			    	"status" => 0,
			    	"version" => 1
			    );
		    	$docs_id = $this->receiving_model->add_document($data_docs);
		    	// $arr_response = $this->get_single_company_goods($receiving_id);
		    	$get_select_receiving_documents = $this->receiving_model->get_documents($receiving_id);

		    	foreach ($get_select_receiving_documents as $key => $value) {
					$get_time = strtotime($get_select_receiving_documents[$key]["date_added"]);
					$new_date = date('F d, Y h:i A', $get_time);
					$get_select_receiving_documents[$key]["date_added_formatted"] = $new_date;
				}


		    	header("Content-Type: application/json");
			    $arr_response[0]["documents"] = array();
			    $arr_response[0]["documents"] = $get_select_receiving_documents;
			    unset($arr_response["location"]);
		    	unset($arr_response["path"]);
		    }else{
		    	$data_docs = array(
			    	"document_name" => $document_name,
			    	"date_added_formatted" =>  date("j-M-Y g:i A", strtotime( date("Y-m-d H:i:s") )),
			    	"document_path" => $arr_response["location"],
			    	"date_added" => date("Y-m-d H:i:s"),
			    	"status" => 0,
			    	"version" => 1
			    );

		    	header("Content-Type: application/json");
			    $arr_response[0]["documents"] = array();
			    $arr_response[0]["documents"][] = $data_docs;
			    unset($arr_response["location"]);
		    	unset($arr_response["path"]);
		    }
		}

		header("Content-Type: application/json");
		$response["status"] = $status;
		$response["message"] = $message;
		$response["data"] = $arr_response;
		echo json_encode($response);
	}

	public function complete_product_details()
	{
		$receiving_id = $this->input->post("receiving_id");
		$receiving_record = $this->input->post("receiving_record");

		$this->form_validation->set_rules("receiving_id", "Receiving ID", "trim|required");
		$this->form_validation->set_rules("receiving_record", "Product Details", "trim|required");

		$response = array();
		$store_assign_id = array();

		$item_id = "";

		if($this->form_validation->run() === FALSE){
			header("Content-Type: application/json");
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}else{

			$records = json_decode($receiving_record, true);
			// echo "<pre>";
			// print_r($records);
			// echo "</pre>";
			// exit();

			foreach ($records as $key => $value) {
				$batch = $value["batch"];
				$total_batch_amount = 0;
				// $total_bag_amount = 0;
				$prod_inv_qty_id = $value["receiving_item_record_id"];
				$item_id = $prod_inv_qty_id;



				foreach ($batch as $bk => $bv) {
					$batch_amount = floatval($bv["batch_amount"]);
					$total_batch_amount = $total_batch_amount + $batch_amount;


					// $bag_amount = floatval($bv["bag_amount"])
					// $total_bag_amount +=$bag_amount
				}


				$check_storage = $this->receiving_model->check_storage_assignment($receiving_id, $prod_inv_qty_id);
				

				if($check_storage == 0)
				{
					/*STEP 1 create storage assignment*/
					$store_assign_data = array(
						"receiving_log_id" => $receiving_id,
						"product_inventory_qty_id" => $prod_inv_qty_id,
						"total" => $total_batch_amount,
						"is_deleted" => 0
					);
					$store_id = $this->receiving_model->create_storage_assignment($store_assign_data);
					$store_assign_id["id"] = $store_id;
				}else{
					/*STEP 1 Update storage assignment*/
					$store_assign_data = array(
						"total" => $total_batch_amount,
						"is_deleted" => 0
					);
					$this->receiving_model->update_storage_assignment($receiving_id, $prod_inv_qty_id, $store_assign_data);
					$store_id = $this->receiving_model->get_storage_assignment_id($receiving_id, $prod_inv_qty_id);
					$store_assign_id["id"] = $store_id[0]["id"];
				}

				

				/*STEP 2 create batch records */

				$this->receiving_model->delete_batch($store_assign_id["id"]);

				foreach ($batch as $bk => $bv) {

					$bag_amount = 0;

					if($bv["bag_amount"] != "")
					{
						$bag_amount = $bv["bag_amount"];
					}

					$data_batch = array(
						"storage_assignment_id" => $store_assign_id["id"],
						"name" => $bv["batch_name"],
						"quantity" => $bv["batch_amount"],
						"bag_amount"=>$bag_amount,
						"unit_of_measure_id" => $bv["uom_id"],
						"location_id" => $bv["location"]["id"],
						"storage_id" => $bv["location"]["storage_id"],
						"is_deleted" => 0
					);

					$batch_id = $this->receiving_model->create_batch($data_batch);
				}


				$check_discrepancy = $this->receiving_model->check_discrepancy($prod_inv_qty_id);

				if($check_discrepancy == 0)
				{
					/* STEP 3 save discrepancy record */

					$discrepancy_type = 0;
					if($value["discrepancy_type"] == "up"){
						$discrepancy_type = 1;
					}else if($value["discrepancy_type"] == "down"){
						$discrepancy_type = 2;
					}

					$discrepancy_data = array(
						"product_inventory_qty_id" => $prod_inv_qty_id,
						"discrepancy_quantity" => $value['discrepancy_value'],
						"received_quantity" => $value['received_quantity'],
						"type" => $discrepancy_type,
						"cause" => $value["discrepancy_cause"],
						"remarks" => $value["discrepancy_reason"],
						"is_deleted" => 0
					);

					$discrepancy_id = $this->receiving_model->create_discrepancy_record($discrepancy_data);
				}else{

					$discrepancy_type = 0;
					if($value["discrepancy_type"] == "up"){
						$discrepancy_type = 1;
					}else if($value["discrepancy_type"] == "down"){
						$discrepancy_type = 2;
					}
					$discrepancy_data = array(
						"discrepancy_quantity" => $value['discrepancy_value'],
						"received_quantity" => $value['received_quantity'],
						"type" => $discrepancy_type,
						"cause" => $value["discrepancy_cause"],
						"remarks" => $value["discrepancy_reason"],
						"is_deleted" => 0
					);

					$this->receiving_model->update_discrepancy_record($prod_inv_qty_id, $discrepancy_data);



				}


				

			}

			/*  update receiving Record
			$receiving_data = array("status" => 2);
			$query = " id = ".$receiving_id;
			$this->receiving_model->edit($query, $receiving_data);
			*/

			$new_record = $this->get_single_company_goods($receiving_id);

			header("Content-Type: application/json");
			$response["status"] = true;
			$response["message"] = "update success";
			$response["data"] = $new_record;

			header("Content-Type: application/json");
			echo json_encode($response);
		}
	}

	public function update_receiving_item()
	{
		$status = true; $message = ""; $arr_response = array(); $response = array();
		$receiving_id = $this->input->post("receiving_id");
		$item_data = $this->input->post("receiving_item_data");
		$item = json_decode($item_data, true);

		$this->load->model("receiving_model");
		$this->load->model('Products/products_model');
		$measure_piece_data = $this->products_model->get_unit_of_measure(" WHERE name = 'pc' ");

		$data = array(
			"loading_method" => $item["item_loading_method"],
			"qty" => 0
		);

		$id = $item["record_id"];
		$data["qty"] = $item["item_qty_unit_of_measure"];
		if($item["item_loading_method"] == "piece"){
			$data["qty"] = $item["item_qty_by_piece"];
			$data["unit_of_measure_id"] = $measure_piece_data[0]["id"];
		}else{
			$data["unit_of_measure_id"] = $item["item_unit_of_measure"];
		}

		$this->receiving_model->update_product_inventory_qty($id, $data);

		$new_record = $this->get_single_company_goods($receiving_id);
		$arr_response = $new_record;

		header("Content-Type: application/json");
		$response["status"] = $status;
		$response["message"] = $message;
		$response["data"] = $arr_response;
		echo json_encode($response);
	}

	public function update_product_details()
	{
		$status = true; $message = ""; $arr_response = array(); $response = array();
		$receiving_id = $this->input->post("receiving_id");
		$receiving_product_details = $this->input->post("receiving_product_details");
		$item = json_decode($receiving_product_details, true);

		$this->load->model("receiving_model");

		$storage_assignment = $item["storage_assignment"];
		$storage_assignment_id = $storage_assignment["id"];

		$product_inventory_qty_id = $storage_assignment["product_inventory_qty_id"];

		$store_assign_data = array(
			"is_deleted" => 1
		);
		$this->receiving_model->update_storage_assignment($receiving_id, $product_inventory_qty_id, $store_assign_data);

		$this->receiving_model->delete_batch($storage_assignment_id);

		$total_batch_amount = 0;

		$batch = $item["batch"];
		foreach ($batch as $bk => $bv) {
			$batch_amount = floatval($bv["batch_amount"]);
			$total_batch_amount = $total_batch_amount + $batch_amount;
		}

		$store_assign_data = array(
			"receiving_log_id" => $receiving_id,
			"product_inventory_qty_id" => $product_inventory_qty_id,
			"total" => $total_batch_amount,
			"is_deleted" => 0
		);
		$store_id = $this->receiving_model->create_storage_assignment($store_assign_data);

		foreach ($batch as $bk => $bv) {
			$batch_data = array(
				"storage_assignment_id" => $store_id,
				"name" => $bv["batch_name"],
				"quantity" => $bv["batch_amount"],
				"unit_of_measure_id" => $bv["uom_id"],
				"location_id" => $bv["location"]["id"],
				"storage_id" => $bv["location"]["storage_id"],
				"is_deleted" => 0
			);
			$this->receiving_model->create_batch($batch_data);
		}

		$discrepancy_type = 0;
		if($item["discrepancy_type"] == "up"){
			$discrepancy_type = 1;
		}else if($item["discrepancy_type"] == "down"){
			$discrepancy_type = 2;
		}

		$discrepancy_data = array(
			"discrepancy_quantity" => $item['discrepancy'],
			"received_quantity" => $item['receiving_qty'],
			"type" => $discrepancy_type,
			"cause" => $item["cause"],
			"remarks" => $item["remarks"],
			"is_deleted" => 0
		);
		$this->receiving_model->update_discrepancy_record($product_inventory_qty_id, $discrepancy_data);

		$new_record = $this->get_single_company_goods($receiving_id);
		$arr_response = $new_record;

		header("Content-Type: application/json");
		$response["status"] = $status;
		$response["message"] = $message;
		$response["data"] = $arr_response;
		echo json_encode($response);
	}

	public function update_products_and_record()
	{
		$response = array(
			"status" => true,
			"message" => "",
			"data" => array()
		);

		$receiving_id = $this->input->post("receiving_id");
		$info_string = $this->input->post("info");
		$deleted_items_string = $this->input->post("deleted_items");
		$added_items_string = $this->input->post("added_items");

		$info = json_decode($info_string, true);
		$deleted_items = json_decode($deleted_items_string, true);
		$added_items = json_decode($added_items_string, true);

		$receiving_log = $this->receiving_model->get_receiving_log(" WHERE id = ".$receiving_id);
		$invoice_data = $this->receiving_model->get_invoice(" WHERE number = '".$info["invoice_number"]."'");
		$officer_data = $this->receiving_model->get_officer(" WHERE name = '".$info["officer"]."'");
		$courier_data = $this->receiving_model->get_courier(" WHERE name = '".$info["courier"]."'");

		$invoice_id = $receiving_log[0]["invoice_id"];
		$officer_id = $receiving_log[0]["officer_id"];
		$courier_id = $receiving_log[0]["courier_id"];

		if(count($invoice_data) == 0){
			$invoice_id = $this->receiving_model->add_invoice(array(
				"number" => $info["invoice_number"],
				"date" => $info["invoice_date"]." ".date("H:i:s")
			));
		}else{
			$invoice_id = $invoice_data[0]["id"];
			$this->receiving_model->update_invoice($invoice_id, array(
				"date" => $info["invoice_date"]." ".date("H:i:s")
			));
		}

		if(count($officer_data) == 0){
			$officer_id = $this->receiving_model->add_officer(array(
				"name" => $info['officer']
			));
		}else{
			$officer_id = $officer_data[0]["id"];
		}

		if(count($courier_data) == 0){
			$courier_id = $this->receiving_model->add_courier(array(
				"name" => $info['courier']
			));
		}else{
			$courier_id = $courier_data[0]["id"];
		}

		$receiving_save_log_data = array(
			"purchase_order" => $info["po_number"],
			"invoice_id" => $invoice_id,
			"officer_id" => $officer_id,
			"courier_id" => $courier_id
		);

		$this->receiving_model->edit(array("id" => intval($receiving_id)), $receiving_save_log_data);

		foreach ($deleted_items as $k => $v) {
			$this->receiving_model->update_product_inventory_qty($v["id"], array("is_deleted" => 1));
		}

		foreach ($added_items as $k => $v) {
			$vendor_id = 0;
			$vendor_fetched_data = $this->receiving_model->get_vendor(strtolower($v["vendor"]));
			 
			if(count($vendor_fetched_data) > 0){
				$vendor_id = $vendor[0]["id"];
			}else{
				$vendor_id = $this->receiving_model->add_vendor(array(
					"name" => strtolower($v["vendor"])
				));
			}

			$this->receiving_model->add_receiving_item(array(
				"qty" => $v["qty"],
				"product_receiving_id" => $receiving_id,
				"product_id" => $v["item_id"],
				"loading_method" => $v["loading_method"],
				"vendor_id" => $vendor_id,
				"unit_of_measure_id" => $v["unit_of_measure_id"],
				"bag" => 0
			));
		}


		header("Content-Type: application/json");
		echo json_encode($response);
	}

	public function complete_record()
	{
		$response = array(
			"status" => true,
			"message" => "",
			"data" => array()
		);

		$receiving_id = $this->input->post("receiving_id");
		$receiving_data = $this->get_single_company_goods($receiving_id);

		$this->load->model('Inventory/Inventory_model');

		$data = $receiving_data[0];
		$items = $data["items"];

		$receiving_id = intval($receiving_id);

		$this->receiving_model->edit(array("id" => $receiving_id), array("status" => 1));

		foreach ($items as $k => $v) {
			$discrepancy = $v["discrepancy"];
			$product_id = $v["product_id"];
			$received_quantity = $discrepancy["received_quantity"];
			$unit_of_measure_id = $v["unit_of_measure_id"];

			$inventory_data = array(
				"quantity_received" => $received_quantity,
				"unit_of_measure_id" => $unit_of_measure_id,
				"date_created" => date("Y-m-d H:i:s"),
				"status" => 0,
				"product_id" => $product_id,
				"last_update_date" => date("Y-m-d H:i:s")
			);

			$this->Inventory_model->add_product_inventory($inventory_data);
		}

		header("Content-Type: application/json");
		echo json_encode($response);
	}


	//==================================================//
	//=============END OF PUBLIC FUNCTIONS==============//
	//==================================================//

}

/* End of file Receiving.php */
/* Location: ./application/modules/receiving/controllers/Receiving.php */