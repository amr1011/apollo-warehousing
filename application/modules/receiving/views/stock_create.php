
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL; ?>receiving/company_goods">Receiving - Company Goods </a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color">Create Record</li>
				</ul>
			</div>
			<div class="semi-main">

				<div class="padding-top-30 margin-bottom-30 text-right width-100percent">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color cancel-go-back">Cancel</button>
					<button class="btn general-btn modal-trigger" modal-target="confirm-create-record">Create Record</button>
				</div>
				<div class="clear"></div>
				
				<form id="submitReceiving">

				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0">
					<p class="font-20 font-bold black-color padding-bottom-20">Receiving No. <span class="receiving-number"><?php echo $receiving_number; ?></span></p>
					<div class="width-50percent display-inline-top">
						<div class="padding-bottom-10">
							<p class="font-14 font-bold display-inline-mid width-120">PO No.: </p>
							<input id="stock_create_po_number" type="text" class="display-inline-mid t-medium margin-left-20">
						</div>

						<div class="padding-top-10 padding-bottom-10">
							<p class="font-14 font-bold display-inline-top width-120 margin-top-10">Purchase Officer: </p>
							<input id="stock_create_officer" datavalid="required" labelinput="Purchase Officer" type="text" class="display-inline-mid t-medium margin-left-20" >
						</div>

						<!-- <div class="padding-top-10 padding-bottom-10">
							<p class="font-14 font-bold display-inline-top width-120 margin-top-10">Origin: </p>
							<input id="stock_create_origin" type="text" class="display-inline-mid t-medium margin-left-20">
						</div> -->

						<div class="padding-top-10 padding-bottom-10">
							<p class="font-14 font-bold display-inline-top width-120 margin-top-10">Courier Name: </p>
							<input id="stock_create_courier" datavalid="required" labelinput="Courier Name" type="text" class="display-inline-mid t-medium margin-left-20">
						</div>

					</div>

					<div class="width-50percent display-inline-top">
						<div class="padding-bottom-10">
							<p class="font-14 font-bold display-inline-mid width-150 padding-left-20">Invoice No.: </p>
							<input id="stock_create_invoice_number" datavalid="required" labelinput="Invoice Number" type="text" class="display-inline-mid  t-medium">
						</div>

						<div class="padding-top-10">
							<p class="font-14 font-bold display-inline-mid width-150 padding-left-20">Invoice Date: </p>
							<div class="input-group italic display-inline-mid fixed-datepicker medium">
								<input id="stock_create_invoice_date" datavalid="required" labelinput="Invoice Date" class="form-control dp  default-cursor " placeholder="Invoice Date" data-date-format="MM/DD/YYYY" type="text">
								<span class="input-group-addon "><i class="fa fa-calendar"></i></span>
							</div>
						
						</div>
					</div>
				</div>

				</form>

				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white padding-bottom-20 " id="productList">
					<div class="width-100percent text-left padding-all-30 no-padding-top no-padding-left margin-bottom-30 border-bottom-small border-gray">
						<p class="font-20 font-bold black-color padding-bottom-20 ">Item List</p>
						<p class="font-14 font-bold display-inline-mid padding-right-20">Item: <span class="red-color display-inline-top">*</span></p>
						<div class="select dispaly-inline-mid large">
							<select id="selectItem">
							</select>
						</div>
						<a href="javascript:void(0)" class="display-inline-mid padding-left-20">
							<button class="btn general-btn padding-left-20 padding-right-20" id="addItem">Add Item</button>
						</a>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400 ">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title padding-top-10 font-500 font-20 black-color"> Documents</h4>
							</a>

							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="upload-documents">Upload</button>
							</div>
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-20">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>
								<span id="displayDocuments"></span>				
							</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-20 black-color padding-top-10"> Notes</h4>
							</a>
							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="add-notes">Add Notes</button>
							</div>
							<div class="clear"></div>
						</div>
						
					
						
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-20" id="noteContainer">
						
							</div>
						</div>
					</div>
				</div>
				<div class="width-100percent text-right border-top-small border-gray padding-top-10">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color cancel-go-back">Cancel</button>
					<button class="btn general-btn modal-trigger" modal-target="confirm-create-record">Create Record</button>
				</div>
		</div>
	</div>

	<!--MODALS-->

	<!--Start of Confirm Create Record MODAL-->
	<div class="modal-container" modal-id="created-record">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Create Record</h4>				
				<!-- <div class="modal-close close-me"></div> -->
			</div>

			<!-- content -->
			<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
				<div class="padding-all-10 bggray-7cace5">
					<p class="font-14 font-400 white-color message"></p>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<a href="javascript:void(0)">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" >Show Record</button>
				</a>
			</div>
		</div>	
	</div>
	<!--End of Confirm Create Record MODAL-->

	<!--Start of Confirm Create Record MODAL-->
	<div class="modal-container" modal-id="confirm-create-record">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Create Record</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<p class="font-14 font-400">Are you sure you want to create Receiving No. 1234567890?</p>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<!-- <button id="submit_create_record" type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 close-me modal-trigger" modal-target="created-record">Confirm</button> -->
				<button id="submit_create_record" type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Confirm Create Record MODAL-->

	<!--Start of Redirect to Create Product MODAL-->
	<div class="modal-container" modal-id="redirect-product-management">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Message</h4>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<p class="font-20 font-400" style="line-height:18px;">There are no items added yet, redirecting you to Item management please wait..</p>
			</div>
		
			<div class="modal-footer text-right" style="height:50px;">
				 
			</div>
		</div>	
	</div>
	<!--End of Redirect to Create Product MODAL-->

	

	<!--Start of Upload Document MODAL-->
	<div class="modal-container" modal-id="upload-document">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Upload Document</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">Dcoument Name:</p>
					<input type="text" class="width-231px display-inline-mid">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">File Location:</p>
					<input type="text" class="width-231px display-inline-mid">
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Browse</button>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Upload Document MODAL-->

	<!--Start of Add Notes MODAL-->
	<div class="modal-container" modal-id="add-notes">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Add Notes</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
					<input type="text" class="width-380px display-inline-mid" id="noteSubject">
				</div>
				<div class="text-left padding-bottom-10">
					<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
					<textarea class="width-380px margin-left-10 height-250px" id="noteMessage"></textarea>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" id="confirmNote">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Notes MODAL-->

	<!--Start of Add Batch MODAL-->
	<div class="modal-container" modal-id="add-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Add Batch</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Item:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Holcim Cement</p>
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Transfer Balance:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per margin-top-15">Qty to Transfer:</p>
						<input type="text" class="t-medium no-margin-left">
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per">Batch Name: </p>
						<div class="select width-250px">
							<select>
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div>
					<div class="padding-top-30">
						<p class="font-14 font-500 no-margin-left">Origin</p>
						<p class="font-14 font-500 display-inline-mid width-25per margin-top-15 no-margin-right">Storage Location:</p>
						<div class="select width-250px">
							<select>
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div>
						
					</div>
				</div>
				<p class="font-14 font-500">Sub Location:</p>
				<div class="width-100percent bggray-white box-shadow-dark">

					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-75percent">Location Address</th>
								<th class="black-color width-25percent">Location Qty</th>
							</tr>
						</thead>
					</table>
					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22"></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>
				</div>

				<p class="font-14 font-500 no-margin-left margin-top-20">Destination</p>
				<p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>

				<div class="bggray-white height-300px overflow-y-auto">
					<div class="padding-all-10 padding-bottom-10 border-bottom-small border-black">
						<i class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small  padding-right-10 border-black "></i>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5">Warehouse 1</p>
						<span class="display-inline-mid padding-all-5">&gt;</span>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid">Area 1</p>
					</div>
					<div class="font-0">
						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor ">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 1</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 2</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 3</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 4</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 5</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 6</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 7</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 8</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 9</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 10</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 11</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 12</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 13</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 14</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

					</div>
				</div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Batch MODAL-->

	<!-- Start of Edit Batch Modal  -->
	<div class="modal-container" modal-id="edit-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Edit Batch</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Item:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Holcim Cement</p>
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Transfer Balance:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per margin-top-15">Total Quantity to Transfer:</p>
						<input type="text" class="t-medium no-margin-left">
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per">Batch Name: </p>
						<div class="select width-250px">
							<select>
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div>
					<div class="padding-top-30">
						<p class="font-14 font-500 no-margin-left">Origin</p>
						<p class="font-14 font-500 display-inline-mid width-25per margin-top-15 no-margin-right">Storage Location:</p>
						<div class="select width-250px">
							<select>
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div>
						
					</div>
				</div>
				<p class="font-14 font-500">Sub Location:</p>
				<div class="width-100percent bggray-white box-shadow-dark">

					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-75percent">Location Address</th>
								<th class="black-color width-25percent">Location Qty</th>
							</tr>
						</thead>
					</table>
					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22"></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>
				</div>

				<p class="font-14 font-400 no-margin-left margin-top-20">Destination</p>
				<p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>

				<div class="bggray-white height-300px overflow-y-auto">
					<div class="padding-all-10 padding-bottom-10 border-bottom-small border-black">
						<i class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small  padding-right-10 border-black "></i>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5">Warehouse 1</p>
						<span class="display-inline-mid padding-all-5">&gt;</span>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid">Area 1</p>
					</div>
					<div class="font-0">
						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor ">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 1</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 2</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 3</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 4</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 5</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 6</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 7</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 8</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 9</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 10</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 11</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 12</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 13</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 14</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

					</div>
				</div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button id="" type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!-- end of edit batch modal  -->