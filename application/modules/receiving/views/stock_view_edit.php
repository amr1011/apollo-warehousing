<div class="main">
    <div class="breadcrumbs no-margin-left padding-left-50">
        <ul>
            <li>
                <a href="<?php echo BASEURL; ?>receiving/company_goods">Receiving - Company Goods</a>
            </li>
            <li>
                <span>&gt;</span>
            </li>
            <li class="font-bold black-color edit-record-no-bread">
            </li>
        </ul>
    </div>
    <div class="semi-main">
        <div class="padding-top-30 margin-bottom-30 text-right width-100percent">
            
        	<button class="font-12 btn btn-default font-12 display-inline-mid close-me red-color" id="btnCancel" type="button">Cancel</button> 
        	<button class="btn general-btn" id="btnSave">Save Changes</button>
            
        </div>
        <div class="clear"></div>
        <div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0">
            <p class="font-20 font-bold black-color padding-bottom-20 receiving-no-title">
                
            </p>
            <form id="formBasic">
            <div class="width-50percent display-inline-top">
                <div class="padding-bottom-10">
                    <p class="font-14 font-bold display-inline-mid width-120">
                        PO No.:
                    </p>
                    <input class="display-inline-mid t-medium margin-left-20"  type="text" id="poNumber" />
                </div>
                <div class="padding-top-10 padding-bottom-10">
                    <p class="font-14 font-bold display-inline-top width-120 margin-top-10">
                        Purchase Officer:
                    </p>
                    <input class="display-inline-mid t-medium margin-left-20" datavalid="required" labelinput="Officer" type="text" id="purchaseOfficer">
                </div>
                <div class="padding-top-10 padding-bottom-10">
                    <p class="font-14 font-bold display-inline-top width-120 margin-top-10">
                        Courier Name:
                    </p><input class="display-inline-mid t-medium margin-left-20" datavalid="required" labelinput="Courrier" type="text" id="courrierName">
                </div>
            </div>
            <div class="width-50percent display-inline-top">
                <div class="padding-bottom-10">
                    <p class="font-14 font-bold display-inline-mid width-150 padding-left-20">
                        Invoice No.:
                    </p><input class="display-inline-mid t-medium" type="text" datavalid="required" labelinput="Invoice Number" id="invoiceNumber" />
                </div>
                <div class="padding-top-10">
                    <p class="font-14 font-bold display-inline-mid width-150 padding-left-20">
                        Invoice Date:
                    </p>
                    <div class="input-group italic display-inline-mid fixed-datepicker medium">
                        <input class="form-control dp default-cursor" data-date-format="MM/DD/YYYY" placeholder="Transfer Date" datavalid="required" labelinput="Invoice Date" type="text" id="invoiceDate" />
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white padding-bottom-20">
            <div class="width-100percent text-left padding-all-30 no-padding-top no-padding-left margin-bottom-30 border-bottom-small border-gray">
                <p class="font-20 font-bold black-color padding-bottom-20">
                    Item List
                </p>
                <p class="font-14 font-bold display-inline-mid padding-right-20">
                    Item: <span class="red-color display-inline-top">*</span>
                </p>
                <div class="select dispaly-inline-mid large select-item">
                    <select class="transform-dd" id="selectItem"></select>
                </div> 
                <button class="btn general-btn display-inline-mid padding-left-20 padding-right-20 margin-left-10 " id="addItem">Add Item</button> 
            </div>
            <span class="exisiting-container">
	            <div class="border-full padding-all-20 existing-item-template margin-top-5 existing-item" style="display:none;">
	                <div class="bggray-white text-left">
	                    <div class="height-100percent display-inline-mid width-10per">
	                        <div class="image" style="background-position: center;background-size: cover;width: 84px;height: 93px;background-repeat: no-repeat;"></div>
	                    </div>
	                    <div class="display-inline-mid width-90per padding-left-10">
	                        <div class="padding-top-15 padding-left-15 padding-bottom-15">
	                            <p class="font-16 font-bold f-left sku-name"> </p>
	                            <div class="f-right width-20px margin-left-10">
	                                <img alt="close" class="width-100percent default-cursor remove" src="../assets//images/ui/icon-close.svg">
	                            </div>
	                            <div class="clear"></div>
	                        </div>
	                    </div>
	                </div>
	            </div>
            </span>

            <span class="item-list-container">
	            <div class="border-full padding-all-20 margin-top-20 item-template added-item" style="display:none;">
	                <div class="bggray-white">
	                    <div class="height-100percent display-inline-mid width-230px">
	                        <div class="image" style="background-position: center;background-size: cover;width: 230px;height: 255px;background-repeat: no-repeat;"></div>
	                    </div>
	                    <div class="display-inline-top width-75percent padding-left-10">
	                        <div class="padding-all-15 bg-light-gray">
	                            <p class="font-16 font-bold f-left sku-name"></p>
	                            <div class="f-right width-20px margin-left-10">
	                                <img alt="close" class="width-100percent default-cursor close" src="../assets//images/ui/icon-close.svg">
	                            </div>
	                            <div class="clear"></div>
	                        </div>
	                        <div class="padding-all-15 text-left">
	                            <p class="f-left no-margin-all width-20percent font-bold">
	                                Vendor
	                            </p>
	                            <p class="f-left no-margin-all width-20percent">
	                            	<input type="text" class="display-inline-mid width-100px vendor-name" style="padding:0px 5px" />
	                            </p>
	                            <p class="f-left no-margin-all width-25per font-bold">
	                                Qty in Inventory
	                            </p>
	                            <p class="f-left width-20percent margin-left-15 qty-inventory"> 0
	                            </p>
	                            <div class="clear"></div>
	                        </div>
	                        <div class="padding-all-15 bg-light-gray text-left font-0">
	                            <p class="display-inline-mid width-20percent font-bold">
	                                Aging Days
	                            </p>
	                            <p class="display-inline-mid width-20percent aging-days">
	                            	0
	                            </p>
	                            <p class="display-inline-mid width-25per font-14 font-bold">
	                                Loading Method
	                            </p>
	                            <div class="display-inline-mid">
	                                <input class="display-inline-mid width-50px default-cursor radio-bulk-piece" data-value="bulk" name="bulk-or-piece" checked="checked" type="radio"> <label class="display-inline-mid font-14 margin-top-5 default-cursor" for="bulk">By Bulk</label>
	                            </div>
	                            <div class="display-inline-mid margin-left-10">
	                                <input class="display-inline-mid width-50px default-cursor radio-bulk-piece" data-value="piece" name="bulk-or-piece" type="radio"> <label class="display-inline-mid font-14 margin-top-5 default-cursor" for="piece">By Piece</label>
	                            </div>
	                        </div>
	                        <div class="padding-all-15 text-left font-0">
	                            <p class="display-inline-mid width-20percent font-bold">
	                                Description
	                            </p>
	                            <p class="display-inline-mid width-20percent description">
	                            </p>
	                            <div class="display-inline-mid width-60percent">
	                                <p class="display-inline-mid font-14 font-500 padding-right-30">
	                                    Qty to Receive
	                                </p>
	                                <input class="margin-left-10 display-inline-mid t-small width-70px qty-receive" type="text">
	                                <div class="select small font-12 display-inline-mid width-70px margin-left-5 margin-right-5 select-uom-container">
	                                </div>
	                                <input class="display-inline-mid t-small width-70px qty-piece" type="text">
	                                <p class="font-14 font-400 display-inline-mid padding-left-5">
	                                    Piece
	                                </p>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
            </span>
        </div>
    </div>
</div>


<div class="modal-container" modal-id="generate-view">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>

<div class="modal-container complete-product-modal" modal-id="confirm-remove-exisiting">
    <div class="modal-body small ">
        <div class="modal-head">
            <h4 class="text-left">Remove existing item</h4>
            <div class="modal-close close-me"></div>
        </div>
        <!-- content -->
        <div class="modal-content text-left">
            <div class="padding-all-10">
                <p class="font-14 font-400">Are you sure you want to remove <span class="item-name font-16 font-bold"></span> ?</p>
            </div>
        </div>
        <div class="modal-footer text-right">   
            <button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>                 
            <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 confirm-btn">Confirm</button>                                   
        </div>
    </div>
</div>


<div class="modal-container complete-product-modal" modal-id="confirm-update">
    <div class="modal-body small ">
        <div class="modal-head">
            <h4 class="text-left">Confirmation Message</h4>
            <div class="modal-close close-me"></div>
        </div>
        <!-- content -->
        <div class="modal-content text-left">
            <div class="padding-all-10">
                <p class="font-14 font-400">Are you sure you want to  update this record ?</p>
            </div>
        </div>
        <div class="modal-footer text-right">   
            <button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>                 
            <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 confirm-btn">Confirm</button>                                   
        </div>
    </div>
</div>

<div class="modal-container" modal-id="updating-data">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color message">Updateing, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>