<div class="main">
			<div class="center-main">
				<div class="mod-select padding-left-3 padding-top-60">			
					<div class="select f-left width-450px ">
						<select>
							<option value="Displaying All Consignee Goods Receiving Records"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>						
				</div>
				<div class="f-right margin-top-10">	
					<a href="<?php echo BASEURL; ?>receiving/consignee_create">
						<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn">Create Record</button>				
					</a>
					<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn dropdown-btn">View Filter / Search</button>			
				</div>
				<div class="clear"></div>

				<div class="dropdown-search">
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<table>
						<tbody>
							<tr class="width-100percent">
								<td class="width-75px">Search</td>
								<td>
									<div class="select width-200px font-400">
										<select>
											<option value="items">Items</option>
										</select>
									</div>
								</td>
								<td class="width-100percent">
									<input type="text" />
								</td>
							</tr>
							<tr class="width-100percent">
								<td>Filter By:</td>
								<td>
									<div class="input-group width-200px f-left italic">
									<input class="form-control dp border-big-bl border-big-tl default-cursor " placeholder="Receiving Date" data-date-format="MM/DD/YYYY" type="text">
									<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar white-color">
									</span></span>
									</div>
								</td>
								<td class="search-action">
									<div class="f-left">
										
										<div class="select width-150px italic">
											<select>
												<option value="status">Status</option>
											</select>
										</div>
									</div>
									<div class="f-right margin-left-30">
										<p class="display-inline-mid">Sort By:</p>
										<div class="select width-150px display-inline-mid italic">
											<select>
												<option value="withdrawal No." >Receiving No.</option>
											</select>
										</div>
										<div class="select width-150px display-inline-mid italic">
											<select>
												<option value="ascending">Ascending</option>
												<option value="descending">Descending</option>
											</select>
										</div>
									</div>
									<div class="clear"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="f-left margin-top-10 margin-left-10">
						<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
						<p class="display-inline-mid font-4 font-bold">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 ">Search</button>
					<div class="clear"></div>
				</div>

				<div style="float: left; font-weight: bold; padding-top: 10px;"><span id="total-row-consignee"></span></div>


				<div class="width-100percent bggray-white">

					<table class="tbl-4c3h margin-top-30">
						<thead>
							<tr>
								<th class=" black-color width-10percent no-padding-left text-center">Receiving No.</th>
								<th class=" black-color width-10percent no-padding-left">PO No.</th>
								<th class=" black-color width-10percent no-padding-left">Issued Date</th>
								<th class=" black-color width-10percent no-padding-left">Origin</th>
								<th class="black-color width-30percent no-padding-left">Items</th>
								<th class=" black-color width-20percent no-padding-left">Owner</th>
								<th class=" black-color width-10percent no-padding-left">Status</th>												
							</tr>
						</thead>
					</table>

					<div id="displayAllReceivingConsigneeList">
						<!-- <div class="tbl-like hover-transfer-tbl">
					
							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="display-inline-mid text-center width-10percent text-left">
									<p class=" font-400 font-14 f-none text-center no-padding-left width-100percent ">1234567890</p>						
								</div>
								<div class="display-inline-mid text-center width-10percent">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">123456</p>	
								</div>
								<div class="display-inline-mid text-center width-10percent ">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">22-Oct-2015</p>
								</div>
								<div class="display-inline-mid text-center width-10percent ">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">Vessel</p>	
								</div>
								<div class=" display-inline-mid font-400 text-center width-30percent">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14 width-100percent">									
										<li>#12345679 - Dinorado Rice</li>
										<li>#12345679 - Brazillian Wheat</li>									
									</ul>
								</div>
								<div class="font-400 display-inline-mid text-center width-20percent ">
									<div class="owner-img width-30per display-inline-mid">
										<img src="../assets/images/profile/profile_e.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="display-inline-mid text-center width-10percent">
									<p class=" font-400 font-14 f-none width-100percent">Ongoing</p>							
								</div>
							</div>

							<div class="hover-tbl "> 
								<a href="<?php echo BASEURL; ?>receiving/consignee_ongoing_vessel">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>
								<button class="btn general-btn modal-trigger" modal-target="">Complete Product Details</button>
								
							</div>
						</div>

						<div class="tbl-like hover-transfer-tbl">
							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="display-inline-mid text-center width-10percent text-left">
									<p class=" font-400 font-14 f-none text-center no-padding-left width-100percent ">1234567890</p>						
								</div>
								<div class="display-inline-mid text-center width-10percent">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">123456</p>	
								</div>
								<div class="display-inline-mid text-center width-10percent ">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">22-Oct-2015</p>
								</div>
								<div class="display-inline-mid text-center width-10percent ">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">Vessel</p>	
								</div>
								<div class=" display-inline-mid font-400 text-center width-30percent">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14 width-100percent">									
										<li>#12345679 - Dinorado Rice</li>
										<li>#12345679 - Brazillian Wheat</li>									
									</ul>
								</div>
								<div class="font-400 display-inline-mid text-center width-20percent ">
									<div class="owner-img width-30per display-inline-mid">
										<img src="../assets/images/profile/profile_e.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="display-inline-mid text-center width-10percent">
									<p class=" font-400 font-14 f-none width-100percent">Complete</p>							
								</div>
							</div>


							<div class="hover-tbl "> 
								<a href="<?php echo BASEURL; ?>receiving/consignee_complete_vessel">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>	
							</div>
						</div> -->
						<!-- <div class="tbl-like hover-transfer-tbl">

							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="display-inline-mid text-center width-10percent text-left">
									<p class=" font-400 font-14 f-none text-center no-padding-left width-100percent ">1234567890</p>						
								</div>
								<div class="display-inline-mid text-center width-10percent">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">123456</p>	
								</div>
								<div class="display-inline-mid text-center width-10percent ">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">22-Oct-2015</p>
								</div>
								<div class="display-inline-mid text-center width-10percent ">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">Truck</p>	
								</div>
								<div class=" display-inline-mid font-400 text-center width-30percent">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14 width-100percent">									
										<li>#12345679 - Dinorado Rice</li>
										<li>#12345679 - Brazillian Wheat</li>									
									</ul>
								</div>
								<div class="font-400 display-inline-mid text-center width-20percent ">
									<div class="owner-img width-30per display-inline-mid">
										<img src="../assets/images/profile/profile_e.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="display-inline-mid text-center width-10percent">
									<p class=" font-400 font-14 f-none width-100percent">Ongoing</p>							
								</div>
							</div>


							<div class="hover-tbl "> 
								<a href="<?php echo BASEURL; ?>receiving/consignee_ongoing_truck_1">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>
								<button class="btn general-btn modal-trigger " modal-target="">Complete Product Details</button>	
							</div>

						</div>
						<div class="tbl-like hover-transfer-tbl">

							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="display-inline-mid text-center width-10percent text-left">
									<p class=" font-400 font-14 f-none text-center no-padding-left width-100percent ">1234567890</p>						
								</div>
								<div class="display-inline-mid text-center width-10percent">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">123456</p>	
								</div>
								<div class="display-inline-mid text-center width-10percent ">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">22-Oct-2015</p>
								</div>
								<div class="display-inline-mid text-center width-10percent ">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">Truck</p>	
								</div>
								<div class=" display-inline-mid font-400 text-center width-30percent">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14 width-100percent">									
										<li>#12345679 - Dinorado Rice</li>
										<li>#12345679 - Brazillian Wheat</li>									
									</ul>
								</div>
								<div class="font-400 display-inline-mid text-center width-20percent ">
									<div class="owner-img width-30per display-inline-mid">
										<img src="../assets/images/profile/profile_e.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="display-inline-mid text-center width-10percent">
									<p class=" font-400 font-14 f-none width-100percent">Complete</p>							
								</div>
							</div>


							<div class="hover-tbl "> 
								<a href="receiving-view-complete.php">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>	
							</div>

						</div>	
						<div class="tbl-like hover-transfer-tbl">

							<div class="first-tbl height-auto padding-bottom-20 padding-top-20 transfer-tbl text-left font-0">
								<div class="display-inline-mid text-center width-10percent text-left">
									<p class=" font-400 font-14 f-none text-center no-padding-left width-100percent ">1234567890</p>						
								</div>
								<div class="display-inline-mid text-center width-10percent">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">123456</p>	
								</div>
								<div class="display-inline-mid text-center width-10percent ">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">22-Oct-2015</p>
								</div>
								<div class="display-inline-mid text-center width-10percent ">
									<p class=" font-400 font-14 f-none width-100percent no-padding-left">Vessel</p>	
								</div>
								<div class=" display-inline-mid font-400 text-center width-30percent">
									<ul class="list-style-square text-left text-indent-20 padding-left-50 font-14 width-100percent">									
										<li>#12345679 - Dinorado Rice</li>
										<li>#12345679 - Brazillian Wheat</li>									
									</ul>
								</div>
								<div class="font-400 display-inline-mid text-center width-20percent ">
									<div class="owner-img width-30per display-inline-mid">
										<img src="../assets/images/profile/profile_e.png" alt="Profile Owner" class="width-60percent">
									</div>
									<p class="width-100px font-400 font-14 display-inline-mid f-none">Dwayne Garcia</p>								
								</div>
								<div class="display-inline-mid text-center width-10percent">
									<p class=" font-400 font-14 f-none width-100percent">Ongoing</p>							
								</div>
							</div>

							<div class="hover-tbl "> 
								<a href="receiving-view-ongoing.php">
									<button class="btn general-btn margin-right-10">View Record</button>
								</a>
								<div class="display-inline-mid">
									<button class="btn general-btn modal-trigger" modal-target="">Complete Product Details</button>
								</div>
							</div>
						</div> -->
					</div>

					
				</div>
			</div>
		</div>

		<div class="modal-container show-complete-record-vessel-modal" modal-id="complete-product-details">
			<div class="modal-body medium">				

				<div class="modal-head">
					<h4 class="text-left">Complete Receiving Record</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div>						
						<p class="display-inline-mid font-14 font-400 width-200px margin-top-15">Berthing Date/Time Start:</p>
						<div class="input-group width-200px italic display-inline-mid fixed-datepicker">
							<input class="form-control dp  default-cursor" id="berthing-start-date" placeholder="Berthing Date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon width-0 "><i class="fa fa-calendar"></i></span>
						</div>
						<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
							<input class="form-control dp time " placeholder="Berthing Time" id="berthing-start-time" type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>					
					</div>
					<div>						
						<p class="display-inline-mid font-14 font-400 width-200px margin-top-15">Unloading Date/Time Start:</p>
						<div class="input-group width-200px italic display-inline-mid fixed-datepicker">
							<input class="form-control dp  default-cursor trigger-compute-duration" id="unloading-start-date" placeholder="Uploading Date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon width-0 "><i class="fa fa-calendar"></i></span>
						</div>
						<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
							<input class="form-control dp time trigger-compute-duration" placeholder="Uploading Time" id="unloading-start-time" type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>					
					</div>
					<div class="margin-top-5">
						<p class="display-inline-mid font-14 font-400  width-200px margin-top-15">Unloading Date/Time End:</p>
						<div class="input-group width-200px italic display-inline-mid fixed-datepicker">
							<input class="form-control dp  default-cursor trigger-compute-duration" placeholder="Unloading Date" id="unloading-end-date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon width-0 "><i class="fa fa-calendar"></i></span>
						</div>
						<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
							<input class="form-control dp time trigger-compute-duration" placeholder="Unloading Time" id="unloading-end-time" type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>	
					</div>
					<div class="margin-top-15">
						<p class="display-inline-mid font-14 font-400 width-200px">Unloading Duration:</p>
						<p class="display-inline-mid font-14 font-400 " id="duration-date-time"> 0 Hours</p>
					</div>
					<div class="margin-top-15">
						<p class="display-inline-mid font-14 font-400 width-200px">Departure Date/Time:</p>
						<div class="input-group width-200px italic display-inline-mid fixed-datepicker">
							<input class="form-control dp  default-cursor " placeholder="Departure Date" id="departure-date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon width-0 "><i class="fa fa-calendar"></i></span>
						</div>
						<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
							<input class="form-control dp time " placeholder="Duration Time" id="departure-time" type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>	
					</div>
					
				</div>
			
				<div class="modal-footer ">
					<div class="f-left">					
						<!-- <button type="button" class="font-12 btn btn-primary font-12 show-consignee-record">Show Record</button>						 -->
					</div>
					<div class="f-right width-200px">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<a href="javascript:void(0)">
							<button type="button" id="confirm-complete-receiving-record" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
						</a>
					</div>
				</div>
			</div>	
		</div>

		<div class="modal-container show-complete-record-truck-modal" modal-id="receiving-record">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Complete Receiving Record</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div>
						<p class="display-inline-mid font-14 font-400 width-150px"> Truck TARE IN:</p>
						<input type="text" class="t-small display-inline-mid trigger-compute-net-weight" id="set-tare-in">
						<p class="display-inline-mid margin-top-10">MT</p>			
					</div>
					<div class="margin-top-5">
						<p class="display-inline-mid font-14 font-400 width-150px"> Truck TARE OUT:</p>
						<input type="text" class="t-small display-inline-mid trigger-compute-net-weight" id="set-tare-out">
						<p class="display-inline-mid">MT</p>			
					</div>
					<div class="margin-top-5">
						<p class="display-inline-mid font-14 font-400 width-150px">Net Weight:</p>
						<p class="display-inline-mid font-14 font-400 " id="set-net-weight"> 0 MT</p>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<div class="f-left">
						<!-- <button type="button" class="font-12 btn btn-primary font-12 show-consignee-record-truck">Show Record</button>						 -->
					</div>
					<div class="f-right width-200px">
						<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						<a href="javascript:void(0)">
							<button type="button" id="complete-receiving-record-truck" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
						</a>
					</div>
					<div class="clear"></div>
				</div>
			</div>	
		</div>