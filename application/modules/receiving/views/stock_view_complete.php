
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="stock-receiving.php">Receiving - Company Goods</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color bread-show">Receiving No. 1234567890 (Complete)</li>
				</ul>
			</div>
			<div class="semi-main">
				
				<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white">
					
					<h4 class="font-500 font-20 black-color  padding-bottom-10 f-left"> Receiving Information</h4>					
					<div class="clear"></div>
					<div class="display-inline-mid width-50per">
						<div class="text-left">
							<p class="f-left font-14 font-bold width-40percent">PO No.:</p>
							<p class="f-left font-16 font-bold width-50percent">123456</p>
							<div class="clear"></div>
						</div>

						<div class="text-left padding-top-10">
							<p class="f-left font-14 font-bold width-40percent">Purchase Officer:</p>
							<p class="f-left font-16 font-bold width-50percent">Dennis Mendez</p>
							<div class="clear"></div>
						</div>

						<div class="text-left padding-top-10">
							<p class="f-left font-14 font-bold width-40percent">Origin:</p>
							<p class="f-left font-16 font-bold width-50percent">Local</p>
							<div class="clear"></div>
						</div>

						<div class="text-left padding-top-10">
							<p class="f-left font-14 font-bold width-40percent">Courier Name:</p>
							<p class="f-left font-16 font-bold width-50percent">Ruth Gomez</p>
							<div class="clear"></div>
						</div>
					</div>

					<div class="display-inline-top width-50per">
						<div class="text-left">
							<p class="f-left font-14 font-bold width-40percent">Invoice No.:</p>
							<p class="f-left font-16 font-bold width-50percent">123456</p>
							<div class="clear"></div>
						</div>

						<div class="text-left padding-top-10">
							<p class="f-left font-14 font-bold width-40percent padding-top-5 margin-top-3">Invoice Date:</p>
							<p class="f-left font-16 font-bold width-50percent">August 10, 2015 | 08:25 AM</p>
							<div class="clear"></div>
						</div>

						<div class="text-left padding-top-10">
							<p class="f-left font-14 font-bold width-40percent">Date and Time Issued:</p>
							<p class="f-left font-16 font-bold width-50percent">September 10, 2015 | 08:25 AM</p>
							<div class="clear"></div>
						</div>

						<div class="text-left padding-top-10">
							<p class="f-left font-14 font-bold width-40percent">Status:</p>
							<p class="f-left font-16 font-bold width-50percent">Complete</p>
							<div class="clear"></div>
						</div>

					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400 margin-top-5 margin-bottom-5">
							<a class="colapsed black-color " href="#">								
								<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10  active"> SKU #1234567890 - Holcim Cement</h4>
							</a>					
							
							<div class="clear"></div>
						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-20 ">
								<div class="bggray-white border-full padding-all-20 height-190px box-shadow-dark font-0">
									<div class="height-100percent display-inline-mid width-25percent text-center">
										<img src="../assets/images/holcim.jpg" alt="" class="height-100percent">
										<!-- <img src="../assets/images/holcim.jpg" alt="" class="height-100percent width-100percent"> -->
									</div>

									<div class="display-inline-top width-75percent padding-left-10 font-0">
										<div class="width-100percent ">
											<div class="padding-all-15 bg-light-gray">
												<p class="f-left no-margin-all width-20percent font-500">Vendor</p>
												<p class="f-left no-margin-all width-25percent">Gold Ever Trading</p>
												<p class="f-left no-margin-all width-20percent font-500">Stock Inventory</p>
												<p class="f-left no-margin-all width-25percent display-method">1000 KG</p>
												<div class="f-left width-35percent hide-method">
													<div class="f-left">				
														<input type="text" class="t-small width-60px display-inline-mid" value="1000">
														<p class="display-inline-mid margin-left-10">KG</p>						
														
													</div>												
													<div class="clear"></div>
												</div>
												<div class="clear"></div>
												
											</div>
											
											<div class="padding-all-15 text-left">
												<p class="f-left no-margin-all width-20percent font-500">Unit Price</p>
												<p class="f-left no-margin-all width-25percent">100.00 Php</p>
												<p class="f-left no-margin-all width-20percent font-500">Loading Method</p>
												<p class="f-left no-margin-all width-25percent display-receiving" style="display: none;">By Piece</p>
												<div class="f-left">
													<p>By Piece</p>
												</div>
												<div class="clear"></div>
												
											</div>
											<div class="padding-all-15 text-left bg-light-gray">
												<p class="f-left width-20percent font-500">Description</p>
												<p class="f-left width-25percent">Ang Cement na matibay</p>
												
												<p class="f-left width-20percent font-500">Qty to Receive</p>
												<p class="f-left no-margin-all width-25percent display-receiving">100 KG (10 Piece)</p>

												<div class="clear"></div>
											</div>
											
										</div>
										
									</div>
								</div>
								
						
								
								<!-- storage assignment display result  -->
								<div class="assignment-display" id="navigate-acc">
									<div class="padding-top-20">
									<div class="margin-bottom-10 margin-top-10">
										<p class="no-margin-all font-20 font-500 f-left  padding-left-10 padding-top-5">Storage Assignment</p>
										
										<div class="clear"></div>
									</div>
									<table class="tbl-4c3h">
										<thead>
											<tr>
												<th class="width-25percent black-color no-padding-left">Batch Name</th>
												<th class="width-50percent no-padding-left">Storage Location</th>
													<th class="width-25percent black-color no-padding-left">Quantity</th>
												</tr>
											</thead>
										</table>

										<div class="transfer-prod-location font-0 bggray-white ">
											
											<!-- first - panel  -->
											<div class="storage-assign-panel position-rel">
												<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change">
													<div class="width-25percent display-inline-mid text-center">
														<p class="font-14 font-400">123468469</p>
													</div>
													<div class="width-50percent text-center display-inline-mid height-auto line-height-25">
														<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>													
													</div>
													<div class="width-25percent display-inline-mid text-center">
														<p class="font-14 font-400">50 KG (5 Bags)</p>
													</div>
												</div>
												<div class="btn-hover-storage">
													<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>
													<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>
												</div>
											</div>
											<div class="storage-assign-panel position-rel">
												<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content ">
													<div class="width-25percent display-inline-mid text-center">
														<p class="font-14 font-400">123468469</p>
													</div>
													<div class="width-50percent text-center display-inline-mid height-auto line-height-25">
														<p class="font-14 font-400 display-inline-mid padding-right-10">Warehouse 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Area 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Storage 1</p>
														<i class="fa fa-caret-right font-15 display-inline-mid padding-right-10"></i>
														<p class="font-14 font-400 display-inline-mid padding-right-10">Shelf 1</p>
														
													</div>
													<div class="width-25percent display-inline-mid text-center">
														<p class="font-14 font-400">50 KG (5 Bags)</p>
													</div>
												</div>
												<div class="btn-hover-storage">
													<button class="btn general-btn modal-trigger padding-right-30 padding-left-30 display-inline-mid" modal-target="edit-batch">Edit</button>
													<button class="btn general-btn padding-right-30 padding-left-30 display-inline-mid margin-left-10 modal-trigger" modal-target="remove-batch">Remove</button>
												</div>
											</div>
											<div class="total-amount-storage margin-top-10">
												<p class="first-text">Total</p>
												<p class="second-text">100 KG (10 Bags)</p>
												<div class="clear"></div>
											</div>
										</div>
									</div>
								</div>						

								<!-- quantity check  -->
								<div class="margin-bottom-10 margin-top-30 ">
									<p class="no-margin-all font-20 font-500 padding-bottom-10 padding-left-10">Quantity Check</p>
									<div class="width-100percent">
										<div class="padding-all-15 bggray-light">										
											<p class="f-left margin-top-5 width-25percent font-500 padding-left-20">Received Quantity: </p>											
											<div class="f-left width-25percent">
												<p class=" margin-left-10 margin-top-5 show-qty">110 KG</p>
												<div class="hide-qty" >
													<input type="text" class="t-small width-60px" value="110">
													<div class="select small">
														<select>
															<option value="op1">KG</option>
															<option value="op2">MG</option>
														</select>
													</div>
												</div>
											</div>
											<p class="font-bold margin-left-10 f-left width-25percent">Reason for Discrepancy</p>
											<div class="f-left width-20percent">
												<p class="  margin-left-10 show-qty ">Others</p>
												<div class="hide-qty" >
													<div class="select small">
														<select>
															<option value="others1">Others</option>
														</select>
													</div>
												</div>
											</div>
											
											<div class="clear"></div>
										</div>
										<div class="padding-all-15">										
											<p class="f-left margin-top-5 width-25percent font-500 padding-left-20">Discrepancy: </p>											
											<div class="f-left width-25percent">
												<p class=" margin-left-10 margin-top-5 show-qty">10 KG</p>
												<div class="hide-qty">
													<input type="text" class="t-small width-60px" value="110">
													<div class="select small">
														<select>
															<option value="op1">KG</option>
															<option value="op2">MG</option>
														</select>
													</div>
												</div>
											</div>
											<p class="font-bold margin-left-10 f-left width-25percent">Remarks:</p>
											<div class="f-left width-20percent">
												<p class="  margin-left-10 show-qty">Overhaul of Item from Truck.</p>
												<div class="hide-qty">
													<textarea clas="font-15">Overhaul of Item from Truck</textarea>
												</div>
											</div>
											<div class="clear"></div>
										</div>
										
									</div>
										
									
								</div>
							</div>
						</div>
					</div>
				</div>
				

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-20 black-color padding-top-10"> Documents</h4>
							</a>
							
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-10">
									<table class="tbl-4c3h">
										<thead>
											<tr>
												<th class="black-color">Document Name</th>
												<th class="black-color">Date</th>
											</tr>
										</thead>
									</table>

									<div class="table-content position-rel tbl-dark-color">
										<div class="content-show padding-all-10">
											<div class="width-85per display-inline-mid padding-left-10">
												<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
												<p class=" display-inline-mid">Random Document 1</p>
											</div>
											<p class=" display-inline-mid ">22-Oct-2015</p>
										</div>
										<div class="content-hide" style="height: 50px;">
											<a href="#" class="display-inline-mid">
												<button class="btn general-btn padding-left-30 padding-right-30">View</button>
											</a>
											<a href="#" class="display-inline-mid">
												<button class="btn general-btn">Download</button>
											</a>
											<a href="#" class="display-inline-mid">
												<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
											</a>
										</div>
									</div>

									<div class="table-content position-rel">
										<div class="content-show padding-all-10">
											<div class="width-85per display-inline-mid padding-left-10">
												<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
												<p class=" display-inline-mid">Random Document 2</p>
											</div>
											<p class=" display-inline-mid ">22-Oct-2015</p>
										</div>
										<div class="content-hide" style="height: 50px;">
											<a href="#" class="display-inline-mid">
												<button class="btn general-btn padding-left-30 padding-right-30">View</button>
											</a>
											<a href="#" class="display-inline-mid">
												<button class="btn general-btn">Download</button>
											</a>
											<a href="#" class="display-inline-mid">
												<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
											</a>
										</div>
									</div>

									<div class="table-content position-rel tbl-dark-color">
										<div class="content-show padding-all-10">
											<div class="width-85per display-inline-mid padding-left-10">
												<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
												<p class=" display-inline-mid">Random Document 3</p>
											</div>
											<p class=" display-inline-mid ">22-Oct-2015</p>
										</div>
										<div class="content-hide" style="height: 50px;">
											<a href="#" class="display-inline-mid">
												<button class="btn general-btn padding-left-30 padding-right-30">View</button>
											</a>
											<a href="#" class="display-inline-mid">
												<button class="btn general-btn">Download</button>
											</a>
											<a href="#" class="display-inline-mid">
												<button class="btn general-btn padding-left-30 padding-right-30">Print</button>
											</a>
										</div>
									</div>
																	
								</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<h4 class="panel-title font-500 font-20 black-color padding-top-10"> Notes</h4>
							</a>
							
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-10">

								<div class="border-full padding-all-10 margin-left-18">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10 font-400">Show Less</a>
									<div class="clear"></div>
								</div>

								<div class="border-full padding-all-10 margin-left-18 margin-top-10">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
									<a href="" class="f-right padding-all-10 font-400">Show More</a>
									<div class="clear"></div>
								</div>							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
