		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL; ?>receiving/consignee_receiving">Receiving - Consignee Goods</a></li>
					<li><span>&gt;</span></li>						
					<li class=" black-color bread-show font-bold" id="displaySelectedReceiveNumberTemp">Receiving No. 1234567890</li>
					
				</ul>
			</div>
			<div class="semi-main">

				<!-- receiving accordion here  -->
				<div class="f-right padding-top-20 padding-bottom-20">
					<button class="btn general-btn display-inline-mid min-width-100px modal-trigger complete-receiving-record" modal-target="receiving-record">Complete Receiving Record</button>	
				</div>
				<div class="clear"></div>
				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white">
					<div class="panel-group">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">								
								<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10 "> Receiving Information</h4>
							</a>					
							<!-- <a href="consignee-view-vessel-edit.php" class="edit-enter-product"> -->
								<button class="btn general-btn f-right width-100px to-editing-page receiving-info-edit to-edit-page">Edit</button>			

							<div class="clear"></div>
							<!-- </a> -->																	
						</div>

						<!-- <div class="panel-collapse collapse in "> -->
						<div class="panel-collapse collapse in">
							<div class="panel-body">
								<div class="display-inline-mid width-50per margin-top-10">
									<div class="text-left">
										<p class="f-left font-14 font-bold width-40percent margin-top-2">PO No.:</p>
										<p class="f-left font-16 font-bold 0width-50percent " id="displaySelectedPONumTemp">123456</p>
										<div class="clear"></div>
									</div>

									<div class="text-left padding-top-10">
										<p class="f-left font-14 font-bold width-40percent margin-top-2">Mode of Delivery:</p>
										<p class="f-left font-16 font-bold width-50percent">Vessel</p>
										<div class="clear"></div>
									</div>					
								</div>

								<div class="display-inline-top width-50per margin-top-10">
									<div class="text-left">
										<p class="f-left font-14 font-bold width-40percent margin-top-2">Date and Time Issued:</p>
										<p class="f-left font-16 font-bold width-50percent" id="displaySelectedDateIssuedTemp">September 10, 2015 | 08:25 AM</p>
										<div class="clear"></div>
									</div>

								

									<div class="text-left padding-top-10">
										<p class="f-left font-14 font-bold width-40percent margin-top-2">Status:</p>
										<p class="f-left font-16 font-bold width-50percent">Ongoing</p>
										<div class="clear"></div>
									</div>

								</div>

								<div class="width-100per margin-top-30 padding-top-10 text-left border-top-light">
									<h4 class=" font-500 font-18 black-color padding-top-10 "> Delivery Information</h4>

									<div class="display-inline-top width-50per margin-top-20  text-left ">
										<div class="padding-all-15 width-100percent bg-light-gray">
											<p class="font-bold	f-left width-45percent margin-left-10">Vessel Name</p>
											<p class="f-left width-50percent font-400" id="display-selected-vessel-name-temp">MV CHINO</p>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 width-100percent ">
											<p class="font-bold	f-left width-45percent margin-left-10">Vessel Type</p>
											<p class="f-left width-50percent font-400" id="display-selected-vessel-type-temp">Cargo Ship</p>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 width-100percent bg-light-gray">
											<p class="font-bold	f-left width-45percent margin-left-10">Vessel Captain</p>
											<p class="f-left width-50percent font-400" id="display-selected-vessel-captain-temp">Akehito Nakamura</p>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 width-100percent ">
											<p class="font-bold	f-left width-45percent margin-left-10">Hatches</p>
											<p class="f-left width-50percent font-400" id="display-selected-vessel-hatches-temp">4</p>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 width-100percent bg-light-gray">
											<p class="font-bold	f-left width-45percent margin-left-10">Discharge Type</p>
											<p class="f-left width-50percent font-400" id="display-selected-vessel-discharge-type-temp">Pneumatic Unloader</p>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 width-100percent ">
											<p class="font-bold	f-left width-45percent margin-left-10">Bill of Lading Volume</p>
											<p class="f-left width-50percent font-400" id="display-selected-vessel-bill-loading-volume-temp">14000 MT</p>
											<div class="clear"></div>
										</div>
									</div>

									<div class="display-inline-top f-right width-50per margin-top-20 text-left ">
										<div class="padding-all-15 width-100percent bg-light-gray">
											<p class="font-bold	f-left width-47percent gray-color margin-left-10" id="display-selected-vessel-berthing-label">Berthing Date/Time</p>
											<p class="f-left width-45percent italic gray-color margin-left-10" id="display-selected-vessel-berthing-time-temp">Not Yet Assigned</p>
											<div class="clear"></div>
										</div>
										<div class="padding-all-15 width-100percent ">
											<p class="font-bold	f-left width-47percent gray-color margin-left-10">Unloading Date/TIme Start</p>
											<p class="f-left width-45percent italic gray-color margin-left-10">Not Yet Assigned</p>
											<div class="clear"></div>
										</div>
										<div class="padding-all-15 width-100percent bg-light-gray">
											<p class="font-bold	f-left width-47percent gray-color margin-left-10">Unloading Date/Time End</p>
											<p class="f-left width-45percent italic gray-color margin-left-10">Not Yet Assigned</p>
											<div class="clear"></div>
										</div>
										<div class="padding-all-15 width-100percent ">
											<p class="font-bold	f-left width-47percent gray-color margin-left-10">Unloading Duration</p>
											<p class="f-left width-45percent italic gray-color margin-left-10">Not Yet Assigned</p>
											<div class="clear"></div>
										</div>
										<div class="padding-all-15 width-100percent bg-light-gray">
											<p class="font-bold	f-left width-47percent gray-color margin-left-10">Departure Date/Time</p>
											<p class="f-left width-45percent italic gray-color margin-left-10">Not Yet Assigned</p>
											<div class="clear"></div>
										</div>
									</div>
									<div class="clear"></div>									
								</div>

							</div>
						</div>
					</div>						
				</div>
				
						
					<span id="displayProductItemList">
						<div class="border-top border-blue box-shadow-dark margin-bottom-20 record-item bggray-white item-template-product">
							<div class="head panel-group text-left" >
								<div class="panel-heading font-14 font-400 margin-top-5 margin-bottom-5">
									<div class="f-left">
										<a class="colapsed black-color display-inline-mid " href="#">								
											<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10 display-consignee-sku-name"> SKU #1234567890 - Japanese Corn</h4>
										</a>
										<div class="caution display-inline-mid">
											<img src="../assets/images/exclamation.svg" alt="caution" class="exclamation-mark">
											<img src="../assets/images/check.svg" alt="caution" class="check-mark display-none">
										</div>
									</div>

									<div class="f-right width-400px text-right">
										<button class="font-12 btn btn-default font-12 display-inline-mid red-color marker1-cancel display-none">Cancel</button>
										<button class="btn general-btn display-inline-mid marker1">Enter Item Details</button>
										<button class="font-12 btn btn-default font-12 display-inline-mid red-color editing-cancel display-none">Cancel</button>		
										<button class="btn general-btn display-inline-mid min-width-100px  trigger-editing-button editing-button">Edit</button>						
										
									</div>
									<div class="clear"></div>
									
								</div>
								<div class="panel-collapse collapse">
									<div class="panel-body padding-all-20 " >
										<div class="bggray-white border-full padding-all-20 height-190px box-shadow-dark font-0">
											<div class="height-100percent display-inline-mid width-25percent ">
												<img src="" alt="" class="height-100percent display-consignee-prod-img"
												style="background-position: center;background-size: contain;width: 228px;height: 150px !important;background-repeat: no-repeat; display: -moz-box;">

												<!-- <img src="../assets/images/holcim.jpg" alt="" class="height-100percent width-100percent"> -->
											</div>

											<div class="display-inline-top width-75percent padding-left-10 font-0">
												<div class="width-100percent ">
													<div class="padding-all-15 bg-light-gray">
														<p class="font-bold f-left no-margin-all width-20percent font-500">Vendor</p>
														<p class="f-left no-margin-all width-25percent display-consignee-vendor-name">Innova Farming</p>
														<p class="font-bold f-left no-margin-all width-20percent font-500">Loading Method</p>
														<p class="f-left no-margin-all width-25percent display-label display-consignee-loading-method">By Bags</p>
														
														<div class="loading-method-container ">

														</div>
														
														<div class="clear"></div>
														
													</div>
													
													<div class="padding-all-15">
														<p class="font-bold display-inline-mid no-margin-all width-20percent font-500">Shelf Life</p>
														<p class="display-inline-mid no-margin-all width-25percent display-consignee-aging-days">10 Days</p>
														<p class="font-bold display-inline-mid no-margin-all width-20percent font-500">Qty to Receive</p>
														<p class="display-inline-mid no-margin-all width-25percent display-label display-consignee-qtr-to-receive">100 KG (10 Bags)</p>

														<div class="width-35percent display-inline-mid text-left font-0 hide-label display-none">											
															<input type="text" class="display-inline-mid width-70px height-26 t-small set-qty-to-bulk" name="bag-or-bulks">
															<div class="font-12 display-inline-mid width-50px margin-left-5 margin-right-5 get-dropdown-here">
																<select class="padding-all-5 default-cursor unit-of-measure">
																	<!-- <option value="op1">KG</option>
																	<option value="op2">MT</option> -->
																</select>
															</div>
															
															<input type="text" class="display-inline-mid width-70px height-26 t-small set-qty-to-bag" style="width: 56px !important;" name="bag-or-bulks"  >
															<p class="font-14 font-400 display-inline-mid padding-left-5 padding-right-5">Bags</p>
															
															<div class="clear"></div>

															
														</div>
														<div class="clear"></div>
													</div>
													
												</div>
												
											</div>
										</div>
										
										<!-- consignee distribution  -->
										<div class="padding-top-20" id="navigate-acc">
											<div class="margin-bottom-10 margin-top-10">
												<p class="no-margin-all font-20 font-500  padding-left-10 f-left">Consignee Distribution</p>										
												<div class="clear"></div>
											</div>
											<table class="tbl-4c3h">
												<thead>
													<tr>
														<th class="width-50percent black-color no-padding-left">Consignee Name</th>
														<th class="width-50percent no-padding-left">Quantity</th>												
													</tr>
												</thead>
											</table>
												<span class="display-edit-consignee-distibution">
													<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change display-edit-consignee-items">
														<div class="width-50percent display-inline-mid text-center">
															<p class="font-14 font-400 display-distribution display-consignee-distribution-name">Alpha Food Inc.</p>
														</div>
														<div class="width-45percent display-inline-mid text-center">
															<p class="font-14 font-400 display-distribution display-consignee-distribution-quantity">50 KG (5 Bags)</p>
														</div>
													</div>
												</span>
											
												<span class="set-consignee-items"></span>
										
												
										
											
												<div class="padding-top-10">
													
													<a href="#" class="f-right font-400 hide-link trigger-add-consignee">+ Add Consignee</a>
												</div>
												<div class="clear"></div>
										</div> 


										<!-- storage assignment display result  -->
										<div class="assignment-display storage-check-btn" >
											<div class="padding-top-20">
											<div class="margin-bottom-10 margin-top-10">
												<p class="no-margin-all font-20 font-500 f-left  padding-left-10 padding-top-5">Storage Assignment</p>
												<div class="f-right adding-batch">
													<button class="btn general-btn modal-trigger show-add-batch-modal" modal-target="add-batch">Add Batch</button>
												</div>
												<div class="clear"></div>
											</div>
											<table class="tbl-4c3h">
												<thead>
													<tr>
														<th class="width-25percent black-color no-padding-left">Batch Name</th>
														<th class="width-50percent no-padding-left">Storage Location</th>
															<th class="width-25percent black-color no-padding-left">Quantity</th>
														</tr>
													</thead>
												</table>
												<div class="transfer-prod-location font-0 bggray-white position-rel">
			                                        <div class="width-100percent padding-top-10 padding-bottom-10 show-ongoing-content margin-bottom-20 background-change">
			                                            <div class="width-100percent display-inline-mid text-center">
			                                                <p class="font-14 font-400 italic gray-color">Not Yet Assigned</p>
			                                            </div>
			                                        </div>
			                                    </div>
											</div>
										</div>

										<!-- quantity check  -->
										<div class="margin-bottom-10 margin-top-20 quantity-check-hide">
											<p class="no-margin-all font-20 font-500 padding-bottom-10 padding-left-10 gray-color">Quantity Check</p>
											<div class="width-100percent">
												<div class="padding-all-15 bggray-light">
													<p class="f-left no-margin-all width-25percent font-500 padding-left-20 gray-color">Received Quantity</p>
													<p class="f-left no-margin-all width-25percent italic gray-color display-success-receive-qty">Not Yet Assigned</p>
													<p class="f-left no-margin-all width-25percent font-500 gray-color">Reason of Discrepancy</p>
													<p class="f-left no-margin-all width-25percent italic gray-color display-success-reason-discrepancy">Not Yet Assigned</p>
													<div class="clear"></div>
												</div>
												
												<div class="padding-all-15">
													<p class="f-left no-margin-all width-25percent font-500 padding-left-20 gray-color">Discrepancy</p>
													<p class="f-left no-margin-all width-25percent italic gray-color display-success-discrepancy">Not Yet Assigned</p>
													<p class="f-left no-margin-all width-25percent font-500 gray-color">Remarks</p>
													<p class="f-left no-margin-all width-25percent italic gray-color display-success-discrepancy-remarks">Not Yet Assigned</p>
													<div class="clear"></div>
												</div>
											</div>
										</div>

										<!-- quantity check  -->
										<div class="margin-bottom-10 margin-top-30 quantity-check-btn">
											<p class="no-margin-all font-20 font-500 padding-bottom-10 padding-left-10">Quantity Check</p>
											<div class="width-100percent">
												<div class="padding-all-15 bggray-light">
													<p class="f-left margin-top-5 width-25percent font-500 padding-left-20">Received Quantity: </p>
													<div class="">
														
													<p class="f-left width-25percent margin-top-5 f-left italic set-receive-quantity">0 KG</p>
														
													</div>
													<p class="f-left margin-top-5 width-25percent font-500">Cause of Discrepancy</p>
													<div class="select medium set-dropdown-data">
														<select class="transform-dd set-discrepancy-cause">
															<!-- <option value="">Select</option> -->
															<option value="Others">Others</option>
															<option value="Extra">Extra</option>
														</select>
													</div>
													<div class="clear"></div>
												</div>
												
												<div class="padding-all-15">
													<p class="f-left no-margin-all width-25percent font-500 padding-left-20">Discrepancy</p>
													<p class="f-left no-margin-all width-25percent italic discrepancy">0 KG</p>
													<p class="f-left no-margin-all width-25percent font-500">Remarks</p>
													<textarea class="f-left width-25percent font-14 set-discrepancy-remarks"></textarea>
													<div class="clear"></div>
												</div>
											</div>
										</div>



										 <!-- <div class="margin-bottom-10 margin-top-30 discrepancy-container">
			                                <p class="no-margin-all font-20 font-500 padding-bottom-10 padding-left-10 gray-color">Quantity Check</p>
			                                <div class="width-100percent">
			                                    <div class="padding-all-15 bggray-light">
			                                        <p class="f-left margin-top-5 width-25percent font-500 padding-left-20 gray-color">Received Quantity: </p>
			                                        <div class="f-left width-25percent">
			                                            <p class=" margin-left-10 margin-top-5 show-qty received-quantity"> </p>
			                                        </div>
			                                        <p class="font-bold margin-left-10 f-left width-25percent gray-color">Case of Discrepancy</p>
			                                        <div class="f-left width-20percent">
			                                            <p class="  margin-left-10 show-qty cause-of-discrepancy">
			                                            </p>
			                                        </div>
			                                        <div class="clear"></div>
			                                    </div>
			                                    <div class="padding-all-15">
			                                        <p class="f-left margin-top-5 width-25percent font-500 padding-left-20 gray-color">Discrepancy: </p>
			                                        <div class="f-left width-25percent">
			                                            <p class=" margin-left-10 margin-top-5 show-qty discrepancy"> </p>
			                                        </div>
			                                        <p class="font-bold margin-left-10 f-left width-25percent gray-color">Remarks:</p>
			                                        <div class="f-left width-20percent">
			                                            <p class="  margin-left-10 show-qty remarks-textbox"> </p>
			                                        </div>
			                                        <div class="clear"></div>
			                                    </div>
			                                </div>
			                            </div> -->

											
										</div>
									</div>
								</div>
						</div>
					</span>
					
					

					<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
						<div class="head panel-group text-left">
							<div class="panel-heading font-14 font-400">
								<a class="colapsed black-color f-left" href="#">
									<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
									<h4 class="panel-title  font-500 font-20 black-color padding-top-10"> Documents</h4>
								</a>
								<div class="f-right">
									<button class="btn general-btn modal-trigger" modal-target="upload-documents">Upload</button>
								</div>
								<div class="clear"></div>

							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body padding-all-10">
									<table class="tbl-4c3h">
										<thead>
											<tr>
												<th class="black-color">Document Name</th>
												<th class="black-color">Date</th>
											</tr>
										</thead>
									</table>
									<span id="displayDocuments"></span>
																	
								</div>
							</div>
						</div>
					</div>

					<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
						<div class="head panel-group text-left">
							<div class="panel-heading font-14 font-400">
								<a class="colapsed black-color f-left" href="#">
									<h4 class="panel-title font-500 font-20 black-color padding-top-10"> Notes</h4>
								</a>
								<div class="f-right">
									<button class="btn general-btn modal-trigger" modal-target="add-note">Add Notes</button>
								</div>
								<div class="clear"></div>

							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body padding-all-10" id="display-receiving-notes">
<!-- 
									<div class="border-full padding-all-10 margin-left-18">
										<div class="border-bottom-small border-gray padding-bottom-10">
											<p class="f-left font-14 font-400">Subject: Drivers Note About the meaning of life</p>
											<p class="f-right font-14 font-400">Date/Time: 25-Oct-2015 01:30 PM</p>
											<div class="clear"></div>
										</div>
										<p class="font-14 font-400 no-padding-left padding-all-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
										<a href="" class="f-right padding-all-10 font-400">Show Less</a>
										<div class="clear"></div>
									</div> -->

									<div class="border-full padding-all-10 margin-left-18 margin-top-10 display-receiving-notes-row notes-contain">
										<div class="border-bottom-small border-gray padding-bottom-10">
											<p class="f-left font-14 font-400 display-receiving-notes-subject">Subject: Drivers Note About the meaning of life</p>
											<p class="f-right font-14 font-400 display-receiving-notes-date-created">Date/Time: 25-Oct-2015 01:30 PM</p>
											<div class="clear"></div>
										</div>
										<p class="font-14 font-400 no-padding-left padding-all-10 display-receiving-notes-message show-notes-message"></p>
										<a href="" class="f-right padding-all-10 font-400 show-notes-messge-length">Show More</a>
										<div class="clear"></div>
									</div>							
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>

		<!--Start of Upload Document MODAL-->
		<div class="modal-container" modal-id="upload-documents">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Upload Document</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">Dcoument Name:</p>
						<input type="text" class="width-231px display-inline-mid">
					</div>
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all display-inline-mid width-110px">File Location:</p>
						<input type="text" class="width-231px display-inline-mid">
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Browse</button>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
				</div>
			</div>	
		</div>
		<!--End of Upload Document MODAL-->

		<!--Start of Add Notes MODAL-->
		<div class="modal-container" modal-id="add-note">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Add Notes</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<form id="addReceivingNoteForm">
					<!-- content -->
					<div class="modal-content text-left">
						<div class="text-left padding-bottom-10">
							<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
							<input type="text" id="addNoteSubject" datavalid="required" class="width-380px display-inline-mid margin-left-10">
						</div>
						<div class="text-left padding-bottom-10">
							<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
							<textarea id="addNoteMessage" datavalid="required" class="width-380px margin-left-10 height-250px"></textarea>
						</div>
					</div>
				</form>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="add-notes-save" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
				</div>
			</div>	
		</div>
		<!--End of Add Notes MODAL-->
			
		<!-- Start of Remove Batch  -->
		<div class="modal-container" modal-id="remove-batch">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Remove Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all">Are you sure you want to remove Batch 1234567890</p>
						
					</div>					
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Remove</button>
				</div>
			</div>	
		</div>

		<!--Start of edit Batch MODAL-->
		<!-- <div class="modal-container" modal-id="edit-batch">
			<div class="modal-body medium margin-top-100 margin-bottom-100">				

				<div class="modal-head">
					<h4 class="text-left">Edit Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<div class="modal-content text-left">
					<div class="text-left padding-bottom-10">

						<div class="">
							<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Product:</p>
							<p class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Japanese Cement</p>
						</div>

						<div class="margin-top-10">
							<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Vessel:</p>
							<p class="font-14 font-400 no-margin-left display-inline-mid ">MV Sand King</p>
						</div>

						<div class="">
							<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per ">Batch Name:</p>
							<input type="text" class="t-small no-margin-left" value=" CNEKIEK12345">
						</div>

						<div class="">
							<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per">Consignee: </p>
							<div class="select large">
								<select class="transform-dd">
									<option value="CNEKIEK12345">Alpha Food Inc.</option>
								</select>
							</div>
						</div>
						
						<div class="margin-top-10">
							<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Amount to Receive:</p>
							<p class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
						</div>						

						<div class="margin-top-5">
							<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Batch Amount:</p>
							<input type="text" class="t-small display-inline-mid">
							<p class="display-inline-mid font-400 font-14 margin-top-15">KG</p>
						</div>

						<div class="margin-top-10">
							<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Receiving Balance:</p>
							<p class="font-14 font-400 no-margin-left display-inline-mid red-color">20 KG</p>
						</div>	
					</div>
					<p class="no-margin-all padding-bottom-10">Click a Location to Select It. Double-click to go to its sub-locations</p>				
					<div class="bggray-white height-300px overflow-y-auto">
		                <div class="padding-all-10 height-50px padding-bottom-10 border-bottom-small border-black" id="breadCrumbsHierarchy">
		                </div>
		                <div class="font-0" id="batchDisplay">
		                </div>
		            </div>

				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
				</div>
			</div>	
		</div> -->
		<!--End of edit Batch MODAL-->
		
		<!-- start of add batch modal  -->
		<div class="modal-container" id="add-batch-modal" modal-id="add-batch">
			<div class="modal-body medium margin-top-100 margin-bottom-100">				

				<div class="modal-head">
					<h4 class="text-left">Add Batch</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="text-left padding-bottom-10">

						<div class="">
							<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Item:</p>
							<p class="font-14 font-400 no-margin-left display-inline-mid product-name-sku">SKU No. 1234567890 - Japanese Cement</p>
						</div>

						<div class="margin-top-10">
							<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Vessel:</p>
							<p class="font-14 font-400 no-margin-left display-inline-mid product-vessel-name">MV Sand King</p>
						</div>

						<div class="margin-top-10">
							<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Amount to Receive:</p>
							<p class="font-14 font-400 no-margin-left display-inline-mid to-receive">90 KG</p>
						</div>	

						<div class="">
							<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per ">Batch Name:</p>
							<input type="text" class="t-small no-margin-left input-batch-name" >
						</div>
											

						<div class="margin-top-5">
							<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Batch Amount:</p>
							<input type="text" class="t-small display-inline-mid input-batch-amount">
							<p class="display-inline-mid font-400 font-14 margin-top-15 batch-amount-uom">KG</p>
						</div>

						<div class="margin-top-10">
							<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Receiving Balance:</p>
							<p class="font-14 font-400 no-margin-left display-inline-mid receiving-balance">20 KG</p>
						</div>	
					</div>
					<p class="no-margin-all padding-bottom-10">Click a Location to Select It. Double-click to go to its sub-locations</p>				
					<div class="bggray-white height-300px overflow-y-auto">
		                <div class="padding-all-10 height-50px padding-bottom-10 border-bottom-small border-black" id="breadCrumbsHierarchy">
		                </div>
		                <div class="font-0" id="batchDisplay">
		                </div>
		            </div>

				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" id="confirmAddBatch">Confirm</button>
				</div>
			</div>
		</div>	
	
		<!-- Are you sure product details -->
		<div class="modal-container" modal-id="are-you-sure">
			<div class="modal-body small ">				

				<div class="modal-head">
					<h4 class="text-left">Complete Item Details</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-all-10">
					<p class="font-14 font-400">Are you sure you want to complete the item details ?</p>
				</div>
				</div>
			
				<div class="modal-footer text-right">	

					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>					
					<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 modal-trigger" modal-target="product-details">Confirm</button>									
				</div>
			</div>	
		</div>

		<!-- start of complete product details  -->
		<div class="modal-container display-product-details" modal-id="product-details">
			<div class="modal-body small ">				

				<div class="modal-head">
					<h4 class="text-left">Complete Item Details</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div class="padding-all-10 bggray-7cace5">
					<p class="font-14 font-400 white-color">Item Details Saved. Stock Receiving is now complete.</p>
				</div>
				</div>
			
				<div class="modal-footer text-right">	
					<a href="consignee-view-ongoing-1-vessel.php">				
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Show Record</button>
					</a>
				</div>
			</div>	
		</div>

		<!-- complete receiving record  -->
		<div class="modal-container" modal-id="receiving-record">
			<div class="modal-body medium">				

				<div class="modal-head">
					<h4 class="text-left">Complete Receiving Record</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left">
					<div>						
						<p class="display-inline-mid font-14 font-400 width-200px margin-top-15">Berthing Date/Time Start:</p>
						<div class="input-group width-200px italic display-inline-mid fixed-datepicker">
							<input class="form-control dp  default-cursor" id="berthing-start-date" placeholder="Berthing Date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon width-0 "><i class="fa fa-calendar"></i></span>
						</div>
						<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
							<input class="form-control dp time " placeholder="Berthing Time" id="berthing-start-time" type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>					
					</div>
					<div>						
						<p class="display-inline-mid font-14 font-400 width-200px margin-top-15">Unloading Date/Time Start:</p>
						<div class="input-group width-200px italic display-inline-mid fixed-datepicker">
							<input class="form-control dp  default-cursor trigger-compute-duration" id="unloading-start-date" placeholder="Uploading Date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon width-0 "><i class="fa fa-calendar"></i></span>
						</div>
						<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
							<input class="form-control dp time trigger-compute-duration" placeholder="Uploading Time" id="unloading-start-time" type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>					
					</div>
					<div class="margin-top-5">
						<p class="display-inline-mid font-14 font-400  width-200px margin-top-15">Unloading Date/Time End:</p>
						<div class="input-group width-200px italic display-inline-mid fixed-datepicker">
							<input class="form-control dp  default-cursor trigger-compute-duration" placeholder="Unloading Date" id="unloading-end-date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon width-0 "><i class="fa fa-calendar"></i></span>
						</div>
						<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
							<input class="form-control dp time trigger-compute-duration" placeholder="Unloading Time" id="unloading-end-time" type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>	
					</div>
					<div class="margin-top-15">
						<p class="display-inline-mid font-14 font-400 width-200px">Unloading Duration:</p>
						<p class="display-inline-mid font-14 font-400 " id="duration-date-time"> 0 Hours</p>
					</div>
					<div class="margin-top-15">
						<p class="display-inline-mid font-14 font-400 width-200px">Departure Date/Time:</p>
						<div class="input-group width-200px italic display-inline-mid fixed-datepicker">
							<input class="form-control dp  default-cursor " placeholder="Departure Date" id="departure-date" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon width-0 "><i class="fa fa-calendar"></i></span>
						</div>
						<div class="input-group width-200px italic display-inline-mid fixed-timepicker">
							<input class="form-control dp time " placeholder="Departure Time" id="departure-time"  type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>	
					</div>
					
					
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<!-- <a href="consignee-complete-vessel.php"> -->
					<a href="javascript:void(0)">
						<button type="button" id="confirm-complete-receiving-record" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
					</a>
				</div>
			</div>	
		</div>