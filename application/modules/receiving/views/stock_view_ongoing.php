<div class="main">
    <div class="breadcrumbs no-margin-left padding-left-50">
        <ul>
            <li><a href="<?php echo BASEURL; ?>receiving/company_goods">Receiving - Company Goods</a></li>
            <li><span>&gt;</span></li>
            <li class="font-bold black-color bread-show" id="displayReceivingNumber"> </li>
        </ul>
    </div>
    <div class="semi-main">
        <div class="f-right margin-bottom-10 width-300px text-right margin-bottom-20">
            <button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color btn-cancel-hide" id="cancelProductDetails">Cancel</button>
            <button class="btn general-btn click-check-btn display-inline-mid receiving-stock-disable" id="enterProductDetails">Enter Product Details</button>
        </div>
        <div class="clear"></div>
        <div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white">
            <h4 class="font-500 font-20 black-color  padding-bottom-10 f-left"> Receiving Information </h4>
            <a href ="<?php echo BASEURL; ?>receiving/stock_view_edit" class="f-right">
            <button class="btn general-btn f-right width-100px receiving-info-edit receiving-stock-disable" id="editRecord">Edit</button>
            </a>
            <div class="clear"></div>
            <div class="display-inline-mid width-50per">
                <div class="text-left">
                    <p class="f-left font-14 font-bold width-40percent">PO No.:</p>
                    <p  class="f-left font-16 font-bold width-50percent" id="displayPoNumber"> </p>
                    <div class="clear"></div>
                </div>
                <div class="text-left padding-top-10">
                    <p class="f-left font-14 font-bold width-40percent">Purchase Officer:</p>
                    <p  class="f-left font-16 font-bold width-50percent" id="displayOfficer"> </p>
                    <div class="clear"></div>
                </div>
                <div class="text-left padding-top-10">
                    <p class="f-left font-14 font-bold width-40percent">Courier Name:</p>
                    <p class="f-left font-16 font-bold width-50percent" id="displayCourierName"> </p>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="display-inline-top width-50per">
                <div class="text-left">
                    <p class="f-left font-14 font-bold width-40percent">Invoice No.:</p>
                    <p  class="f-left font-16 font-bold width-50percent" id="displayInvoiceNumber"> </p>
                    <div class="clear"></div>
                </div>
                <div class="text-left padding-top-10">
                    <p class="f-left font-14 font-bold width-40percent  margin-top-3">Invoice Date:</p>
                    <p  class="f-left font-16 font-bold width-50percent" id="displayInvoiceDate"> </p>
                    <div class="clear"></div>
                </div>
                <div class="text-left padding-top-10">
                    <p class="f-left font-14 font-bold width-40percent">Date and Time Issued:</p>
                    <p class="f-left font-16 font-bold width-50percent" id="displayCreatedDate"> </p>
                    <div class="clear"></div>
                </div>
                <div class="text-left padding-top-10">
                    <p class="f-left font-14 font-bold width-40percent">Status:</p>
                    <p  class="f-left font-16 font-bold width-50percent" id="displayStatus"> </p>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <span id="displayItemList">
            <div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white record-item item-template" style="display:none;">
                <div class="head panel-group text-left">
                    <div class="panel-heading font-14 font-400 margin-top-5 margin-bottom-5">
                        <div class="f-left">
                            <a class="colapsed black-color " href="#">
                                <h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10  active name product-name-sku" >  </h4>
                            </a>
                            <div class="caution display-inline-mid">
                                <img src="../assets/images/exclamation.svg" alt="caution" class="exclamation-mark">
                                <img src="../assets/images/check.svg" alt="caution" class="check-mark display-none">
                            </div>
                            <div class="clear"></div>
                        </div>
                        
                        <div class="f-right width-400px text-right">
                            <button class="btn general-btn display-inline-mid receiving-first-disable display-none update-product-details">Update Item Details</button>
                            <button class="font-12 btn btn-default font-12 display-inline-mid red-color editing-cancel display-none update-cancel-save-changes">Cancel</button>        
                            <button class="btn general-btn display-inline-mid min-width-100px editing-button update-save-changes  display-none">Save Changes</button>
                            
                            <button class="btn general-btn display-inline-mid receiving-first-disable marker1 enter-product-details">Enter Item Details</button>
                            <button class="btn general-btn display-inline-mid min-width-100px editing-button item-edit">Edit</button>
                            <button class="font-12 btn btn-default font-12 display-inline-mid red-color editing-cancel display-none item-edit-cancel-changes">Cancel</button>        
                            <button class="btn general-btn display-inline-mid min-width-100px editing-button display-none item-edit-save-changes">Save Changes</button> 

                            <button class="font-12 btn btn-default font-12 display-inline-mid red-color editing-cancel display-none item-cancel-save-changes">Cancel</button>        
                            <button class="btn general-btn display-inline-mid min-width-100px editing-button item-save-changes  display-none">Save Changes</button>                   
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="panel-collapse collapse in">
                        <div class="panel-body padding-all-20 ">
                            <div class="bggray-white border-full padding-all-20 height-190px box-shadow-dark font-0">
                                <div class="height-100percent display-inline-mid width-25percent text-center">
                                    <div class="item-image-receiving-view item-img"></div>
                                </div>
                                <div class="display-inline-top width-75percent padding-left-10 font-0">
                                    <div class="width-100percent ">
                                        <div class="padding-all-15 bg-light-gray">
                                            <p class="f-left no-margin-all width-20percent font-500">Vendor</p>
                                            <p class="f-left no-margin-all width-25percent vendor-name"> none </p>
                                            <p class="f-left no-margin-all width-20percent font-500">Stock Inventory</p>
                                            <p class="f-left no-margin-all width-25percent stock-inventory"> 0 </p>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="padding-all-15">
                                            <p class="f-left no-margin-all width-20percent font-500">Unit Price</p>
                                            <p class="f-left no-margin-all width-25percent price"> none </p>
                                            <p class="f-left no-margin-all width-20percent font-500">Loading Method</p>
                                            <p class="f-left no-margin-all width-25percent display-receiving loading-method">By Piece</p>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="padding-all-15 bg-light-gray">
                                            <p class="f-left no-margin-all width-20percent font-500">Description</p>
                                            <p class="f-left no-margin-all width-25percent item-description"> none </p>
                                            <p class="f-left no-margin-all width-20percent font-500">Qty to Receive</p>
                                            <p class="f-left no-margin-all width-25percent display-receiving qty-to-receive" style="display: block;"> none </p>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="assignment-display">
                                <div class="padding-top-20">
                                    <div class="margin-bottom-10 margin-top-10">
                                        <p class="no-margin-all font-20 font-500 f-left  padding-left-10 padding-top-5 gray-color">Storage Assignment</p>
                                        <div class="f-right hide-method" style="display: none;">
                                            <button class="btn general-btn modal-trigger" modal-target="add-batch">Add a Batch</button>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <table class="tbl-4c3h">
                                        <thead>
                                            <tr>
                                                <th class="width-25percent black-color no-padding-left gray-color">Batch Name</th>
                                                <th class="width-50percent no-padding-left gray-color">Storage Location</th>
                                                <th class="width-25percent black-color no-padding-left gray-color">Quantity</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="transfer-prod-location font-0 bggray-white position-rel">
                                        <div class="width-100percent padding-top-10 padding-bottom-10 show-ongoing-content margin-bottom-20 background-change">
                                            <div class="width-100percent display-inline-mid text-center">
                                                <p class="font-14 font-400 italic gray-color">Not Yet Assigned</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="margin-bottom-10 margin-top-30 discrepancy-container">
                                <p class="no-margin-all font-20 font-500 padding-bottom-10 padding-left-10 gray-color">Quantity Check</p>
                                <div class="width-100percent">
                                    <div class="padding-all-15 bggray-light">
                                        <p class="f-left margin-top-5 width-25percent font-500 padding-left-20 gray-color">Received Quantity: </p>
                                        <div class="f-left width-25percent">
                                            <p class=" margin-left-10 margin-top-5 show-qty received-quantity"> </p>
                                        </div>
                                        <p class="font-bold margin-left-10 f-left width-25percent gray-color">Case of Discrepancy</p>
                                        <div class="f-left width-20percent">
                                            <p class="  margin-left-10 show-qty cause-of-discrepancy">
                                            </p>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="padding-all-15">
                                        <p class="f-left margin-top-5 width-25percent font-500 padding-left-20 gray-color">Discrepancy: </p>
                                        <div class="f-left width-25percent">
                                            <p class=" margin-left-10 margin-top-5 show-qty discrepancy"> </p>
                                        </div>
                                        <p class="font-bold margin-left-10 f-left width-25percent gray-color">Remarks:</p>
                                        <div class="f-left width-20percent">
                                            <p class="  margin-left-10 show-qty remarks-textbox"> </p>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </span>
        <div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
            <div class="head panel-group text-left">
                <div class="panel-heading font-14 font-400">
                    <a class="colapsed black-color f-left" href="javascript:void(0)">
                        <!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
                        <h4 class="panel-title  font-500 font-20 black-color padding-top-10"> Documents</h4>
                    </a>
                    <div class="f-right">
                        <button class="btn general-btn modal-trigger" modal-target="upload-documents" >Upload</button>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="panel-collapse collapse in">
                    <div class="panel-body padding-all-10">
                        <table class="tbl-4c3h">
                            <thead>
                                <tr>
                                    <th class="black-color">Document Name</th>
                                    <th class="black-color">Date</th>
                                </tr>
                            </thead>
                        </table>
                        <span id="displayDocuments"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
            <div class="head panel-group text-left">
                <div class="panel-heading font-14 font-400">
                    <a class="colapsed black-color f-left" href="#">
                        <h4 class="panel-title font-500 font-20 black-color padding-top-10"> Notes</h4>
                    </a>
                    <div class="f-right">
                        <button class="btn general-btn modal-trigger" modal-target="add-note" >Add Notes</button>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="panel-collapse collapse in">
                    <div class="panel-body padding-all-10" id="noteContainer">
                        <div class="border-full padding-all-10 margin-left-18 margin-top-10 note-template" style="display:none;">
                            <div class="border-bottom-small border-gray padding-bottom-10">
                                <p class="f-left font-14 font-400 subject"> </p>
                                <p class="f-right font-14 font-400 datetime"> </p>
                                <div class="clear"></div>
                            </div>
                            <p class="font-14 font-400 no-padding-left padding-all-10 message"></p>
                            <a href="" class="f-right padding-all-10 font-400"> </a>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-container display-product-details" modal-id="product-details">
    <div class="modal-body small ">
        <div class="modal-head">
            <h4 class="text-left">Complete Item Details</h4>
            <div class="modal-close close-me"></div>
        </div>
        <!-- content -->
        <div class="modal-content text-left">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-14 font-400 white-color">Item Details Saved. Stock Receiving is now complete.</p>
            </div>
        </div>
        <div class="modal-footer text-right">   
            <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Show Record</button>
        </div>
    </div>
</div>
<div class="modal-container display-product-details" modal-id="completing-">
    <div class="modal-body small ">
        <div class="modal-head">
            <h4 class="text-left">Complete Item Details</h4>
            <div class="modal-close close-me"></div>
        </div>
        <!-- content -->
        <div class="modal-content text-left">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-14 font-400 white-color">Item Details Saved. Stock Receiving is now complete.</p>
            </div>
        </div>
        <div class="modal-footer text-right">   
            <a href="stock-view-ongoing-1.php">             
            <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Show Record</button>
            </a>
        </div>
    </div>
</div>
<div class="modal-container complete-product-modal" modal-id="are-you-sure">
    <div class="modal-body small ">
        <div class="modal-head">
            <h4 class="text-left">Complete Item Details</h4>
            <div class="modal-close close-me"></div>
        </div>
        <!-- content -->
        <div class="modal-content text-left">
            <div class="padding-all-10">
                <p class="font-14 font-400">Are you sure you want to complete the item details ?</p>
            </div>
        </div>
        <div class="modal-footer text-right">   
            <button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>                 
            <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 submit-btn">Confirm</button>                                   
        </div>
    </div>
</div>
  
<!--Start of Upload Document MODAL-->
<div class="modal-container" modal-id="upload-documents">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Upload Document</h4>
            <div class="modal-close close-me"></div>
        </div>
        <!-- content -->
        <div class="modal-content text-left">
            <div class="text-left padding-bottom-10">
                <p class="font-14 font-400 no-margin-all display-inline-mid width-110px">Dcoument Name:</p>
                <input type="text" class="width-231px display-inline-mid">
            </div>
            <div class="text-left padding-bottom-10">
                <p class="font-14 font-400 no-margin-all display-inline-mid width-110px">File Location:</p>
                <input type="text" class="width-231px display-inline-mid">
                <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Browse</button>
            </div>
        </div>
        <div class="modal-footer text-right">
            <button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
            <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
        </div>
    </div>
</div>
<!--End of Upload Document MODAL-->
<!--Start of Add Notes MODAL-->
<div class="modal-container" modal-id="add-note">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Add Notes</h4>
            <div class="modal-close close-me"></div>
        </div>
        <!-- content -->
        <div class="modal-content text-left">
            <div class="text-left padding-bottom-10">
                <p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
                <input type="text" class="width-380px display-inline-mid" id="noteSubject">
            </div>
            <div class="text-left padding-bottom-10">
                <p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
                <textarea class="width-380px margin-left-10 height-250px" id="noteMessage"></textarea>
            </div>
        </div>
        <div class="modal-footer text-right">
            <button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
            <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" id="confirmNote">Confirm</button>
        </div>
    </div>
</div>
<!--End of Add Notes MODAL-->
<!--Start of Add Batch MODAL-->
<div class="modal-container" modal-id="edit-batch">
    <div class="modal-body medium margin-top-100 margin-bottom-100">
        <div class="modal-head">
            <h4 class="text-left">Edit Batch</h4>
            <div class="modal-close close-me"></div>
        </div>
        <!-- content -->
        <div class="modal-content text-left">
            <div class="text-left">
                <div class="">
                    <p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Item:</p>
                    <p class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Holcim Cement</p>
                </div>
                <div class="margin-top-10">
                    <p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Vessel:</p>
                    <p class="font-14 font-400 no-margin-left display-inline-mid ">MV Sand King</p>
                </div>
                <div class="">
                    <p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per ">Batch Name:</p>
                    <input type="text" class="t-small no-margin-left" value="CNEKIEK12344">
                </div>
                <div class="">
                    <p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per">Consignee: </p>
                    <div class="select large">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option" data-value="CNEKIEK12345">Alpha Food Inc.</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="CNEKIEK12345">Alpha Food Inc.</option>
                        </select>
                    </div>
                </div>
                <div class="margin-top-10">
                    <p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Amount to Receive:</p>
                    <p class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
                </div>
                <div class="margin-top-5">
                    <p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Batch Amount:</p>
                    <input type="text" class="t-small display-inline-mid">
                    <p class="display-inline-mid font-400 font-14 margin-top-15">KG</p>
                </div>
                <div class="margin-top-10">
                    <p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Receiving Balance:</p>
                    <p class="font-14 font-400 no-margin-left display-inline-mid red-color">20 KG</p>
                </div>
            </div>
            <p class="no-margin-left font-14 font-500 margin-top-10">Storage Location:</p>
            <p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>
            <div class="bggray-white height-300px overflow-y-auto">
                <div class="padding-all-10 padding-bottom-10 border-bottom-small border-black">
                    <i class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small  padding-right-10 border-black "></i>
                    <p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5">Warehouse 1</p>
                    <span class="display-inline-mid padding-all-5">&gt;</span>
                    <p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid">Area 1</p>
                </div>
                <div class="font-0">
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor ">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 1</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 2</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 3</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 4</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 5</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 6</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 7</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 8</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 9</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 10</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 11</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 12</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 13</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                    <div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
                        <div class="display-inline-mid width-20percent overflow-hide half-border-radius">
                            <img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
                        </div>
                        <div class="display-inline-mid">
                            <p class="font-14 font-400">Area 14</p>
                            <p class="font-12 font-400 italic">Qty in Location 100 KG</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer text-right">
            <button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
            <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
        </div>
    </div>
</div>
<!--End of Add Batch MODAL-->
<div class="modal-container " modal-id="add-batch">
    <div class="modal-body medium margin-top-100 margin-bottom-100">
        <div class="modal-head">
            <h4 class="text-left">Add Batch</h4>
            <div class="modal-close close-me"></div>
        </div>
        <!-- content -->
        <div class="modal-content text-left">
            <div class="text-left">
                <div class="">
                    <p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Item:</p>
                    <p class="font-14 font-400 no-margin-left display-inline-mid product-name-sku"></p>
                </div>
                <div class="">
                    <p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per ">Batch Name:</p>
                    <input type="text" class="t-small no-margin-left input-batch-name" placeholder="Batch Name">
                </div>
                <div class="">
                </div>
                <div class="margin-top-10">
                    <p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Amount to Receive:</p>
                    <p class="font-14 font-400 no-margin-left display-inline-mid to-receive"> </p>
                </div>
                <div class="margin-top-5">
                    <p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Batch Amount:</p>
                    <input type="text" class="t-small display-inline-mid input-batch-amount" placeholder="Batch amount">
                    <p class="display-inline-mid font-400 font-14 margin-top-15 batch-amount-uom"></p>
                </div>
                <div class="margin-top-10">
                    <p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Receiving Balance:</p>
                    <p class="font-14 font-400 no-margin-left display-inline-mid red-color receiving-balance"> </p>
                </div>
            </div>
            <p class="no-margin-left font-14 font-500 margin-top-10">Storage Location:</p>
            <p class="no-margin-all padding-bottom-10">Click a Location to Select It. Double-click to go to its sub-locations</p>
            <div class="bggray-white height-300px overflow-y-auto">
                <div class="padding-all-10 height-50px padding-bottom-10 border-bottom-small border-black" id="breadCrumbsHierarchy">
                </div>
                <div class="font-0" id="batchDisplay">
                </div>
            </div>
        </div>
        <div class="modal-footer text-right">
            <button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
            <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20" id="confirmAddBatch">Confirm</button>
        </div>
    </div>
</div>

<div class="modal-container" modal-id="no-receiving-data">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">No receiving company goods found, redirecting to list</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>
<div class="modal-container" modal-id="generate-view">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Generating view, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>

<div class="modal-container" modal-id="complete-product-details">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color">Completing the item details, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>


<div class="modal-container complete-product-modal" modal-id="confirm-complete-product">
    <div class="modal-body small ">
        <div class="modal-head">
            <h4 class="text-left">Complete Item Details</h4>
            <div class="modal-close close-me"></div>
        </div>
        <!-- content -->
        <div class="modal-content text-left">
            <div class="padding-all-10">
                <p class="font-14 font-400">Are you sure you want to complete the item details ?</p>
            </div>
        </div>
        <div class="modal-footer text-right">   
            <button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>                 
            <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 submit-btn">Confirm</button>                                   
        </div>
    </div>
</div>

<div class="modal-container" modal-id="complete-single-product-details">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color message">Completing the <i></i> item details, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>

<div class="modal-container" modal-id="modal-update-product-details">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Message</h4>
        </div>
        <!-- content -->
        <div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
            <div class="padding-all-10 bggray-7cace5">
                <p class="font-16 font-400 white-color message">Updating item details, please wait...</p>
            </div>
        </div>
        <div class="modal-footer text-right" style="height: 50px;">
        </div>
    </div>
</div>


<div class="modal-container " modal-id="complete-receiving-modal">
    <div class="modal-body small ">
        <div class="modal-head">
            <h4 class="text-left">Complete Receiving Record</h4>
            <div class="modal-close close-me"></div>
        </div>
        <!-- content -->
        <div class="modal-content text-left">
            <div class="padding-all-10">
                <p class="font-14 font-400">Are you sure you want to complete the receiving record ?</p>
            </div>
        </div>
        <div class="modal-footer text-right">   
            <button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>                 
            <button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 submit-btn">Confirm</button>                                   
        </div>
    </div>
</div>