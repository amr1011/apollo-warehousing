<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL; ?>receiving/consignee_receiving">Receiving - Consignee Goods</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color" id="display-edit-consignee-vessel-receivingNum-head">Edit Record No. 1234567890</li>
				</ul>
			</div>

			<form id="EditReceivingConsigneeGoodsForm">
				<div class="semi-main">

					<div class="padding-top-30 margin-bottom-30 text-right width-100percent">
						<a href="<?php echo BASEURL; ?>receiving/consignee_ongoing_vessel">
							<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
						</a>
						<!-- <a href="<?php echo BASEURL; ?>receiving/consignee_ongoing_vessel"> -->
						<a href="javascript:void(0)">
							<button class="btn general-btn " id="editSaveNewConsigneeGoods" >Save Changes</button>
						</a>
					</div>
					<div class="clear"></div>

					<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0">
						<p class="font-20 font-bold black-color padding-bottom-20" id="display-edit-consignee-vessel-receivingNum">Receiving No. 1234567890</p>

						<div class="width-50percent display-inline-top">
							<div class="">
								<p class="font-14 font-bold display-inline-mid width-120">PO No.: </p>
								<input type="text" class="display-inline-mid t-medium margin-left-20"  id="display-edit-consignee-vessel-poNum" value="">
							</div>										
						</div>

						<div class="width-50percent display-inline-top">
							<div class="">
								<p class="font-14 font-bold display-inline-top width-120 margin-top-5">Mode of Delivery: </p>
								<p class="font-14 font-bold display-inline-top width-120 margin-top-5">Vessel</p>
								<!-- in -->

								<!-- <div class="select large margin-left-20 mode-of-deliver">
									<select id="display-edit-vessel-mode-of-delivery">
										<option value="vessel">Vessel</option>
										<option value="truck">Truck</option>
									</select>
								</div> -->
							</div>

							
						</div>
					</div>
					
					<!-- for vessel  -->
					<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0	">
						<p class="font-20 font-bold black-color padding-bottom-20">Vessel Details</p>

						<div class="width-40percent display-inline-top">
							<div class="padding-bottom-10">
								<p class="font-14 font-bold display-inline-mid width-120">Vessel Name: </p>
								<!-- <input type="text" class="display-inline-mid t-medium margin-left-20" id="display-edit-vessel-name" datavalid="required" value="MV CHINO"> -->
								<div class="select large display-vessel-name margin-left-20" style="width: 250px;">
									<select datavalid="required" id="display-edit-vessel-name">
										<option value="">Select Vessel Name</option>
									</select>
								</div>
							</div>

							<div class="padding-top-10 padding-bottom-10">
								<p class="font-14 font-bold display-inline-top width-120 margin-top-10">Hatches: </p>
								<input type="text" class="display-inline-mid t-medium margin-left-20" id="display-edit-vessel-hatches" datavalid="required" value="4">
							</div>

							<div class="padding-top-10 padding-bottom-10">
								<p class="font-14 font-bold display-inline-top width-120 margin-top-10">Vessel Type: </p>
								<input type="text" class="display-inline-mid t-medium margin-left-20" id="display-edit-vessel-type"  datavalid="required" value="Cargo Ship">
							</div>

							<div class="padding-top-10 padding-bottom-10">
								<p class="font-14 font-bold display-inline-top width-120 margin-top-10">Vessel Captain: </p>
								<input type="text" class="display-inline-mid t-medium margin-left-20" id="display-edit-vessel-captain" datavalid="required" value="Akehito Nakamura">
							</div>

						</div>

						<div class="width-55percent display-inline-top margin-left-30">

							<div class="padding-bottom-10">
								<p class="font-14 font-bold display-inline-mid width-150">Discharge Type: </p>
								<input type="text" class="display-inline-mid  t-medium margin-left-10" id="display-edit-vessel-discharge-type" datavalid="required" value="Pneumatic Unloader">	
							</div>

							<div class="padding-top-10">
								<p class="font-14 font-bold display-inline-mid width-150 padding-top-5">Bill of Lading Volume: </p>
								<input type="text" class="display-inline-mid  t-medium margin-left-10" id="display-edit-vessel-bill-of-landing" datavalid="required" value="14000 MT">	
								<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10 remove-measure-drop">
									<select class="unit-of-measure addBillingMeasure" id="bill-lading-volume-measure"></select>
								</p>												
							</div>

							<div class="padding-top-20">
								<p class="font-14 font-bold display-inline-mid width-150">Berthing Date / Time: </p>
								<div class="input-group italic display-inline-mid fixed-datepicker medium margin-left-10">
									<input class="form-control dp  default-cursor " placeholder="Transfer Date" id="display-edit-vessel-berthing-date"  data-date-format="MM/DD/YYYY" type="text">
									<span class="input-group-addon "><i class="fa fa-calendar"></i></span>
								</div>
								<div class="input-group width-200px italic display-inline-mid fixed-timepicker medium margin-left-10">
							<input class="form-control dp time " placeholder="Transfer Time" id="display-edit-vessel-berthing-time"  type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>
							
							</div>
						</div>
					</div>

					

					<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white padding-bottom-20 ">
						<div class="width-100percent text-left padding-all-30 no-padding-top no-padding-left margin-bottom-30 border-bottom-small border-gray">
							<p class="font-20 font-bold black-color padding-bottom-20 ">Item List</p>
							<p class="font-14 font-bold display-inline-mid padding-right-20">Item: <span class="red-color display-inline-top">*</span></p>
							<div class="select dispaly-inline-mid large setProductListSelect">
								<select id="selectProductListSelect">
									<!-- <option value="Select">Select Item</option> -->
								</select>
							</div>
							<a href="javascript:void(0)" class="display-inline-mid padding-left-20"  id="addItemProductConsigneeBtn">
								<button class="btn general-btn padding-left-20 padding-right-20">Add Item</button>
							</a>
						</div>

						<span id="displayEditProductCurrent">
							<div class="border-full padding-all-20 display-edit-product-current-all display-products-info" style='margin-top: 5px;'>
								<div class="bggray-white text-left">
									<div class="height-100percent display-inline-mid width-10per">
										<img src="../assets/images/corn.jpg" id="" alt="" class="height-100percent width-100percent display-edit-vessel-product-image">
									</div>

									<div class="display-inline-top width-90per padding-left-10">
										<div class="padding-top-15 padding-left-15 padding-bottom-15">
											<p class="font-16 font-bold f-left display-edit-vessel-product-name-sku" id="">SKU# 1234567890 - Japanese Corn</p>
											<div class="f-right width-20px margin-left-10">
												<img src="../assets//images/ui/icon-close.svg"  alt="close" class="width-100percent default-cursor deleteEditSeletedProduct">
											</div>
											<div class="clear"></div>
										</div>																						
									</div>
								</div>					
							</div>
						</span>

						<div id="displayEditReceivingConsigneeProductSelects">
							<!-- <div class="border-full padding-all-20 margin-top-20">
								<div class="bggray-white">
									<div class="height-100percent display-inline-mid width-230px">
										<img src="../assets/images/Australian-wheat.jpg" alt="" class="height-100percent width-100percent">
									</div>

									<div class="display-inline-top width-75percent padding-left-10">
										<div class="padding-all-15 bg-light-gray">
											<p class="font-16 font-bold f-left">SKU# 1234567890 - Australian Wheat </p>
											<div class="f-right width-20px margin-left-10">
												<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
											</div>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 text-left">

											<p class="f-left no-margin-all width-20percent font-bold">Vendor</p>
											<p class="f-left no-margin-all width-20percent">Innova Farming</p>
											<p class="f-left no-margin-all width-20percent font-bold">Loading Method</p>
											
											<div class="f-left">										
												<input type="radio" class="display-inline-mid width-50px default-cursor" name="bag-or-bulk" id="bulk">
												<label for="bulk" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>
											</div>

											<div class="f-left margin-left-10">																	
												<input type="radio" class="display-inline-mid width-50px default-cursor" name="bag-or-bulk" id="piece">
												<label for="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bags</label>
											</div>								
											<div class="clear"></div>
										</div>
										
										
										<div class="padding-all-15 bg-light-gray text-left font-0">
											<p class="display-inline-mid width-20percent font-bold">Aging Days</p>
											<p class="display-inline-mid width-20percent">10 Days</p>
											<p class="display-inline-mid width-25per font-14 font-bold">Qty to Receive</p>
											
											<input type="text" class="display-inline-mid width-70px height-26 t-small" name="bag-or-bulk">
											<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">KG</p>
											
											<input type="text" class="display-inline-mid width-70px height-26 t-small" name="bag-or-bulk">
											<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">Bags</p>
											
											<div class="clear"></div>
										</div>
										
										
										<div class="padding-all-15 text-left">
											<p class="f-left  font-bold">Consignee Distribution:</p>									
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 text-left bg-light-gray">
											<div class="margin-top-10">
												<p class="font-bold display-inline-mid">1.</p>
												<div class="select large display-inline-mid margin-left-20">
													<select>
														<option value="consignee-name">Consignee Name</option>
													</select>
												</div>
												<p class="font-bold display-inline-mid margin-left-30">Quantity</p>
												<input type="text" class="t-small display-inline-mid width-70px margin-left-10">
												<p class="display-inline-mid font-bold margin-left-30">KG</p>
												<input type="text" class="t-small display-inline-mid width-70px margin-left-10">
												<p class="display-inline-mid margin-left-10 font-bold">Bags</p>
												<div class="display-inline-mid width-20px margin-left-10">
													<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
												</div>
											</div>
											<div class="margin-top-10">
												<p class="font-bold display-inline-mid">2.</p>
												<div class="select large display-inline-mid margin-left-20">
													<select>
														<option value="consignee-name">Consignee Name</option>
													</select>
												</div>
												<p class="font-bold display-inline-mid margin-left-30">Quantity</p>
												<input type="text" class="t-small display-inline-mid width-70px margin-left-10">
												<p class="display-inline-mid font-bold margin-left-30">KG</p>
												<input type="text" class="t-small display-inline-mid width-70px margin-left-10">
												<p class="display-inline-mid margin-left-10 font-bold">Bags</p>
												<div class="display-inline-mid width-20px margin-left-10">
													<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
												</div>
											</div>
											<div class="margin-top-10">
												<p class="font-bold display-inline-mid">3.</p>
												<div class="select large display-inline-mid margin-left-20">
													<select>
														<option value="consignee-name">Consignee Name</option>
													</select>
												</div>
												<p class="font-bold display-inline-mid margin-left-30">Quantity</p>
												<input type="text" class="t-small display-inline-mid width-70px margin-left-10">
												<p class="display-inline-mid font-bold margin-left-30">KG</p>
												<input type="text" class="t-small display-inline-mid width-70px margin-left-10">
												<p class="display-inline-mid margin-left-10 font-bold">Bags</p>
												<div class="display-inline-mid width-20px margin-left-10">
													<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
												</div>
											</div>									
										</div>
										<a href="#" class="f-right margin-top-10 font-500">+ Add Consignee</a>
										<div class="clear"></div>
										
									</div>
								</div>
								
							</div> -->
						</div>

						

					</div>

				
				</div>
			</form>
	</div>