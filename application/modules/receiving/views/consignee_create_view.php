


<div class="main">
		<div class="breadcrumbs no-margin-left padding-left-50">			
			<ul>
				<li><a href="<?php echo BASEURL; ?>receiving/consignee_receiving">Receiving - Consignee Goods</a></li>
				<li><span>&gt;</span></li>						
				<li class="font-bold black-color">Create Record</li>
			</ul>
		</div>


		
			<div class="semi-main">

				<form id="addReceivingConsigneeGoodsForm">

					<div class="padding-top-30 margin-bottom-30 text-right width-100percent">
						<!-- <a href="<?php echo BASEURL; ?>receiving/consignee_receiving"> -->
							<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color cancel-creating-record">Cancel</button>
						<!-- </a> -->
						<button  class="btn general-btn  confirmSaveNewConsigneeGoods">Create Record</button>
					</div>
					<div class="clear"></div>

					<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0">
						<p class="font-20 font-500 black-color padding-bottom-20">Receiving No. <?php echo $receiving_number; ?></p>
						<input type="hidden" id="setReceivingNumber" value="<?php echo $receiving_number; ?>">

						<div class="width-50percent display-inline-top">
							<div class="">
								<p class="font-14 font-bold display-inline-mid width-120">PO No.: </p>
								<input type="text" id="addInputPoNumber"   class="display-inline-mid t-medium margin-left-20">
							</div>										
						</div>

						<div class="width-50percent display-inline-top">
							<div class="">
								<p class="font-14 font-bold display-inline-top width-120 margin-top-5">Mode of Delivery: </p>
								<div class="select large margin-left-20 mode-of-deliver">
									<select id="addSelectModeDelivery" datavalid="required">
										<!-- <option value="">Select Mode of Delivery</option> -->
										<option value="truck">Truck</option>
										<option value="vessel">Vessel</option>
									</select>
								</div>
							</div>					
						</div>
					</div>
					
					<!-- for vessel  -->
					<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0 vessel-delivery">
						<p class="font-20 font-500 black-color padding-bottom-20">Vessel Details</p>

						<div class="width-40percent display-inline-top">
							<div class="padding-bottom-10">
								<p class="font-14 font-bold display-inline-mid width-120">Vessel Name: </p>
								<!-- <input type="text" id="addInputVesselName" class="display-inline-mid t-medium margin-left-20"> -->
								<div class="select large margin-left-20" style="width: 250px;">
									<select datavalid="required" id="addInputVesselName">
										<!-- <option value="">Select Vessel Name</option> -->
									</select>
								</div>
								
							</div>

							<div class="padding-top-10 padding-bottom-10">
								<p class="font-14 font-bold display-inline-top width-120 margin-top-10">Hatches: </p>
								<input type="text" id="addInputVesselHatches" class="display-inline-mid t-medium margin-left-20" >
							</div>

							<div class="padding-top-10 padding-bottom-10">
								<p class="font-14 font-bold display-inline-top width-120 margin-top-10">Vessel Type: </p>
								<input type="text" id="addInputVesselType" class="display-inline-mid t-medium margin-left-20">
							</div>

							<div class="padding-top-10 padding-bottom-10">
								<p class="font-14 font-bold display-inline-top width-120 margin-top-10">Vessel Captain: </p>
								<input type="text" id="addInputVesselCaptain" class="display-inline-mid t-medium margin-left-20">
							</div>

						</div>

						<div class="width-55percent display-inline-top margin-left-30">

							<div class="padding-bottom-10">
								<p class="font-14 font-bold display-inline-mid width-150">Discharge Type: </p>
								<input type="text" id="addInputVesselDischargeType" class="display-inline-mid  t-medium margin-left-10">
							</div>

							<div class="padding-top-10">
								<p class="font-14 font-bold display-inline-mid width-150 padding-top-5">Bill of Lading Volume: </p>
								<input type="text" id="addInputVesselLandingVolume" class="display-inline-mid  t-medium margin-left-10">	
								<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10 remove-measure-drop">
									<select class="unit-of-measure addBillingMeasure" id="bill-lading-volume-measure"></select>
								</p>											
							</div>

							<div class="padding-top-20">
								<p class="font-14 font-bold display-inline-mid width-150">Berthing Date / Time: </p>
								<div class="input-group italic display-inline-mid fixed-datepicker medium margin-left-10">
									<input class="form-control dp  default-cursor " id="addInputVesselBerthingDate" placeholder="Transfer Date" data-date-format="MM/DD/YYYY" type="text">
									<span class="input-group-addon "><i class="fa fa-calendar"></i></span>
								</div>
								<div class="input-group width-200px italic display-inline-mid fixed-timepicker medium margin-left-10">
							<input class="form-control dp time " placeholder="Transfer Time" id="addInputVesselTransferTime" type="text">
							<span class="input-group-addon "><span class="fa fa-clock-o font-20"></span></span>
						</div>
							
							</div>
						</div>
					</div>

					<!-- for trucking  -->
					<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white text-left font-0 truck-delivery">
						<p class="font-20 font-500 black-color padding-bottom-20">Trucking Details</p>

						<div class="width-40percent display-inline-top">
							<div class="padding-bottom-10">
								<p class="font-14 font-bold display-inline-mid width-120">Vessel Origin: </p>
								<!-- <input type="text" id="addInputTruckOrigin" class="display-inline-mid t-medium margin-left-20"> -->
								<div class="select large margin-left-20" style="width: 250px;">
									<select datavalid="required" id="addInputTruckOrigin">
										<!-- <option value="">Select Vessel Name</option> -->
									</select>
								</div>
							</div>

							<div class="padding-top-10 padding-bottom-10">
								<p class="font-14 font-bold display-inline-top width-120 margin-top-10">Trucking: </p>
								<input type="text" id="addInputTruckings" class="display-inline-mid t-medium margin-left-20" >
							</div>

							<div class="padding-top-10 padding-bottom-10">
								<p class="font-14 font-bold display-inline-top width-120 margin-top-10">Plate Number: </p>
								<input type="text" id="addInputTruckPlateNumber" class="display-inline-mid t-medium margin-left-20">
							</div>

						</div>

						<div class="width-55percent display-inline-top margin-left-30">

							<div class="padding-bottom-10">
								<p class="font-14 font-bold display-inline-mid width-150">Driver Name: </p>
								<input type="text" id="addInputTruckDriverName" class="display-inline-mid  t-medium margin-left-10">
							</div>

							<div class="padding-top-10">
								<p class="font-14 font-bold display-inline-mid width-150">License No.: </p>
								<input type="text" id="addInputTruckLicenseNum" class="display-inline-mid  t-medium margin-left-10">												
							</div>
															
						</div>
					</div>


					<div class="border-top border-blue box-shadow-dark padding-all-20 margin-bottom-20 bggray-white padding-bottom-20 ">
						<div class="width-100percent text-left padding-all-30 no-padding-top no-padding-left margin-bottom-30 border-bottom-small border-gray">
							<p class="font-20 font-500 black-color padding-bottom-20 ">Product List</p>
							<p class="font-14 font-bold display-inline-mid padding-right-20">Item: <span class="red-color display-inline-top">*</span></p>
							<div class="select dispaly-inline-mid large setProductListSelect">
								<select id="selectProductListSelect" datavalid="required">
									<!-- <option value="">Select Product Item</option> -->
								</select>
							</div>
							<a href="javascript:void(0)" class="display-inline-mid padding-left-20">
								<button id="addItemProductConsigneeBtn" class="btn general-btn padding-left-20 padding-right-20">Add Item</button>
							</a>
						</div>

						<div id="displayReceivingConsigneeProductSelects">
							
						
							<!-- <div class="border-full padding-all-20">
								<div class="bggray-white">
									<div class="height-100percent display-inline-mid width-230px">
										<img src="../assets/images/corn.jpg" alt="" class="height-100percent width-100percent">
									</div>

									<div class="display-inline-top width-75percent padding-left-10">
										<div class="padding-all-15 bg-light-gray">
											<p class="font-16 font-bold f-left">SKU# 1234567890 - Japanese Corn</p>
											<div class="f-right width-20px margin-left-10">
												<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
											</div>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 text-left">

											<p class="f-left no-margin-all width-20percent font-bold">Vendor</p>
											<p class="f-left no-margin-all width-20percent">Innova Farming</p>
											<p class="f-left no-margin-all width-20percent font-bold">Loading Method</p>
											
											<div class="f-left">										
												<input type="radio" class="display-inline-mid width-50px default-cursor" name="bag-or-bulk" id="bulk">
												<label for="bulk" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bulk</label>
											</div>

											<div class="f-left margin-left-10">																	
												<input type="radio" class="display-inline-mid width-50px default-cursor" name="bag-or-bulk" id="piece">
												<label for="piece" class="display-inline-mid font-14 margin-top-5 default-cursor">By Bags</label>
											</div>								
											<div class="clear"></div>
										</div>
										
										
										<div class="padding-all-15 bg-light-gray text-left font-0">
											<p class="display-inline-mid width-20percent font-bold">Aging Days</p>
											<p class="display-inline-mid width-20percent">10 Days</p>
											<p class="display-inline-mid width-25per font-14 font-bold">Qty to Receive</p>									
											
											<input type="text" class="display-inline-mid width-70px height-26 t-small" name="bag-or-bulk">
											<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">KG</p>
											
											<input type="text" class="display-inline-mid width-70px height-26 t-small" name="bag-or-bulk">
											<p class="font-14 font-400 display-inline-mid padding-left-10 padding-right-10">Bags</p>
											
											<div class="clear"></div>

											
										</div>
										
										
										
										<div class="padding-all-15 text-left">
											<p class="f-left  font-bold">Consignee Distribution:</p>									
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 text-left bg-light-gray">
											<div class="margin-top-10">
												<p class="font-bold display-inline-mid">1.</p>
												<div class="select large display-inline-mid margin-left-20">
													<select>
														<option value="consignee-name">Consignee Name</option>
													</select>
												</div>
												<p class="font-bold display-inline-mid margin-left-30">Quantity</p>
												<input type="text" class="t-small display-inline-mid width-70px margin-left-20">
												<p class="display-inline-mid font-300 margin-left-10 margin-right-15">KG</p>
												<input type="text" class="t-small display-inline-mid width-70px margin-left-10">
												<p class="display-inline-mid margin-left-10 font-300">Bags</p>
												<div class="display-inline-mid width-20px margin-left-10">
													<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
												</div>
											</div>
											<div class="margin-top-10">
												<p class="font-bold display-inline-mid">2.</p>
												<div class="select large display-inline-mid margin-left-20">
													<select>
														<option value="consignee-name">Consignee Name</option>
													</select>
												</div>
												<p class="font-bold display-inline-mid margin-left-30">Quantity</p>
												<input type="text" class="t-small display-inline-mid width-70px margin-left-20">
												<p class="display-inline-mid font-300 margin-left-10 margin-right-15">KG</p>
												<input type="text" class="t-small display-inline-mid width-70px margin-left-10">
												<p class="display-inline-mid margin-left-10 font-300">Bags</p>
												<div class="display-inline-mid width-20px margin-left-10">
													<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
												</div>
											</div>
											<div class="margin-top-10">
												<p class="font-bold display-inline-mid">3.</p>
												<div class="select large display-inline-mid margin-left-20">
													<select>
														<option value="consignee-name">Consignee Name</option>
													</select>
												</div>
												<p class="font-bold display-inline-mid margin-left-30">Quantity</p>
												<input type="text" class="t-small display-inline-mid width-70px margin-left-20">
												<p class="display-inline-mid font-300 margin-left-10 margin-right-15">KG</p>
												<input type="text" class="t-small display-inline-mid width-70px margin-left-10">
												<p class="display-inline-mid margin-left-10 font-300">Bags</p>
												<div class="display-inline-mid width-20px margin-left-10">
													<img src="../assets//images/ui/icon-close.svg" alt="close" class="width-100percent default-cursor">
												</div>
											</div>									
										</div>
										<a href="#" class="f-right margin-top-10 font-500">+ Add Consignee</a>
										<div class="clear"></div>
										
									</div>
								</div>
								
							</div> -->
						</div>

					</div>
				</form>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400 ">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title padding-top-10 font-500 black-color font-20"> Documents</h4>
							</a>

							<div class="f-right">
								<button class="btn general-btn modal-trigger" modal-target="upload-documents" >Upload</button>
							</div>
							<div class="clear"></div>

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-20">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>
								<span id="displayDocuments"></span>	

									<!-- =<div class="table-content position-rel tbl-dark-color">
									<div class="content-show padding-all-10">
										<div class="width-85per display-inline-mid">
											<i class="fa fa-file-pdf-o font-30 display-inline-mid width-50px"></i>
											<p class=" display-inline-mid">RandomDocument </p>
										</div>
										<p class=" display-inline-mid ">22-Oct-2015</p>
									</div>
									<div class="content-hide">
									
										<button class="btn general-btn padding-left-30 padding-right-30">Remove File</button>	
									</div>
								</div> -->								
							</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-20 black-color padding-top-10"> Notes</h4>
							</a>
							<div class="f-right">
								<button id="AddReceivingConsigneeNote" class="btn general-btn modal-trigger" modal-target="add-notes">Add Notes</button>
							</div>
							<div class="clear"></div>
						</div>
						
					
						
						<div class="panel-collapse collapse in">
							<div id="AddReceivingConsigneeNotesDisplay" class="panel-body padding-all-20">
								
							</div>
						</div>
					</div>
				</div>
				<div class="width-100percent text-right border-top-small border-gray padding-top-10">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color cancel-creating-record">Cancel</button>
					<button class="confirmSaveNewConsigneeGoods btn general-btn" >Create Record</button>
				</div>

			</div>
	
	</div>

	<!--MODALS-->

	<!--Start of Confirm Create Record MODAL-->
	<div class="modal-container" id="SaveconfirmCreateRecord" >
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Create Record</h4>				
				<div class="modal-close close-me" style="display: none;"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left padding-top-20 padding-bottom-20 width-100percent">
				<div class="padding-all-10 bggray-7cace5">
					<p class="font-14 font-400 white-color" id="displayReceiveConsigneeGoodsId">Receiving No. 1234567890 has been created.</p>
				</div>
			</div>
		
			<div class="modal-footer text-right">
				<a href="" id="create-link-to-go">
					<button type="button" id="save-receiving-number" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20 " >Show Record</button>
				</a>
			</div>
		</div>	
	</div>
	<!--End of Confirm Create Record MODAL-->

	<!--Start of Confirm Create Record MODAL-->
	<div class="modal-container" id="confirmCreateRecord">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Create Record</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<p class="font-14 font-400">Are you sure you want to create Receiving record No. <?php echo $receiving_number; ?>?</p>
			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20  addSaveNewConsigneeGoods" >Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Confirm Create Record MODAL-->

	

	

	<!--Start of Add Notes MODAL-->
	<div class="modal-container" modal-id="add-notes">
		<div class="modal-body small">				

			<div class="modal-head">
				<h4 class="text-left">Add Notes</h4>				
				<div class="modal-close close-me"></div>
			</div>
			<form id="addReceivingConsigneeNoteForm">
				<!-- content -->
				<div class="modal-content text-left">
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all display-inline-mid width-60px">Subject:</p>
						<input type="text" id="AddReceivingConsigneeNoteSubject" datavalid="required"  class="width-380px display-inline-mid margin-left-10">
					</div>
					<div class="text-left padding-bottom-10">
						<p class="font-14 font-400 no-margin-all display-inline-top width-60px">Message:</p>
						<textarea id="AddReceivingConsigneeNoteMessage" datavalid="required" class="width-380px margin-left-10 height-250px"></textarea>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<button type="button" id="addRecievingConsigneeNotesSave" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
				</div>
			</form>
		</div>	
	</div>
	<!--End of Add Notes MODAL-->

	<!--Start of Add Batch MODAL-->
	<div class="modal-container" modal-id="add-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Add Batch</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Product:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Holcim Cement</p>
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Transfer Balance:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per margin-top-15">Qty to Transfer:</p>
						<input type="text" class="t-medium no-margin-left">
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per">Batch Name: </p>
						<div class="select width-250px">
							<select>
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div>
					<div class="padding-top-30">
						<p class="font-14 font-500 no-margin-left">Origin</p>
						<p class="font-14 font-500 display-inline-mid width-25per margin-top-15 no-margin-right">Storage Location:</p>
						<div class="select width-250px">
							<select>
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div>
						
					</div>
				</div>
				<p class="font-14 font-500">Sub Location:</p>
				<div class="width-100percent bggray-white box-shadow-dark">

					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-75percent">Location Address</th>
								<th class="black-color width-25percent">Location Qty</th>
							</tr>
						</thead>
					</table>
					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22"></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>
				</div>

				<p class="font-14 font-500 no-margin-left margin-top-20">Destination</p>
				<p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>

				<div class="bggray-white height-300px overflow-y-auto">
					<div class="padding-all-10 padding-bottom-10 border-bottom-small border-black">
						<i class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small  padding-right-10 border-black "></i>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5">Warehouse 1</p>
						<span class="display-inline-mid padding-all-5">&gt;</span>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid">Area 1</p>
					</div>
					<div class="font-0">
						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor ">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 1</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 2</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 3</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 4</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 5</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 6</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 7</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 8</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 9</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 10</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 11</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 12</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 13</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 14</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

					</div>
				</div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!--End of Add Batch MODAL-->

	<!-- Start of Edit Batch Modal  -->
	<div class="modal-container" modal-id="edit-batch">
		<div class="modal-body medium margin-top-100 margin-bottom-100">				

			<div class="modal-head">
				<h4 class="text-left">Edit Batch</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content text-left">
				<div class="text-left padding-bottom-10">
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Product:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">SKU No. 1234567890 - Holcim Cement</p>
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per">Transfer Balance:</p>
						<p class="font-14 font-400 no-margin-left display-inline-mid ">90 KG</p>
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left display-inline-mid width-25per margin-top-15">Qty to Transfer:</p>
						<input type="text" class="t-medium no-margin-left">
					</div>
					<div class="padding-bottom-10">
						<p class="font-14 font-500 no-margin-left margin-top-15 display-inline-mid width-25per">Batch Name: </p>
						<div class="select width-250px">
							<select>
								<option value="CNEKIEK12345"> CNEKIEK12345</option>
							</select>
						</div>
					</div>
					<div class="padding-top-30">
						<p class="font-14 font-500 no-margin-left">Origin</p>
						<p class="font-14 font-500 display-inline-mid width-25per margin-top-15 no-margin-right">Storage Location:</p>
						<div class="select width-250px">
							<select>
								<option value="CNEKIEK12345"> Warehouse 1</option>
							</select>
						</div>
						
					</div>
				</div>
				<p class="font-14 font-500">Sub Location:</p>
				<div class="width-100percent bggray-white box-shadow-dark">

					<table class="tbl-4c3h ">
						<thead>
							<tr>
								<th class="black-color width-75percent">Location Address</th>
								<th class="black-color width-25percent">Location Qty</th>
							</tr>
						</thead>
					</table>
					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22"></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1</p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>

					<div class="bg-light-gray font-0 text-center location-check position-rel default-cursor">
						<div class="display-inline-mid height-100percent bggray-7cace5 location-address-check padding-top-15">
							<i class="fa fa-check font-22 "></i>
						</div>
						<div class="width-75percent display-inline-mid padding-all-20">
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Area 1  </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Storage 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Shelf   </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Rack 1 </p>
							<i class="display-inline-mid font-14 padding-right-10">&gt;</i>
							<p class="font-14 font-400 display-inline-mid no-margin-all padding-right-10"> Pallete 1 </p>
							<div class="clear"></div>
						</div>
						<div class="width-25percent display-inline-mid">
							<p class="font-14 font-400 no-margin-all ">1000 KG</p>
						</div>
					</div>
				</div>

				<p class="font-14 font-400 no-margin-left margin-top-20">Destination</p>
				<p class="font-12 no-margin-left">Click a location to select it. Double click to go to its sub locations.</p>

				<div class="bggray-white height-300px overflow-y-auto">
					<div class="padding-all-10 padding-bottom-10 border-bottom-small border-black">
						<i class="fa fa-arrow-circle-left font-22 display-inline-mid border-right-small  padding-right-10 border-black "></i>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid bggray-7cace5">Warehouse 1</p>
						<span class="display-inline-mid padding-all-5">&gt;</span>
						<p class="padding-all-10 font-14 font-400 no-margin-all display-inline-mid">Area 1</p>
					</div>
					<div class="font-0">
						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor ">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 1</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 2</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 3</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 4</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 5</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 6</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 7</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 8</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 9</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 10</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 11</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 12</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 13</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

						<div class="area width-50percent padding-all-10 padding-left-20 display-inline-mid default-cursor">
							<div class="display-inline-mid width-20percent overflow-hide half-border-radius">
								<img src="../assets/images/profile/profile_p.png" alt="images" class="width-100percent">
							</div>
							<div class="display-inline-mid">
								<p class="font-14 font-400">Area 14</p>
								<p class="font-12 font-400 italic">Qty in Location 100 KG</p>
							</div>
						</div>

					</div>
				</div>

			</div>
		
			<div class="modal-footer text-right">
				<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
				<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-20 padding-right-20">Confirm</button>
			</div>
		</div>	
	</div>
	<!-- end of edit batch modal  -->



