<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL; ?>receiving/consignee_receiving">Consignee - Receiving</a></li>
					<li><span>&gt;</span></li>						
					<li class=" black-color bread-show font-bold" id="display-complete-receiving-number">Receiving No. 1234567890 (Complete)</li>
					
				</ul>
			</div>
			<div class="semi-main">
				
				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white">
					<div class="panel-group">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color f-left" href="#">								
								<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10  active"> Receiving Information</h4>
							</a>
							<div class="clear"></div>
						</div>

						<div class="panel-collapse collapse in ">
							<div class="panel-body">
								<div class="display-inline-mid width-50per margin-top-10">
									<div class="text-left">
										<p class="f-left font-14 font-bold width-40percent margin-top-2">PO No.:</p>
										<p class="f-left font-16 font-bold 0width-50percent " id="display-complete-receiving-purchase-order">123456</p>
										<div class="clear"></div>
									</div>

									<div class="text-left padding-top-10">
										<p class="f-left font-14 font-bold width-40percent margin-top-2">Mode of Delivery:</p>
										<p class="f-left font-16 font-bold width-50percent">Truck</p>
										<div class="clear"></div>
									</div>					
								</div>

								<div class="display-inline-top width-50per margin-top-10">
									<div class="text-left">
										<p class="f-left font-14 font-bold width-40percent margin-top-2">Date and Time Issued:</p>
										<p class="f-left font-16 font-bold width-50percent" id="display-complete-receiving-date-issued">September 10, 2015 | 08:25 AM</p>
										<div class="clear"></div>
									</div>
							
									<div class="text-left padding-top-10">
										<p class="f-left font-14 font-bold width-40percent margin-top-2">Status:</p>
										<p class="f-left font-16 font-bold width-50percent">Complete</p>
										<div class="clear"></div>
									</div>

								</div>

								<div class="width-100per margin-top-30 padding-top-10 text-left border-top-light">
									<h4 class=" font-500 font-18 black-color padding-top-10 "> Delivery Information</h4>

									<div class="display-inline-top width-50per margin-top-20 ">
										<div class="padding-all-15 width-100percent bg-light-gray ">
											<p class="font-bold	f-left width-45percent margin-left-10">Vessel Origin</p>
											<p class="f-left width-50percent font-400" id="display-complete-receiving-vessel-name">Japan</p>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 width-100percent ">
											<p class="font-bold	f-left width-45percent margin-left-10">Trucking</p>
											<p class="f-left width-50percent font-400" id="display-complete-receiving-trucking">MutherTruckers Inc.</p>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 width-100percent bg-light-gray">
											<p class="font-bold	f-left width-45percent margin-left-10">Driver Name</p>
											<p class="f-left width-50percent font-400" id="display-complete-receiving-driver-name">Ademar Jaime Urbina</p>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 width-100percent ">
											<p class="font-bold	f-left width-45percent margin-left-10">License Name</p>
											<p class="f-left width-50percent font-400" id="display-complete-receiving-license-number">F23-15-357346</p>
											<div class="clear"></div>
										</div>

										<div class="padding-all-15 width-100percent bg-light-gray">
											<p class="font-bold	f-left width-45percent margin-left-10">Plate Number</p>
											<p class="f-left width-50percent font-400" id="display-complete-receiving-plate-number">MTR2345</p>
											<div class="clear"></div>
										</div>
									</div>

									<div class="display-inline-top width-50per  margin-top-20 f-right">
										<div class="padding-all-15 width-100percent bg-light-gray">
											<p class="font-bold	f-left width-45percent margin-left-10 ">TARE IN</p>
											<p class="f-left width-45percent  margin-left-15" id="display-complete-receiving-tare-in">2500 MT</p>
											<div class="clear"></div>
										</div>
										<div class="padding-all-15 width-100percent ">
											<p class="font-bold	f-left width-45percent margin-left-10 ">TARE OUT</p>
											<p class="f-left width-45percent  margin-left-15" id="display-complete-receiving-tare-out">2000 MT</p>
											<div class="clear"></div>
										</div>
										<div class="padding-all-15 width-100percent bg-light-gray">
											<p class="font-bold	f-left width-45percent  margin-left-10">Net Weight</p>
											<p class="f-left width-45percent  margin-left-15" id="display-complete-receiving-net-weight">500 MT</p>
											<div class="clear"></div>
										</div>

									</div>
									<div class="clear"></div>									
								</div>

							</div>
						</div>
					</div>						
				</div>


				<span id="displayProductItemList">
					<div class="border-top border-blue box-shadow-dark margin-bottom-20 record-item bggray-white item-template-product ">
						<div class="head panel-group text-left" >
							<div class="panel-heading font-14 font-400 margin-top-5 margin-bottom-5">
								<a class="colapsed black-color " href="#">								
									<h4 class="panel-title font-500 font-20 black-color padding-top-10 padding-bottom-10 display-consignee-sku-name "> SKU #1234567890 - Japanese Corn</h4>
								</a>
							</div>
							<div class="panel-collapse collapse in">
								<div class="panel-body padding-all-20 " >

									<div class="bggray-white border-full padding-all-20 height-190px box-shadow-dark font-0">
										<div class="height-100percent display-inline-mid width-25percent ">
										<img src="" alt="" class="height-100percent display-consignee-prod-img" 
										style="background-position: center;background-size: contain;width: 228px;height: 150px !important;background-repeat: no-repeat;">
										<!-- <img src="../assets/images/holcim.jpg" alt="" class="height-100percent width-100percent"> -->
									</div>

										<div class="display-inline-top width-70percent padding-left-10 font-0">
											<div class="width-100percent ">
												<div class="padding-all-15 bg-light-gray">
													<p class="f-left no-margin-all width-25percent font-500">Vendor</p>
													<p class="f-left no-margin-all width-25percent display-consignee-vendor-name" >Innova Farming</p>
													<p class="f-left no-margin-all width-25percent font-500">Loading Method</p>
													<p class="f-left no-margin-all width-25percent display-consignee-loading-method">By Bags</p>
													
													<div class="clear"></div>
												</div>
												
												<div class="padding-all-15">
													<p class="f-left no-margin-all width-25percent font-500">Aging Days</p>
													<p class="f-left no-margin-all width-25percent display-consignee-aging-days">10 Days</p>
													<p class="f-left no-margin-all width-25percent font-500">Qty to Receive</p>
													<p class="f-left no-margin-all width-25percent display-consignee-qtr-to-receive">100 KG (10 Bags)</p>
													<div class="clear"></div>
												</div>
												
											</div>
											
										</div>
									</div>
									
									<!-- consignee distribution  -->
									<div class="padding-top-20" id="navigate-acc">
										<div class="margin-bottom-10 margin-top-10">
											<p class="no-margin-all font-20 font-500  padding-left-10">Consignee Distribution</p>
										</div>
										<table class="tbl-4c3h">
											<thead>
												<tr>
													<th class="width-50percent black-color no-padding-left">Consignee Name</th>
													<th class="width-50percent no-padding-left">Quantity</th>												
												</tr>
											</thead>
										</table>
										<span class="display-edit-consignee-distibution">
											<div class="width-100percent padding-top-15 padding-bottom-15 show-ongoing-content background-change display-edit-consignee-items">
												<div class="width-50percent display-inline-mid text-center">
													<p class="font-14 font-400 display-distribution display-consignee-distribution-name">Alpha Food Inc.</p>
												</div>
												<div class="width-45percent display-inline-mid text-center">
													<p class="font-14 font-400 display-distribution display-consignee-distribution-quantity">50 KG (5 Bags)</p>
												</div>
											</div>
										</span>
									</div> 

								

									<!-- storage assignment display result  -->
									<div class="assignment-display " >
										<div class="padding-top-20">
										<div class="margin-bottom-10 margin-top-10">
											<p class="no-margin-all font-20 font-500 f-left  padding-left-10 padding-top-5">Storage Assignment</p>
											
											<div class="clear"></div>
										</div>
										<table class="tbl-4c3h">
											<thead>
												<tr>
													<th class="width-25percent black-color no-padding-left">Batch Name</th>
													<th class="width-50percent no-padding-left">Storage Location</th>
													<th class="width-25percent black-color no-padding-left">Quantity</th>
													<th class="width-25percent black-color no-padding-left display-bag-per-batch" style="display: none;">Bags</th>
												</tr>
												</thead>
											</table>

											<div class="transfer-prod-location font-0 bggray-white position-rel">
		                                        <div class="width-100percent padding-top-10 padding-bottom-10 show-ongoing-content margin-bottom-20 background-change">
		                                            <div class="width-100percent display-inline-mid text-center">
		                                                <p class="font-14 font-400 italic gray-color">Not Yet Assigned</p>
		                                            </div>
		                                        </div>
		                                    </div>
										</div>
									</div>

									<!-- quantity check  -->
									<div class="margin-bottom-10 margin-top-20 ">
										<p class="no-margin-all font-20 font-500 padding-bottom-10 padding-left-10">Quantity Check</p>
										<div class="width-100percent">
											<div class="padding-all-15 bggray-light">										
												<p class="f-left margin-top-5 width-25percent font-500 padding-left-20">Received Quantity: </p>											
												<p class=" margin-left-10 margin-top-5 f-left width-25percent display-success-receive-qty">110 KG</p>
												<p class="font-bold margin-left-10 f-left width-25percent">Reason for Discrepancy</p>
												<p class="  margin-left-10  f-left width-15percent display-success-reason-discrepancy">Others</p>
												<div class="clear"></div>
											</div>
											<div class="padding-all-15">										
												<p class="f-left margin-top-5 width-25percent font-500 padding-left-20">Discrepancy: </p>											
												<p class=" margin-left-10 margin-top-5 f-left width-25percent display-success-discrepancy">10 KG</p>
												<p class="font-bold margin-left-10 f-left width-25percent ">Remarks:</p>
												<p class="  margin-left-10  f-left width-15percent display-success-discrepancy-remarks">Overhaul of Item from Truck.</p>
												<div class="clear"></div>
											</div>
											
										</div>
											
										
									</div>

										
								</div>
							</div>
						</div>
					</div>
				</span>


				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left padding-bottom-20">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color" href="#">
								<!-- <i class="fa fa-caret-down font-20 black-color"></i> -->
								<h4 class="panel-title  font-500 font-20 black-color padding-top-10"> Documents</h4>
							</a>													
						</div>
						<div class="panel-collapse collapse in">							
							<div class="panel-body padding-all-10">
								<table class="tbl-4c3h">
									<thead>
										<tr>
											<th class="black-color">Document Name</th>
											<th class="black-color">Date</th>
										</tr>
									</thead>
								</table>

								<span id="displayDocuments"></span>	
																
							</div>
						</div>
					</div>
				</div>

				<div class="border-top border-blue box-shadow-dark margin-bottom-20 bggray-white ">
					<div class="head panel-group text-left padding-bottom-20">
						<div class="panel-heading font-14 font-400">
							<a class="colapsed black-color " href="#">
								<h4 class="panel-title font-500 font-20 black-color padding-top-10"> Notes</h4>
							</a>
							
							

						</div>
						<div class="panel-collapse collapse in">
							<div class="panel-body padding-all-10" id="display-receiving-notes">

								<div class="border-full padding-all-10 margin-left-18 margin-top-10 display-receiving-notes-row notes-contain">
									<div class="border-bottom-small border-gray padding-bottom-10">
										<p class="f-left font-14 font-400 display-receiving-notes-subject">Subject: Drivers Note About the meaning of life</p>
										<p class="f-right font-14 font-400 display-receiving-notes-date-created">Date/Time: 25-Oct-2015 01:30 PM</p>
										<div class="clear"></div>
									</div>
									<p class="font-14 font-400 no-padding-left padding-all-10 display-receiving-notes-message show-notes-message" ></p>
									<a href="" class="f-right padding-all-10 font-400 show-notes-messge-length">Show More</a>
									<div class="clear"></div>
								</div>
				
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>