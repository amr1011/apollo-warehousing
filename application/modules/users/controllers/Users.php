<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->library('MY_Form_validation');
        $this->form_validation->CI =& $this;
        $this->load->model("users_model");
        $response = array(
        	'status' => false,
        	'message' => '',
        	'data' => ''
        );
	}

	public function index()
	{
		$user_session = $this->session->userdata('apollo_user');

		if($user_session === null){
			redirect(BASEURL.'login');
		}else{
			redirect(BASEURL.'dashboard');
		}


		/*if(IS_DEVELOPMENT === true)
		{
			$user_development = array(
				"id" => 10000,
				"email" => "apollo@email.com",
				"username" => "apollo",
				"password" => "62cc2d8b4bf2d8728120d052163a77df",
				"company_id" => 1,
				"role_id" => 1,
				"account_type" => 1,
				"first_name" => "apollo",
				"last_name" => "developer",
				"middle_name" => "",
				"image" => BASEURL."assets/images/profile/profile_a.png"
			);

			$this->session->set_userdata('apollo_user', $user_development);

			redirect(BASEURL.'dashboard');
		}
		else
		{
			redirect(BASEURL.'login');
		}*/





	}

	public function search_user_password()
	{
		$user_data = $this->session->userdata('apollo_user');
		$password = md5($this->input->post('password'));
		$if_true = 0;

		if($user_data["password"] == $password)
		{
			$if_true = 1;
		}else{
			$if_true = 0;
		}

		echo $if_true;

	}

	public function get_all_team() {
		$team_result = $this->users_model->get_all_team();
		
		if(!$team_result) {
			
			$response['message'] = 'No team records found.';

		} else {
			
			foreach ($team_result as $key => $team) {
				$count_result = $this->users_model->get_member_count($team['id']);
				$team_result[ $key ]['member_count'] = $count_result['COUNT(*)'];
			}

			$response['status'] = true;
			$response['data'] = $team_result;

		}
		$this->build_response($response);
	}

	public function get_team_details() {
		$team_id = $this->input->get('team_id');

		if($team_id == 'undefined'
		|| $team_id == 'null'
		|| $team_id == '') {

			$response['message'] = 'Please select a team first.';

		} else {

			$team_result = $this->users_model->get_team_details($team_id);

			if(!$team_result) {

				$response['message'] = 'No team record found';

			} else {

				$response['status'] = true;
				$response['data'] = $team_result;

			}
		}
		$this->build_response($response);
	}

	public function get_all_users() {
		$user_result = $this->users_model->get_all_users();

		if(!$user_result) {
			$response['message'] = 'No user records found.';
		} else {
			$response['status'] = true;
			$response['data'] = $user_result;
		}
		$this->build_response($response);
	}

	public function get_user_details() {
		$user_id = $this->input->get('user_id');

		if($user_id == 'undefined'
		|| $user_id == 'null'
		|| $user_id == '') {

			$response['message'] = 'Please select a valid user.';

		} else {

			$user_result = $this->users_model->get_user_details($user_id);

			if(!$user_result) {

				$response['message'] = 'No user data found.';

			} else {

				$user_result[0]['permissions'] = $this->users_model->get_user_permission($user_id);
				$response['status'] = true;
				$response['data'] = $user_result;	

			}

		}
		$this->build_response($response);
	}

	public function get_all_roles() {
		$roles_result = $this->users_model->get_all_roles();

		if(!$roles_result) {
			$response['message'] = 'No role records found.';
		} else {
			$response['status'] = true;
			$response['data'] = $roles_result;
		}
		$this->build_response($response);
	}

	public function get_all_permissions() {
		$permission_result = $this->users_model->get_all_permissions();

		if(!$permission_result) {
			$response['message'] = 'No permission records found.';
		} else {
			$response['status'] = true;
			$response['data'] = $permission_result;
		}
		$this->build_response($response);
	}

	public function get_team_users() {
		$team_id = $this->input->get('team_id');
		// $this->form_validation->set_rules('team_id', 'Team Id', 'trim|required');

		if ($team_id == '' ||
			$team_id == 'undefined' ||
			$team_id == null) {

			$response['message'] = 'Choose a valid team';

		} else {
			
			$user_result = $this->users_model->get_team_users($team_id);

			if(!$user_result) {

				$response['message'] = 'No users found';

			} else {

				$response['status'] = true;
				$response['data'] = $user_result;

			}

		}
		$this->build_response($response);
	}

	public function add_team() {
		$team_name = $this->input->post('team_name');
		$this->form_validation->set_rules('team_name', 'Team Name', 'trim|required|min_length[2]|max_length[50]');

		if (!$this->form_validation->run()) {
			
			$response['message'] = 'Please enter a valid team name.';

		} else {
			
			if($this->check_existing_team($team_name)) {
				
				$response['message'] = 'Team already exists.';

			} else {
				
				$team_result = $this->users_model->add_team($team_name);
				
				if(!($team_result > 0)) {
					
					$response['message'] = 'Team not added.';

				} else {

					$response['status'] = true;
					$response['message'] = 'Team added.';

				}

			}

		}
		$this->build_response($response);
	}

	public function edit_team() {
		$team_id = $this->input->post('team_id');
		$team_name = $this->input->post('team_name');
		$this->form_validation->set_rules('team_id', 'Team Id', 'trim|required');
		$this->form_validation->set_rules('team_name', 'Team Name', 'trim|required');

		if (!$this->form_validation->run()) {
			
			$response['message'] = 'Please select a team or place a name for the team.';

		} else {
			
			$team_result = $this->users_model->edit_team($team_id, $team_name);

			if(!($team_result > 0)) {

				$response['message'] = 'Team not updated.';

			} else {

				$response['status'] = true;
				$response['message'] = 'Team information updated.';

			}

		}
		$this->build_response($response);
	}

	public function delete_team() {
		$team_id = $this->input->post('team_id');
		$this->form_validation->set_rules('team_id', 'Team Id', 'trim|required');

		if (!$this->form_validation->run()) {
			
			$response['message'] = 'Please select a team.';

		} else {
			
			$team_result = $this->users_model->delete_team($team_id);

			if(!($team_result > 0)) {

				$response['message'] = 'Team not deleted.';

			} else {

				$this->users_model->delete_team_users($team_id);
				$response['status'] = true;
				$response['message'] = 'Team deleted.';

			}

		}
		$this->build_response($response);
	}

	public function add_user() {
		$permissions = $this->input->post('permissions');
		$user_details = array(
			'fname' => $this->input->post('fname'),
			'lname' => $this->input->post('lname'),
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
			'user_role_id' => $this->input->post('role'),
			'team_id' => $this->input->post('team_id'),
			'image' => $this->input->post('user_image')
		);
		/*echo "<pre>";
   		print_r($user_details);
   		echo "</pre>";
   		exit;*/
		$this->form_validation->set_rules('fname', 'Firstname', 'trim|required|min_length[2]|max_length[50]');
		$this->form_validation->set_rules('lname', 'Lastname', 'trim|required|min_length[2]|max_length[50]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[2]|max_length[50]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('role', 'User Role', 'trim|required');

		


		if (!$this->form_validation->run()) {
			
			$response['message'] = validation_errors();

		} else {

			if($this->users_model->check_existing_user_pass($user_details['username'],$user_details['password'])) {
				
				$response['message'] = 'Username/Password already exists.';

			} else{

					if(count($permissions) < 1){

						$response['message'] = 'Please check atleast one System Permissions.';
					}
					else{

						$user_result = $this->users_model->add_user($user_details);
			   			if($user_result < 1) {

							$response['message'] = 'User not registered.';

						} else {

						foreach ($permissions as $key => $permission) {
							$this->add_user_permissions($user_result, $permission);
						}
						$response['status'] = true;
						$response['message'] = 'User registered.';

						}
					}		
			}
		}

		$this->build_response($response);
	}

	public function edit_user() {
		$deleted_permissions = $this->input->post('deleted_permissions');
		$new_permissions = $this->input->post('new_permissions');
		$user_id = $this->input->post('user_id');
		$update_details = array(
			'fname' => $this->input->post('fname'),
			'lname' => $this->input->post('lname'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'user_role_id' => $this->input->post('role'),
			'image' => $this->input->post('image')
		);
		
		if($this->input->post('name') != 'null') {
			$update_details['password'] = md5($this->input->post('password'));
		}

		$this->form_validation->set_rules('user_id', 'User Id', 'trim|required');
		$this->form_validation->set_rules('fname', 'Firstname', 'trim|required|min_length[2]|max_length[50]');
		$this->form_validation->set_rules('lname', 'Lastname', 'trim|required|min_length[2]|max_length[50]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[2]|max_length[50]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('role', 'User Role', 'trim|required');

		if (!$this->form_validation->run()) {

			$response['message'] = validation_errors();

		} else {
			
			$error_count = 0;
			$user_result = $this->users_model->update_user($user_id, $update_details);

			if($deleted_permissions != 'null') {

				foreach ($deleted_permissions as $key => $deleted) {
					$deleted_result = $this->users_model->delete_user_permission($user_id, $deleted);
				}

				if(!($deleted_result > 0)) {
					$error_count++;
				}

			} else {
				$error_count++;
			}

			if($new_permissions != 'null') {

				foreach ($new_permissions as $key => $new) {
					$new_result = $this->add_user_permissions($user_id, $new);
				}

				if(!($new_result > 0)) {
					$error_count++;
				}

			} else {
				$error_count++;
			}

			if(!$user_result) {

				$error_count++;

			}
			else {

				$error_count--;
			}


			if($error_count == 3) {

				$response['message'] = 'User not updated.'.'</br>'.validation_errors();

			} else {

				$response['status'] = true;
				$response['message'] = 'User information updated.';

			}
			
		}
		$this->build_response($response);
	}

	public function delete_user() {
		$user_id = $this->input->post('user_id');
		$this->form_validation->set_rules('user_id', 'User Id', 'trim|required');

		if (!$this->form_validation->run()) {
			
			$response['message'] = 'Please select a user.';

		} else {
			
			$user_result = $this->users_model->delete_user($user_id);
			if(!($user_result > 0)) {

				$response['message'] = 'User account not deleted.';

			} else {

				$response['status'] = true;
				$response['message'] = 'User account deleted';

			}

		}
		$this->build_response($response);
	}

	public function add_user_permissions($user_id, $permission_id) {
		$permission_details = array(
			'user_id' => $user_id,
			'user_permission_id' => $permission_id
		);

		$permission_result = $this->users_model->add_user_permissions($permission_details);
	}

	public function check_existing_team($team_name) {
		$existing_result = $this->users_model->check_existing_team($team_name);
		
		if(!$existing_result) {
			return false;
		} else {
			return true;
		}
	}

	public function build_response($response) {
		header("Content-Type: application/json");
		echo json_encode($response);
	}
	
}

/* End of file Users.php */
/* Location: ./application/modules/users/controllers/Users.php */