<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

	public function get_user($id)
	{	
		$response = array();
		return $response;
	}

	public function get_all_roles() {
		$sql = 'SELECT *
		FROM user_role
		WHERE is_deleted = 0';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_permissions() {
		$sql = 'SELECT *
		FROM user_permissions
		WHERE is_deleted = 0';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_team() {
		$sql = 'SELECT *
		FROM team
		WHERE is_deleted = 0';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_team_details($team_id) {
		$sql = 'SELECT *
		FROM team
		WHERE is_deleted = 0
		AND id = "' . $team_id . '"';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_all_users() {
		$sql = 'SELECT id, username, fname, lname,
		user_role_id, team_id
		FROM user
		WHERE is_deleted = 0';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_user_details($user_id) {
		$sql = 'SELECT u.id, u.username,u.password,u.image, u.fname, u.lname,
		u.password, ur.name as user_role
		FROM user u
		LEFT JOIN user_role ur ON ur.id = u.user_role_id
		WHERE u.is_deleted = 0
		AND u.id = "' . $user_id . '"';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_user_permission($user_id) {
		$sql = 'SELECT upm.user_permission_id as id, up.name as name
		FROM user_permission_map upm
		LEFT JOIN user_permissions up ON up.id = upm.user_permission_id
		WHERE upm.user_id = "' . $user_id . '"';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_team_users($team_id) {
		$sql = 'SELECT id, username, fname, lname,
		user_role_id, team_id, image
		FROM user
		WHERE is_deleted = 0
		AND team_id = "' . $team_id . '"';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function check_existing_team($team_name) {
		$sql = 'SELECT *
		FROM team
		WHERE name = "' . $team_name . '"';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function check_existing_user_pass($user_name,$password) {
		$sql = 'SELECT *
		FROM user
		WHERE username = "' . $user_name . '" OR password ="'. $password .'"';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function add_team($team_name) {
		$team_details = array( 'name' => $team_name );
		$this->db->insert('team', $team_details);
		return $this->db->insert_id();
	}

	public function edit_team($team_id, $team_name) {
		$update_details = array( 'name' => $team_name );
		$this->db->where('id', $team_id);
		$this->db->update('team', $update_details);
		return $this->db->affected_rows();
	}

	public function delete_team($team_id) {
		$update_details = array( 'is_deleted' => 1 );
		$this->db->where('id', $team_id);
		$this->db->update('team', $update_details);
		return $this->db->affected_rows();
	}

	public function delete_team_users($team_id) {
		$update_details = array( 'is_deleted' => 1 );
		$this->db->where('team_id', $team_id);
		$this->db->update('user', $update_details);
		return $this->db->affected_rows();
	}

	public function add_user($user_details) {
		$this->db->insert('user', $user_details);
		return $this->db->insert_id();
	}

	public function delete_user($user_id) {
		$update_details = array( 'is_deleted' => 1 );
		$this->db->where('id', $user_id);
		$this->db->update('user', $update_details);
		return $this->db->affected_rows();
	}

	public function add_user_permissions($permission_details) {
		$this->db->insert('user_permission_map', $permission_details);
		return $this->db->insert_id();
	}

	public function delete_user_permission($user_id, $permission_id) {
		$delete_details = array( 'user_id' => $user_id, 'user_permission_id' => $permission_id );
		$this->db->where($delete_details);
		$this->db->delete('user_permission_map');
		return $this->db->affected_rows();
	}

	public function get_member_count($team_id) {
		$sql = 'SELECT COUNT(*)
		FROM user
		WHERE team_id = "' . $team_id . '"
		AND is_deleted = 0';
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function update_user($user_id, $update_details) {
		$this->db->where('id', $user_id);
		$this->db->update('user', $update_details);
		return $this->db->affected_rows();
	}

}

/* End of file Users_model.php */
/* Location: ./application/modules/users/models/Users_model.php */