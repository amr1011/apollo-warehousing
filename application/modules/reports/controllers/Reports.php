<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MX_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->library('MY_Form_validation');
        $this->form_validation->CI =& $this;
		$this->load->model('receiving/receiving_model');
		$this->load->model('products/products_model');
		$this->load->model('vessel_list/vessel_model');
		$this->load->model('consignee/consignee_model');
		$this->load->model('reports/reports_model');
		$response = array(
			"status" => false,
			"message" => "message here",
			"data" => null
		);
	}


	public function index()
	{
		$this->display_reports();
	}

	public function display_reports()
	{
		$data['title'] = 'Reports';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("reports_view", $data);
		$this->load->view("includes/footer.php");
	}

	public function billing_attachment_report()
	{
		$data['title'] = 'Reports';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("billing_attachment_report", $data);
		$this->load->view("includes/footer.php");
	}

	public function cargo_withdrawal()
	{
		$data['title'] = 'Reports';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("cargo_withdrawal", $data);
		$this->load->view("includes/footer.php");
	}

	public function consignee_balance_summary()
	{
		$data['title'] = 'Reports';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_balance_summary", $data);
		$this->load->view("includes/footer.php");
	}

	public function consignee_summary_report()
	{
		$data['title'] = 'Reports';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("consignee_summary_report", $data);
		$this->load->view("includes/footer.php");
	}

	public function customize_reports()
	{
		$data['title'] = 'Reports';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("customize_reports", $data);
		$this->load->view("includes/footer.php");
	}

	public function inventory_report()
	{
		$data['title'] = 'Reports';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("inventory_report", $data);
		$this->load->view("includes/footer.php");
	}

	public function sack_withdrawal_summary_reports()
	{
		$data['title'] = 'Reports';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("sack_withdrawal_summary_reports", $data);
		$this->load->view("includes/footer.php");
	}

	public function warehouse_storage_report()
	{
		$data['title'] = 'Reports';
		$this->load->view("includes/header");
		$this->load->view("includes/side_nav", $data);
		$this->load->view("includes/top_nav");
		$this->load->view("warehouse_storage_report", $data);
		$this->load->view("includes/footer.php");
	}

	public function get_all_vessel()
	{
		$this->load->model("vessel_model");
		$vessels = array();

		$vessels = $this->vessel_model->gel_all();

		foreach ($vessels as $key => $value) {

			$vessels[$key]["date_formatted"] = date("M. d, Y", strtotime($value["date_created"]));
		}

		$response = array(
			"status" => true,
			"message" => "",
			"data" => $vessels
		);

		header("Content-Type:application/json;");
		echo json_encode($response);

	}

	public function get_consignee_names()
	{
		$result = $this->consignee_model->get_consignee_names();

		header("Content-Type: application/json");
		$response["status"] = true;
		$response["message"] = "Successful";
		$response["data"]= $result;

		echo json_encode($response);

	}

	public function get_consignee_vessels()
	{
		$id = $this->input->post('id');
		$result = $this->reports_model->get_consignee_vessels($id);

		header("Content-Type: application/json");
		$response["status"] = true;
		$response["message"] = "Successful";
		$response["data"]= $result;

		echo json_encode($response);

	}

	public function generateCargoWithdrawalSummary()
	{
		$datefrom = $this->input->post('datefrom');
		$dateto = $this->input->post('dateto');
		$getDateFrom = date("Y-m-d", strtotime($datefrom));
		$getDateTo = date("Y-m-d", strtotime($dateto));
		$credentials = array(
			'date_from' => $getDateFrom,
			'date_to' => $getDateTo,
			'consignee' => $this->input->post('consignee'),
			'vessel' => $this->input->post('vessel')
		);

		
		$cargodetails = $this->reports_model->generateCargoWithdrawalSummary($credentials['consignee'],$credentials['vessel'],$credentials['date_from'],$credentials['date_to']);

		// echo "<pre>";
		// print_r($cargodetails);
		// echo "</pre>";
		// exit;
		
		if(!$cargodetails) {
			
			$response['message'] = 'No records found.';

		} else {

			$response['status'] = true;
			$response['data'] = $cargodetails;

		}
		$this->build_response($response);
	}

	public function build_response($response) {
		header("Content-Type: application/json");
		echo json_encode($response);
	}

}

/* End of file Reports.php */
/* Location: ./application/modules/reports/controllers/Reports.php */