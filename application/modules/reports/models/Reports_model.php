<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model 
{	
	
	function generateCargoWithdrawalSummary($consignee,$vessel,$date_from,$date_to){

		$sql = "SELECT wcdrd.cdr_no as 'cdr_no',
					   DATE_FORMAT(wcdrd.date_completed,'%d-%b-%Y') as 'date_of_exit',
					   c.name as 'consignee',
					   vl.name as 'vessel',
					   p.name as 'commodity',
					   wp.loading_method as 'type',
					   wp.qty as 'no_of_bags_bulk',
						(CASE
						WHEN wcond.mode_of_delivery = 'truck' THEN wtd.trucking
						WHEN wcond.mode_of_delivery = 'vessel' THEN vl.name
						END) as 'trucking_small_vessel',
						wtd.plate_no as 'plate_no',
						wcdrd.gate_pass_ctrl_no as 'gatepas_no',
						c2.name as 'cargo_swapping',
						(wcdrd.weight_in - wcdrd.weight_out) as 'net_mt'
				FROM withdrawal_consignee_details wcond
				LEFT JOIN withdrawal_cdr_details wcdrd ON wcond.withdrawal_log_id = wcdrd.withdrawal_log_id
				LEFT JOIN consignee c ON wcond.consignee_id = c.id
				LEFT JOIN vessel_list vl ON wcond.origin_vessel_id = vl.id
				LEFT JOIN withdrawal_product wp ON wcond.withdrawal_log_id = wp.withdrawal_log_id
				LEFT JOIN product p ON wp.product_id = p.id
				LEFT JOIN withdrawal_truck_details wtd ON wcond.withdrawal_log_id = wtd.withdrawal_log_id
				LEFT JOIN withdrawal_log wl ON wcond.withdrawal_log_id = wl.id
				LEFT JOIN withdrawal_consignee_swap wcs ON wcond.withdrawal_log_id = wcs.withdrawal_log_id
				LEFT JOIN consignee c2  ON wcs.consignee_id_to = c2.id
				WHERE 1 AND wcdrd.date_completed BETWEEN DATE('{$date_from}') AND DATE('{$date_to}') AND c.name IN ($consignee) AND vl.name IN ($vessel)";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function get_consignee_vessels($cID)
	{
		$sql = "SELECT DISTINCT vl.name 
				FROM withdrawal_consignee_details wcd
				LEFT JOIN consignee c ON wcd.consignee_id = c.id
				LEFT JOIN vessel_list vl ON wcd.origin_vessel_id = vl.id
				WHERE 1 AND c.id IN ({$cID})";

		$query = $this->db->query($sql);
		return $query->result_array();
	}



}