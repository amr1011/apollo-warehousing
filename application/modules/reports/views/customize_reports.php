
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="reports.php">Reports</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color">Customize Report</li>
				</ul>
			</div>
			<div class="center-main">
				<p class="f-left font-20 font-400">Customize Report. 1 - Nov. 1, 2015</p>
				<button class="btn general-btn f-right">Download CSV</button>
				<div class="clear"></div>

				<div class="margin-top-30 cargo-summary padding-all-10 border-top border-blue box-shadow-dark">
					<table class="tbl-4c3h">
						<thead>
							<tr>
								<th class=" font-14 font-700 black-color no-padding-left width-100px">CDR No.</th>
								<th class=" font-14 font-700 black-color no-padding-left width-150px">Date of Exit</th>
								<th class=" font-14 font-700 black-color no-padding-left width-300px">Consignee</th>
								<th class=" font-14 font-700 black-color no-padding-left width-150px">Vessel</th>
								<th class=" font-14 font-700 black-color no-padding-left width-200px">Commodity</th>
								<th class=" font-14 font-700 black-color no-padding-left width-250px">Trucking / <br />Small Vessel Name</th>
								<th class=" font-14 font-700 black-color no-padding-left width-100px">Plate No.</th>
								<th class=" font-14 font-700 black-color no-padding-left width-100px">Type</th>
								<th class=" font-14 font-700 black-color no-padding-left width-150px">NET MT / <br />Less Sack Weight</th>							
							</tr>
						</thead>
						<tbody class="text-center">
							<tr>
								<td class=" font-14 font-400 black-color no-padding-left">615780</td>
								<td class=" font-14 font-400 black-color no-padding-left">22-Oct-2015</td>
								<td class=" font-14 font-400 black-color no-padding-left">Beta Feeds</td>
								<td class=" font-14 font-400 black-color no-padding-left">ASIA RUBY I</td>
								<td class=" font-14 font-400 black-color no-padding-left">Japanese Rice </td>
								<td class=" font-14 font-400 black-color no-padding-left">VIXXEN</td>
								<td class=" font-14 font-400 black-color no-padding-left">AGG 324</td>
								<td class=" font-14 font-400 black-color no-padding-left">Bags</td>
								<td class=" font-14 font-400 black-color no-padding-left">36.90</td>							
							</tr>
							<tr>
								<td class=" font-14 font-400 black-color no-padding-left">701937</td>
								<td class=" font-14 font-400 black-color no-padding-left">22-Oct-2016</td>
								<td class=" font-14 font-400 black-color no-padding-left">ALIBRA IMPEX / EVEREST</td>
								<td class=" font-14 font-400 black-color no-padding-left">ASIA RUBY I</td>
								<td class=" font-14 font-400 black-color no-padding-left">Baby Oil</td>
								<td class=" font-14 font-400 black-color no-padding-left">EY KING</td>
								<td class=" font-14 font-400 black-color no-padding-left">AGG 324</td>
								<td class=" font-14 font-400 black-color no-padding-left">Bulks</td>
								<td class=" font-14 font-400 black-color no-padding-left">37.84</td>	
								
							</tr>
							<tr>
								<td class=" font-14 font-400 black-color no-padding-left">701937</td>
								<td class=" font-14 font-400 black-color no-padding-left">22-Oct-2016</td>
								<td class=" font-14 font-400 black-color no-padding-left">ALIBRA IMPEX / EVEREST</td>
								<td class=" font-14 font-400 black-color no-padding-left">ASIA RUBY I</td>
								<td class=" font-14 font-400 black-color no-padding-left">Australian Wheat</td>
								<td class=" font-14 font-400 black-color no-padding-left">VIXXEN</td>
								<td class=" font-14 font-400 black-color no-padding-left">AGG 324</td>
								<td class=" font-14 font-400 black-color no-padding-left">Bulks</td>
								<td class=" font-14 font-400 black-color no-padding-left">37.84</td>									
							</tr>
							<tr>
								<td class=" font-14 font-400 black-color no-padding-left">701937</td>
								<td class=" font-14 font-400 black-color no-padding-left">22-Oct-2016</td>
								<td class=" font-14 font-400 black-color no-padding-left">Beta Feeds</td>
								<td class=" font-14 font-400 black-color no-padding-left">ASIA RUBY I</td>
								<td class=" font-14 font-400 black-color no-padding-left">Japanese RIce</td>
								<td class=" font-14 font-400 black-color no-padding-left">EY KING</td>
								<td class=" font-14 font-400 black-color no-padding-left">AGG 324</td>
								<td class=" font-14 font-400 black-color no-padding-left">Bulks</td>
								<td class=" font-14 font-400 black-color no-padding-left">37.84</td>									
							</tr>
							<tr class="last-panel">
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td class=" font-14 font-400 black-color no-padding-left">Total</td>
								<td class=" font-14 font-400 black-color no-padding-left">140.40</td>									
							</tr>
						</tbody>
					</table>
					
				</div>

			
			</div>
		</div>
