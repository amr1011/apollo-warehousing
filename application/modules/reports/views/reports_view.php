<div class="main">
			<div class="semi-main">
				<div class="mod-select padding-left-3 padding-top-60">			
					<div class="select f-left width-350px ">
						<select class="transform-dd">
							<option value="Displaying All Reports"  html=""></option>
							<option value="Product1" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product2" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product3" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product4" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
							<option value="Product5" html="<div class='box-shadow-dark padding-all-10'><table><tbody><tr><td>Search Type:</td><td>Product</td></tr><tr><td>Keywords:</td><td>UNO Feed - Premium Hog Diets</td></tr><tr><td>View By:</td><td>Product</td></tr><tr><td>Filter By:</td><td>All Location / All Categories</td></tr><tr><td>Sort By:</td><td>SKU No.</td></tr></tbody></table></div>"></option>
						</select>
					</div>						
				</div>
				<div class="f-right margin-top-10">	
					
					<button class="margin-left-5 margin-right-5 display-inline-mid btn general-btn dropdown-btn">View Filter / Search</button>					
				</div>
				<div class="clear"></div>

				<div class="dropdown-search" >
					<div class="triangle">
						<i class="fa fa-caret-up"></i>
					</div>
					<div class="padding-all-10">
						<p class="f-left margin-top-5 margin-right-10 font-400">Search:</p>
						<div class="f-left width-65per">
							<input type="text" class="t-small super-width-100per" >
						</div>
						<p class="f-left font-400 margin-left-20 margin-top-5 margin-right-10">Sort By:</p>
						<div class="select f-left medium">
							<select class="transform-dd ">
								<option value="stats1">Ascending</option>
								<option value="stats2">Descending</option>
							</select>
						</div>
						<div class="clear"></div>
					</div>
					
					<div class="f-left margin-top-10 margin-left-10 display-search-result">
						<i class="fa fa-caret-right fa-2x display-inline-mid"></i>
						<p class="display-inline-mid font-15 font-bold margin-left-10">Save Search Result</p>
					</div>
					<button type="button" class="btn general-btn font-bold f-right margin-top-10 margin-right-30">Search</button>
					<div class="clear"></div>
					<div class="search-result padding-all-10">
						<div class="margin-left-50">
							<p class="font-400 font-15 black-color f-left">Who can also view this Search Result? </p>
							<div class="clear"></div>
							
							<div class="f-left">
								<div class="display-inline-mid margin-right-10">
									<input type="radio" id="me" name="search-result" class="width-20px transform-scale">
									<label for="me" class="margin-bottom-5 default-cursor font-15 black-color font-400">Only Me</label>
								</div>
								<div class="display-inline-mid margin-right-50">
									<input type="radio" id="selected" name="search-result" class="width-20px transform-scale">
									<label for="selected" class="margin-bottom-5 default-cursor font-15 black-color font-400">With Selected Members</label>
								</div>
								<div class="display-inline-mid member-selection">
									<div class="member-container">
										<p>1 Member Selected</p>
										<i class=" fa fa-caret-down fa-2x"></i>
									</div>
									<div class="popup_person_list">
										<div class="popup_person_list_div">
											
											<div class="thumb_list_view small marked">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													<div class="img-like display-inline-mid">
														<img class="user-image" src="../assets/images/profile/profile_d.png">
													</div>
													<div class="profile display-inline-mid">
														<p class="profile_name font-15">Dwayne Garcia Ricardo Gomez Kuala Lumpur Eklabu</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

										</div>										
									</div>
								</div>
								<div class="display-inline-mid margin-left-50">								
									<!-- dropdown here with effects  -->
									<button type="button" class="general-btn">Save Search Results</button>
								</div>
							</div>
							<div class="clear"></div>
						</div>

					</div>
				</div>

				

				<div class="first-container width-100per margin-top-20 margin-bottom-20">
					<div class="width-100percent text-left font-0">
	
						<p class="font-500 font-20 black-color margin-bottom-10">Standard Reports</p>

						<div class="display-inline-mid width-33per bggray-white box-shadow-light margin-all-5 text-left default-cursor border-top border-blue">
							<div class="padding-all-10">
								<p class="font-bold font-18">Consignee Summary Report</p>
								<p class="margin-top-5 font-400">Description here</p>
								<div class="text-center margin-top-10 margin-bottom-10">
									<button class=" btn general-btn modal-trigger" modal-target="consignee-summary-report">Generate</button>
								</div>
							</div>							
						</div>

						<div class="display-inline-mid width-33per bggray-white box-shadow-light margin-all-5 text-left default-cursor border-top border-blue">
							<div class="padding-all-10">
								<p class="font-bold font-18">Sack Withdrawal Summary</p>
								<p class="margin-top-5 font-400">Description here</p>
								<div class="text-center margin-top-10 margin-bottom-10">
									<button class=" btn general-btn modal-trigger" modal-target="sack-withdrawal-summary-reports">Generate</button>
								</div>
							</div>							
						</div>

						

						<div class="display-inline-mid width-33per bggray-white box-shadow-light margin-all-5 text-left default-cursor border-top border-blue">
							<div class="padding-all-10">
								<p class="font-bold font-18">Cargo Withdrawal Summary</p>
								<p class="margin-top-5 font-400">Description here</p>
								<div class="text-center margin-top-10 margin-bottom-10">
									<button class=" btn general-btn modal-trigger" modal-target="cargo-withdrawal-summary">Generate</button>
								</div>
							</div>							
						</div>

						

						<div class="display-inline-mid width-33per bggray-white box-shadow-light margin-all-5 text-left default-cursor border-top border-blue">
							<div class="padding-all-10">
								<p class="font-bold font-18">Consignee Balance Summary</p>
								<p class="margin-top-5 font-400">Description here</p>
								<div class="text-center margin-top-10 margin-bottom-10">
									<button class=" btn general-btn modal-trigger" modal-target="consignee-balance-summary">Generate</button>
								</div>
							</div>							
						</div>

						

						<div class="display-inline-mid width-33per bggray-white box-shadow-light margin-all-5 text-left default-cursor border-top border-blue">
							<div class="padding-all-10">
								<p class="font-bold font-18">Warehouse Storage Report</p>
								<p class="margin-top-5 font-400">Description here</p>
								<div class="text-center margin-top-10 margin-bottom-10">
									<button class=" btn general-btn modal-trigger" modal-target="warehouse-storage-report">Generate</button>
								</div>
							</div>							
						</div>

						
						<div class="display-inline-mid width-33per bggray-white box-shadow-light margin-all-5 text-left default-cursor border-top border-blue">
							<div class="padding-all-10">
								<p class="font-bold font-18">Inventory Report</p>
								<p class="margin-top-5 font-400">Description here</p>
								<div class="text-center margin-top-10 margin-bottom-10">
									<button class=" btn general-btn modal-trigger" modal-target="inventory-report">Generate</button>
								</div>
							</div>							
						</div>

						<div class="display-inline-mid width-33per bggray-white box-shadow-light margin-all-5 text-left default-cursor border-top border-blue">
							<div class="padding-all-10">
								<p class="font-bold font-18">Billing Attachment</p>
								<p class="margin-top-5 font-400">Description here</p>
								<div class="text-center margin-top-10 margin-bottom-10">
									<button class=" btn general-btn modal-trigger" modal-target="billing-attachment">Generate</button>
								</div>
							</div>							
						</div>
						
						<div class="margin-top-30 margin-bottom-10">
							<p class="font-500 font-20 black-color  f-left margin-left-5">Custom Reports</p>
							<button class=" btn general-btn f-right margin-right-20 modal-trigger" modal-target="customized-report">Create Custom Report</button>
							<div class="clear"></div>
						</div>

						<div class="display-inline-mid width-33per bggray-white box-shadow-light margin-all-5 text-left default-cursor border-top border-blue">
							<div class="padding-all-10">
								<p class="font-bold font-18">Consignee Balance for 2</p>
								<p class="margin-top-5 font-400">Derived From: Consignee Balance Summary </p>
								<div class="text-center margin-top-10 margin-bottom-10">
									<button class=" btn general-btn modal-trigger" modal-target="custom-report">Generate</button>
								</div>
							</div>							
						</div>
						<div class="display-inline-mid width-33per bggray-white box-shadow-light margin-all-5 text-left default-cursor border-top border-blue">
							<div class="padding-all-10">
								<p class="font-bold font-18">3 Warehouses</p>
								<p class="margin-top-5 font-400">Derived From: Warehouse Storage Report</p>
								<div class="text-center margin-top-10 margin-bottom-10">
									<button class=" btn general-btn modal-trigger" modal-target="custom-report">Generate</button>
								</div>
							</div>							
						</div>
						<div class="display-inline-mid width-33per bggray-white box-shadow-light margin-all-5 text-left default-cursor border-top border-blue">
							<div class="padding-all-10">
								<p class="font-bold font-18">Cargo Haul 21</p>
								<p class="margin-top-5 font-400">Derived From: Cargo Withdrawal Summary</p>
								<div class="text-center margin-top-10 margin-bottom-10">
									<button class=" btn general-btn modal-trigger" modal-target="custom-report">Generate</button>
								</div>
							</div>							
						</div>
														
					</div>
				</div>

			</div>
		</div>



		<div class="modal-container" modal-id="billing-attachment">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Billing Attachment</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left add-vessel">	
					
					<div class="width-100per margin-top-20">
						<p class="font-14 font-400 margin-top-10 f-left width-100px">Consignee:</p>
						<div class="select f-left width-300px  ">
							<select class="transform-dd">
								<option value="option">All Consignees</option>
							</select>
						</div>
						<div class="clear"></div>

						<p class="font-14 font-400 margin-top-10 f-left width-100px">Vessels:</p>
						<div class="select f-left width-300px  ">
							<select class="transform-dd">
								<option value="option">All Vessels</option>
							</select>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<a href="consignee_summary_report">
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Generate Report</button>
					</a>
				</div>
			</div>	
		</div>
		
		<div class="modal-container" modal-id="consignee-summary-report">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Consignee Summary Report</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left add-vessel">	
					<p class="font-15 font-400">Please give the date range of the report</p>
					
					<div class="width-100per margin-top-20">
						<p class="font-14 font-400 f-left margin-top-10 width-100px">Start Date:</p>
						<div class="input-group width-150px f-left">
							<input class="form-control dp border-big-bl border-big-tl default-cursor width-260px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>
						<p class="font-14 font-400 margin-top-10 f-left width-100px">End Date:</p>
						<div class="input-group width-150px f-left">
							<input class="form-control dp border-big-bl border-big-tl default-cursor width-260px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>

						<p class="font-14 font-400 margin-top-10 f-left width-100px">Consignee:</p>
						<div class="select f-left width-300px  ">
							<select class="transform-dd">
								<option value="option">All Consignees</option>
							</select>
						</div>
						<div class="clear"></div>

						<p class="font-14 font-400 margin-top-10 f-left width-100px">Vessels:</p>
						<div class="display-inline-mid member-selection">
									<div class="member-container">
										<p>1 Member Selected</p>
										<i class=" fa fa-caret-down fa-2x"></i>
									</div>
									<div class="popup_person_list">
										<div class="popup_person_list_div">

											<div class="height-30px margin-all-10 border-full">
												
												
											</div>
											
											<div class="thumb_list_view small marked">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													
													<div class="display-inline-mid">
														<p class="profile_name font-15 ">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>
											

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													
													<div class="display-inline-mid">
														<p class="profile_name font-15 ">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													
													<div class="display-inline-mid">
														<p class="profile_name font-15 ">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													
													<div class="display-inline-mid">
														<p class="profile_name font-15 ">Dwayne Garcia Ricardo Gomez Kuala Lumpur Eklabu</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

										</div>										
									</div>
								</div>
						<div class="clear"></div>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<a href="consignee_summary_report">
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Generate Report</button>
					</a>
				</div>
			</div>	
		</div>
		
		<div class="modal-container" modal-id="sack-withdrawal-summary-reports">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Sack Withdrawal Summary</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left add-vessel">	
					<p class="font-15 font-400">Please give the date range of the report</p>
					
					<div class="width-100per margin-top-30">
						<p class="font-14 font-400 f-left margin-top-10 width-100px">Search:</p>
						<div class="select large">
							<select class="transform-dd">
								<option value="cont1">Alpha Foods</option>
								<option value="cont2">Alpha Foods 2</option>
								<option value="cont3">Alpha Foods 3</option>
								<option value="cont4">Alpha Foods 4</option>
							</select>
						</div>
						<div class="clear"></div>
						<p class="font-14 font-400 f-left margin-top-10 width-100px">Start Date:</p>
						<div class="input-group width-150px f-left">
							<input class="form-control dp border-big-bl border-big-tl default-cursor width-191px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>
						<p class="font-14 font-400 margin-top-10 f-left width-100px">End Date:</p>
						<div class="input-group width-150px f-left">
							<input class="form-control dp border-big-bl border-big-tl default-cursor width-191px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<a href="sack_withdrawal_summary_reports">
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Generate Report</button>
					</a>
				</div>
			</div>	
		</div>
		
		<div class="modal-container" modal-id="cargo-withdrawal-summary">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Cargo Withdrawal Summary</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left add-vessel">	
					<p class="font-15 font-400">Please give the date range of the report</p>
					
					<div class="width-100per margin-top-30">
						<p class="font-14 font-400 f-left margin-top-10 width-100px">Start Date:</p>
						<div class="input-group width-150px f-left" id="cargo-from">
							<input class="cargo-with-date-from form-control dp border-big-bl border-big-tl default-cursor width-200px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>
						<p class="font-14 font-400 margin-top-10 f-left width-100px">End Date:</p>
						<div class="input-group width-150px f-left" id="cargo-to">
							<input class="cargo-with-date-to form-control dp border-big-bl border-big-tl default-cursor width-200px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>
						<p class="font-14 font-400 margin-top-10 f-left width-100px">Consignee:</p>
						<div class="display-inline-mid member-selection " id="consignee-records">
							
									
						</div>
						<div class="clear"></div>
						<p class="font-14 font-400 margin-top-10 f-left width-100px">Vessels:</p>
						<div class="display-inline-mid member-selection" id="vessel-records">
									
						</div>
						<div class="clear"></div>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<!-- <a href="cargo_withdrawal"> -->

						<button type="button" class="generate-cargo font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Generate Report</button>
				</div>
			</div>	
		</div>

		<div class="modal-container" modal-id="customized-report">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Customize Report</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left add-vessel">	
										
					<div class="width-100per margin-top-20">
						<div>
							<p class="f-left font-14 font-400 width-150px margin-top-10">Report Template:</p>
							<div class="select f-left width-250px">
								<select class="transform-dd">
									<option value="cargo-withdrawal">Cargo Withdrawal Summary</option>
								</select>
							</div>
							<div class="clear"></div>
						</div>
						<div>
							<p class="f-left font-14 font-400 width-150px">Save As:</p>
							<input type="text" class="t-medium f-left" value="CWS the 3rd">
							<div class="clear"></div>
						</div>
						<p class="font-14 font-400 ">Data:</p>

						<div class="container-report">
							<div>
								<div class="f-left">
									<p class="font-400 font-14">CDR No.</p>
								</div>
								<div class="f-right">
									<div class="toggleSwitch yes-no customize-report-button">
		                                <input type="checkbox" id="switch1" name="switch1" class="switch" checked>
		                                <label for="switch1">f</label>
		                            </div>
								</div>
								<div class="clear"></div>
							</div>
							<div>
								<div class="f-left">
									<p class="font-400 font-14">Date of Exit</p>
								</div>
								<div class="f-right">
									<div class="toggleSwitch yes-no customize-report-button">
		                                <input type="checkbox" id="switch2" name="switch2" class="switch" checked>
		                                <label for="switch2">f</label>
		                            </div>
								</div>
								<div class="clear"></div>
							</div>
							<div>
								<div class="f-left">
									<p class="font-400 font-14">Consignee</p>
								</div>
								<div class="f-right">
									<div class="toggleSwitch yes-no customize-report-button">
		                                <input type="checkbox" id="switch3" name="switch3" class="switch" >
		                                <label for="switch3">f</label>
		                            </div>
								</div>
								<div class="clear"></div>
							</div>
							<div>
								<div class="f-left">
									<p class="font-400 font-14">Vessel</p>
								</div>
								<div class="f-right">
									<div class="toggleSwitch yes-no customize-report-button">
		                                <input type="checkbox" id="switch4" name="switch4" class="switch">
		                                <label for="switch4">f</label>
		                            </div>
								</div>
								<div class="clear"></div>
							</div>
							<div>
								<div class="f-left">
									<p class="font-400 font-14">Commodity</p>
								</div>
								<div class="f-right">
									<div class="toggleSwitch yes-no customize-report-button">
		                                <input type="checkbox" id="switch5" name="switch5" class="switch" checked>
		                                <label for="switch5">f</label>
		                            </div>
								</div>
								<div class="clear"></div>
							</div>
							<div>
								<div class="f-left">
									<p class="font-400 font-14">Trucking / Small Vessel Name</p>
								</div>
								<div class="f-right">
									<div class="toggleSwitch yes-no customize-report-button">
		                                <input type="checkbox" id="switch6" name="switch6" class="switch" checked>
		                                <label for="switch6">f</label>
		                            </div>
								</div>
								<div class="clear"></div>
							</div>
							<div>
								<div class="f-left">
									<p class="font-400 font-14">Plate No.</p>
								</div>
								<div class="f-right">
									<div class="toggleSwitch yes-no customize-report-button">
		                                <input type="checkbox" id="switch7" name="switch7" class="switch" >
		                                <label for="switch7">f</label>
		                            </div>
								</div>
								<div class="clear"></div>
							</div>
							<div>
								<div class="f-left">
									<p class="font-400 font-14">Type</p>
								</div>
								<div class="f-right">
									<div class="toggleSwitch yes-no customize-report-button">
		                                <input type="checkbox" id="switch8" name="switch8" class="switch">
		                                <label for="switch8">f</label>
		                            </div>
								</div>
								<div class="clear"></div>
							</div>

						</div>

					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<a href="customize_reports">
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Create Custom Report</button>
					</a>
				</div>
			</div>	
		</div>

		<div class="modal-container" modal-id="consignee-balance-summary">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Consignee Balance Summary</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left add-vessel">	
					<p class="font-15 font-400">Please give the date range of the report</p>
					
					<div class="width-100per margin-top-30">
						<p class="font-14 font-400 f-left margin-top-10 width-100px">Consignee:</p>
						<div class="select large">
							<select class="transform-dd">
								<option value="cont1">Alpha Foods</option>
								<option value="cont2">Alpha Foods 2</option>
								<option value="cont3">Alpha Foods 3</option>
								<option value="cont4">Alpha Foods 4</option>
							</select>
						</div>
						<div class="clear"></div>
						<p class="font-14 font-400 margin-top-10 f-left width-100px">Vessels:</p>
						<div class="display-inline-mid member-selection">
									<div class="member-container">
										<p>1 Member Selected</p>
										<i class=" fa fa-caret-down fa-2x"></i>
									</div>
									<div class="popup_person_list">
										<div class="popup_person_list_div">

											<div class="height-30px margin-all-10 border-full">
												
												
											</div>
											
											<div class="thumb_list_view small marked">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													
													<div class="display-inline-mid">
														<p class="profile_name font-15 ">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>
											

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													
													<div class="display-inline-mid">
														<p class="profile_name font-15 ">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													
													<div class="display-inline-mid">
														<p class="profile_name font-15 ">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													
													<div class="display-inline-mid">
														<p class="profile_name font-15 ">Dwayne Garcia Ricardo Gomez Kuala Lumpur Eklabu</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

										</div>										
									</div>
								</div>
						<div class="clear"></div>
						<p class="font-14 font-400 f-left margin-top-10 width-100px">Start Date:</p>
						<div class="input-group width-150px f-left">
							<input class="form-control dp border-big-bl border-big-tl default-cursor width-191px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>
						<p class="font-14 font-400 margin-top-10 f-left width-100px">End Date:</p>
						<div class="input-group width-150px f-left">
							<input class="form-control dp border-big-bl border-big-tl default-cursor width-191px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<a href="consignee_balance_summary">
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Generate Report</button>
					</a>
				</div>
			</div>	
		</div>

		<div class="modal-container" modal-id="warehouse-storage-report">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Warehouse Storage Report</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left add-vessel">	
					<p class="font-15 font-400">Please give the date range of the report</p>
					
					<div class="width-100per margin-top-20">
						<p class="font-14 font-400 margin-top-10 f-left width-100px">Products:</p>
						<div class="display-inline-mid member-selection">
									<div class="member-container">
										<p>Select Products </p>
										<i class=" fa fa-caret-down fa-2x"></i>
									</div>
									<div class="popup_person_list">
										<div class="popup_person_list_div">

											<div class="height-30px margin-all-10 border-full">
												
												
											</div>
											
											<div class="thumb_list_view small marked">
												<div class="thumb_list_inner">
													<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
													
													<div class="display-inline-mid">
														<p class="profile_name font-15 ">1234567890 - Australlian Wheat</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>
											

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													
													<div class="display-inline-mid">
														<p class="profile_name font-15 ">1234567891 - Japanese Wheat</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>	

											<div class="thumb_list_view small">
												<div class="thumb_list_inner">
													
													<div class="display-inline-mid">
														<p class="profile_name font-15 ">1234567892 - Plain Corn</p>														
													</div>
												</div>
												<div class="check">
													<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
												</div>
											</div>		

										</div>										
									</div>
						</div>
						<div class="clear"></div>
						<p class="font-14 font-400 f-left margin-top-10 width-100px">End Date:</p>
						<div class="input-group width-150px f-left">
							<input class="form-control dp border-big-bl border-big-tl default-cursor width-191px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<a href="warehouse_storage_report">
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Generate Report</button>
					</a>
				</div>
			</div>	
		</div>

		<div class="modal-container" modal-id="inventory-report">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Inventory Report</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left add-vessel">	
					<p class="font-15 font-400">Please give the date range of the report</p>
					
					<div class="width-100per margin-top-30">
						<p class="font-14 font-400 f-left margin-top-10 width-100px">Start Date:</p>
						<div class="input-group width-150px f-left">
							<input class="form-control dp border-big-bl border-big-tl default-cursor width-200px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>
						<p class="font-14 font-400 margin-top-10 f-left width-100px">End Date:</p>
						<div class="input-group width-150px f-left">
							<input class="form-control dp border-big-bl border-big-tl default-cursor width-200px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<a href="inventory_report">
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Generate Report</button>
					</a>
				</div>
			</div>	
		</div>


		<div class="modal-container" modal-id="custom-report">
			<div class="modal-body small">				

				<div class="modal-head">
					<h4 class="text-left">Inventory Report</h4>				
					<div class="modal-close close-me"></div>
				</div>

				<!-- content -->
				<div class="modal-content text-left add-vessel">	
					<p class="font-15 font-400">Please give the date range of the report</p>
					
					<div class="width-100per margin-top-30">
						<p class="font-14 font-400 f-left margin-top-10 width-100px">Start Date:</p>
						<div class="input-group width-150px f-left">
							<input class="form-control dp border-big-bl border-big-tl default-cursor width-200px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>
						<p class="font-14 font-400 margin-top-10 f-left width-100px">End Date:</p>
						<div class="input-group width-150px f-left">
							<input class="form-control dp border-big-bl border-big-tl default-cursor width-200px" data-date-format="MM/DD/YYYY" type="text">
							<span class="input-group-addon border-big-tr border-big-br"><span class="fa fa-calendar"></span></span>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			
				<div class="modal-footer text-right">
					<button type="button" class="font-12 btn btn-default font-12 display-inline-mid close-me red-color">Cancel</button>
					<a href="customize_reports">
						<button type="button" class="font-12 btn btn-primary font-12 display-inline-mid padding-left-30 padding-right-30">Generate Report</button>
					</a>
				</div>
			</div>	
		</div>






		<!-- For Adding All Consignee Dropdown  -->
		<span class="add-consignee-contain consignee-contain" style="display: none;">
			<div class="member-container">
					<p class="collected-textbox" style="padding: 2px; scroll: auto;"></p>
					<i class=" fa fa-caret-down fa-2x"></i>
			</div>
			<div class="popup_person_list">
				<div class="popup_person_list_div">

					<div class="height-30px margin-all-10 border-full collected-textbox" style="padding: 10px; scroll: auto;">
						
						
					</div>
					<span class="per-consignee-record">
						<div class="thumb_list_view small consignee-per-item consignee-item">
							<div class="thumb_list_inner">
								
								<div class="display-inline-mid">
									<p class="profile_name font-15 ">Dwayne Garcia</p>														
								</div>
							</div>
							<div class="check">
								<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
							</div>
						</div>
						

					</span>

						

					

				</div>										
			</div>
		</span>
		<!-- End For Adding All Consignee Dropdown  -->

		<!-- For Adding All Consignee Dropdown  -->
		<span class="add-vessel-contain vessel-contain" style="display: none;">
			<div class="member-container">
					<p class="collected-textbox" style="padding: 2px; scroll: auto;"></p>
					<i class=" fa fa-caret-down fa-2x"></i>
			</div>
			<div class="popup_person_list">
				<div class="popup_person_list_div">

					<div class="height-30px margin-all-10 border-full collected-textbox" style="padding: 10px; scroll: auto;">
						
						
					</div>
						<span class="per-vessel-record">
					<div class="thumb_list_view small vessel-per-item vessel-item">
						<div class="thumb_list_inner">
							<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
							
							<div class="display-inline-mid">
								<p class="profile_name font-15 " >Dwayne Garcia</p>														
							</div>
						</div>
						<div class="check">
							<img class="img-responsive" src="../assets/images/ui/check_icon.svg">
						</div>
					</div>
					</span>
					</div>
				</div>
					
		</span>
		<!-- End For Adding All Consignee Dropdown  -->