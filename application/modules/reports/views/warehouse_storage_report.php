
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="reports.php">Reports</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color">Warehouse Storage Report</li>
				</ul>
			</div>
			<div class="center-main">
				<p class="f-left font-20 font-400">Warehouse Storage Report for Nov. 1, 2015</p>
				<button class="btn general-btn f-right">Download CSV</button>
				<div class="clear"></div>

				<div class="margin-top-30 consignee-summary padding-all-10 border-top border-blue box-shadow-dark">
					<table class="tbl-4c3h">
						<thead>
							<tr>
								<th class=" font-14 font-700 black-color ">Storage</th>
								<th class=" font-14 font-700 black-color  ">AUS Wheat</th>
								<th class=" font-14 font-700 black-color  ">Corn</th>
								<th class=" font-14 font-700 black-color  ">Holcim Cement</th>
								<th class=" font-14 font-700 black-color  ">Total</th>
								<th class=" font-14 font-700 black-color  ">Capacity</th>
								<th class=" font-14 font-700 black-color  ">Capacity Percentage</th>
							</tr>
						</thead>
						<tbody class="text-center">
							<tr>
								<td class=" font-14 font-700 black-color  ">A1</td>
								<td class=" font-14 font-700 black-color  ">615780</td>
								<td class=" font-14 font-700 black-color  ">887643</td>
								<td class=" font-14 font-700 black-color  ">887643</td>
								<td class=" font-14 font-700 black-color  ">2,391,066</td>
								<td class=" font-14 font-700 black-color  ">3,000,000</td>
								<td class=" font-14 font-700 black-color  ">50.22%</td>
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">A2</td>
								<td class=" font-14 font-700 black-color  ">701937</td>
								<td class=" font-14 font-700 black-color  ">435382</td>
								<td class=" font-14 font-700 black-color  ">435382</td>
								<td class=" font-14 font-700 black-color  ">1,572,701</td>
								<td class=" font-14 font-700 black-color  ">2,000,000</td>
								<td class=" font-14 font-700 black-color  ">40.26%</td>
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">A3</td>
								<td class=" font-14 font-700 black-color  ">615780</td>
								<td class=" font-14 font-700 black-color  ">887643</td>
								<td class=" font-14 font-700 black-color  ">887643</td>
								<td class=" font-14 font-700 black-color  ">2,391,066</td>
								<td class=" font-14 font-700 black-color  ">3,000,000</td>
								<td class=" font-14 font-700 black-color  ">50.22%</td>
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">A4</td>
								<td class=" font-14 font-700 black-color  ">701937</td>
								<td class=" font-14 font-700 black-color  ">435382</td>
								<td class=" font-14 font-700 black-color  ">435382</td>
								<td class=" font-14 font-700 black-color  ">1,572,701</td>
								<td class=" font-14 font-700 black-color  ">2,000,000</td>
								<td class=" font-14 font-700 black-color  ">40.26%</td>	
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
