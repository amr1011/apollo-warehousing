
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="<?php echo BASEURL . '/reports/display_reports';  ?>">Reports</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color">Cargo Withdrawal Summary</li>
				</ul>
			</div>
			<div class="center-main">
				<p class="f-left font-20 font-400 date-range">Cargo Withdrawal Summary for Oct. 1 - Nov. 1, 2015</p>
				<button class="btn general-btn f-right">Download CSV</button>
				<div class="clear"></div>

				<div class="margin-top-30 cargo-summary padding-all-10 border-top border-blue box-shadow-dark">
					<table class="tbl-4c3h cargo-summary-table">
						<thead>
							<tr >
								<th class=" font-14 font-700 black-color no-padding-left width-100px">CDR No.</th>
								<th class=" font-14 font-700 black-color no-padding-left width-150px">Date of Exit</th>
								<th class=" font-14 font-700 black-color no-padding-left width-300px">Consignee</th>
								<th class=" font-14 font-700 black-color no-padding-left width-150px">Vessel</th>
								<th class=" font-14 font-700 black-color no-padding-left width-200px">Commodity</th>
								<th class=" font-14 font-700 black-color no-padding-left width-100px">Type</th>
								<th class=" font-14 font-700 black-color no-padding-left width-200px">No. of Bags /Bulk</th>
								<th class=" font-14 font-700 black-color no-padding-left width-250px">Trucking / <br />Small Vessel Name</th>
								<th class=" font-14 font-700 black-color no-padding-left width-100px">Plate No.</th>
								<th class=" font-14 font-700 black-color no-padding-left width-100px">Gatepass No.</th>
								<th class=" font-14 font-700 black-color no-padding-left width-100px">Cargo Swapping</th>
								<th class=" font-14 font-700 black-color no-padding-left width-150px">NET MT / <br />Less Sack Weight</th>							
							</tr>
						</thead>
						<tbody class="text-center" id="display-selected-cargo">
							<tr class="set-withdrawal-item withdrawal-item" style="display: none">
								<td class="cdr-no font-14 font-400 black-color no-padding-left">615780</td>
								<td class="date-of-exit font-14 font-400 black-color no-padding-left">22-Oct-2015</td>
								<td class="consignee font-14 font-400 black-color no-padding-left">Beta Feeds</td>
								<td class="vessel font-14 font-400 black-color no-padding-left">ASIA RUBY I</td>
								<td class="commodity font-14 font-400 black-color no-padding-left">Japanese Rice </td>
								<td class="type font-14 font-400 black-color no-padding-left">Bags</td>
								<td class="no-of-bags-bulk font-14 font-400 black-color no-padding-left">24</td>
								<td class="trucking-small-vessel-name font-14 font-400 black-color no-padding-left">VIXXEN</td>
								<td class="plate-no font-14 font-400 black-color no-padding-left">AGG 324</td>
								<td class="gatepass-no font-14 font-400 black-color no-padding-left">GPN16000181560001</td>
								<td class="cargo-swapping font-14 font-400 black-color no-padding-left">NUTRITECH VENTURES, INC.</td>
								<td class="net-mt font-14 font-400 black-color no-padding-left">36.90</td>							
							</tr>
							<tr class="last-panel">
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td ></td>
								<td class=" font-14 font-400 black-color no-padding-left">Total</td>
								<td class=" font-14 font-400 black-color no-padding-left total-net"></td>									
							</tr>
						</tbody>
					</table>
					
				</div>

			
			</div>
		</div>