
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="reports.php">Reports</a></li>
					<li><span>&gt;</span></li>
					<li class="font-bold black-color">Sack Withdrawal Summary</li>
				</ul>
			</div>
			<div class="center-main">
				<p class="f-left font-20 font-400">Sack Withdrawal Summary for Jan. 1 - Feb. 1, 2015</p>
				<button class="btn general-btn f-right">Download CSV</button>
				<div class="clear"></div>

				<div class="margin-top-30 consignee-summary border-top border-blue padding-all-10 box-shadow-dark">
					<table class="tbl-4c3h">
						<thead>
							<tr>
								<th class=" font-14 font-700 black-color ">Date</th>
								<th class=" font-14 font-700 black-color  ">MIS No.</th>
								<th class=" font-14 font-700 black-color  ">CDR No.</th>
								<th class=" font-14 font-700 black-color  ">Vessel</th>
								<th class=" font-14 font-700 black-color  ">Bags</th>
								<th class=" font-14 font-700 black-color  ">Trucking</th>
								<th class=" font-14 font-700 black-color  ">Plate No.</th>
								<th class=" font-14 font-700 black-color  ">Remarks</th>
							</tr>
						</thead>
						<tbody class="text-center">
							<tr>
								<td class=" font-14 font-700 black-color  ">22-Oct-2015</td>
								<td class=" font-14 font-700 black-color  "></td>
								<td class=" font-14 font-700 black-color  ">419</td>
								<td class=" font-14 font-700 black-color  ">ASIA RUBY I</td>
								<td class=" font-14 font-700 black-color  ">621</td>
								<td class=" font-14 font-700 black-color  ">VIXXEN</td>
								<td class=" font-14 font-700 black-color  ">AGG 324</td>
								<td class=" font-14 font-700 black-color  ">DELIVERY</td>
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">22-Oct-2015</td>
								<td class=" font-14 font-700 black-color  "></td>
								<td class=" font-14 font-700 black-color  ">67</td>
								<td class=" font-14 font-700 black-color  ">ASIA RUBY I</td>
								<td class=" font-14 font-700 black-color  ">779</td>
								<td class=" font-14 font-700 black-color  ">EY KING</td>
								<td class=" font-14 font-700 black-color  ">GFB 545</td>
								<td class=" font-14 font-700 black-color  ">DELIVERY</td>		
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">22-Oct-2015</td>
								<td class=" font-14 font-700 black-color  "></td>
								<td class=" font-14 font-700 black-color  ">419</td>
								<td class=" font-14 font-700 black-color  ">ASIA RUBY I</td>
								<td class=" font-14 font-700 black-color  ">621</td>
								<td class=" font-14 font-700 black-color  ">VIXXEN</td>
								<td class=" font-14 font-700 black-color  ">AGG 324</td>
								<td class=" font-14 font-700 black-color  ">DELIVERY</td>
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">22-Oct-2015</td>
								<td class=" font-14 font-700 black-color  "></td>
								<td class=" font-14 font-700 black-color  ">67</td>
								<td class=" font-14 font-700 black-color  ">ASIA RUBY I</td>
								<td class=" font-14 font-700 black-color  ">779</td>
								<td class=" font-14 font-700 black-color  ">EY KING</td>
								<td class=" font-14 font-700 black-color  ">GFB 545</td>
								<td class=" font-14 font-700 black-color  ">DELIVERY</td>		
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>