
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="reports.php">Reports</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color">Consignee Balance Summary</li>
				</ul>
			</div>
			<div class="center-main">
				<p class="f-left font-20 font-400">Consignee Balance Summary for Alpha Foods</p>
				<button class="btn general-btn f-right">Download CSV</button>
				<div class="clear"></div>

				<div class="margin-top-30 consignee-summary padding-all-10 border-top border-blue box-shadow-dark">
					<table class="tbl-4c3h">
						<thead>
							<tr>
								<th class=" font-14 font-700 black-color ">Vessel Name</th>
								<th class=" font-14 font-700 black-color  ">Completion Date</th>
								<th class=" font-14 font-700 black-color  ">Aging Days</th>
								<th class=" font-14 font-700 black-color  ">B/L Quantity</th>
								<th class=" font-14 font-700 black-color  ">Guaranteed (99%)</th>
								<th class=" font-14 font-700 black-color  ">FOB Bulk Ex Vessel</th>
								<th class=" font-14 font-700 black-color  ">FOT Bulk Ex Vessel</th>
								<th class=" font-14 font-700 black-color  ">FOT Bagged Ex Warehouse</th>
								<th class=" font-14 font-700 black-color  ">FOT Bulk Ex Warehouse</th>							
								<th class=" font-14 font-700 black-color  ">Balance</th>							
							</tr>
						</thead>
						<tbody class="text-center">
							<tr>
								<td class=" font-14 font-700 black-color  ">Vessel Name</td>
								<td class=" font-14 font-700 black-color  ">Completion Date</td>
								<td class=" font-14 font-700 black-color  ">Aging Days</td>
								<td class=" font-14 font-700 black-color  ">B/L Quantity</td>
								<td class=" font-14 font-700 black-color  ">Guaranteed (99%)</td>
								<td class=" font-14 font-700 black-color  ">FOB Bulk Ex Vessel</td>
								<td class=" font-14 font-700 black-color  ">FOT Bulk Ex Vessel</td>
								<td class=" font-14 font-700 black-color  ">FOT Bagged Ex Warehouse</td>
								<td class=" font-14 font-700 black-color  ">FOT Bulk Ex Warehouse</td>							
								<td class=" font-14 font-700 black-color  ">Balance</td>								
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">Vessel Name</td>
								<td class=" font-14 font-700 black-color  ">Completion Date</td>
								<td class=" font-14 font-700 black-color  ">Aging Days</td>
								<td class=" font-14 font-700 black-color  ">B/L Quantity</td>
								<td class=" font-14 font-700 black-color  ">Guaranteed (99%)</td>
								<td class=" font-14 font-700 black-color  ">FOB Bulk Ex Vessel</td>
								<td class=" font-14 font-700 black-color  ">FOT Bulk Ex Vessel</td>
								<td class=" font-14 font-700 black-color  ">FOT Bagged Ex Warehouse</td>
								<td class=" font-14 font-700 black-color  ">FOT Bulk Ex Warehouse</td>							
								<td class=" font-14 font-700 black-color  ">Balance</td>							
								
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">Vessel Name</td>
								<td class=" font-14 font-700 black-color  ">Completion Date</td>
								<td class=" font-14 font-700 black-color  ">Aging Days</td>
								<td class=" font-14 font-700 black-color  ">B/L Quantity</td>
								<td class=" font-14 font-700 black-color  ">Guaranteed (99%)</td>
								<td class=" font-14 font-700 black-color  ">FOB Bulk Ex Vessel</td>
								<td class=" font-14 font-700 black-color  ">FOT Bulk Ex Vessel</td>
								<td class=" font-14 font-700 black-color  ">FOT Bagged Ex Warehouse</td>
								<td class=" font-14 font-700 black-color  ">FOT Bulk Ex Warehouse</td>							
								<td class=" font-14 font-700 black-color  ">Balance</td>															
							</tr>
							
							
						</tbody>
					</table>
					
				</div>

			
			</div>
		</div>
