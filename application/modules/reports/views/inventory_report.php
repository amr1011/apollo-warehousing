
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="reports.php">Reports</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color">Inventory Report</li>
				</ul>
			</div>
			<div class="center-main">
				<p class="f-left font-20 font-400">Inventory Report for Nov. 1, 2015</p>
				<button class="btn general-btn f-right">Download CSV</button>
				<div class="clear"></div>
				<div class="margin-top-30 consignee-summary padding-all-10 border-top border-blue box-shadow-dark">
					<table class="tbl-4c3h">
						<thead>
							<tr>
								<th class=" font-14 font-700 black-color ">SKU No.</th>
								<th class=" font-14 font-700 black-color  ">Product Name</th>
								<th class=" font-14 font-700 black-color  ">Brand</th>
								<th class=" font-14 font-700 black-color  ">Consignee</th>
								<th class=" font-14 font-700 black-color  ">Unit</th>
								<th class=" font-14 font-700 black-color  ">Min QTY</th>
								<th class=" font-14 font-700 black-color  ">Max QTY</th>
								<th class=" font-14 font-700 black-color  ">Total Received</th>
								<th class=" font-14 font-700 black-color  ">Total Consumed</th>
								<th class=" font-14 font-700 black-color  ">Total Onhand</th>
							</tr>
						</thead>
						<tbody class="text-center">
							<tr>
								<td class=" font-14 font-700 black-color  ">553</td>
								<td class=" font-14 font-700 black-color  ">Australian Wheat</td>
								<td class=" font-14 font-700 black-color  ">Standard</td>
								<td class=" font-14 font-700 black-color  ">Alpha Foods</td>
								<td class=" font-14 font-700 black-color  ">Pieces</td>
								<td class=" font-14 font-700 black-color  ">3,000,000</td>
								<td class=" font-14 font-700 black-color  ">15,000,000</td>
								<td class=" font-14 font-700 black-color  ">5,000,000</td>
								<td class=" font-14 font-700 black-color  ">2,000,000</td>
								<td class=" font-14 font-700 black-color  ">3,000,000</td>
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">791</td>
								<td class=" font-14 font-700 black-color  ">Japanese Wheat</td>
								<td class=" font-14 font-700 black-color  ">Standard</td>
								<td class=" font-14 font-700 black-color  ">Charlie Chicken</td>
								<td class=" font-14 font-700 black-color  ">Pieces</td>
								<td class=" font-14 font-700 black-color  ">2,000,000</td>
								<td class=" font-14 font-700 black-color  ">15,000,000</td>
								<td class=" font-14 font-700 black-color  ">5,000,000</td>
								<td class=" font-14 font-700 black-color  ">3,000,000</td>
								<td class=" font-14 font-700 black-color  ">2,000,000</td>	
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">43</td>
								<td class=" font-14 font-700 black-color  ">Japanese Corn</td>
								<td class=" font-14 font-700 black-color  ">Standard</td>
								<td class=" font-14 font-700 black-color  ">Beta Feeds</td>
								<td class=" font-14 font-700 black-color  ">Pieces</td>
								<td class=" font-14 font-700 black-color  ">3,000,000</td>
								<td class=" font-14 font-700 black-color  ">15,000,000</td>
								<td class=" font-14 font-700 black-color  ">5,000,000</td>
								<td class=" font-14 font-700 black-color  ">2,000,000</td>
								<td class=" font-14 font-700 black-color  ">3,000,000</td>	
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">980</td>
								<td class=" font-14 font-700 black-color  ">American Corn</td>
								<td class=" font-14 font-700 black-color  ">Standard</td>
								<td class=" font-14 font-700 black-color  ">Charlie Chicken</td>
								<td class=" font-14 font-700 black-color  ">Pieces</td>
								<td class=" font-14 font-700 black-color  ">2,000,000</td>
								<td class=" font-14 font-700 black-color  ">15,000,000</td>
								<td class=" font-14 font-700 black-color  ">5,000,000</td>
								<td class=" font-14 font-700 black-color  ">3,000,000</td>
								<td class=" font-14 font-700 black-color  ">2,000,000</td>		
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
