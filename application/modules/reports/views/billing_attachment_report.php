
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="reports.php">Reports</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color">Billing Attachment Report</li>
				</ul>
			</div>
			<div class="center-main">
				<p class="f-left font-20 font-400 line-height-25">Billing Attachment Report for Oct. 1 - Nov. 1, 2015</p>
				<button class="btn general-btn f-right">Download CSV</button>
				<div class="clear"></div>
				<div class="margin-top-10">
					<div class="text-left width-70per line-height-30">
						<div>
							<p class="font-bold display-inline-top width-25per">Vessel Name:</p>
							<p class="display-inline-top font-400"> Asia Ruby I</p>
						</div>
						<div>
							<p class="font-bold display-inline-top width-25per">Cargo:</p>
							<p class="display-inline-top font-400">Australian Wheat</p>
						</div>
						<div>
							<p class="font-bold display-inline-top width-25per">Consignee:</p>
							<p class="display-inline-top font-400">Bounty Fresh</p>
						</div>
					</div>
					<div class="text-left margin-top-30 width-70per line-height-30">
						<div>
							<p class="font-bold display-inline-top width-25per">Vessel Arrived:</p>
							<p class="display-inline-top width-30per font-400"> 22-Oct-2015</p>
							<p class="font-bold display-inline-top  width-25per">Allocation:</p>
							<p class="display-inline-top font-400">10,000.00</p>
						</div>
						<div>
							<p class="font-bold display-inline-top width-25per">Vessel Completion:</p>
							<p class="display-inline-top width-30per font-400">30-Oct-2015</p>
							<p class="font-bold display-inline-top width-25per">Guarantee:</p>
							<p class="display-inline-top font-400">9,900.00</p>
						</div>
						<div>
							<p class="font-bold display-inline-top width-25per">Cutoff for Storage</p>
							<p class="display-inline-top width-30per font-400">30-Nov-2015</p>
							<p class="font-bold display-inline-top">Cargo Swapping:</p>
						</div>
						<div>
							<p class="font-bold display-inline-top width-25per">Delivery Completion:</p>
							<p class="display-inline-top  width-30per font-400">N/A</p>
							<p class="font-bold display-inline-top width-25per">Remaining Balance:</p>
							<p class="display-inline-top font-400">N/A</p>
						</div>
						<div>
							<p class="font-bold display-inline-top width-25per">Aging:</p>
							<p class="display-inline-top  width-30per font-400">01 Day/s</p>
							<p class="font-bold display-inline-top width-25per">Mode of Delivery:</p>
							<p class="display-inline-top font-400">N/A</p>
						</div>
					</div>
				</div>

				<div class="margin-top-30 consignee-summary border-top border-blue box-shadow-dark padding-all-10">

					<table class="tbl-4c3h">
						<thead> 
							<tr>
								<th class=" font-14 font-700 black-color ">Date of Exit</th>
								<th class=" font-14 font-700 black-color  ">CDR No.</th>
								<th class=" font-14 font-700 black-color  ">ATL No.</th>
								<th class=" font-14 font-700 black-color  ">Gate Pass No.</th>
								<th class=" font-14 font-700 black-color  ">Storage</th>
								<th class=" font-14 font-700 black-color  ">TARE IN</th>
								<th class=" font-14 font-700 black-color  ">TARE OUT</th>
								<th class=" font-14 font-700 black-color  ">No. of Bags / Bulk</th>
								<th class=" font-14 font-700 black-color  ">Less Sack Weight</th>
								<th class=" font-14 font-700 black-color  ">Trucking Small / Vessel Name</th>
								<th class=" font-14 font-700 black-color  ">Plate No.</th>
								<th class=" font-14 font-700 black-color  ">Balance per Day</th>
							</tr>
						</thead>
						<tbody class="text-center">
							<tr>
								<td class=" font-14 font-700 black-color  ">22-Oct-2015</td>
								<td class=" font-14 font-700 black-color  ">615780</td>
								<td class=" font-14 font-700 black-color  ">887643</td>
								<td class=" font-14 font-700 black-color  ">GPN14496054442392</td>
								<td class=" font-14 font-700 black-color  ">B1</td>
								<td class=" font-14 font-700 black-color  ">10.81</td>
								<td class=" font-14 font-700 black-color  ">50.22</td>
								<td class=" font-14 font-700 black-color  ">600</td>
								<td class=" font-14 font-700 black-color  ">27.17</td>
								<td class=" font-14 font-700 black-color  ">VIXXEN</td>
								<td class=" font-14 font-700 black-color  ">AGG 324</td>
								<td class=" font-14 font-700 black-color  ">282</td>
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">22-Oct-2015</td>
								<td class=" font-14 font-700 black-color  ">701937</td>
								<td class=" font-14 font-700 black-color  ">435382</td>
								<td class=" font-14 font-700 black-color  ">GPN15336987032657</td>
								<td class=" font-14 font-700 black-color  ">B1</td>
								<td class=" font-14 font-700 black-color  ">10.68</td>
								<td class=" font-14 font-700 black-color  ">40.26</td>
								<td class=" font-14 font-700 black-color  ">500</td>
								<td class=" font-14 font-700 black-color  ">28.80</td>
								<td class=" font-14 font-700 black-color  ">EY KING</td>
								<td class=" font-14 font-700 black-color  ">GFB 545</td>
								<td class=" font-14 font-700 black-color  ">942</td>
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">22-Oct-2015</td>
								<td class=" font-14 font-700 black-color  ">628189</td>
								<td class=" font-14 font-700 black-color  ">467235</td>
								<td class=" font-14 font-700 black-color  ">GPN19810000351732</td>
								<td class=" font-14 font-700 black-color  ">B1</td>
								<td class=" font-14 font-700 black-color  ">15.44</td>
								<td class=" font-14 font-700 black-color  ">30.00</td>
								<td class=" font-14 font-700 black-color  ">BULK</td>
								<td class=" font-14 font-700 black-color  ">23.39</td>
								<td class=" font-14 font-700 black-color  ">VIXXEN</td>
								<td class=" font-14 font-700 black-color  ">AGG 324</td>
								<td class=" font-14 font-700 black-color  ">156</td>
							</tr>
							<tr>
								<td class=" font-14 font-700 black-color  ">22-Oct-2015</td>
								<td class=" font-14 font-700 black-color  ">818470</td>
								<td class=" font-14 font-700 black-color  ">302662</td>
								<td class=" font-14 font-700 black-color  ">GPN16354281196577</td>
								<td class=" font-14 font-700 black-color  ">B1</td>
								<td class=" font-14 font-700 black-color  ">17.25</td>
								<td class=" font-14 font-700 black-color  ">80.21</td>
								<td class=" font-14 font-700 black-color  ">120</td>
								<td class=" font-14 font-700 black-color  ">26.67</td>
								<td class=" font-14 font-700 black-color  ">EY KING</td>
								<td class=" font-14 font-700 black-color  ">GFB 545</td>
								<td class=" font-14 font-700 black-color  ">200</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>