	
		<div class="main">
			<div class="breadcrumbs no-margin-left padding-left-50">			
				<ul>
					<li><a href="reports.php">Reports</a></li>
					<li><span>&gt;</span></li>						
					<li class="font-bold black-color">Consignee Summary Report</li>
				</ul>
			</div>
			<div class="center-main">
				<p class="f-left font-20 font-400">Consignee Summary Report for Jan. 1 - Feb. 1, 2015</p>
				<button class="btn general-btn f-right">Download CSV</button>
				<div class="clear"></div>

				<div class="margin-top-30 consignee-summary border-top border-blue padding-all-10 box-shadow-dark">
					<table class="tbl-4c3h">
						<thead>
							<tr>
								<th class=" font-14 font-700 black-color padding-top-5 padding-bottom-5  " rowspan="2">Consignee</th>
								<th class=" font-14 font-700 black-color padding-top-5 padding-bottom-5 color-light-gray" colspan="4">ASIA RUBY I</th>
								<th class=" font-14 font-700 black-color padding-top-5 padding-bottom-5 bggray-darkgray" colspan="4">Southern Light</th>
							</tr>
							<tr>
								<th class=" font-14 font-700 black-color border-top-none padding-top-5 padding-bottom-5 ">BL Quantity</th>
								<th class=" font-14 font-700 black-color border-top-none padding-top-5 padding-bottom-5   ">Guaranteed</th>
								<th class=" font-14 font-700 black-color border-top-none padding-top-5 padding-bottom-5  ">Withdrawn</th>
								<th class=" font-14 font-700 black-color border-top-none padding-top-5 padding-bottom-5  ">Balance</th>
								<th class=" font-14 font-700 black-color border-top-none padding-top-5 padding-bottom-5  ">BL Quantity</th>
								<th class=" font-14 font-700 black-color border-top-none padding-top-5 padding-bottom-5  ">Guaranteed</th>
								<th class=" font-14 font-700 black-color border-top-none padding-top-5 padding-bottom-5  ">Withdrawn</th>
								<th class=" font-14 font-700 black-color border-top-none padding-top-5 padding-bottom-5  ">Balance</th>
							</tr>
						</thead>
						<tbody class="text-center">
							<tr>
								<td class=" font-14 font-bold black-color  ">BOUNTY AGRO VENTURE</td>
								<td class=" font-14 font-700 black-color  ">7,314.49</td>
								<td class=" font-14 font-700 black-color  ">7,241.35</td>
								<td class=" font-14 font-700 black-color  ">7,239.35</td>
								<td class=" font-14 font-700 black-color  ">2.02</td>
								<td class=" font-14 font-700 black-color  ">7,314.49</td>
								<td class=" font-14 font-700 black-color  ">7,241.35</td>
								<td class=" font-14 font-700 black-color  ">7,239.35</td>
								<td class=" font-14 font-700 black-color  ">2.02</td>
							</tr>
							<tr>
								<td class=" font-14 font-bold black-color  ">BOUNTY FRESH</td>
								<td class=" font-14 font-700 black-color  ">9,220.11</td>
								<td class=" font-14 font-700 black-color  ">9,127.91</td>
								<td class=" font-14 font-700 black-color  ">9,063.06</td>
								<td class=" font-14 font-700 black-color  ">64.85</td>
								<td class=" font-14 font-700 black-color  ">9,220.11</td>
								<td class=" font-14 font-700 black-color  ">9,127.91</td>
								<td class=" font-14 font-700 black-color  ">9,063.06</td>
								<td class=" font-14 font-700 black-color  ">64.85</td>
							</tr>
							<tr>
								<td class=" font-14 font-bold black-color  ">BOUNTY FARMS</td>
								<td class=" font-14 font-700 black-color  ">1,347.41</td>
								<td class=" font-14 font-700 black-color  ">1,333.93</td>
								<td class=" font-14 font-700 black-color  ">1,333.67</td>
								<td class=" font-14 font-700 black-color  ">0.26</td>
								<td class=" font-14 font-700 black-color  ">1,347.41</td>
								<td class=" font-14 font-700 black-color  ">1,333.93</td>
								<td class=" font-14 font-700 black-color  ">1,333.67</td>
								<td class=" font-14 font-700 black-color  ">0.26</td>	
							</tr>
							<tr>
								<td class=" font-14 font-bold black-color  ">HYPIG GENETICS</td>
								<td class=" font-14 font-700 black-color  ">1,366.66</td>
								<td class=" font-14 font-700 black-color  ">1,333.93</td>
								<td class=" font-14 font-700 black-color  ">1,352.99</td>
								<td class=" font-14 font-700 black-color  ">0</td>
								<td class=" font-14 font-700 black-color  ">1,366.66</td>
								<td class=" font-14 font-700 black-color  ">1,352.99</td>
								<td class=" font-14 font-700 black-color  ">1,352.99</td>
								<td class=" font-14 font-700 black-color  ">0</td>			
							</tr>
							<tr>
								<td class=" font-14 font-bold black-color  ">INOZA FEED MILLING CORP</td>
								<td class=" font-14 font-700 black-color  ">962.43</td>
								<td class=" font-14 font-700 black-color  ">952.81</td>
								<td class=" font-14 font-700 black-color  ">952.18</td>
								<td class=" font-14 font-700 black-color  ">0.63</td>
								<td class=" font-14 font-700 black-color  ">962.43</td>
								<td class=" font-14 font-700 black-color  ">952.81</td>
								<td class=" font-14 font-700 black-color  ">952.18</td>
								<td class=" font-14 font-700 black-color  ">0.63</td>		
							</tr>
							<tr>
								<td class=" font-14 font-bold black-color  ">BULACAN HOG</td>
								<td class=" font-14 font-700 black-color  ">239.54</td>
								<td class=" font-14 font-700 black-color  ">290.61</td>
								<td class=" font-14 font-700 black-color  ">291.55</td>
								<td class=" font-14 font-700 black-color  ">-0.95</td>
								<td class=" font-14 font-700 black-color  ">239.54</td>
								<td class=" font-14 font-700 black-color  ">290.61</td>
								<td class=" font-14 font-700 black-color  ">291.55</td>
								<td class=" font-14 font-700 black-color  ">-0.95</td>		
							</tr>
							<tr class="color-light-gray">
								<td class=" font-14 font-bold black-color padding-top-10 padding-bottom-10">Total</td>
								<td class=" font-14 font-700 black-color padding-top-10 padding-bottom-10">20,450.64</td>
								<td class=" font-14 font-700 black-color padding-top-10 padding-bottom-10">20,299.60</td>
								<td class=" font-14 font-700 black-color padding-top-10 padding-bottom-10">20,232.78</td>
								<td class=" font-14 font-700 black-color padding-top-10 padding-bottom-10">66.81</td>
								<td class=" font-14 font-700 black-color padding-top-10 padding-bottom-10">20,450.64</td>
								<td class=" font-14 font-700 black-color padding-top-10 padding-bottom-10">20,299.60</td>
								<td class=" font-14 font-700 black-color padding-top-10 padding-bottom-10">20,232.78</td>
								<td class=" font-14 font-700 black-color padding-top-10 padding-bottom-10">66.81</td>		
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		