<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	public function index()
	{
		$user_data = $this->session->userdata('apollo_user');
		if($user_data === null)
		{
			$this->load->view('login_view');
		}
		else
		{
			redirect(BASEURL.'dashboard');
		}

	}

	public function unset_user(){
		 $this->session->unset_userdata('apollo_user');
		 redirect(BASEURL.'login');
	}

	public function authenticate(){
		$this->load->library('form_validation');
		$response = array();

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$this->form_validation->set_rules("username", "Username", "trim|required");
		$this->form_validation->set_rules("password", "Password", "trim|required");
		

		if($this->form_validation->run() === FALSE)
		{
			$response["status"] = false;
			$response["message"] = validation_errors();
			$response["data"] = $this->form_validation->error_array();
			echo json_encode($response);
		}
		else
		{
			if(IS_DEVELOPMENT === true)
			{
				$user_development = array(
					"id" => 10000,
					"email" => "apollo@email.com",
					"username" => "apollo",
					"password" => "62cc2d8b4bf2d8728120d052163a77df",
					"company_id" => 1,
					"role_id" => 1,
					"account_type" => 1,
					"first_name" => "apollo",
					"last_name" => "developer",
					"middle_name" => "",
					"image" => BASEURL."assets/images/profile/profile_a.png"
				);
				if(md5($password) !== $user_development["password"] || $username !== $user_development["username"]){
					header("Content-type: application/json");
					$response["status"] = false;
					$response["message"] = "Username or password did not match!";
					$response["data"] = array();
					echo json_encode($response);
				}else{
					$this->session->set_userdata('apollo_user', $user_development);
					header("Content-type: application/json");
					$response["status"] = true;
					$response["message"] = "Success!";
					$response["data"] = array();
					echo json_encode($response);
				}
			}
			else
			{
				//set here the database query for user that will be saved on session
			}
		}
	}

}

/* End of file login.php */
/* Location: ./application/modules/login/controllers/login.php */