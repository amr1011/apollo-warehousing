<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <link rel="shortcut icon" href="assets/ico/favicon.ico">

        <title>Apollo Login</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo BASEURL; ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo BASEURL; ?>assets/css/font-awesome.min.css" rel="stylesheet">

        <!-- Core CSS -->
    
    
        <!-- Custom CSS -->
        <link href="<?php echo BASEURL; ?>assets/css/global/commons.css" rel="stylesheet">
        <link href="<?php echo BASEURL; ?>assets/css/global/ui-widgets.css" rel="stylesheet">
        <link href="<?php echo BASEURL; ?>assets/css/login.css" rel="stylesheet">
        <link href="<?php echo BASEURL; ?>assets/css/override.css" rel="stylesheet">
    </head>
    <body class="login">

        <section class="login-parent content-overflow-y">
            <div class="bg-img"></div>
            <div class="login-form">
                <form id="formLogin">
                    <div class="image-content">
                        <img src="<?php echo BASEURL; ?>assets/images/logo-old.svg" alt="autom8 logo"/>
                    </div>
                    <!-- <h2 class="gray-color">Sign in to Apollo Warehousing</h2> -->
                    <div class="form-group">
                    	<div class="red-color error-message"></div>
                        <input type="text" placeholder="Username" class="username" datavalid="required" labelinput="username">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="password" datavalid="required" labelinput="password">
                    </div>
                    <p class="forgot"><a href="">Can't Remember your Password?</a></p>
                    <button type="button" class="btn general-btn">Login</button>
                </form>
            </div>
        </section>

		<script type="text/javascript" src="<?php echo BASEURL; ?>assets/js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="<?php echo BASEURL; ?>assets/js/core/platform.js"></script>
		<script type="text/javascript" src="<?php echo BASEURL; ?>assets/js/jquery.cookie.js"></script>
		<script type="text/javascript" src="<?php echo BASEURL; ?>assets/js/core/config.js"></script>
		<script type="text/javascript" src="<?php echo BASEURL; ?>assets/js/core/ConnectionDetector.js"></script>
		<script type="text/javascript" src="<?php echo BASEURL; ?>assets/js/core/integr8formvalidation.js"></script>
		<script type="text/javascript" src="<?php echo BASEURL; ?>assets/js/modules/login.js"></script>

    </body>
</html>